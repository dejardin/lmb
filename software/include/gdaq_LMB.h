#ifndef EXTERN
  #define EXTERN
  #define EXTERNC
#else
  #define EXTERNC extern "C"
#endif

#include "gtk/gtk.h"
#include "uhal/uhal.hpp"
#include <signal.h>
#include "limits.h"

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TStyle.h>
#include <TRandom.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>
#include <TVirtualFFT.h>

#define I2C_SHIFT_DEV_NUMBER   0
#define N_ADC                  3
#define N_CHANNEL              25

// LMB_SETTINGS
#define TRIGGER_MASK           (1<<0) // to mask output signals
#define LED_ON                 (1<<8)
#define GENE_BC0               (1<<9)
#define GENE_WTE               (1<<10)
#define SELF_TRIGGER           (1<<11)
#define SELF_TRIGGER_MASK      (1<<12) // to select which ADC generate self trigger
#define SELF_TRIGGER_MODE      (1<<15)
#define WTE_POS                (1<<16)
#define DELAY_SCAN             (1<<31)

// ACTIONS
#define LMB_RESET              (1<<0)
#define CLOCK_RESET            (1<<1)
#define JESD_RESET             (1<<2)
#define IO_RESET               (1<<3)
#define I2C_RESET              (1<<4)
#define PLL_RESET              (1<<5)
#define ADC_RESET              (1<<6)
#define AXI_RESET              (1<<7)
#define EXTRA_TRIGGER          (1<<30)
#define GENE_TRIGGER           (1<<31)

// DAQ_DELAY
#define HW_DAQ_DELAY           (1<<0)
#define SW_DAQ_DELAY           (1<<16)

// DRP_XADC
#define XADC_DATA              (1<<0)
#define XADC_ADDR              (1<<16)
#define DRP_WRb                (1<<31)

// AXI_ADDR
#define AXI_RWB                (1<<31)

// I2C_BUS
#define I2C_BUS                (1<<0)
// I2C_CTRL
#define I2C_DATA               (1<<0)
#define I2C_ADDR               (1<<16)
#define I2C_LONG               (1<<30)
#define I2C_RWB                (1<<31)

// RESYNC_IDLE
#define RESYNC_IDLE_PATTERN    (1<<0)

// CAP_CTRL
#define CAPTURE_START          (1<<0)
#define CAPTURE_STOP           (1<<1)
//#define N_DAQ_FRAME_MAX        4096    // we are acquiring frames of 128 bits (8 samples per 160 MHz clock)
#define N_DAQ_FRAME_MAX       49152    // we are acquiring frames of 128 bits (8 samples per 160 MHz clock)
#define MAX_PAYLOAD            1380

using namespace uhal;

typedef struct
{
  GtkWindow *main_window;
  GtkWindow *DAQ_window;
  GtkWindow *trigger_window;
  GtkWindow *actions_window;
  GtkWindow *map_window;
  GtkBuilder *builder;
  gpointer user_data;
  gint window_xpos0;
  gint window_ypos0;
  gint window_xoffset;
  gint window_yoffset;
  gint window_width[7];
  gint window_height[7];
} SGlobalData;
EXTERN SGlobalData gtk_data;

typedef struct
{
  Int_t  DTU_BL_G10[N_CHANNEL], DTU_BL_G1[N_CHANNEL];
  bool DTU_force_G1[N_CHANNEL], DTU_force_G10[N_CHANNEL], DTU_G1_window16[N_CHANNEL], DTU_eLink[N_CHANNEL];
  bool DTU_status[N_CHANNEL], channel_status[N_CHANNEL], ADC_status[N_ADC], twos_complement[N_ADC];
  Int_t I2C_address[N_CHANNEL], I2C_bus[N_CHANNEL];
  Int_t DAC_value[3];
  Int_t PLL_I2C_RWb, PLL_register_number, PLL_register_value; 
  Int_t ADC_SPI_RWb, ADC_number, ADC_register_number, ADC_register_value;
} AsicSetting_s;
EXTERN AsicSetting_s asic;

typedef struct
{
  Int_t LMB_number, firmware_version, ndaq_frame, nsample, nevent, numrun, cur_evt;
  Int_t PLL_freq;
  Int_t daq_initialized;
  Int_t old_read_address, I2C_shift_dev_number, I2C_lpGBT_mode;
  Int_t I2C_DTU_nreg, DTU_test_mode;
  Int_t trigger_type, self_trigger, self_trigger_mode, self_trigger_threshold, self_trigger_mask;
  Int_t fifo_mode;         // Use memory in FIFO mode or not
  Int_t sw_DAQ_delay, hw_DAQ_delay, resync_idle_pattern, resync_phase, resync_data;
  Int_t trigger_delay[8], trigger_width[8], trigger_number, trigger_mask, trigger_inhibit_duration, trigger_tap_value[3];
  Int_t autorun, error, ADC_pattern[4], ADC_mode, WTE_pos;
  Int_t XADC_Temp[5], XADC_leak[5];
  Int_t AXI_RWb[3], AXI_reg_number[3], AXI_reg_value[3];
  UInt_t trigger_map, JESD_reset_delay;
  Long_t timestamp;
  Short_t *event[N_ADC];
  Double_t *fevent[N_ADC];
  bool led_ON, gen_BC0, gen_WTE, ADC_scrambling, follow_FIFO, delay_scan;
  bool dump_data, wait_for_ever, daq_running, calibration_running;
  bool debug_DAQ, debug_I2C, debug_GTK, debug_draw, zoom_draw, DAQ_timeout;
  ValVector< uint32_t > mem;
  TGraph *tg[N_ADC];
  TH1D *hmean[N_ADC], *hrms[N_ADC], *hdensity[N_ADC];
  TProfile *pshape[N_ADC];
  TCanvas *c1, *c2, *c3;
  TTree *tdata;
  TFile *fd;
  char last_fname[132];
  char xml_filename[PATH_MAX], comment[1024];
} DAQSetting_s;
EXTERN DAQSetting_s daq;

using namespace uhal;

EXTERN struct timeval tv;
EXTERN void read_conf_from_xml_file(char*);
EXTERN void write_conf_to_xml_file(char*);
EXTERN Int_t synchronize_links(uhal::HwInterface, Int_t);
EXTERN std::vector<uhal::HwInterface> devices;
EXTERN void init_gDAQ(void);
EXTERN void init_PLL(void);
EXTERN void init_ADCs(Int_t init);
EXTERN void init_FIFO(Int_t init);
EXTERN void init_JESD204(Int_t channel, Int_t init);
EXTERN void take_run(void);
EXTERN void set_ped_dac(int);
EXTERN void get_temp(void);
EXTERN void get_baseline(void);
EXTERN void get_event(Int_t, Int_t);
EXTERN void get_single_event(void);
EXTERN void end_of_run(void);
EXTERN void update_VFE_control(Int_t);
EXTERN void update_trigger(void);
EXTERN void update_DAQ_settings(void);
EXTERN void update_ADC_patterns(Int_t,Int_t);
EXTERN void update_channel(void);

EXTERNC G_MODULE_EXPORT void delete_event(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void callback_about(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void my_exit(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void button_clicked(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void switch_toggled(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void Entry_modified(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void Entry_changed(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void read_config(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void save_config(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void save_config_as(GtkMenuItem*, gpointer);
