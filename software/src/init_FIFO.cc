#define EXTERN extern
#include "gdaq_LMB.h"

void init_FIFO(Int_t init)
{
  UInt_t device_number, val;
  UInt_t command, reg_loc;
  ValWord<uint32_t> reg;
  Int_t ADC_reg_val[2][76];
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char reg_name[80];
  UInt_t FIFO_content[1024] = {0};
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;


  uhal::HwInterface hw=devices.front();
  if(init==0) // get current sequence position
  {
    reg=hw.getNode("FIFO_CUR").read(); // Read current monitoring setting
    hw.dispatch();
    printf("Current monitoring setting :%8.8x\n",reg.value());
  }
  else if(init==1) // Load FIFO content
  {
// Read FIFO content from file :
    FILE *fd=fopen("xml/monitoring_sequence.txt","r");
    if (fd == NULL)
    {
       printf("Sequence description file not found\n");
       return;
    }
    Int_t FIFO_length = 0;
    Int_t LMR, evt_type, n_evt;
    while ((nread = getline(&line, &len, fd)) != EOF)
    {
      if (line[0]=='#') continue;
      printf("Retrieved line of length %zd:\n", nread);
      fwrite(line, nread, 1, stdout);
      sscanf(line,"%d %d %d",&LMR, &evt_type, &n_evt);
      FIFO_content[FIFO_length++]=LMR<<24 | evt_type<<16 | n_evt;
    }
    free(line);
    fclose(fd);

    printf("Start FIFO initialisation\n");
    hw.getNode("FIFO_LENGTH").write(FIFO_length);
    hw.getNode("FIFO_ADDR").write(0); // Write and read address at 0
    hw.getNode("FIFO_CUR").write(0); // Start monitoring sequence at position 0
    for(Int_t ipos=0; ipos<FIFO_length; ipos++)
    {
      hw.getNode("FIFO_CONTENT").write(FIFO_content[ipos]);
    }
    hw.dispatch();
// Read back FIFO content :
    reg=hw.getNode("FIFO_LENGTH").read();
    hw.dispatch();
    printf("FIFO length : %d\n",reg.value());
    reg=hw.getNode("FIFO_ADDR").read(); // Write and read address
    hw.dispatch();
    printf("FIFO W/R addresses : %8.8x\n",reg.value());
    UInt_t read_address=reg.value()&0xffff;
    for(Int_t ipos=0; ipos<FIFO_length; ipos++)
    {
      reg=hw.getNode("FIFO_CONTENT").read();
      hw.dispatch();
      printf("FIFO content at %d : %8.8x\n",read_address+ipos,reg.value());
    }
  }
  else if(init==2) // follow FIFO sequence
  {
    Int_t nseq=0;
    UInt_t seq_old=0;
    while(nseq<100 && daq.follow_FIFO)
    {
      reg=hw.getNode("FIFO_CUR").read();
      hw.dispatch();
      if((reg.value()>>16) != (seq_old>>16))
      {
        printf("Sequence change : was %8.8x, now %8.8x\n",seq_old,reg.value());
        seq_old=reg.value();
        nseq++;
      }
      while (gtk_events_pending()) gtk_main_iteration();
    }
  }
}
