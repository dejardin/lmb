#define EXTERN extern
#include "gdaq_LMB.h"

void init_JESD204(Int_t channel, Int_t init)
{
  UInt_t device_number, val;
  UInt_t command, reg_loc;
  ValWord<uint32_t> reg;
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char IPB_name[80], reg_name[80];

  uhal::HwInterface hw=devices.front();

  if(init==0)
  {
    printf("Reset AXI FSM\n");
// Reset links :
    command = AXI_RESET*1;
    hw.getNode("ACTIONS").write(command);
    hw.dispatch();
  }
  else if(init==1)
  {
    printf("Start JESD204C initialisation\n");
// Reset links :
    command = JESD_RESET*1 | IO_RESET*1;
    //command = JESD_RESET*1;
    hw.getNode("ACTIONS").write(command);
    hw.dispatch();
    command = 0;
    hw.getNode("ACTIONS").write(command);
    hw.dispatch();
    usleep(1000);
    // and get link config from JESD ILAS sequence :
    for(Int_t iADC=1; iADC<=3; iADC++)
    {
      sprintf(reg_name,"ADC%d_CFG0",iADC);
      reg=hw.getNode(reg_name).read();
      hw.dispatch();
      printf("ADC%d(ILAS0) : 0x%8.8x\n",iADC,reg.value());
      sprintf(reg_name,"ADC%d_CFG1",iADC);
      reg=hw.getNode(reg_name).read();
      hw.dispatch();
      printf("ADC%d(ILAS1) : 0x%8.8x\n",iADC,reg.value());
      sprintf(reg_name,"ADC%d_CFG2",iADC);
      reg=hw.getNode(reg_name).read();
      hw.dispatch();
      printf("ADC%d(ILAS2) : 0x%8.8x\n",iADC,reg.value());
      sprintf(reg_name,"ADC%d_CFG3",iADC);
      reg=hw.getNode(reg_name).read();
      hw.dispatch();
      printf("ADC%d(ILAS3) : 0x%8.8x\n",iADC,reg.value());
    }
  }
  else
  {
    printf("Load register %4.4x of JESD%d with %8.8x\n",daq.AXI_reg_number[channel-1], channel, daq.AXI_reg_value[channel-1]);
    sprintf(IPB_name,"AXI%d_DATA",channel);
    command = daq.AXI_reg_value[channel-1];
    printf("Send data command 0x%8.8x\n",command);
    hw.getNode(IPB_name).write(command);
    sprintf(IPB_name,"AXI%d_ADDR",channel);
    command = AXI_RWB*daq.AXI_RWb[channel-1] | daq.AXI_reg_number[channel-1];
    printf("Send addr command 0x%8.8x\n",command);
    hw.getNode(IPB_name).write(command);
    hw.dispatch();
    if(daq.AXI_RWb[channel-1]==1)
    {
      sprintf(IPB_name,"AXI%d_DATA",channel);
      reg=hw.getNode(IPB_name).read();
      hw.dispatch();
      printf("JESD%d read back value : 0x%8.8x\n",channel,reg.value());
      sprintf(widget_name,"4_JESD%d_AXI_reg_value_read",channel);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%8.8x",reg.value());
      gtk_label_set_text((GtkLabel*)widget,widget_text);
    }
  }
}
