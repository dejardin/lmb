#define EXTERN extern
#include "gdaq_LMB.h"

void set_ped_dac(int dac)
{
  UInt_t device_number, val;
  UInt_t I2C_data, command;
  FILE *fcal;
  UInt_t iret=0;
  ValWord<uint32_t> reg;
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char reg_name[80];
  Int_t address[4]={0x68, 0x4e, 0x4e, 0x4e};

  if(daq.daq_initialized<0)return;
  if(dac<0 || dac>3) return;
  uhal::HwInterface hw=devices.front();

  if(dac>0)
  {
    if(asic.DAC_value[dac-1]<0)asic.DAC_value[dac-1]=0;
    if(asic.DAC_value[dac-1]>4095)asic.DAC_value[dac-1]=4095;
    sprintf(widget_name,"4_DAC%d_value",dac);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%d",asic.DAC_value[dac-1]);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);

// I2C DAC protocol is strange : don't have register, but send command number instead
    command=(dac<<24) | (0x30<<16) | asic.DAC_value[dac-1];
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
// wait for end of I2C transaction: bit 31 of return word
    do 
    {
      reg=hw.getNode("PLL_CTRL").read();
      hw.dispatch();
      printf("%d\n",reg.value()>>31);
    } while ((reg.value()>>31) == 1);
    command=(1<<31) | (dac<<24);
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
    do 
    {
      reg=hw.getNode("PLL_CTRL").read();
      hw.dispatch();
      printf("%d\n",reg.value()>>31);
    } while ((reg.value()>>31) == 1);
    printf(" DAC %d: now %4.4d should be %4.4d\n",dac,(reg.value()>>4)&0xfff,asic.DAC_value[dac-1]);
    sprintf(widget_name,"4_DAC%d_value_read",dac);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"read: %d",(reg.value()>>4)&0xfff);
    gtk_label_set_text((GtkLabel*)widget,widget_text);
  }
  else
  {
    command=(asic.PLL_I2C_RWb<<31) | (dac<<24) | (asic.PLL_register_number<<16) | asic.PLL_register_value;
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
    if(asic.PLL_I2C_RWb==1)
    {
      reg=hw.getNode("PLL_CTRL").read();
      hw.dispatch();
    }
  }
}
