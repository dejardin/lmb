#define EXTERN extern
#include "gdaq_LMB.h"
void get_baseline(void)
{
  uhal::HwInterface hw=devices.front();
  ValWord<uint32_t> reg;
  char reg_name[80];
  for(Int_t iADC=1; iADC<=3; iADC++)
  {   
    sprintf(reg_name,"ADC%d_BASELINE",iADC);
    reg=hw.getNode(reg_name).read();
    hw.dispatch();
    printf("ADC%d baseline : %d\n",iADC,reg.value());
  }   
}
