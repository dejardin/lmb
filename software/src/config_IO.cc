#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_LMB.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "limits.h"

void read_config(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Open File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_OPEN,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    strcpy(daq.xml_filename,filename);
    read_conf_from_xml_file (filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);

    char fname[132],fname2[111];
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }

  gtk_widget_destroy (dialog);
  return;
}
void save_config(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Save current config in %s\n",daq.xml_filename);
  write_conf_to_xml_file(daq.xml_filename);
  return;
}

void save_config_as(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Save File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_SAVE,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
  printf("Current file : %s\n",daq.xml_filename);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), daq.xml_filename);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    printf("Select file : %s\n",filename);
    strcpy(daq.xml_filename,filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
    write_conf_to_xml_file(daq.xml_filename);

    char fname[132],fname2[111];
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }

  gtk_widget_destroy (dialog);
  return;
}

void read_conf_from_xml_file(char *filename)
{
  xmlChar *child1_name, *child1_status, *child1_id, *child1_value, *child1_level;
  xmlChar *child2_name, *child2_status, *child2_id, *child2_value, *child2_level, *child2_width, *child2_delay;
  xmlChar *child3_name, *child3_status, *child3_id, *child3_value, *child3_level;
  xmlChar *child4_name, *child4_status, *child4_id, *child4_value, *child4_level;
  xmlDocPtr doc;
  xmlNodePtr top, level_1, level_2, level_3, level_4;
  Int_t loc_VFE, loc_ich;

  char fname[132], widget_name[80], content[132];
  GtkWidget *widget;
  daq.LMB_number=0;
  daq.resync_phase=0;

// disable channels y defaults :
  for(Int_t ich=0; ich<N_ADC; ich++)
  {
    asic.ADC_status[ich]=true;
  }
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    asic.channel_status[ich]=false;
    asic.DTU_status[ich]=false;
    asic.I2C_bus[ich]=-1;
    asic.I2C_address[ich]=-1;
    asic.DTU_G1_window16[ich]=TRUE;
  }

  if(filename==NULL)
    sprintf(fname,"xml/config_LMB.xml");
  else
    strcpy(fname,filename);
  printf("Read conf from xml file %s\n",fname);

  doc = xmlParseFile(fname);
  if (doc == NULL )
  {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }

  top = xmlDocGetRootElement(doc);
  if (top == NULL)
  {
    fprintf(stderr,"empty document\n");
    xmlFreeDoc(doc);
    return;
  }

  printf("Start to parse document : %s\n",fname);
  if (xmlStrcmp(top->name, (const xmlChar *) "lmb"))
  {
    fprintf(stderr,"document of the wrong type, root node != lmb");
    xmlFreeDoc(doc);
    return;
  }
  else
  {
    printf("Starting point : %s\n",top->name);
  }

  level_1 = top->xmlChildrenNode;
  while (level_1 != NULL)
  {
    child1_name=xmlGetProp(level_1,(const xmlChar *)"name");
    child1_value=xmlGetProp(level_1,(const xmlChar *)"value");
    child1_level=xmlGetProp(level_1,(const xmlChar *)"level");
    child1_id=xmlGetProp(level_1,(const xmlChar *)"id");
    child1_status=xmlGetProp(level_1,(const xmlChar *)"status");
    printf("level1 : %s %s : value %s level %s id %s status %s\n",level_1->name,child1_name, child1_value, child1_level, child1_id, child1_status);
    if ((!xmlStrcmp(level_1->name, (const xmlChar *)"object")))
    {
      printf("Start to parse object : %s\n",xmlGetProp(level_1,(const xmlChar *)"name"));

// DAQ settings :
      if ((!xmlStrcmp(child1_name, (const xmlChar *)"DAQ")))
      {
        level_2 = level_1->xmlChildrenNode;
        while (level_2 != NULL)
        {
          //child2_value = xmlNodeListGetString(doc, level_2->xmlChildrenNode, 1);
          child2_name=xmlGetProp(level_2,(const xmlChar *)"name");
          child2_value=xmlGetProp(level_2,(const xmlChar *)"value");
          child2_width=xmlGetProp(level_2,(const xmlChar *)"width");
          child2_delay=xmlGetProp(level_2,(const xmlChar *)"delay");
          child2_level=xmlGetProp(level_2,(const xmlChar *)"level");
          child2_id=xmlGetProp(level_2,(const xmlChar *)"id");
          child2_status=xmlGetProp(level_2,(const xmlChar *)"status");
          printf("level2 : %s %s : value %s level %s id %s status %s\n",level_2->name,child2_name, child2_value, child2_level, child2_id, child2_status);
          printf("delay %s width %s\n", child2_delay, child2_width);
          if ((!xmlStrcmp(level_2->name, (const xmlChar *)"property")))
          {
            if(!xmlStrcmp(child2_name, (const xmlChar *)"LMB"))
            {
              sscanf((const char*)child2_value,"%d",&daq.LMB_number);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_LMB_number");
              g_assert (widget);
              sprintf(content,"%d",daq.LMB_number);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"I2C_shift_dev_number"))
            {
              sscanf((const char*)child2_value,"%d",&daq.I2C_shift_dev_number);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_DAQ"))
            {
              daq.debug_DAQ=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_DAQ");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.debug_DAQ);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_I2C"))
            {
              daq.debug_I2C=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_I2C");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.debug_I2C);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_GTK"))
            {
              daq.debug_GTK=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_draw"))
            {
              daq.debug_draw=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_draw");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.debug_draw);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"zoom"))
            {
              daq.zoom_draw=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_zoom_draw");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.zoom_draw);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"wait_for_ever"))
            {
              daq.wait_for_ever=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_DAQ_timeout");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.wait_for_ever);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"I2C_lpGBT"))
            {
              daq.I2C_lpGBT_mode=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_I2C_protocol");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.I2C_lpGBT_mode);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"HW_delay"))
            {
              sscanf((const char*)child2_value,"%d",&daq.hw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_HW_delay");
              g_assert (widget);
              sprintf(content,"%d",daq.hw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"SW_delay"))
            {
              sscanf((const char*)child2_value,"%d",&daq.sw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_SW_delay");
              g_assert (widget);
              sprintf(content,"%d",daq.sw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_inhibit"))
            {
              sscanf((const char*)child2_value,"%x",&daq.trigger_inhibit_duration);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_inhibit_value");
              g_assert (widget);
              sprintf(content,"%x",daq.trigger_inhibit_duration);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"JESD_reset_delay"))
            {
              sscanf((const char*)child2_value,"%x",&daq.JESD_reset_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_JESD_reset_delay_value");
              g_assert (widget);
              sprintf(content,"%x",daq.JESD_reset_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"nevent"))
            {
              sscanf((const char*)child2_value,"%d",&daq.nevent);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_nevent");
              g_assert (widget);
              sprintf(content,"%d",daq.nevent);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"nframe"))
            {
              sscanf((const char*)child2_value,"%d",&daq.ndaq_frame);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_nframe");
              g_assert (widget);
              sprintf(content,"%d",daq.ndaq_frame);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"PLL_freq"))
            {
              sscanf((const char*)child2_value,"%d",&daq.PLL_freq);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_PLL_setting");
              g_assert (widget);
              if(daq.PLL_freq==1280)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"DAC1_value"))
            {
              sscanf((const char*)child2_value,"%d",&asic.DAC_value[0]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC1_value");
              g_assert (widget);
              sprintf(content,"%d",asic.DAC_value[0]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"DAC2_value"))
            {
              sscanf((const char*)child2_value,"%d",&asic.DAC_value[1]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC2_value");
              g_assert (widget);
              sprintf(content,"%d",asic.DAC_value[1]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"DAC3_value"))
            {
              sscanf((const char*)child2_value,"%d",&asic.DAC_value[2]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC3_value");
              g_assert (widget);
              sprintf(content,"%d",asic.DAC_value[2]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"twos_complement"))
            {
              asic.twos_complement[0]=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              asic.twos_complement[1]=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              asic.twos_complement[2]=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              printf("twos complement : %d %d %d\n",asic.twos_complement[0],asic.twos_complement[1],asic.twos_complement[2]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out0"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[0]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[0]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out1"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[1]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[1]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out2"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[2]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[2]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out3"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[3]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[3]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out4"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[4]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[4]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out5"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[5]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[5]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out6"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[6]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[6]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_out7"))
            {
              sscanf((const char*)child2_width,"%d",&daq.trigger_width[7]);
              sscanf((const char*)child2_delay,"%d",&daq.trigger_delay[7]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"trigger_map"))
            {
              sscanf((const char*)child2_value,"%d",&daq.trigger_map);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TAP1_value"))
            {
              sscanf((const char*)child2_value,"%d",&daq.trigger_tap_value[0]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_trigger_tap1_value");
              g_assert (widget);
              sprintf(content,"%d",daq.trigger_tap_value[0]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TAP2_value"))
            {
              sscanf((const char*)child2_value,"%d",&daq.trigger_tap_value[1]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_trigger_tap2_value");
              g_assert (widget);
              sprintf(content,"%d",daq.trigger_tap_value[1]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TAP3_value"))
            {
              sscanf((const char*)child2_value,"%d",&daq.trigger_tap_value[2]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_trigger_tap3_value");
              g_assert (widget);
              sprintf(content,"%d",daq.trigger_tap_value[2]);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"generate_BC0"))
            {
              daq.gen_BC0=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_gen_BC0");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.gen_BC0);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"generate_WTE"))
            {
              daq.gen_WTE=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_gen_WTE");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.gen_WTE);
            }
          }
          level_2 = level_2->next;
          if(!child2_name)xmlFree(child2_name);
          if(!child2_value)xmlFree(child2_value);
          if(!child2_width)xmlFree(child2_width);
          if(!child2_delay)xmlFree(child2_delay);
          if(!child2_status)xmlFree(child2_status);
          if(!child2_id)xmlFree(child2_id);
          if(!child2_level)xmlFree(child2_level);
        }
        printf("with debug flags : DAQ %d, I2C %d, GTK %d, draw %d\n",daq.debug_DAQ,daq.debug_I2C,daq.debug_GTK,daq.debug_draw);
      }
    }
    level_1 = level_1->next;
    if(!child1_name)xmlFree(child1_name);
    if(!child1_value)xmlFree(child1_value);
    if(!child1_level)xmlFree(child1_level);
    if(!child1_id)xmlFree(child1_id);
    if(!child1_status)xmlFree(child1_status);
  }
  xmlFreeDoc(doc);

  printf("Will use I2C_shift_dev_number %d\n",daq.I2C_shift_dev_number);
}

void write_conf_to_xml_file(char *filename)
{
  char status[80];

  FILE*fd_out=fopen(filename,"w+");
  fprintf(fd_out,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(fd_out,"<lmb>\n");
  fprintf(fd_out,"  <object name=\"DAQ\">\n");
  fprintf(fd_out,"    <property name=\"LMB\" value=\"%d\"></property>\n",daq.LMB_number);
  fprintf(fd_out,"    <property name=\"resync_phase\" value=\"%d\"></property>\n",daq.resync_phase);
  fprintf(fd_out,"    <property name=\"I2C_shift_dev_number\" value=\"%d\"></property>\n",daq.I2C_shift_dev_number);
  sprintf(status,"OFF");
  if(daq.debug_DAQ) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_DAQ\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_I2C) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_I2C\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_GTK) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_GTK\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_draw) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_draw\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.zoom_draw) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"zoom_draw\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.wait_for_ever) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"wait_for_ever\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.I2C_lpGBT_mode) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"I2C_lpGBT\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"HW_delay\" value=\"%d\"></property>\n",daq.hw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"SW_delay\" value=\"%d\"></property>\n",daq.sw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"trigger_inhibit\" value=\"%x\"></property>\n",daq.trigger_inhibit_duration);
  fprintf(fd_out,"    <property name=\"JESD_reset_delay\" value=\"%x\"></property>\n",daq.JESD_reset_delay);
  fprintf(fd_out,"    <property name=\"nevent\" value=\"%d\"></property>\n",daq.nevent);
  fprintf(fd_out,"    <property name=\"nframe\" value=\"%d\"></property>\n",daq.ndaq_frame);
  fprintf(fd_out,"    <property name=\"PLL_freq\" value=\"%d\"></property>\n",daq.PLL_freq);
  fprintf(fd_out,"    <property name=\"DAC1_value\" value=\"%d\"></property>\n",asic.DAC_value[0]);
  fprintf(fd_out,"    <property name=\"DAC2_value\" value=\"%d\"></property>\n",asic.DAC_value[1]);
  fprintf(fd_out,"    <property name=\"DAC3_value\" value=\"%d\"></property>\n",asic.DAC_value[2]);
  sprintf(status,"OFF");
  if(asic.twos_complement[0]) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"twos_complement\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"trigger_out0\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[0],daq.trigger_delay[0]);
  fprintf(fd_out,"    <property name=\"trigger_out1\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[1],daq.trigger_delay[1]);
  fprintf(fd_out,"    <property name=\"trigger_out2\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[2],daq.trigger_delay[2]);
  fprintf(fd_out,"    <property name=\"trigger_out3\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[3],daq.trigger_delay[3]);
  fprintf(fd_out,"    <property name=\"trigger_out4\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[4],daq.trigger_delay[4]);
  fprintf(fd_out,"    <property name=\"trigger_out5\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[5],daq.trigger_delay[5]);
  fprintf(fd_out,"    <property name=\"trigger_out6\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[6],daq.trigger_delay[6]);
  fprintf(fd_out,"    <property name=\"trigger_out7\" width=\"%d\" delay=\"%d\"></property>\n",daq.trigger_width[7],daq.trigger_delay[7]);
  fprintf(fd_out,"    <property name=\"trigger_map\" value=\"%d\"></property>\n",daq.trigger_map);
  fprintf(fd_out,"    <property name=\"TAP1_value\" value=\"%d\"></property>\n",daq.trigger_tap_value[0]);
  fprintf(fd_out,"    <property name=\"TAP2_value\" value=\"%d\"></property>\n",daq.trigger_tap_value[1]);
  fprintf(fd_out,"    <property name=\"TAP3_value\" value=\"%d\"></property>\n",daq.trigger_tap_value[2]);
  sprintf(status,"OFF");
  if(daq.gen_BC0) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"generate_BC0\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.gen_WTE) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"generate_WTE\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"  </object>\n");
  fprintf(fd_out,"</lmb>\n");
  fclose(fd_out);
}
