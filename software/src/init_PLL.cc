#define EXTERN extern
#include "gdaq_LMB.h"

void init_PLL()
{
  UInt_t device_number, val;
  UInt_t I2C_data, command;
  FILE *fcal;
  UInt_t iret=0;
  ValWord<uint32_t> reg;
  Int_t ADC_reg_val[2][76];
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char reg_name[80];
  UInt_t PLL_reg_numbers[37]= {0x88, 0xb9, 0x8b, 0x8a, 0x87, 0x86, 0x84,
                               0x83, 0x82, 0x81, 0x80, 0x30, 0x2f, 0x2e, 0x2d, 0x2c, 0x2b,
                               0x2a, 0x29, 0x28, 0x21, 0x20, 0x1f, 0x19, 0x18, 0x17, 0x16,
                               0x14, 0x13, 0x0b, 0x0a, 0x08, 0x06, 0x05, 0x03, 0x02, 0x00};
  UInt_t PLL_reg_contents[37]={0x40, 0x13, 0xff, 0x0f, 0xa2, 0x01, 0x02,
                               0x1f, 0x01, 0x06, 0x20, 0x4f, 0x00, 0x00, 0x4f, 0x00, 0x00,
//                             0xff, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x3f, 0x1f, 0xdf, // 1280 MHz
                               0xff, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x3f, 0x1f, 0xdf, // 640 MHz
                               0x3e, 0x2c, 0x40, 0x00, 0x00, 0x2d, 0xed, 0x15, 0xA2, 0x34};

  uhal::HwInterface hw=devices.front();
  if(daq.PLL_freq==1280)
  {
    printf("prepare PLL for 1280 Mhz running\n");
    PLL_reg_contents[20]=0x00; //Reg 33 content
    PLL_reg_contents[19]=0x20; //Reg 40 content
    PLL_reg_contents[18]=0x01; //Reg 41 content
    PLL_reg_contents[17]=0xff; //Reg 42 content
  }
  else if(daq.PLL_freq==640)
  {
    printf("prepare PLL for 640 Mhz running\n");
    PLL_reg_contents[20]=0x01; //Reg 33 content
    PLL_reg_contents[19]=0x20; //Reg 40 content
    PLL_reg_contents[18]=0x01; //Reg 41 content
    PLL_reg_contents[17]=0xff; //Reg 42 content
  }
  else if(daq.PLL_freq==160)
  {
    printf("prepare PLL for 160 Mhz running\n");
    PLL_reg_contents[20]=0x07; //Reg 33 content
    PLL_reg_contents[19]=0x00; //Reg 40 content
    PLL_reg_contents[18]=0x02; //Reg 41 content
    PLL_reg_contents[17]=0x7f; //Reg 42 content
  }

// Reset PLL :
  command=PLL_RESET*1;
  hw.getNode("ACTIONS").write(command);

  command=(1<<31) | (0<<24) | (136<<16) | 0x80;
  hw.getNode("PLL_CTRL").write(command);
  hw.dispatch();
  usleep(10000);

  for(Int_t i=36; i>=0; i--)
  {
// PLL I2C address=0x68, translated in firmaware
// send only the chip number (0) since the I2C bus is used also for DACs setting
    command=(1<<31) | (0<<24) | (PLL_reg_numbers[i]<<16) | PLL_reg_contents[i];
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
    reg=hw.getNode("PLL_CTRL").read();
    hw.dispatch();
    printf("I2C read back : reg %d, was 0x%x,",PLL_reg_numbers[i],reg.value()&0xffff);
    command=(0<<24) | (PLL_reg_numbers[i]<<16) | PLL_reg_contents[i];
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
    command=(1<<31) | (0<<24) | (PLL_reg_numbers[i]<<16) | PLL_reg_contents[i];
    hw.getNode("PLL_CTRL").write(command);
    hw.dispatch();
    reg=hw.getNode("PLL_CTRL").read();
    hw.dispatch();
    printf(" now 0x%x should be 0x%x\n",reg.value(),PLL_reg_contents[i]&0xffff);
  }
}
