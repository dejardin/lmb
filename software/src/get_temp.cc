#define EXTERN extern
#include "gdaq_LMB.h"

void get_temp(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, command, iret;
  GtkWidget* widget;
  char widget_text[80], widget_name[80];;

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();

// Read FPGA temperature :
  Int_t XADC_Temp_FPGA=0;
  Int_t average=32;
  Double_t fpga_temp=0.;
  ValWord<uint32_t> temp;
  Double_t ave_val=0.;
  for(int iave=0; iave<average; iave++)
  {
    command=DRP_WRb*0 | (XADC_Temp_FPGA<<16);
    hw.getNode("DRP_XADC").write(command);
    temp  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    double loc_val=double((temp.value()&0xffff)>>4)/4096.;
    ave_val+=loc_val;
  }
  fpga_temp=(ave_val/average)*4096*0.123-273.;
  printf("FPGA temperature : %.2f deg\n", fpga_temp);
  sprintf(widget_name,"2_FPGA_temp");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"FPGA temp:\n%.1f <sup>o</sup>C",fpga_temp);
  gtk_label_set_markup((GtkLabel*)widget,widget_text);
  //command=0x000000f6;
  //hw.getNode("TH_SENSOR").write(command);
  //usleep(100000);
  ValWord<uint32_t> sensor  = hw.getNode("TH_SENSOR").read();
  hw.dispatch();
  Double_t local_temp=-45.+175.*(sensor.value()&0xffff)/65535.;
  Double_t local_humidity=-6.+125.*(sensor.value()>>16)/65535.;
  printf("Temp/Humidity sensor : %d -> %.2f oC, %d -> %.2f %%\n",sensor.value()&0xffff, local_temp,
                                                                (sensor.value()>>16),   local_humidity);
  sprintf(widget_name,"2_local_temp");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"Local temp:\n%.1f <sup>o</sup>C",local_temp);
  gtk_label_set_markup((GtkLabel*)widget,widget_text);
  sprintf(widget_name,"2_local_humidity");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"Local humidity:\n%.1f %%",local_humidity);
  gtk_label_set_markup((GtkLabel*)widget,widget_text);

}
