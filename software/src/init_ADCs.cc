#define EXTERN extern
#include "gdaq_LMB.h"

void init_ADCs(Int_t init)
{
  UInt_t device_number, val;
  UInt_t command, reg_loc;
  ValWord<uint32_t> reg;
  Int_t ADC_reg_val[2][76];
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char reg_name[80];

  uhal::HwInterface hw=devices.front();

  if(init==0) // hard reset
  {
    printf("Reset ADC\n");
    command = ADC_RESET*1;
    hw.getNode("ACTIONS").write(command);
    hw.dispatch();
  }
  else if(init==1) // soft reset
  {
    printf("Start ADC initialisation\n");
// Reset ADCs
/*
    for(Int_t iADC=1; iADC<=N_ADC; iADC++)
    {
      command = (iADC<<24) | (0x0000<<8) | (0x81);
      hw.getNode("ADC_CTRL").write(command);
      hw.dispatch();
    }
    usleep(5000);
 */
    for(Int_t iADC=1; iADC<=N_ADC; iADC++)
    {
/*
// Set Vref pin for DC coupling :
      command = (iADC<<24) | (0x1908<<8) | (0x04);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18a6<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e6<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1910<<8) | (0x0d); // 1.70V full scale
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e0<<8) | (0x02);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e1<<8) | (0x14);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e2<<8) | (0x14);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e3<<8) | (0x40);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x18e3<<8) | (0x5f);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1a4c<<8) | (0x12); // 360 uA
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1a4d<<8) | (0x12); // 360 uA
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1b03<<8) | (0x02);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1b08<<8) | (0xc1);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x1b10<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x0561<<8) | (0x00); // Offset binary coding
      hw.getNode("ADC_CTRL").write(command);
// Power down the links
*/
      command = (iADC<<24) | (0x0002<<8) | (0x03);
      hw.getNode("ADC_CTRL").write(command);
/*
      command = (iADC<<24) | (0x0108<<8) | (0x00); // Input clock divider = 1
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x056e<<8) | (0x10); // PLL for 1280 MS/s
      hw.getNode("ADC_CTRL").write(command);
// JESD204B link config
      command = (iADC<<24) | (0x0200<<8) | (0x00); // ADC in full bandwidth
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x0201<<8) | (0x00); // Chip decimation=1, no DDC
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x058b<<8) | (daq.ADC_scrambling<<7) | (0x03); // L=4 : 4 lanes active, scrambling upon request
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x058c<<8) | (0x00); // F=1 : 1 byte per frame
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x058d<<8) | (0x1f); // K=32 : 32 frames per multiframe
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x058e<<8) | (0x00); // M=1 : 1 converter
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x058f<<8) | ((2<<6)|0x0d); // N=14 : 14-bit resolution, CS=2
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x0590<<8) | (0x2f); // N'=16, subcass=1 
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x0559<<8) | 0x00; // CS0=0, CS1=0
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x055a<<8) | 0x05; // CS2=SYSREF
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x0120<<8) | 0x02; // SYSREF = N shot mode
      hw.getNode("ADC_CTRL").write(command);
      command = (iADC<<24) | (0x01ff<<8) | 0x01; // SYSREF used as timestamp
      hw.getNode("ADC_CTRL").write(command);
      //command = (iADC<<24) | (0x0571<<8) | 0x16; // Disable character replacement for debug
      //hw.getNode("ADC_CTRL").write(command);
      if(iADC==3)
      {
        command = (iADC<<24) | (0x05C0<<8) | 0x11; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C1<<8) | 0x11; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C2<<8) | 0x11; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C3<<8) | 0x11; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C4<<8) | 0x82; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C6<<8) | 0x82; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05C8<<8) | 0x82; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
        command = (iADC<<24) | (0x05CA<<8) | 0x82; // Voltage swing
        hw.getNode("ADC_CTRL").write(command);
      }
*/
// power up the link
      command = (iADC<<24) | (0x0002<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
// Reset JESD204B start-up circuit
//      command = (iADC<<24) | (0x1228<<8) | (0x4f);
//      hw.getNode("ADC_CTRL").write(command);
// JESD204B start-up circuit in normal operation
//      command = (iADC<<24) | (0x1228<<8) | (0x0f);
//      hw.getNode("ADC_CTRL").write(command);
// JESD204B PLL force normal operation
//      command = (iADC<<24) | (0x1222<<8) | (0x00);
//      hw.getNode("ADC_CTRL").write(command);
// Reset JESD204B PLL calib
//      command = (iADC<<24) | (0x1222<<8) | (0x04);
//      hw.getNode("ADC_CTRL").write(command);
// JESD204B PLL force normal operation
//      command = (iADC<<24) | (0x1222<<8) | (0x00);
//      hw.getNode("ADC_CTRL").write(command);
// Clear LoL bit
 //     command = (iADC<<24) | (0x1262<<8) | (0x08);
 //     hw.getNode("ADC_CTRL").write(command);
// LoL bit in normal operation
//      command = (iADC<<24) | (0x1262<<8) | (0x00);
//      hw.getNode("ADC_CTRL").write(command);
      hw.dispatch();
    }
    usleep(50000);
    for(Int_t iADC=1; iADC<=N_ADC; iADC++)
    {
      command = (1<<31) | (iADC<<24) | (0x056f<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x056f) : PLL lock status=%d, LoL=%d\n",iADC,reg_loc>>7,reg_loc&0x08);
// Reset JESD204 startup circuit
      command = (iADC<<24) | (0x1228<<8) | (0x4f);
      hw.getNode("ADC_CTRL").write(command);
// JESD204 startup circuit in normal operation
      command = (iADC<<24) | (0x1228<<8) | (0x0f);
      hw.getNode("ADC_CTRL").write(command);

      update_ADC_patterns(iADC,-1);
      update_ADC_patterns(iADC,0);
      update_ADC_patterns(iADC,1);
      update_ADC_patterns(iADC,2);
      update_ADC_patterns(iADC,3);
    }
// Read back status bits :
    for(Int_t iADC=1; iADC<=N_ADC; iADC++)
    {
      command = (1<<31) | (iADC<<24) | (0x0108<<8) | (0x00); // Input clock divider = 1
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0108) : Input clk divider=%d\n",iADC,reg_loc+1);
      command = (1<<31) | (iADC<<24) | (0x0200<<8) | (0x00); // ADC in full bandwidth
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0200) : Bandwidth=0x%x\n",iADC,reg_loc);
      command = (1<<31) | (iADC<<24) | (0x0201<<8) | (0x00); // Chip decimation=1, no DDC
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0201) : decimation=%d\n",iADC,reg_loc);
      command = (1<<31) | (iADC<<24) | (0x056e<<8) | (0x10); // PLL for 1280 MS/s
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x056e) : PLL setting=%d\n",iADC,reg_loc>>4);
      command = (1<<31) | (iADC<<24) | (0x056f<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x056f) : PLL lock status=%d, LoL=%d\n",iADC,reg_loc>>7,reg_loc&0x08);
      command = (1<<31) | (iADC<<24) | (0x058b<<8) | (0x03); // L=4 : 4 lanes active
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x058b) : L=%d, scrambling %d\n",iADC,(reg_loc&0x1f)+1, reg_loc>>7);
      command = (1<<31) | (iADC<<24) | (0x058c<<8) | (0x00); // F=1 : 1 byte per frame
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x058c) : F=%d\n",iADC,reg_loc+1);
      command = (1<<31) | (iADC<<24) | (0x058d<<8) | (0x1f); // K=32 : 32 frames per multiframe
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x058d) : K=%d\n",iADC,reg_loc+1);
      command = (1<<31) | (iADC<<24) | (0x058e<<8) | (0x00); // M=1 : 1 converter
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x058e) : M=%d\n",iADC,reg_loc+1);
      command = (1<<31) | (iADC<<24) | (0x058f<<8) | (0x8d); // N=14 : 14-bit resolution, CS=2
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x058f) : N=%d, CS=%d\n",iADC,(reg_loc&0x1f)+1, reg_loc>>6);
      command = (1<<31) | (iADC<<24) | (0x0559<<8) | (0x00); // CS0=0, CS1=0
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0559) : CS0=%d, CS1=%d\n",iADC,reg_loc&0xf, (reg_loc>>4)&0x0f);
      command = (1<<31) | (iADC<<24) | (0x055a<<8) | (0x03); // CS2=SYSREF
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x055a) : CS2=%d\n",iADC,reg_loc&0x1f);
      //command = (1<<31) | (iADC<<24) | (0x0590<<8) | (0x2f); // N'=16, subcass=1 
      command = (1<<31) | (iADC<<24) | (0x0590<<8) | (0x0f); // N'=16, subcass=0 
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0590) : N'=%d, subclass %d\n",iADC,(reg_loc&0x1f)+1, reg_loc>>5);
      command = (1<<31) | (iADC<<24) | (0x0591<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0591) : S=%d\n",iADC,(reg_loc&0x1f)+1);
      command = (1<<31) | (iADC<<24) | (0x0592<<8) | (0x00);
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0592) : CF=%d, HD=%d\n",iADC,reg_loc&0x1f,reg_loc>>7);
// Two's complement ?
      if(asic.twos_complement[iADC-1])
      {
        command = (iADC<<24) | (0x561<<8) | (0x01);
        hw.getNode("ADC_CTRL").write(command);
        hw.dispatch();
      }
      command = (1<<31) || (iADC<<24) | (0x561<<8) | (0x01);
      hw.getNode("ADC_CTRL").write(command);
      reg=hw.getNode("ADC_CTRL").read();
      hw.dispatch();
      reg_loc=reg.value()&0xff;
      printf("ADC%d(0x0561) : Two's complement=%d\n",iADC,reg_loc);
    }
  }
  else
  {
    if(asic.ADC_SPI_RWb==0)
    {
      printf("Load register %4.4x of ADC %d with %2.2x\n",asic.ADC_register_number, asic.ADC_number, asic.ADC_register_value);
      command = (asic.ADC_number<<24) | (asic.ADC_register_number<<8) | (asic.ADC_register_value);
      printf("Send command 0x%8.8x\n",command);
      hw.getNode("ADC_CTRL").write(command);
      hw.dispatch();
      if(asic.ADC_register_number==0x561)
      {
        if(asic.ADC_register_value&1==1) asic.twos_complement[asic.ADC_number]=TRUE;
        else                             asic.twos_complement[asic.ADC_number]=FALSE;
      }
    }
    command = (1<<31) | (asic.ADC_number<<24) | (asic.ADC_register_number<<8) | (asic.ADC_register_value);
    printf("Send command 0x%8.8x\n",command);
    hw.getNode("ADC_CTRL").write(command);
    reg=hw.getNode("ADC_CTRL").read();
    hw.dispatch();
    printf("ADC read back value : 0x%8.8x\n",reg.value());
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_reg_value_read");
    g_assert (widget);
    sprintf(widget_text,"0x%2.2x",reg.value()&0xFF);
    gtk_label_set_text((GtkLabel*)widget,widget_text);
  }
}
