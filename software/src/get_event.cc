#define EXTERN extern
#include "gdaq_LMB.h"

void take_run(void)
{
  uhal::HwInterface hw=devices.front();
  ValWord<uint32_t> orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  Double_t dv=1700./16384.;
  Double_t dt=6.25/8.;
  bool debug_DAQ=daq.debug_DAQ;
  daq.debug_DAQ=FALSE;
  char hname[132], widget_name[132], widget_text[132];
  GtkWidget *widget;
  time_t rawtime=time(NULL);
  struct tm *timeinfo=localtime(&rawtime);
  if(daq.trigger_type==0)
    sprintf(daq.last_fname,"data/ped_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==2)
    sprintf(daq.last_fname,"data/laser_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==3)
    sprintf(daq.last_fname,"data/cal_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  
  printf("Start run recording for %d events with %d frames and trigger type %d\n",daq.nevent, daq.ndaq_frame, daq.trigger_type);
  printf("Run file name : %s\n",daq.last_fname);

  if(daq.fd != NULL)
  {
    daq.fd->Close();
    daq.fd->~TFile();
  }
  daq.fd=new TFile(daq.last_fname,"recreate");

  if(daq.tdata==NULL)
  {
    daq.tdata=new TTree("tdata","tdata");
    daq.tdata->Branch("timestamp",&daq.timestamp,"timestamp/l");
    for(int ich=0; ich<N_ADC; ich++)
    {
      char bname[80], btype[80];
      sprintf(bname,"ch%d",ich);
      sprintf(btype,"ch%d[%d]/S",ich,daq.ndaq_frame*8);
      daq.tdata->Branch(bname,daq.event[ich],btype);
    }
  }

  for(Int_t ich=0; ich<N_ADC; ich++)
  {
    if(daq.hmean[ich]!=NULL)daq.hmean[ich]->~TH1D();
    if(daq.hrms[ich]!=NULL)daq.hrms[ich]->~TH1D();
    if(daq.hdensity[ich]!=NULL)daq.hdensity[ich]->~TH1D();
    sprintf(hname,"mean_ch%d",ich);
    daq.hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    daq.hrms[ich]=new TH1D(hname,hname,200,0.,2.);
  }

  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  for(Int_t ich=0; ich<N_ADC; ich++)
  {
    sprintf(hname,"ch_%d_shape",ich);
    daq.pshape[ich]=new TProfile(hname,hname,daq.ndaq_frame*8,0.,dt*daq.ndaq_frame*8);
  }

  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  daq.cur_evt=0;
  Int_t draw=1;
  while(daq.cur_evt<daq.nevent)
  {
    sprintf(widget_name,"3_nevent");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%d",daq.cur_evt);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
// Have we asked to abort the run ?
    if(!daq.daq_running)
    {
      printf("Abort button pressed 1 !\n");
      break;
    }
    get_event(0,draw);
    daq.tdata->Fill();
    for(Int_t ich=0; ich<N_ADC; ich++)
    {
      Double_t mean=0., rms=0.; 
      for(Int_t is=0; is<daq.nsample; is++)
      {
        mean+=daq.fevent[ich][is];
        rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
      }
      mean/=daq.nsample;
      rms/=daq.nsample;
      rms=sqrt(rms-mean*mean);
      if(daq.trigger_type!=1)
      {
        //printf("ch %d, rms = %.3f\n",ich,rms);
        for(Int_t is=0; is<daq.nsample; is++)
        {
          daq.pshape[ich]->Fill(dt*is,daq.fevent[ich][is]);
        }
        daq.hmean[ich]->Fill(mean*dv);
        daq.hrms[ich]->Fill(rms*dv);
        if(draw==1 && asic.ADC_status[ich])printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
      }
    }
    if(draw==1)printf("\n");
    draw=0;
    if(!daq.daq_running)
    {
      printf("Abort button pressed 2 !\n");
      break;
    }
  }

  if(daq.daq_running)
  {
    daq.c1->Write();
    daq.tdata->Write();
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<N_ADC; ich++)
    {
      daq.pshape[ich]->Write();
      daq.pshape[ich]->~TProfile();
      daq.hmean[ich]->Write();
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->Write();
      daq.hrms[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }
  else
  {
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<N_ADC; ich++)
    {
      daq.pshape[ich]->~TProfile();
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }

  daq.debug_DAQ=debug_DAQ;
  sprintf(widget_name,"3_nevent");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.nevent);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"4_take_run");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(daq.daq_running)gtk_button_clicked((GtkButton*)widget);
  printf("End of run !\n");
}

void get_single_event(void)
{
  Double_t dv=1700./16384.;

  daq.cur_evt=0;
  get_event(1,1);
  for(Int_t ich=0; ich<N_ADC; ich++)
  {
    Double_t mean=0., rms=0.; 
    for(Int_t is=0; is<daq.nsample; is++)
    {
      mean+=daq.fevent[ich][is];
      rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
    }
    mean/=daq.nsample;
    rms/=daq.nsample;
    rms=sqrt(rms-mean*mean);
    if(asic.ADC_status[ich])printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
  }
  printf("\n");
}

void get_event(Int_t debug_mode, Int_t draw)
{
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=1852;
  int n_transfer;
  int n_last;
  int BC0_pos[N_ADC]={0};
  daq.error      = 0;
  int command    = 0;
  Int_t loc_ch[N_ADC]={1, 2, 3};
  uhal::HwInterface hw=devices.front();
  Double_t dt=6.25/8.; // 1280 MS/s ADCs
  daq.nsample=daq.ndaq_frame*8;

// With LMB, we have single event capture/read (no FIFO mode). Reset read address at each event. (FIXME)
  daq.old_read_address=-1;
  hw.getNode("CAP_ADDRESS").write(-1);
// In debug mode, we reinit DAQ buffer at each event :
  if(debug_mode!=0 || daq.cur_evt==0)
  {
    printf("Take event with %d daq frames (%d samples per ADC)\n",daq.ndaq_frame, daq.nsample);
    command = ((daq.ndaq_frame+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
    command = ((daq.ndaq_frame+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
  }
  hw.dispatch();

  ValWord<uint32_t> ctrl,address,free_mem,orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  ctrl = hw.getNode("CAP_CTRL").read();
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(daq.debug_DAQ)printf("Starting with address : %8.8x, free memory %8.8x, trigger_type : %d with frame length : %d\n",
                           address.value(),free_mem.value(),daq.trigger_type,(ctrl.value()>>16));

  ValVector< uint32_t > block;

// For pedestal trigger, send a soft trigger to LMB
  if(daq.trigger_type !=2 )
  {
    if(daq.trigger_type == 0)
      command = GENE_TRIGGER*1;
    else if(daq.trigger_type == 1)
      command = GENE_TRIGGER*1;
    else if(daq.trigger_type == 5)
      command = GENE_TRIGGER*1;

    if(daq.debug_DAQ)printf("Send trigger with command : 0x%8.8x ",command);
// Read base address and send trigger
    hw.getNode("ACTIONS").write(command);
    hw.dispatch();
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-daq.old_read_address;
      if(delta_address<0)delta_address+=N_DAQ_FRAME_MAX;
      if(daq.debug_DAQ) printf("ongoing R/W addresses    : old %d, new %d delta %d\n", daq.old_read_address, new_write_address,delta_address);
    }
    while(delta_address < daq.ndaq_frame+1 && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", daq.old_read_address, new_write_address, address.value());
      daq.error=1;
    }
  }
  else
  {
    int nretry=0;
    //command = GENE_AWG*1+LED_ON*1+GENE_WTE*0+GENE_TRIGGER*0;
    command = GENE_TRIGGER*0;
    hw.getNode("ACTIONS").write(command);
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(daq.debug_DAQ && !daq.wait_for_ever)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      if(daq.wait_for_ever)
      {
        usleep(1000);
        printf(".");
        fflush(stdout);
        while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
      }
      nretry++;
    }
    while((free_mem.value()==N_DAQ_FRAME_MAX-1) && (nretry<100 || daq.wait_for_ever));
    //if(daq.wait_for_ever)printf("\n");
    if(nretry>=100 && !daq.wait_for_ever)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      daq.error=1;
    }
  }

  if(daq.debug_DAQ)printf("Event kept with address : %8.8x, free memory %d, trigger_type : %d\n",address.value(),free_mem.value(),daq.trigger_type);
// Keep reading address for next event
  daq.old_read_address=address.value()>>16;
  if(daq.old_read_address==N_DAQ_FRAME_MAX-1)daq.old_read_address=-1;

  daq.mem.clear();

// Read event samples from FPGA
  n_word=(daq.ndaq_frame+1)*(N_ADC*4+1); // nsample*(address+N_ADC*4) 32 bits words per event to get the N_ADC channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(daq.debug_DAQ)printf("Will read %d words in %d transfer of %d + %d in last transfer\n",n_word,n_transfer,MAX_PAYLOAD/4,n_last);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of frame per event.\n");
    printf("Max event size : %d frames\n",N_DAQ_FRAME_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    daq.error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)
    {
      daq.mem.push_back(block[is]);
    }
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(daq.debug_DAQ)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)
  {
    daq.mem.push_back(block[is]);
  }
  daq.mem.valid(true);

// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  //if(daq.mem[3]!=0xffffffff || daq.mem[4]!=0xffffffff || daq.mem[5]!=0xffffffff)
  //{
  //  printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[5], daq.mem[4], daq.mem[3], daq.mem[2], daq.mem[1], daq.mem[0]);
  //  daq.error=1;
  //}
  daq.timestamp=daq.mem[2];
  daq.timestamp=(daq.timestamp<<32)+daq.mem[1];
  
  if(daq.debug_DAQ)
  {
    printf("timestamp : %8.8x %8.8x %ld\n",daq.mem[2],daq.mem[1],daq.timestamp);
    Int_t i0=0;
    printf("Headers :\n");
    printf("addr : %8.8x\n",daq.mem[i0+0]);
    i0+=1;
    printf("  data : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=4;
    printf("  data : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=4;
    printf("  data : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=4;
    printf("First sample :\n");
    printf("addr : %8.8x\n",daq.mem[i0+0]);
    i0+=1;
    printf("  ADC1 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC2 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC3 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("Second sample :\n");
    printf("addr : %8.8x\n",daq.mem[i0+0]);
    i0+=1;
    printf("  ADC1 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC2 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC3 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("Third sample :\n");
    printf("addr : %8.8x\n",daq.mem[i0+0]);
    i0+=1;
    printf("  ADC1 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC2 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
    printf("  ADC3 : %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3]);
    i0+=4;
  }

  FILE *fd=NULL;
  if(daq.dump_data && daq.cur_evt==0) fd=fopen("data/dump.txt","w+");
  if(daq.dump_data && daq.cur_evt>0) fclose(fd);

  for(int iframe=1; iframe<=daq.ndaq_frame; iframe++)
  {
    for(Int_t ich=0; ich<N_ADC; ich++)
    {
      Int_t j=iframe*(4*N_ADC+1)+ich*4;
      for(int iw=0; iw<4; iw++)
      {
        Int_t is=(iframe-1)*8+2*iw;
        daq.event[ich][is]   =(daq.mem[j+1+iw]&0x0000ffff)>>2;  // 2 samples per word in the DAQ frame
        daq.event[ich][is+1] =(daq.mem[j+1+iw]&0xffff0000)>>18; // The 2 lsb of each sample are Control bits
        if(asic.twos_complement[ich])
        {
          //if((daq.event[ich][is]>>13)&1==1)  daq.event[ich][is]  =8192-daq.event[ich][is];
          //if((daq.event[ich][is+1]>>13)&1==1)daq.event[ich][is+1]=8192-daq.event[ich][is+1];
          if(((daq.event[ich][is]>>13)&1)==1)  daq.event[ich][is]  |=0xC000;
          if(((daq.event[ich][is+1]>>13)&1)==1)daq.event[ich][is+1]|=0xC000;
        }
        daq.fevent[ich][is]  =(double)daq.event[ich][is];
        daq.fevent[ich][is+1]=(double)daq.event[ich][is+1];
      }
      if(daq.dump_data && iframe<100)
      {
        fprintf(fd,"%8.8x %8.8x %8.8x %8.8x\n",daq.mem[j+1],daq.mem[j+2],daq.mem[j+3],daq.mem[j+4]);
      }
    }
  }

  if((daq.trigger_type !=trigger_type_old || draw==1) && daq.debug_draw )
  {
    for(int ich=0; ich<N_ADC; ich++)
    {
      daq.tg[ich]->Set(0);
      Double_t ymax=-999999., ymin=999999., ymean=0.;
      for(int isample=0; isample<daq.nsample; isample++)
      {
        if(daq.fevent[ich][isample]>ymax)ymax=daq.fevent[ich][isample];
        if(daq.fevent[ich][isample]<ymin)ymin=daq.fevent[ich][isample];
        daq.tg[ich]->SetPoint(isample,dt*isample,daq.fevent[ich][isample]);
        if(isample<20)ymean+=daq.fevent[ich][isample];
      }
      ymean/=20.;
      daq.tg[ich]->SetMaximum(100.*(floor(ymax/100.)+1));
      daq.tg[ich]->SetMinimum(100.*(floor(ymin/100.)));
      if(daq.zoom_draw==1)
      {
        daq.tg[ich]->SetMinimum(10.*(floor(ymean/10.)-2));
        daq.tg[ich]->SetMaximum(10.*(floor(ymean/10.)+2));
      }
    }
    for(int ich=0; ich<N_ADC; ich++)
    {
      daq.c1->cd(loc_ch[ich]);
      //daq.tg[ich]->SetMaximum(100.);
      //daq.tg[ich]->SetMinimum(0.);
      Int_t first_draw=1;
      if(daq.tg[ich]->GetN()>0)
      {
        daq.tg[ich]->Draw("alp");
        gPad->SetGridx();
        gPad->SetGridy();
        daq.tg[ich]->GetXaxis()->SetTitle("time [ns]");
        daq.tg[ich]->GetXaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetXaxis()->SetTitleFont(62);
        daq.tg[ich]->GetXaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetXaxis()->SetLabelFont(62);
        daq.tg[ich]->GetYaxis()->SetTitle("amplitude [lsb]");
        daq.tg[ich]->GetYaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetYaxis()->SetTitleOffset(0.9);
        daq.tg[ich]->GetYaxis()->SetTitleFont(62);
        daq.tg[ich]->GetYaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetYaxis()->SetLabelFont(62);
        if(daq.fd!=NULL)daq.tg[ich]->Write();
        first_draw=0;
      }
      daq.c1->Update();
    }
  }

  trigger_type_old=daq.trigger_type;
  daq.cur_evt++;
  return;
}
