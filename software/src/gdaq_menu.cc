#define EXTERN extern
#include <gtk/gtk.h>
#include <TSystem.h>
#include "gdaq_LMB.h"

void callback_about (GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;

  dialog =  gtk_about_dialog_new ();

  /* Pour l'exemple on va rendre la fenêtre a propos" modale par rapport a la fenêtre principale. */
  gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(gtk_builder_get_object (gtk_data.builder, "MainWindow")));

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

void delete_event(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal delete !\n");
  end_of_run();
}

void my_exit(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal destroy !\n");
  end_of_run();
}

void button_clicked(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  GtkWidget *image=NULL;
  GtkWidget *event_box=NULL;
  char widget_name[80];
  char reg_name[80];
  char content[80];
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  if(daq.debug_GTK)printf("Button %s clicked !\n",name);

  if(strcmp(name,"1_DTU_force_G10")==0)
  {
    //asic.DTU_force_G10[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
  }
  else if(strcmp(name,"1_DTU_force_G1")==0)
  {
    //asic.DTU_force_G1[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
  }
  else if(strcmp(name,"2_DAQ_timeout")==0)
  {
    daq.wait_for_ever=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK)printf("New wait_for_ever : %d\n",daq.wait_for_ever);
  }
  else if(strcmp(name,"2_debug_DAQ")==0)
  {
    daq.debug_DAQ=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK)printf("New debug_DAQ : %d\n",daq.debug_DAQ);
  }
  else if(strcmp(name,"2_debug_I2C")==0)
  {
    daq.debug_I2C=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK)printf("New debug_I2C : %d\n",daq.debug_I2C);
  }
  else if(strcmp(name,"2_debug_GTK")==0)
  {
    daq.debug_GTK=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK)printf("New debug_GTK : %d\n",daq.debug_GTK);
  }
  else if(strcmp(name,"2_debug_draw")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.debug_draw=TRUE;
      if(daq.debug_GTK) printf("Set debug_draw to 1 : %d\n",daq.debug_draw);
      if(daq.c1==NULL)
      {
        daq.c1=new TCanvas("c1","c1",1120,0,800,800);
        daq.c1->Divide(1,3);
        daq.c1->Update();
      }
    }
    else
    {
      daq.debug_draw=FALSE;
      if(daq.debug_GTK)
      {
        printf("destroy draw window !\n");
        printf("Set debug_draw to 0 : %d\n",daq.debug_draw);
      }
      if(daq.c1!=NULL)
      {
        //delete daq.c1;
        daq.c1->~TCanvas();
        gSystem->ProcessEvents();
        daq.c1=NULL;
      }
    }
    if(daq.debug_GTK) printf("debug drawings: %d\n",daq.debug_draw);
  }
  else if(strcmp(name,"2_zoom")==0)
  {
    daq.zoom_draw=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK) printf("New Zoom drawings: %d\n",daq.zoom_draw);
  }
  else if(strcmp(name,"2_I2C_protocol")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.I2C_lpGBT_mode=1;
      gtk_button_set_label((GtkButton*)button,"   I2C  lpGBT  ");
    }
    else
    {
      daq.I2C_lpGBT_mode=0;
      gtk_button_set_label((GtkButton*)button,"   I2C standard");
    }
    if(daq.debug_GTK) printf("I2C set to standard: %d\n",daq.I2C_lpGBT_mode);
    update_VFE_control(0);
  }
  else if(strcmp(name,"0_ped_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_ped_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for pedestal events\n");
      daq.trigger_type=0;
      daq.self_trigger=0;
      update_DAQ_settings();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(name,"0_laser_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_laser_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for external pulses\n");
      daq.trigger_type=2;
      daq.self_trigger=1;
      update_DAQ_settings();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate self_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    }
  }
  else if(strcmp(name,"3_trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger=1;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        self");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
    else
    {
      daq.self_trigger=0;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        soft");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    update_DAQ_settings();
  }
  else if(strcmp(name,"3_self_trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger_mode=1;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n         delta");
    }
    else
    {
      daq.self_trigger_mode=0;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n       absolute");
    }
    update_DAQ_settings();
  }
  else if(strcmp(&name[2],"init_LMB")==0)
  {
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      printf("Reset LMB\n");
      hw.getNode("ACTIONS").write(LMB_RESET*1);
      hw.dispatch();
      usleep(1000000);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC1_value");
      g_assert (widget);
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC2_value");
      g_assert (widget);
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC3_value");
      g_assert (widget);
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_init_FIFO");
      g_assert (widget);
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_RW");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_reg_number");
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,"561");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_number");
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,"1");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_reg_value");
      g_assert (widget);
      if(asic.twos_complement[0])
        gtk_entry_set_text((GtkEntry*)widget,"1");
      else
        gtk_entry_set_text((GtkEntry*)widget,"0");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_load_SPI");
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_number");
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,"2");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_reg_value");
      g_assert (widget);
      if(asic.twos_complement[1])
        gtk_entry_set_text((GtkEntry*)widget,"1");
      else
        gtk_entry_set_text((GtkEntry*)widget,"0");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_load_SPI");
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_number");
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,"3");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_SPI_reg_value");
      g_assert (widget);
      if(asic.twos_complement[2])
        gtk_entry_set_text((GtkEntry*)widget,"1");
      else
        gtk_entry_set_text((GtkEntry*)widget,"0");
      gtk_widget_activate(widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_load_SPI");
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);
    }
  }
  else if(strcmp(&name[2],"init_PLL")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_PLL();
    }
  }
  else if(strcmp(&name[2],"PLL_setting")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.PLL_freq=1280;
      gtk_button_set_label((GtkButton*)button,"     PLL\n1280  MHz");
    }
    else
    {
      daq.PLL_freq=640;
      gtk_button_set_label((GtkButton*)button,"    PLL\n640 MHz");
    }
  }
  else if(strcmp(&name[2],"init_ADCs")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_ADCs(1);
    }
  }
  else if(strcmp(&name[2],"init_FIFO")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_FIFO(1);
    }
  }
  else if(strcmp(&name[2],"get_cur_pos")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_FIFO(0);
    }
  }
  else if(strcmp(&name[2],"follow_sequence")==0)
  {
    if(daq.daq_initialized>=0)
    {
      if(daq.follow_FIFO==0)
      {
        gtk_button_set_label((GtkButton*)button,"         abort          ");
        daq.follow_FIFO=TRUE;
        init_FIFO(2);
      }
      else
      {
        daq.follow_FIFO=FALSE;
        gtk_button_set_label((GtkButton*)button,"Follow sequence");
      }
    }
  }
  else if(strcmp(&name[2],"PLL_I2C_RW")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      asic.PLL_I2C_RWb=0;
      gtk_button_set_label((GtkButton*)button,"W");
    }
    else
    {
      asic.PLL_I2C_RWb=1;
      gtk_button_set_label((GtkButton*)button,"R");
    }
  }
  else if(strcmp(&name[2],"load_I2C")==0)
  {
    if(daq.daq_initialized>=0)
    {
      set_ped_dac(0);
    }
  }
  else if(strcmp(&name[2],"ADC_SPI_RW")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      asic.ADC_SPI_RWb=0;
      gtk_button_set_label((GtkButton*)button,"W");
    }
    else
    {
      asic.ADC_SPI_RWb=1;
      gtk_button_set_label((GtkButton*)button,"R");
    }
  }
  else if(strcmp(&name[2],"load_SPI")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_ADCs(2);
    }
  }
  else if(strcmp(&name[2],"reset_AXI")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_JESD204(0,0);
    }
  }
  else if(strcmp(&name[2],"init_JESD204")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_JESD204(0,1);
    }
  }
  else if(strcmp(&name[2],"load_JESD1")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_JESD204(1,2);
    }
  }
  else if(strcmp(&name[2],"load_JESD2")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_JESD204(2,2);
    }
  }
  else if(strcmp(&name[2],"load_JESD3")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_JESD204(3,2);
    }
  }
  else if(strcmp(&name[2],"JESD1_AXI_RW")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.AXI_RWb[0]=0;
      gtk_button_set_label((GtkButton*)button,"W");
    }
    else
    {
      daq.AXI_RWb[0]=1;
      gtk_button_set_label((GtkButton*)button,"R");
    }
  }
  else if(strcmp(&name[2],"JESD2_AXI_RW")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.AXI_RWb[1]=0;
      gtk_button_set_label((GtkButton*)button,"W");
    }
    else
    {
      daq.AXI_RWb[1]=1;
      gtk_button_set_label((GtkButton*)button,"R");
    }
  }
  else if(strcmp(&name[2],"JESD3_AXI_RW")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.AXI_RWb[2]=0;
      gtk_button_set_label((GtkButton*)button,"W");
    }
    else
    {
      daq.AXI_RWb[2]=1;
      gtk_button_set_label((GtkButton*)button,"R");
    }
  }
  else if(strcmp(&name[2],"get_temp")==0)
  {
    get_temp();
  }
  else if(strcmp(&name[2],"get_baseline")==0)
  {
    if(daq.daq_initialized>=0)
    {
      get_baseline();
    }
  }
  else if(strcmp(&name[2],"gen_BC0")==0)
  {
    daq.gen_BC0=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_DAQ_settings();
  }
  else if(strcmp(&name[2],"gen_WTE")==0)
  {
    printf("WTE2 : %d\n",daq.gen_WTE);
    daq.gen_WTE=gtk_toggle_button_get_active((GtkToggleButton*)button);
    printf("WTE2 : %d\n",daq.gen_WTE);
    update_DAQ_settings();
  }
  else if(strcmp(&name[2],"ADC_scrambling")==0)
  {
    daq.ADC_scrambling=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.ADC_scrambling)
      gtk_button_set_label((GtkButton*)button,"ADC scrambling\n          ON");
    else
      gtk_button_set_label((GtkButton*)button,"ADC scrambling\n          OFF");
  }
  else if(strcmp(&name[2],"ADC_mode")==0)
  {
   printf("Changing ADC mode. Was %d, ",daq.ADC_mode); 
    if(daq.ADC_mode==15)
    {
      daq.ADC_mode=0;
      gtk_button_set_label((GtkButton*)button,"ADC mode\n   data");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==0)
    {
      daq.ADC_mode=1;
      gtk_button_set_label((GtkButton*)button,"ADC mode\n midscale");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==1)
    {
      daq.ADC_mode=2;
      gtk_button_set_label((GtkButton*)button,"ADC mode\nfull scale+");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==2)
    {
      daq.ADC_mode=3;
      gtk_button_set_label((GtkButton*)button,"ADC mode\nfull scale-");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==3)
    {
      daq.ADC_mode=4;
      gtk_button_set_label((GtkButton*)button,"ADC mode\nchecker board");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==4)
    {
      daq.ADC_mode=5;
      gtk_button_set_label((GtkButton*)button,"ADC mode\nrandom long");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==5)
    {
      daq.ADC_mode=6;
      gtk_button_set_label((GtkButton*)button,"ADC mode\nrandom short");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==6)
    {
      daq.ADC_mode=7;
      gtk_button_set_label((GtkButton*)button,"ADC mode\n 0/1 toggle");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    else if(daq.ADC_mode==7)
    {
      daq.ADC_mode=8;
      gtk_button_set_label((GtkButton*)button,"ADC mode\n   patterns");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
    else if(daq.ADC_mode==8)
    {
      daq.ADC_mode=15;
      gtk_button_set_label((GtkButton*)button,"ADC mode\n    ramp");
      printf("new %d\n",daq.ADC_mode); 
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern32_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_pattern10_box");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    update_ADC_patterns(asic.ADC_number,-1);
  }
  else if(strcmp(&name[2],"init_DAQ")==0)
  {
    // Create contact with FEAD board :
    if(daq.daq_initialized<0)
    {
      daq.daq_initialized=0;
      init_gDAQ();
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_DAQ");
      g_assert (widget);
      gtk_button_set_label((GtkButton*)widget,"DAQ initialized");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_PLL");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_init_PLL");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_ADCs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_init_ADCs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_temp");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_temp");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_gen_BC0");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_gen_WTE");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC1_value");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC2_value");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_DAC3_value");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
    else
    {
      gtk_toggle_button_set_active((GtkToggleButton*)button,TRUE);
    }
  }
  else if(strcmp(&name[2],"take_run")==0)
  {
    if(!daq.daq_running)
    {
      printf("Take run\n");
      daq.daq_running=TRUE;
      sprintf(content,"Abort");
      gtk_button_set_label((GtkButton*)button,content);
// Disable "get_event"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);

      take_run();
    }
    else
    {
      daq.daq_running=FALSE;
      sprintf(content,"Take run");
      gtk_button_set_label((GtkButton*)button,content);
// Re-enable "get_event"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
  }
  else if(strcmp(&name[2],"get_event")==0)
  {
    if(daq.debug_DAQ)printf("Get event\n");
    get_single_event();
  }
  else if(strcmp(name,"4_generate_trigger")==0)
  {
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      printf("Send laser trigger\n");
      hw.getNode("ACTIONS").write(EXTRA_TRIGGER*1);
      hw.getNode("ACTIONS").write(EXTRA_TRIGGER*0);
      hw.dispatch();
    }
  }
  if(daq.daq_initialized==0 && daq.trigger_type>=0)
  {
    daq.daq_initialized++;
// Release other actions :
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
  }
}

void Entry_changed(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;
  printf("Should not be there !\n");
}

void Entry_modified(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;

  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  UInt_t command;
  int device_number, iret;
  char text[80], widget_name[80];
  uhal::HwInterface hw=devices.front();

  if(daq.debug_GTK)printf("Entry %s modified : new value %s\n",name,gtk_entry_get_text((GtkEntry*)button));
  if(strcmp(name,"1_I2C_bus")==0)
  {
    //sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.I2C_bus[ich]);
  }
  else if(strcmp(name,"1_I2C_address")==0)
  {
    //sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.I2C_address[ich]);
  }
  else if(strcmp(name,"1_DTU_BL_G10")==0)
  {
    //sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G10[ich]);
    //if(asic.DTU_BL_G10[ich]<0)asic.DTU_BL_G10[ich]=0;
    //if(asic.DTU_BL_G10[ich]>255)asic.DTU_BL_G10[ich]=255;
    //sprintf(text,"%d",asic.DTU_BL_G10[ich]);
    //gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"1_DTU_BL_G1")==0)
  {
    //sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G1[ich]);
    //if(asic.DTU_BL_G1[ich]<0)asic.DTU_BL_G1[ich]=0;
    //if(asic.DTU_BL_G1[ich]>255)asic.DTU_BL_G1[ich]=255;
    //sprintf(text,"%d",asic.DTU_BL_G1[ich]);
    //gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"2_LMB_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.LMB_number);
    if(daq.debug_GTK)printf("New LMB board %d\n",daq.LMB_number);
  }
  else if(strcmp(name,"2_nframe")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ndaq_frame);
    if(daq.ndaq_frame<=0)daq.ndaq_frame=1;
    if(daq.ndaq_frame>N_DAQ_FRAME_MAX-2)daq.ndaq_frame=N_DAQ_FRAME_MAX-2;
    sprintf(text,"%d",daq.ndaq_frame);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"2_HW_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.hw_DAQ_delay);
    if(daq.hw_DAQ_delay<0)daq.hw_DAQ_delay=0;
    if(daq.hw_DAQ_delay>0xffff)daq.hw_DAQ_delay=0xffff;
    sprintf(text,"%d",daq.hw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_DAQ_settings();
  }
  else if(strcmp(name,"2_SW_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.sw_DAQ_delay);
    if(daq.sw_DAQ_delay<0)daq.sw_DAQ_delay=0;
    if(daq.sw_DAQ_delay>0xffff)daq.sw_DAQ_delay=0xffff;
    sprintf(text,"%d",daq.sw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_DAQ_settings();
  }
  else if(strcmp(name,"2_JESD_reset_delay_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.JESD_reset_delay);
    update_DAQ_settings();
  }
  else if(strcmp(name,"3_nevent")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.nevent);
    if(daq.nevent<-1)daq.nevent=-1;
    if(daq.nevent==0)daq.nevent=1;
    sprintf(text,"%d",daq.nevent);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"3_self_trigger_threshold")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.self_trigger_threshold);
    if(daq.self_trigger_threshold<0)daq.self_trigger_threshold=0;
    if(daq.self_trigger_threshold>16381)daq.self_trigger_threshold=16381;
    sprintf(text,"%d",daq.self_trigger_threshold);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_DAQ_settings();
  }
  else if(strcmp(name,"3_self_trigger_mask")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.self_trigger_mask);
    daq.self_trigger_mask&=0x7;
    sprintf(text,"%x",daq.self_trigger_mask);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_DAQ_settings();
  }
  else if(strcmp(name,"3_trigger_inhibit_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_inhibit_duration);
    if(daq.trigger_inhibit_duration>0xffffff)daq.trigger_inhibit_duration=0xffffff;
    sprintf(text,"%x",daq.trigger_inhibit_duration);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("INHIBIT").write(daq.trigger_inhibit_duration);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"5_trigger_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.trigger_number);
    if(daq.trigger_number>7)daq.trigger_number=7;
    if(daq.trigger_number<0)daq.trigger_number=0;
    sprintf(text,"%d",daq.trigger_number);
    gtk_entry_set_text((GtkEntry*)button,text);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_trigger_delay");
    g_assert (widget);
    sprintf(text,"%d",daq.trigger_delay[daq.trigger_number]);
    gtk_entry_set_text((GtkEntry*)widget,text);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_trigger_width");
    g_assert (widget);
    sprintf(text,"%d",daq.trigger_width[daq.trigger_number]);
    gtk_entry_set_text((GtkEntry*)widget,text);
    gtk_widget_activate(widget);
  }
  else if(strcmp(name,"5_trigger_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.trigger_delay[daq.trigger_number]);
    if(daq.trigger_delay[daq.trigger_number]<0)daq.trigger_delay[daq.trigger_number]=0;
    if(daq.trigger_delay[daq.trigger_number]>0x1ffffff)daq.trigger_delay[daq.trigger_number]=0x1ffffff;
    sprintf(text,"%d",daq.trigger_delay[daq.trigger_number]);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(name,"5_trigger_width")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.trigger_width[daq.trigger_number]);
    if(daq.trigger_width[daq.trigger_number]<1)daq.trigger_width[daq.trigger_number]=1;
    if(daq.trigger_width[daq.trigger_number]>255)daq.trigger_width[daq.trigger_number]=255;
    sprintf(text,"%d",daq.trigger_width[daq.trigger_number]);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(name,"5_trigger_mask")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_mask);
    if(daq.trigger_mask<0)daq.trigger_mask=0;
    if(daq.trigger_mask>0xff)daq.trigger_mask=0xff;
    sprintf(text,"%x",daq.trigger_mask);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(name,"5_trigger_map")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_map);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("TRIG_MAP").write(daq.trigger_map);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"5_trigger_tap1_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_tap_value[0]);
    update_trigger();
  }
  else if(strcmp(name,"5_trigger_tap2_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_tap_value[1]);
    update_trigger();
  }
  else if(strcmp(name,"5_trigger_tap3_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.trigger_tap_value[2]);
    update_trigger();
  }
  else if(strcmp(name,"4_DAC1_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DAC_value[0]);
    if(asic.DAC_value[0]<0)asic.DAC_value[0]=0;
    if(asic.DAC_value[0]>4095)asic.DAC_value[0]=4095;
    sprintf(text,"%d",asic.DAC_value[0]);
    gtk_entry_set_text((GtkEntry*)button,text);
    set_ped_dac(1);
  }
  else if(strcmp(name,"4_DAC2_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DAC_value[1]);
    if(asic.DAC_value[1]<0)asic.DAC_value[1]=0;
    if(asic.DAC_value[1]>4095)asic.DAC_value[1]=4095;
    sprintf(text,"%d",asic.DAC_value[1]);
    gtk_entry_set_text((GtkEntry*)button,text);
    set_ped_dac(2);
  }
  else if(strcmp(name,"4_DAC3_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DAC_value[2]);
    if(asic.DAC_value[2]<0)asic.DAC_value[2]=0;
    if(asic.DAC_value[2]>4095)asic.DAC_value[2]=4095;
    sprintf(text,"%d",asic.DAC_value[2]);
    gtk_entry_set_text((GtkEntry*)button,text);
    set_ped_dac(3);
  }
  else if(strcmp(name,"4_PLL_I2C_reg_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.PLL_register_number);
    printf("Set PLL register number to %d\n",asic.PLL_register_number);
  }
  else if(strcmp(name,"4_PLL_I2C_reg_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.PLL_register_value);
    printf("Set PLL register value to %d\n",asic.PLL_register_value);
  }
  else if(strcmp(name,"4_ADC_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.ADC_number);
    if(asic.ADC_number>3)asic.ADC_number=3;
    if(asic.ADC_number<1)asic.ADC_number=1;
    sprintf(text,"%d",asic.ADC_number);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"4_ADC_SPI_reg_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.ADC_register_number);
  }
  else if(strcmp(name,"4_ADC_SPI_reg_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.ADC_register_value);
  }
  else if(strcmp(name,"4_JESD1_AXI_reg_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_number[0]);
  }
  else if(strcmp(name,"4_JESD1_AXI_reg_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_value[0]);
  }
  else if(strcmp(name,"4_JESD2_AXI_reg_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_number[1]);
  }
  else if(strcmp(name,"4_JESD2_AXI_reg_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_value[1]);
  }
  else if(strcmp(name,"4_JESD3_AXI_reg_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_number[2]);
  }
  else if(strcmp(name,"4_JESD3_AXI_reg_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.AXI_reg_value[2]);
  }
  else if(strcmp(name,"4_pattern0_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.ADC_pattern[0]);
    update_ADC_patterns(asic.ADC_number,0);
  }
  else if(strcmp(name,"4_pattern1_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.ADC_pattern[1]);
    update_ADC_patterns(asic.ADC_number,1);
  }
  else if(strcmp(name,"4_pattern2_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.ADC_pattern[2]);
    update_ADC_patterns(asic.ADC_number,2);
  }
  else if(strcmp(name,"4_pattern3_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.ADC_pattern[3]);
    update_ADC_patterns(asic.ADC_number,3);
  }
}

void update_ADC_patterns(Int_t iADC, Int_t word)
{
  if(daq.daq_initialized>=0)
  {
    UInt_t command;
    uhal::HwInterface hw=devices.front();
    if(word<0)
    {
      printf("Update ADC%d mode with %d\n",iADC,daq.ADC_mode);
      Int_t reg=0x550;
      command = (iADC<<24) | (reg<<8) | (daq.ADC_mode&0xff); // data mode
      hw.getNode("ADC_CTRL").write(command);
    }
    else
    {
      printf("Update ADC%d pattern %d with 0x%4.4x\n",iADC,word,daq.ADC_pattern[word]);
      Int_t reg=0x551+word*2;
      command = (iADC<<24) | (reg<<8) | (daq.ADC_pattern[word]&0xff); 
      hw.getNode("ADC_CTRL").write(command);
      reg++;
      command = (iADC<<24) | (reg<<8) | ((daq.ADC_pattern[word]>>8)&0xff);
      hw.getNode("ADC_CTRL").write(command);
    }
  }
}

void update_VFE_control(Int_t pwup_reset)
{
  Int_t pwup_reset_loc=0;
  if(pwup_reset>0) pwup_reset_loc=1;
  if(daq.daq_initialized>=0)
  {
    uhal::HwInterface hw=devices.front();
    if(daq.debug_GTK)printf("Update VFE settings :\n");
    UInt_t VFE_control;
//    VFE_control= PWUP_RESETB*pwup_reset_loc | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
//    hw.getNode("VFE_CTRL").write(VFE_control);
//    if(pwup_reset>0)
//    {
//      VFE_control= I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
//      hw.getNode("VFE_CTRL").write(VFE_control);
//    }
//    hw.dispatch();
  }
}
void update_trigger(void)
{
  char reg_name[32];
  if(daq.daq_initialized>=0)
  {
    uhal::HwInterface hw=devices.front();
    for(Int_t itrig=0 ; itrig<8; itrig++)
    {
      sprintf(reg_name,"TRIGGER_%d",itrig);
      hw.getNode(reg_name).write((daq.trigger_width[itrig]<<25) + daq.trigger_delay[itrig]);
      printf("Trigger %d, delay=%d, width=%d\n",itrig,daq.trigger_delay[itrig],daq.trigger_width[itrig]);
    }
    hw.getNode("TRIG_TAPS").write(daq.trigger_tap_value[2]<<18 | daq.trigger_tap_value[1]<<9 |daq.trigger_tap_value[0]);
    hw.dispatch();
  }
}
void update_DAQ_settings(void)
{
  if(daq.daq_initialized>=0)
  {
    printf("Update DAQ settings : LED %d, WTE %d, BC0 %d, output trigger mask %2.2x\n",daq.led_ON,daq.gen_WTE,daq.gen_BC0,daq.trigger_mask);
    printf("self trigger           : %d\n",daq.self_trigger);
    printf("self trigger threshold : %d\n",daq.self_trigger_threshold);
    printf("self trigger mask      : %d\n",daq.self_trigger_mask);
    printf("SW/HW trigger delays   : %d, %d\n",daq.sw_DAQ_delay, daq.hw_DAQ_delay);
    UInt_t command=  LED_ON*daq.led_ON + GENE_WTE*daq.gen_WTE + GENE_BC0*daq.gen_BC0 + TRIGGER_MASK*daq.trigger_mask +
                     SELF_TRIGGER*daq.self_trigger + SELF_TRIGGER_MASK*daq.self_trigger_mask + SELF_TRIGGER_MODE*daq.self_trigger_mode +
                     WTE_POS*daq.WTE_pos + DELAY_SCAN*daq.delay_scan;
    uhal::HwInterface hw=devices.front();
    printf("command : 0x%8.8x\n",command);
    hw.getNode("SETTINGS").write(command);
    hw.getNode("DAQ_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay + HW_DAQ_DELAY*daq.hw_DAQ_delay);
    hw.getNode("JESD_RST_DELAY").write(daq.JESD_reset_delay);
    hw.getNode("TRIG_THRES").write(daq.self_trigger_threshold);
    hw.dispatch();
  }
}

