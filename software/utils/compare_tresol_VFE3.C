#define NMEAS 10
void compare_tresol_VFE3()
{
  double N[5]={0.92,0.87,0.79,0.89,1.03};
  double q[NMEAS]=     {  10.,  20.,  50., 100., 150., 200., 250., 300., 350., 400.};
  double dt_LDO2[4][NMEAS]={ {250.0,114.0, 50.6, 25.6, 17.7, 13.8, 11.0,  9.9,  8.7,  8.4}, // R320-27pF-LPF35/ 1 vs 0
                             {315.6,122.2, 55.6, 27.7, 20.1, 15.6, 13.2, 10.8,  9.8,  9.0}, // R320-27pF-LPF35/ 2 vs 0
                             {243.8,113.2, 47.3, 23.3, 16.2, 12.1,  9.8,  8.1,  7.5,  7.0}, // R320-27pF-LPF35/ 3 vs 0
                             {237.4,129.7, 45.3, 22.6, 16.1, 12.8, 11.0,  9.3,  8.4,  7.8}}; // R320-27pF-LPF35/ 4 vs 0
  double dt_LDO0[4][NMEAS]={ {261.0,122.7, 51.0, 24.5, 16.1, 13.1, 10.2,  9.2,  7.4,  6.5}, // R320-27pF-LPF35/ 1 vs 0
                             {293.0,154.9, 56.9, 30.4, 20.8, 15.5, 12.6, 11.1,  9.8,  8.5}, // R320-27pF-LPF35/ 2 vs 0
                             {228.1,112.5, 46.3, 22.1, 15.3, 11.7, 10.0,  8.3,  6.7,  6.1}, // R320-27pF-LPF35/ 3 vs 0
                             {237.4,113.0, 46.2, 21.9, 15.5, 11.2,  9.4,  8.2,  6.6,  6.3}}; // R320-27pF-LPF35/ 4 vs 0
  double dt_DCDC2[4][NMEAS]={{250.3,123.9, 49.1, 24.4, 17.2, 12.5, 10.3,  9.1,  7.8,  6.8}, // R320-27pF-LPF35/ 1 vs 0
                             {277.2,143.6, 57.8, 30.6, 20.1, 15.6, 12.2, 10.5,  9.4,  8.1}, // R320-27pF-LPF35/ 2 vs 0
                             {222.5,114.5, 47.8, 24.6, 16.1, 12.2, 10.7,  9.6,  8.6,  8.4}, // R320-27pF-LPF35/ 3 vs 0
                             {230.6,118.0, 49.0, 23.0, 16.4, 11.4,  9.8,  8.3,  7.4,  6.5}}; // R320-27pF-LPF35/ 4 vs 0
  double dt_DCDC1[4][NMEAS]={{267.4,111.6, 47.4, 23.7, 16.5, 13.0, 10.6,  8.6,  7.6,  6.9}, // R320-27pF-LPF35/ 1 vs 0
                             {256.1,152.4, 59.6, 29.7, 21.0, 15.3, 12.4, 10.4,  9.6,  8.4}, // R320-27pF-LPF35/ 2 vs 0
                             {203.3,113.8, 43.2, 23.0, 15.3, 11.7,  9.4,  8.1,  7.1,  6.3}, // R320-27pF-LPF35/ 3 vs 0
                             {214.0,119.3, 46.3, 22.4, 15.6, 11.9, 10.0,  7.8,  7.2,  6.6}}; // R320-27pF-LPF35/ 4 vs 0
  double dt_DCDC0[4][NMEAS]={{267.4,123.0, 49.7, 24.7, 16.0, 12.2, 10.1,  9.0,  7.2,  6.8}, // R320-27pF-LPF35/ 1 vs 0
                             {256.1,156.7, 60.3, 30.8, 21.0, 15.5, 12.7, 10.5,  8.9,  7.9}, // R320-27pF-LPF35/ 2 vs 0
                             {203.3,119.0, 44.0, 23.8, 15.0, 11.9,  9.6,  7.8,  6.7,  6.5}, // R320-27pF-LPF35/ 3 vs 0
                             {214.0,116.0, 47.8, 23.9, 15.4, 11.7,  9.9,  8.3,  6.8,  6.5}}; // R320-27pF-LPF35/ 4 vs 0
  for(int imeas=0; imeas<NMEAS; imeas++)
  {
    dt_LDO2[0][imeas]/=sqrt(2.);
    dt_LDO2[1][imeas]/=sqrt(2.);
    dt_LDO2[2][imeas]/=sqrt(2.);
    dt_LDO2[3][imeas]/=sqrt(2.);
    dt_LDO0[0][imeas]/=sqrt(2.);
    dt_LDO0[1][imeas]/=sqrt(2.);
    dt_LDO0[2][imeas]/=sqrt(2.);
    dt_LDO0[3][imeas]/=sqrt(2.);
    dt_DCDC2[0][imeas]/=sqrt(2.);
    dt_DCDC2[1][imeas]/=sqrt(2.);
    dt_DCDC2[2][imeas]/=sqrt(2.);
    dt_DCDC2[3][imeas]/=sqrt(2.);
    dt_DCDC1[0][imeas]/=sqrt(2.);
    dt_DCDC1[1][imeas]/=sqrt(2.);
    dt_DCDC1[2][imeas]/=sqrt(2.);
    dt_DCDC1[3][imeas]/=sqrt(2.);
    dt_DCDC0[0][imeas]/=sqrt(2.);
    dt_DCDC0[1][imeas]/=sqrt(2.);
    dt_DCDC0[2][imeas]/=sqrt(2.);
    dt_DCDC0[3][imeas]/=sqrt(2.);
  }
  TCanvas *c0=new TCanvas();
  TF1 *f1=new TF1("f1","sqrt([0]*[0]/x/x+[1]*[1]/x+[2]*[2])");
  TGraph *tg_LDO2[4], *tg_LDO0[4], *tg_DCDC2[4], *tg_DCDC1[4], *tg_DCDC0[4];
  tg_LDO2[0]=new TGraph(NMEAS,q,dt_LDO2[0]);
  tg_LDO2[1]=new TGraph(NMEAS,q,dt_LDO2[1]);
  tg_LDO2[2]=new TGraph(NMEAS,q,dt_LDO2[2]);
  tg_LDO2[3]=new TGraph(NMEAS,q,dt_LDO2[3]);
  tg_LDO0[0]=new TGraph(NMEAS,q,dt_LDO0[0]);
  tg_LDO0[1]=new TGraph(NMEAS,q,dt_LDO0[1]);
  tg_LDO0[2]=new TGraph(NMEAS,q,dt_LDO0[2]);
  tg_LDO0[3]=new TGraph(NMEAS,q,dt_LDO0[3]);
  tg_DCDC2[0]=new TGraph(NMEAS,q,dt_DCDC2[0]);
  tg_DCDC2[1]=new TGraph(NMEAS,q,dt_DCDC2[1]);
  tg_DCDC2[2]=new TGraph(NMEAS,q,dt_DCDC2[2]);
  tg_DCDC2[3]=new TGraph(NMEAS,q,dt_DCDC2[3]);
  tg_DCDC1[0]=new TGraph(NMEAS,q,dt_DCDC1[0]);
  tg_DCDC1[1]=new TGraph(NMEAS,q,dt_DCDC1[1]);
  tg_DCDC1[2]=new TGraph(NMEAS,q,dt_DCDC1[2]);
  tg_DCDC1[3]=new TGraph(NMEAS,q,dt_DCDC1[3]);
  tg_DCDC0[0]=new TGraph(NMEAS,q,dt_DCDC0[0]);
  tg_DCDC0[1]=new TGraph(NMEAS,q,dt_DCDC0[1]);
  tg_DCDC0[2]=new TGraph(NMEAS,q,dt_DCDC0[2]);
  tg_DCDC0[3]=new TGraph(NMEAS,q,dt_DCDC0[3]);
  f1->SetParameter(0,1500.);
  f1->SetParameter(1,25.);
  f1->SetParameter(2,2.);
  tg_LDO2[0]->Fit(f1,"","", 9.,500.);
  tg_LDO2[1]->Fit(f1,"","", 9.,500.);
  tg_LDO2[2]->Fit(f1,"","", 9.,500.);
  tg_LDO2[3]->Fit(f1,"","", 9.,500.);
  tg_LDO2[0]->SetMarkerStyle(20);
  tg_LDO2[0]->SetMarkerSize(1.0);
  tg_LDO2[0]->SetMarkerColor(kRed);
  tg_LDO2[0]->SetLineColor(kRed);
  tg_LDO2[0]->SetMaximum(50.);
  tg_LDO2[0]->SetMinimum(0.);
  tg_LDO2[0]->Draw("alp");
  tg_LDO2[0]->GetXaxis()->SetLimits(0.,400.);
  tg_LDO2[0]->GetXaxis()->SetTitle("pulse amplitude [#muA]");
  tg_LDO2[0]->GetYaxis()->SetTitle("resolution [ps]");
  tg_LDO2[0]->Draw("alp");
  tg_LDO2[1]->SetMarkerStyle(20);
  tg_LDO2[1]->SetMarkerSize(1.0);
  tg_LDO2[1]->SetMarkerColor(kMagenta);
  tg_LDO2[1]->SetLineColor(kMagenta);
  tg_LDO2[1]->Draw("lp");
  tg_LDO2[2]->SetMarkerStyle(20);
  tg_LDO2[2]->SetMarkerSize(1.0);
  tg_LDO2[2]->SetMarkerColor(kCyan);
  tg_LDO2[2]->SetLineColor(kCyan);
  tg_LDO2[2]->Draw("lp");
  tg_LDO2[3]->SetMarkerStyle(20);
  tg_LDO2[3]->SetMarkerSize(1.0);
  tg_LDO2[3]->SetMarkerColor(kBlue);
  tg_LDO2[3]->SetLineColor(kBlue);
  tg_LDO2[3]->Draw("lp");
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  tl->AddEntry(tg_LDO2[0],"Ch0 vs Ch1","lp");
  tl->AddEntry(tg_LDO2[1],"Ch2 vs Ch1","lp");
  tl->AddEntry(tg_LDO2[2],"Ch3 vs Ch1","lp");
  tl->AddEntry(tg_LDO2[3],"Ch4 vs Ch1","lp");
  tl->DrawClone();

  TCanvas *c1=new TCanvas("C1_compare","C1_compare");
  tg_LDO2[0]->SetTitle("Channel 2 vs channel 1");
  tg_LDO2[0]->SetMaximum(50.);
  tg_LDO2[0]->SetMinimum(0.);
  tg_LDO2[0]->SetMarkerStyle(20);
  tg_LDO2[0]->SetMarkerSize(1.0);
  tg_LDO2[0]->SetMarkerColor(kRed);
  tg_LDO0[0]->SetLineColor(kRed);
  tg_LDO0[0]->SetMarkerStyle(20);
  tg_LDO0[0]->SetMarkerSize(1.0);
  tg_LDO0[0]->SetMarkerColor(kMagenta);
  tg_LDO2[0]->SetLineColor(kMagenta);
  tg_DCDC2[0]->SetMarkerStyle(20);
  tg_DCDC2[0]->SetMarkerSize(1.0);
  tg_DCDC2[0]->SetMarkerColor(kBlue);
  tg_DCDC2[0]->SetLineColor(kBlue);
  tg_DCDC1[0]->SetMarkerStyle(20);
  tg_DCDC1[0]->SetMarkerSize(1.0);
  tg_DCDC1[0]->SetMarkerColor(kCyan);
  tg_DCDC1[0]->SetLineColor(kCyan);
  tg_DCDC0[0]->SetMarkerStyle(20);
  tg_DCDC0[0]->SetMarkerSize(1.0);
  tg_DCDC0[0]->SetMarkerColor(kGreen);
  tg_DCDC0[0]->SetLineColor(kGreen);
  tg_LDO2[0]->Draw("alp");
  tg_LDO0[0]->Draw("lp");
  tg_DCDC2[0]->Draw("lp");
  tg_DCDC1[0]->Draw("lp");
  tg_DCDC0[0]->Draw("lp");
  tl->Clear();
  tl->AddEntry(tg_LDO2[0],"LDO with 2 #Omega filter","lp");
  tl->AddEntry(tg_LDO0[0],"LDO with 0 #Omega filter","lp");
  tl->AddEntry(tg_DCDC2[0],"DCDC with 2 #Omega filter","lp");
  tl->AddEntry(tg_DCDC1[0],"DCDC with 1 #Omega filter","lp");
  tl->AddEntry(tg_DCDC0[0],"DCDC with 0 #Omega filter","lp");
  tl->Draw();

  TCanvas *c2=new TCanvas("C2_compare","C2_compare");
  tg_LDO2[1]->SetTitle("Channel 3 vs Channel 1");
  tg_LDO2[1]->SetMaximum(50.);
  tg_LDO2[1]->SetMinimum(0.);
  tg_LDO2[1]->SetMarkerStyle(20);
  tg_LDO2[1]->SetMarkerSize(1.0);
  tg_LDO2[1]->SetMarkerColor(kRed);
  tg_LDO0[1]->SetLineColor(kRed);
  tg_LDO0[1]->SetMarkerStyle(20);
  tg_LDO0[1]->SetMarkerSize(1.0);
  tg_LDO0[1]->SetMarkerColor(kMagenta);
  tg_LDO2[1]->SetLineColor(kMagenta);
  tg_DCDC2[1]->SetMarkerStyle(20);
  tg_DCDC2[1]->SetMarkerSize(1.0);
  tg_DCDC2[1]->SetMarkerColor(kBlue);
  tg_DCDC2[1]->SetLineColor(kBlue);
  tg_DCDC1[1]->SetMarkerStyle(20);
  tg_DCDC1[1]->SetMarkerSize(1.0);
  tg_DCDC1[1]->SetMarkerColor(kCyan);
  tg_DCDC1[1]->SetLineColor(kCyan);
  tg_DCDC0[1]->SetMarkerStyle(20);
  tg_DCDC0[1]->SetMarkerSize(1.0);
  tg_DCDC0[1]->SetMarkerColor(kGreen);
  tg_DCDC0[1]->SetLineColor(kGreen);
  tg_LDO2[1]->Draw("alp");
  tg_LDO0[1]->Draw("lp");
  tg_DCDC2[1]->Draw("lp");
  tg_DCDC1[1]->Draw("lp");
  tg_DCDC0[1]->Draw("lp");
  tl->Draw();

  TCanvas *c3=new TCanvas("C3_compare","C3_compare");
  tg_LDO2[2]->SetTitle("Channel 4 vs channel 1");
  tg_LDO2[2]->SetMaximum(50.);
  tg_LDO2[2]->SetMinimum(0.);
  tg_LDO2[2]->SetMarkerStyle(20);
  tg_LDO2[2]->SetMarkerSize(1.0);
  tg_LDO2[2]->SetMarkerColor(kRed);
  tg_LDO0[2]->SetLineColor(kRed);
  tg_LDO0[2]->SetMarkerStyle(20);
  tg_LDO0[2]->SetMarkerSize(1.0);
  tg_LDO0[2]->SetMarkerColor(kMagenta);
  tg_LDO2[2]->SetLineColor(kMagenta);
  tg_DCDC2[2]->SetMarkerStyle(20);
  tg_DCDC2[2]->SetMarkerSize(1.0);
  tg_DCDC2[2]->SetMarkerColor(kBlue);
  tg_DCDC2[2]->SetLineColor(kBlue);
  tg_DCDC1[2]->SetMarkerStyle(20);
  tg_DCDC1[2]->SetMarkerSize(1.0);
  tg_DCDC1[2]->SetMarkerColor(kCyan);
  tg_DCDC1[2]->SetLineColor(kCyan);
  tg_DCDC0[2]->SetMarkerStyle(20);
  tg_DCDC0[2]->SetMarkerSize(1.0);
  tg_DCDC0[2]->SetMarkerColor(kGreen);
  tg_DCDC0[2]->SetLineColor(kGreen);
  tg_LDO2[2]->Draw("alp");
  tg_LDO0[2]->Draw("lp");
  tg_DCDC2[2]->Draw("lp");
  tg_DCDC1[2]->Draw("lp");
  tg_DCDC0[2]->Draw("lp");
  tl->Draw();

  TCanvas *c4=new TCanvas("C4_compare","C4_compare");
  tg_LDO2[3]->SetTitle("Channel 5 vs channel 1");
  tg_LDO2[3]->SetMaximum(50.);
  tg_LDO2[3]->SetMinimum(0.);
  tg_LDO2[3]->SetMarkerStyle(20);
  tg_LDO2[3]->SetMarkerSize(1.0);
  tg_LDO2[3]->SetMarkerColor(kRed);
  tg_LDO0[3]->SetLineColor(kRed);
  tg_LDO0[3]->SetMarkerStyle(20);
  tg_LDO0[3]->SetMarkerSize(1.0);
  tg_LDO0[3]->SetMarkerColor(kMagenta);
  tg_LDO2[3]->SetLineColor(kMagenta);
  tg_DCDC2[3]->SetMarkerStyle(20);
  tg_DCDC2[3]->SetMarkerSize(1.0);
  tg_DCDC2[3]->SetMarkerColor(kBlue);
  tg_DCDC2[3]->SetLineColor(kBlue);
  tg_DCDC1[3]->SetMarkerStyle(20);
  tg_DCDC1[3]->SetMarkerSize(1.0);
  tg_DCDC1[3]->SetMarkerColor(kCyan);
  tg_DCDC1[3]->SetLineColor(kCyan);
  tg_DCDC0[3]->SetMarkerStyle(20);
  tg_DCDC0[3]->SetMarkerSize(1.0);
  tg_DCDC0[3]->SetMarkerColor(kGreen);
  tg_DCDC0[3]->SetLineColor(kGreen);
  tg_LDO2[3]->Draw("alp");
  tg_LDO0[3]->Draw("lp");
  tg_DCDC2[3]->Draw("lp");
  tg_DCDC1[3]->Draw("lp");
  tg_DCDC0[3]->Draw("lp");
  tl->Draw();
}
