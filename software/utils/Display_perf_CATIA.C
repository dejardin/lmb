void Display_perf_CATIA(void)
{
  gStyle->SetOptStat(0);
// Measurements 0/0 47/0 47/27 47/47 Ohm/pF with CATIA-v1-rev3
  const char *label[4]={"0#Omega/0pF","47#Omega/0pF","47#Omega/27pF","47#Omega/47pF"};
  TH1D *h1=new TH1D("","",4,0.5,4.5);
  for(int i=0; i<4; i++)h1->GetXaxis()->SetBinLabel(i+1,label[i]);
// With CATIA-rev2, ADG736 (1)
  Double_t rt1[2][4]={{5.125,0.   ,6.500,6.875},       // BW50
                      {5.625,0.   ,6.875,7.260}};      // BW35
// and CATIA-rev2, TMUX1072 (2)
  Double_t rt2[2][5]={{5.250,5.375,6.000,6.625},
                      {5.750,5.875,6.500,7.240}};
// and CATIA-rev0, TMUX1072 (3)
  Double_t rt3[2][5]={{5.625,5.750,6.625,7.250},
                      {6.250,6.250,7.000,7.500}};

  Double_t x[4];

  TCanvas *c1=new TCanvas("RT vs mux");
  h1->SetMaximum(8.);
  h1->SetMinimum(4.);
  h1->SetTitle("Signal rise time vs filters for different multiplexers");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  h1->GetXaxis()->SetLabelSize(0.06);
  h1->GetXaxis()->SetLabelFont(62);
  h1->GetYaxis()->SetLabelSize(0.05);
  h1->GetYaxis()->SetLabelFont(62);
  h1->GetYaxis()->SetTitle("Rise-time [ns]");
  h1->GetYaxis()->SetTitleOffset(1.);
  h1->GetYaxis()->SetTitleSize(0.05);
  h1->GetYaxis()->SetTitleFont(62);
  h1->DrawCopy();
  for(Int_t i=0; i<4; i++){x[i]=i+1.;}
  TGraph *tg_rt1_BW50=new TGraph(4,x,rt1[0]);
  tg_rt1_BW50->SetMarkerStyle(20);
  tg_rt1_BW50->SetMarkerSize(1.);
  tg_rt1_BW50->SetMarkerColor(kBlue);
  tg_rt1_BW50->Draw("p");
  TGraph *tg_rt2_BW50=new TGraph(4,x,rt2[0]);
  tg_rt2_BW50->SetMarkerStyle(20);
  tg_rt2_BW50->SetMarkerSize(1.);
  tg_rt2_BW50->SetMarkerColor(kRed);
  tg_rt2_BW50->Draw("p");
  TGraph *tg_rt3_BW50=new TGraph(4,x,rt3[0]);
  tg_rt3_BW50->SetMarkerStyle(20);
  tg_rt3_BW50->SetMarkerSize(1.);
  tg_rt3_BW50->SetMarkerColor(kBlue);
  for(Int_t i=0; i<4; i++){x[i]=i+1.25;}
  TGraph *tg_rt1_BW35=new TGraph(4,x,rt1[1]);
  tg_rt1_BW35->SetMarkerStyle(20);
  tg_rt1_BW35->SetMarkerSize(1.);
  tg_rt1_BW35->SetMarkerColor(kCyan);
  tg_rt1_BW35->Draw("p");
  TGraph *tg_rt2_BW35=new TGraph(4,x,rt2[1]);
  tg_rt2_BW35->SetMarkerStyle(20);
  tg_rt2_BW35->SetMarkerSize(1.);
  tg_rt2_BW35->SetMarkerColor(kMagenta);
  tg_rt2_BW35->Draw("p");
  TGraph *tg_rt3_BW35=new TGraph(4,x,rt3[1]);
  tg_rt3_BW35->SetMarkerStyle(20);
  tg_rt3_BW35->SetMarkerSize(1.);
  tg_rt3_BW35->SetMarkerColor(kCyan);
  TLegend *tl1=new TLegend();
  tl1->SetFillStyle(0);
  tl1->AddEntry(tg_rt1_BW50,"G10 rise time, LPF 50 MHz, ADG736","lp");
  tl1->AddEntry(tg_rt2_BW50,"G10 rise time, LPF 50 MHz, TMUX1072","lp");
  tl1->AddEntry(tg_rt1_BW35,"G10 rise time, LPF 35 MHz, ADG736","lp");
  tl1->AddEntry(tg_rt2_BW35,"G10 rise time, LPF 35 MHz, TMUX1072","lp");
  tl1->Draw();

  TCanvas *c2=new TCanvas("RT vs revision");
  h1->SetTitle("Signal rise time vs filters for different CATIA revisions");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  tg_rt2_BW50->Draw("p");
  tg_rt3_BW50->Draw("p");
  tg_rt2_BW35->Draw("p");
  tg_rt3_BW35->Draw("p");
  TLegend *tl2=new TLegend();
  tl2->SetFillStyle(0);
  tl2->AddEntry(tg_rt2_BW50,"CATIA-rev2 R.T., LPF 50 MHz, TMUX1072","lp");
  tl2->AddEntry(tg_rt3_BW50,"CATIA-rev0 R.T., LPF 50 MHz, TMUX1072","lp");
  tl2->AddEntry(tg_rt2_BW35,"CATIA-rev2 R.T., LPF 35 MHz, TMUX1072","lp");
  tl2->AddEntry(tg_rt3_BW35,"CATIA-rev0 R.T., LPF 35 MHz, TMUX1072","lp");
  tl2->Draw();

// Gain
// Rev2 with ADG736 (1)
  Double_t gain1[2][4]={{9.180,0.   ,7.700,6.980},       // BW50
                        {8.770,0.   ,7.450,6.780}};      // BW35
// Rev2 with TMUX1072 (2)
  Double_t gain2[2][5]={{9.050,8.880,8.100,7.370},
                        {8.580,8.500,7.820,7.150}};
// Rev0 with TMUX1072 (3)
  Double_t gain3[2][5]={{8.980,8.830,8.220,7.520},
                        {8.480,8.380,7.830,7.190}};
  TCanvas *c3=new TCanvas("Gain vs mux");
  h1->SetTitle("Channel gain vs filters for different multiplexers");
  h1->SetMaximum(10.);
  h1->SetMinimum(6.);
  h1->GetYaxis()->SetTitle("gain [mV/MeV]");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  for(Int_t i=0; i<4; i++){x[i]=i+1.;}
  TGraph *tg_g1_BW50=new TGraph(4,x,gain1[0]);
  tg_g1_BW50->SetMarkerStyle(20);
  tg_g1_BW50->SetMarkerSize(1.);
  tg_g1_BW50->SetMarkerColor(kBlue);
  tg_g1_BW50->Draw("p");
  TGraph *tg_g2_BW50=new TGraph(4,x,gain2[0]);
  tg_g2_BW50->SetMarkerStyle(20);
  tg_g2_BW50->SetMarkerSize(1.);
  tg_g2_BW50->SetMarkerColor(kRed);
  tg_g2_BW50->Draw("p");
  TGraph *tg_g3_BW50=new TGraph(4,x,gain3[0]);
  tg_g3_BW50->SetMarkerStyle(20);
  tg_g3_BW50->SetMarkerSize(1.);
  tg_g3_BW50->SetMarkerColor(kBlue);
  for(Int_t i=0; i<4; i++){x[i]=i+1.25;}
  TGraph *tg_g1_BW35=new TGraph(4,x,gain1[1]);
  tg_g1_BW35->SetMarkerStyle(20);
  tg_g1_BW35->SetMarkerSize(1.);
  tg_g1_BW35->SetMarkerColor(kCyan);
  tg_g1_BW35->Draw("p");
  TGraph *tg_g2_BW35=new TGraph(4,x,gain2[1]);
  tg_g2_BW35->SetMarkerStyle(20);
  tg_g2_BW35->SetMarkerSize(1.);
  tg_g2_BW35->SetMarkerColor(kMagenta);
  tg_g2_BW35->Draw("p");
  TGraph *tg_g3_BW35=new TGraph(4,x,gain3[1]);
  tg_g3_BW35->SetMarkerStyle(20);
  tg_g3_BW35->SetMarkerSize(1.);
  tg_g3_BW35->SetMarkerColor(kCyan);
  TLegend *tl3=new TLegend();
  tl3->SetFillStyle(0);
  tl3->AddEntry(tg_g1_BW50,"Gain, LPF 50 MHz, ADG736","lp");
  tl3->AddEntry(tg_g2_BW50,"Gain, LPF 50 MHz, TMUX1072","lp");
  tl3->AddEntry(tg_g1_BW35,"Gain, LPF 35 MHz, ADG736","lp");
  tl3->AddEntry(tg_g2_BW35,"Gain, LPF 35 MHz, TMUX1072","lp");
  tl3->Draw();

  TCanvas *c4=new TCanvas("gain vs revision");
  h1->SetTitle("Channel gain vs filters for different CATIA revisions");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  tg_g2_BW50->Draw("p");
  tg_g3_BW50->Draw("p");
  tg_g2_BW35->Draw("p");
  tg_g3_BW35->Draw("p");
  TLegend *tl4=new TLegend();
  tl4->SetFillStyle(0);
  tl4->AddEntry(tg_g2_BW50,"CATIA-rev2 gain, LPF 50 MHz, TMUX1072","lp");
  tl4->AddEntry(tg_g3_BW50,"CATIA-rev0 gain, LPF 50 MHz, TMUX1072","lp");
  tl4->AddEntry(tg_g2_BW35,"CATIA-rev2 gain, LPF 35 MHz, TMUX1072","lp");
  tl4->AddEntry(tg_g3_BW35,"CATIA-rev0 gain, LPF 35 MHz, TMUX1072","lp");
  tl4->Draw();

// Noise
// Rev2 with ADG736 (1)
  Double_t noise1[2][4]={{181.0,0.   ,104.0, 94.0},       // BW50
                         {126.0,0.   , 93.0, 91.0}};      // BW35
// Rev2 with TMUX1072 (2)
  Double_t noise2[2][5]={{183.0,150.0,106.0, 97.0},
                         {128.0,120.0, 95.0, 92.0}};
// Rev0 with TMUX1072 (3)
  Double_t noise3[2][5]={{143.0,131.0, 98.0, 91.0},
                         {111.0,107.0, 92.0, 88.0}};
  TCanvas *c5=new TCanvas("Noise vs mux");
  h1->SetTitle("Noise vs filters for different multiplexers");
  h1->SetMaximum(200.);
  h1->SetMinimum(80.);
  h1->GetYaxis()->SetTitle("sample RMS noise [MeV]");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  for(Int_t i=0; i<4; i++){x[i]=i+1.;}
  TGraph *tg_n1_BW50=new TGraph(4,x,noise1[0]);
  tg_n1_BW50->SetMarkerStyle(20);
  tg_n1_BW50->SetMarkerSize(1.);
  tg_n1_BW50->SetMarkerColor(kBlue);
  tg_n1_BW50->Draw("p");
  TGraph *tg_n2_BW50=new TGraph(4,x,noise2[0]);
  tg_n2_BW50->SetMarkerStyle(20);
  tg_n2_BW50->SetMarkerSize(1.);
  tg_n2_BW50->SetMarkerColor(kRed);
  tg_n2_BW50->Draw("p");
  TGraph *tg_n3_BW50=new TGraph(4,x,noise3[0]);
  tg_n3_BW50->SetMarkerStyle(20);
  tg_n3_BW50->SetMarkerSize(1.);
  tg_n3_BW50->SetMarkerColor(kBlue);
  for(Int_t i=0; i<4; i++){x[i]=i+1.25;}
  TGraph *tg_n1_BW35=new TGraph(4,x,noise1[1]);
  tg_n1_BW35->SetMarkerStyle(20);
  tg_n1_BW35->SetMarkerSize(1.);
  tg_n1_BW35->SetMarkerColor(kCyan);
  tg_n1_BW35->Draw("p");
  TGraph *tg_n2_BW35=new TGraph(4,x,noise2[1]);
  tg_n2_BW35->SetMarkerStyle(20);
  tg_n2_BW35->SetMarkerSize(1.);
  tg_n2_BW35->SetMarkerColor(kMagenta);
  tg_n2_BW35->Draw("p");
  TGraph *tg_n3_BW35=new TGraph(4,x,noise3[1]);
  tg_n3_BW35->SetMarkerStyle(20);
  tg_n3_BW35->SetMarkerSize(1.);
  tg_n3_BW35->SetMarkerColor(kCyan);
  TLegend *tl5=new TLegend();
  tl5->SetFillStyle(0);
  tl5->AddEntry(tg_g1_BW50,"Noise, LPF 50 MHz, ADG736","lp");
  tl5->AddEntry(tg_g2_BW50,"Noise, LPF 50 MHz, TMUX1072","lp");
  tl5->AddEntry(tg_g1_BW35,"Noise, LPF 35 MHz, ADG736","lp");
  tl5->AddEntry(tg_g2_BW35,"Noise, LPF 35 MHz, TMUX1072","lp");
  tl5->Draw();

  TCanvas *c6=new TCanvas("noise vs revision");
  h1->SetTitle("Noise vs filters for different CATIA revisions");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  tg_n2_BW50->Draw("p");
  tg_n3_BW50->Draw("p");
  tg_n2_BW35->Draw("p");
  tg_n3_BW35->Draw("p");
  TLegend *tl6=new TLegend();
  tl6->SetFillStyle(0);
  tl6->AddEntry(tg_g2_BW50,"CATIA-rev2 noise, LPF 50 MHz, TMUX1072","lp");
  tl6->AddEntry(tg_g3_BW50,"CATIA-rev0 noise, LPF 50 MHz, TMUX1072","lp");
  tl6->AddEntry(tg_g2_BW35,"CATIA-rev2 noise, LPF 35 MHz, TMUX1072","lp");
  tl6->AddEntry(tg_g3_BW35,"CATIA-rev0 noise, LPF 35 MHz, TMUX1072","lp");
  tl6->Draw();

// Figure of Merit
// Rev2 with ADG736 (1)
  Double_t fom1[2][4]={{928.0,0.   ,611.0,653.0},       // BW50
                       {709.0,0.   ,625.0,660.0}};      // BW35
// Rev2 with TMUX1072 (2)
  Double_t fom2[2][5]={{983.0,806.0,630.0,642.0},
                       {736.0,705.0,615.0,661.0}};
// Rev0 with TMUX1072 (3)
  Double_t fom3[2][5]={{804.0,750.0,648.0,660.0},
                       {694.0,668.0,640.0,658.0}};
  TCanvas *c7=new TCanvas("FoM vs mux");
  h1->SetTitle("Figure of Merit vs filters for different multiplexers");
  h1->SetMaximum(1000.);
  h1->SetMinimum(600.);
  h1->GetYaxis()->SetTitle("fom [ps.GeV]");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  for(Int_t i=0; i<4; i++){x[i]=i+1.;}
  TGraph *tg_f1_BW50=new TGraph(4,x,fom1[0]);
  tg_f1_BW50->SetMarkerStyle(20);
  tg_f1_BW50->SetMarkerSize(1.);
  tg_f1_BW50->SetMarkerColor(kBlue);
  tg_f1_BW50->Draw("p");
  TGraph *tg_f2_BW50=new TGraph(4,x,fom2[0]);
  tg_f2_BW50->SetMarkerStyle(20);
  tg_f2_BW50->SetMarkerSize(1.);
  tg_f2_BW50->SetMarkerColor(kRed);
  tg_f2_BW50->Draw("p");
  TGraph *tg_f3_BW50=new TGraph(4,x,fom3[0]);
  tg_f3_BW50->SetMarkerStyle(20);
  tg_f3_BW50->SetMarkerSize(1.);
  tg_f3_BW50->SetMarkerColor(kBlue);
  for(Int_t i=0; i<4; i++){x[i]=i+1.25;}
  TGraph *tg_f1_BW35=new TGraph(4,x,fom1[1]);
  tg_f1_BW35->SetMarkerStyle(20);
  tg_f1_BW35->SetMarkerSize(1.);
  tg_f1_BW35->SetMarkerColor(kCyan);
  tg_f1_BW35->Draw("p");
  TGraph *tg_f2_BW35=new TGraph(4,x,fom2[1]);
  tg_f2_BW35->SetMarkerStyle(20);
  tg_f2_BW35->SetMarkerSize(1.);
  tg_f2_BW35->SetMarkerColor(kMagenta);
  tg_f2_BW35->Draw("p");
  TGraph *tg_f3_BW35=new TGraph(4,x,fom3[1]);
  tg_f3_BW35->SetMarkerStyle(20);
  tg_f3_BW35->SetMarkerSize(1.);
  tg_f3_BW35->SetMarkerColor(kCyan);
  TLegend *tl7=new TLegend();
  tl7->SetFillStyle(0);
  tl7->AddEntry(tg_f1_BW50,"FoM, LPF 50 MHz, ADG736","lp");
  tl7->AddEntry(tg_f2_BW50,"FoM, LPF 50 MHz, TMUX1072","lp");
  tl7->AddEntry(tg_f1_BW35,"FoM, LPF 35 MHz, ADG736","lp");
  tl7->AddEntry(tg_f2_BW35,"FoM, LPF 35 MHz, TMUX1072","lp");
  tl7->Draw();

  TCanvas *c8=new TCanvas("noise vs revision");
  h1->SetTitle("Figure of Merit vs filters for different CATIA revisions");
  h1->DrawCopy();
  gPad->SetGridx();
  gPad->SetGridy();
  tg_f2_BW50->Draw("p");
  tg_f3_BW50->Draw("p");
  tg_f2_BW35->Draw("p");
  tg_f3_BW35->Draw("p");
  TLegend *tl8=new TLegend();
  tl8->SetFillStyle(0);
  tl8->AddEntry(tg_g2_BW50,"CATIA-rev2 FoM, LPF 50 MHz, TMUX1072","lp");
  tl8->AddEntry(tg_g3_BW50,"CATIA-rev0 FoM, LPF 50 MHz, TMUX1072","lp");
  tl8->AddEntry(tg_g2_BW35,"CATIA-rev2 FoM, LPF 35 MHz, TMUX1072","lp");
  tl8->AddEntry(tg_g3_BW35,"CATIA-rev0 FoM, LPF 35 MHz, TMUX1072","lp");
  tl8->Draw();

}

  
