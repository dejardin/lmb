#include <TH1D.h>
#include <TProfile.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TLegend.h>
void show_TP_pulse_DTU(char *fname,int dac_val=192, int step=32, int nstep=84, int G10=1, double tref=800., Double_t fit_duration=800.)
{
  #include "../include/TP_parasitic.h"
  Int_t N_active_channels=6;
  int color[6]={kBlue, kCyan, kGreen, kYellow, kMagenta, kRed};
  char gname[132],pname[132];
  double dv_DAC=1074./pow(2,12);
  double Rinj=2471;
  if(G10==0)Rinj=272;
  double dac_INL=0.1; 
  //double Vmax=1200.;
  //double Vref=1100.;
  double Vmax=1250.;
  double Vref=1100.;
  Int_t n_display_step=6;
  Int_t display_step[6]={35,40,50,60,70,75};
  Int_t display_channel=4;

  TFile *tf=new TFile(fname);
  TProfile *loc_tp;
  TProfile *tp;
  TH1D *h1, *h2;
  
  TCanvas *tc0,*tc1;
  tc0=new TCanvas("cc0","cc0");
  tc1=new TCanvas("cc1","cc1");
  tc0->SetTitle("Current injection pulse response");
  tc1->SetTitle("Normalized current injection pulse response");
  gStyle->SetOptStat(0);
  tc0->Update();
  tc1->Update();
  double s1=-1,s2=-1,i1=-1,i2=-1,i0=-1,gain[6];

  
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  for(int ich=display_channel; ich<=display_channel; ich++)
  {
    if(G10==1 && (ich==2 || ich==3 || ich==5))continue;
    if(G10==0 && (ich==0 || ich==1 || ich==4))continue;
    for(int istep=0; istep<n_display_step; istep++)
    {
      sprintf(pname,"ch_%d_step_%d_%d",ich,display_step[istep],dac_val+step*display_step[istep]);
      //printf("Search for histogram %s\n",pname);
      gDirectory->GetObject(pname,tp);
      tp->SetTitle("TIA : Current injection pulse response");
      tp->SetMaximum(Vmax);
      tp->SetMinimum(0.);
      tp->SetMarkerStyle(20);
      tp->SetMarkerColor(color[ich]);
      tp->SetMarkerSize(0.3);
      tp->SetLineColor(color[ich]);
      Int_t n=tp->GetNbinsX();
      if(istep==0)
      {
        h1=new TH1D("h1","h1",n,0.,6.25*n);
        h2=new TH1D("h2","h2",n,0.,6.25*n);
      }
      Double_t ped=0., top=0.;
      Int_t nped=0, ntop=0;
      for(int i=0; i<n; i++)
      {
        double t=6.25*i;
        double y=tp->GetBinContent(i+1);
        if(y>0)y-=parasitic[i];
        h1->SetBinContent(i+1,y);
        //h1->SetBinContent(i+1,y);
        if(t<500.){ped+=y; nped++;}
        if(t>650. && t<750.){top+=y; ntop++;}
      }
      ped/=nped;
      top/=ntop;
      for(int i=0; i<n; i++)
      {
        double y=h1->GetBinContent(i+1);
        y-=ped;
        h1->SetBinContent(i+1,y);
        h2->SetBinContent(i+1,y/(top-ped));
      }
      h1->SetLineWidth(2.);
      h1->SetLineColor(color[istep%6]);
      h1->SetMaximum(1200.);
      h1->SetMinimum(-50.);
      h2->SetLineWidth(2.);
      h2->SetLineColor(color[istep%6]);
      h2->SetMaximum(1.05);
      h2->SetMinimum(-0.05);

      if(istep==0)
      {
        tc0->cd();
        h1->DrawClone();
        gPad->SetGridx();
        gPad->SetGridy();
        h1->GetXaxis()->SetTitle("time [ns]");
        h1->GetYaxis()->SetTitle("amplitude [mV]");
        h1->GetXaxis()->SetTitleFont(62);
        h1->GetXaxis()->SetTitleSize(0.05);
        h1->GetXaxis()->SetLabelFont(62);
        h1->GetXaxis()->SetLabelSize(0.05);
        h1->GetYaxis()->SetTitleOffset(1.00);
        h1->GetYaxis()->SetTitleFont(62);
        h1->GetYaxis()->SetTitleSize(0.05);
        h1->GetYaxis()->SetLabelFont(62);
        h1->GetYaxis()->SetLabelSize(0.05);
        tc0->Update();
        tc1->cd();
        h2->DrawClone();
        gPad->SetGridx();
        gPad->SetGridy();
        h2->GetXaxis()->SetTitle("time [ns]");
        h2->GetYaxis()->SetTitle("normalized amplitude [mV]");
        h2->GetXaxis()->SetTitleFont(62);
        h2->GetXaxis()->SetTitleSize(0.05);
        h2->GetXaxis()->SetLabelFont(62);
        h2->GetXaxis()->SetLabelSize(0.05);
        h2->GetYaxis()->SetTitleOffset(1.00);
        h2->GetYaxis()->SetTitleFont(62);
        h2->GetYaxis()->SetTitleSize(0.05);
        h2->GetYaxis()->SetLabelFont(62);
        h2->GetYaxis()->SetLabelSize(0.05);
        tc1->Update();
      }
      else
      {
        tc0->cd();
        h1->DrawClone("same");
        tc0->Update();
        tc1->cd();
        h2->DrawClone("same");
        tc1->Update();
      }
      //return;
    }
  }

  sprintf(pname,"%s",fname);
  char *pos=strstr(pname,".root");
  sprintf(pos,"_corrected.root");
  printf("Output file : %s\n",pname);
  //TFile *tfout=new TFile(pname,"recreate");
  //tcc->Write();
  //tc0->Write();
}
      
      
