#define NMEAS 9
void Display_tresol()
{
  double N[5]={0.92,0.87,0.79,0.89,1.03};
  double q[NMEAS]=     {  10.,  20.,  50., 100., 200., 250., 300., 350., 400.};
  double dt[4][NMEAS]={{289.4,141.2, 55.8, 29.5, 14.0, 11.7, 10.2,  8.7,  8.4}, // R320-27pF-LPF35/ 0 vs 3
                       {322.9,167.1, 63.4, 33.2, 16.1, 12.8, 11.8, 10.0,  8.9}, // R320-27pF-LPF35/ 1 vs 3
                       {313.6,157.6, 61.8, 34.0, 16.0, 13.5, 12.3,  9.7,  9.0}, // R320-27pF-LPF35/ 2 vs 3
                       {336.6,165.4, 65.4, 32.8, 16.6, 13.4, 11.1,  9.7,  9.1}}; // R320-27pF-LPF35/ 4 vs 3
  for(int imeas=0; imeas<NMEAS; imeas++)
  {
    dt[0][imeas]/=sqrt(2.);
    dt[1][imeas]/=sqrt(2.);
    dt[2][imeas]/=sqrt(2.);
    dt[3][imeas]/=sqrt(2.);
  }
  TF1 *ftmp;
  TF1 *f1=new TF1("f1","sqrt([0]*[0]/x/x+[1]*[1]/x+[2]*[2])");
  TGraph *tg[4];
  tg[0]=new TGraph(NMEAS,q,dt[0]);
  tg[1]=new TGraph(NMEAS,q,dt[1]);
  tg[2]=new TGraph(NMEAS,q,dt[2]);
  tg[3]=new TGraph(NMEAS,q,dt[3]);
  f1->SetParameter(0,1500.);
  f1->FixParameter(1,0.);
  f1->SetParameter(2,2.);
  tg[0]->Fit(f1,"","", 9.,500.);
  ftmp=tg[0]->GetFunction("f1");
  ftmp->SetLineColor(kRed);
  tg[1]->Fit(f1,"","", 9.,500.);
  ftmp=tg[1]->GetFunction("f1");
  ftmp->SetLineColor(kMagenta);
  tg[2]->Fit(f1,"","", 9.,500.);
  ftmp=tg[2]->GetFunction("f1");
  ftmp->SetLineColor(kCyan);
  tg[3]->Fit(f1,"","", 9.,500.);
  ftmp=tg[3]->GetFunction("f1");
  ftmp->SetLineColor(kBlue);
  tg[0]->SetMarkerStyle(20);
  tg[0]->SetMarkerSize(1.0);
  tg[0]->SetMarkerColor(kRed);
  tg[0]->SetLineColor(kRed);
  tg[0]->SetMaximum(50.);
  tg[0]->SetMinimum(0.);
  tg[0]->Draw("alp");
  tg[0]->GetXaxis()->SetLimits(0.,500.);
  tg[0]->GetXaxis()->SetTitle("pulse amplitude [#muA]");
  tg[0]->GetYaxis()->SetTitle("resolution [ps]");
  tg[0]->Draw("alp");
  tg[1]->SetMarkerStyle(20);
  tg[1]->SetMarkerSize(1.0);
  tg[1]->SetMarkerColor(kMagenta);
  tg[1]->SetLineColor(kMagenta);
  tg[1]->Draw("lp");
  tg[2]->SetMarkerStyle(20);
  tg[2]->SetMarkerSize(1.0);
  tg[2]->SetMarkerColor(kCyan);
  tg[2]->SetLineColor(kCyan);
  tg[2]->Draw("lp");
  tg[3]->SetMarkerStyle(20);
  tg[3]->SetMarkerSize(1.0);
  tg[3]->SetMarkerColor(kBlue);
  tg[3]->SetLineColor(kBlue);
  tg[3]->Draw("lp");
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  tl->AddEntry(tg[0],"Ch1 vs Ch4","lp");
  tl->AddEntry(tg[1],"Ch2 vs Ch4","lp");
  tl->AddEntry(tg[2],"Ch3 vs Ch4","lp");
  tl->AddEntry(tg[3],"Ch5 vs Ch4","lp");
  tl->Draw();
}
