void Display_saturation(Int_t G1=0, Int_t version=4)
{
  TFile *file[13];
  char hname[132];
  Double_t current[13]={300.,400.,500.,1000.,2000.,3000.,4000.,5000.,6000.,7000.,8000.,9000.,10000.};
  Int_t max_file=9;
  for(Int_t ifile=0; ifile<max_file; ifile++)
  {
    if(version==4)
    {
      if(G1==0)
      {
        if(current[ifile]>=1000.)
        {
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R340_LPF35.root",int(current[ifile]/1000.));
          //sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R340_LPF35_CATIA_V1.4_1uF_10uF.root",int(current[ifile]/1000.));
        }
        else
        {
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R340_LPF35.root",int(current[ifile]));
          //sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R340_LPF35_CATIA_V1.4_1uF_10uF.root",int(current[ifile]));
        }
      }
      else
      {
        if(current[ifile]>=1000.)
        {
          //sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R340_LPF35_G1_forced.root",int(current[ifile]/1000.));
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R340_LPF35_CATIA_V1.4_1uF_10uF_G1_forced.root",int(current[ifile]/1000.));
        }
        else
        {
          //sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R340_LPF35_G1_forced.root",int(current[ifile]));
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R340_LPF35_CATIA_V1.4_1uF_10uF_G1_forced.root",int(current[ifile]));
        }
      }
    }
    else
    {
      if(G1==0)
      {
        if(current[ifile]>=1000.)
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R400_LPF35_CATIA_V1.1.root",int(current[ifile]/1000.));
        else
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R400_LPF35_CATIA_V1.1.root",int(current[ifile]));
      }
      else
      {
        if(current[ifile]>=1000.)
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%2.2dV_R400_LPF35_CATIA_V1.1_G1_forced.root",int(current[ifile]/1000.));
        else
          sprintf(hname,"data/CATIA_V1.4/AWG_single_%3.3dmV_R400_LPF35_CATIA_V1.1_G1_forced.root",int(current[ifile]));
      }
    }

    file[ifile]=new TFile(hname);
  }
  //gStyle->SetPalette(kVisibleSpectrum);
  gStyle->SetPalette(kRainBow);
  gStyle->SetOptStat(0);
  TLegend *tl=new TLegend(0.69, 0.40,0.90, 0.90);
  tl->SetFillStyle(0);
  for(Int_t ifile=0; ifile<max_file; ifile++)
  {
    Int_t color=gStyle->GetColorPalette((ifile*255)/max_file+1);
    file[ifile]->cd();
    TProfile *tp;
    TH1D *h1;
    gDirectory->GetObject("ch_0_step_0_0",tp);
    Int_t nbin=tp->GetNbinsX();
    Double_t Xmin=tp->GetXaxis()->GetXmin();
    Double_t Xmax=tp->GetXaxis()->GetXmax();
    h1=new TH1D("h1","h1",nbin,Xmin,Xmax);
    for(Int_t i=0; i<nbin; i++)
    {
      Double_t y=tp->GetBinContent(i+1);
      //if(G1==1)y*=10.;
      h1->SetBinContent(i+1,y);
    }
    if(G1==1)
      sprintf(hname,"G1 output, CATIA-V1.%d, C_{link}=1 #muF",version);
    else
      //sprintf(hname,"G10 output, CATIA-V1.%d, C_{link}=1 #muF",version);
      sprintf(hname,"G10 output, CATIA-V1.%d",version);
    h1->SetTitle(hname);
    h1->SetLineWidth(2);
    h1->SetLineColor(color);
    h1->SetMarkerColor(color);
    sprintf(hname,"%.1f mA pulse",current[ifile]/1000.);
    if(ifile==0)
    {
      if(G1==0)
      {
        if(version==4)
        {
          h1->SetMaximum(20);
          h1->SetMinimum(-10);
        }
        else
        {
          h1->SetMaximum(10);
          h1->SetMinimum(-20);
        }
      }
      else
      {
        if(version==4)
        {
          h1->SetMaximum(5.);
          h1->SetMinimum(-1.);
        }
        else
        {
          h1->SetMaximum(5.);
          h1->SetMinimum(-1.);
        }
      }
      h1->Draw();
      h1->GetXaxis()->SetRangeUser(100.,600e3);
      //h1->GetXaxis()->SetRangeUser(100.,5.e3);
      h1->GetXaxis()->SetTitle("time [ns]");
      h1->GetXaxis()->SetTitleFont(62);
      h1->GetXaxis()->SetTitleSize(0.05);
      h1->GetXaxis()->SetLabelFont(62);
      h1->GetXaxis()->SetLabelSize(0.05);
      h1->GetYaxis()->SetTitle("amplitude [mV]");
      h1->GetYaxis()->SetTitleFont(62);
      h1->GetYaxis()->SetTitleSize(0.05);
      h1->GetYaxis()->SetTitleOffset(1.00);
      h1->GetYaxis()->SetLabelFont(62);
      h1->GetYaxis()->SetLabelSize(0.05);
      //gPad->SetLogx();
      gPad->SetGridx();
      gPad->SetGridy();
      h1->Draw();
    }
    else
      h1->Draw("same");
    tl->AddEntry(h1,hname,"lp");
  }
  tl->Draw();
}
