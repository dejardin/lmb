LMB stands for Laser Monitoring Board. It is foreseen to drive and monitor the lasers of the ECAL monitoring system at HL-LHC.

It receives the 160 MHz clock and triggers info from lpGBT located on an embedded FE board (connected to a dedicated back end board).

- There is a 1280 MHz clock generator which drives 3 fast 14 bits ADCs to digitize signals from PIN diodes to make an image of each laser pulse.
The data of this ADC is splitted to 8 streams at 160 MHz using the compressed data format of the LiTE-DTU used for the barrel electronics.

- All the streams are connected to the FE board to be sent to the Back-End electronics and thus be packed with the remaining part of the ECAL events.

- The board generates triggers for lasers, DSO and others according to data received by the FE board. Each trigger can be tuned in delay and width.
A port mapping allows to connect any signal to any connector.

- There is also a connection by IPbus with a Gigabit ethernet port for fast data monitoring and device programming.

- The board format is not yet fixed.
