// File>>> deglitch.v
//
// Date: Wed Mar  4 13:15:26 CET 2020
// Author: gianni
//
// Revision history:
//
// Deglitch circuit
//

module deglitch (
	input  A,
	output Z);

	wire net0;
	wire net1;

	OR2D4 inst00(.A1(A), .A2(net1), .Z(Z));
	DEL4  inst01(.I(A), .Z(net0));
	DEL4  inst02(.I(net0), .Z(net1));
endmodule

