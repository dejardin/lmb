// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
// Date        : Thu Aug 22 12:31:36 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode synth_stub
//               /data/cms/ecal/fe/LMB/firmware/LMB.runs/clk_generator_ethernet_synth_1/clk_generator_ethernet_stub.v
// Design      : clk_generator_ethernet
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_generator_ethernet(clk_ethernet, clk_axi, reset, locked, clk_160_in)
/* synthesis syn_black_box black_box_pad_pin="reset,locked" */
/* synthesis syn_force_seq_prim="clk_ethernet" */
/* synthesis syn_force_seq_prim="clk_axi" */
/* synthesis syn_force_seq_prim="clk_160_in" */;
  output clk_ethernet /* synthesis syn_isclock = 1 */;
  output clk_axi /* synthesis syn_isclock = 1 */;
  input reset;
  output locked;
  input clk_160_in /* synthesis syn_isclock = 1 */;
endmodule
