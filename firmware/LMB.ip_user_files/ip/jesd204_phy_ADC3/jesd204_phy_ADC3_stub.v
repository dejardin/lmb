// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
// Date        : Thu Aug 22 14:22:28 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode synth_stub
//               /data/cms/ecal/fe/LMB/firmware/LMB.gen/sources_1/ip/jesd204_phy_ADC3/jesd204_phy_ADC3_stub.v
// Design      : jesd204_phy_ADC3
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "jesd204_phy_v4_0_20,Vivado 2024.1.1" *)
module jesd204_phy_ADC3(s_axi_aclk, s_axi_aresetn, s_axi_awaddr, 
  s_axi_awvalid, s_axi_awready, s_axi_wdata, s_axi_wvalid, s_axi_wready, s_axi_bresp, 
  s_axi_bvalid, s_axi_bready, s_axi_araddr, s_axi_arvalid, s_axi_arready, s_axi_rdata, 
  s_axi_rresp, s_axi_rvalid, s_axi_rready, tx_sys_reset, rx_sys_reset, tx_reset_gt, 
  rx_reset_gt, tx_reset_done, rx_reset_done, gt_powergood, cpll_refclk, qpll0_refclk, 
  common0_qpll0_lock_out, common0_qpll0_refclk_out, common0_qpll0_clk_out, qpll1_refclk, 
  common0_qpll1_lock_out, common0_qpll1_refclk_out, common0_qpll1_clk_out, 
  rxencommaalign, tx_core_clk, txoutclk, rx_core_clk, rxoutclk, drpclk, gt_prbssel, 
  gt0_txcharisk, gt0_txdata, gt1_txcharisk, gt1_txdata, gt2_txcharisk, gt2_txdata, 
  gt3_txcharisk, gt3_txdata, gt0_rxcharisk, gt0_rxdisperr, gt0_rxnotintable, gt0_rxdata, 
  gt1_rxcharisk, gt1_rxdisperr, gt1_rxnotintable, gt1_rxdata, gt2_rxcharisk, gt2_rxdisperr, 
  gt2_rxnotintable, gt2_rxdata, gt3_rxcharisk, gt3_rxdisperr, gt3_rxnotintable, gt3_rxdata, 
  rxn_in, rxp_in, txn_out, txp_out)
/* synthesis syn_black_box black_box_pad_pin="s_axi_aresetn,s_axi_awaddr[11:0],s_axi_awvalid,s_axi_awready,s_axi_wdata[31:0],s_axi_wvalid,s_axi_wready,s_axi_bresp[1:0],s_axi_bvalid,s_axi_bready,s_axi_araddr[11:0],s_axi_arvalid,s_axi_arready,s_axi_rdata[31:0],s_axi_rresp[1:0],s_axi_rvalid,s_axi_rready,tx_sys_reset,rx_sys_reset,tx_reset_gt,rx_reset_gt,tx_reset_done,rx_reset_done,gt_powergood,cpll_refclk,common0_qpll0_lock_out,common0_qpll0_refclk_out,common0_qpll0_clk_out,common0_qpll1_lock_out,common0_qpll1_refclk_out,common0_qpll1_clk_out,rxencommaalign,txoutclk,rxoutclk,gt_prbssel[3:0],gt0_txcharisk[3:0],gt0_txdata[31:0],gt1_txcharisk[3:0],gt1_txdata[31:0],gt2_txcharisk[3:0],gt2_txdata[31:0],gt3_txcharisk[3:0],gt3_txdata[31:0],gt0_rxcharisk[3:0],gt0_rxdisperr[3:0],gt0_rxnotintable[3:0],gt0_rxdata[31:0],gt1_rxcharisk[3:0],gt1_rxdisperr[3:0],gt1_rxnotintable[3:0],gt1_rxdata[31:0],gt2_rxcharisk[3:0],gt2_rxdisperr[3:0],gt2_rxnotintable[3:0],gt2_rxdata[31:0],gt3_rxcharisk[3:0],gt3_rxdisperr[3:0],gt3_rxnotintable[3:0],gt3_rxdata[31:0],rxn_in[3:0],rxp_in[3:0],txn_out[3:0],txp_out[3:0]" */
/* synthesis syn_force_seq_prim="s_axi_aclk" */
/* synthesis syn_force_seq_prim="qpll0_refclk" */
/* synthesis syn_force_seq_prim="qpll1_refclk" */
/* synthesis syn_force_seq_prim="tx_core_clk" */
/* synthesis syn_force_seq_prim="rx_core_clk" */
/* synthesis syn_force_seq_prim="drpclk" */;
  input s_axi_aclk /* synthesis syn_isclock = 1 */;
  input s_axi_aresetn;
  input [11:0]s_axi_awaddr;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [11:0]s_axi_araddr;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  input tx_sys_reset;
  input rx_sys_reset;
  input tx_reset_gt;
  input rx_reset_gt;
  output tx_reset_done;
  output rx_reset_done;
  output gt_powergood;
  input cpll_refclk;
  input qpll0_refclk /* synthesis syn_isclock = 1 */;
  output common0_qpll0_lock_out;
  output common0_qpll0_refclk_out;
  output common0_qpll0_clk_out;
  input qpll1_refclk /* synthesis syn_isclock = 1 */;
  output common0_qpll1_lock_out;
  output common0_qpll1_refclk_out;
  output common0_qpll1_clk_out;
  input rxencommaalign;
  input tx_core_clk /* synthesis syn_isclock = 1 */;
  output txoutclk;
  input rx_core_clk /* synthesis syn_isclock = 1 */;
  output rxoutclk;
  input drpclk /* synthesis syn_isclock = 1 */;
  input [3:0]gt_prbssel;
  input [3:0]gt0_txcharisk;
  input [31:0]gt0_txdata;
  input [3:0]gt1_txcharisk;
  input [31:0]gt1_txdata;
  input [3:0]gt2_txcharisk;
  input [31:0]gt2_txdata;
  input [3:0]gt3_txcharisk;
  input [31:0]gt3_txdata;
  output [3:0]gt0_rxcharisk;
  output [3:0]gt0_rxdisperr;
  output [3:0]gt0_rxnotintable;
  output [31:0]gt0_rxdata;
  output [3:0]gt1_rxcharisk;
  output [3:0]gt1_rxdisperr;
  output [3:0]gt1_rxnotintable;
  output [31:0]gt1_rxdata;
  output [3:0]gt2_rxcharisk;
  output [3:0]gt2_rxdisperr;
  output [3:0]gt2_rxnotintable;
  output [31:0]gt2_rxdata;
  output [3:0]gt3_rxcharisk;
  output [3:0]gt3_rxdisperr;
  output [3:0]gt3_rxnotintable;
  output [31:0]gt3_rxdata;
  input [3:0]rxn_in;
  input [3:0]rxp_in;
  output [3:0]txn_out;
  output [3:0]txp_out;
endmodule
