-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
-- Date        : Sun Jan 26 16:32:53 2025
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode synth_stub -rename_top high_speed_selectio_wiz_Bank_66 -prefix
--               high_speed_selectio_wiz_Bank_66_ high_speed_selectio_wiz_Bank_66_stub.vhdl
-- Design      : high_speed_selectio_wiz_Bank_66
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity high_speed_selectio_wiz_Bank_66 is
  Port ( 
    vtc_rdy_bsc0 : out STD_LOGIC;
    en_vtc_bsc0 : in STD_LOGIC;
    vtc_rdy_bsc1 : out STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    vtc_rdy_bsc2 : out STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    vtc_rdy_bsc3 : out STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    vtc_rdy_bsc4 : out STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    vtc_rdy_bsc5 : out STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC;
    dly_rdy_bsc0 : out STD_LOGIC;
    dly_rdy_bsc1 : out STD_LOGIC;
    dly_rdy_bsc2 : out STD_LOGIC;
    dly_rdy_bsc3 : out STD_LOGIC;
    dly_rdy_bsc4 : out STD_LOGIC;
    dly_rdy_bsc5 : out STD_LOGIC;
    rst_seq_done : out STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    pll0_clkout0 : out STD_LOGIC;
    pll0_clkout1 : out STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC;
    pll0_locked : out STD_LOGIC;
    ch2_out3_P : out STD_LOGIC;
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out3_N : out STD_LOGIC;
    ch2_out0_P : out STD_LOGIC;
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out0_N : out STD_LOGIC;
    ch3_out1_P : out STD_LOGIC;
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out1_N : out STD_LOGIC;
    ch1_out7_P : out STD_LOGIC;
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch1_out7_N : out STD_LOGIC;
    ch3_out2_P : out STD_LOGIC;
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out2_N : out STD_LOGIC;
    ch2_out6_P : out STD_LOGIC;
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out6_N : out STD_LOGIC;
    ch3_out0_P : out STD_LOGIC;
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out0_N : out STD_LOGIC;
    ch2_out7_P : out STD_LOGIC;
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out7_N : out STD_LOGIC;
    ch2_out5_P : out STD_LOGIC;
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out5_N : out STD_LOGIC;
    ch2_out2_P : out STD_LOGIC;
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out2_N : out STD_LOGIC;
    ch3_out4_P : out STD_LOGIC;
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out4_N : out STD_LOGIC;
    ch3_out3_P : out STD_LOGIC;
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out3_N : out STD_LOGIC;
    ch3_out5_P : out STD_LOGIC;
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out5_N : out STD_LOGIC;
    ch3_out7_P : out STD_LOGIC;
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out7_N : out STD_LOGIC;
    ch3_out6_P : out STD_LOGIC;
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out6_N : out STD_LOGIC;
    TH_out_P : out STD_LOGIC;
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    TH_out_N : out STD_LOGIC
  );

end high_speed_selectio_wiz_Bank_66;

architecture stub of high_speed_selectio_wiz_Bank_66 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "vtc_rdy_bsc0,en_vtc_bsc0,vtc_rdy_bsc1,en_vtc_bsc1,vtc_rdy_bsc2,en_vtc_bsc2,vtc_rdy_bsc3,en_vtc_bsc3,vtc_rdy_bsc4,en_vtc_bsc4,vtc_rdy_bsc5,en_vtc_bsc5,dly_rdy_bsc0,dly_rdy_bsc1,dly_rdy_bsc2,dly_rdy_bsc3,dly_rdy_bsc4,dly_rdy_bsc5,rst_seq_done,shared_pll0_clkoutphy_out,pll0_clkout0,pll0_clkout1,rst,clk,pll0_locked,ch2_out3_P,data_from_fabric_ch2_out3_P[7:0],ch2_out3_N,ch2_out0_P,data_from_fabric_ch2_out0_P[7:0],ch2_out0_N,ch3_out1_P,data_from_fabric_ch3_out1_P[7:0],ch3_out1_N,ch1_out7_P,data_from_fabric_ch1_out7_P[7:0],ch1_out7_N,ch3_out2_P,data_from_fabric_ch3_out2_P[7:0],ch3_out2_N,ch2_out6_P,data_from_fabric_ch2_out6_P[7:0],ch2_out6_N,ch3_out0_P,data_from_fabric_ch3_out0_P[7:0],ch3_out0_N,ch2_out7_P,data_from_fabric_ch2_out7_P[7:0],ch2_out7_N,ch2_out5_P,data_from_fabric_ch2_out5_P[7:0],ch2_out5_N,ch2_out2_P,data_from_fabric_ch2_out2_P[7:0],ch2_out2_N,ch3_out4_P,data_from_fabric_ch3_out4_P[7:0],ch3_out4_N,ch3_out3_P,data_from_fabric_ch3_out3_P[7:0],ch3_out3_N,ch3_out5_P,data_from_fabric_ch3_out5_P[7:0],ch3_out5_N,ch3_out7_P,data_from_fabric_ch3_out7_P[7:0],ch3_out7_N,ch3_out6_P,data_from_fabric_ch3_out6_P[7:0],ch3_out6_N,TH_out_P,data_from_fabric_TH_out_P[7:0],TH_out_N";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9,Vivado 2024.1.1";
begin
end;
