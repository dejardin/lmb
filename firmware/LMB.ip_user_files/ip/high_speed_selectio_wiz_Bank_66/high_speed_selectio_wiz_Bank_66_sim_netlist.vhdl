-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
-- Date        : Sun Jan 26 16:32:53 2025
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim -rename_top high_speed_selectio_wiz_Bank_66 -prefix
--               high_speed_selectio_wiz_Bank_66_ high_speed_selectio_wiz_Bank_66_sim_netlist.vhdl
-- Design      : high_speed_selectio_wiz_Bank_66
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_ctrl_top is
  port (
    core_rdy : out STD_LOGIC;
    in0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    \BITSLICE_CTRL[4].bs_ctrl_inst_0\ : out STD_LOGIC;
    vtc_rdy_bsc4 : out STD_LOGIC;
    vtc_rdy_bsc5 : out STD_LOGIC;
    vtc_rdy_bsc2 : out STD_LOGIC;
    vtc_rdy_bsc3 : out STD_LOGIC;
    vtc_rdy_bsc1 : out STD_LOGIC;
    vtc_rdy_bsc0 : out STD_LOGIC;
    dly_rdy_bsc2 : out STD_LOGIC;
    dly_rdy_bsc3 : out STD_LOGIC;
    dly_rdy_bsc0 : out STD_LOGIC;
    dly_rdy_bsc1 : out STD_LOGIC;
    dly_rdy_bsc5 : out STD_LOGIC;
    dly_rdy_bsc4 : out STD_LOGIC;
    n0_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_rx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_rx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_tx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_tx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_rx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_rx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_tx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_tx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_rx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_rx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_tx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_tx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_rx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_rx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_tx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_tx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_rx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_rx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_tx_bit_ctrl_out2 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_tx_bit_ctrl_out4 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n5_rx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n5_tx_bit_ctrl_out0 : out STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_en_vtc_in : in STD_LOGIC;
    shared_pll0_clkoutphy_out : in STD_LOGIC;
    pll0_clkout1_out : in STD_LOGIC;
    bsctrl_rst : in STD_LOGIC;
    tx_bs0_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs0_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n0_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    n1_en_vtc_in : in STD_LOGIC;
    tx_bs6_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n1_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    n2_en_vtc_in : in STD_LOGIC;
    tx_bs13_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n2_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    n3_en_vtc_in : in STD_LOGIC;
    tx_bs19_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n3_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    n4_en_vtc_in : in STD_LOGIC;
    tx_bs26_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n4_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    n5_en_vtc_in : in STD_LOGIC;
    tx_bs32_rx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_tx_bit_ctrl_out : in STD_LOGIC_VECTOR ( 39 downto 0 );
    n5_tbyte_in : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_ctrl_top;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_ctrl_top is
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_263\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_264\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_265\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_266\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_267\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_268\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_269\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_270\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_271\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_272\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_273\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_274\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_275\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_276\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_277\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_278\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_279\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_280\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_281\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_282\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_283\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_284\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_285\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_286\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_287\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_288\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_289\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_290\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_291\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_292\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_293\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_294\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_295\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_296\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_297\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_298\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_299\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_300\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_301\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_302\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_543\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_544\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_545\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_546\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_547\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_548\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_549\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_550\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_551\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_552\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_553\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_554\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_555\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_556\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_557\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_558\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_559\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_560\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_561\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_562\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_563\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_564\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_565\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_566\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_567\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_568\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_569\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_570\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_571\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_572\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_573\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_574\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_575\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_576\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_577\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_578\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_579\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_580\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_581\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_582\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[0].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[1].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_263\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_264\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_265\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_266\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_267\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_268\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_269\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_270\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_271\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_272\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_273\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_274\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_275\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_276\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_277\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_278\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_279\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_280\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_281\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_282\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_283\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_284\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_285\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_286\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_287\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_288\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_289\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_290\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_291\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_292\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_293\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_294\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_295\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_296\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_297\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_298\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_299\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_300\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_301\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_302\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_543\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_544\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_545\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_546\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_547\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_548\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_549\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_550\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_551\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_552\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_553\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_554\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_555\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_556\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_557\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_558\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_559\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_560\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_561\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_562\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_563\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_564\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_565\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_566\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_567\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_568\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_569\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_570\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_571\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_572\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_573\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_574\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_575\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_576\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_577\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_578\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_579\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_580\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_581\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_582\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[2].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[3].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \^bitslice_ctrl[4].bs_ctrl_inst_0\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_263\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_264\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_265\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_266\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_267\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_268\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_269\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_270\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_271\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_272\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_273\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_274\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_275\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_276\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_277\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_278\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_279\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_280\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_281\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_282\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_283\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_284\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_285\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_286\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_287\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_288\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_289\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_290\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_291\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_292\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_293\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_294\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_295\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_296\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_297\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_298\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_299\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_300\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_301\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_302\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_543\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_544\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_545\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_546\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_547\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_548\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_549\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_550\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_551\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_552\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_553\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_554\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_555\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_556\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_557\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_558\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_559\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_560\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_561\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_562\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_563\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_564\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_565\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_566\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_567\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_568\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_569\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_570\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_571\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_572\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_573\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_574\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_575\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_576\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_577\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_578\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_579\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_580\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_581\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_582\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[4].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_10\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_11\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_12\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_13\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_14\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_15\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_16\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_17\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_18\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_19\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_20\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_21\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_22\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_5\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_7\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_8\ : STD_LOGIC;
  signal \BITSLICE_CTRL[5].bs_ctrl_inst_n_9\ : STD_LOGIC;
  signal \^dly_rdy_bsc0\ : STD_LOGIC;
  signal \^dly_rdy_bsc1\ : STD_LOGIC;
  signal \^dly_rdy_bsc2\ : STD_LOGIC;
  signal \^dly_rdy_bsc3\ : STD_LOGIC;
  signal \^dly_rdy_bsc4\ : STD_LOGIC;
  signal \^dly_rdy_bsc5\ : STD_LOGIC;
  signal \^in0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal n0_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out1 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out3 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out5 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out6 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out_tri : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal nclk_nibble_out : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pclk_nibble_out : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^vtc_rdy_bsc0\ : STD_LOGIC;
  signal \^vtc_rdy_bsc1\ : STD_LOGIC;
  signal \^vtc_rdy_bsc2\ : STD_LOGIC;
  signal \^vtc_rdy_bsc3\ : STD_LOGIC;
  signal \^vtc_rdy_bsc4\ : STD_LOGIC;
  signal \^vtc_rdy_bsc5\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_DYN_DCI_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \BITSLICE_CTRL[0].bs_ctrl_inst\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \BITSLICE_CTRL[1].bs_ctrl_inst\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \BITSLICE_CTRL[2].bs_ctrl_inst\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \BITSLICE_CTRL[3].bs_ctrl_inst\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \BITSLICE_CTRL[4].bs_ctrl_inst\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \BITSLICE_CTRL[5].bs_ctrl_inst\ : label is "PRIMITIVE";
begin
  \BITSLICE_CTRL[4].bs_ctrl_inst_0\ <= \^bitslice_ctrl[4].bs_ctrl_inst_0\;
  dly_rdy_bsc0 <= \^dly_rdy_bsc0\;
  dly_rdy_bsc1 <= \^dly_rdy_bsc1\;
  dly_rdy_bsc2 <= \^dly_rdy_bsc2\;
  dly_rdy_bsc3 <= \^dly_rdy_bsc3\;
  dly_rdy_bsc4 <= \^dly_rdy_bsc4\;
  dly_rdy_bsc5 <= \^dly_rdy_bsc5\;
  in0(0) <= \^in0\(0);
  vtc_rdy_bsc0 <= \^vtc_rdy_bsc0\;
  vtc_rdy_bsc1 <= \^vtc_rdy_bsc1\;
  vtc_rdy_bsc2 <= \^vtc_rdy_bsc2\;
  vtc_rdy_bsc3 <= \^vtc_rdy_bsc3\;
  vtc_rdy_bsc4 <= \^vtc_rdy_bsc4\;
  vtc_rdy_bsc5 <= \^vtc_rdy_bsc5\;
\BITSLICE_CTRL[0].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc0\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[0].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n0_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(1),
      NCLK_NIBBLE_OUT => nclk_nibble_out(0),
      PCLK_NIBBLE_IN => pclk_nibble_out(1),
      PCLK_NIBBLE_OUT => pclk_nibble_out(0),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[0].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs0_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => tx_bs2_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => tx_bs4_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n0_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n0_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n0_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n0_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n0_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n0_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_263\,
      RX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_264\,
      RX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_265\,
      RX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_266\,
      RX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_267\,
      RX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_268\,
      RX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_269\,
      RX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_270\,
      RX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_271\,
      RX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_272\,
      RX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_273\,
      RX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_274\,
      RX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_275\,
      RX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_276\,
      RX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_277\,
      RX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_278\,
      RX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_279\,
      RX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_280\,
      RX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_281\,
      RX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_282\,
      RX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_283\,
      RX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_284\,
      RX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_285\,
      RX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_286\,
      RX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_287\,
      RX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_288\,
      RX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_289\,
      RX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_290\,
      RX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_291\,
      RX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_292\,
      RX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_293\,
      RX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_294\,
      RX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_295\,
      RX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_296\,
      RX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_297\,
      RX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_298\,
      RX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_299\,
      RX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_300\,
      RX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_301\,
      RX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_302\,
      TBYTE_IN(3 downto 0) => n0_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs0_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => tx_bs2_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => tx_bs4_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n0_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n0_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n0_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n0_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n0_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n0_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_543\,
      TX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_544\,
      TX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_545\,
      TX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_546\,
      TX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_547\,
      TX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_548\,
      TX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_549\,
      TX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_550\,
      TX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_551\,
      TX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_552\,
      TX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_553\,
      TX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_554\,
      TX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_555\,
      TX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_556\,
      TX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_557\,
      TX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_558\,
      TX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_559\,
      TX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_560\,
      TX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_561\,
      TX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_562\,
      TX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_563\,
      TX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_564\,
      TX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_565\,
      TX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_566\,
      TX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_567\,
      TX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_568\,
      TX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_569\,
      TX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_570\,
      TX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_571\,
      TX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_572\,
      TX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_573\,
      TX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_574\,
      TX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_575\,
      TX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_576\,
      TX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_577\,
      TX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_578\,
      TX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_579\,
      TX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_580\,
      TX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_581\,
      TX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[0].bs_ctrl_inst_n_582\,
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n0_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc0\
    );
\BITSLICE_CTRL[1].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc1\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[1].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n1_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(0),
      NCLK_NIBBLE_OUT => nclk_nibble_out(1),
      PCLK_NIBBLE_IN => pclk_nibble_out(0),
      PCLK_NIBBLE_OUT => pclk_nibble_out(1),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[1].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[1].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs6_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => tx_bs8_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => tx_bs10_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n1_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n1_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n1_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n1_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n1_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n1_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39 downto 0) => n1_rx_bit_ctrl_out6(39 downto 0),
      TBYTE_IN(3 downto 0) => n1_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs6_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => tx_bs8_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => tx_bs10_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n1_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n1_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n1_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n1_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n1_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n1_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39 downto 0) => n1_tx_bit_ctrl_out6(39 downto 0),
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n1_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc1\
    );
\BITSLICE_CTRL[2].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc2\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[2].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n2_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(3),
      NCLK_NIBBLE_OUT => nclk_nibble_out(2),
      PCLK_NIBBLE_IN => pclk_nibble_out(3),
      PCLK_NIBBLE_OUT => pclk_nibble_out(2),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[2].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs13_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => tx_bs15_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => tx_bs17_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n2_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n2_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n2_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n2_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n2_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n2_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_263\,
      RX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_264\,
      RX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_265\,
      RX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_266\,
      RX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_267\,
      RX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_268\,
      RX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_269\,
      RX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_270\,
      RX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_271\,
      RX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_272\,
      RX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_273\,
      RX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_274\,
      RX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_275\,
      RX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_276\,
      RX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_277\,
      RX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_278\,
      RX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_279\,
      RX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_280\,
      RX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_281\,
      RX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_282\,
      RX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_283\,
      RX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_284\,
      RX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_285\,
      RX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_286\,
      RX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_287\,
      RX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_288\,
      RX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_289\,
      RX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_290\,
      RX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_291\,
      RX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_292\,
      RX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_293\,
      RX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_294\,
      RX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_295\,
      RX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_296\,
      RX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_297\,
      RX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_298\,
      RX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_299\,
      RX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_300\,
      RX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_301\,
      RX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_302\,
      TBYTE_IN(3 downto 0) => n2_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs13_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => tx_bs15_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => tx_bs17_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n2_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n2_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n2_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n2_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n2_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n2_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_543\,
      TX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_544\,
      TX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_545\,
      TX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_546\,
      TX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_547\,
      TX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_548\,
      TX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_549\,
      TX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_550\,
      TX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_551\,
      TX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_552\,
      TX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_553\,
      TX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_554\,
      TX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_555\,
      TX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_556\,
      TX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_557\,
      TX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_558\,
      TX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_559\,
      TX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_560\,
      TX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_561\,
      TX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_562\,
      TX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_563\,
      TX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_564\,
      TX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_565\,
      TX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_566\,
      TX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_567\,
      TX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_568\,
      TX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_569\,
      TX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_570\,
      TX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_571\,
      TX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_572\,
      TX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_573\,
      TX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_574\,
      TX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_575\,
      TX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_576\,
      TX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_577\,
      TX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_578\,
      TX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_579\,
      TX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_580\,
      TX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_581\,
      TX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[2].bs_ctrl_inst_n_582\,
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n2_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc2\
    );
\BITSLICE_CTRL[3].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc3\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[3].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n3_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(2),
      NCLK_NIBBLE_OUT => nclk_nibble_out(3),
      PCLK_NIBBLE_IN => pclk_nibble_out(2),
      PCLK_NIBBLE_OUT => pclk_nibble_out(3),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[3].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[3].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs19_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => tx_bs21_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => tx_bs23_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n3_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n3_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n3_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n3_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n3_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n3_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39 downto 0) => n3_rx_bit_ctrl_out6(39 downto 0),
      TBYTE_IN(3 downto 0) => n3_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs19_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => tx_bs21_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => tx_bs23_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n3_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n3_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n3_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n3_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n3_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n3_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39 downto 0) => n3_tx_bit_ctrl_out6(39 downto 0),
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n3_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc3\
    );
\BITSLICE_CTRL[4].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc4\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[4].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n4_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(5),
      NCLK_NIBBLE_OUT => nclk_nibble_out(4),
      PCLK_NIBBLE_IN => pclk_nibble_out(5),
      PCLK_NIBBLE_OUT => pclk_nibble_out(4),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[4].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs26_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => tx_bs28_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => tx_bs30_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n4_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n4_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n4_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n4_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n4_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n4_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_263\,
      RX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_264\,
      RX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_265\,
      RX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_266\,
      RX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_267\,
      RX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_268\,
      RX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_269\,
      RX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_270\,
      RX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_271\,
      RX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_272\,
      RX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_273\,
      RX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_274\,
      RX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_275\,
      RX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_276\,
      RX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_277\,
      RX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_278\,
      RX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_279\,
      RX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_280\,
      RX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_281\,
      RX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_282\,
      RX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_283\,
      RX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_284\,
      RX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_285\,
      RX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_286\,
      RX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_287\,
      RX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_288\,
      RX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_289\,
      RX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_290\,
      RX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_291\,
      RX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_292\,
      RX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_293\,
      RX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_294\,
      RX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_295\,
      RX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_296\,
      RX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_297\,
      RX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_298\,
      RX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_299\,
      RX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_300\,
      RX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_301\,
      RX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_302\,
      TBYTE_IN(3 downto 0) => n4_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs26_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => tx_bs28_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => tx_bs30_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n4_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n4_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n4_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n4_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n4_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n4_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_543\,
      TX_BIT_CTRL_OUT6(38) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_544\,
      TX_BIT_CTRL_OUT6(37) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_545\,
      TX_BIT_CTRL_OUT6(36) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_546\,
      TX_BIT_CTRL_OUT6(35) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_547\,
      TX_BIT_CTRL_OUT6(34) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_548\,
      TX_BIT_CTRL_OUT6(33) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_549\,
      TX_BIT_CTRL_OUT6(32) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_550\,
      TX_BIT_CTRL_OUT6(31) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_551\,
      TX_BIT_CTRL_OUT6(30) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_552\,
      TX_BIT_CTRL_OUT6(29) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_553\,
      TX_BIT_CTRL_OUT6(28) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_554\,
      TX_BIT_CTRL_OUT6(27) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_555\,
      TX_BIT_CTRL_OUT6(26) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_556\,
      TX_BIT_CTRL_OUT6(25) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_557\,
      TX_BIT_CTRL_OUT6(24) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_558\,
      TX_BIT_CTRL_OUT6(23) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_559\,
      TX_BIT_CTRL_OUT6(22) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_560\,
      TX_BIT_CTRL_OUT6(21) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_561\,
      TX_BIT_CTRL_OUT6(20) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_562\,
      TX_BIT_CTRL_OUT6(19) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_563\,
      TX_BIT_CTRL_OUT6(18) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_564\,
      TX_BIT_CTRL_OUT6(17) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_565\,
      TX_BIT_CTRL_OUT6(16) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_566\,
      TX_BIT_CTRL_OUT6(15) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_567\,
      TX_BIT_CTRL_OUT6(14) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_568\,
      TX_BIT_CTRL_OUT6(13) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_569\,
      TX_BIT_CTRL_OUT6(12) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_570\,
      TX_BIT_CTRL_OUT6(11) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_571\,
      TX_BIT_CTRL_OUT6(10) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_572\,
      TX_BIT_CTRL_OUT6(9) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_573\,
      TX_BIT_CTRL_OUT6(8) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_574\,
      TX_BIT_CTRL_OUT6(7) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_575\,
      TX_BIT_CTRL_OUT6(6) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_576\,
      TX_BIT_CTRL_OUT6(5) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_577\,
      TX_BIT_CTRL_OUT6(4) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_578\,
      TX_BIT_CTRL_OUT6(3) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_579\,
      TX_BIT_CTRL_OUT6(2) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_580\,
      TX_BIT_CTRL_OUT6(1) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_581\,
      TX_BIT_CTRL_OUT6(0) => \BITSLICE_CTRL[4].bs_ctrl_inst_n_582\,
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n4_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc4\
    );
\BITSLICE_CTRL[5].bs_ctrl_inst\: unisim.vcomponents.BITSLICE_CONTROL
    generic map(
      CTRL_CLK => "EXTERNAL",
      DIV_MODE => "DIV4",
      EN_CLK_TO_EXT_NORTH => "DISABLE",
      EN_CLK_TO_EXT_SOUTH => "DISABLE",
      EN_DYN_ODLY_MODE => "FALSE",
      EN_OTHER_NCLK => "FALSE",
      EN_OTHER_PCLK => "FALSE",
      IDLY_VT_TRACK => "TRUE",
      INV_RXCLK => "FALSE",
      ODLY_VT_TRACK => "TRUE",
      QDLY_VT_TRACK => "TRUE",
      READ_IDLE_COUNT => B"00" & X"0",
      REFCLK_SRC => "PLLCLK",
      ROUNDING_FACTOR => 16,
      RXGATE_EXTEND => "FALSE",
      RX_CLK_PHASE_N => "SHIFT_0",
      RX_CLK_PHASE_P => "SHIFT_0",
      RX_GATING => "DISABLE",
      SELF_CALIBRATE => "ENABLE",
      SERIAL_MODE => "FALSE",
      SIM_DEVICE => "ULTRASCALE",
      SIM_SPEEDUP => "FAST",
      SIM_VERSION => 1.000000,
      TX_GATING => "ENABLE"
    )
        port map (
      CLK_FROM_EXT => '1',
      CLK_TO_EXT_NORTH => \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_CLK_TO_EXT_NORTH_UNCONNECTED\,
      CLK_TO_EXT_SOUTH => \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_CLK_TO_EXT_SOUTH_UNCONNECTED\,
      DLY_RDY => \^dly_rdy_bsc5\,
      DYN_DCI(6 downto 0) => \NLW_BITSLICE_CTRL[5].bs_ctrl_inst_DYN_DCI_UNCONNECTED\(6 downto 0),
      EN_VTC => n5_en_vtc_in,
      NCLK_NIBBLE_IN => nclk_nibble_out(4),
      NCLK_NIBBLE_OUT => nclk_nibble_out(5),
      PCLK_NIBBLE_IN => pclk_nibble_out(4),
      PCLK_NIBBLE_OUT => pclk_nibble_out(5),
      PHY_RDCS0(3 downto 0) => B"0000",
      PHY_RDCS1(3 downto 0) => B"0000",
      PHY_RDEN(3 downto 0) => B"0000",
      PHY_WRCS0(3 downto 0) => B"0000",
      PHY_WRCS1(3 downto 0) => B"0000",
      PLL_CLK => shared_pll0_clkoutphy_out,
      REFCLK => '0',
      RIU_ADDR(5 downto 0) => B"000000",
      RIU_CLK => pll0_clkout1_out,
      RIU_NIBBLE_SEL => '0',
      RIU_RD_DATA(15) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_7\,
      RIU_RD_DATA(14) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_8\,
      RIU_RD_DATA(13) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_9\,
      RIU_RD_DATA(12) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_10\,
      RIU_RD_DATA(11) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_11\,
      RIU_RD_DATA(10) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_12\,
      RIU_RD_DATA(9) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_13\,
      RIU_RD_DATA(8) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_14\,
      RIU_RD_DATA(7) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_15\,
      RIU_RD_DATA(6) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_16\,
      RIU_RD_DATA(5) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_17\,
      RIU_RD_DATA(4) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_18\,
      RIU_RD_DATA(3) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_19\,
      RIU_RD_DATA(2) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_20\,
      RIU_RD_DATA(1) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_21\,
      RIU_RD_DATA(0) => \BITSLICE_CTRL[5].bs_ctrl_inst_n_22\,
      RIU_VALID => \BITSLICE_CTRL[5].bs_ctrl_inst_n_5\,
      RIU_WR_DATA(15 downto 0) => B"0000000000000000",
      RIU_WR_EN => '0',
      RST => bsctrl_rst,
      RX_BIT_CTRL_IN0(39 downto 0) => tx_bs32_rx_bit_ctrl_out(39 downto 0),
      RX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN2(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN4(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      RX_BIT_CTRL_OUT0(39 downto 0) => n5_rx_bit_ctrl_out0(39 downto 0),
      RX_BIT_CTRL_OUT1(39 downto 0) => n5_rx_bit_ctrl_out1(39 downto 0),
      RX_BIT_CTRL_OUT2(39 downto 0) => n5_rx_bit_ctrl_out2(39 downto 0),
      RX_BIT_CTRL_OUT3(39 downto 0) => n5_rx_bit_ctrl_out3(39 downto 0),
      RX_BIT_CTRL_OUT4(39 downto 0) => n5_rx_bit_ctrl_out4(39 downto 0),
      RX_BIT_CTRL_OUT5(39 downto 0) => n5_rx_bit_ctrl_out5(39 downto 0),
      RX_BIT_CTRL_OUT6(39 downto 0) => n5_rx_bit_ctrl_out6(39 downto 0),
      TBYTE_IN(3 downto 0) => n5_tbyte_in(3 downto 0),
      TX_BIT_CTRL_IN0(39 downto 0) => tx_bs32_tx_bit_ctrl_out(39 downto 0),
      TX_BIT_CTRL_IN1(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN2(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN3(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN4(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN5(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN6(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_IN_TRI(39 downto 0) => B"0000000000000000000000000000000000000000",
      TX_BIT_CTRL_OUT0(39 downto 0) => n5_tx_bit_ctrl_out0(39 downto 0),
      TX_BIT_CTRL_OUT1(39 downto 0) => n5_tx_bit_ctrl_out1(39 downto 0),
      TX_BIT_CTRL_OUT2(39 downto 0) => n5_tx_bit_ctrl_out2(39 downto 0),
      TX_BIT_CTRL_OUT3(39 downto 0) => n5_tx_bit_ctrl_out3(39 downto 0),
      TX_BIT_CTRL_OUT4(39 downto 0) => n5_tx_bit_ctrl_out4(39 downto 0),
      TX_BIT_CTRL_OUT5(39 downto 0) => n5_tx_bit_ctrl_out5(39 downto 0),
      TX_BIT_CTRL_OUT6(39 downto 0) => n5_tx_bit_ctrl_out6(39 downto 0),
      TX_BIT_CTRL_OUT_TRI(39 downto 0) => n5_tx_bit_ctrl_out_tri(39 downto 0),
      VTC_RDY => \^vtc_rdy_bsc5\
    );
\CORE_RDY_GEN[0].core_rdy_r[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^in0\(0),
      I1 => \^bitslice_ctrl[4].bs_ctrl_inst_0\,
      O => core_rdy
    );
\GEN_RIU_FROM_PLL.hssio_state[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^vtc_rdy_bsc4\,
      I1 => \^vtc_rdy_bsc5\,
      I2 => \^vtc_rdy_bsc2\,
      I3 => \^vtc_rdy_bsc3\,
      I4 => \^vtc_rdy_bsc1\,
      I5 => \^vtc_rdy_bsc0\,
      O => \^bitslice_ctrl[4].bs_ctrl_inst_0\
    );
src_data_inferred_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^dly_rdy_bsc2\,
      I1 => \^dly_rdy_bsc3\,
      I2 => \^dly_rdy_bsc0\,
      I3 => \^dly_rdy_bsc1\,
      I4 => \^dly_rdy_bsc5\,
      I5 => \^dly_rdy_bsc4\,
      O => \^in0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_scheme is
  port (
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_locked_out : out STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    pll0_clkout0_out : out STD_LOGIC;
    pll0_clkout1_out : out STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC;
    pll0_clkoutphy_en_in : in STD_LOGIC
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_scheme;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_scheme is
  signal \GEN_PLL_IN_IP_US.pll0_clkout0\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_10\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_11\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_12\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_13\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_14\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_15\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_16\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_17\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_18\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_19\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_20\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_21\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_22\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_23\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_6\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_8\ : STD_LOGIC;
  signal \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_9\ : STD_LOGIC;
  signal pll0_clkout1 : STD_LOGIC;
  signal \^pll0_locked_out\ : STD_LOGIC;
  signal \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKFBIN_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKFBOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKOUT0B_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKOUT1B_UNCONNECTED\ : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \GEN_PLL_IN_IP_US.pll0_clkout0_buf\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \GEN_PLL_IN_IP_US.pll0_clkout1_buf\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst\ : label is "PRIMITIVE";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst\ : label is "MLO";
begin
  pll0_locked_out <= \^pll0_locked_out\;
\GEN_PLL_IN_IP_US.pll0_clkout0_buf\: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => \^pll0_locked_out\,
      I => \GEN_PLL_IN_IP_US.pll0_clkout0\,
      O => pll0_clkout0_out
    );
\GEN_PLL_IN_IP_US.pll0_clkout1_buf\: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => \^pll0_locked_out\,
      I => pll0_clkout1,
      O => pll0_clkout1_out
    );
\GEN_PLL_IN_IP_US.plle3_adv_pll0_inst\: unisim.vcomponents.PLLE3_ADV
    generic map(
      CLKFBOUT_MULT => 8,
      CLKFBOUT_PHASE => 0.000000,
      CLKIN_PERIOD => 12.500000,
      CLKOUT0_DIVIDE => 4,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT1_DIVIDE => 4,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUTPHY_MODE => "VCO_2X",
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKFBIN_INVERTED => '0',
      IS_CLKIN_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER => 0.010000,
      STARTUP_WAIT => "FALSE"
    )
        port map (
      CLKFBIN => \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKFBIN_UNCONNECTED\,
      CLKFBOUT => \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKFBOUT_UNCONNECTED\,
      CLKIN => clk,
      CLKOUT0 => \GEN_PLL_IN_IP_US.pll0_clkout0\,
      CLKOUT0B => \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKOUT0B_UNCONNECTED\,
      CLKOUT1 => pll0_clkout1,
      CLKOUT1B => \NLW_GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_CLKOUT1B_UNCONNECTED\,
      CLKOUTPHY => shared_pll0_clkoutphy_out,
      CLKOUTPHYEN => pll0_clkoutphy_en_in,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_8\,
      DO(14) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_9\,
      DO(13) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_10\,
      DO(12) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_11\,
      DO(11) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_12\,
      DO(10) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_13\,
      DO(9) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_14\,
      DO(8) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_15\,
      DO(7) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_16\,
      DO(6) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_17\,
      DO(5) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_18\,
      DO(4) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_19\,
      DO(3) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_20\,
      DO(2) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_21\,
      DO(1) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_22\,
      DO(0) => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_23\,
      DRDY => \GEN_PLL_IN_IP_US.plle3_adv_pll0_inst_n_6\,
      DWE => '0',
      LOCKED => \^pll0_locked_out\,
      PWRDWN => '0',
      RST => rst
    );
\GEN_RIU_FROM_PLL.rst_seq_done_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rst,
      I1 => \^pll0_locked_out\,
      O => AR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_iobuf_top is
  port (
    data_to_pins : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bs_to_buf_data_in : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_iobuf_top;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_iobuf_top is
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \io_gen[0].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of \io_gen[0].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \io_gen[0].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[10].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[10].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[10].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[13].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[13].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[13].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[15].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[15].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[15].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[17].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[17].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[17].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[19].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[19].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[19].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[21].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[21].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[21].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[23].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[23].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[23].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[26].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[26].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[26].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[28].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[28].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[28].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[2].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[2].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[2].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[30].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[30].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[30].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[32].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[32].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[32].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[4].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[4].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[4].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[6].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[6].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[6].obufds_inst\ : label is "OBUFDS";
  attribute BOX_TYPE of \io_gen[8].obufds_inst\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \io_gen[8].obufds_inst\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of \io_gen[8].obufds_inst\ : label is "OBUFDS";
begin
\io_gen[0].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(0),
      O => data_to_pins(0),
      OB => data_to_pins(1)
    );
\io_gen[10].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(5),
      O => data_to_pins(10),
      OB => data_to_pins(11)
    );
\io_gen[13].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(6),
      O => data_to_pins(12),
      OB => data_to_pins(13)
    );
\io_gen[15].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(7),
      O => data_to_pins(14),
      OB => data_to_pins(15)
    );
\io_gen[17].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(8),
      O => data_to_pins(16),
      OB => data_to_pins(17)
    );
\io_gen[19].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(9),
      O => data_to_pins(18),
      OB => data_to_pins(19)
    );
\io_gen[21].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(10),
      O => data_to_pins(20),
      OB => data_to_pins(21)
    );
\io_gen[23].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(11),
      O => data_to_pins(22),
      OB => data_to_pins(23)
    );
\io_gen[26].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(12),
      O => data_to_pins(24),
      OB => data_to_pins(25)
    );
\io_gen[28].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(13),
      O => data_to_pins(26),
      OB => data_to_pins(27)
    );
\io_gen[2].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(1),
      O => data_to_pins(2),
      OB => data_to_pins(3)
    );
\io_gen[30].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(14),
      O => data_to_pins(28),
      OB => data_to_pins(29)
    );
\io_gen[32].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(15),
      O => data_to_pins(30),
      OB => data_to_pins(31)
    );
\io_gen[4].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(2),
      O => data_to_pins(4),
      OB => data_to_pins(5)
    );
\io_gen[6].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(3),
      O => data_to_pins(6),
      OB => data_to_pins(7)
    );
\io_gen[8].obufds_inst\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => bs_to_buf_data_in(4),
      O => data_to_pins(8),
      OB => data_to_pins(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell is
  port (
    \sync_flop_1_reg[0]_0\ : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
  \sync_flop_1_reg[0]_0\ <= sync_flop_1;
\GEN_NIB0_TBYTE.n0_tbyte_d[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sync_flop_1,
      O => SR(0)
    );
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \out\,
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_0 is
  port (
    SS : out STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_clkout1_out : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_0 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_0;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_0 is
  signal n_0_0 : STD_LOGIC;
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
  SS(0) <= sync_flop_1;
i_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => n_0_0
    );
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => n_0_0,
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_1 is
  port (
    D : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \GEN_RIU_FROM_PLL.hssio_state_reg[6]\ : out STD_LOGIC;
    in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_clkout1_out : in STD_LOGIC;
    \GEN_RIU_FROM_PLL.hssio_state_reg[7]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \GEN_RIU_FROM_PLL.hssio_state_reg[8]\ : in STD_LOGIC;
    \GEN_RIU_FROM_PLL.hssio_state_reg[0]\ : in STD_LOGIC;
    pll0_clkoutphy_en : in STD_LOGIC;
    \GEN_RIU_FROM_PLL.hssio_state_reg[5]\ : in STD_LOGIC;
    \GEN_RIU_FROM_PLL.bsc_en_vtc_reg\ : in STD_LOGIC;
    pll0_clkout1_stable : in STD_LOGIC;
    bsc_en_vtc : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_1 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_1;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_1 is
  signal \GEN_RIU_FROM_PLL.bsc_en_vtc_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[8]_i_4_n_0\ : STD_LOGIC;
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.bsc_en_vtc_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[7]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[8]_i_4\ : label is "soft_lutpair1";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\GEN_RIU_FROM_PLL.bsc_en_vtc_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BABB8A88"
    )
        port map (
      I0 => Q(4),
      I1 => \GEN_RIU_FROM_PLL.bsc_en_vtc_i_2_n_0\,
      I2 => \GEN_RIU_FROM_PLL.bsc_en_vtc_reg\,
      I3 => pll0_clkout1_stable,
      I4 => bsc_en_vtc,
      O => \GEN_RIU_FROM_PLL.hssio_state_reg[6]\
    );
\GEN_RIU_FROM_PLL.bsc_en_vtc_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => Q(3),
      I1 => Q(4),
      I2 => Q(5),
      I3 => \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\,
      O => \GEN_RIU_FROM_PLL.bsc_en_vtc_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFEFEEB"
    )
        port map (
      I0 => \GEN_RIU_FROM_PLL.hssio_state_reg[0]\,
      I1 => Q(0),
      I2 => \GEN_RIU_FROM_PLL.hssio_state_reg[8]\,
      I3 => Q(6),
      I4 => Q(2),
      I5 => \GEN_RIU_FROM_PLL.hssio_state[0]_i_3_n_0\,
      O => D(0)
    );
\GEN_RIU_FROM_PLL.hssio_state[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8F8F8F88"
    )
        port map (
      I0 => Q(6),
      I1 => Q(2),
      I2 => sync_flop_1,
      I3 => Q(4),
      I4 => Q(5),
      O => \GEN_RIU_FROM_PLL.hssio_state[0]_i_3_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAEAA"
    )
        port map (
      I0 => pll0_clkoutphy_en,
      I1 => \GEN_RIU_FROM_PLL.hssio_state_reg[5]\,
      I2 => sync_flop_1,
      I3 => Q(3),
      I4 => Q(5),
      I5 => Q(4),
      O => D(1)
    );
\GEN_RIU_FROM_PLL.hssio_state[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => Q(4),
      I1 => Q(5),
      I2 => Q(3),
      I3 => \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\,
      O => D(2)
    );
\GEN_RIU_FROM_PLL.hssio_state[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000C80"
    )
        port map (
      I0 => \GEN_RIU_FROM_PLL.hssio_state_reg[7]\,
      I1 => \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\,
      I2 => Q(5),
      I3 => Q(4),
      I4 => Q(3),
      O => D(3)
    );
\GEN_RIU_FROM_PLL.hssio_state[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => Q(1),
      I1 => Q(6),
      I2 => Q(2),
      I3 => Q(0),
      I4 => sync_flop_1,
      O => \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010FFFF00100010"
    )
        port map (
      I0 => \GEN_RIU_FROM_PLL.hssio_state_reg[8]\,
      I1 => Q(0),
      I2 => Q(6),
      I3 => Q(2),
      I4 => \GEN_RIU_FROM_PLL.hssio_state_reg[7]\,
      I5 => \GEN_RIU_FROM_PLL.hssio_state[8]_i_4_n_0\,
      O => D(4)
    );
\GEN_RIU_FROM_PLL.hssio_state[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => Q(3),
      I1 => Q(4),
      I2 => Q(5),
      I3 => \GEN_RIU_FROM_PLL.hssio_state[7]_i_2_n_0\,
      O => \GEN_RIU_FROM_PLL.hssio_state[8]_i_4_n_0\
    );
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => in0(0),
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_2 is
  port (
    multi_intf_lock_in : in STD_LOGIC;
    pll0_clkout1_out : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_2 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_2;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_2 is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => multi_intf_lock_in,
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_3 is
  port (
    pll0_locked_out : in STD_LOGIC;
    pll0_clkout1_out : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_3 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_3;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_3 is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => pll0_locked_out,
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_4 is
  port (
    rst : in STD_LOGIC;
    pll0_clkout1_out : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_4 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_4;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_4 is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => rst,
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_5 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_clkout0_out : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \pll0_fab_clk_cntr_reg[6]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_5 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_5;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_5 is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\pll0_fab_clk_cntr[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"5D"
    )
        port map (
      I0 => sync_flop_1,
      I1 => \out\(0),
      I2 => \pll0_fab_clk_cntr_reg[6]\,
      O => SR(0)
    );
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => in0(0),
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_6 is
  port (
    in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_clkout1_out : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_6 : entity is "high_speed_selectio_wiz_v3_6_9_sync_cell";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_6;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_6 is
  signal sync_flop_0 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of sync_flop_0 : signal is "true";
  signal sync_flop_1 : STD_LOGIC;
  attribute async_reg of sync_flop_1 : signal is "true";
  signal sync_flop_2 : STD_LOGIC;
  attribute async_reg of sync_flop_2 : signal is "true";
  signal sync_flop_3 : STD_LOGIC;
  attribute async_reg of sync_flop_3 : signal is "true";
  signal sync_flop_4 : STD_LOGIC;
  attribute async_reg of sync_flop_4 : signal is "true";
  signal sync_flop_5 : STD_LOGIC;
  attribute async_reg of sync_flop_5 : signal is "true";
  signal sync_flop_6 : STD_LOGIC;
  attribute async_reg of sync_flop_6 : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sync_flop_0_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sync_flop_0_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_1_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sync_flop_6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \sync_flop_6_reg[0]\ : label is "yes";
begin
\sync_flop_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => in0(0),
      Q => sync_flop_0,
      R => '0'
    );
\sync_flop_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_0,
      Q => sync_flop_1,
      R => '0'
    );
\sync_flop_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_1,
      Q => sync_flop_2,
      R => '0'
    );
\sync_flop_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_2,
      Q => sync_flop_3,
      R => '0'
    );
\sync_flop_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_3,
      Q => sync_flop_4,
      R => '0'
    );
\sync_flop_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_4,
      Q => sync_flop_5,
      R => '0'
    );
\sync_flop_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => sync_flop_5,
      Q => sync_flop_6,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_tx_bs is
  port (
    bs_to_buf_data_in : out STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs0_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs0_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs_rst_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs_rst_dly_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs0_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs0_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs2_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs4_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs6_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs8_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs10_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs13_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs15_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs17_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs19_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs21_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs23_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs26_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs28_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs30_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs32_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_tx_bs;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_tx_bs is
  signal bs_to_buf_t : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal \NLW_TX_BS[0].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[10].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[13].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[15].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[17].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[19].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[21].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[23].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[26].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[28].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[2].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[30].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[32].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[4].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[6].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_TX_BS[8].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \TX_BS[0].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[10].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[13].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[15].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[17].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[19].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[21].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[23].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[26].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[28].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[2].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[30].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[32].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[4].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[6].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
  attribute BOX_TYPE of \TX_BS[8].u_tx_bitslice_if_bs\ : label is "PRIMITIVE";
begin
\TX_BS[0].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[0].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out3_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(0),
      RST => tx_bs_rst_in(0),
      RST_DLY => tx_bs_rst_dly_in(0),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs0_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs0_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs0_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs0_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(0)
    );
\TX_BS[10].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[10].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out6_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(5),
      RST => tx_bs_rst_in(5),
      RST_DLY => tx_bs_rst_dly_in(5),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs10_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs10_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs10_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs10_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(10)
    );
\TX_BS[13].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[13].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out0_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(6),
      RST => tx_bs_rst_in(6),
      RST_DLY => tx_bs_rst_dly_in(6),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs13_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs13_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs13_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs13_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(13)
    );
\TX_BS[15].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[15].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out7_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(7),
      RST => tx_bs_rst_in(7),
      RST_DLY => tx_bs_rst_dly_in(7),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs15_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs15_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs15_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs15_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(15)
    );
\TX_BS[17].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[17].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out5_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(8),
      RST => tx_bs_rst_in(8),
      RST_DLY => tx_bs_rst_dly_in(8),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs17_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs17_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs17_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs17_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(17)
    );
\TX_BS[19].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[19].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out2_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(9),
      RST => tx_bs_rst_in(9),
      RST_DLY => tx_bs_rst_dly_in(9),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs19_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs19_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs19_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs19_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(19)
    );
\TX_BS[21].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[21].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out4_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(10),
      RST => tx_bs_rst_in(10),
      RST_DLY => tx_bs_rst_dly_in(10),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs21_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs21_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs21_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs21_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(21)
    );
\TX_BS[23].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[23].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out3_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(11),
      RST => tx_bs_rst_in(11),
      RST_DLY => tx_bs_rst_dly_in(11),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs23_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs23_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs23_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs23_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(23)
    );
\TX_BS[26].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[26].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out5_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(12),
      RST => tx_bs_rst_in(12),
      RST_DLY => tx_bs_rst_dly_in(12),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs26_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs26_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs26_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs26_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(26)
    );
\TX_BS[28].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[28].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out7_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(13),
      RST => tx_bs_rst_in(13),
      RST_DLY => tx_bs_rst_dly_in(13),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs28_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs28_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs28_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs28_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(28)
    );
\TX_BS[2].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[2].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch2_out0_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(1),
      RST => tx_bs_rst_in(1),
      RST_DLY => tx_bs_rst_dly_in(1),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs2_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs2_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs2_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs2_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(2)
    );
\TX_BS[30].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[30].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out6_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(14),
      RST => tx_bs_rst_in(14),
      RST_DLY => tx_bs_rst_dly_in(14),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs30_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs30_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs30_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs30_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(30)
    );
\TX_BS[32].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[32].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_TH_out_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(15),
      RST => tx_bs_rst_in(15),
      RST_DLY => tx_bs_rst_dly_in(15),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs32_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs32_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs32_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs32_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(32)
    );
\TX_BS[4].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[4].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out1_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(2),
      RST => tx_bs_rst_in(2),
      RST_DLY => tx_bs_rst_dly_in(2),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs4_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs4_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs4_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs4_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(4)
    );
\TX_BS[6].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[6].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch1_out7_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(3),
      RST => tx_bs_rst_in(3),
      RST_DLY => tx_bs_rst_dly_in(3),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs6_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs6_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs6_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs6_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(6)
    );
\TX_BS[8].u_tx_bitslice_if_bs\: unisim.vcomponents.TX_BITSLICE
    generic map(
      DATA_WIDTH => 8,
      DELAY_FORMAT => "TIME",
      DELAY_TYPE => "FIXED",
      DELAY_VALUE => 0,
      ENABLE_PRE_EMPHASIS => "FALSE",
      INIT => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_DLY_INVERTED => '0',
      IS_RST_INVERTED => '0',
      NATIVE_ODELAY_BYPASS => "FALSE",
      OUTPUT_PHASE_90 => "FALSE",
      REFCLK_FREQUENCY => 1280.000000,
      SIM_DEVICE => "ULTRASCALE",
      SIM_VERSION => 1.000000,
      TBYTE_CTL => "T",
      UPDATE_MODE => "ASYNC"
    )
        port map (
      CE => '0',
      CLK => '0',
      CNTVALUEIN(8 downto 0) => B"000000000",
      CNTVALUEOUT(8 downto 0) => \NLW_TX_BS[8].u_tx_bitslice_if_bs_CNTVALUEOUT_UNCONNECTED\(8 downto 0),
      D(7 downto 0) => data_from_fabric_ch3_out2_P(7 downto 0),
      EN_VTC => '1',
      INC => '0',
      LOAD => '0',
      O => bs_to_buf_data_in(4),
      RST => tx_bs_rst_in(4),
      RST_DLY => tx_bs_rst_dly_in(4),
      RX_BIT_CTRL_IN(39 downto 0) => tx_bs8_rx_bit_ctrl_in(39 downto 0),
      RX_BIT_CTRL_OUT(39 downto 0) => tx_bs8_rx_bit_ctrl_out(39 downto 0),
      T => '0',
      TBYTE_IN => '0',
      TX_BIT_CTRL_IN(39 downto 0) => tx_bs8_tx_bit_ctrl_in(39 downto 0),
      TX_BIT_CTRL_OUT(39 downto 0) => tx_bs8_tx_bit_ctrl_out(39 downto 0),
      T_OUT => bs_to_buf_t(8)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_top is
  port (
    bs_to_buf_data_in : out STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs0_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs0_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_rx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_tx_bit_ctrl_out : out STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs_rst_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs_rst_dly_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    tx_bs0_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs0_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs2_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs2_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs4_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs4_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs6_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs6_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs8_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs8_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs10_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs10_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs13_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs13_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs15_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs15_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs17_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs17_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs19_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs19_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs21_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs21_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs23_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs23_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs26_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs26_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs28_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs28_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs30_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs30_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tx_bs32_rx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    tx_bs32_tx_bit_ctrl_in : in STD_LOGIC_VECTOR ( 39 downto 0 );
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_top;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_top is
begin
u_tx_bs: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_tx_bs
     port map (
      bs_to_buf_data_in(15 downto 0) => bs_to_buf_data_in(15 downto 0),
      data_from_fabric_TH_out_P(7 downto 0) => data_from_fabric_TH_out_P(7 downto 0),
      data_from_fabric_ch1_out7_P(7 downto 0) => data_from_fabric_ch1_out7_P(7 downto 0),
      data_from_fabric_ch2_out0_P(7 downto 0) => data_from_fabric_ch2_out0_P(7 downto 0),
      data_from_fabric_ch2_out2_P(7 downto 0) => data_from_fabric_ch2_out2_P(7 downto 0),
      data_from_fabric_ch2_out3_P(7 downto 0) => data_from_fabric_ch2_out3_P(7 downto 0),
      data_from_fabric_ch2_out5_P(7 downto 0) => data_from_fabric_ch2_out5_P(7 downto 0),
      data_from_fabric_ch2_out6_P(7 downto 0) => data_from_fabric_ch2_out6_P(7 downto 0),
      data_from_fabric_ch2_out7_P(7 downto 0) => data_from_fabric_ch2_out7_P(7 downto 0),
      data_from_fabric_ch3_out0_P(7 downto 0) => data_from_fabric_ch3_out0_P(7 downto 0),
      data_from_fabric_ch3_out1_P(7 downto 0) => data_from_fabric_ch3_out1_P(7 downto 0),
      data_from_fabric_ch3_out2_P(7 downto 0) => data_from_fabric_ch3_out2_P(7 downto 0),
      data_from_fabric_ch3_out3_P(7 downto 0) => data_from_fabric_ch3_out3_P(7 downto 0),
      data_from_fabric_ch3_out4_P(7 downto 0) => data_from_fabric_ch3_out4_P(7 downto 0),
      data_from_fabric_ch3_out5_P(7 downto 0) => data_from_fabric_ch3_out5_P(7 downto 0),
      data_from_fabric_ch3_out6_P(7 downto 0) => data_from_fabric_ch3_out6_P(7 downto 0),
      data_from_fabric_ch3_out7_P(7 downto 0) => data_from_fabric_ch3_out7_P(7 downto 0),
      tx_bs0_rx_bit_ctrl_in(39 downto 0) => tx_bs0_rx_bit_ctrl_in(39 downto 0),
      tx_bs0_rx_bit_ctrl_out(39 downto 0) => tx_bs0_rx_bit_ctrl_out(39 downto 0),
      tx_bs0_tx_bit_ctrl_in(39 downto 0) => tx_bs0_tx_bit_ctrl_in(39 downto 0),
      tx_bs0_tx_bit_ctrl_out(39 downto 0) => tx_bs0_tx_bit_ctrl_out(39 downto 0),
      tx_bs10_rx_bit_ctrl_in(39 downto 0) => tx_bs10_rx_bit_ctrl_in(39 downto 0),
      tx_bs10_rx_bit_ctrl_out(39 downto 0) => tx_bs10_rx_bit_ctrl_out(39 downto 0),
      tx_bs10_tx_bit_ctrl_in(39 downto 0) => tx_bs10_tx_bit_ctrl_in(39 downto 0),
      tx_bs10_tx_bit_ctrl_out(39 downto 0) => tx_bs10_tx_bit_ctrl_out(39 downto 0),
      tx_bs13_rx_bit_ctrl_in(39 downto 0) => tx_bs13_rx_bit_ctrl_in(39 downto 0),
      tx_bs13_rx_bit_ctrl_out(39 downto 0) => tx_bs13_rx_bit_ctrl_out(39 downto 0),
      tx_bs13_tx_bit_ctrl_in(39 downto 0) => tx_bs13_tx_bit_ctrl_in(39 downto 0),
      tx_bs13_tx_bit_ctrl_out(39 downto 0) => tx_bs13_tx_bit_ctrl_out(39 downto 0),
      tx_bs15_rx_bit_ctrl_in(39 downto 0) => tx_bs15_rx_bit_ctrl_in(39 downto 0),
      tx_bs15_rx_bit_ctrl_out(39 downto 0) => tx_bs15_rx_bit_ctrl_out(39 downto 0),
      tx_bs15_tx_bit_ctrl_in(39 downto 0) => tx_bs15_tx_bit_ctrl_in(39 downto 0),
      tx_bs15_tx_bit_ctrl_out(39 downto 0) => tx_bs15_tx_bit_ctrl_out(39 downto 0),
      tx_bs17_rx_bit_ctrl_in(39 downto 0) => tx_bs17_rx_bit_ctrl_in(39 downto 0),
      tx_bs17_rx_bit_ctrl_out(39 downto 0) => tx_bs17_rx_bit_ctrl_out(39 downto 0),
      tx_bs17_tx_bit_ctrl_in(39 downto 0) => tx_bs17_tx_bit_ctrl_in(39 downto 0),
      tx_bs17_tx_bit_ctrl_out(39 downto 0) => tx_bs17_tx_bit_ctrl_out(39 downto 0),
      tx_bs19_rx_bit_ctrl_in(39 downto 0) => tx_bs19_rx_bit_ctrl_in(39 downto 0),
      tx_bs19_rx_bit_ctrl_out(39 downto 0) => tx_bs19_rx_bit_ctrl_out(39 downto 0),
      tx_bs19_tx_bit_ctrl_in(39 downto 0) => tx_bs19_tx_bit_ctrl_in(39 downto 0),
      tx_bs19_tx_bit_ctrl_out(39 downto 0) => tx_bs19_tx_bit_ctrl_out(39 downto 0),
      tx_bs21_rx_bit_ctrl_in(39 downto 0) => tx_bs21_rx_bit_ctrl_in(39 downto 0),
      tx_bs21_rx_bit_ctrl_out(39 downto 0) => tx_bs21_rx_bit_ctrl_out(39 downto 0),
      tx_bs21_tx_bit_ctrl_in(39 downto 0) => tx_bs21_tx_bit_ctrl_in(39 downto 0),
      tx_bs21_tx_bit_ctrl_out(39 downto 0) => tx_bs21_tx_bit_ctrl_out(39 downto 0),
      tx_bs23_rx_bit_ctrl_in(39 downto 0) => tx_bs23_rx_bit_ctrl_in(39 downto 0),
      tx_bs23_rx_bit_ctrl_out(39 downto 0) => tx_bs23_rx_bit_ctrl_out(39 downto 0),
      tx_bs23_tx_bit_ctrl_in(39 downto 0) => tx_bs23_tx_bit_ctrl_in(39 downto 0),
      tx_bs23_tx_bit_ctrl_out(39 downto 0) => tx_bs23_tx_bit_ctrl_out(39 downto 0),
      tx_bs26_rx_bit_ctrl_in(39 downto 0) => tx_bs26_rx_bit_ctrl_in(39 downto 0),
      tx_bs26_rx_bit_ctrl_out(39 downto 0) => tx_bs26_rx_bit_ctrl_out(39 downto 0),
      tx_bs26_tx_bit_ctrl_in(39 downto 0) => tx_bs26_tx_bit_ctrl_in(39 downto 0),
      tx_bs26_tx_bit_ctrl_out(39 downto 0) => tx_bs26_tx_bit_ctrl_out(39 downto 0),
      tx_bs28_rx_bit_ctrl_in(39 downto 0) => tx_bs28_rx_bit_ctrl_in(39 downto 0),
      tx_bs28_rx_bit_ctrl_out(39 downto 0) => tx_bs28_rx_bit_ctrl_out(39 downto 0),
      tx_bs28_tx_bit_ctrl_in(39 downto 0) => tx_bs28_tx_bit_ctrl_in(39 downto 0),
      tx_bs28_tx_bit_ctrl_out(39 downto 0) => tx_bs28_tx_bit_ctrl_out(39 downto 0),
      tx_bs2_rx_bit_ctrl_in(39 downto 0) => tx_bs2_rx_bit_ctrl_in(39 downto 0),
      tx_bs2_rx_bit_ctrl_out(39 downto 0) => tx_bs2_rx_bit_ctrl_out(39 downto 0),
      tx_bs2_tx_bit_ctrl_in(39 downto 0) => tx_bs2_tx_bit_ctrl_in(39 downto 0),
      tx_bs2_tx_bit_ctrl_out(39 downto 0) => tx_bs2_tx_bit_ctrl_out(39 downto 0),
      tx_bs30_rx_bit_ctrl_in(39 downto 0) => tx_bs30_rx_bit_ctrl_in(39 downto 0),
      tx_bs30_rx_bit_ctrl_out(39 downto 0) => tx_bs30_rx_bit_ctrl_out(39 downto 0),
      tx_bs30_tx_bit_ctrl_in(39 downto 0) => tx_bs30_tx_bit_ctrl_in(39 downto 0),
      tx_bs30_tx_bit_ctrl_out(39 downto 0) => tx_bs30_tx_bit_ctrl_out(39 downto 0),
      tx_bs32_rx_bit_ctrl_in(39 downto 0) => tx_bs32_rx_bit_ctrl_in(39 downto 0),
      tx_bs32_rx_bit_ctrl_out(39 downto 0) => tx_bs32_rx_bit_ctrl_out(39 downto 0),
      tx_bs32_tx_bit_ctrl_in(39 downto 0) => tx_bs32_tx_bit_ctrl_in(39 downto 0),
      tx_bs32_tx_bit_ctrl_out(39 downto 0) => tx_bs32_tx_bit_ctrl_out(39 downto 0),
      tx_bs4_rx_bit_ctrl_in(39 downto 0) => tx_bs4_rx_bit_ctrl_in(39 downto 0),
      tx_bs4_rx_bit_ctrl_out(39 downto 0) => tx_bs4_rx_bit_ctrl_out(39 downto 0),
      tx_bs4_tx_bit_ctrl_in(39 downto 0) => tx_bs4_tx_bit_ctrl_in(39 downto 0),
      tx_bs4_tx_bit_ctrl_out(39 downto 0) => tx_bs4_tx_bit_ctrl_out(39 downto 0),
      tx_bs6_rx_bit_ctrl_in(39 downto 0) => tx_bs6_rx_bit_ctrl_in(39 downto 0),
      tx_bs6_rx_bit_ctrl_out(39 downto 0) => tx_bs6_rx_bit_ctrl_out(39 downto 0),
      tx_bs6_tx_bit_ctrl_in(39 downto 0) => tx_bs6_tx_bit_ctrl_in(39 downto 0),
      tx_bs6_tx_bit_ctrl_out(39 downto 0) => tx_bs6_tx_bit_ctrl_out(39 downto 0),
      tx_bs8_rx_bit_ctrl_in(39 downto 0) => tx_bs8_rx_bit_ctrl_in(39 downto 0),
      tx_bs8_rx_bit_ctrl_out(39 downto 0) => tx_bs8_rx_bit_ctrl_out(39 downto 0),
      tx_bs8_tx_bit_ctrl_in(39 downto 0) => tx_bs8_tx_bit_ctrl_in(39 downto 0),
      tx_bs8_tx_bit_ctrl_out(39 downto 0) => tx_bs8_tx_bit_ctrl_out(39 downto 0),
      tx_bs_rst_dly_in(15 downto 0) => tx_bs_rst_dly_in(15 downto 0),
      tx_bs_rst_in(15 downto 0) => tx_bs_rst_in(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_rst_scheme is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \bs_rst_int_r_reg[32]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \GEN_RIU_FROM_PLL.bs_dly_rst_reg_0\ : out STD_LOGIC;
    pll0_clkoutphy_en_in : out STD_LOGIC;
    \GEN_RIU_FROM_PLL.rst_seq_done_reg_0\ : out STD_LOGIC;
    n0_en_vtc_in : out STD_LOGIC;
    n1_en_vtc_in : out STD_LOGIC;
    n2_en_vtc_in : out STD_LOGIC;
    n3_en_vtc_in : out STD_LOGIC;
    n4_en_vtc_in : out STD_LOGIC;
    n5_en_vtc_in : out STD_LOGIC;
    pll0_locked_out : in STD_LOGIC;
    rst : in STD_LOGIC;
    multi_intf_lock_in : in STD_LOGIC;
    in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    pll0_clkout1_out : in STD_LOGIC;
    pll0_clkout0_out : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \GEN_RIU_FROM_PLL.hssio_state_reg[7]_0\ : in STD_LOGIC;
    en_vtc_bsc0 : in STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_rst_scheme;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_rst_scheme is
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[4]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[5]\ : STD_LOGIC;
  signal \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_stable_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.bs_dly_rst_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.bs_dly_rst_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.bs_dly_rst_i_3_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.bs_dly_rst_i_4_n_0\ : STD_LOGIC;
  signal \^gen_riu_from_pll.bs_dly_rst_reg_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.bsc_en_vtc_i_3_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[4]_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[5]_i_3_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_2_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.rst_seq_done_i_1_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\ : STD_LOGIC;
  signal \GEN_RIU_FROM_PLL.rst_seq_done_i_4_n_0\ : STD_LOGIC;
  signal \^gen_riu_from_pll.rst_seq_done_reg_0\ : STD_LOGIC;
  signal bs_dly_rst_r : STD_LOGIC_VECTOR ( 51 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of bs_dly_rst_r : signal is "true";
  signal bs_rst_dphy_sync : STD_LOGIC;
  signal bs_rst_int_r : STD_LOGIC_VECTOR ( 51 downto 0 );
  attribute RTL_KEEP of bs_rst_int_r : signal is "true";
  signal bs_rst_r : STD_LOGIC_VECTOR ( 51 downto 0 );
  attribute RTL_KEEP of bs_rst_r : signal is "true";
  signal bsc_en_vtc : STD_LOGIC;
  signal bsc_en_vtc_1 : STD_LOGIC;
  signal hssio_state : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal p_1_in_0 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal pll0_clkout1_stable : STD_LOGIC;
  signal pll0_clkoutphy_en : STD_LOGIC;
  signal \^pll0_clkoutphy_en_in\ : STD_LOGIC;
  signal pll0_fab_clk_cntr : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \pll0_fab_clk_cntr[6]_i_3_n_0\ : STD_LOGIC;
  signal \pll0_fab_clk_cntr[6]_i_4_n_0\ : STD_LOGIC;
  signal \pll0_fab_clk_cntr__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pll1_clkoutphy_en : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cell_dly_rdy_inst_n_5 : STD_LOGIC;
  signal sync_cell_start_fab_cntr_pll0_inst_n_0 : STD_LOGIC;
  signal wait_pll0_x_fab_clk_timeout : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[0].bs_ctrl_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[1].bs_ctrl_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[2].bs_ctrl_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[3].bs_ctrl_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[4].bs_ctrl_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \BITSLICE_CTRL[5].bs_ctrl_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[2]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[4]_i_1\ : label is "soft_lutpair5";
  attribute inverted : string;
  attribute inverted of \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[6]_inv\ : label is "yes";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.bs_dly_rst_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.bsc_en_vtc_i_3\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[0]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[3]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[4]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[4]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[5]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.hssio_state[5]_i_3\ : label is "soft_lutpair3";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[0]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[3]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[4]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[5]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[6]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[7]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute FSM_ENCODED_STATES of \GEN_RIU_FROM_PLL.hssio_state_reg[8]\ : label is "DEASSERT_BS_RESETS:000001000,ASSERT_PLL_CLKOUTPHYEN:000010000,WAIT_FOR_BSC_DLY_RDY:000100000,ASSERT_BSC_EN_VTC:001000000,ASSERT_ALL_RESETS:000000001,WAIT_FOR_BSC_VTC_RDY:010000000,RESET_SEQ_DONE:100000000";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \GEN_RIU_FROM_PLL.rst_seq_done_i_3\ : label is "soft_lutpair6";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \bs_rst_gen[0].bs_rst_r_reg[0]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[10].bs_dly_rst_r_reg[10]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[10].bs_rst_r_reg[10]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[11].bs_dly_rst_r_reg[11]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[11].bs_rst_r_reg[11]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[12].bs_dly_rst_r_reg[12]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[12].bs_rst_r_reg[12]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[13].bs_dly_rst_r_reg[13]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[13].bs_rst_r_reg[13]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[14].bs_dly_rst_r_reg[14]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[14].bs_rst_r_reg[14]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[15].bs_dly_rst_r_reg[15]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[15].bs_rst_r_reg[15]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[16].bs_dly_rst_r_reg[16]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[16].bs_rst_r_reg[16]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[17].bs_dly_rst_r_reg[17]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[17].bs_rst_r_reg[17]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[18].bs_dly_rst_r_reg[18]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[18].bs_rst_r_reg[18]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[19].bs_dly_rst_r_reg[19]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[19].bs_rst_r_reg[19]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[1].bs_dly_rst_r_reg[1]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[1].bs_rst_r_reg[1]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[20].bs_dly_rst_r_reg[20]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[20].bs_rst_r_reg[20]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[21].bs_dly_rst_r_reg[21]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[21].bs_rst_r_reg[21]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[22].bs_dly_rst_r_reg[22]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[22].bs_rst_r_reg[22]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[23].bs_dly_rst_r_reg[23]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[23].bs_rst_r_reg[23]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[24].bs_dly_rst_r_reg[24]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[24].bs_rst_r_reg[24]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[25].bs_dly_rst_r_reg[25]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[25].bs_rst_r_reg[25]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[26].bs_dly_rst_r_reg[26]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[26].bs_rst_r_reg[26]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[27].bs_dly_rst_r_reg[27]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[27].bs_rst_r_reg[27]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[28].bs_dly_rst_r_reg[28]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[28].bs_rst_r_reg[28]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[29].bs_dly_rst_r_reg[29]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[29].bs_rst_r_reg[29]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[2].bs_dly_rst_r_reg[2]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[2].bs_rst_r_reg[2]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[30].bs_dly_rst_r_reg[30]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[30].bs_rst_r_reg[30]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[31].bs_dly_rst_r_reg[31]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[31].bs_rst_r_reg[31]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[32].bs_dly_rst_r_reg[32]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[32].bs_rst_r_reg[32]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[33].bs_dly_rst_r_reg[33]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[33].bs_rst_r_reg[33]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[34].bs_dly_rst_r_reg[34]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[34].bs_rst_r_reg[34]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[35].bs_dly_rst_r_reg[35]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[35].bs_rst_r_reg[35]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[36].bs_dly_rst_r_reg[36]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[36].bs_rst_r_reg[36]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[37].bs_dly_rst_r_reg[37]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[37].bs_rst_r_reg[37]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[38].bs_dly_rst_r_reg[38]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[38].bs_rst_r_reg[38]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[39].bs_dly_rst_r_reg[39]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[39].bs_rst_r_reg[39]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[3].bs_dly_rst_r_reg[3]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[3].bs_rst_r_reg[3]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[40].bs_dly_rst_r_reg[40]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[40].bs_rst_r_reg[40]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[41].bs_dly_rst_r_reg[41]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[41].bs_rst_r_reg[41]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[42].bs_dly_rst_r_reg[42]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[42].bs_rst_r_reg[42]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[43].bs_dly_rst_r_reg[43]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[43].bs_rst_r_reg[43]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[44].bs_dly_rst_r_reg[44]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[44].bs_rst_r_reg[44]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[45].bs_dly_rst_r_reg[45]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[45].bs_rst_r_reg[45]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[46].bs_dly_rst_r_reg[46]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[46].bs_rst_r_reg[46]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[47].bs_dly_rst_r_reg[47]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[47].bs_rst_r_reg[47]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[48].bs_dly_rst_r_reg[48]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[48].bs_rst_r_reg[48]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[49].bs_dly_rst_r_reg[49]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[49].bs_rst_r_reg[49]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[4].bs_dly_rst_r_reg[4]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[4].bs_rst_r_reg[4]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[50].bs_dly_rst_r_reg[50]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[50].bs_rst_r_reg[50]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[51].bs_dly_rst_r_reg[51]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[51].bs_rst_r_reg[51]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[5].bs_dly_rst_r_reg[5]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[5].bs_rst_r_reg[5]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[6].bs_dly_rst_r_reg[6]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[6].bs_rst_r_reg[6]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[7].bs_dly_rst_r_reg[7]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[7].bs_rst_r_reg[7]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[8].bs_dly_rst_r_reg[8]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[8].bs_rst_r_reg[8]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[9].bs_dly_rst_r_reg[9]\ : label is "no";
  attribute equivalent_register_removal of \bs_rst_gen[9].bs_rst_r_reg[9]\ : label is "no";
  attribute KEEP : string;
  attribute KEEP of \bs_rst_int_r_reg[0]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[10]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[11]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[12]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[13]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[14]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[15]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[16]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[17]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[18]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[19]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[1]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[20]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[21]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[22]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[23]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[24]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[25]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[26]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[27]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[28]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[29]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[2]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[30]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[31]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[32]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[33]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[34]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[35]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[36]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[37]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[38]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[39]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[3]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[40]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[41]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[42]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[43]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[44]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[45]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[46]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[47]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[48]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[49]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[4]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[50]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[51]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[5]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[6]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[7]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[8]\ : label is "yes";
  attribute KEEP of \bs_rst_int_r_reg[9]\ : label is "yes";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[0]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[1]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[2]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \pll0_fab_clk_cntr[6]_i_4\ : label is "soft_lutpair4";
begin
  \GEN_RIU_FROM_PLL.bs_dly_rst_reg_0\ <= \^gen_riu_from_pll.bs_dly_rst_reg_0\;
  \GEN_RIU_FROM_PLL.rst_seq_done_reg_0\ <= \^gen_riu_from_pll.rst_seq_done_reg_0\;
  \bs_rst_int_r_reg[32]_0\(15) <= bs_rst_int_r(32);
  \bs_rst_int_r_reg[32]_0\(14) <= bs_rst_int_r(30);
  \bs_rst_int_r_reg[32]_0\(13) <= bs_rst_int_r(28);
  \bs_rst_int_r_reg[32]_0\(12) <= bs_rst_int_r(26);
  \bs_rst_int_r_reg[32]_0\(11) <= bs_rst_int_r(23);
  \bs_rst_int_r_reg[32]_0\(10) <= bs_rst_int_r(21);
  \bs_rst_int_r_reg[32]_0\(9) <= bs_rst_int_r(19);
  \bs_rst_int_r_reg[32]_0\(8) <= bs_rst_int_r(17);
  \bs_rst_int_r_reg[32]_0\(7) <= bs_rst_int_r(15);
  \bs_rst_int_r_reg[32]_0\(6) <= bs_rst_int_r(13);
  \bs_rst_int_r_reg[32]_0\(5) <= bs_rst_int_r(10);
  \bs_rst_int_r_reg[32]_0\(4) <= bs_rst_int_r(8);
  \bs_rst_int_r_reg[32]_0\(3) <= bs_rst_int_r(6);
  \bs_rst_int_r_reg[32]_0\(2) <= bs_rst_int_r(4);
  \bs_rst_int_r_reg[32]_0\(1) <= bs_rst_int_r(2);
  \bs_rst_int_r_reg[32]_0\(0) <= bs_rst_int_r(0);
  \out\(15) <= bs_dly_rst_r(32);
  \out\(14) <= bs_dly_rst_r(30);
  \out\(13) <= bs_dly_rst_r(28);
  \out\(12) <= bs_dly_rst_r(26);
  \out\(11) <= bs_dly_rst_r(23);
  \out\(10) <= bs_dly_rst_r(21);
  \out\(9) <= bs_dly_rst_r(19);
  \out\(8) <= bs_dly_rst_r(17);
  \out\(7) <= bs_dly_rst_r(15);
  \out\(6) <= bs_dly_rst_r(13);
  \out\(5) <= bs_dly_rst_r(10);
  \out\(4) <= bs_dly_rst_r(8);
  \out\(3) <= bs_dly_rst_r(6);
  \out\(2) <= bs_dly_rst_r(4);
  \out\(1) <= bs_dly_rst_r(2);
  \out\(0) <= bs_dly_rst_r(0);
  pll0_clkoutphy_en_in <= \^pll0_clkoutphy_en_in\;
\BITSLICE_CTRL[0].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc0,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n0_en_vtc_in
    );
\BITSLICE_CTRL[1].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc1,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n1_en_vtc_in
    );
\BITSLICE_CTRL[2].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc2,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n2_en_vtc_in
    );
\BITSLICE_CTRL[3].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc3,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n3_en_vtc_in
    );
\BITSLICE_CTRL[4].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc4,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n4_en_vtc_in
    );
\BITSLICE_CTRL[5].bs_ctrl_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => en_vtc_bsc5,
      I1 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      I2 => bsc_en_vtc,
      O => n5_en_vtc_in
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      O => p_0_in(0)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      O => p_0_in(1)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      I2 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\,
      O => p_0_in(2)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I2 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\,
      I3 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\,
      O => p_0_in(3)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I2 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      I3 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\,
      I4 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[4]\,
      O => p_0_in(4)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      I2 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I3 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\,
      I4 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[4]\,
      I5 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[5]\,
      O => p_0_in(5)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr[6]_inv_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[4]\,
      I1 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\,
      I2 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\,
      I3 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\,
      I4 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\,
      I5 => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[5]\,
      O => p_0_in(6)
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(0),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[0]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(1),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[1]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(2),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[2]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(3),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[3]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(4),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[4]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      CLR => AR(0),
      D => p_0_in(5),
      Q => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg_n_0_[5]\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_cntr_reg[6]_inv\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => pll0_clkout1_out,
      CE => sel,
      D => p_0_in(6),
      PRE => AR(0),
      Q => sel
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_stable_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => sel,
      I1 => pll0_clkout1_stable,
      O => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_stable_i_1_n_0\
    );
\GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_stable_reg\: unisim.vcomponents.FDCE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      CLR => AR(0),
      D => \GEN_PLL0_CLKOUT1_CNTR.pll0_clkout1_stable_i_1_n_0\,
      Q => pll0_clkout1_stable
    );
\GEN_RIU_FROM_PLL.bs_dly_rst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEDFFFFEFED0000"
    )
        port map (
      I0 => hssio_state(0),
      I1 => \GEN_RIU_FROM_PLL.bs_dly_rst_i_2_n_0\,
      I2 => hssio_state(3),
      I3 => pll0_clkout1_stable,
      I4 => \GEN_RIU_FROM_PLL.bs_dly_rst_i_3_n_0\,
      I5 => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      O => \GEN_RIU_FROM_PLL.bs_dly_rst_i_1_n_0\
    );
\GEN_RIU_FROM_PLL.bs_dly_rst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => hssio_state(8),
      I1 => hssio_state(4),
      I2 => hssio_state(7),
      I3 => hssio_state(5),
      I4 => bsc_en_vtc_1,
      O => \GEN_RIU_FROM_PLL.bs_dly_rst_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.bs_dly_rst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEEFFFFEFEED"
    )
        port map (
      I0 => hssio_state(0),
      I1 => \GEN_RIU_FROM_PLL.bs_dly_rst_i_4_n_0\,
      I2 => hssio_state(8),
      I3 => hssio_state(4),
      I4 => \GEN_RIU_FROM_PLL.hssio_state[4]_i_2_n_0\,
      I5 => pll0_clkout1_stable,
      O => \GEN_RIU_FROM_PLL.bs_dly_rst_i_3_n_0\
    );
\GEN_RIU_FROM_PLL.bs_dly_rst_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEA"
    )
        port map (
      I0 => hssio_state(3),
      I1 => bsc_en_vtc_1,
      I2 => hssio_state(5),
      I3 => hssio_state(7),
      O => \GEN_RIU_FROM_PLL.bs_dly_rst_i_4_n_0\
    );
\GEN_RIU_FROM_PLL.bs_dly_rst_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \GEN_RIU_FROM_PLL.bs_dly_rst_i_1_n_0\,
      PRE => AR(0),
      Q => \^gen_riu_from_pll.bs_dly_rst_reg_0\
    );
\GEN_RIU_FROM_PLL.bsc_en_vtc_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => hssio_state(4),
      I1 => hssio_state(8),
      I2 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I3 => hssio_state(0),
      O => \GEN_RIU_FROM_PLL.bsc_en_vtc_i_3_n_0\
    );
\GEN_RIU_FROM_PLL.bsc_en_vtc_reg\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      CLR => AR(0),
      D => sync_cell_dly_rdy_inst_n_5,
      Q => bsc_en_vtc
    );
\GEN_RIU_FROM_PLL.hssio_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => hssio_state(3),
      I1 => bsc_en_vtc_1,
      I2 => hssio_state(5),
      I3 => hssio_state(7),
      O => \GEN_RIU_FROM_PLL.hssio_state[0]_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => hssio_state(0),
      I1 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I2 => hssio_state(8),
      I3 => hssio_state(4),
      O => p_1_in_0(3)
    );
\GEN_RIU_FROM_PLL.hssio_state[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000004"
    )
        port map (
      I0 => \GEN_RIU_FROM_PLL.hssio_state[4]_i_2_n_0\,
      I1 => hssio_state(3),
      I2 => hssio_state(0),
      I3 => hssio_state(4),
      I4 => hssio_state(8),
      O => p_1_in_0(4)
    );
\GEN_RIU_FROM_PLL.hssio_state[4]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => bsc_en_vtc_1,
      I1 => hssio_state(5),
      I2 => hssio_state(7),
      O => \GEN_RIU_FROM_PLL.hssio_state[4]_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[5]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => hssio_state(8),
      I1 => hssio_state(4),
      I2 => hssio_state(0),
      I3 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      O => pll0_clkoutphy_en
    );
\GEN_RIU_FROM_PLL.hssio_state[5]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => hssio_state(0),
      I1 => hssio_state(4),
      I2 => hssio_state(8),
      I3 => hssio_state(3),
      O => \GEN_RIU_FROM_PLL.hssio_state[5]_i_3_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => hssio_state(0),
      I1 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I2 => hssio_state(8),
      I3 => hssio_state(4),
      I4 => pll0_clkout1_stable,
      O => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      D => p_1_in_0(0),
      PRE => AR(0),
      Q => hssio_state(0)
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(3),
      Q => hssio_state(3)
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(4),
      Q => hssio_state(4)
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(5),
      Q => hssio_state(5)
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(6),
      Q => bsc_en_vtc_1
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(7),
      Q => hssio_state(7)
    );
\GEN_RIU_FROM_PLL.hssio_state_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => \GEN_RIU_FROM_PLL.hssio_state[8]_i_1_n_0\,
      CLR => AR(0),
      D => p_1_in_0(8),
      Q => hssio_state(8)
    );
\GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004FFFF00040000"
    )
        port map (
      I0 => hssio_state(8),
      I1 => hssio_state(4),
      I2 => hssio_state(0),
      I3 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I4 => \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_2_n_0\,
      I5 => \^pll0_clkoutphy_en_in\,
      O => \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_1_n_0\
    );
\GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFFFFFEFEEF"
    )
        port map (
      I0 => hssio_state(4),
      I1 => \GEN_RIU_FROM_PLL.hssio_state[0]_i_2_n_0\,
      I2 => hssio_state(0),
      I3 => hssio_state(8),
      I4 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I5 => pll0_clkout1_stable,
      O => \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.pll0_clkoutphy_en_reg\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      CLR => AR(0),
      D => \GEN_RIU_FROM_PLL.pll0_clkoutphy_en_i_1_n_0\,
      Q => \^pll0_clkoutphy_en_in\
    );
\GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAA8AAA"
    )
        port map (
      I0 => pll1_clkoutphy_en,
      I1 => \GEN_RIU_FROM_PLL.hssio_state[4]_i_2_n_0\,
      I2 => hssio_state(0),
      I3 => \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_2_n_0\,
      I4 => hssio_state(3),
      I5 => hssio_state(8),
      O => \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_1_n_0\
    );
\GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pll0_clkout1_stable,
      I1 => hssio_state(4),
      O => \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_2_n_0\
    );
\GEN_RIU_FROM_PLL.pll1_clkoutphy_en_reg\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      CLR => AR(0),
      D => \GEN_RIU_FROM_PLL.pll1_clkoutphy_en_i_1_n_0\,
      Q => pll1_clkoutphy_en
    );
\GEN_RIU_FROM_PLL.rst_seq_done_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100011601000100"
    )
        port map (
      I0 => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      I1 => hssio_state(4),
      I2 => hssio_state(0),
      I3 => hssio_state(8),
      I4 => \GEN_RIU_FROM_PLL.rst_seq_done_i_4_n_0\,
      I5 => \^gen_riu_from_pll.rst_seq_done_reg_0\,
      O => \GEN_RIU_FROM_PLL.rst_seq_done_i_1_n_0\
    );
\GEN_RIU_FROM_PLL.rst_seq_done_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => hssio_state(7),
      I1 => hssio_state(5),
      I2 => bsc_en_vtc_1,
      I3 => hssio_state(3),
      O => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\
    );
\GEN_RIU_FROM_PLL.rst_seq_done_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF0FFF0F002"
    )
        port map (
      I0 => pll0_clkout1_stable,
      I1 => hssio_state(4),
      I2 => hssio_state(7),
      I3 => hssio_state(5),
      I4 => bsc_en_vtc_1,
      I5 => hssio_state(3),
      O => \GEN_RIU_FROM_PLL.rst_seq_done_i_4_n_0\
    );
\GEN_RIU_FROM_PLL.rst_seq_done_reg\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      CLR => AR(0),
      D => \GEN_RIU_FROM_PLL.rst_seq_done_i_1_n_0\,
      Q => \^gen_riu_from_pll.rst_seq_done_reg_0\
    );
\bs_rst_gen[0].bs_dly_rst_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(0),
      R => '0'
    );
\bs_rst_gen[0].bs_rst_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(0),
      R => '0'
    );
\bs_rst_gen[10].bs_dly_rst_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(10),
      R => '0'
    );
\bs_rst_gen[10].bs_rst_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(10),
      R => '0'
    );
\bs_rst_gen[11].bs_dly_rst_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(11),
      R => '0'
    );
\bs_rst_gen[11].bs_rst_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(11),
      R => '0'
    );
\bs_rst_gen[12].bs_dly_rst_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(12),
      R => '0'
    );
\bs_rst_gen[12].bs_rst_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(12),
      R => '0'
    );
\bs_rst_gen[13].bs_dly_rst_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(13),
      R => '0'
    );
\bs_rst_gen[13].bs_rst_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(13),
      R => '0'
    );
\bs_rst_gen[14].bs_dly_rst_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(14),
      R => '0'
    );
\bs_rst_gen[14].bs_rst_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(14),
      R => '0'
    );
\bs_rst_gen[15].bs_dly_rst_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(15),
      R => '0'
    );
\bs_rst_gen[15].bs_rst_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(15),
      R => '0'
    );
\bs_rst_gen[16].bs_dly_rst_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(16),
      R => '0'
    );
\bs_rst_gen[16].bs_rst_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(16),
      R => '0'
    );
\bs_rst_gen[17].bs_dly_rst_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(17),
      R => '0'
    );
\bs_rst_gen[17].bs_rst_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(17),
      R => '0'
    );
\bs_rst_gen[18].bs_dly_rst_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(18),
      R => '0'
    );
\bs_rst_gen[18].bs_rst_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(18),
      R => '0'
    );
\bs_rst_gen[19].bs_dly_rst_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(19),
      R => '0'
    );
\bs_rst_gen[19].bs_rst_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(19),
      R => '0'
    );
\bs_rst_gen[1].bs_dly_rst_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(1),
      R => '0'
    );
\bs_rst_gen[1].bs_rst_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(1),
      R => '0'
    );
\bs_rst_gen[20].bs_dly_rst_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(20),
      R => '0'
    );
\bs_rst_gen[20].bs_rst_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(20),
      R => '0'
    );
\bs_rst_gen[21].bs_dly_rst_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(21),
      R => '0'
    );
\bs_rst_gen[21].bs_rst_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(21),
      R => '0'
    );
\bs_rst_gen[22].bs_dly_rst_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(22),
      R => '0'
    );
\bs_rst_gen[22].bs_rst_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(22),
      R => '0'
    );
\bs_rst_gen[23].bs_dly_rst_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(23),
      R => '0'
    );
\bs_rst_gen[23].bs_rst_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(23),
      R => '0'
    );
\bs_rst_gen[24].bs_dly_rst_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(24),
      R => '0'
    );
\bs_rst_gen[24].bs_rst_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(24),
      R => '0'
    );
\bs_rst_gen[25].bs_dly_rst_r_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(25),
      R => '0'
    );
\bs_rst_gen[25].bs_rst_r_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(25),
      R => '0'
    );
\bs_rst_gen[26].bs_dly_rst_r_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(26),
      R => '0'
    );
\bs_rst_gen[26].bs_rst_r_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(26),
      R => '0'
    );
\bs_rst_gen[27].bs_dly_rst_r_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(27),
      R => '0'
    );
\bs_rst_gen[27].bs_rst_r_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(27),
      R => '0'
    );
\bs_rst_gen[28].bs_dly_rst_r_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(28),
      R => '0'
    );
\bs_rst_gen[28].bs_rst_r_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(28),
      R => '0'
    );
\bs_rst_gen[29].bs_dly_rst_r_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(29),
      R => '0'
    );
\bs_rst_gen[29].bs_rst_r_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(29),
      R => '0'
    );
\bs_rst_gen[2].bs_dly_rst_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(2),
      R => '0'
    );
\bs_rst_gen[2].bs_rst_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(2),
      R => '0'
    );
\bs_rst_gen[30].bs_dly_rst_r_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(30),
      R => '0'
    );
\bs_rst_gen[30].bs_rst_r_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(30),
      R => '0'
    );
\bs_rst_gen[31].bs_dly_rst_r_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(31),
      R => '0'
    );
\bs_rst_gen[31].bs_rst_r_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(31),
      R => '0'
    );
\bs_rst_gen[32].bs_dly_rst_r_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(32),
      R => '0'
    );
\bs_rst_gen[32].bs_rst_r_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(32),
      R => '0'
    );
\bs_rst_gen[33].bs_dly_rst_r_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(33),
      R => '0'
    );
\bs_rst_gen[33].bs_rst_r_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(33),
      R => '0'
    );
\bs_rst_gen[34].bs_dly_rst_r_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(34),
      R => '0'
    );
\bs_rst_gen[34].bs_rst_r_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(34),
      R => '0'
    );
\bs_rst_gen[35].bs_dly_rst_r_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(35),
      R => '0'
    );
\bs_rst_gen[35].bs_rst_r_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(35),
      R => '0'
    );
\bs_rst_gen[36].bs_dly_rst_r_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(36),
      R => '0'
    );
\bs_rst_gen[36].bs_rst_r_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(36),
      R => '0'
    );
\bs_rst_gen[37].bs_dly_rst_r_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(37),
      R => '0'
    );
\bs_rst_gen[37].bs_rst_r_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(37),
      R => '0'
    );
\bs_rst_gen[38].bs_dly_rst_r_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(38),
      R => '0'
    );
\bs_rst_gen[38].bs_rst_r_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(38),
      R => '0'
    );
\bs_rst_gen[39].bs_dly_rst_r_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(39),
      R => '0'
    );
\bs_rst_gen[39].bs_rst_r_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(39),
      R => '0'
    );
\bs_rst_gen[3].bs_dly_rst_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(3),
      R => '0'
    );
\bs_rst_gen[3].bs_rst_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(3),
      R => '0'
    );
\bs_rst_gen[40].bs_dly_rst_r_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(40),
      R => '0'
    );
\bs_rst_gen[40].bs_rst_r_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(40),
      R => '0'
    );
\bs_rst_gen[41].bs_dly_rst_r_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(41),
      R => '0'
    );
\bs_rst_gen[41].bs_rst_r_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(41),
      R => '0'
    );
\bs_rst_gen[42].bs_dly_rst_r_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(42),
      R => '0'
    );
\bs_rst_gen[42].bs_rst_r_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(42),
      R => '0'
    );
\bs_rst_gen[43].bs_dly_rst_r_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(43),
      R => '0'
    );
\bs_rst_gen[43].bs_rst_r_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(43),
      R => '0'
    );
\bs_rst_gen[44].bs_dly_rst_r_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(44),
      R => '0'
    );
\bs_rst_gen[44].bs_rst_r_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(44),
      R => '0'
    );
\bs_rst_gen[45].bs_dly_rst_r_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(45),
      R => '0'
    );
\bs_rst_gen[45].bs_rst_r_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(45),
      R => '0'
    );
\bs_rst_gen[46].bs_dly_rst_r_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(46),
      R => '0'
    );
\bs_rst_gen[46].bs_rst_r_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(46),
      R => '0'
    );
\bs_rst_gen[47].bs_dly_rst_r_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(47),
      R => '0'
    );
\bs_rst_gen[47].bs_rst_r_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(47),
      R => '0'
    );
\bs_rst_gen[48].bs_dly_rst_r_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(48),
      R => '0'
    );
\bs_rst_gen[48].bs_rst_r_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(48),
      R => '0'
    );
\bs_rst_gen[49].bs_dly_rst_r_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(49),
      R => '0'
    );
\bs_rst_gen[49].bs_rst_r_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(49),
      R => '0'
    );
\bs_rst_gen[4].bs_dly_rst_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(4),
      R => '0'
    );
\bs_rst_gen[4].bs_rst_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(4),
      R => '0'
    );
\bs_rst_gen[50].bs_dly_rst_r_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(50),
      R => '0'
    );
\bs_rst_gen[50].bs_rst_r_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(50),
      R => '0'
    );
\bs_rst_gen[51].bs_dly_rst_r_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(51),
      R => '0'
    );
\bs_rst_gen[51].bs_rst_r_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(51),
      R => '0'
    );
\bs_rst_gen[5].bs_dly_rst_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(5),
      R => '0'
    );
\bs_rst_gen[5].bs_rst_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(5),
      R => '0'
    );
\bs_rst_gen[6].bs_dly_rst_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(6),
      R => '0'
    );
\bs_rst_gen[6].bs_rst_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(6),
      R => '0'
    );
\bs_rst_gen[7].bs_dly_rst_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(7),
      R => '0'
    );
\bs_rst_gen[7].bs_rst_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(7),
      R => '0'
    );
\bs_rst_gen[8].bs_dly_rst_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(8),
      R => '0'
    );
\bs_rst_gen[8].bs_rst_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(8),
      R => '0'
    );
\bs_rst_gen[9].bs_dly_rst_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_dly_rst_r(9),
      R => '0'
    );
\bs_rst_gen[9].bs_rst_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => \^gen_riu_from_pll.bs_dly_rst_reg_0\,
      Q => bs_rst_r(9),
      R => '0'
    );
\bs_rst_int_r_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(0),
      Q => bs_rst_int_r(0),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[10]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(10),
      Q => bs_rst_int_r(10),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[11]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(11),
      Q => bs_rst_int_r(11),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[12]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(12),
      Q => bs_rst_int_r(12),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[13]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(13),
      Q => bs_rst_int_r(13),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(14),
      Q => bs_rst_int_r(14),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[15]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(15),
      Q => bs_rst_int_r(15),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[16]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(16),
      Q => bs_rst_int_r(16),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[17]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(17),
      Q => bs_rst_int_r(17),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[18]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(18),
      Q => bs_rst_int_r(18),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[19]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(19),
      Q => bs_rst_int_r(19),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(1),
      Q => bs_rst_int_r(1),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[20]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(20),
      Q => bs_rst_int_r(20),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[21]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(21),
      Q => bs_rst_int_r(21),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[22]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(22),
      Q => bs_rst_int_r(22),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[23]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(23),
      Q => bs_rst_int_r(23),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[24]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(24),
      Q => bs_rst_int_r(24),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[25]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(25),
      Q => bs_rst_int_r(25),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[26]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(26),
      Q => bs_rst_int_r(26),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[27]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(27),
      Q => bs_rst_int_r(27),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[28]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(28),
      Q => bs_rst_int_r(28),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[29]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(29),
      Q => bs_rst_int_r(29),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(2),
      Q => bs_rst_int_r(2),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[30]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(30),
      Q => bs_rst_int_r(30),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[31]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(31),
      Q => bs_rst_int_r(31),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[32]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(32),
      Q => bs_rst_int_r(32),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[33]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(33),
      Q => bs_rst_int_r(33),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[34]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(34),
      Q => bs_rst_int_r(34),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[35]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(35),
      Q => bs_rst_int_r(35),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[36]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(36),
      Q => bs_rst_int_r(36),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[37]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(37),
      Q => bs_rst_int_r(37),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[38]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(38),
      Q => bs_rst_int_r(38),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[39]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(39),
      Q => bs_rst_int_r(39),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(3),
      Q => bs_rst_int_r(3),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[40]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(40),
      Q => bs_rst_int_r(40),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[41]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(41),
      Q => bs_rst_int_r(41),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[42]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(42),
      Q => bs_rst_int_r(42),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[43]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(43),
      Q => bs_rst_int_r(43),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[44]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(44),
      Q => bs_rst_int_r(44),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[45]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(45),
      Q => bs_rst_int_r(45),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[46]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(46),
      Q => bs_rst_int_r(46),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[47]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(47),
      Q => bs_rst_int_r(47),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[48]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(48),
      Q => bs_rst_int_r(48),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[49]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(49),
      Q => bs_rst_int_r(49),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(4),
      Q => bs_rst_int_r(4),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[50]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(50),
      Q => bs_rst_int_r(50),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[51]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(51),
      Q => bs_rst_int_r(51),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(5),
      Q => bs_rst_int_r(5),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(6),
      Q => bs_rst_int_r(6),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(7),
      Q => bs_rst_int_r(7),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[8]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(8),
      Q => bs_rst_int_r(8),
      S => bs_rst_dphy_sync
    );
\bs_rst_int_r_reg[9]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout1_out,
      CE => '1',
      D => bs_rst_r(9),
      Q => bs_rst_int_r(9),
      S => bs_rst_dphy_sync
    );
\pll0_fab_clk_cntr[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(0),
      O => p_1_in(0)
    );
\pll0_fab_clk_cntr[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(0),
      I1 => \pll0_fab_clk_cntr__0\(1),
      O => p_1_in(1)
    );
\pll0_fab_clk_cntr[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(0),
      I1 => \pll0_fab_clk_cntr__0\(1),
      I2 => \pll0_fab_clk_cntr__0\(2),
      O => p_1_in(2)
    );
\pll0_fab_clk_cntr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(1),
      I1 => \pll0_fab_clk_cntr__0\(0),
      I2 => \pll0_fab_clk_cntr__0\(2),
      I3 => \pll0_fab_clk_cntr__0\(3),
      O => p_1_in(3)
    );
\pll0_fab_clk_cntr[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(2),
      I1 => \pll0_fab_clk_cntr__0\(0),
      I2 => \pll0_fab_clk_cntr__0\(1),
      I3 => \pll0_fab_clk_cntr__0\(3),
      I4 => \pll0_fab_clk_cntr__0\(4),
      O => p_1_in(4)
    );
\pll0_fab_clk_cntr[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(3),
      I1 => \pll0_fab_clk_cntr__0\(1),
      I2 => \pll0_fab_clk_cntr__0\(0),
      I3 => \pll0_fab_clk_cntr__0\(2),
      I4 => \pll0_fab_clk_cntr__0\(4),
      I5 => \pll0_fab_clk_cntr__0\(5),
      O => p_1_in(5)
    );
\pll0_fab_clk_cntr[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \pll0_fab_clk_cntr[6]_i_4_n_0\,
      I1 => \pll0_fab_clk_cntr__0\(5),
      I2 => pll0_fab_clk_cntr(6),
      O => p_1_in(6)
    );
\pll0_fab_clk_cntr[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(3),
      I1 => \pll0_fab_clk_cntr__0\(2),
      I2 => \pll0_fab_clk_cntr__0\(5),
      I3 => \pll0_fab_clk_cntr__0\(4),
      I4 => \pll0_fab_clk_cntr__0\(1),
      I5 => \pll0_fab_clk_cntr__0\(0),
      O => \pll0_fab_clk_cntr[6]_i_3_n_0\
    );
\pll0_fab_clk_cntr[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \pll0_fab_clk_cntr__0\(4),
      I1 => \pll0_fab_clk_cntr__0\(2),
      I2 => \pll0_fab_clk_cntr__0\(0),
      I3 => \pll0_fab_clk_cntr__0\(1),
      I4 => \pll0_fab_clk_cntr__0\(3),
      O => \pll0_fab_clk_cntr[6]_i_4_n_0\
    );
\pll0_fab_clk_cntr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(0),
      Q => \pll0_fab_clk_cntr__0\(0),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(1),
      Q => \pll0_fab_clk_cntr__0\(1),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(2),
      Q => \pll0_fab_clk_cntr__0\(2),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(3),
      Q => \pll0_fab_clk_cntr__0\(3),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(4),
      Q => \pll0_fab_clk_cntr__0\(4),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(5),
      Q => \pll0_fab_clk_cntr__0\(5),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
\pll0_fab_clk_cntr_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => p_1_in(6),
      Q => pll0_fab_clk_cntr(6),
      R => sync_cell_start_fab_cntr_pll0_inst_n_0
    );
sync_cell_bs_rst_dphy_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_0
     port map (
      SS(0) => bs_rst_dphy_sync,
      pll0_clkout1_out => pll0_clkout1_out
    );
sync_cell_dly_rdy_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_1
     port map (
      D(4 downto 1) => p_1_in_0(8 downto 5),
      D(0) => p_1_in_0(0),
      \GEN_RIU_FROM_PLL.bsc_en_vtc_reg\ => \GEN_RIU_FROM_PLL.bsc_en_vtc_i_3_n_0\,
      \GEN_RIU_FROM_PLL.hssio_state_reg[0]\ => \GEN_RIU_FROM_PLL.hssio_state[0]_i_2_n_0\,
      \GEN_RIU_FROM_PLL.hssio_state_reg[5]\ => \GEN_RIU_FROM_PLL.hssio_state[5]_i_3_n_0\,
      \GEN_RIU_FROM_PLL.hssio_state_reg[6]\ => sync_cell_dly_rdy_inst_n_5,
      \GEN_RIU_FROM_PLL.hssio_state_reg[7]\ => \GEN_RIU_FROM_PLL.hssio_state_reg[7]_0\,
      \GEN_RIU_FROM_PLL.hssio_state_reg[8]\ => \GEN_RIU_FROM_PLL.rst_seq_done_i_3_n_0\,
      Q(6 downto 5) => hssio_state(8 downto 7),
      Q(4) => bsc_en_vtc_1,
      Q(3 downto 1) => hssio_state(5 downto 3),
      Q(0) => hssio_state(0),
      bsc_en_vtc => bsc_en_vtc,
      in0(0) => in0(0),
      pll0_clkout1_out => pll0_clkout1_out,
      pll0_clkout1_stable => pll0_clkout1_stable,
      pll0_clkoutphy_en => pll0_clkoutphy_en
    );
sync_cell_mult_intf_lock_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_2
     port map (
      multi_intf_lock_in => multi_intf_lock_in,
      pll0_clkout1_out => pll0_clkout1_out
    );
sync_cell_pll0lock_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_3
     port map (
      pll0_clkout1_out => pll0_clkout1_out,
      pll0_locked_out => pll0_locked_out
    );
sync_cell_rst_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_4
     port map (
      pll0_clkout1_out => pll0_clkout1_out,
      rst => rst
    );
sync_cell_start_fab_cntr_pll0_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_5
     port map (
      SR(0) => sync_cell_start_fab_cntr_pll0_inst_n_0,
      in0(0) => pll1_clkoutphy_en,
      \out\(0) => pll0_fab_clk_cntr(6),
      pll0_clkout0_out => pll0_clkout0_out,
      \pll0_fab_clk_cntr_reg[6]\ => \pll0_fab_clk_cntr[6]_i_3_n_0\
    );
sync_cell_wait_pll0_fab_timeout_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell_6
     port map (
      in0(0) => wait_pll0_x_fab_clk_timeout,
      pll0_clkout1_out => pll0_clkout1_out
    );
wait_pll0_x_fab_clk_timeout_reg: unisim.vcomponents.FDRE
     port map (
      C => pll0_clkout0_out,
      CE => '1',
      D => pll0_fab_clk_cntr(6),
      Q => wait_pll0_x_fab_clk_timeout,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_rst_top is
  port (
    pll0_locked_out : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \bs_rst_int_r_reg[32]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    pll0_clkout1_out : out STD_LOGIC;
    pll0_clkout0_out : out STD_LOGIC;
    bsctrl_rst : out STD_LOGIC;
    rst_seq_done1 : out STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    n0_en_vtc_in : out STD_LOGIC;
    n1_en_vtc_in : out STD_LOGIC;
    n2_en_vtc_in : out STD_LOGIC;
    n3_en_vtc_in : out STD_LOGIC;
    n4_en_vtc_in : out STD_LOGIC;
    n5_en_vtc_in : out STD_LOGIC;
    rst : in STD_LOGIC;
    multi_intf_lock_in : in STD_LOGIC;
    in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \GEN_RIU_FROM_PLL.hssio_state_reg[7]\ : in STD_LOGIC;
    clk : in STD_LOGIC;
    en_vtc_bsc0 : in STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_rst_top;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_rst_top is
  signal \^pll0_clkout0_out\ : STD_LOGIC;
  signal \^pll0_clkout1_out\ : STD_LOGIC;
  signal pll0_clkoutphy_en : STD_LOGIC;
  signal \^pll0_locked_out\ : STD_LOGIC;
  signal rst_to_sm : STD_LOGIC;
begin
  pll0_clkout0_out <= \^pll0_clkout0_out\;
  pll0_clkout1_out <= \^pll0_clkout1_out\;
  pll0_locked_out <= \^pll0_locked_out\;
clk_scheme_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_scheme
     port map (
      AR(0) => rst_to_sm,
      clk => clk,
      pll0_clkout0_out => \^pll0_clkout0_out\,
      pll0_clkout1_out => \^pll0_clkout1_out\,
      pll0_clkoutphy_en_in => pll0_clkoutphy_en,
      pll0_locked_out => \^pll0_locked_out\,
      rst => rst,
      shared_pll0_clkoutphy_out => shared_pll0_clkoutphy_out
    );
rst_scheme_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_rst_scheme
     port map (
      AR(0) => rst_to_sm,
      \GEN_RIU_FROM_PLL.bs_dly_rst_reg_0\ => bsctrl_rst,
      \GEN_RIU_FROM_PLL.hssio_state_reg[7]_0\ => \GEN_RIU_FROM_PLL.hssio_state_reg[7]\,
      \GEN_RIU_FROM_PLL.rst_seq_done_reg_0\ => rst_seq_done1,
      \bs_rst_int_r_reg[32]_0\(15 downto 0) => \bs_rst_int_r_reg[32]\(15 downto 0),
      en_vtc_bsc0 => en_vtc_bsc0,
      en_vtc_bsc1 => en_vtc_bsc1,
      en_vtc_bsc2 => en_vtc_bsc2,
      en_vtc_bsc3 => en_vtc_bsc3,
      en_vtc_bsc4 => en_vtc_bsc4,
      en_vtc_bsc5 => en_vtc_bsc5,
      in0(0) => in0(0),
      multi_intf_lock_in => multi_intf_lock_in,
      n0_en_vtc_in => n0_en_vtc_in,
      n1_en_vtc_in => n1_en_vtc_in,
      n2_en_vtc_in => n2_en_vtc_in,
      n3_en_vtc_in => n3_en_vtc_in,
      n4_en_vtc_in => n4_en_vtc_in,
      n5_en_vtc_in => n5_en_vtc_in,
      \out\(15 downto 0) => \out\(15 downto 0),
      pll0_clkout0_out => \^pll0_clkout0_out\,
      pll0_clkout1_out => \^pll0_clkout1_out\,
      pll0_clkoutphy_en_in => pll0_clkoutphy_en,
      pll0_locked_out => \^pll0_locked_out\,
      rst => rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_hssio_wiz_top is
  port (
    ch2_out3_P : out STD_LOGIC;
    ch2_out3_N : out STD_LOGIC;
    ch2_out0_P : out STD_LOGIC;
    ch2_out0_N : out STD_LOGIC;
    ch3_out1_P : out STD_LOGIC;
    ch3_out1_N : out STD_LOGIC;
    ch1_out7_P : out STD_LOGIC;
    ch1_out7_N : out STD_LOGIC;
    ch3_out2_P : out STD_LOGIC;
    ch3_out2_N : out STD_LOGIC;
    ch2_out6_P : out STD_LOGIC;
    ch2_out6_N : out STD_LOGIC;
    ch3_out0_P : out STD_LOGIC;
    ch3_out0_N : out STD_LOGIC;
    ch2_out7_P : out STD_LOGIC;
    ch2_out7_N : out STD_LOGIC;
    ch2_out5_P : out STD_LOGIC;
    ch2_out5_N : out STD_LOGIC;
    ch2_out2_P : out STD_LOGIC;
    ch2_out2_N : out STD_LOGIC;
    ch3_out4_P : out STD_LOGIC;
    ch3_out4_N : out STD_LOGIC;
    ch3_out3_P : out STD_LOGIC;
    ch3_out3_N : out STD_LOGIC;
    ch3_out5_P : out STD_LOGIC;
    ch3_out5_N : out STD_LOGIC;
    ch3_out7_P : out STD_LOGIC;
    ch3_out7_N : out STD_LOGIC;
    ch3_out6_P : out STD_LOGIC;
    ch3_out6_N : out STD_LOGIC;
    TH_out_P : out STD_LOGIC;
    TH_out_N : out STD_LOGIC;
    dly_rdy_bsc0 : out STD_LOGIC;
    vtc_rdy_bsc0 : out STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    pll0_clkout1_out : out STD_LOGIC;
    dly_rdy_bsc1 : out STD_LOGIC;
    vtc_rdy_bsc1 : out STD_LOGIC;
    dly_rdy_bsc2 : out STD_LOGIC;
    vtc_rdy_bsc2 : out STD_LOGIC;
    dly_rdy_bsc3 : out STD_LOGIC;
    vtc_rdy_bsc3 : out STD_LOGIC;
    dly_rdy_bsc4 : out STD_LOGIC;
    vtc_rdy_bsc4 : out STD_LOGIC;
    dly_rdy_bsc5 : out STD_LOGIC;
    vtc_rdy_bsc5 : out STD_LOGIC;
    pll0_locked_out : out STD_LOGIC;
    pll0_clkout0_out : out STD_LOGIC;
    \GEN_RIU_FROM_PLL.rst_seq_done_reg\ : out STD_LOGIC;
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    multi_intf_lock_in : in STD_LOGIC;
    tri_tbyte6 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    tri_tbyte7 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    en_vtc_bsc0 : in STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC
  );
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_hssio_wiz_top;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_hssio_wiz_top is
  signal all_bsc_dly_rdy_in : STD_LOGIC;
  signal bs_ctrl_top_inst_n_2 : STD_LOGIC;
  signal bs_rst : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal bs_to_buf_data : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal bsctrl_rst : STD_LOGIC;
  signal core_rdy : STD_LOGIC;
  signal core_rdy_r : STD_LOGIC_VECTOR ( 51 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of core_rdy_r : signal is "true";
  signal n0_en_vtc : STD_LOGIC;
  signal n0_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n0_tbyte_d : signal is "true";
  signal n0_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n0_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_en_vtc : STD_LOGIC;
  signal n1_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n1_tbyte_d : signal is "true";
  signal n1_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n1_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_en_vtc : STD_LOGIC;
  signal n2_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n2_tbyte_d : signal is "true";
  signal n2_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n2_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_en_vtc : STD_LOGIC;
  signal n3_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n3_tbyte_d : signal is "true";
  signal n3_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n3_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_en_vtc : STD_LOGIC;
  signal n4_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_rx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n4_tbyte_d : signal is "true";
  signal n4_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_in2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_in4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out2 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n4_tx_bit_ctrl_out4 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_en_vtc : STD_LOGIC;
  signal n5_rx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_rx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n5_tbyte_d : signal is "true";
  signal n5_tx_bit_ctrl_in0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n5_tx_bit_ctrl_out0 : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal n6_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n6_tbyte_d : signal is "true";
  signal n7_tbyte_d : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP of n7_tbyte_d : signal is "true";
  signal \^pll0_clkout0_out\ : STD_LOGIC;
  signal \^pll0_clkout1_out\ : STD_LOGIC;
  signal rst_dly : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal rst_seq_done1 : STD_LOGIC;
  attribute RTL_KEEP of rst_seq_done1 : signal is "true";
  signal rst_seq_done2 : STD_LOGIC;
  attribute RTL_KEEP of rst_seq_done2 : signal is "true";
  signal rst_seq_done3 : STD_LOGIC;
  attribute RTL_KEEP of rst_seq_done3 : signal is "true";
  signal rst_seq_done_pll0_sync : STD_LOGIC;
  signal \^shared_pll0_clkoutphy_out\ : STD_LOGIC;
  signal sync_cell_rst_seq_pll0_inst_n_1 : STD_LOGIC;
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \CORE_RDY_GEN[10].core_rdy_r_reg[10]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[11].core_rdy_r_reg[11]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[12].core_rdy_r_reg[12]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[13].core_rdy_r_reg[13]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[14].core_rdy_r_reg[14]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[15].core_rdy_r_reg[15]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[16].core_rdy_r_reg[16]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[17].core_rdy_r_reg[17]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[18].core_rdy_r_reg[18]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[19].core_rdy_r_reg[19]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[1].core_rdy_r_reg[1]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[20].core_rdy_r_reg[20]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[21].core_rdy_r_reg[21]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[22].core_rdy_r_reg[22]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[23].core_rdy_r_reg[23]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[24].core_rdy_r_reg[24]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[25].core_rdy_r_reg[25]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[26].core_rdy_r_reg[26]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[27].core_rdy_r_reg[27]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[28].core_rdy_r_reg[28]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[29].core_rdy_r_reg[29]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[2].core_rdy_r_reg[2]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[30].core_rdy_r_reg[30]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[31].core_rdy_r_reg[31]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[32].core_rdy_r_reg[32]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[33].core_rdy_r_reg[33]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[34].core_rdy_r_reg[34]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[35].core_rdy_r_reg[35]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[36].core_rdy_r_reg[36]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[37].core_rdy_r_reg[37]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[38].core_rdy_r_reg[38]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[39].core_rdy_r_reg[39]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[3].core_rdy_r_reg[3]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[40].core_rdy_r_reg[40]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[41].core_rdy_r_reg[41]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[42].core_rdy_r_reg[42]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[43].core_rdy_r_reg[43]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[44].core_rdy_r_reg[44]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[45].core_rdy_r_reg[45]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[46].core_rdy_r_reg[46]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[47].core_rdy_r_reg[47]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[48].core_rdy_r_reg[48]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[49].core_rdy_r_reg[49]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[4].core_rdy_r_reg[4]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[50].core_rdy_r_reg[50]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[51].core_rdy_r_reg[51]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[5].core_rdy_r_reg[5]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[6].core_rdy_r_reg[6]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[7].core_rdy_r_reg[7]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[8].core_rdy_r_reg[8]\ : label is "no";
  attribute equivalent_register_removal of \CORE_RDY_GEN[9].core_rdy_r_reg[9]\ : label is "no";
  attribute KEEP : string;
  attribute KEEP of \GEN_NIB0_TBYTE.n0_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB0_TBYTE.n0_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB0_TBYTE.n0_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB0_TBYTE.n0_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB1_TBYTE.n1_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB1_TBYTE.n1_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB1_TBYTE.n1_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB1_TBYTE.n1_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB2_TBYTE.n2_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB2_TBYTE.n2_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB2_TBYTE.n2_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB2_TBYTE.n2_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB3_TBYTE.n3_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB3_TBYTE.n3_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB3_TBYTE.n3_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB3_TBYTE.n3_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB4_TBYTE.n4_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB4_TBYTE.n4_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB4_TBYTE.n4_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB4_TBYTE.n4_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB5_TBYTE.n5_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB5_TBYTE.n5_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB5_TBYTE.n5_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB5_TBYTE.n5_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB6_TBYTE.n6_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB6_TBYTE.n6_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB6_TBYTE.n6_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB6_TBYTE.n6_tbyte_d_reg[3]\ : label is "yes";
  attribute KEEP of \GEN_NIB7_TBYTE.n7_tbyte_d_reg[0]\ : label is "yes";
  attribute KEEP of \GEN_NIB7_TBYTE.n7_tbyte_d_reg[1]\ : label is "yes";
  attribute KEEP of \GEN_NIB7_TBYTE.n7_tbyte_d_reg[2]\ : label is "yes";
  attribute KEEP of \GEN_NIB7_TBYTE.n7_tbyte_d_reg[3]\ : label is "yes";
begin
  \GEN_RIU_FROM_PLL.rst_seq_done_reg\ <= rst_seq_done3;
  pll0_clkout0_out <= \^pll0_clkout0_out\;
  pll0_clkout1_out <= \^pll0_clkout1_out\;
  shared_pll0_clkoutphy_out <= \^shared_pll0_clkoutphy_out\;
\CORE_RDY_GEN[0].core_rdy_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(0),
      R => '0'
    );
\CORE_RDY_GEN[10].core_rdy_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(10),
      R => '0'
    );
\CORE_RDY_GEN[11].core_rdy_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(11),
      R => '0'
    );
\CORE_RDY_GEN[12].core_rdy_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(12),
      R => '0'
    );
\CORE_RDY_GEN[13].core_rdy_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(13),
      R => '0'
    );
\CORE_RDY_GEN[14].core_rdy_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(14),
      R => '0'
    );
\CORE_RDY_GEN[15].core_rdy_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(15),
      R => '0'
    );
\CORE_RDY_GEN[16].core_rdy_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(16),
      R => '0'
    );
\CORE_RDY_GEN[17].core_rdy_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(17),
      R => '0'
    );
\CORE_RDY_GEN[18].core_rdy_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(18),
      R => '0'
    );
\CORE_RDY_GEN[19].core_rdy_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(19),
      R => '0'
    );
\CORE_RDY_GEN[1].core_rdy_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(1),
      R => '0'
    );
\CORE_RDY_GEN[20].core_rdy_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(20),
      R => '0'
    );
\CORE_RDY_GEN[21].core_rdy_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(21),
      R => '0'
    );
\CORE_RDY_GEN[22].core_rdy_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(22),
      R => '0'
    );
\CORE_RDY_GEN[23].core_rdy_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(23),
      R => '0'
    );
\CORE_RDY_GEN[24].core_rdy_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(24),
      R => '0'
    );
\CORE_RDY_GEN[25].core_rdy_r_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(25),
      R => '0'
    );
\CORE_RDY_GEN[26].core_rdy_r_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(26),
      R => '0'
    );
\CORE_RDY_GEN[27].core_rdy_r_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(27),
      R => '0'
    );
\CORE_RDY_GEN[28].core_rdy_r_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(28),
      R => '0'
    );
\CORE_RDY_GEN[29].core_rdy_r_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(29),
      R => '0'
    );
\CORE_RDY_GEN[2].core_rdy_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(2),
      R => '0'
    );
\CORE_RDY_GEN[30].core_rdy_r_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(30),
      R => '0'
    );
\CORE_RDY_GEN[31].core_rdy_r_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(31),
      R => '0'
    );
\CORE_RDY_GEN[32].core_rdy_r_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(32),
      R => '0'
    );
\CORE_RDY_GEN[33].core_rdy_r_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(33),
      R => '0'
    );
\CORE_RDY_GEN[34].core_rdy_r_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(34),
      R => '0'
    );
\CORE_RDY_GEN[35].core_rdy_r_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(35),
      R => '0'
    );
\CORE_RDY_GEN[36].core_rdy_r_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(36),
      R => '0'
    );
\CORE_RDY_GEN[37].core_rdy_r_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(37),
      R => '0'
    );
\CORE_RDY_GEN[38].core_rdy_r_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(38),
      R => '0'
    );
\CORE_RDY_GEN[39].core_rdy_r_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(39),
      R => '0'
    );
\CORE_RDY_GEN[3].core_rdy_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(3),
      R => '0'
    );
\CORE_RDY_GEN[40].core_rdy_r_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(40),
      R => '0'
    );
\CORE_RDY_GEN[41].core_rdy_r_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(41),
      R => '0'
    );
\CORE_RDY_GEN[42].core_rdy_r_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(42),
      R => '0'
    );
\CORE_RDY_GEN[43].core_rdy_r_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(43),
      R => '0'
    );
\CORE_RDY_GEN[44].core_rdy_r_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(44),
      R => '0'
    );
\CORE_RDY_GEN[45].core_rdy_r_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(45),
      R => '0'
    );
\CORE_RDY_GEN[46].core_rdy_r_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(46),
      R => '0'
    );
\CORE_RDY_GEN[47].core_rdy_r_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(47),
      R => '0'
    );
\CORE_RDY_GEN[48].core_rdy_r_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(48),
      R => '0'
    );
\CORE_RDY_GEN[49].core_rdy_r_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(49),
      R => '0'
    );
\CORE_RDY_GEN[4].core_rdy_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(4),
      R => '0'
    );
\CORE_RDY_GEN[50].core_rdy_r_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(50),
      R => '0'
    );
\CORE_RDY_GEN[51].core_rdy_r_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(51),
      R => '0'
    );
\CORE_RDY_GEN[5].core_rdy_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(5),
      R => '0'
    );
\CORE_RDY_GEN[6].core_rdy_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(6),
      R => '0'
    );
\CORE_RDY_GEN[7].core_rdy_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(7),
      R => '0'
    );
\CORE_RDY_GEN[8].core_rdy_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(8),
      R => '0'
    );
\CORE_RDY_GEN[9].core_rdy_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout1_out\,
      CE => '1',
      D => core_rdy,
      Q => core_rdy_r(9),
      R => '0'
    );
\GEN_IOBUF.iobuf_top_inst\: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_iobuf_top
     port map (
      bs_to_buf_data_in(15) => bs_to_buf_data(32),
      bs_to_buf_data_in(14) => bs_to_buf_data(30),
      bs_to_buf_data_in(13) => bs_to_buf_data(28),
      bs_to_buf_data_in(12) => bs_to_buf_data(26),
      bs_to_buf_data_in(11) => bs_to_buf_data(23),
      bs_to_buf_data_in(10) => bs_to_buf_data(21),
      bs_to_buf_data_in(9) => bs_to_buf_data(19),
      bs_to_buf_data_in(8) => bs_to_buf_data(17),
      bs_to_buf_data_in(7) => bs_to_buf_data(15),
      bs_to_buf_data_in(6) => bs_to_buf_data(13),
      bs_to_buf_data_in(5) => bs_to_buf_data(10),
      bs_to_buf_data_in(4) => bs_to_buf_data(8),
      bs_to_buf_data_in(3) => bs_to_buf_data(6),
      bs_to_buf_data_in(2) => bs_to_buf_data(4),
      bs_to_buf_data_in(1) => bs_to_buf_data(2),
      bs_to_buf_data_in(0) => bs_to_buf_data(0),
      data_to_pins(31) => TH_out_N,
      data_to_pins(30) => TH_out_P,
      data_to_pins(29) => ch3_out6_N,
      data_to_pins(28) => ch3_out6_P,
      data_to_pins(27) => ch3_out7_N,
      data_to_pins(26) => ch3_out7_P,
      data_to_pins(25) => ch3_out5_N,
      data_to_pins(24) => ch3_out5_P,
      data_to_pins(23) => ch3_out3_N,
      data_to_pins(22) => ch3_out3_P,
      data_to_pins(21) => ch3_out4_N,
      data_to_pins(20) => ch3_out4_P,
      data_to_pins(19) => ch2_out2_N,
      data_to_pins(18) => ch2_out2_P,
      data_to_pins(17) => ch2_out5_N,
      data_to_pins(16) => ch2_out5_P,
      data_to_pins(15) => ch2_out7_N,
      data_to_pins(14) => ch2_out7_P,
      data_to_pins(13) => ch3_out0_N,
      data_to_pins(12) => ch3_out0_P,
      data_to_pins(11) => ch2_out6_N,
      data_to_pins(10) => ch2_out6_P,
      data_to_pins(9) => ch3_out2_N,
      data_to_pins(8) => ch3_out2_P,
      data_to_pins(7) => ch1_out7_N,
      data_to_pins(6) => ch1_out7_P,
      data_to_pins(5) => ch3_out1_N,
      data_to_pins(4) => ch3_out1_P,
      data_to_pins(3) => ch2_out0_N,
      data_to_pins(2) => ch2_out0_P,
      data_to_pins(1) => ch2_out3_N,
      data_to_pins(0) => ch2_out3_P
    );
\GEN_NIB0_TBYTE.n0_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n0_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB0_TBYTE.n0_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n0_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB0_TBYTE.n0_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n0_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB0_TBYTE.n0_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n0_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB1_TBYTE.n1_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n1_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB1_TBYTE.n1_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n1_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB1_TBYTE.n1_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n1_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB1_TBYTE.n1_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n1_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB2_TBYTE.n2_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n2_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB2_TBYTE.n2_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n2_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB2_TBYTE.n2_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n2_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB2_TBYTE.n2_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n2_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB3_TBYTE.n3_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n3_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB3_TBYTE.n3_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n3_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB3_TBYTE.n3_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n3_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB3_TBYTE.n3_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n3_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB4_TBYTE.n4_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n4_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB4_TBYTE.n4_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n4_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB4_TBYTE.n4_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n4_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB4_TBYTE.n4_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n4_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB5_TBYTE.n5_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n5_tbyte_d(0),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB5_TBYTE.n5_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n5_tbyte_d(1),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB5_TBYTE.n5_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n5_tbyte_d(2),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB5_TBYTE.n5_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => rst_seq_done_pll0_sync,
      Q => n5_tbyte_d(3),
      R => sync_cell_rst_seq_pll0_inst_n_1
    );
\GEN_NIB6_TBYTE.n6_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte6(0),
      Q => n6_tbyte_d(0),
      R => '0'
    );
\GEN_NIB6_TBYTE.n6_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte6(1),
      Q => n6_tbyte_d(1),
      R => '0'
    );
\GEN_NIB6_TBYTE.n6_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte6(2),
      Q => n6_tbyte_d(2),
      R => '0'
    );
\GEN_NIB6_TBYTE.n6_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte6(3),
      Q => n6_tbyte_d(3),
      R => '0'
    );
\GEN_NIB7_TBYTE.n7_tbyte_d_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte7(0),
      Q => n7_tbyte_d(0),
      R => '0'
    );
\GEN_NIB7_TBYTE.n7_tbyte_d_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte7(1),
      Q => n7_tbyte_d(1),
      R => '0'
    );
\GEN_NIB7_TBYTE.n7_tbyte_d_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte7(2),
      Q => n7_tbyte_d(2),
      R => '0'
    );
\GEN_NIB7_TBYTE.n7_tbyte_d_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^pll0_clkout0_out\,
      CE => '1',
      D => tri_tbyte7(3),
      Q => n7_tbyte_d(3),
      R => '0'
    );
bs_ctrl_top_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_ctrl_top
     port map (
      \BITSLICE_CTRL[4].bs_ctrl_inst_0\ => bs_ctrl_top_inst_n_2,
      bsctrl_rst => bsctrl_rst,
      core_rdy => core_rdy,
      dly_rdy_bsc0 => dly_rdy_bsc0,
      dly_rdy_bsc1 => dly_rdy_bsc1,
      dly_rdy_bsc2 => dly_rdy_bsc2,
      dly_rdy_bsc3 => dly_rdy_bsc3,
      dly_rdy_bsc4 => dly_rdy_bsc4,
      dly_rdy_bsc5 => dly_rdy_bsc5,
      in0(0) => all_bsc_dly_rdy_in,
      n0_en_vtc_in => n0_en_vtc,
      n0_rx_bit_ctrl_out0(39 downto 0) => n0_rx_bit_ctrl_out0(39 downto 0),
      n0_rx_bit_ctrl_out2(39 downto 0) => n0_rx_bit_ctrl_out2(39 downto 0),
      n0_rx_bit_ctrl_out4(39 downto 0) => n0_rx_bit_ctrl_out4(39 downto 0),
      n0_tbyte_in(3 downto 0) => n0_tbyte_d(3 downto 0),
      n0_tx_bit_ctrl_out0(39 downto 0) => n0_tx_bit_ctrl_out0(39 downto 0),
      n0_tx_bit_ctrl_out2(39 downto 0) => n0_tx_bit_ctrl_out2(39 downto 0),
      n0_tx_bit_ctrl_out4(39 downto 0) => n0_tx_bit_ctrl_out4(39 downto 0),
      n1_en_vtc_in => n1_en_vtc,
      n1_rx_bit_ctrl_out0(39 downto 0) => n1_rx_bit_ctrl_out0(39 downto 0),
      n1_rx_bit_ctrl_out2(39 downto 0) => n1_rx_bit_ctrl_out2(39 downto 0),
      n1_rx_bit_ctrl_out4(39 downto 0) => n1_rx_bit_ctrl_out4(39 downto 0),
      n1_tbyte_in(3 downto 0) => n1_tbyte_d(3 downto 0),
      n1_tx_bit_ctrl_out0(39 downto 0) => n1_tx_bit_ctrl_out0(39 downto 0),
      n1_tx_bit_ctrl_out2(39 downto 0) => n1_tx_bit_ctrl_out2(39 downto 0),
      n1_tx_bit_ctrl_out4(39 downto 0) => n1_tx_bit_ctrl_out4(39 downto 0),
      n2_en_vtc_in => n2_en_vtc,
      n2_rx_bit_ctrl_out0(39 downto 0) => n2_rx_bit_ctrl_out0(39 downto 0),
      n2_rx_bit_ctrl_out2(39 downto 0) => n2_rx_bit_ctrl_out2(39 downto 0),
      n2_rx_bit_ctrl_out4(39 downto 0) => n2_rx_bit_ctrl_out4(39 downto 0),
      n2_tbyte_in(3 downto 0) => n2_tbyte_d(3 downto 0),
      n2_tx_bit_ctrl_out0(39 downto 0) => n2_tx_bit_ctrl_out0(39 downto 0),
      n2_tx_bit_ctrl_out2(39 downto 0) => n2_tx_bit_ctrl_out2(39 downto 0),
      n2_tx_bit_ctrl_out4(39 downto 0) => n2_tx_bit_ctrl_out4(39 downto 0),
      n3_en_vtc_in => n3_en_vtc,
      n3_rx_bit_ctrl_out0(39 downto 0) => n3_rx_bit_ctrl_out0(39 downto 0),
      n3_rx_bit_ctrl_out2(39 downto 0) => n3_rx_bit_ctrl_out2(39 downto 0),
      n3_rx_bit_ctrl_out4(39 downto 0) => n3_rx_bit_ctrl_out4(39 downto 0),
      n3_tbyte_in(3 downto 0) => n3_tbyte_d(3 downto 0),
      n3_tx_bit_ctrl_out0(39 downto 0) => n3_tx_bit_ctrl_out0(39 downto 0),
      n3_tx_bit_ctrl_out2(39 downto 0) => n3_tx_bit_ctrl_out2(39 downto 0),
      n3_tx_bit_ctrl_out4(39 downto 0) => n3_tx_bit_ctrl_out4(39 downto 0),
      n4_en_vtc_in => n4_en_vtc,
      n4_rx_bit_ctrl_out0(39 downto 0) => n4_rx_bit_ctrl_out0(39 downto 0),
      n4_rx_bit_ctrl_out2(39 downto 0) => n4_rx_bit_ctrl_out2(39 downto 0),
      n4_rx_bit_ctrl_out4(39 downto 0) => n4_rx_bit_ctrl_out4(39 downto 0),
      n4_tbyte_in(3 downto 0) => n4_tbyte_d(3 downto 0),
      n4_tx_bit_ctrl_out0(39 downto 0) => n4_tx_bit_ctrl_out0(39 downto 0),
      n4_tx_bit_ctrl_out2(39 downto 0) => n4_tx_bit_ctrl_out2(39 downto 0),
      n4_tx_bit_ctrl_out4(39 downto 0) => n4_tx_bit_ctrl_out4(39 downto 0),
      n5_en_vtc_in => n5_en_vtc,
      n5_rx_bit_ctrl_out0(39 downto 0) => n5_rx_bit_ctrl_out0(39 downto 0),
      n5_tbyte_in(3 downto 0) => n5_tbyte_d(3 downto 0),
      n5_tx_bit_ctrl_out0(39 downto 0) => n5_tx_bit_ctrl_out0(39 downto 0),
      pll0_clkout1_out => \^pll0_clkout1_out\,
      shared_pll0_clkoutphy_out => \^shared_pll0_clkoutphy_out\,
      tx_bs0_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in0(39 downto 0),
      tx_bs0_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in0(39 downto 0),
      tx_bs10_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in4(39 downto 0),
      tx_bs10_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in4(39 downto 0),
      tx_bs13_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in0(39 downto 0),
      tx_bs13_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in0(39 downto 0),
      tx_bs15_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in2(39 downto 0),
      tx_bs15_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in2(39 downto 0),
      tx_bs17_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in4(39 downto 0),
      tx_bs17_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in4(39 downto 0),
      tx_bs19_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in0(39 downto 0),
      tx_bs19_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in0(39 downto 0),
      tx_bs21_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in2(39 downto 0),
      tx_bs21_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in2(39 downto 0),
      tx_bs23_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in4(39 downto 0),
      tx_bs23_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in4(39 downto 0),
      tx_bs26_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in0(39 downto 0),
      tx_bs26_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in0(39 downto 0),
      tx_bs28_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in2(39 downto 0),
      tx_bs28_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in2(39 downto 0),
      tx_bs2_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in2(39 downto 0),
      tx_bs2_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in2(39 downto 0),
      tx_bs30_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in4(39 downto 0),
      tx_bs30_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in4(39 downto 0),
      tx_bs32_rx_bit_ctrl_out(39 downto 0) => n5_rx_bit_ctrl_in0(39 downto 0),
      tx_bs32_tx_bit_ctrl_out(39 downto 0) => n5_tx_bit_ctrl_in0(39 downto 0),
      tx_bs4_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in4(39 downto 0),
      tx_bs4_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in4(39 downto 0),
      tx_bs6_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in0(39 downto 0),
      tx_bs6_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in0(39 downto 0),
      tx_bs8_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in2(39 downto 0),
      tx_bs8_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in2(39 downto 0),
      vtc_rdy_bsc0 => vtc_rdy_bsc0,
      vtc_rdy_bsc1 => vtc_rdy_bsc1,
      vtc_rdy_bsc2 => vtc_rdy_bsc2,
      vtc_rdy_bsc3 => vtc_rdy_bsc3,
      vtc_rdy_bsc4 => vtc_rdy_bsc4,
      vtc_rdy_bsc5 => vtc_rdy_bsc5
    );
bs_top_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_bs_top
     port map (
      bs_to_buf_data_in(15) => bs_to_buf_data(32),
      bs_to_buf_data_in(14) => bs_to_buf_data(30),
      bs_to_buf_data_in(13) => bs_to_buf_data(28),
      bs_to_buf_data_in(12) => bs_to_buf_data(26),
      bs_to_buf_data_in(11) => bs_to_buf_data(23),
      bs_to_buf_data_in(10) => bs_to_buf_data(21),
      bs_to_buf_data_in(9) => bs_to_buf_data(19),
      bs_to_buf_data_in(8) => bs_to_buf_data(17),
      bs_to_buf_data_in(7) => bs_to_buf_data(15),
      bs_to_buf_data_in(6) => bs_to_buf_data(13),
      bs_to_buf_data_in(5) => bs_to_buf_data(10),
      bs_to_buf_data_in(4) => bs_to_buf_data(8),
      bs_to_buf_data_in(3) => bs_to_buf_data(6),
      bs_to_buf_data_in(2) => bs_to_buf_data(4),
      bs_to_buf_data_in(1) => bs_to_buf_data(2),
      bs_to_buf_data_in(0) => bs_to_buf_data(0),
      data_from_fabric_TH_out_P(7 downto 0) => data_from_fabric_TH_out_P(7 downto 0),
      data_from_fabric_ch1_out7_P(7 downto 0) => data_from_fabric_ch1_out7_P(7 downto 0),
      data_from_fabric_ch2_out0_P(7 downto 0) => data_from_fabric_ch2_out0_P(7 downto 0),
      data_from_fabric_ch2_out2_P(7 downto 0) => data_from_fabric_ch2_out2_P(7 downto 0),
      data_from_fabric_ch2_out3_P(7 downto 0) => data_from_fabric_ch2_out3_P(7 downto 0),
      data_from_fabric_ch2_out5_P(7 downto 0) => data_from_fabric_ch2_out5_P(7 downto 0),
      data_from_fabric_ch2_out6_P(7 downto 0) => data_from_fabric_ch2_out6_P(7 downto 0),
      data_from_fabric_ch2_out7_P(7 downto 0) => data_from_fabric_ch2_out7_P(7 downto 0),
      data_from_fabric_ch3_out0_P(7 downto 0) => data_from_fabric_ch3_out0_P(7 downto 0),
      data_from_fabric_ch3_out1_P(7 downto 0) => data_from_fabric_ch3_out1_P(7 downto 0),
      data_from_fabric_ch3_out2_P(7 downto 0) => data_from_fabric_ch3_out2_P(7 downto 0),
      data_from_fabric_ch3_out3_P(7 downto 0) => data_from_fabric_ch3_out3_P(7 downto 0),
      data_from_fabric_ch3_out4_P(7 downto 0) => data_from_fabric_ch3_out4_P(7 downto 0),
      data_from_fabric_ch3_out5_P(7 downto 0) => data_from_fabric_ch3_out5_P(7 downto 0),
      data_from_fabric_ch3_out6_P(7 downto 0) => data_from_fabric_ch3_out6_P(7 downto 0),
      data_from_fabric_ch3_out7_P(7 downto 0) => data_from_fabric_ch3_out7_P(7 downto 0),
      tx_bs0_rx_bit_ctrl_in(39 downto 0) => n0_rx_bit_ctrl_out0(39 downto 0),
      tx_bs0_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in0(39 downto 0),
      tx_bs0_tx_bit_ctrl_in(39 downto 0) => n0_tx_bit_ctrl_out0(39 downto 0),
      tx_bs0_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in0(39 downto 0),
      tx_bs10_rx_bit_ctrl_in(39 downto 0) => n1_rx_bit_ctrl_out4(39 downto 0),
      tx_bs10_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in4(39 downto 0),
      tx_bs10_tx_bit_ctrl_in(39 downto 0) => n1_tx_bit_ctrl_out4(39 downto 0),
      tx_bs10_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in4(39 downto 0),
      tx_bs13_rx_bit_ctrl_in(39 downto 0) => n2_rx_bit_ctrl_out0(39 downto 0),
      tx_bs13_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in0(39 downto 0),
      tx_bs13_tx_bit_ctrl_in(39 downto 0) => n2_tx_bit_ctrl_out0(39 downto 0),
      tx_bs13_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in0(39 downto 0),
      tx_bs15_rx_bit_ctrl_in(39 downto 0) => n2_rx_bit_ctrl_out2(39 downto 0),
      tx_bs15_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in2(39 downto 0),
      tx_bs15_tx_bit_ctrl_in(39 downto 0) => n2_tx_bit_ctrl_out2(39 downto 0),
      tx_bs15_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in2(39 downto 0),
      tx_bs17_rx_bit_ctrl_in(39 downto 0) => n2_rx_bit_ctrl_out4(39 downto 0),
      tx_bs17_rx_bit_ctrl_out(39 downto 0) => n2_rx_bit_ctrl_in4(39 downto 0),
      tx_bs17_tx_bit_ctrl_in(39 downto 0) => n2_tx_bit_ctrl_out4(39 downto 0),
      tx_bs17_tx_bit_ctrl_out(39 downto 0) => n2_tx_bit_ctrl_in4(39 downto 0),
      tx_bs19_rx_bit_ctrl_in(39 downto 0) => n3_rx_bit_ctrl_out0(39 downto 0),
      tx_bs19_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in0(39 downto 0),
      tx_bs19_tx_bit_ctrl_in(39 downto 0) => n3_tx_bit_ctrl_out0(39 downto 0),
      tx_bs19_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in0(39 downto 0),
      tx_bs21_rx_bit_ctrl_in(39 downto 0) => n3_rx_bit_ctrl_out2(39 downto 0),
      tx_bs21_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in2(39 downto 0),
      tx_bs21_tx_bit_ctrl_in(39 downto 0) => n3_tx_bit_ctrl_out2(39 downto 0),
      tx_bs21_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in2(39 downto 0),
      tx_bs23_rx_bit_ctrl_in(39 downto 0) => n3_rx_bit_ctrl_out4(39 downto 0),
      tx_bs23_rx_bit_ctrl_out(39 downto 0) => n3_rx_bit_ctrl_in4(39 downto 0),
      tx_bs23_tx_bit_ctrl_in(39 downto 0) => n3_tx_bit_ctrl_out4(39 downto 0),
      tx_bs23_tx_bit_ctrl_out(39 downto 0) => n3_tx_bit_ctrl_in4(39 downto 0),
      tx_bs26_rx_bit_ctrl_in(39 downto 0) => n4_rx_bit_ctrl_out0(39 downto 0),
      tx_bs26_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in0(39 downto 0),
      tx_bs26_tx_bit_ctrl_in(39 downto 0) => n4_tx_bit_ctrl_out0(39 downto 0),
      tx_bs26_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in0(39 downto 0),
      tx_bs28_rx_bit_ctrl_in(39 downto 0) => n4_rx_bit_ctrl_out2(39 downto 0),
      tx_bs28_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in2(39 downto 0),
      tx_bs28_tx_bit_ctrl_in(39 downto 0) => n4_tx_bit_ctrl_out2(39 downto 0),
      tx_bs28_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in2(39 downto 0),
      tx_bs2_rx_bit_ctrl_in(39 downto 0) => n0_rx_bit_ctrl_out2(39 downto 0),
      tx_bs2_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in2(39 downto 0),
      tx_bs2_tx_bit_ctrl_in(39 downto 0) => n0_tx_bit_ctrl_out2(39 downto 0),
      tx_bs2_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in2(39 downto 0),
      tx_bs30_rx_bit_ctrl_in(39 downto 0) => n4_rx_bit_ctrl_out4(39 downto 0),
      tx_bs30_rx_bit_ctrl_out(39 downto 0) => n4_rx_bit_ctrl_in4(39 downto 0),
      tx_bs30_tx_bit_ctrl_in(39 downto 0) => n4_tx_bit_ctrl_out4(39 downto 0),
      tx_bs30_tx_bit_ctrl_out(39 downto 0) => n4_tx_bit_ctrl_in4(39 downto 0),
      tx_bs32_rx_bit_ctrl_in(39 downto 0) => n5_rx_bit_ctrl_out0(39 downto 0),
      tx_bs32_rx_bit_ctrl_out(39 downto 0) => n5_rx_bit_ctrl_in0(39 downto 0),
      tx_bs32_tx_bit_ctrl_in(39 downto 0) => n5_tx_bit_ctrl_out0(39 downto 0),
      tx_bs32_tx_bit_ctrl_out(39 downto 0) => n5_tx_bit_ctrl_in0(39 downto 0),
      tx_bs4_rx_bit_ctrl_in(39 downto 0) => n0_rx_bit_ctrl_out4(39 downto 0),
      tx_bs4_rx_bit_ctrl_out(39 downto 0) => n0_rx_bit_ctrl_in4(39 downto 0),
      tx_bs4_tx_bit_ctrl_in(39 downto 0) => n0_tx_bit_ctrl_out4(39 downto 0),
      tx_bs4_tx_bit_ctrl_out(39 downto 0) => n0_tx_bit_ctrl_in4(39 downto 0),
      tx_bs6_rx_bit_ctrl_in(39 downto 0) => n1_rx_bit_ctrl_out0(39 downto 0),
      tx_bs6_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in0(39 downto 0),
      tx_bs6_tx_bit_ctrl_in(39 downto 0) => n1_tx_bit_ctrl_out0(39 downto 0),
      tx_bs6_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in0(39 downto 0),
      tx_bs8_rx_bit_ctrl_in(39 downto 0) => n1_rx_bit_ctrl_out2(39 downto 0),
      tx_bs8_rx_bit_ctrl_out(39 downto 0) => n1_rx_bit_ctrl_in2(39 downto 0),
      tx_bs8_tx_bit_ctrl_in(39 downto 0) => n1_tx_bit_ctrl_out2(39 downto 0),
      tx_bs8_tx_bit_ctrl_out(39 downto 0) => n1_tx_bit_ctrl_in2(39 downto 0),
      tx_bs_rst_dly_in(15) => rst_dly(32),
      tx_bs_rst_dly_in(14) => rst_dly(30),
      tx_bs_rst_dly_in(13) => rst_dly(28),
      tx_bs_rst_dly_in(12) => rst_dly(26),
      tx_bs_rst_dly_in(11) => rst_dly(23),
      tx_bs_rst_dly_in(10) => rst_dly(21),
      tx_bs_rst_dly_in(9) => rst_dly(19),
      tx_bs_rst_dly_in(8) => rst_dly(17),
      tx_bs_rst_dly_in(7) => rst_dly(15),
      tx_bs_rst_dly_in(6) => rst_dly(13),
      tx_bs_rst_dly_in(5) => rst_dly(10),
      tx_bs_rst_dly_in(4) => rst_dly(8),
      tx_bs_rst_dly_in(3) => rst_dly(6),
      tx_bs_rst_dly_in(2) => rst_dly(4),
      tx_bs_rst_dly_in(1) => rst_dly(2),
      tx_bs_rst_dly_in(0) => rst_dly(0),
      tx_bs_rst_in(15) => bs_rst(32),
      tx_bs_rst_in(14) => bs_rst(30),
      tx_bs_rst_in(13) => bs_rst(28),
      tx_bs_rst_in(12) => bs_rst(26),
      tx_bs_rst_in(11) => bs_rst(23),
      tx_bs_rst_in(10) => bs_rst(21),
      tx_bs_rst_in(9) => bs_rst(19),
      tx_bs_rst_in(8) => bs_rst(17),
      tx_bs_rst_in(7) => bs_rst(15),
      tx_bs_rst_in(6) => bs_rst(13),
      tx_bs_rst_in(5) => bs_rst(10),
      tx_bs_rst_in(4) => bs_rst(8),
      tx_bs_rst_in(3) => bs_rst(6),
      tx_bs_rst_in(2) => bs_rst(4),
      tx_bs_rst_in(1) => bs_rst(2),
      tx_bs_rst_in(0) => bs_rst(0)
    );
clk_rst_top_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_clk_rst_top
     port map (
      \GEN_RIU_FROM_PLL.hssio_state_reg[7]\ => bs_ctrl_top_inst_n_2,
      \bs_rst_int_r_reg[32]\(15) => bs_rst(32),
      \bs_rst_int_r_reg[32]\(14) => bs_rst(30),
      \bs_rst_int_r_reg[32]\(13) => bs_rst(28),
      \bs_rst_int_r_reg[32]\(12) => bs_rst(26),
      \bs_rst_int_r_reg[32]\(11) => bs_rst(23),
      \bs_rst_int_r_reg[32]\(10) => bs_rst(21),
      \bs_rst_int_r_reg[32]\(9) => bs_rst(19),
      \bs_rst_int_r_reg[32]\(8) => bs_rst(17),
      \bs_rst_int_r_reg[32]\(7) => bs_rst(15),
      \bs_rst_int_r_reg[32]\(6) => bs_rst(13),
      \bs_rst_int_r_reg[32]\(5) => bs_rst(10),
      \bs_rst_int_r_reg[32]\(4) => bs_rst(8),
      \bs_rst_int_r_reg[32]\(3) => bs_rst(6),
      \bs_rst_int_r_reg[32]\(2) => bs_rst(4),
      \bs_rst_int_r_reg[32]\(1) => bs_rst(2),
      \bs_rst_int_r_reg[32]\(0) => bs_rst(0),
      bsctrl_rst => bsctrl_rst,
      clk => clk,
      en_vtc_bsc0 => en_vtc_bsc0,
      en_vtc_bsc1 => en_vtc_bsc1,
      en_vtc_bsc2 => en_vtc_bsc2,
      en_vtc_bsc3 => en_vtc_bsc3,
      en_vtc_bsc4 => en_vtc_bsc4,
      en_vtc_bsc5 => en_vtc_bsc5,
      in0(0) => all_bsc_dly_rdy_in,
      multi_intf_lock_in => multi_intf_lock_in,
      n0_en_vtc_in => n0_en_vtc,
      n1_en_vtc_in => n1_en_vtc,
      n2_en_vtc_in => n2_en_vtc,
      n3_en_vtc_in => n3_en_vtc,
      n4_en_vtc_in => n4_en_vtc,
      n5_en_vtc_in => n5_en_vtc,
      \out\(15) => rst_dly(32),
      \out\(14) => rst_dly(30),
      \out\(13) => rst_dly(28),
      \out\(12) => rst_dly(26),
      \out\(11) => rst_dly(23),
      \out\(10) => rst_dly(21),
      \out\(9) => rst_dly(19),
      \out\(8) => rst_dly(17),
      \out\(7) => rst_dly(15),
      \out\(6) => rst_dly(13),
      \out\(5) => rst_dly(10),
      \out\(4) => rst_dly(8),
      \out\(3) => rst_dly(6),
      \out\(2) => rst_dly(4),
      \out\(1) => rst_dly(2),
      \out\(0) => rst_dly(0),
      pll0_clkout0_out => \^pll0_clkout0_out\,
      pll0_clkout1_out => \^pll0_clkout1_out\,
      pll0_locked_out => pll0_locked_out,
      rst => rst,
      rst_seq_done1 => rst_seq_done3,
      shared_pll0_clkoutphy_out => \^shared_pll0_clkoutphy_out\
    );
rst_seq_done1_inst: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rst_seq_done3,
      O => rst_seq_done1
    );
rst_seq_done2_inst: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rst_seq_done3,
      O => rst_seq_done2
    );
sync_cell_rst_seq_pll0_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9_sync_cell
     port map (
      CLK => \^pll0_clkout0_out\,
      SR(0) => sync_cell_rst_seq_pll0_inst_n_1,
      \out\ => rst_seq_done1,
      \sync_flop_1_reg[0]_0\ => rst_seq_done_pll0_sync
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 is
  port (
    daddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    dclk : in STD_LOGIC;
    den : in STD_LOGIC;
    di : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dwe : in STD_LOGIC;
    do_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    drdy : out STD_LOGIC;
    clk_p : in STD_LOGIC;
    clk_n : in STD_LOGIC;
    clk : in STD_LOGIC;
    riu_clk : in STD_LOGIC;
    app_clk : in STD_LOGIC;
    bs_rst_dphy_in : in STD_LOGIC;
    rst_seq_done : out STD_LOGIC;
    pll0_clkout0 : out STD_LOGIC;
    pll0_locked : out STD_LOGIC;
    pll0_clkout1 : out STD_LOGIC;
    pll1_clkout0 : out STD_LOGIC;
    pll1_locked : out STD_LOGIC;
    intf_rdy : out STD_LOGIC;
    multi_intf_lock_in : in STD_LOGIC;
    shared_pll0_clkout0_in : in STD_LOGIC;
    shared_pll1_clkout0_in : in STD_LOGIC;
    shared_pll0_clkoutphy_in : in STD_LOGIC;
    shared_pll1_clkoutphy_in : in STD_LOGIC;
    shared_pll0_locked_in : in STD_LOGIC;
    shared_pll1_locked_in : in STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    shared_pll1_clkoutphy_out : out STD_LOGIC;
    clk_from_ibuf : out STD_LOGIC;
    lptx_t : in STD_LOGIC_VECTOR ( 15 downto 0 );
    lptx_i_p : in STD_LOGIC_VECTOR ( 15 downto 0 );
    lptx_i_n : in STD_LOGIC_VECTOR ( 15 downto 0 );
    hs_rx_disable : in STD_LOGIC_VECTOR ( 15 downto 0 );
    lp_rx_disable : in STD_LOGIC_VECTOR ( 15 downto 0 );
    lp_rx_o_p : out STD_LOGIC_VECTOR ( 15 downto 0 );
    lp_rx_o_n : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dly_rdy_bsc0 : out STD_LOGIC;
    vtc_rdy_bsc0 : out STD_LOGIC;
    en_vtc_bsc0 : in STD_LOGIC;
    phy_rden_bsc0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc1 : out STD_LOGIC;
    vtc_rdy_bsc1 : out STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    phy_rden_bsc1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc2 : out STD_LOGIC;
    vtc_rdy_bsc2 : out STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    phy_rden_bsc2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc3 : out STD_LOGIC;
    vtc_rdy_bsc3 : out STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    phy_rden_bsc3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc4 : out STD_LOGIC;
    vtc_rdy_bsc4 : out STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    phy_rden_bsc4 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc5 : out STD_LOGIC;
    vtc_rdy_bsc5 : out STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC;
    phy_rden_bsc5 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc6 : out STD_LOGIC;
    vtc_rdy_bsc6 : out STD_LOGIC;
    en_vtc_bsc6 : in STD_LOGIC;
    phy_rden_bsc6 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dly_rdy_bsc7 : out STD_LOGIC;
    vtc_rdy_bsc7 : out STD_LOGIC;
    en_vtc_bsc7 : in STD_LOGIC;
    phy_rden_bsc7 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    riu_addr_bg0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    riu_wr_data_bg0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg0 : in STD_LOGIC;
    riu_rd_data_bg0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg0 : out STD_LOGIC;
    riu_addr_bg0_bs0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg0_bs0 : in STD_LOGIC;
    riu_wr_data_bg0_bs0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg0_bs0 : in STD_LOGIC;
    riu_rd_data_bg0_bs0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg0_bs0 : out STD_LOGIC;
    riu_addr_bg0_bs1 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg0_bs1 : in STD_LOGIC;
    riu_wr_data_bg0_bs1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg0_bs1 : in STD_LOGIC;
    riu_rd_data_bg0_bs1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg0_bs1 : out STD_LOGIC;
    riu_addr_bg1 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    riu_wr_data_bg1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg1 : in STD_LOGIC;
    riu_rd_data_bg1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg1 : out STD_LOGIC;
    riu_addr_bg1_bs2 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg1_bs2 : in STD_LOGIC;
    riu_wr_data_bg1_bs2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg1_bs2 : in STD_LOGIC;
    riu_rd_data_bg1_bs2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg1_bs2 : out STD_LOGIC;
    riu_addr_bg1_bs3 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg1_bs3 : in STD_LOGIC;
    riu_wr_data_bg1_bs3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg1_bs3 : in STD_LOGIC;
    riu_rd_data_bg1_bs3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg1_bs3 : out STD_LOGIC;
    riu_addr_bg2 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    riu_wr_data_bg2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg2 : in STD_LOGIC;
    riu_rd_data_bg2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg2 : out STD_LOGIC;
    riu_addr_bg2_bs4 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg2_bs4 : in STD_LOGIC;
    riu_wr_data_bg2_bs4 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg2_bs4 : in STD_LOGIC;
    riu_rd_data_bg2_bs4 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg2_bs4 : out STD_LOGIC;
    riu_addr_bg2_bs5 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg2_bs5 : in STD_LOGIC;
    riu_wr_data_bg2_bs5 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg2_bs5 : in STD_LOGIC;
    riu_rd_data_bg2_bs5 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg2_bs5 : out STD_LOGIC;
    riu_addr_bg3 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg3 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    riu_wr_data_bg3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg3 : in STD_LOGIC;
    riu_rd_data_bg3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg3 : out STD_LOGIC;
    riu_addr_bg3_bs6 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg3_bs6 : in STD_LOGIC;
    riu_wr_data_bg3_bs6 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg3_bs6 : in STD_LOGIC;
    riu_rd_data_bg3_bs6 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg3_bs6 : out STD_LOGIC;
    riu_addr_bg3_bs7 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    riu_nibble_sel_bg3_bs7 : in STD_LOGIC;
    riu_wr_data_bg3_bs7 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_wr_en_bg3_bs7 : in STD_LOGIC;
    riu_rd_data_bg3_bs7 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    riu_valid_bg3_bs7 : out STD_LOGIC;
    tx_clk : in STD_LOGIC;
    rx_clk : in STD_LOGIC;
    bidir_tx_clk : in STD_LOGIC;
    bidir_rx_clk : in STD_LOGIC;
    bidir_tx_bs_tri_clk : in STD_LOGIC;
    bg0_pin0_nc : in STD_LOGIC;
    bg0_pin6_nc : in STD_LOGIC;
    bg1_pin0_nc : in STD_LOGIC;
    bg1_pin6_nc : in STD_LOGIC;
    bg2_pin0_nc : in STD_LOGIC;
    bg2_pin6_nc : in STD_LOGIC;
    bg3_pin0_nc : in STD_LOGIC;
    bg3_pin6_nc : in STD_LOGIC;
    start_bitslip : in STD_LOGIC;
    rx_bitslip_sync_done : out STD_LOGIC;
    rxtx_bitslip_sync_done : out STD_LOGIC;
    ch2_out3_P : out STD_LOGIC;
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_0 : in STD_LOGIC;
    tx_cntvaluein_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_0 : in STD_LOGIC;
    tx_en_vtc_0 : in STD_LOGIC;
    tx_inc_0 : in STD_LOGIC;
    tx_load_0 : in STD_LOGIC;
    rx_cntvalueout_0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_0 : in STD_LOGIC;
    rx_en_vtc_0 : in STD_LOGIC;
    rx_inc_0 : in STD_LOGIC;
    rx_load_0 : in STD_LOGIC;
    rx_cntvalueout_ext_0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_0 : in STD_LOGIC;
    rx_en_vtc_ext_0 : in STD_LOGIC;
    rx_inc_ext_0 : in STD_LOGIC;
    rx_load_ext_0 : in STD_LOGIC;
    fifo_empty_0 : out STD_LOGIC;
    fifo_rd_clk_0 : in STD_LOGIC;
    fifo_rd_en_0 : in STD_LOGIC;
    bitslip_error_0 : out STD_LOGIC;
    ch2_out3_N : out STD_LOGIC;
    tri_t_1 : in STD_LOGIC;
    tx_cntvaluein_1 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_1 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_1 : in STD_LOGIC;
    tx_en_vtc_1 : in STD_LOGIC;
    tx_inc_1 : in STD_LOGIC;
    tx_load_1 : in STD_LOGIC;
    rx_cntvalueout_1 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_1 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_1 : in STD_LOGIC;
    rx_en_vtc_1 : in STD_LOGIC;
    rx_inc_1 : in STD_LOGIC;
    rx_load_1 : in STD_LOGIC;
    rx_cntvalueout_ext_1 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_1 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_1 : in STD_LOGIC;
    rx_en_vtc_ext_1 : in STD_LOGIC;
    rx_inc_ext_1 : in STD_LOGIC;
    rx_load_ext_1 : in STD_LOGIC;
    fifo_empty_1 : out STD_LOGIC;
    fifo_rd_clk_1 : in STD_LOGIC;
    fifo_rd_en_1 : in STD_LOGIC;
    bitslip_error_1 : out STD_LOGIC;
    ch2_out0_P : out STD_LOGIC;
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_2 : in STD_LOGIC;
    tx_cntvaluein_2 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_2 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_2 : in STD_LOGIC;
    tx_en_vtc_2 : in STD_LOGIC;
    tx_inc_2 : in STD_LOGIC;
    tx_load_2 : in STD_LOGIC;
    rx_cntvalueout_2 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_2 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_2 : in STD_LOGIC;
    rx_en_vtc_2 : in STD_LOGIC;
    rx_inc_2 : in STD_LOGIC;
    rx_load_2 : in STD_LOGIC;
    rx_cntvalueout_ext_2 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_2 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_2 : in STD_LOGIC;
    rx_en_vtc_ext_2 : in STD_LOGIC;
    rx_inc_ext_2 : in STD_LOGIC;
    rx_load_ext_2 : in STD_LOGIC;
    fifo_empty_2 : out STD_LOGIC;
    fifo_rd_clk_2 : in STD_LOGIC;
    fifo_rd_en_2 : in STD_LOGIC;
    bitslip_error_2 : out STD_LOGIC;
    ch2_out0_N : out STD_LOGIC;
    tri_t_3 : in STD_LOGIC;
    tx_cntvaluein_3 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_3 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_3 : in STD_LOGIC;
    tx_en_vtc_3 : in STD_LOGIC;
    tx_inc_3 : in STD_LOGIC;
    tx_load_3 : in STD_LOGIC;
    rx_cntvalueout_3 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_3 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_3 : in STD_LOGIC;
    rx_en_vtc_3 : in STD_LOGIC;
    rx_inc_3 : in STD_LOGIC;
    rx_load_3 : in STD_LOGIC;
    rx_cntvalueout_ext_3 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_3 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_3 : in STD_LOGIC;
    rx_en_vtc_ext_3 : in STD_LOGIC;
    rx_inc_ext_3 : in STD_LOGIC;
    rx_load_ext_3 : in STD_LOGIC;
    fifo_empty_3 : out STD_LOGIC;
    fifo_rd_clk_3 : in STD_LOGIC;
    fifo_rd_en_3 : in STD_LOGIC;
    bitslip_error_3 : out STD_LOGIC;
    ch3_out1_P : out STD_LOGIC;
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_4 : in STD_LOGIC;
    tx_cntvaluein_4 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_4 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_4 : in STD_LOGIC;
    tx_en_vtc_4 : in STD_LOGIC;
    tx_inc_4 : in STD_LOGIC;
    tx_load_4 : in STD_LOGIC;
    rx_cntvalueout_4 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_4 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_4 : in STD_LOGIC;
    rx_en_vtc_4 : in STD_LOGIC;
    rx_inc_4 : in STD_LOGIC;
    rx_load_4 : in STD_LOGIC;
    rx_cntvalueout_ext_4 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_4 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_4 : in STD_LOGIC;
    rx_en_vtc_ext_4 : in STD_LOGIC;
    rx_inc_ext_4 : in STD_LOGIC;
    rx_load_ext_4 : in STD_LOGIC;
    fifo_empty_4 : out STD_LOGIC;
    fifo_rd_clk_4 : in STD_LOGIC;
    fifo_rd_en_4 : in STD_LOGIC;
    bitslip_error_4 : out STD_LOGIC;
    ch3_out1_N : out STD_LOGIC;
    tri_t_5 : in STD_LOGIC;
    tx_cntvaluein_5 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_5 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_5 : in STD_LOGIC;
    tx_en_vtc_5 : in STD_LOGIC;
    tx_inc_5 : in STD_LOGIC;
    tx_load_5 : in STD_LOGIC;
    rx_cntvalueout_5 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_5 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_5 : in STD_LOGIC;
    rx_en_vtc_5 : in STD_LOGIC;
    rx_inc_5 : in STD_LOGIC;
    rx_load_5 : in STD_LOGIC;
    rx_cntvalueout_ext_5 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_5 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_5 : in STD_LOGIC;
    rx_en_vtc_ext_5 : in STD_LOGIC;
    rx_inc_ext_5 : in STD_LOGIC;
    rx_load_ext_5 : in STD_LOGIC;
    fifo_empty_5 : out STD_LOGIC;
    fifo_rd_clk_5 : in STD_LOGIC;
    fifo_rd_en_5 : in STD_LOGIC;
    bitslip_error_5 : out STD_LOGIC;
    ch1_out7_P : out STD_LOGIC;
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_6 : in STD_LOGIC;
    tx_cntvaluein_6 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_6 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_6 : in STD_LOGIC;
    tx_en_vtc_6 : in STD_LOGIC;
    tx_inc_6 : in STD_LOGIC;
    tx_load_6 : in STD_LOGIC;
    rx_cntvalueout_6 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_6 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_6 : in STD_LOGIC;
    rx_en_vtc_6 : in STD_LOGIC;
    rx_inc_6 : in STD_LOGIC;
    rx_load_6 : in STD_LOGIC;
    rx_cntvalueout_ext_6 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_6 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_6 : in STD_LOGIC;
    rx_en_vtc_ext_6 : in STD_LOGIC;
    rx_inc_ext_6 : in STD_LOGIC;
    rx_load_ext_6 : in STD_LOGIC;
    fifo_empty_6 : out STD_LOGIC;
    fifo_rd_clk_6 : in STD_LOGIC;
    fifo_rd_en_6 : in STD_LOGIC;
    bitslip_error_6 : out STD_LOGIC;
    ch1_out7_N : out STD_LOGIC;
    tri_t_7 : in STD_LOGIC;
    tx_cntvaluein_7 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_7 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_7 : in STD_LOGIC;
    tx_en_vtc_7 : in STD_LOGIC;
    tx_inc_7 : in STD_LOGIC;
    tx_load_7 : in STD_LOGIC;
    rx_cntvalueout_7 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_7 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_7 : in STD_LOGIC;
    rx_en_vtc_7 : in STD_LOGIC;
    rx_inc_7 : in STD_LOGIC;
    rx_load_7 : in STD_LOGIC;
    rx_cntvalueout_ext_7 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_7 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_7 : in STD_LOGIC;
    rx_en_vtc_ext_7 : in STD_LOGIC;
    rx_inc_ext_7 : in STD_LOGIC;
    rx_load_ext_7 : in STD_LOGIC;
    fifo_empty_7 : out STD_LOGIC;
    fifo_rd_clk_7 : in STD_LOGIC;
    fifo_rd_en_7 : in STD_LOGIC;
    bitslip_error_7 : out STD_LOGIC;
    ch3_out2_P : out STD_LOGIC;
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_8 : in STD_LOGIC;
    tx_cntvaluein_8 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_8 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_8 : in STD_LOGIC;
    tx_en_vtc_8 : in STD_LOGIC;
    tx_inc_8 : in STD_LOGIC;
    tx_load_8 : in STD_LOGIC;
    rx_cntvalueout_8 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_8 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_8 : in STD_LOGIC;
    rx_en_vtc_8 : in STD_LOGIC;
    rx_inc_8 : in STD_LOGIC;
    rx_load_8 : in STD_LOGIC;
    rx_cntvalueout_ext_8 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_8 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_8 : in STD_LOGIC;
    rx_en_vtc_ext_8 : in STD_LOGIC;
    rx_inc_ext_8 : in STD_LOGIC;
    rx_load_ext_8 : in STD_LOGIC;
    fifo_empty_8 : out STD_LOGIC;
    fifo_rd_clk_8 : in STD_LOGIC;
    fifo_rd_en_8 : in STD_LOGIC;
    bitslip_error_8 : out STD_LOGIC;
    ch3_out2_N : out STD_LOGIC;
    tri_t_9 : in STD_LOGIC;
    tx_cntvaluein_9 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_9 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_9 : in STD_LOGIC;
    tx_en_vtc_9 : in STD_LOGIC;
    tx_inc_9 : in STD_LOGIC;
    tx_load_9 : in STD_LOGIC;
    rx_cntvalueout_9 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_9 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_9 : in STD_LOGIC;
    rx_en_vtc_9 : in STD_LOGIC;
    rx_inc_9 : in STD_LOGIC;
    rx_load_9 : in STD_LOGIC;
    rx_cntvalueout_ext_9 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_9 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_9 : in STD_LOGIC;
    rx_en_vtc_ext_9 : in STD_LOGIC;
    rx_inc_ext_9 : in STD_LOGIC;
    rx_load_ext_9 : in STD_LOGIC;
    fifo_empty_9 : out STD_LOGIC;
    fifo_rd_clk_9 : in STD_LOGIC;
    fifo_rd_en_9 : in STD_LOGIC;
    bitslip_error_9 : out STD_LOGIC;
    ch2_out6_P : out STD_LOGIC;
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_10 : in STD_LOGIC;
    tx_cntvaluein_10 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_10 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_10 : in STD_LOGIC;
    tx_en_vtc_10 : in STD_LOGIC;
    tx_inc_10 : in STD_LOGIC;
    tx_load_10 : in STD_LOGIC;
    rx_cntvalueout_10 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_10 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_10 : in STD_LOGIC;
    rx_en_vtc_10 : in STD_LOGIC;
    rx_inc_10 : in STD_LOGIC;
    rx_load_10 : in STD_LOGIC;
    rx_cntvalueout_ext_10 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_10 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_10 : in STD_LOGIC;
    rx_en_vtc_ext_10 : in STD_LOGIC;
    rx_inc_ext_10 : in STD_LOGIC;
    rx_load_ext_10 : in STD_LOGIC;
    fifo_empty_10 : out STD_LOGIC;
    fifo_rd_clk_10 : in STD_LOGIC;
    fifo_rd_en_10 : in STD_LOGIC;
    bitslip_error_10 : out STD_LOGIC;
    ch2_out6_N : out STD_LOGIC;
    tri_t_11 : in STD_LOGIC;
    tx_cntvaluein_11 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_11 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_11 : in STD_LOGIC;
    tx_en_vtc_11 : in STD_LOGIC;
    tx_inc_11 : in STD_LOGIC;
    tx_load_11 : in STD_LOGIC;
    rx_cntvalueout_11 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_11 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_11 : in STD_LOGIC;
    rx_en_vtc_11 : in STD_LOGIC;
    rx_inc_11 : in STD_LOGIC;
    rx_load_11 : in STD_LOGIC;
    rx_cntvalueout_ext_11 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_11 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_11 : in STD_LOGIC;
    rx_en_vtc_ext_11 : in STD_LOGIC;
    rx_inc_ext_11 : in STD_LOGIC;
    rx_load_ext_11 : in STD_LOGIC;
    fifo_empty_11 : out STD_LOGIC;
    fifo_rd_clk_11 : in STD_LOGIC;
    fifo_rd_en_11 : in STD_LOGIC;
    bitslip_error_11 : out STD_LOGIC;
    ch3_out0_P : out STD_LOGIC;
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_13 : in STD_LOGIC;
    tx_cntvaluein_13 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_13 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_13 : in STD_LOGIC;
    tx_en_vtc_13 : in STD_LOGIC;
    tx_inc_13 : in STD_LOGIC;
    tx_load_13 : in STD_LOGIC;
    rx_cntvalueout_13 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_13 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_13 : in STD_LOGIC;
    rx_en_vtc_13 : in STD_LOGIC;
    rx_inc_13 : in STD_LOGIC;
    rx_load_13 : in STD_LOGIC;
    rx_cntvalueout_ext_13 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_13 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_13 : in STD_LOGIC;
    rx_en_vtc_ext_13 : in STD_LOGIC;
    rx_inc_ext_13 : in STD_LOGIC;
    rx_load_ext_13 : in STD_LOGIC;
    fifo_empty_13 : out STD_LOGIC;
    fifo_rd_clk_13 : in STD_LOGIC;
    fifo_rd_en_13 : in STD_LOGIC;
    bitslip_error_13 : out STD_LOGIC;
    ch3_out0_N : out STD_LOGIC;
    tri_t_14 : in STD_LOGIC;
    tx_cntvaluein_14 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_14 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_14 : in STD_LOGIC;
    tx_en_vtc_14 : in STD_LOGIC;
    tx_inc_14 : in STD_LOGIC;
    tx_load_14 : in STD_LOGIC;
    rx_cntvalueout_14 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_14 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_14 : in STD_LOGIC;
    rx_en_vtc_14 : in STD_LOGIC;
    rx_inc_14 : in STD_LOGIC;
    rx_load_14 : in STD_LOGIC;
    rx_cntvalueout_ext_14 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_14 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_14 : in STD_LOGIC;
    rx_en_vtc_ext_14 : in STD_LOGIC;
    rx_inc_ext_14 : in STD_LOGIC;
    rx_load_ext_14 : in STD_LOGIC;
    fifo_empty_14 : out STD_LOGIC;
    fifo_rd_clk_14 : in STD_LOGIC;
    fifo_rd_en_14 : in STD_LOGIC;
    bitslip_error_14 : out STD_LOGIC;
    ch2_out7_P : out STD_LOGIC;
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_15 : in STD_LOGIC;
    tx_cntvaluein_15 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_15 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_15 : in STD_LOGIC;
    tx_en_vtc_15 : in STD_LOGIC;
    tx_inc_15 : in STD_LOGIC;
    tx_load_15 : in STD_LOGIC;
    rx_cntvalueout_15 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_15 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_15 : in STD_LOGIC;
    rx_en_vtc_15 : in STD_LOGIC;
    rx_inc_15 : in STD_LOGIC;
    rx_load_15 : in STD_LOGIC;
    rx_cntvalueout_ext_15 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_15 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_15 : in STD_LOGIC;
    rx_en_vtc_ext_15 : in STD_LOGIC;
    rx_inc_ext_15 : in STD_LOGIC;
    rx_load_ext_15 : in STD_LOGIC;
    fifo_empty_15 : out STD_LOGIC;
    fifo_rd_clk_15 : in STD_LOGIC;
    fifo_rd_en_15 : in STD_LOGIC;
    bitslip_error_15 : out STD_LOGIC;
    ch2_out7_N : out STD_LOGIC;
    tri_t_16 : in STD_LOGIC;
    tx_cntvaluein_16 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_16 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_16 : in STD_LOGIC;
    tx_en_vtc_16 : in STD_LOGIC;
    tx_inc_16 : in STD_LOGIC;
    tx_load_16 : in STD_LOGIC;
    rx_cntvalueout_16 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_16 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_16 : in STD_LOGIC;
    rx_en_vtc_16 : in STD_LOGIC;
    rx_inc_16 : in STD_LOGIC;
    rx_load_16 : in STD_LOGIC;
    rx_cntvalueout_ext_16 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_16 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_16 : in STD_LOGIC;
    rx_en_vtc_ext_16 : in STD_LOGIC;
    rx_inc_ext_16 : in STD_LOGIC;
    rx_load_ext_16 : in STD_LOGIC;
    fifo_empty_16 : out STD_LOGIC;
    fifo_rd_clk_16 : in STD_LOGIC;
    fifo_rd_en_16 : in STD_LOGIC;
    bitslip_error_16 : out STD_LOGIC;
    ch2_out5_P : out STD_LOGIC;
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_17 : in STD_LOGIC;
    tx_cntvaluein_17 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_17 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_17 : in STD_LOGIC;
    tx_en_vtc_17 : in STD_LOGIC;
    tx_inc_17 : in STD_LOGIC;
    tx_load_17 : in STD_LOGIC;
    rx_cntvalueout_17 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_17 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_17 : in STD_LOGIC;
    rx_en_vtc_17 : in STD_LOGIC;
    rx_inc_17 : in STD_LOGIC;
    rx_load_17 : in STD_LOGIC;
    rx_cntvalueout_ext_17 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_17 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_17 : in STD_LOGIC;
    rx_en_vtc_ext_17 : in STD_LOGIC;
    rx_inc_ext_17 : in STD_LOGIC;
    rx_load_ext_17 : in STD_LOGIC;
    fifo_empty_17 : out STD_LOGIC;
    fifo_rd_clk_17 : in STD_LOGIC;
    fifo_rd_en_17 : in STD_LOGIC;
    bitslip_error_17 : out STD_LOGIC;
    ch2_out5_N : out STD_LOGIC;
    tri_t_18 : in STD_LOGIC;
    tx_cntvaluein_18 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_18 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_18 : in STD_LOGIC;
    tx_en_vtc_18 : in STD_LOGIC;
    tx_inc_18 : in STD_LOGIC;
    tx_load_18 : in STD_LOGIC;
    rx_cntvalueout_18 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_18 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_18 : in STD_LOGIC;
    rx_en_vtc_18 : in STD_LOGIC;
    rx_inc_18 : in STD_LOGIC;
    rx_load_18 : in STD_LOGIC;
    rx_cntvalueout_ext_18 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_18 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_18 : in STD_LOGIC;
    rx_en_vtc_ext_18 : in STD_LOGIC;
    rx_inc_ext_18 : in STD_LOGIC;
    rx_load_ext_18 : in STD_LOGIC;
    fifo_empty_18 : out STD_LOGIC;
    fifo_rd_clk_18 : in STD_LOGIC;
    fifo_rd_en_18 : in STD_LOGIC;
    bitslip_error_18 : out STD_LOGIC;
    ch2_out2_P : out STD_LOGIC;
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_19 : in STD_LOGIC;
    tx_cntvaluein_19 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_19 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_19 : in STD_LOGIC;
    tx_en_vtc_19 : in STD_LOGIC;
    tx_inc_19 : in STD_LOGIC;
    tx_load_19 : in STD_LOGIC;
    rx_cntvalueout_19 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_19 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_19 : in STD_LOGIC;
    rx_en_vtc_19 : in STD_LOGIC;
    rx_inc_19 : in STD_LOGIC;
    rx_load_19 : in STD_LOGIC;
    rx_cntvalueout_ext_19 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_19 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_19 : in STD_LOGIC;
    rx_en_vtc_ext_19 : in STD_LOGIC;
    rx_inc_ext_19 : in STD_LOGIC;
    rx_load_ext_19 : in STD_LOGIC;
    fifo_empty_19 : out STD_LOGIC;
    fifo_rd_clk_19 : in STD_LOGIC;
    fifo_rd_en_19 : in STD_LOGIC;
    bitslip_error_19 : out STD_LOGIC;
    ch2_out2_N : out STD_LOGIC;
    tri_t_20 : in STD_LOGIC;
    tx_cntvaluein_20 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_20 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_20 : in STD_LOGIC;
    tx_en_vtc_20 : in STD_LOGIC;
    tx_inc_20 : in STD_LOGIC;
    tx_load_20 : in STD_LOGIC;
    rx_cntvalueout_20 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_20 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_20 : in STD_LOGIC;
    rx_en_vtc_20 : in STD_LOGIC;
    rx_inc_20 : in STD_LOGIC;
    rx_load_20 : in STD_LOGIC;
    rx_cntvalueout_ext_20 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_20 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_20 : in STD_LOGIC;
    rx_en_vtc_ext_20 : in STD_LOGIC;
    rx_inc_ext_20 : in STD_LOGIC;
    rx_load_ext_20 : in STD_LOGIC;
    fifo_empty_20 : out STD_LOGIC;
    fifo_rd_clk_20 : in STD_LOGIC;
    fifo_rd_en_20 : in STD_LOGIC;
    bitslip_error_20 : out STD_LOGIC;
    ch3_out4_P : out STD_LOGIC;
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_21 : in STD_LOGIC;
    tx_cntvaluein_21 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_21 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_21 : in STD_LOGIC;
    tx_en_vtc_21 : in STD_LOGIC;
    tx_inc_21 : in STD_LOGIC;
    tx_load_21 : in STD_LOGIC;
    rx_cntvalueout_21 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_21 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_21 : in STD_LOGIC;
    rx_en_vtc_21 : in STD_LOGIC;
    rx_inc_21 : in STD_LOGIC;
    rx_load_21 : in STD_LOGIC;
    rx_cntvalueout_ext_21 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_21 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_21 : in STD_LOGIC;
    rx_en_vtc_ext_21 : in STD_LOGIC;
    rx_inc_ext_21 : in STD_LOGIC;
    rx_load_ext_21 : in STD_LOGIC;
    fifo_empty_21 : out STD_LOGIC;
    fifo_rd_clk_21 : in STD_LOGIC;
    fifo_rd_en_21 : in STD_LOGIC;
    bitslip_error_21 : out STD_LOGIC;
    ch3_out4_N : out STD_LOGIC;
    tri_t_22 : in STD_LOGIC;
    tx_cntvaluein_22 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_22 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_22 : in STD_LOGIC;
    tx_en_vtc_22 : in STD_LOGIC;
    tx_inc_22 : in STD_LOGIC;
    tx_load_22 : in STD_LOGIC;
    rx_cntvalueout_22 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_22 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_22 : in STD_LOGIC;
    rx_en_vtc_22 : in STD_LOGIC;
    rx_inc_22 : in STD_LOGIC;
    rx_load_22 : in STD_LOGIC;
    rx_cntvalueout_ext_22 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_22 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_22 : in STD_LOGIC;
    rx_en_vtc_ext_22 : in STD_LOGIC;
    rx_inc_ext_22 : in STD_LOGIC;
    rx_load_ext_22 : in STD_LOGIC;
    fifo_empty_22 : out STD_LOGIC;
    fifo_rd_clk_22 : in STD_LOGIC;
    fifo_rd_en_22 : in STD_LOGIC;
    bitslip_error_22 : out STD_LOGIC;
    ch3_out3_P : out STD_LOGIC;
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_23 : in STD_LOGIC;
    tx_cntvaluein_23 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_23 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_23 : in STD_LOGIC;
    tx_en_vtc_23 : in STD_LOGIC;
    tx_inc_23 : in STD_LOGIC;
    tx_load_23 : in STD_LOGIC;
    rx_cntvalueout_23 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_23 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_23 : in STD_LOGIC;
    rx_en_vtc_23 : in STD_LOGIC;
    rx_inc_23 : in STD_LOGIC;
    rx_load_23 : in STD_LOGIC;
    rx_cntvalueout_ext_23 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_23 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_23 : in STD_LOGIC;
    rx_en_vtc_ext_23 : in STD_LOGIC;
    rx_inc_ext_23 : in STD_LOGIC;
    rx_load_ext_23 : in STD_LOGIC;
    fifo_empty_23 : out STD_LOGIC;
    fifo_rd_clk_23 : in STD_LOGIC;
    fifo_rd_en_23 : in STD_LOGIC;
    bitslip_error_23 : out STD_LOGIC;
    ch3_out3_N : out STD_LOGIC;
    tri_t_24 : in STD_LOGIC;
    tx_cntvaluein_24 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_24 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_24 : in STD_LOGIC;
    tx_en_vtc_24 : in STD_LOGIC;
    tx_inc_24 : in STD_LOGIC;
    tx_load_24 : in STD_LOGIC;
    rx_cntvalueout_24 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_24 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_24 : in STD_LOGIC;
    rx_en_vtc_24 : in STD_LOGIC;
    rx_inc_24 : in STD_LOGIC;
    rx_load_24 : in STD_LOGIC;
    rx_cntvalueout_ext_24 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_24 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_24 : in STD_LOGIC;
    rx_en_vtc_ext_24 : in STD_LOGIC;
    rx_inc_ext_24 : in STD_LOGIC;
    rx_load_ext_24 : in STD_LOGIC;
    fifo_empty_24 : out STD_LOGIC;
    fifo_rd_clk_24 : in STD_LOGIC;
    fifo_rd_en_24 : in STD_LOGIC;
    bitslip_error_24 : out STD_LOGIC;
    ch3_out5_P : out STD_LOGIC;
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_26 : in STD_LOGIC;
    tx_cntvaluein_26 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_26 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_26 : in STD_LOGIC;
    tx_en_vtc_26 : in STD_LOGIC;
    tx_inc_26 : in STD_LOGIC;
    tx_load_26 : in STD_LOGIC;
    rx_cntvalueout_26 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_26 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_26 : in STD_LOGIC;
    rx_en_vtc_26 : in STD_LOGIC;
    rx_inc_26 : in STD_LOGIC;
    rx_load_26 : in STD_LOGIC;
    rx_cntvalueout_ext_26 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_26 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_26 : in STD_LOGIC;
    rx_en_vtc_ext_26 : in STD_LOGIC;
    rx_inc_ext_26 : in STD_LOGIC;
    rx_load_ext_26 : in STD_LOGIC;
    fifo_empty_26 : out STD_LOGIC;
    fifo_rd_clk_26 : in STD_LOGIC;
    fifo_rd_en_26 : in STD_LOGIC;
    bitslip_error_26 : out STD_LOGIC;
    ch3_out5_N : out STD_LOGIC;
    tri_t_27 : in STD_LOGIC;
    tx_cntvaluein_27 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_27 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_27 : in STD_LOGIC;
    tx_en_vtc_27 : in STD_LOGIC;
    tx_inc_27 : in STD_LOGIC;
    tx_load_27 : in STD_LOGIC;
    rx_cntvalueout_27 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_27 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_27 : in STD_LOGIC;
    rx_en_vtc_27 : in STD_LOGIC;
    rx_inc_27 : in STD_LOGIC;
    rx_load_27 : in STD_LOGIC;
    rx_cntvalueout_ext_27 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_27 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_27 : in STD_LOGIC;
    rx_en_vtc_ext_27 : in STD_LOGIC;
    rx_inc_ext_27 : in STD_LOGIC;
    rx_load_ext_27 : in STD_LOGIC;
    fifo_empty_27 : out STD_LOGIC;
    fifo_rd_clk_27 : in STD_LOGIC;
    fifo_rd_en_27 : in STD_LOGIC;
    bitslip_error_27 : out STD_LOGIC;
    ch3_out7_P : out STD_LOGIC;
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_28 : in STD_LOGIC;
    tx_cntvaluein_28 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_28 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_28 : in STD_LOGIC;
    tx_en_vtc_28 : in STD_LOGIC;
    tx_inc_28 : in STD_LOGIC;
    tx_load_28 : in STD_LOGIC;
    rx_cntvalueout_28 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_28 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_28 : in STD_LOGIC;
    rx_en_vtc_28 : in STD_LOGIC;
    rx_inc_28 : in STD_LOGIC;
    rx_load_28 : in STD_LOGIC;
    rx_cntvalueout_ext_28 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_28 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_28 : in STD_LOGIC;
    rx_en_vtc_ext_28 : in STD_LOGIC;
    rx_inc_ext_28 : in STD_LOGIC;
    rx_load_ext_28 : in STD_LOGIC;
    fifo_empty_28 : out STD_LOGIC;
    fifo_rd_clk_28 : in STD_LOGIC;
    fifo_rd_en_28 : in STD_LOGIC;
    bitslip_error_28 : out STD_LOGIC;
    ch3_out7_N : out STD_LOGIC;
    tri_t_29 : in STD_LOGIC;
    tx_cntvaluein_29 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_29 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_29 : in STD_LOGIC;
    tx_en_vtc_29 : in STD_LOGIC;
    tx_inc_29 : in STD_LOGIC;
    tx_load_29 : in STD_LOGIC;
    rx_cntvalueout_29 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_29 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_29 : in STD_LOGIC;
    rx_en_vtc_29 : in STD_LOGIC;
    rx_inc_29 : in STD_LOGIC;
    rx_load_29 : in STD_LOGIC;
    rx_cntvalueout_ext_29 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_29 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_29 : in STD_LOGIC;
    rx_en_vtc_ext_29 : in STD_LOGIC;
    rx_inc_ext_29 : in STD_LOGIC;
    rx_load_ext_29 : in STD_LOGIC;
    fifo_empty_29 : out STD_LOGIC;
    fifo_rd_clk_29 : in STD_LOGIC;
    fifo_rd_en_29 : in STD_LOGIC;
    bitslip_error_29 : out STD_LOGIC;
    ch3_out6_P : out STD_LOGIC;
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_30 : in STD_LOGIC;
    tx_cntvaluein_30 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_30 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_30 : in STD_LOGIC;
    tx_en_vtc_30 : in STD_LOGIC;
    tx_inc_30 : in STD_LOGIC;
    tx_load_30 : in STD_LOGIC;
    rx_cntvalueout_30 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_30 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_30 : in STD_LOGIC;
    rx_en_vtc_30 : in STD_LOGIC;
    rx_inc_30 : in STD_LOGIC;
    rx_load_30 : in STD_LOGIC;
    rx_cntvalueout_ext_30 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_30 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_30 : in STD_LOGIC;
    rx_en_vtc_ext_30 : in STD_LOGIC;
    rx_inc_ext_30 : in STD_LOGIC;
    rx_load_ext_30 : in STD_LOGIC;
    fifo_empty_30 : out STD_LOGIC;
    fifo_rd_clk_30 : in STD_LOGIC;
    fifo_rd_en_30 : in STD_LOGIC;
    bitslip_error_30 : out STD_LOGIC;
    ch3_out6_N : out STD_LOGIC;
    tri_t_31 : in STD_LOGIC;
    tx_cntvaluein_31 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_31 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_31 : in STD_LOGIC;
    tx_en_vtc_31 : in STD_LOGIC;
    tx_inc_31 : in STD_LOGIC;
    tx_load_31 : in STD_LOGIC;
    rx_cntvalueout_31 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_31 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_31 : in STD_LOGIC;
    rx_en_vtc_31 : in STD_LOGIC;
    rx_inc_31 : in STD_LOGIC;
    rx_load_31 : in STD_LOGIC;
    rx_cntvalueout_ext_31 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_31 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_31 : in STD_LOGIC;
    rx_en_vtc_ext_31 : in STD_LOGIC;
    rx_inc_ext_31 : in STD_LOGIC;
    rx_load_ext_31 : in STD_LOGIC;
    fifo_empty_31 : out STD_LOGIC;
    fifo_rd_clk_31 : in STD_LOGIC;
    fifo_rd_en_31 : in STD_LOGIC;
    bitslip_error_31 : out STD_LOGIC;
    TH_out_P : out STD_LOGIC;
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    tri_t_32 : in STD_LOGIC;
    tx_cntvaluein_32 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_32 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_32 : in STD_LOGIC;
    tx_en_vtc_32 : in STD_LOGIC;
    tx_inc_32 : in STD_LOGIC;
    tx_load_32 : in STD_LOGIC;
    rx_cntvalueout_32 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_32 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_32 : in STD_LOGIC;
    rx_en_vtc_32 : in STD_LOGIC;
    rx_inc_32 : in STD_LOGIC;
    rx_load_32 : in STD_LOGIC;
    rx_cntvalueout_ext_32 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_32 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_32 : in STD_LOGIC;
    rx_en_vtc_ext_32 : in STD_LOGIC;
    rx_inc_ext_32 : in STD_LOGIC;
    rx_load_ext_32 : in STD_LOGIC;
    fifo_empty_32 : out STD_LOGIC;
    fifo_rd_clk_32 : in STD_LOGIC;
    fifo_rd_en_32 : in STD_LOGIC;
    bitslip_error_32 : out STD_LOGIC;
    TH_out_N : out STD_LOGIC;
    tri_t_33 : in STD_LOGIC;
    tx_cntvaluein_33 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_33 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_33 : in STD_LOGIC;
    tx_en_vtc_33 : in STD_LOGIC;
    tx_inc_33 : in STD_LOGIC;
    tx_load_33 : in STD_LOGIC;
    rx_cntvalueout_33 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_33 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_33 : in STD_LOGIC;
    rx_en_vtc_33 : in STD_LOGIC;
    rx_inc_33 : in STD_LOGIC;
    rx_load_33 : in STD_LOGIC;
    rx_cntvalueout_ext_33 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_33 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_33 : in STD_LOGIC;
    rx_en_vtc_ext_33 : in STD_LOGIC;
    rx_inc_ext_33 : in STD_LOGIC;
    rx_load_ext_33 : in STD_LOGIC;
    fifo_empty_33 : out STD_LOGIC;
    fifo_rd_clk_33 : in STD_LOGIC;
    fifo_rd_en_33 : in STD_LOGIC;
    bitslip_error_33 : out STD_LOGIC;
    tri_t_12 : in STD_LOGIC;
    tx_cntvaluein_12 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_12 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_12 : in STD_LOGIC;
    tx_en_vtc_12 : in STD_LOGIC;
    tx_inc_12 : in STD_LOGIC;
    tx_load_12 : in STD_LOGIC;
    rx_cntvalueout_12 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_12 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_12 : in STD_LOGIC;
    rx_en_vtc_12 : in STD_LOGIC;
    rx_inc_12 : in STD_LOGIC;
    rx_load_12 : in STD_LOGIC;
    rx_cntvalueout_ext_12 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_12 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_12 : in STD_LOGIC;
    rx_en_vtc_ext_12 : in STD_LOGIC;
    rx_inc_ext_12 : in STD_LOGIC;
    rx_load_ext_12 : in STD_LOGIC;
    fifo_empty_12 : out STD_LOGIC;
    fifo_rd_clk_12 : in STD_LOGIC;
    fifo_rd_en_12 : in STD_LOGIC;
    bitslip_error_12 : out STD_LOGIC;
    tri_t_25 : in STD_LOGIC;
    tx_cntvaluein_25 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_25 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_25 : in STD_LOGIC;
    tx_en_vtc_25 : in STD_LOGIC;
    tx_inc_25 : in STD_LOGIC;
    tx_load_25 : in STD_LOGIC;
    rx_cntvalueout_25 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_25 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_25 : in STD_LOGIC;
    rx_en_vtc_25 : in STD_LOGIC;
    rx_inc_25 : in STD_LOGIC;
    rx_load_25 : in STD_LOGIC;
    rx_cntvalueout_ext_25 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_25 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_25 : in STD_LOGIC;
    rx_en_vtc_ext_25 : in STD_LOGIC;
    rx_inc_ext_25 : in STD_LOGIC;
    rx_load_ext_25 : in STD_LOGIC;
    fifo_empty_25 : out STD_LOGIC;
    fifo_rd_clk_25 : in STD_LOGIC;
    fifo_rd_en_25 : in STD_LOGIC;
    bitslip_error_25 : out STD_LOGIC;
    tri_t_34 : in STD_LOGIC;
    tx_cntvaluein_34 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_34 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_34 : in STD_LOGIC;
    tx_en_vtc_34 : in STD_LOGIC;
    tx_inc_34 : in STD_LOGIC;
    tx_load_34 : in STD_LOGIC;
    rx_cntvalueout_34 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_34 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_34 : in STD_LOGIC;
    rx_en_vtc_34 : in STD_LOGIC;
    rx_inc_34 : in STD_LOGIC;
    rx_load_34 : in STD_LOGIC;
    rx_cntvalueout_ext_34 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_34 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_34 : in STD_LOGIC;
    rx_en_vtc_ext_34 : in STD_LOGIC;
    rx_inc_ext_34 : in STD_LOGIC;
    rx_load_ext_34 : in STD_LOGIC;
    fifo_empty_34 : out STD_LOGIC;
    fifo_rd_clk_34 : in STD_LOGIC;
    fifo_rd_en_34 : in STD_LOGIC;
    bitslip_error_34 : out STD_LOGIC;
    tri_t_35 : in STD_LOGIC;
    tx_cntvaluein_35 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_35 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_35 : in STD_LOGIC;
    tx_en_vtc_35 : in STD_LOGIC;
    tx_inc_35 : in STD_LOGIC;
    tx_load_35 : in STD_LOGIC;
    rx_cntvalueout_35 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_35 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_35 : in STD_LOGIC;
    rx_en_vtc_35 : in STD_LOGIC;
    rx_inc_35 : in STD_LOGIC;
    rx_load_35 : in STD_LOGIC;
    rx_cntvalueout_ext_35 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_35 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_35 : in STD_LOGIC;
    rx_en_vtc_ext_35 : in STD_LOGIC;
    rx_inc_ext_35 : in STD_LOGIC;
    rx_load_ext_35 : in STD_LOGIC;
    fifo_empty_35 : out STD_LOGIC;
    fifo_rd_clk_35 : in STD_LOGIC;
    fifo_rd_en_35 : in STD_LOGIC;
    bitslip_error_35 : out STD_LOGIC;
    tri_t_36 : in STD_LOGIC;
    tx_cntvaluein_36 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_36 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_36 : in STD_LOGIC;
    tx_en_vtc_36 : in STD_LOGIC;
    tx_inc_36 : in STD_LOGIC;
    tx_load_36 : in STD_LOGIC;
    rx_cntvalueout_36 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_36 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_36 : in STD_LOGIC;
    rx_en_vtc_36 : in STD_LOGIC;
    rx_inc_36 : in STD_LOGIC;
    rx_load_36 : in STD_LOGIC;
    rx_cntvalueout_ext_36 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_36 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_36 : in STD_LOGIC;
    rx_en_vtc_ext_36 : in STD_LOGIC;
    rx_inc_ext_36 : in STD_LOGIC;
    rx_load_ext_36 : in STD_LOGIC;
    fifo_empty_36 : out STD_LOGIC;
    fifo_rd_clk_36 : in STD_LOGIC;
    fifo_rd_en_36 : in STD_LOGIC;
    bitslip_error_36 : out STD_LOGIC;
    tri_t_37 : in STD_LOGIC;
    tx_cntvaluein_37 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_37 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_37 : in STD_LOGIC;
    tx_en_vtc_37 : in STD_LOGIC;
    tx_inc_37 : in STD_LOGIC;
    tx_load_37 : in STD_LOGIC;
    rx_cntvalueout_37 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_37 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_37 : in STD_LOGIC;
    rx_en_vtc_37 : in STD_LOGIC;
    rx_inc_37 : in STD_LOGIC;
    rx_load_37 : in STD_LOGIC;
    rx_cntvalueout_ext_37 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_37 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_37 : in STD_LOGIC;
    rx_en_vtc_ext_37 : in STD_LOGIC;
    rx_inc_ext_37 : in STD_LOGIC;
    rx_load_ext_37 : in STD_LOGIC;
    fifo_empty_37 : out STD_LOGIC;
    fifo_rd_clk_37 : in STD_LOGIC;
    fifo_rd_en_37 : in STD_LOGIC;
    bitslip_error_37 : out STD_LOGIC;
    tri_t_38 : in STD_LOGIC;
    tx_cntvaluein_38 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_38 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_38 : in STD_LOGIC;
    tx_en_vtc_38 : in STD_LOGIC;
    tx_inc_38 : in STD_LOGIC;
    tx_load_38 : in STD_LOGIC;
    rx_cntvalueout_38 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_38 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_38 : in STD_LOGIC;
    rx_en_vtc_38 : in STD_LOGIC;
    rx_inc_38 : in STD_LOGIC;
    rx_load_38 : in STD_LOGIC;
    rx_cntvalueout_ext_38 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_38 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_38 : in STD_LOGIC;
    rx_en_vtc_ext_38 : in STD_LOGIC;
    rx_inc_ext_38 : in STD_LOGIC;
    rx_load_ext_38 : in STD_LOGIC;
    fifo_empty_38 : out STD_LOGIC;
    fifo_rd_clk_38 : in STD_LOGIC;
    fifo_rd_en_38 : in STD_LOGIC;
    bitslip_error_38 : out STD_LOGIC;
    tri_t_39 : in STD_LOGIC;
    tx_cntvaluein_39 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_39 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_39 : in STD_LOGIC;
    tx_en_vtc_39 : in STD_LOGIC;
    tx_inc_39 : in STD_LOGIC;
    tx_load_39 : in STD_LOGIC;
    rx_cntvalueout_39 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_39 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_39 : in STD_LOGIC;
    rx_en_vtc_39 : in STD_LOGIC;
    rx_inc_39 : in STD_LOGIC;
    rx_load_39 : in STD_LOGIC;
    rx_cntvalueout_ext_39 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_39 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_39 : in STD_LOGIC;
    rx_en_vtc_ext_39 : in STD_LOGIC;
    rx_inc_ext_39 : in STD_LOGIC;
    rx_load_ext_39 : in STD_LOGIC;
    fifo_empty_39 : out STD_LOGIC;
    fifo_rd_clk_39 : in STD_LOGIC;
    fifo_rd_en_39 : in STD_LOGIC;
    bitslip_error_39 : out STD_LOGIC;
    tri_t_40 : in STD_LOGIC;
    tx_cntvaluein_40 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_40 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_40 : in STD_LOGIC;
    tx_en_vtc_40 : in STD_LOGIC;
    tx_inc_40 : in STD_LOGIC;
    tx_load_40 : in STD_LOGIC;
    rx_cntvalueout_40 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_40 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_40 : in STD_LOGIC;
    rx_en_vtc_40 : in STD_LOGIC;
    rx_inc_40 : in STD_LOGIC;
    rx_load_40 : in STD_LOGIC;
    rx_cntvalueout_ext_40 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_40 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_40 : in STD_LOGIC;
    rx_en_vtc_ext_40 : in STD_LOGIC;
    rx_inc_ext_40 : in STD_LOGIC;
    rx_load_ext_40 : in STD_LOGIC;
    fifo_empty_40 : out STD_LOGIC;
    fifo_rd_clk_40 : in STD_LOGIC;
    fifo_rd_en_40 : in STD_LOGIC;
    bitslip_error_40 : out STD_LOGIC;
    tri_t_41 : in STD_LOGIC;
    tx_cntvaluein_41 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_41 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_41 : in STD_LOGIC;
    tx_en_vtc_41 : in STD_LOGIC;
    tx_inc_41 : in STD_LOGIC;
    tx_load_41 : in STD_LOGIC;
    rx_cntvalueout_41 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_41 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_41 : in STD_LOGIC;
    rx_en_vtc_41 : in STD_LOGIC;
    rx_inc_41 : in STD_LOGIC;
    rx_load_41 : in STD_LOGIC;
    rx_cntvalueout_ext_41 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_41 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_41 : in STD_LOGIC;
    rx_en_vtc_ext_41 : in STD_LOGIC;
    rx_inc_ext_41 : in STD_LOGIC;
    rx_load_ext_41 : in STD_LOGIC;
    fifo_empty_41 : out STD_LOGIC;
    fifo_rd_clk_41 : in STD_LOGIC;
    fifo_rd_en_41 : in STD_LOGIC;
    bitslip_error_41 : out STD_LOGIC;
    tri_t_42 : in STD_LOGIC;
    tx_cntvaluein_42 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_42 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_42 : in STD_LOGIC;
    tx_en_vtc_42 : in STD_LOGIC;
    tx_inc_42 : in STD_LOGIC;
    tx_load_42 : in STD_LOGIC;
    rx_cntvalueout_42 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_42 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_42 : in STD_LOGIC;
    rx_en_vtc_42 : in STD_LOGIC;
    rx_inc_42 : in STD_LOGIC;
    rx_load_42 : in STD_LOGIC;
    rx_cntvalueout_ext_42 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_42 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_42 : in STD_LOGIC;
    rx_en_vtc_ext_42 : in STD_LOGIC;
    rx_inc_ext_42 : in STD_LOGIC;
    rx_load_ext_42 : in STD_LOGIC;
    fifo_empty_42 : out STD_LOGIC;
    fifo_rd_clk_42 : in STD_LOGIC;
    fifo_rd_en_42 : in STD_LOGIC;
    bitslip_error_42 : out STD_LOGIC;
    tri_t_43 : in STD_LOGIC;
    tx_cntvaluein_43 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_43 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_43 : in STD_LOGIC;
    tx_en_vtc_43 : in STD_LOGIC;
    tx_inc_43 : in STD_LOGIC;
    tx_load_43 : in STD_LOGIC;
    rx_cntvalueout_43 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_43 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_43 : in STD_LOGIC;
    rx_en_vtc_43 : in STD_LOGIC;
    rx_inc_43 : in STD_LOGIC;
    rx_load_43 : in STD_LOGIC;
    rx_cntvalueout_ext_43 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_43 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_43 : in STD_LOGIC;
    rx_en_vtc_ext_43 : in STD_LOGIC;
    rx_inc_ext_43 : in STD_LOGIC;
    rx_load_ext_43 : in STD_LOGIC;
    fifo_empty_43 : out STD_LOGIC;
    fifo_rd_clk_43 : in STD_LOGIC;
    fifo_rd_en_43 : in STD_LOGIC;
    bitslip_error_43 : out STD_LOGIC;
    tri_t_44 : in STD_LOGIC;
    tx_cntvaluein_44 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_44 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_44 : in STD_LOGIC;
    tx_en_vtc_44 : in STD_LOGIC;
    tx_inc_44 : in STD_LOGIC;
    tx_load_44 : in STD_LOGIC;
    rx_cntvalueout_44 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_44 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_44 : in STD_LOGIC;
    rx_en_vtc_44 : in STD_LOGIC;
    rx_inc_44 : in STD_LOGIC;
    rx_load_44 : in STD_LOGIC;
    rx_cntvalueout_ext_44 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_44 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_44 : in STD_LOGIC;
    rx_en_vtc_ext_44 : in STD_LOGIC;
    rx_inc_ext_44 : in STD_LOGIC;
    rx_load_ext_44 : in STD_LOGIC;
    fifo_empty_44 : out STD_LOGIC;
    fifo_rd_clk_44 : in STD_LOGIC;
    fifo_rd_en_44 : in STD_LOGIC;
    bitslip_error_44 : out STD_LOGIC;
    tri_t_45 : in STD_LOGIC;
    tx_cntvaluein_45 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_45 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_45 : in STD_LOGIC;
    tx_en_vtc_45 : in STD_LOGIC;
    tx_inc_45 : in STD_LOGIC;
    tx_load_45 : in STD_LOGIC;
    rx_cntvalueout_45 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_45 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_45 : in STD_LOGIC;
    rx_en_vtc_45 : in STD_LOGIC;
    rx_inc_45 : in STD_LOGIC;
    rx_load_45 : in STD_LOGIC;
    rx_cntvalueout_ext_45 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_45 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_45 : in STD_LOGIC;
    rx_en_vtc_ext_45 : in STD_LOGIC;
    rx_inc_ext_45 : in STD_LOGIC;
    rx_load_ext_45 : in STD_LOGIC;
    fifo_empty_45 : out STD_LOGIC;
    fifo_rd_clk_45 : in STD_LOGIC;
    fifo_rd_en_45 : in STD_LOGIC;
    bitslip_error_45 : out STD_LOGIC;
    tri_t_46 : in STD_LOGIC;
    tx_cntvaluein_46 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_46 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_46 : in STD_LOGIC;
    tx_en_vtc_46 : in STD_LOGIC;
    tx_inc_46 : in STD_LOGIC;
    tx_load_46 : in STD_LOGIC;
    rx_cntvalueout_46 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_46 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_46 : in STD_LOGIC;
    rx_en_vtc_46 : in STD_LOGIC;
    rx_inc_46 : in STD_LOGIC;
    rx_load_46 : in STD_LOGIC;
    rx_cntvalueout_ext_46 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_46 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_46 : in STD_LOGIC;
    rx_en_vtc_ext_46 : in STD_LOGIC;
    rx_inc_ext_46 : in STD_LOGIC;
    rx_load_ext_46 : in STD_LOGIC;
    fifo_empty_46 : out STD_LOGIC;
    fifo_rd_clk_46 : in STD_LOGIC;
    fifo_rd_en_46 : in STD_LOGIC;
    bitslip_error_46 : out STD_LOGIC;
    tri_t_47 : in STD_LOGIC;
    tx_cntvaluein_47 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_47 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_47 : in STD_LOGIC;
    tx_en_vtc_47 : in STD_LOGIC;
    tx_inc_47 : in STD_LOGIC;
    tx_load_47 : in STD_LOGIC;
    rx_cntvalueout_47 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_47 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_47 : in STD_LOGIC;
    rx_en_vtc_47 : in STD_LOGIC;
    rx_inc_47 : in STD_LOGIC;
    rx_load_47 : in STD_LOGIC;
    rx_cntvalueout_ext_47 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_47 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_47 : in STD_LOGIC;
    rx_en_vtc_ext_47 : in STD_LOGIC;
    rx_inc_ext_47 : in STD_LOGIC;
    rx_load_ext_47 : in STD_LOGIC;
    fifo_empty_47 : out STD_LOGIC;
    fifo_rd_clk_47 : in STD_LOGIC;
    fifo_rd_en_47 : in STD_LOGIC;
    bitslip_error_47 : out STD_LOGIC;
    tri_t_48 : in STD_LOGIC;
    tx_cntvaluein_48 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_48 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_48 : in STD_LOGIC;
    tx_en_vtc_48 : in STD_LOGIC;
    tx_inc_48 : in STD_LOGIC;
    tx_load_48 : in STD_LOGIC;
    rx_cntvalueout_48 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_48 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_48 : in STD_LOGIC;
    rx_en_vtc_48 : in STD_LOGIC;
    rx_inc_48 : in STD_LOGIC;
    rx_load_48 : in STD_LOGIC;
    rx_cntvalueout_ext_48 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_48 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_48 : in STD_LOGIC;
    rx_en_vtc_ext_48 : in STD_LOGIC;
    rx_inc_ext_48 : in STD_LOGIC;
    rx_load_ext_48 : in STD_LOGIC;
    fifo_empty_48 : out STD_LOGIC;
    fifo_rd_clk_48 : in STD_LOGIC;
    fifo_rd_en_48 : in STD_LOGIC;
    bitslip_error_48 : out STD_LOGIC;
    tri_t_49 : in STD_LOGIC;
    tx_cntvaluein_49 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_49 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_49 : in STD_LOGIC;
    tx_en_vtc_49 : in STD_LOGIC;
    tx_inc_49 : in STD_LOGIC;
    tx_load_49 : in STD_LOGIC;
    rx_cntvalueout_49 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_49 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_49 : in STD_LOGIC;
    rx_en_vtc_49 : in STD_LOGIC;
    rx_inc_49 : in STD_LOGIC;
    rx_load_49 : in STD_LOGIC;
    rx_cntvalueout_ext_49 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_49 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_49 : in STD_LOGIC;
    rx_en_vtc_ext_49 : in STD_LOGIC;
    rx_inc_ext_49 : in STD_LOGIC;
    rx_load_ext_49 : in STD_LOGIC;
    fifo_empty_49 : out STD_LOGIC;
    fifo_rd_clk_49 : in STD_LOGIC;
    fifo_rd_en_49 : in STD_LOGIC;
    bitslip_error_49 : out STD_LOGIC;
    tri_t_50 : in STD_LOGIC;
    tx_cntvaluein_50 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_50 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_50 : in STD_LOGIC;
    tx_en_vtc_50 : in STD_LOGIC;
    tx_inc_50 : in STD_LOGIC;
    tx_load_50 : in STD_LOGIC;
    rx_cntvalueout_50 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_50 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_50 : in STD_LOGIC;
    rx_en_vtc_50 : in STD_LOGIC;
    rx_inc_50 : in STD_LOGIC;
    rx_load_50 : in STD_LOGIC;
    rx_cntvalueout_ext_50 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_50 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_50 : in STD_LOGIC;
    rx_en_vtc_ext_50 : in STD_LOGIC;
    rx_inc_ext_50 : in STD_LOGIC;
    rx_load_ext_50 : in STD_LOGIC;
    fifo_empty_50 : out STD_LOGIC;
    fifo_rd_clk_50 : in STD_LOGIC;
    fifo_rd_en_50 : in STD_LOGIC;
    bitslip_error_50 : out STD_LOGIC;
    tri_t_51 : in STD_LOGIC;
    tx_cntvaluein_51 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_cntvalueout_51 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    tx_ce_51 : in STD_LOGIC;
    tx_en_vtc_51 : in STD_LOGIC;
    tx_inc_51 : in STD_LOGIC;
    tx_load_51 : in STD_LOGIC;
    rx_cntvalueout_51 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_51 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_51 : in STD_LOGIC;
    rx_en_vtc_51 : in STD_LOGIC;
    rx_inc_51 : in STD_LOGIC;
    rx_load_51 : in STD_LOGIC;
    rx_cntvalueout_ext_51 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_cntvaluein_ext_51 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rx_ce_ext_51 : in STD_LOGIC;
    rx_en_vtc_ext_51 : in STD_LOGIC;
    rx_inc_ext_51 : in STD_LOGIC;
    rx_load_ext_51 : in STD_LOGIC;
    fifo_empty_51 : out STD_LOGIC;
    fifo_rd_clk_51 : in STD_LOGIC;
    fifo_rd_en_51 : in STD_LOGIC;
    bitslip_error_51 : out STD_LOGIC;
    fifo_wr_clk_0 : out STD_LOGIC;
    fifo_wr_clk_6 : out STD_LOGIC;
    fifo_wr_clk_13 : out STD_LOGIC;
    fifo_wr_clk_19 : out STD_LOGIC;
    fifo_wr_clk_26 : out STD_LOGIC;
    fifo_wr_clk_32 : out STD_LOGIC;
    fifo_wr_clk_39 : out STD_LOGIC;
    fifo_wr_clk_45 : out STD_LOGIC;
    tri_tbyte0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc0 : in STD_LOGIC;
    bidir_tx_bs_tri_ce0 : in STD_LOGIC;
    bidir_tx_bs_tri_inc0 : in STD_LOGIC;
    bidir_tx_bs_tri_load0 : in STD_LOGIC;
    tri_tbyte1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout1 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein1 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc1 : in STD_LOGIC;
    bidir_tx_bs_tri_ce1 : in STD_LOGIC;
    bidir_tx_bs_tri_inc1 : in STD_LOGIC;
    bidir_tx_bs_tri_load1 : in STD_LOGIC;
    tri_tbyte2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout2 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein2 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc2 : in STD_LOGIC;
    bidir_tx_bs_tri_ce2 : in STD_LOGIC;
    bidir_tx_bs_tri_inc2 : in STD_LOGIC;
    bidir_tx_bs_tri_load2 : in STD_LOGIC;
    tri_tbyte3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout3 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein3 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc3 : in STD_LOGIC;
    bidir_tx_bs_tri_ce3 : in STD_LOGIC;
    bidir_tx_bs_tri_inc3 : in STD_LOGIC;
    bidir_tx_bs_tri_load3 : in STD_LOGIC;
    tri_tbyte4 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout4 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein4 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc4 : in STD_LOGIC;
    bidir_tx_bs_tri_ce4 : in STD_LOGIC;
    bidir_tx_bs_tri_inc4 : in STD_LOGIC;
    bidir_tx_bs_tri_load4 : in STD_LOGIC;
    tri_tbyte5 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout5 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein5 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc5 : in STD_LOGIC;
    bidir_tx_bs_tri_ce5 : in STD_LOGIC;
    bidir_tx_bs_tri_inc5 : in STD_LOGIC;
    bidir_tx_bs_tri_load5 : in STD_LOGIC;
    tri_tbyte6 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout6 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein6 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc6 : in STD_LOGIC;
    bidir_tx_bs_tri_ce6 : in STD_LOGIC;
    bidir_tx_bs_tri_inc6 : in STD_LOGIC;
    bidir_tx_bs_tri_load6 : in STD_LOGIC;
    tri_tbyte7 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bidir_tx_bs_tri_cntvalueout7 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_cntvaluein7 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    bidir_tx_bs_tri_en_vtc7 : in STD_LOGIC;
    bidir_tx_bs_tri_ce7 : in STD_LOGIC;
    bidir_tx_bs_tri_inc7 : in STD_LOGIC;
    bidir_tx_bs_tri_load7 : in STD_LOGIC;
    fifo_rd_data_valid : out STD_LOGIC;
    rst : in STD_LOGIC
  );
  attribute C_ALL_EN_PIN_INFO : string;
  attribute C_ALL_EN_PIN_INFO of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_P loc D14} 1 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_N loc C14} 2 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_P loc B15} 3 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_N loc A15} 4 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_P loc D13} 5 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_N loc C13} 6 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_P loc B14} 7 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_N loc A14} 8 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_P loc C12} 9 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_N loc B12} 10 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_P loc A13} 11 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_N loc A12} 13 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_P loc H14} 14 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_N loc G14} 15 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_P loc G15} 16 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_N loc F15} 17 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_P loc J13} 18 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_N loc H13} 19 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_P loc F14} 20 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_N loc F13} 21 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_P loc G12} 22 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_N loc F12} 23 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_P loc E13} 24 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_N loc E12} 26 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_P loc E10} 27 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_N loc D10} 28 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_P loc E11} 29 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_N loc D11} 30 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_P loc C9} 31 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_N loc C8} 32 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_P loc C11} 33 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_N loc B11}";
  attribute C_ALL_RX_EN : string;
  attribute C_ALL_RX_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_BANK : integer;
  attribute C_BANK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 66;
  attribute C_BIDIR_BITSLICE_EN : string;
  attribute C_BIDIR_BITSLICE_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_BIDIR_FIFO_SYNC_MODE : string;
  attribute C_BIDIR_FIFO_SYNC_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_BIDIR_IS_RX_CLK_INVERTED : string;
  attribute C_BIDIR_IS_RX_CLK_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_IS_RX_RST_DLY_INVERTED : string;
  attribute C_BIDIR_IS_RX_RST_DLY_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_IS_RX_RST_INVERTED : string;
  attribute C_BIDIR_IS_RX_RST_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_IS_TX_CLK_INVERTED : string;
  attribute C_BIDIR_IS_TX_CLK_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_IS_TX_RST_DLY_INVERTED : string;
  attribute C_BIDIR_IS_TX_RST_DLY_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_IS_TX_RST_INVERTED : string;
  attribute C_BIDIR_IS_TX_RST_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_BIDIR_RX_DELAY_FORMAT : string;
  attribute C_BIDIR_RX_DELAY_FORMAT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TIME";
  attribute C_BIDIR_TX_DELAY_FORMAT : string;
  attribute C_BIDIR_TX_DELAY_FORMAT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TIME";
  attribute C_BITSLIP_MODE : string;
  attribute C_BITSLIP_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "SLIP_PER_BIT";
  attribute C_BITSLIP_VAL : string;
  attribute C_BITSLIP_VAL of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "8'b00101100";
  attribute C_BS0_INFO : string;
  attribute C_BS0_INFO of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0 {name bg0_pin0_nc loc D14} 1 {name bg0_pin6_nc loc B14} 2 {name bg1_pin0_nc loc H14} 3 {name bg1_pin6_nc loc F14} 4 {name bg2_pin0_nc loc E10} 5 {name bg2_pin6_nc loc C11} 6 {name bg3_pin0_nc loc H9} 7 {name bg3_pin6_nc loc G11}";
  attribute C_BSC_CTRL_CLK : string;
  attribute C_BSC_CTRL_CLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "EXTERNAL";
  attribute C_BSC_EN_DYN_ODLY_MODE : string;
  attribute C_BSC_EN_DYN_ODLY_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_BSC_IDLY_VT_TRACK : string;
  attribute C_BSC_IDLY_VT_TRACK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_BSC_ODLY_VT_TRACK : string;
  attribute C_BSC_ODLY_VT_TRACK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_BSC_QDLY_VT_TRACK : string;
  attribute C_BSC_QDLY_VT_TRACK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_BSC_READ_IDLE_COUNT : string;
  attribute C_BSC_READ_IDLE_COUNT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "6'b000000";
  attribute C_BSC_REFCLK_SRC : string;
  attribute C_BSC_REFCLK_SRC of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "PLLCLK";
  attribute C_BSC_ROUNDING_FACTOR : integer;
  attribute C_BSC_ROUNDING_FACTOR of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 16;
  attribute C_BSC_RXGATE_EXTEND : string;
  attribute C_BSC_RXGATE_EXTEND of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_BSC_RX_GATING : string;
  attribute C_BSC_RX_GATING of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "DISABLE";
  attribute C_BSC_SELF_CALIBRATE : string;
  attribute C_BSC_SELF_CALIBRATE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "ENABLE";
  attribute C_BSC_SIM_SPEEDUP : string;
  attribute C_BSC_SIM_SPEEDUP of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FAST";
  attribute C_BS_INIT_VAL : string;
  attribute C_BS_INIT_VAL of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000001010101001010101010100101010101010";
  attribute C_CLKIN_DIFF_EN : integer;
  attribute C_CLKIN_DIFF_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_CLKIN_PERIOD : string;
  attribute C_CLKIN_PERIOD of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12.500000";
  attribute C_CLK_FWD : integer;
  attribute C_CLK_FWD of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_CLK_FWD_BITSLICE_NO : integer;
  attribute C_CLK_FWD_BITSLICE_NO of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_CLK_FWD_ENABLE : string;
  attribute C_CLK_FWD_ENABLE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_CLK_FWD_PHASE : integer;
  attribute C_CLK_FWD_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_CLK_SIG_TYPE : string;
  attribute C_CLK_SIG_TYPE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "SINGLE";
  attribute C_CLOCK_TRI : integer;
  attribute C_CLOCK_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_DATA_PIN_EN : integer;
  attribute C_DATA_PIN_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 16;
  attribute C_DATA_TRI : integer;
  attribute C_DATA_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_DEVICE : string;
  attribute C_DEVICE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "xcku035";
  attribute C_DEVICE_FAMILY : string;
  attribute C_DEVICE_FAMILY of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "kintexu";
  attribute C_DIFFERENTIAL_IO_STD : string;
  attribute C_DIFFERENTIAL_IO_STD of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "DIFF_SSTL12";
  attribute C_DIFFERENTIAL_IO_TERMINATION : string;
  attribute C_DIFFERENTIAL_IO_TERMINATION of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_DIFF_EN : string;
  attribute C_DIFF_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000001111111101111111111110111111111111";
  attribute C_DIV_MODE : string;
  attribute C_DIV_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "DIV4";
  attribute C_ENABLE_BITSLIP : integer;
  attribute C_ENABLE_BITSLIP of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_ENABLE_DATA_BITSLIP : integer;
  attribute C_ENABLE_DATA_BITSLIP of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_ENABLE_N_PINS : integer;
  attribute C_ENABLE_N_PINS of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_ENABLE_PLL0_PLLOUT1 : integer;
  attribute C_ENABLE_PLL0_PLLOUT1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_ENABLE_PLL0_PLLOUTFB : integer;
  attribute C_ENABLE_PLL0_PLLOUTFB of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_ENABLE_RIU_INTERFACE : integer;
  attribute C_ENABLE_RIU_INTERFACE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_ENABLE_RIU_SPLIT : integer;
  attribute C_ENABLE_RIU_SPLIT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_ENABLE_TX_TRI : integer;
  attribute C_ENABLE_TX_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_BIDIR : integer;
  attribute C_EN_BIDIR of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_BSC0 : integer;
  attribute C_EN_BSC0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC1 : integer;
  attribute C_EN_BSC1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC2 : integer;
  attribute C_EN_BSC2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC3 : integer;
  attribute C_EN_BSC3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC4 : integer;
  attribute C_EN_BSC4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC5 : integer;
  attribute C_EN_BSC5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_BSC6 : integer;
  attribute C_EN_BSC6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_BSC7 : integer;
  attribute C_EN_BSC7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_MULTI_INTF_PORTS : integer;
  attribute C_EN_MULTI_INTF_PORTS of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_RIU_OR0 : string;
  attribute C_EN_RIU_OR0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_EN_RIU_OR1 : string;
  attribute C_EN_RIU_OR1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_EN_RIU_OR2 : string;
  attribute C_EN_RIU_OR2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TRUE";
  attribute C_EN_RIU_OR3 : string;
  attribute C_EN_RIU_OR3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_EN_RX : integer;
  attribute C_EN_RX of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EN_TX : integer;
  attribute C_EN_TX of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_EN_VTC : integer;
  attribute C_EN_VTC of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_EXDES_BANK : string;
  attribute C_EXDES_BANK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "44_(HP)";
  attribute C_EX_CLK_FREQ : string;
  attribute C_EX_CLK_FREQ of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "80.000000";
  attribute C_EX_INST_GEN : integer;
  attribute C_EX_INST_GEN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_FIFO_SYNC_MODE : integer;
  attribute C_FIFO_SYNC_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_GC_LOC : string;
  attribute C_GC_LOC of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "21 {name IO_L11P_T1U_N8_GC_66 loc G12} 23 {name IO_L12P_T1U_N10_GC_66 loc E13} 28 {name IO_L14P_T2L_N2_GC_66 loc E11}";
  attribute C_INCLK_LOC : string;
  attribute C_INCLK_LOC of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_INCLK_PIN : integer;
  attribute C_INCLK_PIN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 100;
  attribute C_INV_RX_CLK : string;
  attribute C_INV_RX_CLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "8'b00000000";
  attribute C_NIB0_BS0_EN : integer;
  attribute C_NIB0_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB0_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB0_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB0_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB0_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB0_EN_OTHER_NCLK : string;
  attribute C_NIB0_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB0_EN_OTHER_PCLK : string;
  attribute C_NIB0_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB1_BS0_EN : integer;
  attribute C_NIB1_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB1_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB1_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB1_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB1_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB1_EN_OTHER_NCLK : string;
  attribute C_NIB1_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB1_EN_OTHER_PCLK : string;
  attribute C_NIB1_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB2_BS0_EN : integer;
  attribute C_NIB2_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB2_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB2_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB2_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB2_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB2_EN_OTHER_NCLK : string;
  attribute C_NIB2_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB2_EN_OTHER_PCLK : string;
  attribute C_NIB2_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB3_BS0_EN : integer;
  attribute C_NIB3_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB3_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB3_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB3_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB3_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB3_EN_OTHER_NCLK : string;
  attribute C_NIB3_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB3_EN_OTHER_PCLK : string;
  attribute C_NIB3_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB4_BS0_EN : integer;
  attribute C_NIB4_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB4_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB4_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB4_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB4_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB4_EN_OTHER_NCLK : string;
  attribute C_NIB4_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB4_EN_OTHER_PCLK : string;
  attribute C_NIB4_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB5_BS0_EN : integer;
  attribute C_NIB5_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB5_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB5_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB5_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB5_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB5_EN_OTHER_NCLK : string;
  attribute C_NIB5_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB5_EN_OTHER_PCLK : string;
  attribute C_NIB5_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB6_BS0_EN : integer;
  attribute C_NIB6_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB6_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB6_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB6_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB6_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB6_EN_OTHER_NCLK : string;
  attribute C_NIB6_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB6_EN_OTHER_PCLK : string;
  attribute C_NIB6_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB7_BS0_EN : integer;
  attribute C_NIB7_BS0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIB7_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB7_EN_CLK_TO_EXT_NORTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB7_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB7_EN_CLK_TO_EXT_SOUTH of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB7_EN_OTHER_NCLK : string;
  attribute C_NIB7_EN_OTHER_NCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIB7_EN_OTHER_PCLK : string;
  attribute C_NIB7_EN_OTHER_PCLK of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_NIBBLE0_TRI : integer;
  attribute C_NIBBLE0_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE1_TRI : integer;
  attribute C_NIBBLE1_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE2_TRI : integer;
  attribute C_NIBBLE2_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE3_TRI : integer;
  attribute C_NIBBLE3_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE4_TRI : integer;
  attribute C_NIBBLE4_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE5_TRI : integer;
  attribute C_NIBBLE5_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE6_TRI : integer;
  attribute C_NIBBLE6_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_NIBBLE7_TRI : integer;
  attribute C_NIBBLE7_TRI of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_PIN_INFO : string;
  attribute C_PIN_INFO of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_P loc D14} 1 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_N loc C14} 2 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_P loc B15} 3 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_N loc A15} 4 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_P loc D13} 5 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_N loc C13} 6 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_P loc B14} 7 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_N loc A14} 8 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_P loc C12} 9 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_N loc B12} 10 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_P loc A13} 11 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_N loc A12} 13 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_P loc H14} 14 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_N loc G14} 15 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_P loc G15} 16 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_N loc F15} 17 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_P loc J13} 18 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_N loc H13} 19 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_P loc F14} 20 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_N loc F13} 21 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_P loc G12} 22 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_N loc F12} 23 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_P loc E13} 24 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_N loc E12} 26 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_P loc E10} 27 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_N loc D10} 28 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_P loc E11} 29 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_N loc D11} 30 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_P loc C9} 31 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_N loc C8} 32 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_P loc C11} 33 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_N loc B11}";
  attribute C_PLL0_CLK0_PHASE : string;
  attribute C_PLL0_CLK0_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.000000";
  attribute C_PLL0_CLK1_PHASE : string;
  attribute C_PLL0_CLK1_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.000000";
  attribute C_PLL0_CLKFBOUT_MULT : integer;
  attribute C_PLL0_CLKFBOUT_MULT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 8;
  attribute C_PLL0_CLKOUT1_DIVIDE : integer;
  attribute C_PLL0_CLKOUT1_DIVIDE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 4;
  attribute C_PLL0_CLKOUTPHY_MODE : string;
  attribute C_PLL0_CLKOUTPHY_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "VCO_2X";
  attribute C_PLL0_CLK_SOURCE : string;
  attribute C_PLL0_CLK_SOURCE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "BUFG_TO_PLL";
  attribute C_PLL0_DIVCLK_DIVIDE : integer;
  attribute C_PLL0_DIVCLK_DIVIDE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_PLL0_DIV_FACTOR : string;
  attribute C_PLL0_DIV_FACTOR of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.500000";
  attribute C_PLL0_FIFO_WRITE_CLK_EN : integer;
  attribute C_PLL0_FIFO_WRITE_CLK_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_PLL0_MMCM_CLKFBOUT_MULT_F : string;
  attribute C_PLL0_MMCM_CLKFBOUT_MULT_F of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "14.875000";
  attribute C_PLL0_MMCM_CLKOUT0_DIVIDE_F : string;
  attribute C_PLL0_MMCM_CLKOUT0_DIVIDE_F of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "14.875000";
  attribute C_PLL0_MMCM_DIVCLK_DIVIDE : integer;
  attribute C_PLL0_MMCM_DIVCLK_DIVIDE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_PLL0_RX_EXTERNAL_CLK_TO_DATA : integer;
  attribute C_PLL0_RX_EXTERNAL_CLK_TO_DATA of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 5;
  attribute C_PLL1_CLK0_PHASE : string;
  attribute C_PLL1_CLK0_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.000000";
  attribute C_PLL1_CLK1_PHASE : string;
  attribute C_PLL1_CLK1_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.000000";
  attribute C_PLL1_CLKFBOUT_MULT : integer;
  attribute C_PLL1_CLKFBOUT_MULT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 8;
  attribute C_PLL1_CLKOUTPHY_MODE : string;
  attribute C_PLL1_CLKOUTPHY_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "VCO_2X";
  attribute C_PLL1_DIVCLK_DIVIDE : integer;
  attribute C_PLL1_DIVCLK_DIVIDE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 1;
  attribute C_PLL1_DIV_FACTOR : string;
  attribute C_PLL1_DIV_FACTOR of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "0.500000";
  attribute C_PLL_SHARING : integer;
  attribute C_PLL_SHARING of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_PLL_VCOMIN : string;
  attribute C_PLL_VCOMIN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "600.000000";
  attribute C_REC_IN_FREQ : string;
  attribute C_REC_IN_FREQ of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "80.000";
  attribute C_RX_BITSLICE0_EN : string;
  attribute C_RX_BITSLICE0_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "8'b00000000";
  attribute C_RX_BITSLICE_EN : string;
  attribute C_RX_BITSLICE_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_RX_DELAY_CASCADE : integer;
  attribute C_RX_DELAY_CASCADE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_RX_DELAY_FORMAT : string;
  attribute C_RX_DELAY_FORMAT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TIME";
  attribute C_RX_DELAY_TYPE : string;
  attribute C_RX_DELAY_TYPE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE0 : string;
  attribute C_RX_DELAY_TYPE0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE1 : string;
  attribute C_RX_DELAY_TYPE1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE10 : string;
  attribute C_RX_DELAY_TYPE10 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE11 : string;
  attribute C_RX_DELAY_TYPE11 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE12 : string;
  attribute C_RX_DELAY_TYPE12 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE13 : string;
  attribute C_RX_DELAY_TYPE13 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE14 : string;
  attribute C_RX_DELAY_TYPE14 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE15 : string;
  attribute C_RX_DELAY_TYPE15 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE16 : string;
  attribute C_RX_DELAY_TYPE16 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE17 : string;
  attribute C_RX_DELAY_TYPE17 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE18 : string;
  attribute C_RX_DELAY_TYPE18 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE19 : string;
  attribute C_RX_DELAY_TYPE19 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE2 : string;
  attribute C_RX_DELAY_TYPE2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE20 : string;
  attribute C_RX_DELAY_TYPE20 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE21 : string;
  attribute C_RX_DELAY_TYPE21 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE22 : string;
  attribute C_RX_DELAY_TYPE22 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE23 : string;
  attribute C_RX_DELAY_TYPE23 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE24 : string;
  attribute C_RX_DELAY_TYPE24 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE25 : string;
  attribute C_RX_DELAY_TYPE25 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE26 : string;
  attribute C_RX_DELAY_TYPE26 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE27 : string;
  attribute C_RX_DELAY_TYPE27 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE28 : string;
  attribute C_RX_DELAY_TYPE28 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE29 : string;
  attribute C_RX_DELAY_TYPE29 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE3 : string;
  attribute C_RX_DELAY_TYPE3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE30 : string;
  attribute C_RX_DELAY_TYPE30 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE31 : string;
  attribute C_RX_DELAY_TYPE31 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE32 : string;
  attribute C_RX_DELAY_TYPE32 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE33 : string;
  attribute C_RX_DELAY_TYPE33 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE34 : string;
  attribute C_RX_DELAY_TYPE34 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE35 : string;
  attribute C_RX_DELAY_TYPE35 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE36 : string;
  attribute C_RX_DELAY_TYPE36 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE37 : string;
  attribute C_RX_DELAY_TYPE37 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE38 : string;
  attribute C_RX_DELAY_TYPE38 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE39 : string;
  attribute C_RX_DELAY_TYPE39 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE4 : string;
  attribute C_RX_DELAY_TYPE4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE40 : string;
  attribute C_RX_DELAY_TYPE40 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE41 : string;
  attribute C_RX_DELAY_TYPE41 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE42 : string;
  attribute C_RX_DELAY_TYPE42 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE43 : string;
  attribute C_RX_DELAY_TYPE43 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE44 : string;
  attribute C_RX_DELAY_TYPE44 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE45 : string;
  attribute C_RX_DELAY_TYPE45 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE46 : string;
  attribute C_RX_DELAY_TYPE46 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE47 : string;
  attribute C_RX_DELAY_TYPE47 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE48 : string;
  attribute C_RX_DELAY_TYPE48 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE49 : string;
  attribute C_RX_DELAY_TYPE49 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE5 : string;
  attribute C_RX_DELAY_TYPE5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE50 : string;
  attribute C_RX_DELAY_TYPE50 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE51 : string;
  attribute C_RX_DELAY_TYPE51 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE6 : string;
  attribute C_RX_DELAY_TYPE6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE7 : string;
  attribute C_RX_DELAY_TYPE7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE8 : string;
  attribute C_RX_DELAY_TYPE8 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_TYPE9 : string;
  attribute C_RX_DELAY_TYPE9 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_RX_DELAY_VALUE : string;
  attribute C_RX_DELAY_VALUE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE0 : string;
  attribute C_RX_DELAY_VALUE0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE1 : string;
  attribute C_RX_DELAY_VALUE1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE10 : string;
  attribute C_RX_DELAY_VALUE10 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE11 : string;
  attribute C_RX_DELAY_VALUE11 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE12 : string;
  attribute C_RX_DELAY_VALUE12 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE13 : string;
  attribute C_RX_DELAY_VALUE13 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE14 : string;
  attribute C_RX_DELAY_VALUE14 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE15 : string;
  attribute C_RX_DELAY_VALUE15 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE16 : string;
  attribute C_RX_DELAY_VALUE16 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE17 : string;
  attribute C_RX_DELAY_VALUE17 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE18 : string;
  attribute C_RX_DELAY_VALUE18 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE19 : string;
  attribute C_RX_DELAY_VALUE19 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE2 : string;
  attribute C_RX_DELAY_VALUE2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE20 : string;
  attribute C_RX_DELAY_VALUE20 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE21 : string;
  attribute C_RX_DELAY_VALUE21 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE22 : string;
  attribute C_RX_DELAY_VALUE22 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE23 : string;
  attribute C_RX_DELAY_VALUE23 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE24 : string;
  attribute C_RX_DELAY_VALUE24 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE25 : string;
  attribute C_RX_DELAY_VALUE25 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE26 : string;
  attribute C_RX_DELAY_VALUE26 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE27 : string;
  attribute C_RX_DELAY_VALUE27 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE28 : string;
  attribute C_RX_DELAY_VALUE28 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE29 : string;
  attribute C_RX_DELAY_VALUE29 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE3 : string;
  attribute C_RX_DELAY_VALUE3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE30 : string;
  attribute C_RX_DELAY_VALUE30 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE31 : string;
  attribute C_RX_DELAY_VALUE31 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE32 : string;
  attribute C_RX_DELAY_VALUE32 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE33 : string;
  attribute C_RX_DELAY_VALUE33 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE34 : string;
  attribute C_RX_DELAY_VALUE34 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE35 : string;
  attribute C_RX_DELAY_VALUE35 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE36 : string;
  attribute C_RX_DELAY_VALUE36 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE37 : string;
  attribute C_RX_DELAY_VALUE37 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE38 : string;
  attribute C_RX_DELAY_VALUE38 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE39 : string;
  attribute C_RX_DELAY_VALUE39 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE4 : string;
  attribute C_RX_DELAY_VALUE4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE40 : string;
  attribute C_RX_DELAY_VALUE40 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE41 : string;
  attribute C_RX_DELAY_VALUE41 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE42 : string;
  attribute C_RX_DELAY_VALUE42 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE43 : string;
  attribute C_RX_DELAY_VALUE43 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE44 : string;
  attribute C_RX_DELAY_VALUE44 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE45 : string;
  attribute C_RX_DELAY_VALUE45 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE46 : string;
  attribute C_RX_DELAY_VALUE46 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE47 : string;
  attribute C_RX_DELAY_VALUE47 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE48 : string;
  attribute C_RX_DELAY_VALUE48 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE49 : string;
  attribute C_RX_DELAY_VALUE49 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE5 : string;
  attribute C_RX_DELAY_VALUE5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE50 : string;
  attribute C_RX_DELAY_VALUE50 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE51 : string;
  attribute C_RX_DELAY_VALUE51 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE6 : string;
  attribute C_RX_DELAY_VALUE6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE7 : string;
  attribute C_RX_DELAY_VALUE7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE8 : string;
  attribute C_RX_DELAY_VALUE8 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE9 : string;
  attribute C_RX_DELAY_VALUE9 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT0 : string;
  attribute C_RX_DELAY_VALUE_EXT0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT1 : string;
  attribute C_RX_DELAY_VALUE_EXT1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT10 : string;
  attribute C_RX_DELAY_VALUE_EXT10 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT11 : string;
  attribute C_RX_DELAY_VALUE_EXT11 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT12 : string;
  attribute C_RX_DELAY_VALUE_EXT12 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT13 : string;
  attribute C_RX_DELAY_VALUE_EXT13 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT14 : string;
  attribute C_RX_DELAY_VALUE_EXT14 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT15 : string;
  attribute C_RX_DELAY_VALUE_EXT15 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT16 : string;
  attribute C_RX_DELAY_VALUE_EXT16 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT17 : string;
  attribute C_RX_DELAY_VALUE_EXT17 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT18 : string;
  attribute C_RX_DELAY_VALUE_EXT18 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT19 : string;
  attribute C_RX_DELAY_VALUE_EXT19 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT2 : string;
  attribute C_RX_DELAY_VALUE_EXT2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT20 : string;
  attribute C_RX_DELAY_VALUE_EXT20 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT21 : string;
  attribute C_RX_DELAY_VALUE_EXT21 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT22 : string;
  attribute C_RX_DELAY_VALUE_EXT22 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT23 : string;
  attribute C_RX_DELAY_VALUE_EXT23 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT24 : string;
  attribute C_RX_DELAY_VALUE_EXT24 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT25 : string;
  attribute C_RX_DELAY_VALUE_EXT25 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT26 : string;
  attribute C_RX_DELAY_VALUE_EXT26 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT27 : string;
  attribute C_RX_DELAY_VALUE_EXT27 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT28 : string;
  attribute C_RX_DELAY_VALUE_EXT28 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT29 : string;
  attribute C_RX_DELAY_VALUE_EXT29 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT3 : string;
  attribute C_RX_DELAY_VALUE_EXT3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT30 : string;
  attribute C_RX_DELAY_VALUE_EXT30 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT31 : string;
  attribute C_RX_DELAY_VALUE_EXT31 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT32 : string;
  attribute C_RX_DELAY_VALUE_EXT32 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT33 : string;
  attribute C_RX_DELAY_VALUE_EXT33 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT34 : string;
  attribute C_RX_DELAY_VALUE_EXT34 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT35 : string;
  attribute C_RX_DELAY_VALUE_EXT35 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT36 : string;
  attribute C_RX_DELAY_VALUE_EXT36 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT37 : string;
  attribute C_RX_DELAY_VALUE_EXT37 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT38 : string;
  attribute C_RX_DELAY_VALUE_EXT38 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT39 : string;
  attribute C_RX_DELAY_VALUE_EXT39 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT4 : string;
  attribute C_RX_DELAY_VALUE_EXT4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT40 : string;
  attribute C_RX_DELAY_VALUE_EXT40 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT41 : string;
  attribute C_RX_DELAY_VALUE_EXT41 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT42 : string;
  attribute C_RX_DELAY_VALUE_EXT42 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT43 : string;
  attribute C_RX_DELAY_VALUE_EXT43 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT44 : string;
  attribute C_RX_DELAY_VALUE_EXT44 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT45 : string;
  attribute C_RX_DELAY_VALUE_EXT45 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT46 : string;
  attribute C_RX_DELAY_VALUE_EXT46 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT47 : string;
  attribute C_RX_DELAY_VALUE_EXT47 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT48 : string;
  attribute C_RX_DELAY_VALUE_EXT48 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT49 : string;
  attribute C_RX_DELAY_VALUE_EXT49 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT5 : string;
  attribute C_RX_DELAY_VALUE_EXT5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT50 : string;
  attribute C_RX_DELAY_VALUE_EXT50 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT51 : string;
  attribute C_RX_DELAY_VALUE_EXT51 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT6 : string;
  attribute C_RX_DELAY_VALUE_EXT6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT7 : string;
  attribute C_RX_DELAY_VALUE_EXT7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT8 : string;
  attribute C_RX_DELAY_VALUE_EXT8 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT9 : string;
  attribute C_RX_DELAY_VALUE_EXT9 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_RX_EQUALIZATION_D : string;
  attribute C_RX_EQUALIZATION_D of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_RX_EQUALIZATION_S : string;
  attribute C_RX_EQUALIZATION_S of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_RX_FIFO_SYNC_MODE : string;
  attribute C_RX_FIFO_SYNC_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_RX_IS_CLK_EXT_INVERTED : string;
  attribute C_RX_IS_CLK_EXT_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_RX_IS_CLK_INVERTED : string;
  attribute C_RX_IS_CLK_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_RX_IS_RST_DLY_EXT_INVERTED : string;
  attribute C_RX_IS_RST_DLY_EXT_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_RX_IS_RST_DLY_INVERTED : string;
  attribute C_RX_IS_RST_DLY_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_RX_IS_RST_INVERTED : string;
  attribute C_RX_IS_RST_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_RX_PIN_EN : string;
  attribute C_RX_PIN_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_RX_REFCLK_FREQ : string;
  attribute C_RX_REFCLK_FREQ of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1280.000000";
  attribute C_RX_STROBE_EN : string;
  attribute C_RX_STROBE_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "16'b0000000000000000";
  attribute C_SERIALIZATION_FACTOR : integer;
  attribute C_SERIALIZATION_FACTOR of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 8;
  attribute C_SERIAL_MODE : string;
  attribute C_SERIAL_MODE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_SIM_DEVICE : string;
  attribute C_SIM_DEVICE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "ULTRASCALE";
  attribute C_SIM_VERSION : string;
  attribute C_SIM_VERSION of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1.000000";
  attribute C_SINGLE_ENDED_IO_STD : string;
  attribute C_SINGLE_ENDED_IO_STD of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_SINGLE_ENDED_IO_TERMINATION : string;
  attribute C_SINGLE_ENDED_IO_TERMINATION of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_STRB_INFO : string;
  attribute C_STRB_INFO of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99";
  attribute C_TEMPLATE : integer;
  attribute C_TEMPLATE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_TX_BITSLICE_EN : string;
  attribute C_TX_BITSLICE_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "52'b0000000000000000000101010100101010101010010101010101";
  attribute C_TX_DATA_PHASE : integer;
  attribute C_TX_DATA_PHASE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_TX_DELAY_FORMAT : string;
  attribute C_TX_DELAY_FORMAT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TIME";
  attribute C_TX_DELAY_TYPE : integer;
  attribute C_TX_DELAY_TYPE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is 0;
  attribute C_TX_DELAY_TYPE0 : string;
  attribute C_TX_DELAY_TYPE0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE1 : string;
  attribute C_TX_DELAY_TYPE1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE10 : string;
  attribute C_TX_DELAY_TYPE10 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE11 : string;
  attribute C_TX_DELAY_TYPE11 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE12 : string;
  attribute C_TX_DELAY_TYPE12 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE13 : string;
  attribute C_TX_DELAY_TYPE13 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE14 : string;
  attribute C_TX_DELAY_TYPE14 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE15 : string;
  attribute C_TX_DELAY_TYPE15 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE16 : string;
  attribute C_TX_DELAY_TYPE16 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE17 : string;
  attribute C_TX_DELAY_TYPE17 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE18 : string;
  attribute C_TX_DELAY_TYPE18 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE19 : string;
  attribute C_TX_DELAY_TYPE19 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE2 : string;
  attribute C_TX_DELAY_TYPE2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE20 : string;
  attribute C_TX_DELAY_TYPE20 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE21 : string;
  attribute C_TX_DELAY_TYPE21 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE22 : string;
  attribute C_TX_DELAY_TYPE22 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE23 : string;
  attribute C_TX_DELAY_TYPE23 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE24 : string;
  attribute C_TX_DELAY_TYPE24 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE25 : string;
  attribute C_TX_DELAY_TYPE25 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE26 : string;
  attribute C_TX_DELAY_TYPE26 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE27 : string;
  attribute C_TX_DELAY_TYPE27 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE28 : string;
  attribute C_TX_DELAY_TYPE28 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE29 : string;
  attribute C_TX_DELAY_TYPE29 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE3 : string;
  attribute C_TX_DELAY_TYPE3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE30 : string;
  attribute C_TX_DELAY_TYPE30 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE31 : string;
  attribute C_TX_DELAY_TYPE31 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE32 : string;
  attribute C_TX_DELAY_TYPE32 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE33 : string;
  attribute C_TX_DELAY_TYPE33 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE34 : string;
  attribute C_TX_DELAY_TYPE34 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE35 : string;
  attribute C_TX_DELAY_TYPE35 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE36 : string;
  attribute C_TX_DELAY_TYPE36 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE37 : string;
  attribute C_TX_DELAY_TYPE37 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE38 : string;
  attribute C_TX_DELAY_TYPE38 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE39 : string;
  attribute C_TX_DELAY_TYPE39 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE4 : string;
  attribute C_TX_DELAY_TYPE4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE40 : string;
  attribute C_TX_DELAY_TYPE40 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE41 : string;
  attribute C_TX_DELAY_TYPE41 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE42 : string;
  attribute C_TX_DELAY_TYPE42 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE43 : string;
  attribute C_TX_DELAY_TYPE43 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE44 : string;
  attribute C_TX_DELAY_TYPE44 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE45 : string;
  attribute C_TX_DELAY_TYPE45 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE46 : string;
  attribute C_TX_DELAY_TYPE46 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE47 : string;
  attribute C_TX_DELAY_TYPE47 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE48 : string;
  attribute C_TX_DELAY_TYPE48 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE49 : string;
  attribute C_TX_DELAY_TYPE49 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE5 : string;
  attribute C_TX_DELAY_TYPE5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE50 : string;
  attribute C_TX_DELAY_TYPE50 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE51 : string;
  attribute C_TX_DELAY_TYPE51 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE6 : string;
  attribute C_TX_DELAY_TYPE6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE7 : string;
  attribute C_TX_DELAY_TYPE7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE8 : string;
  attribute C_TX_DELAY_TYPE8 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_TYPE9 : string;
  attribute C_TX_DELAY_TYPE9 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "2'b00";
  attribute C_TX_DELAY_VALUE : string;
  attribute C_TX_DELAY_VALUE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE0 : string;
  attribute C_TX_DELAY_VALUE0 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE1 : string;
  attribute C_TX_DELAY_VALUE1 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE10 : string;
  attribute C_TX_DELAY_VALUE10 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE11 : string;
  attribute C_TX_DELAY_VALUE11 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE12 : string;
  attribute C_TX_DELAY_VALUE12 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE13 : string;
  attribute C_TX_DELAY_VALUE13 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE14 : string;
  attribute C_TX_DELAY_VALUE14 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE15 : string;
  attribute C_TX_DELAY_VALUE15 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE16 : string;
  attribute C_TX_DELAY_VALUE16 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE17 : string;
  attribute C_TX_DELAY_VALUE17 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE18 : string;
  attribute C_TX_DELAY_VALUE18 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE19 : string;
  attribute C_TX_DELAY_VALUE19 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE2 : string;
  attribute C_TX_DELAY_VALUE2 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE20 : string;
  attribute C_TX_DELAY_VALUE20 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE21 : string;
  attribute C_TX_DELAY_VALUE21 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE22 : string;
  attribute C_TX_DELAY_VALUE22 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE23 : string;
  attribute C_TX_DELAY_VALUE23 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE24 : string;
  attribute C_TX_DELAY_VALUE24 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE25 : string;
  attribute C_TX_DELAY_VALUE25 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE26 : string;
  attribute C_TX_DELAY_VALUE26 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE27 : string;
  attribute C_TX_DELAY_VALUE27 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE28 : string;
  attribute C_TX_DELAY_VALUE28 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE29 : string;
  attribute C_TX_DELAY_VALUE29 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE3 : string;
  attribute C_TX_DELAY_VALUE3 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE30 : string;
  attribute C_TX_DELAY_VALUE30 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE31 : string;
  attribute C_TX_DELAY_VALUE31 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE32 : string;
  attribute C_TX_DELAY_VALUE32 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE33 : string;
  attribute C_TX_DELAY_VALUE33 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE34 : string;
  attribute C_TX_DELAY_VALUE34 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE35 : string;
  attribute C_TX_DELAY_VALUE35 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE36 : string;
  attribute C_TX_DELAY_VALUE36 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE37 : string;
  attribute C_TX_DELAY_VALUE37 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE38 : string;
  attribute C_TX_DELAY_VALUE38 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE39 : string;
  attribute C_TX_DELAY_VALUE39 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE4 : string;
  attribute C_TX_DELAY_VALUE4 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE40 : string;
  attribute C_TX_DELAY_VALUE40 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE41 : string;
  attribute C_TX_DELAY_VALUE41 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE42 : string;
  attribute C_TX_DELAY_VALUE42 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE43 : string;
  attribute C_TX_DELAY_VALUE43 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE44 : string;
  attribute C_TX_DELAY_VALUE44 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE45 : string;
  attribute C_TX_DELAY_VALUE45 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE46 : string;
  attribute C_TX_DELAY_VALUE46 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE47 : string;
  attribute C_TX_DELAY_VALUE47 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE48 : string;
  attribute C_TX_DELAY_VALUE48 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE49 : string;
  attribute C_TX_DELAY_VALUE49 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE5 : string;
  attribute C_TX_DELAY_VALUE5 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE50 : string;
  attribute C_TX_DELAY_VALUE50 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE51 : string;
  attribute C_TX_DELAY_VALUE51 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE6 : string;
  attribute C_TX_DELAY_VALUE6 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE7 : string;
  attribute C_TX_DELAY_VALUE7 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE8 : string;
  attribute C_TX_DELAY_VALUE8 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DELAY_VALUE9 : string;
  attribute C_TX_DELAY_VALUE9 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "12'b000000000000";
  attribute C_TX_DRIVE_D : string;
  attribute C_TX_DRIVE_D of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "";
  attribute C_TX_DRIVE_S : string;
  attribute C_TX_DRIVE_S of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_TX_IS_CLK_INVERTED : string;
  attribute C_TX_IS_CLK_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_IS_RST_DLY_INVERTED : string;
  attribute C_TX_IS_RST_DLY_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_IS_RST_INVERTED : string;
  attribute C_TX_IS_RST_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_NATIVE_ODELAY_BYPASS : string;
  attribute C_TX_NATIVE_ODELAY_BYPASS of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_TX_PRE_EMPHASIS_D : string;
  attribute C_TX_PRE_EMPHASIS_D of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_TX_PRE_EMPHASIS_S : string;
  attribute C_TX_PRE_EMPHASIS_S of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_TX_REFCLK_FREQ : string;
  attribute C_TX_REFCLK_FREQ of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1280.000000";
  attribute C_TX_SLEW_D : string;
  attribute C_TX_SLEW_D of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "SLOW MEDIUM FAST";
  attribute C_TX_SLEW_S : string;
  attribute C_TX_SLEW_S of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "NONE";
  attribute C_TX_TRI_DELAY_FORMAT : string;
  attribute C_TX_TRI_DELAY_FORMAT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "TIME";
  attribute C_TX_TRI_INIT : string;
  attribute C_TX_TRI_INIT of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b1";
  attribute C_TX_TRI_IS_CLK_INVERTED : string;
  attribute C_TX_TRI_IS_CLK_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_TRI_IS_RST_DLY_INVERTED : string;
  attribute C_TX_TRI_IS_RST_DLY_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_TRI_IS_RST_INVERTED : string;
  attribute C_TX_TRI_IS_RST_INVERTED of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "1'b0";
  attribute C_TX_TRI_NATIVE_ODELAY_BYPASS : string;
  attribute C_TX_TRI_NATIVE_ODELAY_BYPASS of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute C_TX_TRI_OUTPUT_PHASE_90 : string;
  attribute C_TX_TRI_OUTPUT_PHASE_90 of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "FALSE";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "yes";
  attribute TX_BITSLICE_TRI_EN : string;
  attribute TX_BITSLICE_TRI_EN of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 : entity is "8'b00000000";
end high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9 is
  signal \<const0>\ : STD_LOGIC;
begin
  bidir_tx_bs_tri_cntvalueout0(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout0(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout1(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout2(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout3(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout4(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout5(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout6(0) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(8) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(7) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(6) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(5) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(4) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(3) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(2) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(1) <= \<const0>\;
  bidir_tx_bs_tri_cntvalueout7(0) <= \<const0>\;
  bitslip_error_0 <= \<const0>\;
  bitslip_error_1 <= \<const0>\;
  bitslip_error_10 <= \<const0>\;
  bitslip_error_11 <= \<const0>\;
  bitslip_error_12 <= \<const0>\;
  bitslip_error_13 <= \<const0>\;
  bitslip_error_14 <= \<const0>\;
  bitslip_error_15 <= \<const0>\;
  bitslip_error_16 <= \<const0>\;
  bitslip_error_17 <= \<const0>\;
  bitslip_error_18 <= \<const0>\;
  bitslip_error_19 <= \<const0>\;
  bitslip_error_2 <= \<const0>\;
  bitslip_error_20 <= \<const0>\;
  bitslip_error_21 <= \<const0>\;
  bitslip_error_22 <= \<const0>\;
  bitslip_error_23 <= \<const0>\;
  bitslip_error_24 <= \<const0>\;
  bitslip_error_25 <= \<const0>\;
  bitslip_error_26 <= \<const0>\;
  bitslip_error_27 <= \<const0>\;
  bitslip_error_28 <= \<const0>\;
  bitslip_error_29 <= \<const0>\;
  bitslip_error_3 <= \<const0>\;
  bitslip_error_30 <= \<const0>\;
  bitslip_error_31 <= \<const0>\;
  bitslip_error_32 <= \<const0>\;
  bitslip_error_33 <= \<const0>\;
  bitslip_error_34 <= \<const0>\;
  bitslip_error_35 <= \<const0>\;
  bitslip_error_36 <= \<const0>\;
  bitslip_error_37 <= \<const0>\;
  bitslip_error_38 <= \<const0>\;
  bitslip_error_39 <= \<const0>\;
  bitslip_error_4 <= \<const0>\;
  bitslip_error_40 <= \<const0>\;
  bitslip_error_41 <= \<const0>\;
  bitslip_error_42 <= \<const0>\;
  bitslip_error_43 <= \<const0>\;
  bitslip_error_44 <= \<const0>\;
  bitslip_error_45 <= \<const0>\;
  bitslip_error_46 <= \<const0>\;
  bitslip_error_47 <= \<const0>\;
  bitslip_error_48 <= \<const0>\;
  bitslip_error_49 <= \<const0>\;
  bitslip_error_5 <= \<const0>\;
  bitslip_error_50 <= \<const0>\;
  bitslip_error_51 <= \<const0>\;
  bitslip_error_6 <= \<const0>\;
  bitslip_error_7 <= \<const0>\;
  bitslip_error_8 <= \<const0>\;
  bitslip_error_9 <= \<const0>\;
  clk_from_ibuf <= \<const0>\;
  dly_rdy_bsc6 <= \<const0>\;
  dly_rdy_bsc7 <= \<const0>\;
  do_out(15) <= \<const0>\;
  do_out(14) <= \<const0>\;
  do_out(13) <= \<const0>\;
  do_out(12) <= \<const0>\;
  do_out(11) <= \<const0>\;
  do_out(10) <= \<const0>\;
  do_out(9) <= \<const0>\;
  do_out(8) <= \<const0>\;
  do_out(7) <= \<const0>\;
  do_out(6) <= \<const0>\;
  do_out(5) <= \<const0>\;
  do_out(4) <= \<const0>\;
  do_out(3) <= \<const0>\;
  do_out(2) <= \<const0>\;
  do_out(1) <= \<const0>\;
  do_out(0) <= \<const0>\;
  drdy <= \<const0>\;
  fifo_empty_0 <= \<const0>\;
  fifo_empty_1 <= \<const0>\;
  fifo_empty_10 <= \<const0>\;
  fifo_empty_11 <= \<const0>\;
  fifo_empty_12 <= \<const0>\;
  fifo_empty_13 <= \<const0>\;
  fifo_empty_14 <= \<const0>\;
  fifo_empty_15 <= \<const0>\;
  fifo_empty_16 <= \<const0>\;
  fifo_empty_17 <= \<const0>\;
  fifo_empty_18 <= \<const0>\;
  fifo_empty_19 <= \<const0>\;
  fifo_empty_2 <= \<const0>\;
  fifo_empty_20 <= \<const0>\;
  fifo_empty_21 <= \<const0>\;
  fifo_empty_22 <= \<const0>\;
  fifo_empty_23 <= \<const0>\;
  fifo_empty_24 <= \<const0>\;
  fifo_empty_25 <= \<const0>\;
  fifo_empty_26 <= \<const0>\;
  fifo_empty_27 <= \<const0>\;
  fifo_empty_28 <= \<const0>\;
  fifo_empty_29 <= \<const0>\;
  fifo_empty_3 <= \<const0>\;
  fifo_empty_30 <= \<const0>\;
  fifo_empty_31 <= \<const0>\;
  fifo_empty_32 <= \<const0>\;
  fifo_empty_33 <= \<const0>\;
  fifo_empty_34 <= \<const0>\;
  fifo_empty_35 <= \<const0>\;
  fifo_empty_36 <= \<const0>\;
  fifo_empty_37 <= \<const0>\;
  fifo_empty_38 <= \<const0>\;
  fifo_empty_39 <= \<const0>\;
  fifo_empty_4 <= \<const0>\;
  fifo_empty_40 <= \<const0>\;
  fifo_empty_41 <= \<const0>\;
  fifo_empty_42 <= \<const0>\;
  fifo_empty_43 <= \<const0>\;
  fifo_empty_44 <= \<const0>\;
  fifo_empty_45 <= \<const0>\;
  fifo_empty_46 <= \<const0>\;
  fifo_empty_47 <= \<const0>\;
  fifo_empty_48 <= \<const0>\;
  fifo_empty_49 <= \<const0>\;
  fifo_empty_5 <= \<const0>\;
  fifo_empty_50 <= \<const0>\;
  fifo_empty_51 <= \<const0>\;
  fifo_empty_6 <= \<const0>\;
  fifo_empty_7 <= \<const0>\;
  fifo_empty_8 <= \<const0>\;
  fifo_empty_9 <= \<const0>\;
  fifo_rd_data_valid <= \<const0>\;
  fifo_wr_clk_0 <= \<const0>\;
  fifo_wr_clk_13 <= \<const0>\;
  fifo_wr_clk_19 <= \<const0>\;
  fifo_wr_clk_26 <= \<const0>\;
  fifo_wr_clk_32 <= \<const0>\;
  fifo_wr_clk_39 <= \<const0>\;
  fifo_wr_clk_45 <= \<const0>\;
  fifo_wr_clk_6 <= \<const0>\;
  intf_rdy <= \<const0>\;
  lp_rx_o_n(15) <= \<const0>\;
  lp_rx_o_n(14) <= \<const0>\;
  lp_rx_o_n(13) <= \<const0>\;
  lp_rx_o_n(12) <= \<const0>\;
  lp_rx_o_n(11) <= \<const0>\;
  lp_rx_o_n(10) <= \<const0>\;
  lp_rx_o_n(9) <= \<const0>\;
  lp_rx_o_n(8) <= \<const0>\;
  lp_rx_o_n(7) <= \<const0>\;
  lp_rx_o_n(6) <= \<const0>\;
  lp_rx_o_n(5) <= \<const0>\;
  lp_rx_o_n(4) <= \<const0>\;
  lp_rx_o_n(3) <= \<const0>\;
  lp_rx_o_n(2) <= \<const0>\;
  lp_rx_o_n(1) <= \<const0>\;
  lp_rx_o_n(0) <= \<const0>\;
  lp_rx_o_p(15) <= \<const0>\;
  lp_rx_o_p(14) <= \<const0>\;
  lp_rx_o_p(13) <= \<const0>\;
  lp_rx_o_p(12) <= \<const0>\;
  lp_rx_o_p(11) <= \<const0>\;
  lp_rx_o_p(10) <= \<const0>\;
  lp_rx_o_p(9) <= \<const0>\;
  lp_rx_o_p(8) <= \<const0>\;
  lp_rx_o_p(7) <= \<const0>\;
  lp_rx_o_p(6) <= \<const0>\;
  lp_rx_o_p(5) <= \<const0>\;
  lp_rx_o_p(4) <= \<const0>\;
  lp_rx_o_p(3) <= \<const0>\;
  lp_rx_o_p(2) <= \<const0>\;
  lp_rx_o_p(1) <= \<const0>\;
  lp_rx_o_p(0) <= \<const0>\;
  pll1_clkout0 <= \<const0>\;
  pll1_locked <= \<const0>\;
  riu_rd_data_bg0(15) <= \<const0>\;
  riu_rd_data_bg0(14) <= \<const0>\;
  riu_rd_data_bg0(13) <= \<const0>\;
  riu_rd_data_bg0(12) <= \<const0>\;
  riu_rd_data_bg0(11) <= \<const0>\;
  riu_rd_data_bg0(10) <= \<const0>\;
  riu_rd_data_bg0(9) <= \<const0>\;
  riu_rd_data_bg0(8) <= \<const0>\;
  riu_rd_data_bg0(7) <= \<const0>\;
  riu_rd_data_bg0(6) <= \<const0>\;
  riu_rd_data_bg0(5) <= \<const0>\;
  riu_rd_data_bg0(4) <= \<const0>\;
  riu_rd_data_bg0(3) <= \<const0>\;
  riu_rd_data_bg0(2) <= \<const0>\;
  riu_rd_data_bg0(1) <= \<const0>\;
  riu_rd_data_bg0(0) <= \<const0>\;
  riu_rd_data_bg0_bs0(15) <= \<const0>\;
  riu_rd_data_bg0_bs0(14) <= \<const0>\;
  riu_rd_data_bg0_bs0(13) <= \<const0>\;
  riu_rd_data_bg0_bs0(12) <= \<const0>\;
  riu_rd_data_bg0_bs0(11) <= \<const0>\;
  riu_rd_data_bg0_bs0(10) <= \<const0>\;
  riu_rd_data_bg0_bs0(9) <= \<const0>\;
  riu_rd_data_bg0_bs0(8) <= \<const0>\;
  riu_rd_data_bg0_bs0(7) <= \<const0>\;
  riu_rd_data_bg0_bs0(6) <= \<const0>\;
  riu_rd_data_bg0_bs0(5) <= \<const0>\;
  riu_rd_data_bg0_bs0(4) <= \<const0>\;
  riu_rd_data_bg0_bs0(3) <= \<const0>\;
  riu_rd_data_bg0_bs0(2) <= \<const0>\;
  riu_rd_data_bg0_bs0(1) <= \<const0>\;
  riu_rd_data_bg0_bs0(0) <= \<const0>\;
  riu_rd_data_bg0_bs1(15) <= \<const0>\;
  riu_rd_data_bg0_bs1(14) <= \<const0>\;
  riu_rd_data_bg0_bs1(13) <= \<const0>\;
  riu_rd_data_bg0_bs1(12) <= \<const0>\;
  riu_rd_data_bg0_bs1(11) <= \<const0>\;
  riu_rd_data_bg0_bs1(10) <= \<const0>\;
  riu_rd_data_bg0_bs1(9) <= \<const0>\;
  riu_rd_data_bg0_bs1(8) <= \<const0>\;
  riu_rd_data_bg0_bs1(7) <= \<const0>\;
  riu_rd_data_bg0_bs1(6) <= \<const0>\;
  riu_rd_data_bg0_bs1(5) <= \<const0>\;
  riu_rd_data_bg0_bs1(4) <= \<const0>\;
  riu_rd_data_bg0_bs1(3) <= \<const0>\;
  riu_rd_data_bg0_bs1(2) <= \<const0>\;
  riu_rd_data_bg0_bs1(1) <= \<const0>\;
  riu_rd_data_bg0_bs1(0) <= \<const0>\;
  riu_rd_data_bg1(15) <= \<const0>\;
  riu_rd_data_bg1(14) <= \<const0>\;
  riu_rd_data_bg1(13) <= \<const0>\;
  riu_rd_data_bg1(12) <= \<const0>\;
  riu_rd_data_bg1(11) <= \<const0>\;
  riu_rd_data_bg1(10) <= \<const0>\;
  riu_rd_data_bg1(9) <= \<const0>\;
  riu_rd_data_bg1(8) <= \<const0>\;
  riu_rd_data_bg1(7) <= \<const0>\;
  riu_rd_data_bg1(6) <= \<const0>\;
  riu_rd_data_bg1(5) <= \<const0>\;
  riu_rd_data_bg1(4) <= \<const0>\;
  riu_rd_data_bg1(3) <= \<const0>\;
  riu_rd_data_bg1(2) <= \<const0>\;
  riu_rd_data_bg1(1) <= \<const0>\;
  riu_rd_data_bg1(0) <= \<const0>\;
  riu_rd_data_bg1_bs2(15) <= \<const0>\;
  riu_rd_data_bg1_bs2(14) <= \<const0>\;
  riu_rd_data_bg1_bs2(13) <= \<const0>\;
  riu_rd_data_bg1_bs2(12) <= \<const0>\;
  riu_rd_data_bg1_bs2(11) <= \<const0>\;
  riu_rd_data_bg1_bs2(10) <= \<const0>\;
  riu_rd_data_bg1_bs2(9) <= \<const0>\;
  riu_rd_data_bg1_bs2(8) <= \<const0>\;
  riu_rd_data_bg1_bs2(7) <= \<const0>\;
  riu_rd_data_bg1_bs2(6) <= \<const0>\;
  riu_rd_data_bg1_bs2(5) <= \<const0>\;
  riu_rd_data_bg1_bs2(4) <= \<const0>\;
  riu_rd_data_bg1_bs2(3) <= \<const0>\;
  riu_rd_data_bg1_bs2(2) <= \<const0>\;
  riu_rd_data_bg1_bs2(1) <= \<const0>\;
  riu_rd_data_bg1_bs2(0) <= \<const0>\;
  riu_rd_data_bg1_bs3(15) <= \<const0>\;
  riu_rd_data_bg1_bs3(14) <= \<const0>\;
  riu_rd_data_bg1_bs3(13) <= \<const0>\;
  riu_rd_data_bg1_bs3(12) <= \<const0>\;
  riu_rd_data_bg1_bs3(11) <= \<const0>\;
  riu_rd_data_bg1_bs3(10) <= \<const0>\;
  riu_rd_data_bg1_bs3(9) <= \<const0>\;
  riu_rd_data_bg1_bs3(8) <= \<const0>\;
  riu_rd_data_bg1_bs3(7) <= \<const0>\;
  riu_rd_data_bg1_bs3(6) <= \<const0>\;
  riu_rd_data_bg1_bs3(5) <= \<const0>\;
  riu_rd_data_bg1_bs3(4) <= \<const0>\;
  riu_rd_data_bg1_bs3(3) <= \<const0>\;
  riu_rd_data_bg1_bs3(2) <= \<const0>\;
  riu_rd_data_bg1_bs3(1) <= \<const0>\;
  riu_rd_data_bg1_bs3(0) <= \<const0>\;
  riu_rd_data_bg2(15) <= \<const0>\;
  riu_rd_data_bg2(14) <= \<const0>\;
  riu_rd_data_bg2(13) <= \<const0>\;
  riu_rd_data_bg2(12) <= \<const0>\;
  riu_rd_data_bg2(11) <= \<const0>\;
  riu_rd_data_bg2(10) <= \<const0>\;
  riu_rd_data_bg2(9) <= \<const0>\;
  riu_rd_data_bg2(8) <= \<const0>\;
  riu_rd_data_bg2(7) <= \<const0>\;
  riu_rd_data_bg2(6) <= \<const0>\;
  riu_rd_data_bg2(5) <= \<const0>\;
  riu_rd_data_bg2(4) <= \<const0>\;
  riu_rd_data_bg2(3) <= \<const0>\;
  riu_rd_data_bg2(2) <= \<const0>\;
  riu_rd_data_bg2(1) <= \<const0>\;
  riu_rd_data_bg2(0) <= \<const0>\;
  riu_rd_data_bg2_bs4(15) <= \<const0>\;
  riu_rd_data_bg2_bs4(14) <= \<const0>\;
  riu_rd_data_bg2_bs4(13) <= \<const0>\;
  riu_rd_data_bg2_bs4(12) <= \<const0>\;
  riu_rd_data_bg2_bs4(11) <= \<const0>\;
  riu_rd_data_bg2_bs4(10) <= \<const0>\;
  riu_rd_data_bg2_bs4(9) <= \<const0>\;
  riu_rd_data_bg2_bs4(8) <= \<const0>\;
  riu_rd_data_bg2_bs4(7) <= \<const0>\;
  riu_rd_data_bg2_bs4(6) <= \<const0>\;
  riu_rd_data_bg2_bs4(5) <= \<const0>\;
  riu_rd_data_bg2_bs4(4) <= \<const0>\;
  riu_rd_data_bg2_bs4(3) <= \<const0>\;
  riu_rd_data_bg2_bs4(2) <= \<const0>\;
  riu_rd_data_bg2_bs4(1) <= \<const0>\;
  riu_rd_data_bg2_bs4(0) <= \<const0>\;
  riu_rd_data_bg2_bs5(15) <= \<const0>\;
  riu_rd_data_bg2_bs5(14) <= \<const0>\;
  riu_rd_data_bg2_bs5(13) <= \<const0>\;
  riu_rd_data_bg2_bs5(12) <= \<const0>\;
  riu_rd_data_bg2_bs5(11) <= \<const0>\;
  riu_rd_data_bg2_bs5(10) <= \<const0>\;
  riu_rd_data_bg2_bs5(9) <= \<const0>\;
  riu_rd_data_bg2_bs5(8) <= \<const0>\;
  riu_rd_data_bg2_bs5(7) <= \<const0>\;
  riu_rd_data_bg2_bs5(6) <= \<const0>\;
  riu_rd_data_bg2_bs5(5) <= \<const0>\;
  riu_rd_data_bg2_bs5(4) <= \<const0>\;
  riu_rd_data_bg2_bs5(3) <= \<const0>\;
  riu_rd_data_bg2_bs5(2) <= \<const0>\;
  riu_rd_data_bg2_bs5(1) <= \<const0>\;
  riu_rd_data_bg2_bs5(0) <= \<const0>\;
  riu_rd_data_bg3(15) <= \<const0>\;
  riu_rd_data_bg3(14) <= \<const0>\;
  riu_rd_data_bg3(13) <= \<const0>\;
  riu_rd_data_bg3(12) <= \<const0>\;
  riu_rd_data_bg3(11) <= \<const0>\;
  riu_rd_data_bg3(10) <= \<const0>\;
  riu_rd_data_bg3(9) <= \<const0>\;
  riu_rd_data_bg3(8) <= \<const0>\;
  riu_rd_data_bg3(7) <= \<const0>\;
  riu_rd_data_bg3(6) <= \<const0>\;
  riu_rd_data_bg3(5) <= \<const0>\;
  riu_rd_data_bg3(4) <= \<const0>\;
  riu_rd_data_bg3(3) <= \<const0>\;
  riu_rd_data_bg3(2) <= \<const0>\;
  riu_rd_data_bg3(1) <= \<const0>\;
  riu_rd_data_bg3(0) <= \<const0>\;
  riu_rd_data_bg3_bs6(15) <= \<const0>\;
  riu_rd_data_bg3_bs6(14) <= \<const0>\;
  riu_rd_data_bg3_bs6(13) <= \<const0>\;
  riu_rd_data_bg3_bs6(12) <= \<const0>\;
  riu_rd_data_bg3_bs6(11) <= \<const0>\;
  riu_rd_data_bg3_bs6(10) <= \<const0>\;
  riu_rd_data_bg3_bs6(9) <= \<const0>\;
  riu_rd_data_bg3_bs6(8) <= \<const0>\;
  riu_rd_data_bg3_bs6(7) <= \<const0>\;
  riu_rd_data_bg3_bs6(6) <= \<const0>\;
  riu_rd_data_bg3_bs6(5) <= \<const0>\;
  riu_rd_data_bg3_bs6(4) <= \<const0>\;
  riu_rd_data_bg3_bs6(3) <= \<const0>\;
  riu_rd_data_bg3_bs6(2) <= \<const0>\;
  riu_rd_data_bg3_bs6(1) <= \<const0>\;
  riu_rd_data_bg3_bs6(0) <= \<const0>\;
  riu_rd_data_bg3_bs7(15) <= \<const0>\;
  riu_rd_data_bg3_bs7(14) <= \<const0>\;
  riu_rd_data_bg3_bs7(13) <= \<const0>\;
  riu_rd_data_bg3_bs7(12) <= \<const0>\;
  riu_rd_data_bg3_bs7(11) <= \<const0>\;
  riu_rd_data_bg3_bs7(10) <= \<const0>\;
  riu_rd_data_bg3_bs7(9) <= \<const0>\;
  riu_rd_data_bg3_bs7(8) <= \<const0>\;
  riu_rd_data_bg3_bs7(7) <= \<const0>\;
  riu_rd_data_bg3_bs7(6) <= \<const0>\;
  riu_rd_data_bg3_bs7(5) <= \<const0>\;
  riu_rd_data_bg3_bs7(4) <= \<const0>\;
  riu_rd_data_bg3_bs7(3) <= \<const0>\;
  riu_rd_data_bg3_bs7(2) <= \<const0>\;
  riu_rd_data_bg3_bs7(1) <= \<const0>\;
  riu_rd_data_bg3_bs7(0) <= \<const0>\;
  riu_valid_bg0 <= \<const0>\;
  riu_valid_bg0_bs0 <= \<const0>\;
  riu_valid_bg0_bs1 <= \<const0>\;
  riu_valid_bg1 <= \<const0>\;
  riu_valid_bg1_bs2 <= \<const0>\;
  riu_valid_bg1_bs3 <= \<const0>\;
  riu_valid_bg2 <= \<const0>\;
  riu_valid_bg2_bs4 <= \<const0>\;
  riu_valid_bg2_bs5 <= \<const0>\;
  riu_valid_bg3 <= \<const0>\;
  riu_valid_bg3_bs6 <= \<const0>\;
  riu_valid_bg3_bs7 <= \<const0>\;
  rx_bitslip_sync_done <= \<const0>\;
  rx_cntvalueout_0(8) <= \<const0>\;
  rx_cntvalueout_0(7) <= \<const0>\;
  rx_cntvalueout_0(6) <= \<const0>\;
  rx_cntvalueout_0(5) <= \<const0>\;
  rx_cntvalueout_0(4) <= \<const0>\;
  rx_cntvalueout_0(3) <= \<const0>\;
  rx_cntvalueout_0(2) <= \<const0>\;
  rx_cntvalueout_0(1) <= \<const0>\;
  rx_cntvalueout_0(0) <= \<const0>\;
  rx_cntvalueout_1(8) <= \<const0>\;
  rx_cntvalueout_1(7) <= \<const0>\;
  rx_cntvalueout_1(6) <= \<const0>\;
  rx_cntvalueout_1(5) <= \<const0>\;
  rx_cntvalueout_1(4) <= \<const0>\;
  rx_cntvalueout_1(3) <= \<const0>\;
  rx_cntvalueout_1(2) <= \<const0>\;
  rx_cntvalueout_1(1) <= \<const0>\;
  rx_cntvalueout_1(0) <= \<const0>\;
  rx_cntvalueout_10(8) <= \<const0>\;
  rx_cntvalueout_10(7) <= \<const0>\;
  rx_cntvalueout_10(6) <= \<const0>\;
  rx_cntvalueout_10(5) <= \<const0>\;
  rx_cntvalueout_10(4) <= \<const0>\;
  rx_cntvalueout_10(3) <= \<const0>\;
  rx_cntvalueout_10(2) <= \<const0>\;
  rx_cntvalueout_10(1) <= \<const0>\;
  rx_cntvalueout_10(0) <= \<const0>\;
  rx_cntvalueout_11(8) <= \<const0>\;
  rx_cntvalueout_11(7) <= \<const0>\;
  rx_cntvalueout_11(6) <= \<const0>\;
  rx_cntvalueout_11(5) <= \<const0>\;
  rx_cntvalueout_11(4) <= \<const0>\;
  rx_cntvalueout_11(3) <= \<const0>\;
  rx_cntvalueout_11(2) <= \<const0>\;
  rx_cntvalueout_11(1) <= \<const0>\;
  rx_cntvalueout_11(0) <= \<const0>\;
  rx_cntvalueout_12(8) <= \<const0>\;
  rx_cntvalueout_12(7) <= \<const0>\;
  rx_cntvalueout_12(6) <= \<const0>\;
  rx_cntvalueout_12(5) <= \<const0>\;
  rx_cntvalueout_12(4) <= \<const0>\;
  rx_cntvalueout_12(3) <= \<const0>\;
  rx_cntvalueout_12(2) <= \<const0>\;
  rx_cntvalueout_12(1) <= \<const0>\;
  rx_cntvalueout_12(0) <= \<const0>\;
  rx_cntvalueout_13(8) <= \<const0>\;
  rx_cntvalueout_13(7) <= \<const0>\;
  rx_cntvalueout_13(6) <= \<const0>\;
  rx_cntvalueout_13(5) <= \<const0>\;
  rx_cntvalueout_13(4) <= \<const0>\;
  rx_cntvalueout_13(3) <= \<const0>\;
  rx_cntvalueout_13(2) <= \<const0>\;
  rx_cntvalueout_13(1) <= \<const0>\;
  rx_cntvalueout_13(0) <= \<const0>\;
  rx_cntvalueout_14(8) <= \<const0>\;
  rx_cntvalueout_14(7) <= \<const0>\;
  rx_cntvalueout_14(6) <= \<const0>\;
  rx_cntvalueout_14(5) <= \<const0>\;
  rx_cntvalueout_14(4) <= \<const0>\;
  rx_cntvalueout_14(3) <= \<const0>\;
  rx_cntvalueout_14(2) <= \<const0>\;
  rx_cntvalueout_14(1) <= \<const0>\;
  rx_cntvalueout_14(0) <= \<const0>\;
  rx_cntvalueout_15(8) <= \<const0>\;
  rx_cntvalueout_15(7) <= \<const0>\;
  rx_cntvalueout_15(6) <= \<const0>\;
  rx_cntvalueout_15(5) <= \<const0>\;
  rx_cntvalueout_15(4) <= \<const0>\;
  rx_cntvalueout_15(3) <= \<const0>\;
  rx_cntvalueout_15(2) <= \<const0>\;
  rx_cntvalueout_15(1) <= \<const0>\;
  rx_cntvalueout_15(0) <= \<const0>\;
  rx_cntvalueout_16(8) <= \<const0>\;
  rx_cntvalueout_16(7) <= \<const0>\;
  rx_cntvalueout_16(6) <= \<const0>\;
  rx_cntvalueout_16(5) <= \<const0>\;
  rx_cntvalueout_16(4) <= \<const0>\;
  rx_cntvalueout_16(3) <= \<const0>\;
  rx_cntvalueout_16(2) <= \<const0>\;
  rx_cntvalueout_16(1) <= \<const0>\;
  rx_cntvalueout_16(0) <= \<const0>\;
  rx_cntvalueout_17(8) <= \<const0>\;
  rx_cntvalueout_17(7) <= \<const0>\;
  rx_cntvalueout_17(6) <= \<const0>\;
  rx_cntvalueout_17(5) <= \<const0>\;
  rx_cntvalueout_17(4) <= \<const0>\;
  rx_cntvalueout_17(3) <= \<const0>\;
  rx_cntvalueout_17(2) <= \<const0>\;
  rx_cntvalueout_17(1) <= \<const0>\;
  rx_cntvalueout_17(0) <= \<const0>\;
  rx_cntvalueout_18(8) <= \<const0>\;
  rx_cntvalueout_18(7) <= \<const0>\;
  rx_cntvalueout_18(6) <= \<const0>\;
  rx_cntvalueout_18(5) <= \<const0>\;
  rx_cntvalueout_18(4) <= \<const0>\;
  rx_cntvalueout_18(3) <= \<const0>\;
  rx_cntvalueout_18(2) <= \<const0>\;
  rx_cntvalueout_18(1) <= \<const0>\;
  rx_cntvalueout_18(0) <= \<const0>\;
  rx_cntvalueout_19(8) <= \<const0>\;
  rx_cntvalueout_19(7) <= \<const0>\;
  rx_cntvalueout_19(6) <= \<const0>\;
  rx_cntvalueout_19(5) <= \<const0>\;
  rx_cntvalueout_19(4) <= \<const0>\;
  rx_cntvalueout_19(3) <= \<const0>\;
  rx_cntvalueout_19(2) <= \<const0>\;
  rx_cntvalueout_19(1) <= \<const0>\;
  rx_cntvalueout_19(0) <= \<const0>\;
  rx_cntvalueout_2(8) <= \<const0>\;
  rx_cntvalueout_2(7) <= \<const0>\;
  rx_cntvalueout_2(6) <= \<const0>\;
  rx_cntvalueout_2(5) <= \<const0>\;
  rx_cntvalueout_2(4) <= \<const0>\;
  rx_cntvalueout_2(3) <= \<const0>\;
  rx_cntvalueout_2(2) <= \<const0>\;
  rx_cntvalueout_2(1) <= \<const0>\;
  rx_cntvalueout_2(0) <= \<const0>\;
  rx_cntvalueout_20(8) <= \<const0>\;
  rx_cntvalueout_20(7) <= \<const0>\;
  rx_cntvalueout_20(6) <= \<const0>\;
  rx_cntvalueout_20(5) <= \<const0>\;
  rx_cntvalueout_20(4) <= \<const0>\;
  rx_cntvalueout_20(3) <= \<const0>\;
  rx_cntvalueout_20(2) <= \<const0>\;
  rx_cntvalueout_20(1) <= \<const0>\;
  rx_cntvalueout_20(0) <= \<const0>\;
  rx_cntvalueout_21(8) <= \<const0>\;
  rx_cntvalueout_21(7) <= \<const0>\;
  rx_cntvalueout_21(6) <= \<const0>\;
  rx_cntvalueout_21(5) <= \<const0>\;
  rx_cntvalueout_21(4) <= \<const0>\;
  rx_cntvalueout_21(3) <= \<const0>\;
  rx_cntvalueout_21(2) <= \<const0>\;
  rx_cntvalueout_21(1) <= \<const0>\;
  rx_cntvalueout_21(0) <= \<const0>\;
  rx_cntvalueout_22(8) <= \<const0>\;
  rx_cntvalueout_22(7) <= \<const0>\;
  rx_cntvalueout_22(6) <= \<const0>\;
  rx_cntvalueout_22(5) <= \<const0>\;
  rx_cntvalueout_22(4) <= \<const0>\;
  rx_cntvalueout_22(3) <= \<const0>\;
  rx_cntvalueout_22(2) <= \<const0>\;
  rx_cntvalueout_22(1) <= \<const0>\;
  rx_cntvalueout_22(0) <= \<const0>\;
  rx_cntvalueout_23(8) <= \<const0>\;
  rx_cntvalueout_23(7) <= \<const0>\;
  rx_cntvalueout_23(6) <= \<const0>\;
  rx_cntvalueout_23(5) <= \<const0>\;
  rx_cntvalueout_23(4) <= \<const0>\;
  rx_cntvalueout_23(3) <= \<const0>\;
  rx_cntvalueout_23(2) <= \<const0>\;
  rx_cntvalueout_23(1) <= \<const0>\;
  rx_cntvalueout_23(0) <= \<const0>\;
  rx_cntvalueout_24(8) <= \<const0>\;
  rx_cntvalueout_24(7) <= \<const0>\;
  rx_cntvalueout_24(6) <= \<const0>\;
  rx_cntvalueout_24(5) <= \<const0>\;
  rx_cntvalueout_24(4) <= \<const0>\;
  rx_cntvalueout_24(3) <= \<const0>\;
  rx_cntvalueout_24(2) <= \<const0>\;
  rx_cntvalueout_24(1) <= \<const0>\;
  rx_cntvalueout_24(0) <= \<const0>\;
  rx_cntvalueout_25(8) <= \<const0>\;
  rx_cntvalueout_25(7) <= \<const0>\;
  rx_cntvalueout_25(6) <= \<const0>\;
  rx_cntvalueout_25(5) <= \<const0>\;
  rx_cntvalueout_25(4) <= \<const0>\;
  rx_cntvalueout_25(3) <= \<const0>\;
  rx_cntvalueout_25(2) <= \<const0>\;
  rx_cntvalueout_25(1) <= \<const0>\;
  rx_cntvalueout_25(0) <= \<const0>\;
  rx_cntvalueout_26(8) <= \<const0>\;
  rx_cntvalueout_26(7) <= \<const0>\;
  rx_cntvalueout_26(6) <= \<const0>\;
  rx_cntvalueout_26(5) <= \<const0>\;
  rx_cntvalueout_26(4) <= \<const0>\;
  rx_cntvalueout_26(3) <= \<const0>\;
  rx_cntvalueout_26(2) <= \<const0>\;
  rx_cntvalueout_26(1) <= \<const0>\;
  rx_cntvalueout_26(0) <= \<const0>\;
  rx_cntvalueout_27(8) <= \<const0>\;
  rx_cntvalueout_27(7) <= \<const0>\;
  rx_cntvalueout_27(6) <= \<const0>\;
  rx_cntvalueout_27(5) <= \<const0>\;
  rx_cntvalueout_27(4) <= \<const0>\;
  rx_cntvalueout_27(3) <= \<const0>\;
  rx_cntvalueout_27(2) <= \<const0>\;
  rx_cntvalueout_27(1) <= \<const0>\;
  rx_cntvalueout_27(0) <= \<const0>\;
  rx_cntvalueout_28(8) <= \<const0>\;
  rx_cntvalueout_28(7) <= \<const0>\;
  rx_cntvalueout_28(6) <= \<const0>\;
  rx_cntvalueout_28(5) <= \<const0>\;
  rx_cntvalueout_28(4) <= \<const0>\;
  rx_cntvalueout_28(3) <= \<const0>\;
  rx_cntvalueout_28(2) <= \<const0>\;
  rx_cntvalueout_28(1) <= \<const0>\;
  rx_cntvalueout_28(0) <= \<const0>\;
  rx_cntvalueout_29(8) <= \<const0>\;
  rx_cntvalueout_29(7) <= \<const0>\;
  rx_cntvalueout_29(6) <= \<const0>\;
  rx_cntvalueout_29(5) <= \<const0>\;
  rx_cntvalueout_29(4) <= \<const0>\;
  rx_cntvalueout_29(3) <= \<const0>\;
  rx_cntvalueout_29(2) <= \<const0>\;
  rx_cntvalueout_29(1) <= \<const0>\;
  rx_cntvalueout_29(0) <= \<const0>\;
  rx_cntvalueout_3(8) <= \<const0>\;
  rx_cntvalueout_3(7) <= \<const0>\;
  rx_cntvalueout_3(6) <= \<const0>\;
  rx_cntvalueout_3(5) <= \<const0>\;
  rx_cntvalueout_3(4) <= \<const0>\;
  rx_cntvalueout_3(3) <= \<const0>\;
  rx_cntvalueout_3(2) <= \<const0>\;
  rx_cntvalueout_3(1) <= \<const0>\;
  rx_cntvalueout_3(0) <= \<const0>\;
  rx_cntvalueout_30(8) <= \<const0>\;
  rx_cntvalueout_30(7) <= \<const0>\;
  rx_cntvalueout_30(6) <= \<const0>\;
  rx_cntvalueout_30(5) <= \<const0>\;
  rx_cntvalueout_30(4) <= \<const0>\;
  rx_cntvalueout_30(3) <= \<const0>\;
  rx_cntvalueout_30(2) <= \<const0>\;
  rx_cntvalueout_30(1) <= \<const0>\;
  rx_cntvalueout_30(0) <= \<const0>\;
  rx_cntvalueout_31(8) <= \<const0>\;
  rx_cntvalueout_31(7) <= \<const0>\;
  rx_cntvalueout_31(6) <= \<const0>\;
  rx_cntvalueout_31(5) <= \<const0>\;
  rx_cntvalueout_31(4) <= \<const0>\;
  rx_cntvalueout_31(3) <= \<const0>\;
  rx_cntvalueout_31(2) <= \<const0>\;
  rx_cntvalueout_31(1) <= \<const0>\;
  rx_cntvalueout_31(0) <= \<const0>\;
  rx_cntvalueout_32(8) <= \<const0>\;
  rx_cntvalueout_32(7) <= \<const0>\;
  rx_cntvalueout_32(6) <= \<const0>\;
  rx_cntvalueout_32(5) <= \<const0>\;
  rx_cntvalueout_32(4) <= \<const0>\;
  rx_cntvalueout_32(3) <= \<const0>\;
  rx_cntvalueout_32(2) <= \<const0>\;
  rx_cntvalueout_32(1) <= \<const0>\;
  rx_cntvalueout_32(0) <= \<const0>\;
  rx_cntvalueout_33(8) <= \<const0>\;
  rx_cntvalueout_33(7) <= \<const0>\;
  rx_cntvalueout_33(6) <= \<const0>\;
  rx_cntvalueout_33(5) <= \<const0>\;
  rx_cntvalueout_33(4) <= \<const0>\;
  rx_cntvalueout_33(3) <= \<const0>\;
  rx_cntvalueout_33(2) <= \<const0>\;
  rx_cntvalueout_33(1) <= \<const0>\;
  rx_cntvalueout_33(0) <= \<const0>\;
  rx_cntvalueout_34(8) <= \<const0>\;
  rx_cntvalueout_34(7) <= \<const0>\;
  rx_cntvalueout_34(6) <= \<const0>\;
  rx_cntvalueout_34(5) <= \<const0>\;
  rx_cntvalueout_34(4) <= \<const0>\;
  rx_cntvalueout_34(3) <= \<const0>\;
  rx_cntvalueout_34(2) <= \<const0>\;
  rx_cntvalueout_34(1) <= \<const0>\;
  rx_cntvalueout_34(0) <= \<const0>\;
  rx_cntvalueout_35(8) <= \<const0>\;
  rx_cntvalueout_35(7) <= \<const0>\;
  rx_cntvalueout_35(6) <= \<const0>\;
  rx_cntvalueout_35(5) <= \<const0>\;
  rx_cntvalueout_35(4) <= \<const0>\;
  rx_cntvalueout_35(3) <= \<const0>\;
  rx_cntvalueout_35(2) <= \<const0>\;
  rx_cntvalueout_35(1) <= \<const0>\;
  rx_cntvalueout_35(0) <= \<const0>\;
  rx_cntvalueout_36(8) <= \<const0>\;
  rx_cntvalueout_36(7) <= \<const0>\;
  rx_cntvalueout_36(6) <= \<const0>\;
  rx_cntvalueout_36(5) <= \<const0>\;
  rx_cntvalueout_36(4) <= \<const0>\;
  rx_cntvalueout_36(3) <= \<const0>\;
  rx_cntvalueout_36(2) <= \<const0>\;
  rx_cntvalueout_36(1) <= \<const0>\;
  rx_cntvalueout_36(0) <= \<const0>\;
  rx_cntvalueout_37(8) <= \<const0>\;
  rx_cntvalueout_37(7) <= \<const0>\;
  rx_cntvalueout_37(6) <= \<const0>\;
  rx_cntvalueout_37(5) <= \<const0>\;
  rx_cntvalueout_37(4) <= \<const0>\;
  rx_cntvalueout_37(3) <= \<const0>\;
  rx_cntvalueout_37(2) <= \<const0>\;
  rx_cntvalueout_37(1) <= \<const0>\;
  rx_cntvalueout_37(0) <= \<const0>\;
  rx_cntvalueout_38(8) <= \<const0>\;
  rx_cntvalueout_38(7) <= \<const0>\;
  rx_cntvalueout_38(6) <= \<const0>\;
  rx_cntvalueout_38(5) <= \<const0>\;
  rx_cntvalueout_38(4) <= \<const0>\;
  rx_cntvalueout_38(3) <= \<const0>\;
  rx_cntvalueout_38(2) <= \<const0>\;
  rx_cntvalueout_38(1) <= \<const0>\;
  rx_cntvalueout_38(0) <= \<const0>\;
  rx_cntvalueout_39(8) <= \<const0>\;
  rx_cntvalueout_39(7) <= \<const0>\;
  rx_cntvalueout_39(6) <= \<const0>\;
  rx_cntvalueout_39(5) <= \<const0>\;
  rx_cntvalueout_39(4) <= \<const0>\;
  rx_cntvalueout_39(3) <= \<const0>\;
  rx_cntvalueout_39(2) <= \<const0>\;
  rx_cntvalueout_39(1) <= \<const0>\;
  rx_cntvalueout_39(0) <= \<const0>\;
  rx_cntvalueout_4(8) <= \<const0>\;
  rx_cntvalueout_4(7) <= \<const0>\;
  rx_cntvalueout_4(6) <= \<const0>\;
  rx_cntvalueout_4(5) <= \<const0>\;
  rx_cntvalueout_4(4) <= \<const0>\;
  rx_cntvalueout_4(3) <= \<const0>\;
  rx_cntvalueout_4(2) <= \<const0>\;
  rx_cntvalueout_4(1) <= \<const0>\;
  rx_cntvalueout_4(0) <= \<const0>\;
  rx_cntvalueout_40(8) <= \<const0>\;
  rx_cntvalueout_40(7) <= \<const0>\;
  rx_cntvalueout_40(6) <= \<const0>\;
  rx_cntvalueout_40(5) <= \<const0>\;
  rx_cntvalueout_40(4) <= \<const0>\;
  rx_cntvalueout_40(3) <= \<const0>\;
  rx_cntvalueout_40(2) <= \<const0>\;
  rx_cntvalueout_40(1) <= \<const0>\;
  rx_cntvalueout_40(0) <= \<const0>\;
  rx_cntvalueout_41(8) <= \<const0>\;
  rx_cntvalueout_41(7) <= \<const0>\;
  rx_cntvalueout_41(6) <= \<const0>\;
  rx_cntvalueout_41(5) <= \<const0>\;
  rx_cntvalueout_41(4) <= \<const0>\;
  rx_cntvalueout_41(3) <= \<const0>\;
  rx_cntvalueout_41(2) <= \<const0>\;
  rx_cntvalueout_41(1) <= \<const0>\;
  rx_cntvalueout_41(0) <= \<const0>\;
  rx_cntvalueout_42(8) <= \<const0>\;
  rx_cntvalueout_42(7) <= \<const0>\;
  rx_cntvalueout_42(6) <= \<const0>\;
  rx_cntvalueout_42(5) <= \<const0>\;
  rx_cntvalueout_42(4) <= \<const0>\;
  rx_cntvalueout_42(3) <= \<const0>\;
  rx_cntvalueout_42(2) <= \<const0>\;
  rx_cntvalueout_42(1) <= \<const0>\;
  rx_cntvalueout_42(0) <= \<const0>\;
  rx_cntvalueout_43(8) <= \<const0>\;
  rx_cntvalueout_43(7) <= \<const0>\;
  rx_cntvalueout_43(6) <= \<const0>\;
  rx_cntvalueout_43(5) <= \<const0>\;
  rx_cntvalueout_43(4) <= \<const0>\;
  rx_cntvalueout_43(3) <= \<const0>\;
  rx_cntvalueout_43(2) <= \<const0>\;
  rx_cntvalueout_43(1) <= \<const0>\;
  rx_cntvalueout_43(0) <= \<const0>\;
  rx_cntvalueout_44(8) <= \<const0>\;
  rx_cntvalueout_44(7) <= \<const0>\;
  rx_cntvalueout_44(6) <= \<const0>\;
  rx_cntvalueout_44(5) <= \<const0>\;
  rx_cntvalueout_44(4) <= \<const0>\;
  rx_cntvalueout_44(3) <= \<const0>\;
  rx_cntvalueout_44(2) <= \<const0>\;
  rx_cntvalueout_44(1) <= \<const0>\;
  rx_cntvalueout_44(0) <= \<const0>\;
  rx_cntvalueout_45(8) <= \<const0>\;
  rx_cntvalueout_45(7) <= \<const0>\;
  rx_cntvalueout_45(6) <= \<const0>\;
  rx_cntvalueout_45(5) <= \<const0>\;
  rx_cntvalueout_45(4) <= \<const0>\;
  rx_cntvalueout_45(3) <= \<const0>\;
  rx_cntvalueout_45(2) <= \<const0>\;
  rx_cntvalueout_45(1) <= \<const0>\;
  rx_cntvalueout_45(0) <= \<const0>\;
  rx_cntvalueout_46(8) <= \<const0>\;
  rx_cntvalueout_46(7) <= \<const0>\;
  rx_cntvalueout_46(6) <= \<const0>\;
  rx_cntvalueout_46(5) <= \<const0>\;
  rx_cntvalueout_46(4) <= \<const0>\;
  rx_cntvalueout_46(3) <= \<const0>\;
  rx_cntvalueout_46(2) <= \<const0>\;
  rx_cntvalueout_46(1) <= \<const0>\;
  rx_cntvalueout_46(0) <= \<const0>\;
  rx_cntvalueout_47(8) <= \<const0>\;
  rx_cntvalueout_47(7) <= \<const0>\;
  rx_cntvalueout_47(6) <= \<const0>\;
  rx_cntvalueout_47(5) <= \<const0>\;
  rx_cntvalueout_47(4) <= \<const0>\;
  rx_cntvalueout_47(3) <= \<const0>\;
  rx_cntvalueout_47(2) <= \<const0>\;
  rx_cntvalueout_47(1) <= \<const0>\;
  rx_cntvalueout_47(0) <= \<const0>\;
  rx_cntvalueout_48(8) <= \<const0>\;
  rx_cntvalueout_48(7) <= \<const0>\;
  rx_cntvalueout_48(6) <= \<const0>\;
  rx_cntvalueout_48(5) <= \<const0>\;
  rx_cntvalueout_48(4) <= \<const0>\;
  rx_cntvalueout_48(3) <= \<const0>\;
  rx_cntvalueout_48(2) <= \<const0>\;
  rx_cntvalueout_48(1) <= \<const0>\;
  rx_cntvalueout_48(0) <= \<const0>\;
  rx_cntvalueout_49(8) <= \<const0>\;
  rx_cntvalueout_49(7) <= \<const0>\;
  rx_cntvalueout_49(6) <= \<const0>\;
  rx_cntvalueout_49(5) <= \<const0>\;
  rx_cntvalueout_49(4) <= \<const0>\;
  rx_cntvalueout_49(3) <= \<const0>\;
  rx_cntvalueout_49(2) <= \<const0>\;
  rx_cntvalueout_49(1) <= \<const0>\;
  rx_cntvalueout_49(0) <= \<const0>\;
  rx_cntvalueout_5(8) <= \<const0>\;
  rx_cntvalueout_5(7) <= \<const0>\;
  rx_cntvalueout_5(6) <= \<const0>\;
  rx_cntvalueout_5(5) <= \<const0>\;
  rx_cntvalueout_5(4) <= \<const0>\;
  rx_cntvalueout_5(3) <= \<const0>\;
  rx_cntvalueout_5(2) <= \<const0>\;
  rx_cntvalueout_5(1) <= \<const0>\;
  rx_cntvalueout_5(0) <= \<const0>\;
  rx_cntvalueout_50(8) <= \<const0>\;
  rx_cntvalueout_50(7) <= \<const0>\;
  rx_cntvalueout_50(6) <= \<const0>\;
  rx_cntvalueout_50(5) <= \<const0>\;
  rx_cntvalueout_50(4) <= \<const0>\;
  rx_cntvalueout_50(3) <= \<const0>\;
  rx_cntvalueout_50(2) <= \<const0>\;
  rx_cntvalueout_50(1) <= \<const0>\;
  rx_cntvalueout_50(0) <= \<const0>\;
  rx_cntvalueout_51(8) <= \<const0>\;
  rx_cntvalueout_51(7) <= \<const0>\;
  rx_cntvalueout_51(6) <= \<const0>\;
  rx_cntvalueout_51(5) <= \<const0>\;
  rx_cntvalueout_51(4) <= \<const0>\;
  rx_cntvalueout_51(3) <= \<const0>\;
  rx_cntvalueout_51(2) <= \<const0>\;
  rx_cntvalueout_51(1) <= \<const0>\;
  rx_cntvalueout_51(0) <= \<const0>\;
  rx_cntvalueout_6(8) <= \<const0>\;
  rx_cntvalueout_6(7) <= \<const0>\;
  rx_cntvalueout_6(6) <= \<const0>\;
  rx_cntvalueout_6(5) <= \<const0>\;
  rx_cntvalueout_6(4) <= \<const0>\;
  rx_cntvalueout_6(3) <= \<const0>\;
  rx_cntvalueout_6(2) <= \<const0>\;
  rx_cntvalueout_6(1) <= \<const0>\;
  rx_cntvalueout_6(0) <= \<const0>\;
  rx_cntvalueout_7(8) <= \<const0>\;
  rx_cntvalueout_7(7) <= \<const0>\;
  rx_cntvalueout_7(6) <= \<const0>\;
  rx_cntvalueout_7(5) <= \<const0>\;
  rx_cntvalueout_7(4) <= \<const0>\;
  rx_cntvalueout_7(3) <= \<const0>\;
  rx_cntvalueout_7(2) <= \<const0>\;
  rx_cntvalueout_7(1) <= \<const0>\;
  rx_cntvalueout_7(0) <= \<const0>\;
  rx_cntvalueout_8(8) <= \<const0>\;
  rx_cntvalueout_8(7) <= \<const0>\;
  rx_cntvalueout_8(6) <= \<const0>\;
  rx_cntvalueout_8(5) <= \<const0>\;
  rx_cntvalueout_8(4) <= \<const0>\;
  rx_cntvalueout_8(3) <= \<const0>\;
  rx_cntvalueout_8(2) <= \<const0>\;
  rx_cntvalueout_8(1) <= \<const0>\;
  rx_cntvalueout_8(0) <= \<const0>\;
  rx_cntvalueout_9(8) <= \<const0>\;
  rx_cntvalueout_9(7) <= \<const0>\;
  rx_cntvalueout_9(6) <= \<const0>\;
  rx_cntvalueout_9(5) <= \<const0>\;
  rx_cntvalueout_9(4) <= \<const0>\;
  rx_cntvalueout_9(3) <= \<const0>\;
  rx_cntvalueout_9(2) <= \<const0>\;
  rx_cntvalueout_9(1) <= \<const0>\;
  rx_cntvalueout_9(0) <= \<const0>\;
  rx_cntvalueout_ext_0(8) <= \<const0>\;
  rx_cntvalueout_ext_0(7) <= \<const0>\;
  rx_cntvalueout_ext_0(6) <= \<const0>\;
  rx_cntvalueout_ext_0(5) <= \<const0>\;
  rx_cntvalueout_ext_0(4) <= \<const0>\;
  rx_cntvalueout_ext_0(3) <= \<const0>\;
  rx_cntvalueout_ext_0(2) <= \<const0>\;
  rx_cntvalueout_ext_0(1) <= \<const0>\;
  rx_cntvalueout_ext_0(0) <= \<const0>\;
  rx_cntvalueout_ext_1(8) <= \<const0>\;
  rx_cntvalueout_ext_1(7) <= \<const0>\;
  rx_cntvalueout_ext_1(6) <= \<const0>\;
  rx_cntvalueout_ext_1(5) <= \<const0>\;
  rx_cntvalueout_ext_1(4) <= \<const0>\;
  rx_cntvalueout_ext_1(3) <= \<const0>\;
  rx_cntvalueout_ext_1(2) <= \<const0>\;
  rx_cntvalueout_ext_1(1) <= \<const0>\;
  rx_cntvalueout_ext_1(0) <= \<const0>\;
  rx_cntvalueout_ext_10(8) <= \<const0>\;
  rx_cntvalueout_ext_10(7) <= \<const0>\;
  rx_cntvalueout_ext_10(6) <= \<const0>\;
  rx_cntvalueout_ext_10(5) <= \<const0>\;
  rx_cntvalueout_ext_10(4) <= \<const0>\;
  rx_cntvalueout_ext_10(3) <= \<const0>\;
  rx_cntvalueout_ext_10(2) <= \<const0>\;
  rx_cntvalueout_ext_10(1) <= \<const0>\;
  rx_cntvalueout_ext_10(0) <= \<const0>\;
  rx_cntvalueout_ext_11(8) <= \<const0>\;
  rx_cntvalueout_ext_11(7) <= \<const0>\;
  rx_cntvalueout_ext_11(6) <= \<const0>\;
  rx_cntvalueout_ext_11(5) <= \<const0>\;
  rx_cntvalueout_ext_11(4) <= \<const0>\;
  rx_cntvalueout_ext_11(3) <= \<const0>\;
  rx_cntvalueout_ext_11(2) <= \<const0>\;
  rx_cntvalueout_ext_11(1) <= \<const0>\;
  rx_cntvalueout_ext_11(0) <= \<const0>\;
  rx_cntvalueout_ext_12(8) <= \<const0>\;
  rx_cntvalueout_ext_12(7) <= \<const0>\;
  rx_cntvalueout_ext_12(6) <= \<const0>\;
  rx_cntvalueout_ext_12(5) <= \<const0>\;
  rx_cntvalueout_ext_12(4) <= \<const0>\;
  rx_cntvalueout_ext_12(3) <= \<const0>\;
  rx_cntvalueout_ext_12(2) <= \<const0>\;
  rx_cntvalueout_ext_12(1) <= \<const0>\;
  rx_cntvalueout_ext_12(0) <= \<const0>\;
  rx_cntvalueout_ext_13(8) <= \<const0>\;
  rx_cntvalueout_ext_13(7) <= \<const0>\;
  rx_cntvalueout_ext_13(6) <= \<const0>\;
  rx_cntvalueout_ext_13(5) <= \<const0>\;
  rx_cntvalueout_ext_13(4) <= \<const0>\;
  rx_cntvalueout_ext_13(3) <= \<const0>\;
  rx_cntvalueout_ext_13(2) <= \<const0>\;
  rx_cntvalueout_ext_13(1) <= \<const0>\;
  rx_cntvalueout_ext_13(0) <= \<const0>\;
  rx_cntvalueout_ext_14(8) <= \<const0>\;
  rx_cntvalueout_ext_14(7) <= \<const0>\;
  rx_cntvalueout_ext_14(6) <= \<const0>\;
  rx_cntvalueout_ext_14(5) <= \<const0>\;
  rx_cntvalueout_ext_14(4) <= \<const0>\;
  rx_cntvalueout_ext_14(3) <= \<const0>\;
  rx_cntvalueout_ext_14(2) <= \<const0>\;
  rx_cntvalueout_ext_14(1) <= \<const0>\;
  rx_cntvalueout_ext_14(0) <= \<const0>\;
  rx_cntvalueout_ext_15(8) <= \<const0>\;
  rx_cntvalueout_ext_15(7) <= \<const0>\;
  rx_cntvalueout_ext_15(6) <= \<const0>\;
  rx_cntvalueout_ext_15(5) <= \<const0>\;
  rx_cntvalueout_ext_15(4) <= \<const0>\;
  rx_cntvalueout_ext_15(3) <= \<const0>\;
  rx_cntvalueout_ext_15(2) <= \<const0>\;
  rx_cntvalueout_ext_15(1) <= \<const0>\;
  rx_cntvalueout_ext_15(0) <= \<const0>\;
  rx_cntvalueout_ext_16(8) <= \<const0>\;
  rx_cntvalueout_ext_16(7) <= \<const0>\;
  rx_cntvalueout_ext_16(6) <= \<const0>\;
  rx_cntvalueout_ext_16(5) <= \<const0>\;
  rx_cntvalueout_ext_16(4) <= \<const0>\;
  rx_cntvalueout_ext_16(3) <= \<const0>\;
  rx_cntvalueout_ext_16(2) <= \<const0>\;
  rx_cntvalueout_ext_16(1) <= \<const0>\;
  rx_cntvalueout_ext_16(0) <= \<const0>\;
  rx_cntvalueout_ext_17(8) <= \<const0>\;
  rx_cntvalueout_ext_17(7) <= \<const0>\;
  rx_cntvalueout_ext_17(6) <= \<const0>\;
  rx_cntvalueout_ext_17(5) <= \<const0>\;
  rx_cntvalueout_ext_17(4) <= \<const0>\;
  rx_cntvalueout_ext_17(3) <= \<const0>\;
  rx_cntvalueout_ext_17(2) <= \<const0>\;
  rx_cntvalueout_ext_17(1) <= \<const0>\;
  rx_cntvalueout_ext_17(0) <= \<const0>\;
  rx_cntvalueout_ext_18(8) <= \<const0>\;
  rx_cntvalueout_ext_18(7) <= \<const0>\;
  rx_cntvalueout_ext_18(6) <= \<const0>\;
  rx_cntvalueout_ext_18(5) <= \<const0>\;
  rx_cntvalueout_ext_18(4) <= \<const0>\;
  rx_cntvalueout_ext_18(3) <= \<const0>\;
  rx_cntvalueout_ext_18(2) <= \<const0>\;
  rx_cntvalueout_ext_18(1) <= \<const0>\;
  rx_cntvalueout_ext_18(0) <= \<const0>\;
  rx_cntvalueout_ext_19(8) <= \<const0>\;
  rx_cntvalueout_ext_19(7) <= \<const0>\;
  rx_cntvalueout_ext_19(6) <= \<const0>\;
  rx_cntvalueout_ext_19(5) <= \<const0>\;
  rx_cntvalueout_ext_19(4) <= \<const0>\;
  rx_cntvalueout_ext_19(3) <= \<const0>\;
  rx_cntvalueout_ext_19(2) <= \<const0>\;
  rx_cntvalueout_ext_19(1) <= \<const0>\;
  rx_cntvalueout_ext_19(0) <= \<const0>\;
  rx_cntvalueout_ext_2(8) <= \<const0>\;
  rx_cntvalueout_ext_2(7) <= \<const0>\;
  rx_cntvalueout_ext_2(6) <= \<const0>\;
  rx_cntvalueout_ext_2(5) <= \<const0>\;
  rx_cntvalueout_ext_2(4) <= \<const0>\;
  rx_cntvalueout_ext_2(3) <= \<const0>\;
  rx_cntvalueout_ext_2(2) <= \<const0>\;
  rx_cntvalueout_ext_2(1) <= \<const0>\;
  rx_cntvalueout_ext_2(0) <= \<const0>\;
  rx_cntvalueout_ext_20(8) <= \<const0>\;
  rx_cntvalueout_ext_20(7) <= \<const0>\;
  rx_cntvalueout_ext_20(6) <= \<const0>\;
  rx_cntvalueout_ext_20(5) <= \<const0>\;
  rx_cntvalueout_ext_20(4) <= \<const0>\;
  rx_cntvalueout_ext_20(3) <= \<const0>\;
  rx_cntvalueout_ext_20(2) <= \<const0>\;
  rx_cntvalueout_ext_20(1) <= \<const0>\;
  rx_cntvalueout_ext_20(0) <= \<const0>\;
  rx_cntvalueout_ext_21(8) <= \<const0>\;
  rx_cntvalueout_ext_21(7) <= \<const0>\;
  rx_cntvalueout_ext_21(6) <= \<const0>\;
  rx_cntvalueout_ext_21(5) <= \<const0>\;
  rx_cntvalueout_ext_21(4) <= \<const0>\;
  rx_cntvalueout_ext_21(3) <= \<const0>\;
  rx_cntvalueout_ext_21(2) <= \<const0>\;
  rx_cntvalueout_ext_21(1) <= \<const0>\;
  rx_cntvalueout_ext_21(0) <= \<const0>\;
  rx_cntvalueout_ext_22(8) <= \<const0>\;
  rx_cntvalueout_ext_22(7) <= \<const0>\;
  rx_cntvalueout_ext_22(6) <= \<const0>\;
  rx_cntvalueout_ext_22(5) <= \<const0>\;
  rx_cntvalueout_ext_22(4) <= \<const0>\;
  rx_cntvalueout_ext_22(3) <= \<const0>\;
  rx_cntvalueout_ext_22(2) <= \<const0>\;
  rx_cntvalueout_ext_22(1) <= \<const0>\;
  rx_cntvalueout_ext_22(0) <= \<const0>\;
  rx_cntvalueout_ext_23(8) <= \<const0>\;
  rx_cntvalueout_ext_23(7) <= \<const0>\;
  rx_cntvalueout_ext_23(6) <= \<const0>\;
  rx_cntvalueout_ext_23(5) <= \<const0>\;
  rx_cntvalueout_ext_23(4) <= \<const0>\;
  rx_cntvalueout_ext_23(3) <= \<const0>\;
  rx_cntvalueout_ext_23(2) <= \<const0>\;
  rx_cntvalueout_ext_23(1) <= \<const0>\;
  rx_cntvalueout_ext_23(0) <= \<const0>\;
  rx_cntvalueout_ext_24(8) <= \<const0>\;
  rx_cntvalueout_ext_24(7) <= \<const0>\;
  rx_cntvalueout_ext_24(6) <= \<const0>\;
  rx_cntvalueout_ext_24(5) <= \<const0>\;
  rx_cntvalueout_ext_24(4) <= \<const0>\;
  rx_cntvalueout_ext_24(3) <= \<const0>\;
  rx_cntvalueout_ext_24(2) <= \<const0>\;
  rx_cntvalueout_ext_24(1) <= \<const0>\;
  rx_cntvalueout_ext_24(0) <= \<const0>\;
  rx_cntvalueout_ext_25(8) <= \<const0>\;
  rx_cntvalueout_ext_25(7) <= \<const0>\;
  rx_cntvalueout_ext_25(6) <= \<const0>\;
  rx_cntvalueout_ext_25(5) <= \<const0>\;
  rx_cntvalueout_ext_25(4) <= \<const0>\;
  rx_cntvalueout_ext_25(3) <= \<const0>\;
  rx_cntvalueout_ext_25(2) <= \<const0>\;
  rx_cntvalueout_ext_25(1) <= \<const0>\;
  rx_cntvalueout_ext_25(0) <= \<const0>\;
  rx_cntvalueout_ext_26(8) <= \<const0>\;
  rx_cntvalueout_ext_26(7) <= \<const0>\;
  rx_cntvalueout_ext_26(6) <= \<const0>\;
  rx_cntvalueout_ext_26(5) <= \<const0>\;
  rx_cntvalueout_ext_26(4) <= \<const0>\;
  rx_cntvalueout_ext_26(3) <= \<const0>\;
  rx_cntvalueout_ext_26(2) <= \<const0>\;
  rx_cntvalueout_ext_26(1) <= \<const0>\;
  rx_cntvalueout_ext_26(0) <= \<const0>\;
  rx_cntvalueout_ext_27(8) <= \<const0>\;
  rx_cntvalueout_ext_27(7) <= \<const0>\;
  rx_cntvalueout_ext_27(6) <= \<const0>\;
  rx_cntvalueout_ext_27(5) <= \<const0>\;
  rx_cntvalueout_ext_27(4) <= \<const0>\;
  rx_cntvalueout_ext_27(3) <= \<const0>\;
  rx_cntvalueout_ext_27(2) <= \<const0>\;
  rx_cntvalueout_ext_27(1) <= \<const0>\;
  rx_cntvalueout_ext_27(0) <= \<const0>\;
  rx_cntvalueout_ext_28(8) <= \<const0>\;
  rx_cntvalueout_ext_28(7) <= \<const0>\;
  rx_cntvalueout_ext_28(6) <= \<const0>\;
  rx_cntvalueout_ext_28(5) <= \<const0>\;
  rx_cntvalueout_ext_28(4) <= \<const0>\;
  rx_cntvalueout_ext_28(3) <= \<const0>\;
  rx_cntvalueout_ext_28(2) <= \<const0>\;
  rx_cntvalueout_ext_28(1) <= \<const0>\;
  rx_cntvalueout_ext_28(0) <= \<const0>\;
  rx_cntvalueout_ext_29(8) <= \<const0>\;
  rx_cntvalueout_ext_29(7) <= \<const0>\;
  rx_cntvalueout_ext_29(6) <= \<const0>\;
  rx_cntvalueout_ext_29(5) <= \<const0>\;
  rx_cntvalueout_ext_29(4) <= \<const0>\;
  rx_cntvalueout_ext_29(3) <= \<const0>\;
  rx_cntvalueout_ext_29(2) <= \<const0>\;
  rx_cntvalueout_ext_29(1) <= \<const0>\;
  rx_cntvalueout_ext_29(0) <= \<const0>\;
  rx_cntvalueout_ext_3(8) <= \<const0>\;
  rx_cntvalueout_ext_3(7) <= \<const0>\;
  rx_cntvalueout_ext_3(6) <= \<const0>\;
  rx_cntvalueout_ext_3(5) <= \<const0>\;
  rx_cntvalueout_ext_3(4) <= \<const0>\;
  rx_cntvalueout_ext_3(3) <= \<const0>\;
  rx_cntvalueout_ext_3(2) <= \<const0>\;
  rx_cntvalueout_ext_3(1) <= \<const0>\;
  rx_cntvalueout_ext_3(0) <= \<const0>\;
  rx_cntvalueout_ext_30(8) <= \<const0>\;
  rx_cntvalueout_ext_30(7) <= \<const0>\;
  rx_cntvalueout_ext_30(6) <= \<const0>\;
  rx_cntvalueout_ext_30(5) <= \<const0>\;
  rx_cntvalueout_ext_30(4) <= \<const0>\;
  rx_cntvalueout_ext_30(3) <= \<const0>\;
  rx_cntvalueout_ext_30(2) <= \<const0>\;
  rx_cntvalueout_ext_30(1) <= \<const0>\;
  rx_cntvalueout_ext_30(0) <= \<const0>\;
  rx_cntvalueout_ext_31(8) <= \<const0>\;
  rx_cntvalueout_ext_31(7) <= \<const0>\;
  rx_cntvalueout_ext_31(6) <= \<const0>\;
  rx_cntvalueout_ext_31(5) <= \<const0>\;
  rx_cntvalueout_ext_31(4) <= \<const0>\;
  rx_cntvalueout_ext_31(3) <= \<const0>\;
  rx_cntvalueout_ext_31(2) <= \<const0>\;
  rx_cntvalueout_ext_31(1) <= \<const0>\;
  rx_cntvalueout_ext_31(0) <= \<const0>\;
  rx_cntvalueout_ext_32(8) <= \<const0>\;
  rx_cntvalueout_ext_32(7) <= \<const0>\;
  rx_cntvalueout_ext_32(6) <= \<const0>\;
  rx_cntvalueout_ext_32(5) <= \<const0>\;
  rx_cntvalueout_ext_32(4) <= \<const0>\;
  rx_cntvalueout_ext_32(3) <= \<const0>\;
  rx_cntvalueout_ext_32(2) <= \<const0>\;
  rx_cntvalueout_ext_32(1) <= \<const0>\;
  rx_cntvalueout_ext_32(0) <= \<const0>\;
  rx_cntvalueout_ext_33(8) <= \<const0>\;
  rx_cntvalueout_ext_33(7) <= \<const0>\;
  rx_cntvalueout_ext_33(6) <= \<const0>\;
  rx_cntvalueout_ext_33(5) <= \<const0>\;
  rx_cntvalueout_ext_33(4) <= \<const0>\;
  rx_cntvalueout_ext_33(3) <= \<const0>\;
  rx_cntvalueout_ext_33(2) <= \<const0>\;
  rx_cntvalueout_ext_33(1) <= \<const0>\;
  rx_cntvalueout_ext_33(0) <= \<const0>\;
  rx_cntvalueout_ext_34(8) <= \<const0>\;
  rx_cntvalueout_ext_34(7) <= \<const0>\;
  rx_cntvalueout_ext_34(6) <= \<const0>\;
  rx_cntvalueout_ext_34(5) <= \<const0>\;
  rx_cntvalueout_ext_34(4) <= \<const0>\;
  rx_cntvalueout_ext_34(3) <= \<const0>\;
  rx_cntvalueout_ext_34(2) <= \<const0>\;
  rx_cntvalueout_ext_34(1) <= \<const0>\;
  rx_cntvalueout_ext_34(0) <= \<const0>\;
  rx_cntvalueout_ext_35(8) <= \<const0>\;
  rx_cntvalueout_ext_35(7) <= \<const0>\;
  rx_cntvalueout_ext_35(6) <= \<const0>\;
  rx_cntvalueout_ext_35(5) <= \<const0>\;
  rx_cntvalueout_ext_35(4) <= \<const0>\;
  rx_cntvalueout_ext_35(3) <= \<const0>\;
  rx_cntvalueout_ext_35(2) <= \<const0>\;
  rx_cntvalueout_ext_35(1) <= \<const0>\;
  rx_cntvalueout_ext_35(0) <= \<const0>\;
  rx_cntvalueout_ext_36(8) <= \<const0>\;
  rx_cntvalueout_ext_36(7) <= \<const0>\;
  rx_cntvalueout_ext_36(6) <= \<const0>\;
  rx_cntvalueout_ext_36(5) <= \<const0>\;
  rx_cntvalueout_ext_36(4) <= \<const0>\;
  rx_cntvalueout_ext_36(3) <= \<const0>\;
  rx_cntvalueout_ext_36(2) <= \<const0>\;
  rx_cntvalueout_ext_36(1) <= \<const0>\;
  rx_cntvalueout_ext_36(0) <= \<const0>\;
  rx_cntvalueout_ext_37(8) <= \<const0>\;
  rx_cntvalueout_ext_37(7) <= \<const0>\;
  rx_cntvalueout_ext_37(6) <= \<const0>\;
  rx_cntvalueout_ext_37(5) <= \<const0>\;
  rx_cntvalueout_ext_37(4) <= \<const0>\;
  rx_cntvalueout_ext_37(3) <= \<const0>\;
  rx_cntvalueout_ext_37(2) <= \<const0>\;
  rx_cntvalueout_ext_37(1) <= \<const0>\;
  rx_cntvalueout_ext_37(0) <= \<const0>\;
  rx_cntvalueout_ext_38(8) <= \<const0>\;
  rx_cntvalueout_ext_38(7) <= \<const0>\;
  rx_cntvalueout_ext_38(6) <= \<const0>\;
  rx_cntvalueout_ext_38(5) <= \<const0>\;
  rx_cntvalueout_ext_38(4) <= \<const0>\;
  rx_cntvalueout_ext_38(3) <= \<const0>\;
  rx_cntvalueout_ext_38(2) <= \<const0>\;
  rx_cntvalueout_ext_38(1) <= \<const0>\;
  rx_cntvalueout_ext_38(0) <= \<const0>\;
  rx_cntvalueout_ext_39(8) <= \<const0>\;
  rx_cntvalueout_ext_39(7) <= \<const0>\;
  rx_cntvalueout_ext_39(6) <= \<const0>\;
  rx_cntvalueout_ext_39(5) <= \<const0>\;
  rx_cntvalueout_ext_39(4) <= \<const0>\;
  rx_cntvalueout_ext_39(3) <= \<const0>\;
  rx_cntvalueout_ext_39(2) <= \<const0>\;
  rx_cntvalueout_ext_39(1) <= \<const0>\;
  rx_cntvalueout_ext_39(0) <= \<const0>\;
  rx_cntvalueout_ext_4(8) <= \<const0>\;
  rx_cntvalueout_ext_4(7) <= \<const0>\;
  rx_cntvalueout_ext_4(6) <= \<const0>\;
  rx_cntvalueout_ext_4(5) <= \<const0>\;
  rx_cntvalueout_ext_4(4) <= \<const0>\;
  rx_cntvalueout_ext_4(3) <= \<const0>\;
  rx_cntvalueout_ext_4(2) <= \<const0>\;
  rx_cntvalueout_ext_4(1) <= \<const0>\;
  rx_cntvalueout_ext_4(0) <= \<const0>\;
  rx_cntvalueout_ext_40(8) <= \<const0>\;
  rx_cntvalueout_ext_40(7) <= \<const0>\;
  rx_cntvalueout_ext_40(6) <= \<const0>\;
  rx_cntvalueout_ext_40(5) <= \<const0>\;
  rx_cntvalueout_ext_40(4) <= \<const0>\;
  rx_cntvalueout_ext_40(3) <= \<const0>\;
  rx_cntvalueout_ext_40(2) <= \<const0>\;
  rx_cntvalueout_ext_40(1) <= \<const0>\;
  rx_cntvalueout_ext_40(0) <= \<const0>\;
  rx_cntvalueout_ext_41(8) <= \<const0>\;
  rx_cntvalueout_ext_41(7) <= \<const0>\;
  rx_cntvalueout_ext_41(6) <= \<const0>\;
  rx_cntvalueout_ext_41(5) <= \<const0>\;
  rx_cntvalueout_ext_41(4) <= \<const0>\;
  rx_cntvalueout_ext_41(3) <= \<const0>\;
  rx_cntvalueout_ext_41(2) <= \<const0>\;
  rx_cntvalueout_ext_41(1) <= \<const0>\;
  rx_cntvalueout_ext_41(0) <= \<const0>\;
  rx_cntvalueout_ext_42(8) <= \<const0>\;
  rx_cntvalueout_ext_42(7) <= \<const0>\;
  rx_cntvalueout_ext_42(6) <= \<const0>\;
  rx_cntvalueout_ext_42(5) <= \<const0>\;
  rx_cntvalueout_ext_42(4) <= \<const0>\;
  rx_cntvalueout_ext_42(3) <= \<const0>\;
  rx_cntvalueout_ext_42(2) <= \<const0>\;
  rx_cntvalueout_ext_42(1) <= \<const0>\;
  rx_cntvalueout_ext_42(0) <= \<const0>\;
  rx_cntvalueout_ext_43(8) <= \<const0>\;
  rx_cntvalueout_ext_43(7) <= \<const0>\;
  rx_cntvalueout_ext_43(6) <= \<const0>\;
  rx_cntvalueout_ext_43(5) <= \<const0>\;
  rx_cntvalueout_ext_43(4) <= \<const0>\;
  rx_cntvalueout_ext_43(3) <= \<const0>\;
  rx_cntvalueout_ext_43(2) <= \<const0>\;
  rx_cntvalueout_ext_43(1) <= \<const0>\;
  rx_cntvalueout_ext_43(0) <= \<const0>\;
  rx_cntvalueout_ext_44(8) <= \<const0>\;
  rx_cntvalueout_ext_44(7) <= \<const0>\;
  rx_cntvalueout_ext_44(6) <= \<const0>\;
  rx_cntvalueout_ext_44(5) <= \<const0>\;
  rx_cntvalueout_ext_44(4) <= \<const0>\;
  rx_cntvalueout_ext_44(3) <= \<const0>\;
  rx_cntvalueout_ext_44(2) <= \<const0>\;
  rx_cntvalueout_ext_44(1) <= \<const0>\;
  rx_cntvalueout_ext_44(0) <= \<const0>\;
  rx_cntvalueout_ext_45(8) <= \<const0>\;
  rx_cntvalueout_ext_45(7) <= \<const0>\;
  rx_cntvalueout_ext_45(6) <= \<const0>\;
  rx_cntvalueout_ext_45(5) <= \<const0>\;
  rx_cntvalueout_ext_45(4) <= \<const0>\;
  rx_cntvalueout_ext_45(3) <= \<const0>\;
  rx_cntvalueout_ext_45(2) <= \<const0>\;
  rx_cntvalueout_ext_45(1) <= \<const0>\;
  rx_cntvalueout_ext_45(0) <= \<const0>\;
  rx_cntvalueout_ext_46(8) <= \<const0>\;
  rx_cntvalueout_ext_46(7) <= \<const0>\;
  rx_cntvalueout_ext_46(6) <= \<const0>\;
  rx_cntvalueout_ext_46(5) <= \<const0>\;
  rx_cntvalueout_ext_46(4) <= \<const0>\;
  rx_cntvalueout_ext_46(3) <= \<const0>\;
  rx_cntvalueout_ext_46(2) <= \<const0>\;
  rx_cntvalueout_ext_46(1) <= \<const0>\;
  rx_cntvalueout_ext_46(0) <= \<const0>\;
  rx_cntvalueout_ext_47(8) <= \<const0>\;
  rx_cntvalueout_ext_47(7) <= \<const0>\;
  rx_cntvalueout_ext_47(6) <= \<const0>\;
  rx_cntvalueout_ext_47(5) <= \<const0>\;
  rx_cntvalueout_ext_47(4) <= \<const0>\;
  rx_cntvalueout_ext_47(3) <= \<const0>\;
  rx_cntvalueout_ext_47(2) <= \<const0>\;
  rx_cntvalueout_ext_47(1) <= \<const0>\;
  rx_cntvalueout_ext_47(0) <= \<const0>\;
  rx_cntvalueout_ext_48(8) <= \<const0>\;
  rx_cntvalueout_ext_48(7) <= \<const0>\;
  rx_cntvalueout_ext_48(6) <= \<const0>\;
  rx_cntvalueout_ext_48(5) <= \<const0>\;
  rx_cntvalueout_ext_48(4) <= \<const0>\;
  rx_cntvalueout_ext_48(3) <= \<const0>\;
  rx_cntvalueout_ext_48(2) <= \<const0>\;
  rx_cntvalueout_ext_48(1) <= \<const0>\;
  rx_cntvalueout_ext_48(0) <= \<const0>\;
  rx_cntvalueout_ext_49(8) <= \<const0>\;
  rx_cntvalueout_ext_49(7) <= \<const0>\;
  rx_cntvalueout_ext_49(6) <= \<const0>\;
  rx_cntvalueout_ext_49(5) <= \<const0>\;
  rx_cntvalueout_ext_49(4) <= \<const0>\;
  rx_cntvalueout_ext_49(3) <= \<const0>\;
  rx_cntvalueout_ext_49(2) <= \<const0>\;
  rx_cntvalueout_ext_49(1) <= \<const0>\;
  rx_cntvalueout_ext_49(0) <= \<const0>\;
  rx_cntvalueout_ext_5(8) <= \<const0>\;
  rx_cntvalueout_ext_5(7) <= \<const0>\;
  rx_cntvalueout_ext_5(6) <= \<const0>\;
  rx_cntvalueout_ext_5(5) <= \<const0>\;
  rx_cntvalueout_ext_5(4) <= \<const0>\;
  rx_cntvalueout_ext_5(3) <= \<const0>\;
  rx_cntvalueout_ext_5(2) <= \<const0>\;
  rx_cntvalueout_ext_5(1) <= \<const0>\;
  rx_cntvalueout_ext_5(0) <= \<const0>\;
  rx_cntvalueout_ext_50(8) <= \<const0>\;
  rx_cntvalueout_ext_50(7) <= \<const0>\;
  rx_cntvalueout_ext_50(6) <= \<const0>\;
  rx_cntvalueout_ext_50(5) <= \<const0>\;
  rx_cntvalueout_ext_50(4) <= \<const0>\;
  rx_cntvalueout_ext_50(3) <= \<const0>\;
  rx_cntvalueout_ext_50(2) <= \<const0>\;
  rx_cntvalueout_ext_50(1) <= \<const0>\;
  rx_cntvalueout_ext_50(0) <= \<const0>\;
  rx_cntvalueout_ext_51(8) <= \<const0>\;
  rx_cntvalueout_ext_51(7) <= \<const0>\;
  rx_cntvalueout_ext_51(6) <= \<const0>\;
  rx_cntvalueout_ext_51(5) <= \<const0>\;
  rx_cntvalueout_ext_51(4) <= \<const0>\;
  rx_cntvalueout_ext_51(3) <= \<const0>\;
  rx_cntvalueout_ext_51(2) <= \<const0>\;
  rx_cntvalueout_ext_51(1) <= \<const0>\;
  rx_cntvalueout_ext_51(0) <= \<const0>\;
  rx_cntvalueout_ext_6(8) <= \<const0>\;
  rx_cntvalueout_ext_6(7) <= \<const0>\;
  rx_cntvalueout_ext_6(6) <= \<const0>\;
  rx_cntvalueout_ext_6(5) <= \<const0>\;
  rx_cntvalueout_ext_6(4) <= \<const0>\;
  rx_cntvalueout_ext_6(3) <= \<const0>\;
  rx_cntvalueout_ext_6(2) <= \<const0>\;
  rx_cntvalueout_ext_6(1) <= \<const0>\;
  rx_cntvalueout_ext_6(0) <= \<const0>\;
  rx_cntvalueout_ext_7(8) <= \<const0>\;
  rx_cntvalueout_ext_7(7) <= \<const0>\;
  rx_cntvalueout_ext_7(6) <= \<const0>\;
  rx_cntvalueout_ext_7(5) <= \<const0>\;
  rx_cntvalueout_ext_7(4) <= \<const0>\;
  rx_cntvalueout_ext_7(3) <= \<const0>\;
  rx_cntvalueout_ext_7(2) <= \<const0>\;
  rx_cntvalueout_ext_7(1) <= \<const0>\;
  rx_cntvalueout_ext_7(0) <= \<const0>\;
  rx_cntvalueout_ext_8(8) <= \<const0>\;
  rx_cntvalueout_ext_8(7) <= \<const0>\;
  rx_cntvalueout_ext_8(6) <= \<const0>\;
  rx_cntvalueout_ext_8(5) <= \<const0>\;
  rx_cntvalueout_ext_8(4) <= \<const0>\;
  rx_cntvalueout_ext_8(3) <= \<const0>\;
  rx_cntvalueout_ext_8(2) <= \<const0>\;
  rx_cntvalueout_ext_8(1) <= \<const0>\;
  rx_cntvalueout_ext_8(0) <= \<const0>\;
  rx_cntvalueout_ext_9(8) <= \<const0>\;
  rx_cntvalueout_ext_9(7) <= \<const0>\;
  rx_cntvalueout_ext_9(6) <= \<const0>\;
  rx_cntvalueout_ext_9(5) <= \<const0>\;
  rx_cntvalueout_ext_9(4) <= \<const0>\;
  rx_cntvalueout_ext_9(3) <= \<const0>\;
  rx_cntvalueout_ext_9(2) <= \<const0>\;
  rx_cntvalueout_ext_9(1) <= \<const0>\;
  rx_cntvalueout_ext_9(0) <= \<const0>\;
  rxtx_bitslip_sync_done <= \<const0>\;
  shared_pll1_clkoutphy_out <= \<const0>\;
  tx_cntvalueout_0(8) <= \<const0>\;
  tx_cntvalueout_0(7) <= \<const0>\;
  tx_cntvalueout_0(6) <= \<const0>\;
  tx_cntvalueout_0(5) <= \<const0>\;
  tx_cntvalueout_0(4) <= \<const0>\;
  tx_cntvalueout_0(3) <= \<const0>\;
  tx_cntvalueout_0(2) <= \<const0>\;
  tx_cntvalueout_0(1) <= \<const0>\;
  tx_cntvalueout_0(0) <= \<const0>\;
  tx_cntvalueout_1(8) <= \<const0>\;
  tx_cntvalueout_1(7) <= \<const0>\;
  tx_cntvalueout_1(6) <= \<const0>\;
  tx_cntvalueout_1(5) <= \<const0>\;
  tx_cntvalueout_1(4) <= \<const0>\;
  tx_cntvalueout_1(3) <= \<const0>\;
  tx_cntvalueout_1(2) <= \<const0>\;
  tx_cntvalueout_1(1) <= \<const0>\;
  tx_cntvalueout_1(0) <= \<const0>\;
  tx_cntvalueout_10(8) <= \<const0>\;
  tx_cntvalueout_10(7) <= \<const0>\;
  tx_cntvalueout_10(6) <= \<const0>\;
  tx_cntvalueout_10(5) <= \<const0>\;
  tx_cntvalueout_10(4) <= \<const0>\;
  tx_cntvalueout_10(3) <= \<const0>\;
  tx_cntvalueout_10(2) <= \<const0>\;
  tx_cntvalueout_10(1) <= \<const0>\;
  tx_cntvalueout_10(0) <= \<const0>\;
  tx_cntvalueout_11(8) <= \<const0>\;
  tx_cntvalueout_11(7) <= \<const0>\;
  tx_cntvalueout_11(6) <= \<const0>\;
  tx_cntvalueout_11(5) <= \<const0>\;
  tx_cntvalueout_11(4) <= \<const0>\;
  tx_cntvalueout_11(3) <= \<const0>\;
  tx_cntvalueout_11(2) <= \<const0>\;
  tx_cntvalueout_11(1) <= \<const0>\;
  tx_cntvalueout_11(0) <= \<const0>\;
  tx_cntvalueout_12(8) <= \<const0>\;
  tx_cntvalueout_12(7) <= \<const0>\;
  tx_cntvalueout_12(6) <= \<const0>\;
  tx_cntvalueout_12(5) <= \<const0>\;
  tx_cntvalueout_12(4) <= \<const0>\;
  tx_cntvalueout_12(3) <= \<const0>\;
  tx_cntvalueout_12(2) <= \<const0>\;
  tx_cntvalueout_12(1) <= \<const0>\;
  tx_cntvalueout_12(0) <= \<const0>\;
  tx_cntvalueout_13(8) <= \<const0>\;
  tx_cntvalueout_13(7) <= \<const0>\;
  tx_cntvalueout_13(6) <= \<const0>\;
  tx_cntvalueout_13(5) <= \<const0>\;
  tx_cntvalueout_13(4) <= \<const0>\;
  tx_cntvalueout_13(3) <= \<const0>\;
  tx_cntvalueout_13(2) <= \<const0>\;
  tx_cntvalueout_13(1) <= \<const0>\;
  tx_cntvalueout_13(0) <= \<const0>\;
  tx_cntvalueout_14(8) <= \<const0>\;
  tx_cntvalueout_14(7) <= \<const0>\;
  tx_cntvalueout_14(6) <= \<const0>\;
  tx_cntvalueout_14(5) <= \<const0>\;
  tx_cntvalueout_14(4) <= \<const0>\;
  tx_cntvalueout_14(3) <= \<const0>\;
  tx_cntvalueout_14(2) <= \<const0>\;
  tx_cntvalueout_14(1) <= \<const0>\;
  tx_cntvalueout_14(0) <= \<const0>\;
  tx_cntvalueout_15(8) <= \<const0>\;
  tx_cntvalueout_15(7) <= \<const0>\;
  tx_cntvalueout_15(6) <= \<const0>\;
  tx_cntvalueout_15(5) <= \<const0>\;
  tx_cntvalueout_15(4) <= \<const0>\;
  tx_cntvalueout_15(3) <= \<const0>\;
  tx_cntvalueout_15(2) <= \<const0>\;
  tx_cntvalueout_15(1) <= \<const0>\;
  tx_cntvalueout_15(0) <= \<const0>\;
  tx_cntvalueout_16(8) <= \<const0>\;
  tx_cntvalueout_16(7) <= \<const0>\;
  tx_cntvalueout_16(6) <= \<const0>\;
  tx_cntvalueout_16(5) <= \<const0>\;
  tx_cntvalueout_16(4) <= \<const0>\;
  tx_cntvalueout_16(3) <= \<const0>\;
  tx_cntvalueout_16(2) <= \<const0>\;
  tx_cntvalueout_16(1) <= \<const0>\;
  tx_cntvalueout_16(0) <= \<const0>\;
  tx_cntvalueout_17(8) <= \<const0>\;
  tx_cntvalueout_17(7) <= \<const0>\;
  tx_cntvalueout_17(6) <= \<const0>\;
  tx_cntvalueout_17(5) <= \<const0>\;
  tx_cntvalueout_17(4) <= \<const0>\;
  tx_cntvalueout_17(3) <= \<const0>\;
  tx_cntvalueout_17(2) <= \<const0>\;
  tx_cntvalueout_17(1) <= \<const0>\;
  tx_cntvalueout_17(0) <= \<const0>\;
  tx_cntvalueout_18(8) <= \<const0>\;
  tx_cntvalueout_18(7) <= \<const0>\;
  tx_cntvalueout_18(6) <= \<const0>\;
  tx_cntvalueout_18(5) <= \<const0>\;
  tx_cntvalueout_18(4) <= \<const0>\;
  tx_cntvalueout_18(3) <= \<const0>\;
  tx_cntvalueout_18(2) <= \<const0>\;
  tx_cntvalueout_18(1) <= \<const0>\;
  tx_cntvalueout_18(0) <= \<const0>\;
  tx_cntvalueout_19(8) <= \<const0>\;
  tx_cntvalueout_19(7) <= \<const0>\;
  tx_cntvalueout_19(6) <= \<const0>\;
  tx_cntvalueout_19(5) <= \<const0>\;
  tx_cntvalueout_19(4) <= \<const0>\;
  tx_cntvalueout_19(3) <= \<const0>\;
  tx_cntvalueout_19(2) <= \<const0>\;
  tx_cntvalueout_19(1) <= \<const0>\;
  tx_cntvalueout_19(0) <= \<const0>\;
  tx_cntvalueout_2(8) <= \<const0>\;
  tx_cntvalueout_2(7) <= \<const0>\;
  tx_cntvalueout_2(6) <= \<const0>\;
  tx_cntvalueout_2(5) <= \<const0>\;
  tx_cntvalueout_2(4) <= \<const0>\;
  tx_cntvalueout_2(3) <= \<const0>\;
  tx_cntvalueout_2(2) <= \<const0>\;
  tx_cntvalueout_2(1) <= \<const0>\;
  tx_cntvalueout_2(0) <= \<const0>\;
  tx_cntvalueout_20(8) <= \<const0>\;
  tx_cntvalueout_20(7) <= \<const0>\;
  tx_cntvalueout_20(6) <= \<const0>\;
  tx_cntvalueout_20(5) <= \<const0>\;
  tx_cntvalueout_20(4) <= \<const0>\;
  tx_cntvalueout_20(3) <= \<const0>\;
  tx_cntvalueout_20(2) <= \<const0>\;
  tx_cntvalueout_20(1) <= \<const0>\;
  tx_cntvalueout_20(0) <= \<const0>\;
  tx_cntvalueout_21(8) <= \<const0>\;
  tx_cntvalueout_21(7) <= \<const0>\;
  tx_cntvalueout_21(6) <= \<const0>\;
  tx_cntvalueout_21(5) <= \<const0>\;
  tx_cntvalueout_21(4) <= \<const0>\;
  tx_cntvalueout_21(3) <= \<const0>\;
  tx_cntvalueout_21(2) <= \<const0>\;
  tx_cntvalueout_21(1) <= \<const0>\;
  tx_cntvalueout_21(0) <= \<const0>\;
  tx_cntvalueout_22(8) <= \<const0>\;
  tx_cntvalueout_22(7) <= \<const0>\;
  tx_cntvalueout_22(6) <= \<const0>\;
  tx_cntvalueout_22(5) <= \<const0>\;
  tx_cntvalueout_22(4) <= \<const0>\;
  tx_cntvalueout_22(3) <= \<const0>\;
  tx_cntvalueout_22(2) <= \<const0>\;
  tx_cntvalueout_22(1) <= \<const0>\;
  tx_cntvalueout_22(0) <= \<const0>\;
  tx_cntvalueout_23(8) <= \<const0>\;
  tx_cntvalueout_23(7) <= \<const0>\;
  tx_cntvalueout_23(6) <= \<const0>\;
  tx_cntvalueout_23(5) <= \<const0>\;
  tx_cntvalueout_23(4) <= \<const0>\;
  tx_cntvalueout_23(3) <= \<const0>\;
  tx_cntvalueout_23(2) <= \<const0>\;
  tx_cntvalueout_23(1) <= \<const0>\;
  tx_cntvalueout_23(0) <= \<const0>\;
  tx_cntvalueout_24(8) <= \<const0>\;
  tx_cntvalueout_24(7) <= \<const0>\;
  tx_cntvalueout_24(6) <= \<const0>\;
  tx_cntvalueout_24(5) <= \<const0>\;
  tx_cntvalueout_24(4) <= \<const0>\;
  tx_cntvalueout_24(3) <= \<const0>\;
  tx_cntvalueout_24(2) <= \<const0>\;
  tx_cntvalueout_24(1) <= \<const0>\;
  tx_cntvalueout_24(0) <= \<const0>\;
  tx_cntvalueout_25(8) <= \<const0>\;
  tx_cntvalueout_25(7) <= \<const0>\;
  tx_cntvalueout_25(6) <= \<const0>\;
  tx_cntvalueout_25(5) <= \<const0>\;
  tx_cntvalueout_25(4) <= \<const0>\;
  tx_cntvalueout_25(3) <= \<const0>\;
  tx_cntvalueout_25(2) <= \<const0>\;
  tx_cntvalueout_25(1) <= \<const0>\;
  tx_cntvalueout_25(0) <= \<const0>\;
  tx_cntvalueout_26(8) <= \<const0>\;
  tx_cntvalueout_26(7) <= \<const0>\;
  tx_cntvalueout_26(6) <= \<const0>\;
  tx_cntvalueout_26(5) <= \<const0>\;
  tx_cntvalueout_26(4) <= \<const0>\;
  tx_cntvalueout_26(3) <= \<const0>\;
  tx_cntvalueout_26(2) <= \<const0>\;
  tx_cntvalueout_26(1) <= \<const0>\;
  tx_cntvalueout_26(0) <= \<const0>\;
  tx_cntvalueout_27(8) <= \<const0>\;
  tx_cntvalueout_27(7) <= \<const0>\;
  tx_cntvalueout_27(6) <= \<const0>\;
  tx_cntvalueout_27(5) <= \<const0>\;
  tx_cntvalueout_27(4) <= \<const0>\;
  tx_cntvalueout_27(3) <= \<const0>\;
  tx_cntvalueout_27(2) <= \<const0>\;
  tx_cntvalueout_27(1) <= \<const0>\;
  tx_cntvalueout_27(0) <= \<const0>\;
  tx_cntvalueout_28(8) <= \<const0>\;
  tx_cntvalueout_28(7) <= \<const0>\;
  tx_cntvalueout_28(6) <= \<const0>\;
  tx_cntvalueout_28(5) <= \<const0>\;
  tx_cntvalueout_28(4) <= \<const0>\;
  tx_cntvalueout_28(3) <= \<const0>\;
  tx_cntvalueout_28(2) <= \<const0>\;
  tx_cntvalueout_28(1) <= \<const0>\;
  tx_cntvalueout_28(0) <= \<const0>\;
  tx_cntvalueout_29(8) <= \<const0>\;
  tx_cntvalueout_29(7) <= \<const0>\;
  tx_cntvalueout_29(6) <= \<const0>\;
  tx_cntvalueout_29(5) <= \<const0>\;
  tx_cntvalueout_29(4) <= \<const0>\;
  tx_cntvalueout_29(3) <= \<const0>\;
  tx_cntvalueout_29(2) <= \<const0>\;
  tx_cntvalueout_29(1) <= \<const0>\;
  tx_cntvalueout_29(0) <= \<const0>\;
  tx_cntvalueout_3(8) <= \<const0>\;
  tx_cntvalueout_3(7) <= \<const0>\;
  tx_cntvalueout_3(6) <= \<const0>\;
  tx_cntvalueout_3(5) <= \<const0>\;
  tx_cntvalueout_3(4) <= \<const0>\;
  tx_cntvalueout_3(3) <= \<const0>\;
  tx_cntvalueout_3(2) <= \<const0>\;
  tx_cntvalueout_3(1) <= \<const0>\;
  tx_cntvalueout_3(0) <= \<const0>\;
  tx_cntvalueout_30(8) <= \<const0>\;
  tx_cntvalueout_30(7) <= \<const0>\;
  tx_cntvalueout_30(6) <= \<const0>\;
  tx_cntvalueout_30(5) <= \<const0>\;
  tx_cntvalueout_30(4) <= \<const0>\;
  tx_cntvalueout_30(3) <= \<const0>\;
  tx_cntvalueout_30(2) <= \<const0>\;
  tx_cntvalueout_30(1) <= \<const0>\;
  tx_cntvalueout_30(0) <= \<const0>\;
  tx_cntvalueout_31(8) <= \<const0>\;
  tx_cntvalueout_31(7) <= \<const0>\;
  tx_cntvalueout_31(6) <= \<const0>\;
  tx_cntvalueout_31(5) <= \<const0>\;
  tx_cntvalueout_31(4) <= \<const0>\;
  tx_cntvalueout_31(3) <= \<const0>\;
  tx_cntvalueout_31(2) <= \<const0>\;
  tx_cntvalueout_31(1) <= \<const0>\;
  tx_cntvalueout_31(0) <= \<const0>\;
  tx_cntvalueout_32(8) <= \<const0>\;
  tx_cntvalueout_32(7) <= \<const0>\;
  tx_cntvalueout_32(6) <= \<const0>\;
  tx_cntvalueout_32(5) <= \<const0>\;
  tx_cntvalueout_32(4) <= \<const0>\;
  tx_cntvalueout_32(3) <= \<const0>\;
  tx_cntvalueout_32(2) <= \<const0>\;
  tx_cntvalueout_32(1) <= \<const0>\;
  tx_cntvalueout_32(0) <= \<const0>\;
  tx_cntvalueout_33(8) <= \<const0>\;
  tx_cntvalueout_33(7) <= \<const0>\;
  tx_cntvalueout_33(6) <= \<const0>\;
  tx_cntvalueout_33(5) <= \<const0>\;
  tx_cntvalueout_33(4) <= \<const0>\;
  tx_cntvalueout_33(3) <= \<const0>\;
  tx_cntvalueout_33(2) <= \<const0>\;
  tx_cntvalueout_33(1) <= \<const0>\;
  tx_cntvalueout_33(0) <= \<const0>\;
  tx_cntvalueout_34(8) <= \<const0>\;
  tx_cntvalueout_34(7) <= \<const0>\;
  tx_cntvalueout_34(6) <= \<const0>\;
  tx_cntvalueout_34(5) <= \<const0>\;
  tx_cntvalueout_34(4) <= \<const0>\;
  tx_cntvalueout_34(3) <= \<const0>\;
  tx_cntvalueout_34(2) <= \<const0>\;
  tx_cntvalueout_34(1) <= \<const0>\;
  tx_cntvalueout_34(0) <= \<const0>\;
  tx_cntvalueout_35(8) <= \<const0>\;
  tx_cntvalueout_35(7) <= \<const0>\;
  tx_cntvalueout_35(6) <= \<const0>\;
  tx_cntvalueout_35(5) <= \<const0>\;
  tx_cntvalueout_35(4) <= \<const0>\;
  tx_cntvalueout_35(3) <= \<const0>\;
  tx_cntvalueout_35(2) <= \<const0>\;
  tx_cntvalueout_35(1) <= \<const0>\;
  tx_cntvalueout_35(0) <= \<const0>\;
  tx_cntvalueout_36(8) <= \<const0>\;
  tx_cntvalueout_36(7) <= \<const0>\;
  tx_cntvalueout_36(6) <= \<const0>\;
  tx_cntvalueout_36(5) <= \<const0>\;
  tx_cntvalueout_36(4) <= \<const0>\;
  tx_cntvalueout_36(3) <= \<const0>\;
  tx_cntvalueout_36(2) <= \<const0>\;
  tx_cntvalueout_36(1) <= \<const0>\;
  tx_cntvalueout_36(0) <= \<const0>\;
  tx_cntvalueout_37(8) <= \<const0>\;
  tx_cntvalueout_37(7) <= \<const0>\;
  tx_cntvalueout_37(6) <= \<const0>\;
  tx_cntvalueout_37(5) <= \<const0>\;
  tx_cntvalueout_37(4) <= \<const0>\;
  tx_cntvalueout_37(3) <= \<const0>\;
  tx_cntvalueout_37(2) <= \<const0>\;
  tx_cntvalueout_37(1) <= \<const0>\;
  tx_cntvalueout_37(0) <= \<const0>\;
  tx_cntvalueout_38(8) <= \<const0>\;
  tx_cntvalueout_38(7) <= \<const0>\;
  tx_cntvalueout_38(6) <= \<const0>\;
  tx_cntvalueout_38(5) <= \<const0>\;
  tx_cntvalueout_38(4) <= \<const0>\;
  tx_cntvalueout_38(3) <= \<const0>\;
  tx_cntvalueout_38(2) <= \<const0>\;
  tx_cntvalueout_38(1) <= \<const0>\;
  tx_cntvalueout_38(0) <= \<const0>\;
  tx_cntvalueout_39(8) <= \<const0>\;
  tx_cntvalueout_39(7) <= \<const0>\;
  tx_cntvalueout_39(6) <= \<const0>\;
  tx_cntvalueout_39(5) <= \<const0>\;
  tx_cntvalueout_39(4) <= \<const0>\;
  tx_cntvalueout_39(3) <= \<const0>\;
  tx_cntvalueout_39(2) <= \<const0>\;
  tx_cntvalueout_39(1) <= \<const0>\;
  tx_cntvalueout_39(0) <= \<const0>\;
  tx_cntvalueout_4(8) <= \<const0>\;
  tx_cntvalueout_4(7) <= \<const0>\;
  tx_cntvalueout_4(6) <= \<const0>\;
  tx_cntvalueout_4(5) <= \<const0>\;
  tx_cntvalueout_4(4) <= \<const0>\;
  tx_cntvalueout_4(3) <= \<const0>\;
  tx_cntvalueout_4(2) <= \<const0>\;
  tx_cntvalueout_4(1) <= \<const0>\;
  tx_cntvalueout_4(0) <= \<const0>\;
  tx_cntvalueout_40(8) <= \<const0>\;
  tx_cntvalueout_40(7) <= \<const0>\;
  tx_cntvalueout_40(6) <= \<const0>\;
  tx_cntvalueout_40(5) <= \<const0>\;
  tx_cntvalueout_40(4) <= \<const0>\;
  tx_cntvalueout_40(3) <= \<const0>\;
  tx_cntvalueout_40(2) <= \<const0>\;
  tx_cntvalueout_40(1) <= \<const0>\;
  tx_cntvalueout_40(0) <= \<const0>\;
  tx_cntvalueout_41(8) <= \<const0>\;
  tx_cntvalueout_41(7) <= \<const0>\;
  tx_cntvalueout_41(6) <= \<const0>\;
  tx_cntvalueout_41(5) <= \<const0>\;
  tx_cntvalueout_41(4) <= \<const0>\;
  tx_cntvalueout_41(3) <= \<const0>\;
  tx_cntvalueout_41(2) <= \<const0>\;
  tx_cntvalueout_41(1) <= \<const0>\;
  tx_cntvalueout_41(0) <= \<const0>\;
  tx_cntvalueout_42(8) <= \<const0>\;
  tx_cntvalueout_42(7) <= \<const0>\;
  tx_cntvalueout_42(6) <= \<const0>\;
  tx_cntvalueout_42(5) <= \<const0>\;
  tx_cntvalueout_42(4) <= \<const0>\;
  tx_cntvalueout_42(3) <= \<const0>\;
  tx_cntvalueout_42(2) <= \<const0>\;
  tx_cntvalueout_42(1) <= \<const0>\;
  tx_cntvalueout_42(0) <= \<const0>\;
  tx_cntvalueout_43(8) <= \<const0>\;
  tx_cntvalueout_43(7) <= \<const0>\;
  tx_cntvalueout_43(6) <= \<const0>\;
  tx_cntvalueout_43(5) <= \<const0>\;
  tx_cntvalueout_43(4) <= \<const0>\;
  tx_cntvalueout_43(3) <= \<const0>\;
  tx_cntvalueout_43(2) <= \<const0>\;
  tx_cntvalueout_43(1) <= \<const0>\;
  tx_cntvalueout_43(0) <= \<const0>\;
  tx_cntvalueout_44(8) <= \<const0>\;
  tx_cntvalueout_44(7) <= \<const0>\;
  tx_cntvalueout_44(6) <= \<const0>\;
  tx_cntvalueout_44(5) <= \<const0>\;
  tx_cntvalueout_44(4) <= \<const0>\;
  tx_cntvalueout_44(3) <= \<const0>\;
  tx_cntvalueout_44(2) <= \<const0>\;
  tx_cntvalueout_44(1) <= \<const0>\;
  tx_cntvalueout_44(0) <= \<const0>\;
  tx_cntvalueout_45(8) <= \<const0>\;
  tx_cntvalueout_45(7) <= \<const0>\;
  tx_cntvalueout_45(6) <= \<const0>\;
  tx_cntvalueout_45(5) <= \<const0>\;
  tx_cntvalueout_45(4) <= \<const0>\;
  tx_cntvalueout_45(3) <= \<const0>\;
  tx_cntvalueout_45(2) <= \<const0>\;
  tx_cntvalueout_45(1) <= \<const0>\;
  tx_cntvalueout_45(0) <= \<const0>\;
  tx_cntvalueout_46(8) <= \<const0>\;
  tx_cntvalueout_46(7) <= \<const0>\;
  tx_cntvalueout_46(6) <= \<const0>\;
  tx_cntvalueout_46(5) <= \<const0>\;
  tx_cntvalueout_46(4) <= \<const0>\;
  tx_cntvalueout_46(3) <= \<const0>\;
  tx_cntvalueout_46(2) <= \<const0>\;
  tx_cntvalueout_46(1) <= \<const0>\;
  tx_cntvalueout_46(0) <= \<const0>\;
  tx_cntvalueout_47(8) <= \<const0>\;
  tx_cntvalueout_47(7) <= \<const0>\;
  tx_cntvalueout_47(6) <= \<const0>\;
  tx_cntvalueout_47(5) <= \<const0>\;
  tx_cntvalueout_47(4) <= \<const0>\;
  tx_cntvalueout_47(3) <= \<const0>\;
  tx_cntvalueout_47(2) <= \<const0>\;
  tx_cntvalueout_47(1) <= \<const0>\;
  tx_cntvalueout_47(0) <= \<const0>\;
  tx_cntvalueout_48(8) <= \<const0>\;
  tx_cntvalueout_48(7) <= \<const0>\;
  tx_cntvalueout_48(6) <= \<const0>\;
  tx_cntvalueout_48(5) <= \<const0>\;
  tx_cntvalueout_48(4) <= \<const0>\;
  tx_cntvalueout_48(3) <= \<const0>\;
  tx_cntvalueout_48(2) <= \<const0>\;
  tx_cntvalueout_48(1) <= \<const0>\;
  tx_cntvalueout_48(0) <= \<const0>\;
  tx_cntvalueout_49(8) <= \<const0>\;
  tx_cntvalueout_49(7) <= \<const0>\;
  tx_cntvalueout_49(6) <= \<const0>\;
  tx_cntvalueout_49(5) <= \<const0>\;
  tx_cntvalueout_49(4) <= \<const0>\;
  tx_cntvalueout_49(3) <= \<const0>\;
  tx_cntvalueout_49(2) <= \<const0>\;
  tx_cntvalueout_49(1) <= \<const0>\;
  tx_cntvalueout_49(0) <= \<const0>\;
  tx_cntvalueout_5(8) <= \<const0>\;
  tx_cntvalueout_5(7) <= \<const0>\;
  tx_cntvalueout_5(6) <= \<const0>\;
  tx_cntvalueout_5(5) <= \<const0>\;
  tx_cntvalueout_5(4) <= \<const0>\;
  tx_cntvalueout_5(3) <= \<const0>\;
  tx_cntvalueout_5(2) <= \<const0>\;
  tx_cntvalueout_5(1) <= \<const0>\;
  tx_cntvalueout_5(0) <= \<const0>\;
  tx_cntvalueout_50(8) <= \<const0>\;
  tx_cntvalueout_50(7) <= \<const0>\;
  tx_cntvalueout_50(6) <= \<const0>\;
  tx_cntvalueout_50(5) <= \<const0>\;
  tx_cntvalueout_50(4) <= \<const0>\;
  tx_cntvalueout_50(3) <= \<const0>\;
  tx_cntvalueout_50(2) <= \<const0>\;
  tx_cntvalueout_50(1) <= \<const0>\;
  tx_cntvalueout_50(0) <= \<const0>\;
  tx_cntvalueout_51(8) <= \<const0>\;
  tx_cntvalueout_51(7) <= \<const0>\;
  tx_cntvalueout_51(6) <= \<const0>\;
  tx_cntvalueout_51(5) <= \<const0>\;
  tx_cntvalueout_51(4) <= \<const0>\;
  tx_cntvalueout_51(3) <= \<const0>\;
  tx_cntvalueout_51(2) <= \<const0>\;
  tx_cntvalueout_51(1) <= \<const0>\;
  tx_cntvalueout_51(0) <= \<const0>\;
  tx_cntvalueout_6(8) <= \<const0>\;
  tx_cntvalueout_6(7) <= \<const0>\;
  tx_cntvalueout_6(6) <= \<const0>\;
  tx_cntvalueout_6(5) <= \<const0>\;
  tx_cntvalueout_6(4) <= \<const0>\;
  tx_cntvalueout_6(3) <= \<const0>\;
  tx_cntvalueout_6(2) <= \<const0>\;
  tx_cntvalueout_6(1) <= \<const0>\;
  tx_cntvalueout_6(0) <= \<const0>\;
  tx_cntvalueout_7(8) <= \<const0>\;
  tx_cntvalueout_7(7) <= \<const0>\;
  tx_cntvalueout_7(6) <= \<const0>\;
  tx_cntvalueout_7(5) <= \<const0>\;
  tx_cntvalueout_7(4) <= \<const0>\;
  tx_cntvalueout_7(3) <= \<const0>\;
  tx_cntvalueout_7(2) <= \<const0>\;
  tx_cntvalueout_7(1) <= \<const0>\;
  tx_cntvalueout_7(0) <= \<const0>\;
  tx_cntvalueout_8(8) <= \<const0>\;
  tx_cntvalueout_8(7) <= \<const0>\;
  tx_cntvalueout_8(6) <= \<const0>\;
  tx_cntvalueout_8(5) <= \<const0>\;
  tx_cntvalueout_8(4) <= \<const0>\;
  tx_cntvalueout_8(3) <= \<const0>\;
  tx_cntvalueout_8(2) <= \<const0>\;
  tx_cntvalueout_8(1) <= \<const0>\;
  tx_cntvalueout_8(0) <= \<const0>\;
  tx_cntvalueout_9(8) <= \<const0>\;
  tx_cntvalueout_9(7) <= \<const0>\;
  tx_cntvalueout_9(6) <= \<const0>\;
  tx_cntvalueout_9(5) <= \<const0>\;
  tx_cntvalueout_9(4) <= \<const0>\;
  tx_cntvalueout_9(3) <= \<const0>\;
  tx_cntvalueout_9(2) <= \<const0>\;
  tx_cntvalueout_9(1) <= \<const0>\;
  tx_cntvalueout_9(0) <= \<const0>\;
  vtc_rdy_bsc6 <= \<const0>\;
  vtc_rdy_bsc7 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
top_inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_hssio_wiz_top
     port map (
      \GEN_RIU_FROM_PLL.rst_seq_done_reg\ => rst_seq_done,
      TH_out_N => TH_out_N,
      TH_out_P => TH_out_P,
      ch1_out7_N => ch1_out7_N,
      ch1_out7_P => ch1_out7_P,
      ch2_out0_N => ch2_out0_N,
      ch2_out0_P => ch2_out0_P,
      ch2_out2_N => ch2_out2_N,
      ch2_out2_P => ch2_out2_P,
      ch2_out3_N => ch2_out3_N,
      ch2_out3_P => ch2_out3_P,
      ch2_out5_N => ch2_out5_N,
      ch2_out5_P => ch2_out5_P,
      ch2_out6_N => ch2_out6_N,
      ch2_out6_P => ch2_out6_P,
      ch2_out7_N => ch2_out7_N,
      ch2_out7_P => ch2_out7_P,
      ch3_out0_N => ch3_out0_N,
      ch3_out0_P => ch3_out0_P,
      ch3_out1_N => ch3_out1_N,
      ch3_out1_P => ch3_out1_P,
      ch3_out2_N => ch3_out2_N,
      ch3_out2_P => ch3_out2_P,
      ch3_out3_N => ch3_out3_N,
      ch3_out3_P => ch3_out3_P,
      ch3_out4_N => ch3_out4_N,
      ch3_out4_P => ch3_out4_P,
      ch3_out5_N => ch3_out5_N,
      ch3_out5_P => ch3_out5_P,
      ch3_out6_N => ch3_out6_N,
      ch3_out6_P => ch3_out6_P,
      ch3_out7_N => ch3_out7_N,
      ch3_out7_P => ch3_out7_P,
      clk => clk,
      data_from_fabric_TH_out_P(7 downto 0) => data_from_fabric_TH_out_P(7 downto 0),
      data_from_fabric_ch1_out7_P(7 downto 0) => data_from_fabric_ch1_out7_P(7 downto 0),
      data_from_fabric_ch2_out0_P(7 downto 0) => data_from_fabric_ch2_out0_P(7 downto 0),
      data_from_fabric_ch2_out2_P(7 downto 0) => data_from_fabric_ch2_out2_P(7 downto 0),
      data_from_fabric_ch2_out3_P(7 downto 0) => data_from_fabric_ch2_out3_P(7 downto 0),
      data_from_fabric_ch2_out5_P(7 downto 0) => data_from_fabric_ch2_out5_P(7 downto 0),
      data_from_fabric_ch2_out6_P(7 downto 0) => data_from_fabric_ch2_out6_P(7 downto 0),
      data_from_fabric_ch2_out7_P(7 downto 0) => data_from_fabric_ch2_out7_P(7 downto 0),
      data_from_fabric_ch3_out0_P(7 downto 0) => data_from_fabric_ch3_out0_P(7 downto 0),
      data_from_fabric_ch3_out1_P(7 downto 0) => data_from_fabric_ch3_out1_P(7 downto 0),
      data_from_fabric_ch3_out2_P(7 downto 0) => data_from_fabric_ch3_out2_P(7 downto 0),
      data_from_fabric_ch3_out3_P(7 downto 0) => data_from_fabric_ch3_out3_P(7 downto 0),
      data_from_fabric_ch3_out4_P(7 downto 0) => data_from_fabric_ch3_out4_P(7 downto 0),
      data_from_fabric_ch3_out5_P(7 downto 0) => data_from_fabric_ch3_out5_P(7 downto 0),
      data_from_fabric_ch3_out6_P(7 downto 0) => data_from_fabric_ch3_out6_P(7 downto 0),
      data_from_fabric_ch3_out7_P(7 downto 0) => data_from_fabric_ch3_out7_P(7 downto 0),
      dly_rdy_bsc0 => dly_rdy_bsc0,
      dly_rdy_bsc1 => dly_rdy_bsc1,
      dly_rdy_bsc2 => dly_rdy_bsc2,
      dly_rdy_bsc3 => dly_rdy_bsc3,
      dly_rdy_bsc4 => dly_rdy_bsc4,
      dly_rdy_bsc5 => dly_rdy_bsc5,
      en_vtc_bsc0 => en_vtc_bsc0,
      en_vtc_bsc1 => en_vtc_bsc1,
      en_vtc_bsc2 => en_vtc_bsc2,
      en_vtc_bsc3 => en_vtc_bsc3,
      en_vtc_bsc4 => en_vtc_bsc4,
      en_vtc_bsc5 => en_vtc_bsc5,
      multi_intf_lock_in => multi_intf_lock_in,
      pll0_clkout0_out => pll0_clkout0,
      pll0_clkout1_out => pll0_clkout1,
      pll0_locked_out => pll0_locked,
      rst => rst,
      shared_pll0_clkoutphy_out => shared_pll0_clkoutphy_out,
      tri_tbyte6(3 downto 0) => tri_tbyte6(3 downto 0),
      tri_tbyte7(3 downto 0) => tri_tbyte7(3 downto 0),
      vtc_rdy_bsc0 => vtc_rdy_bsc0,
      vtc_rdy_bsc1 => vtc_rdy_bsc1,
      vtc_rdy_bsc2 => vtc_rdy_bsc2,
      vtc_rdy_bsc3 => vtc_rdy_bsc3,
      vtc_rdy_bsc4 => vtc_rdy_bsc4,
      vtc_rdy_bsc5 => vtc_rdy_bsc5
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity high_speed_selectio_wiz_Bank_66 is
  port (
    vtc_rdy_bsc0 : out STD_LOGIC;
    en_vtc_bsc0 : in STD_LOGIC;
    vtc_rdy_bsc1 : out STD_LOGIC;
    en_vtc_bsc1 : in STD_LOGIC;
    vtc_rdy_bsc2 : out STD_LOGIC;
    en_vtc_bsc2 : in STD_LOGIC;
    vtc_rdy_bsc3 : out STD_LOGIC;
    en_vtc_bsc3 : in STD_LOGIC;
    vtc_rdy_bsc4 : out STD_LOGIC;
    en_vtc_bsc4 : in STD_LOGIC;
    vtc_rdy_bsc5 : out STD_LOGIC;
    en_vtc_bsc5 : in STD_LOGIC;
    dly_rdy_bsc0 : out STD_LOGIC;
    dly_rdy_bsc1 : out STD_LOGIC;
    dly_rdy_bsc2 : out STD_LOGIC;
    dly_rdy_bsc3 : out STD_LOGIC;
    dly_rdy_bsc4 : out STD_LOGIC;
    dly_rdy_bsc5 : out STD_LOGIC;
    rst_seq_done : out STD_LOGIC;
    shared_pll0_clkoutphy_out : out STD_LOGIC;
    pll0_clkout0 : out STD_LOGIC;
    pll0_clkout1 : out STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC;
    pll0_locked : out STD_LOGIC;
    ch2_out3_P : out STD_LOGIC;
    data_from_fabric_ch2_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out3_N : out STD_LOGIC;
    ch2_out0_P : out STD_LOGIC;
    data_from_fabric_ch2_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out0_N : out STD_LOGIC;
    ch3_out1_P : out STD_LOGIC;
    data_from_fabric_ch3_out1_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out1_N : out STD_LOGIC;
    ch1_out7_P : out STD_LOGIC;
    data_from_fabric_ch1_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch1_out7_N : out STD_LOGIC;
    ch3_out2_P : out STD_LOGIC;
    data_from_fabric_ch3_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out2_N : out STD_LOGIC;
    ch2_out6_P : out STD_LOGIC;
    data_from_fabric_ch2_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out6_N : out STD_LOGIC;
    ch3_out0_P : out STD_LOGIC;
    data_from_fabric_ch3_out0_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out0_N : out STD_LOGIC;
    ch2_out7_P : out STD_LOGIC;
    data_from_fabric_ch2_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out7_N : out STD_LOGIC;
    ch2_out5_P : out STD_LOGIC;
    data_from_fabric_ch2_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out5_N : out STD_LOGIC;
    ch2_out2_P : out STD_LOGIC;
    data_from_fabric_ch2_out2_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch2_out2_N : out STD_LOGIC;
    ch3_out4_P : out STD_LOGIC;
    data_from_fabric_ch3_out4_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out4_N : out STD_LOGIC;
    ch3_out3_P : out STD_LOGIC;
    data_from_fabric_ch3_out3_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out3_N : out STD_LOGIC;
    ch3_out5_P : out STD_LOGIC;
    data_from_fabric_ch3_out5_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out5_N : out STD_LOGIC;
    ch3_out7_P : out STD_LOGIC;
    data_from_fabric_ch3_out7_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out7_N : out STD_LOGIC;
    ch3_out6_P : out STD_LOGIC;
    data_from_fabric_ch3_out6_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ch3_out6_N : out STD_LOGIC;
    TH_out_P : out STD_LOGIC;
    data_from_fabric_TH_out_P : in STD_LOGIC_VECTOR ( 7 downto 0 );
    TH_out_N : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of high_speed_selectio_wiz_Bank_66 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of high_speed_selectio_wiz_Bank_66 : entity is "high_speed_selectio_wiz_Bank_66,high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of high_speed_selectio_wiz_Bank_66 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of high_speed_selectio_wiz_Bank_66 : entity is "high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9,Vivado 2024.1.1";
end high_speed_selectio_wiz_Bank_66;

architecture STRUCTURE of high_speed_selectio_wiz_Bank_66 is
  signal NLW_inst_bitslip_error_0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_1_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_10_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_11_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_12_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_13_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_14_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_15_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_16_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_17_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_18_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_19_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_2_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_20_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_21_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_22_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_23_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_24_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_25_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_26_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_27_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_28_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_29_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_3_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_30_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_31_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_32_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_33_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_34_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_35_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_36_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_37_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_38_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_39_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_4_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_40_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_41_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_42_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_43_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_44_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_45_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_46_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_47_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_48_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_49_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_5_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_50_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_51_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_7_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_8_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bitslip_error_9_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_clk_from_ibuf_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_dly_rdy_bsc6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_dly_rdy_bsc7_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_drdy_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_1_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_10_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_11_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_12_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_13_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_14_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_15_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_16_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_17_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_18_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_19_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_2_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_20_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_21_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_22_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_23_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_24_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_25_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_26_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_27_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_28_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_29_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_3_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_30_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_31_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_32_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_33_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_34_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_35_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_36_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_37_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_38_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_39_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_4_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_40_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_41_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_42_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_43_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_44_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_45_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_46_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_47_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_48_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_49_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_5_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_50_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_51_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_7_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_8_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_empty_9_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_rd_data_valid_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_13_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_19_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_26_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_32_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_39_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_45_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_fifo_wr_clk_6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_intf_rdy_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_pll1_clkout0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_pll1_locked_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg0_bs0_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg0_bs1_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg1_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg1_bs2_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg1_bs3_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg2_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg2_bs4_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg2_bs5_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg3_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg3_bs6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_riu_valid_bg3_bs7_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_rx_bitslip_sync_done_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_rxtx_bitslip_sync_done_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_shared_pll1_clkoutphy_out_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_vtc_rdy_bsc6_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_vtc_rdy_bsc7_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout0_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout1_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout2_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout3_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout4_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout5_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout6_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_bidir_tx_bs_tri_cntvalueout7_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_do_out_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_lp_rx_o_n_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_lp_rx_o_p_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg0_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg0_bs0_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg0_bs1_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg1_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg1_bs2_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg1_bs3_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg2_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg2_bs4_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg2_bs5_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg3_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg3_bs6_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_riu_rd_data_bg3_bs7_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_inst_rx_cntvalueout_0_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_1_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_10_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_11_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_12_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_13_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_14_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_15_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_16_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_17_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_18_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_19_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_2_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_20_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_21_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_22_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_23_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_24_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_25_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_26_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_27_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_28_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_29_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_3_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_30_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_31_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_32_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_33_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_34_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_35_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_36_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_37_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_38_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_39_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_4_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_40_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_41_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_42_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_43_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_44_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_45_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_46_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_47_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_48_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_49_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_5_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_50_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_51_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_6_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_7_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_8_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_9_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_0_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_1_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_10_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_11_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_12_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_13_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_14_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_15_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_16_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_17_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_18_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_19_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_2_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_20_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_21_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_22_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_23_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_24_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_25_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_26_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_27_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_28_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_29_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_3_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_30_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_31_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_32_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_33_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_34_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_35_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_36_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_37_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_38_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_39_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_4_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_40_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_41_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_42_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_43_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_44_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_45_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_46_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_47_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_48_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_49_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_5_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_50_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_51_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_6_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_7_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_8_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_rx_cntvalueout_ext_9_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_0_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_1_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_10_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_11_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_12_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_13_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_14_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_15_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_16_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_17_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_18_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_19_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_2_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_20_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_21_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_22_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_23_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_24_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_25_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_26_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_27_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_28_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_29_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_3_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_30_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_31_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_32_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_33_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_34_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_35_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_36_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_37_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_38_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_39_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_4_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_40_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_41_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_42_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_43_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_44_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_45_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_46_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_47_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_48_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_49_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_5_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_50_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_51_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_6_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_7_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_8_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_inst_tx_cntvalueout_9_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute C_ALL_EN_PIN_INFO : string;
  attribute C_ALL_EN_PIN_INFO of inst : label is "0 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_P loc D14} 1 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_N loc C14} 2 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_P loc B15} 3 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_N loc A15} 4 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_P loc D13} 5 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_N loc C13} 6 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_P loc B14} 7 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_N loc A14} 8 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_P loc C12} 9 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_N loc B12} 10 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_P loc A13} 11 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_N loc A12} 13 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_P loc H14} 14 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_N loc G14} 15 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_P loc G15} 16 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_N loc F15} 17 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_P loc J13} 18 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_N loc H13} 19 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_P loc F14} 20 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_N loc F13} 21 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_P loc G12} 22 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_N loc F12} 23 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_P loc E13} 24 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_N loc E12} 26 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_P loc E10} 27 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_N loc D10} 28 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_P loc E11} 29 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_N loc D11} 30 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_P loc C9} 31 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_N loc C8} 32 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_P loc C11} 33 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_N loc B11}";
  attribute C_ALL_RX_EN : string;
  attribute C_ALL_RX_EN of inst : label is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_BANK : integer;
  attribute C_BANK of inst : label is 66;
  attribute C_BIDIR_BITSLICE_EN : string;
  attribute C_BIDIR_BITSLICE_EN of inst : label is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_BIDIR_FIFO_SYNC_MODE : string;
  attribute C_BIDIR_FIFO_SYNC_MODE of inst : label is "FALSE";
  attribute C_BIDIR_IS_RX_CLK_INVERTED : string;
  attribute C_BIDIR_IS_RX_CLK_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_IS_RX_RST_DLY_INVERTED : string;
  attribute C_BIDIR_IS_RX_RST_DLY_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_IS_RX_RST_INVERTED : string;
  attribute C_BIDIR_IS_RX_RST_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_IS_TX_CLK_INVERTED : string;
  attribute C_BIDIR_IS_TX_CLK_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_IS_TX_RST_DLY_INVERTED : string;
  attribute C_BIDIR_IS_TX_RST_DLY_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_IS_TX_RST_INVERTED : string;
  attribute C_BIDIR_IS_TX_RST_INVERTED of inst : label is "1'b0";
  attribute C_BIDIR_RX_DELAY_FORMAT : string;
  attribute C_BIDIR_RX_DELAY_FORMAT of inst : label is "TIME";
  attribute C_BIDIR_TX_DELAY_FORMAT : string;
  attribute C_BIDIR_TX_DELAY_FORMAT of inst : label is "TIME";
  attribute C_BITSLIP_MODE : string;
  attribute C_BITSLIP_MODE of inst : label is "SLIP_PER_BIT";
  attribute C_BITSLIP_VAL : string;
  attribute C_BITSLIP_VAL of inst : label is "8'b00101100";
  attribute C_BS0_INFO : string;
  attribute C_BS0_INFO of inst : label is "0 {name bg0_pin0_nc loc D14} 1 {name bg0_pin6_nc loc B14} 2 {name bg1_pin0_nc loc H14} 3 {name bg1_pin6_nc loc F14} 4 {name bg2_pin0_nc loc E10} 5 {name bg2_pin6_nc loc C11} 6 {name bg3_pin0_nc loc H9} 7 {name bg3_pin6_nc loc G11}";
  attribute C_BSC_CTRL_CLK : string;
  attribute C_BSC_CTRL_CLK of inst : label is "EXTERNAL";
  attribute C_BSC_EN_DYN_ODLY_MODE : string;
  attribute C_BSC_EN_DYN_ODLY_MODE of inst : label is "FALSE";
  attribute C_BSC_IDLY_VT_TRACK : string;
  attribute C_BSC_IDLY_VT_TRACK of inst : label is "TRUE";
  attribute C_BSC_ODLY_VT_TRACK : string;
  attribute C_BSC_ODLY_VT_TRACK of inst : label is "TRUE";
  attribute C_BSC_QDLY_VT_TRACK : string;
  attribute C_BSC_QDLY_VT_TRACK of inst : label is "TRUE";
  attribute C_BSC_READ_IDLE_COUNT : string;
  attribute C_BSC_READ_IDLE_COUNT of inst : label is "6'b000000";
  attribute C_BSC_REFCLK_SRC : string;
  attribute C_BSC_REFCLK_SRC of inst : label is "PLLCLK";
  attribute C_BSC_ROUNDING_FACTOR : integer;
  attribute C_BSC_ROUNDING_FACTOR of inst : label is 16;
  attribute C_BSC_RXGATE_EXTEND : string;
  attribute C_BSC_RXGATE_EXTEND of inst : label is "FALSE";
  attribute C_BSC_RX_GATING : string;
  attribute C_BSC_RX_GATING of inst : label is "DISABLE";
  attribute C_BSC_SELF_CALIBRATE : string;
  attribute C_BSC_SELF_CALIBRATE of inst : label is "ENABLE";
  attribute C_BSC_SIM_SPEEDUP : string;
  attribute C_BSC_SIM_SPEEDUP of inst : label is "FAST";
  attribute C_BS_INIT_VAL : string;
  attribute C_BS_INIT_VAL of inst : label is "52'b0000000000000000001010101001010101010100101010101010";
  attribute C_CLKIN_DIFF_EN : integer;
  attribute C_CLKIN_DIFF_EN of inst : label is 0;
  attribute C_CLKIN_PERIOD : string;
  attribute C_CLKIN_PERIOD of inst : label is "12.500000";
  attribute C_CLK_FWD : integer;
  attribute C_CLK_FWD of inst : label is 0;
  attribute C_CLK_FWD_BITSLICE_NO : integer;
  attribute C_CLK_FWD_BITSLICE_NO of inst : label is 0;
  attribute C_CLK_FWD_ENABLE : string;
  attribute C_CLK_FWD_ENABLE of inst : label is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_CLK_FWD_PHASE : integer;
  attribute C_CLK_FWD_PHASE of inst : label is 0;
  attribute C_CLK_SIG_TYPE : string;
  attribute C_CLK_SIG_TYPE of inst : label is "SINGLE";
  attribute C_CLOCK_TRI : integer;
  attribute C_CLOCK_TRI of inst : label is 1;
  attribute C_DATA_PIN_EN : integer;
  attribute C_DATA_PIN_EN of inst : label is 16;
  attribute C_DATA_TRI : integer;
  attribute C_DATA_TRI of inst : label is 1;
  attribute C_DEVICE : string;
  attribute C_DEVICE of inst : label is "xcku035";
  attribute C_DEVICE_FAMILY : string;
  attribute C_DEVICE_FAMILY of inst : label is "kintexu";
  attribute C_DIFFERENTIAL_IO_STD : string;
  attribute C_DIFFERENTIAL_IO_STD of inst : label is "DIFF_SSTL12";
  attribute C_DIFFERENTIAL_IO_TERMINATION : string;
  attribute C_DIFFERENTIAL_IO_TERMINATION of inst : label is "NONE";
  attribute C_DIFF_EN : string;
  attribute C_DIFF_EN of inst : label is "52'b0000000000000000001111111101111111111110111111111111";
  attribute C_DIV_MODE : string;
  attribute C_DIV_MODE of inst : label is "DIV4";
  attribute C_ENABLE_BITSLIP : integer;
  attribute C_ENABLE_BITSLIP of inst : label is 0;
  attribute C_ENABLE_DATA_BITSLIP : integer;
  attribute C_ENABLE_DATA_BITSLIP of inst : label is 0;
  attribute C_ENABLE_N_PINS : integer;
  attribute C_ENABLE_N_PINS of inst : label is 1;
  attribute C_ENABLE_PLL0_PLLOUT1 : integer;
  attribute C_ENABLE_PLL0_PLLOUT1 of inst : label is 1;
  attribute C_ENABLE_PLL0_PLLOUTFB : integer;
  attribute C_ENABLE_PLL0_PLLOUTFB of inst : label is 0;
  attribute C_ENABLE_RIU_INTERFACE : integer;
  attribute C_ENABLE_RIU_INTERFACE of inst : label is 0;
  attribute C_ENABLE_RIU_SPLIT : integer;
  attribute C_ENABLE_RIU_SPLIT of inst : label is 0;
  attribute C_ENABLE_TX_TRI : integer;
  attribute C_ENABLE_TX_TRI of inst : label is 0;
  attribute C_EN_BIDIR : integer;
  attribute C_EN_BIDIR of inst : label is 0;
  attribute C_EN_BSC0 : integer;
  attribute C_EN_BSC0 of inst : label is 1;
  attribute C_EN_BSC1 : integer;
  attribute C_EN_BSC1 of inst : label is 1;
  attribute C_EN_BSC2 : integer;
  attribute C_EN_BSC2 of inst : label is 1;
  attribute C_EN_BSC3 : integer;
  attribute C_EN_BSC3 of inst : label is 1;
  attribute C_EN_BSC4 : integer;
  attribute C_EN_BSC4 of inst : label is 1;
  attribute C_EN_BSC5 : integer;
  attribute C_EN_BSC5 of inst : label is 1;
  attribute C_EN_BSC6 : integer;
  attribute C_EN_BSC6 of inst : label is 0;
  attribute C_EN_BSC7 : integer;
  attribute C_EN_BSC7 of inst : label is 0;
  attribute C_EN_MULTI_INTF_PORTS : integer;
  attribute C_EN_MULTI_INTF_PORTS of inst : label is 0;
  attribute C_EN_RIU_OR0 : string;
  attribute C_EN_RIU_OR0 of inst : label is "TRUE";
  attribute C_EN_RIU_OR1 : string;
  attribute C_EN_RIU_OR1 of inst : label is "TRUE";
  attribute C_EN_RIU_OR2 : string;
  attribute C_EN_RIU_OR2 of inst : label is "TRUE";
  attribute C_EN_RIU_OR3 : string;
  attribute C_EN_RIU_OR3 of inst : label is "FALSE";
  attribute C_EN_RX : integer;
  attribute C_EN_RX of inst : label is 0;
  attribute C_EN_TX : integer;
  attribute C_EN_TX of inst : label is 1;
  attribute C_EN_VTC : integer;
  attribute C_EN_VTC of inst : label is 0;
  attribute C_EXDES_BANK : string;
  attribute C_EXDES_BANK of inst : label is "44_(HP)";
  attribute C_EX_CLK_FREQ : string;
  attribute C_EX_CLK_FREQ of inst : label is "80.000000";
  attribute C_EX_INST_GEN : integer;
  attribute C_EX_INST_GEN of inst : label is 0;
  attribute C_FIFO_SYNC_MODE : integer;
  attribute C_FIFO_SYNC_MODE of inst : label is 1;
  attribute C_GC_LOC : string;
  attribute C_GC_LOC of inst : label is "21 {name IO_L11P_T1U_N8_GC_66 loc G12} 23 {name IO_L12P_T1U_N10_GC_66 loc E13} 28 {name IO_L14P_T2L_N2_GC_66 loc E11}";
  attribute C_INCLK_LOC : string;
  attribute C_INCLK_LOC of inst : label is "NONE";
  attribute C_INCLK_PIN : integer;
  attribute C_INCLK_PIN of inst : label is 100;
  attribute C_INV_RX_CLK : string;
  attribute C_INV_RX_CLK of inst : label is "8'b00000000";
  attribute C_NIB0_BS0_EN : integer;
  attribute C_NIB0_BS0_EN of inst : label is 0;
  attribute C_NIB0_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB0_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB0_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB0_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB0_EN_OTHER_NCLK : string;
  attribute C_NIB0_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB0_EN_OTHER_PCLK : string;
  attribute C_NIB0_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB1_BS0_EN : integer;
  attribute C_NIB1_BS0_EN of inst : label is 0;
  attribute C_NIB1_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB1_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB1_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB1_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB1_EN_OTHER_NCLK : string;
  attribute C_NIB1_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB1_EN_OTHER_PCLK : string;
  attribute C_NIB1_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB2_BS0_EN : integer;
  attribute C_NIB2_BS0_EN of inst : label is 0;
  attribute C_NIB2_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB2_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB2_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB2_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB2_EN_OTHER_NCLK : string;
  attribute C_NIB2_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB2_EN_OTHER_PCLK : string;
  attribute C_NIB2_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB3_BS0_EN : integer;
  attribute C_NIB3_BS0_EN of inst : label is 0;
  attribute C_NIB3_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB3_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB3_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB3_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB3_EN_OTHER_NCLK : string;
  attribute C_NIB3_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB3_EN_OTHER_PCLK : string;
  attribute C_NIB3_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB4_BS0_EN : integer;
  attribute C_NIB4_BS0_EN of inst : label is 0;
  attribute C_NIB4_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB4_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB4_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB4_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB4_EN_OTHER_NCLK : string;
  attribute C_NIB4_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB4_EN_OTHER_PCLK : string;
  attribute C_NIB4_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB5_BS0_EN : integer;
  attribute C_NIB5_BS0_EN of inst : label is 0;
  attribute C_NIB5_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB5_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB5_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB5_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB5_EN_OTHER_NCLK : string;
  attribute C_NIB5_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB5_EN_OTHER_PCLK : string;
  attribute C_NIB5_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB6_BS0_EN : integer;
  attribute C_NIB6_BS0_EN of inst : label is 0;
  attribute C_NIB6_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB6_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB6_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB6_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB6_EN_OTHER_NCLK : string;
  attribute C_NIB6_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB6_EN_OTHER_PCLK : string;
  attribute C_NIB6_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIB7_BS0_EN : integer;
  attribute C_NIB7_BS0_EN of inst : label is 0;
  attribute C_NIB7_EN_CLK_TO_EXT_NORTH : string;
  attribute C_NIB7_EN_CLK_TO_EXT_NORTH of inst : label is "1'b0";
  attribute C_NIB7_EN_CLK_TO_EXT_SOUTH : string;
  attribute C_NIB7_EN_CLK_TO_EXT_SOUTH of inst : label is "1'b0";
  attribute C_NIB7_EN_OTHER_NCLK : string;
  attribute C_NIB7_EN_OTHER_NCLK of inst : label is "1'b0";
  attribute C_NIB7_EN_OTHER_PCLK : string;
  attribute C_NIB7_EN_OTHER_PCLK of inst : label is "1'b0";
  attribute C_NIBBLE0_TRI : integer;
  attribute C_NIBBLE0_TRI of inst : label is 0;
  attribute C_NIBBLE1_TRI : integer;
  attribute C_NIBBLE1_TRI of inst : label is 0;
  attribute C_NIBBLE2_TRI : integer;
  attribute C_NIBBLE2_TRI of inst : label is 0;
  attribute C_NIBBLE3_TRI : integer;
  attribute C_NIBBLE3_TRI of inst : label is 0;
  attribute C_NIBBLE4_TRI : integer;
  attribute C_NIBBLE4_TRI of inst : label is 0;
  attribute C_NIBBLE5_TRI : integer;
  attribute C_NIBBLE5_TRI of inst : label is 0;
  attribute C_NIBBLE6_TRI : integer;
  attribute C_NIBBLE6_TRI of inst : label is 0;
  attribute C_NIBBLE7_TRI : integer;
  attribute C_NIBBLE7_TRI of inst : label is 0;
  attribute C_PIN_INFO : string;
  attribute C_PIN_INFO of inst : label is "0 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_P loc D14} 1 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out3_N loc C14} 2 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_P loc B15} 3 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out0_N loc A15} 4 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_P loc D13} 5 {nibble 0 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out1_N loc C13} 6 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_P loc B14} 7 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch1_out7_N loc A14} 8 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_P loc C12} 9 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out2_N loc B12} 10 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_P loc A13} 11 {nibble 1 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out6_N loc A12} 13 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_P loc H14} 14 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out0_N loc G14} 15 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_P loc G15} 16 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out7_N loc F15} 17 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_P loc J13} 18 {nibble 2 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out5_N loc H13} 19 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_P loc F14} 20 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch2_out2_N loc F13} 21 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_P loc G12} 22 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out4_N loc F12} 23 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_P loc E13} 24 {nibble 3 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out3_N loc E12} 26 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_P loc E10} 27 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out5_N loc D10} 28 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_P loc E11} 29 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out7_N loc D11} 30 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_P loc C9} 31 {nibble 4 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name ch3_out6_N loc C8} 32 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_P loc C11} 33 {nibble 5 dir TX sig_type DIFF data_strb Data data_strb_org Data sig_name TH_out_N loc B11}";
  attribute C_PLL0_CLK0_PHASE : string;
  attribute C_PLL0_CLK0_PHASE of inst : label is "0.000000";
  attribute C_PLL0_CLK1_PHASE : string;
  attribute C_PLL0_CLK1_PHASE of inst : label is "0.000000";
  attribute C_PLL0_CLKFBOUT_MULT : integer;
  attribute C_PLL0_CLKFBOUT_MULT of inst : label is 8;
  attribute C_PLL0_CLKOUT1_DIVIDE : integer;
  attribute C_PLL0_CLKOUT1_DIVIDE of inst : label is 4;
  attribute C_PLL0_CLKOUTPHY_MODE : string;
  attribute C_PLL0_CLKOUTPHY_MODE of inst : label is "VCO_2X";
  attribute C_PLL0_CLK_SOURCE : string;
  attribute C_PLL0_CLK_SOURCE of inst : label is "BUFG_TO_PLL";
  attribute C_PLL0_DIVCLK_DIVIDE : integer;
  attribute C_PLL0_DIVCLK_DIVIDE of inst : label is 1;
  attribute C_PLL0_DIV_FACTOR : string;
  attribute C_PLL0_DIV_FACTOR of inst : label is "0.500000";
  attribute C_PLL0_FIFO_WRITE_CLK_EN : integer;
  attribute C_PLL0_FIFO_WRITE_CLK_EN of inst : label is 0;
  attribute C_PLL0_MMCM_CLKFBOUT_MULT_F : string;
  attribute C_PLL0_MMCM_CLKFBOUT_MULT_F of inst : label is "14.875000";
  attribute C_PLL0_MMCM_CLKOUT0_DIVIDE_F : string;
  attribute C_PLL0_MMCM_CLKOUT0_DIVIDE_F of inst : label is "14.875000";
  attribute C_PLL0_MMCM_DIVCLK_DIVIDE : integer;
  attribute C_PLL0_MMCM_DIVCLK_DIVIDE of inst : label is 1;
  attribute C_PLL0_RX_EXTERNAL_CLK_TO_DATA : integer;
  attribute C_PLL0_RX_EXTERNAL_CLK_TO_DATA of inst : label is 5;
  attribute C_PLL1_CLK0_PHASE : string;
  attribute C_PLL1_CLK0_PHASE of inst : label is "0.000000";
  attribute C_PLL1_CLK1_PHASE : string;
  attribute C_PLL1_CLK1_PHASE of inst : label is "0.000000";
  attribute C_PLL1_CLKFBOUT_MULT : integer;
  attribute C_PLL1_CLKFBOUT_MULT of inst : label is 8;
  attribute C_PLL1_CLKOUTPHY_MODE : string;
  attribute C_PLL1_CLKOUTPHY_MODE of inst : label is "VCO_2X";
  attribute C_PLL1_DIVCLK_DIVIDE : integer;
  attribute C_PLL1_DIVCLK_DIVIDE of inst : label is 1;
  attribute C_PLL1_DIV_FACTOR : string;
  attribute C_PLL1_DIV_FACTOR of inst : label is "0.500000";
  attribute C_PLL_SHARING : integer;
  attribute C_PLL_SHARING of inst : label is 0;
  attribute C_PLL_VCOMIN : string;
  attribute C_PLL_VCOMIN of inst : label is "600.000000";
  attribute C_REC_IN_FREQ : string;
  attribute C_REC_IN_FREQ of inst : label is "80.000";
  attribute C_RX_BITSLICE0_EN : string;
  attribute C_RX_BITSLICE0_EN of inst : label is "8'b00000000";
  attribute C_RX_BITSLICE_EN : string;
  attribute C_RX_BITSLICE_EN of inst : label is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_RX_DELAY_CASCADE : integer;
  attribute C_RX_DELAY_CASCADE of inst : label is 0;
  attribute C_RX_DELAY_FORMAT : string;
  attribute C_RX_DELAY_FORMAT of inst : label is "TIME";
  attribute C_RX_DELAY_TYPE : string;
  attribute C_RX_DELAY_TYPE of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE0 : string;
  attribute C_RX_DELAY_TYPE0 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE1 : string;
  attribute C_RX_DELAY_TYPE1 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE10 : string;
  attribute C_RX_DELAY_TYPE10 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE11 : string;
  attribute C_RX_DELAY_TYPE11 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE12 : string;
  attribute C_RX_DELAY_TYPE12 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE13 : string;
  attribute C_RX_DELAY_TYPE13 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE14 : string;
  attribute C_RX_DELAY_TYPE14 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE15 : string;
  attribute C_RX_DELAY_TYPE15 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE16 : string;
  attribute C_RX_DELAY_TYPE16 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE17 : string;
  attribute C_RX_DELAY_TYPE17 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE18 : string;
  attribute C_RX_DELAY_TYPE18 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE19 : string;
  attribute C_RX_DELAY_TYPE19 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE2 : string;
  attribute C_RX_DELAY_TYPE2 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE20 : string;
  attribute C_RX_DELAY_TYPE20 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE21 : string;
  attribute C_RX_DELAY_TYPE21 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE22 : string;
  attribute C_RX_DELAY_TYPE22 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE23 : string;
  attribute C_RX_DELAY_TYPE23 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE24 : string;
  attribute C_RX_DELAY_TYPE24 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE25 : string;
  attribute C_RX_DELAY_TYPE25 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE26 : string;
  attribute C_RX_DELAY_TYPE26 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE27 : string;
  attribute C_RX_DELAY_TYPE27 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE28 : string;
  attribute C_RX_DELAY_TYPE28 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE29 : string;
  attribute C_RX_DELAY_TYPE29 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE3 : string;
  attribute C_RX_DELAY_TYPE3 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE30 : string;
  attribute C_RX_DELAY_TYPE30 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE31 : string;
  attribute C_RX_DELAY_TYPE31 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE32 : string;
  attribute C_RX_DELAY_TYPE32 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE33 : string;
  attribute C_RX_DELAY_TYPE33 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE34 : string;
  attribute C_RX_DELAY_TYPE34 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE35 : string;
  attribute C_RX_DELAY_TYPE35 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE36 : string;
  attribute C_RX_DELAY_TYPE36 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE37 : string;
  attribute C_RX_DELAY_TYPE37 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE38 : string;
  attribute C_RX_DELAY_TYPE38 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE39 : string;
  attribute C_RX_DELAY_TYPE39 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE4 : string;
  attribute C_RX_DELAY_TYPE4 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE40 : string;
  attribute C_RX_DELAY_TYPE40 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE41 : string;
  attribute C_RX_DELAY_TYPE41 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE42 : string;
  attribute C_RX_DELAY_TYPE42 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE43 : string;
  attribute C_RX_DELAY_TYPE43 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE44 : string;
  attribute C_RX_DELAY_TYPE44 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE45 : string;
  attribute C_RX_DELAY_TYPE45 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE46 : string;
  attribute C_RX_DELAY_TYPE46 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE47 : string;
  attribute C_RX_DELAY_TYPE47 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE48 : string;
  attribute C_RX_DELAY_TYPE48 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE49 : string;
  attribute C_RX_DELAY_TYPE49 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE5 : string;
  attribute C_RX_DELAY_TYPE5 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE50 : string;
  attribute C_RX_DELAY_TYPE50 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE51 : string;
  attribute C_RX_DELAY_TYPE51 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE6 : string;
  attribute C_RX_DELAY_TYPE6 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE7 : string;
  attribute C_RX_DELAY_TYPE7 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE8 : string;
  attribute C_RX_DELAY_TYPE8 of inst : label is "2'b00";
  attribute C_RX_DELAY_TYPE9 : string;
  attribute C_RX_DELAY_TYPE9 of inst : label is "2'b00";
  attribute C_RX_DELAY_VALUE : string;
  attribute C_RX_DELAY_VALUE of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE0 : string;
  attribute C_RX_DELAY_VALUE0 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE1 : string;
  attribute C_RX_DELAY_VALUE1 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE10 : string;
  attribute C_RX_DELAY_VALUE10 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE11 : string;
  attribute C_RX_DELAY_VALUE11 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE12 : string;
  attribute C_RX_DELAY_VALUE12 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE13 : string;
  attribute C_RX_DELAY_VALUE13 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE14 : string;
  attribute C_RX_DELAY_VALUE14 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE15 : string;
  attribute C_RX_DELAY_VALUE15 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE16 : string;
  attribute C_RX_DELAY_VALUE16 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE17 : string;
  attribute C_RX_DELAY_VALUE17 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE18 : string;
  attribute C_RX_DELAY_VALUE18 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE19 : string;
  attribute C_RX_DELAY_VALUE19 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE2 : string;
  attribute C_RX_DELAY_VALUE2 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE20 : string;
  attribute C_RX_DELAY_VALUE20 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE21 : string;
  attribute C_RX_DELAY_VALUE21 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE22 : string;
  attribute C_RX_DELAY_VALUE22 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE23 : string;
  attribute C_RX_DELAY_VALUE23 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE24 : string;
  attribute C_RX_DELAY_VALUE24 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE25 : string;
  attribute C_RX_DELAY_VALUE25 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE26 : string;
  attribute C_RX_DELAY_VALUE26 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE27 : string;
  attribute C_RX_DELAY_VALUE27 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE28 : string;
  attribute C_RX_DELAY_VALUE28 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE29 : string;
  attribute C_RX_DELAY_VALUE29 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE3 : string;
  attribute C_RX_DELAY_VALUE3 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE30 : string;
  attribute C_RX_DELAY_VALUE30 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE31 : string;
  attribute C_RX_DELAY_VALUE31 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE32 : string;
  attribute C_RX_DELAY_VALUE32 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE33 : string;
  attribute C_RX_DELAY_VALUE33 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE34 : string;
  attribute C_RX_DELAY_VALUE34 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE35 : string;
  attribute C_RX_DELAY_VALUE35 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE36 : string;
  attribute C_RX_DELAY_VALUE36 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE37 : string;
  attribute C_RX_DELAY_VALUE37 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE38 : string;
  attribute C_RX_DELAY_VALUE38 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE39 : string;
  attribute C_RX_DELAY_VALUE39 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE4 : string;
  attribute C_RX_DELAY_VALUE4 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE40 : string;
  attribute C_RX_DELAY_VALUE40 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE41 : string;
  attribute C_RX_DELAY_VALUE41 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE42 : string;
  attribute C_RX_DELAY_VALUE42 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE43 : string;
  attribute C_RX_DELAY_VALUE43 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE44 : string;
  attribute C_RX_DELAY_VALUE44 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE45 : string;
  attribute C_RX_DELAY_VALUE45 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE46 : string;
  attribute C_RX_DELAY_VALUE46 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE47 : string;
  attribute C_RX_DELAY_VALUE47 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE48 : string;
  attribute C_RX_DELAY_VALUE48 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE49 : string;
  attribute C_RX_DELAY_VALUE49 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE5 : string;
  attribute C_RX_DELAY_VALUE5 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE50 : string;
  attribute C_RX_DELAY_VALUE50 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE51 : string;
  attribute C_RX_DELAY_VALUE51 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE6 : string;
  attribute C_RX_DELAY_VALUE6 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE7 : string;
  attribute C_RX_DELAY_VALUE7 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE8 : string;
  attribute C_RX_DELAY_VALUE8 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE9 : string;
  attribute C_RX_DELAY_VALUE9 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT0 : string;
  attribute C_RX_DELAY_VALUE_EXT0 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT1 : string;
  attribute C_RX_DELAY_VALUE_EXT1 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT10 : string;
  attribute C_RX_DELAY_VALUE_EXT10 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT11 : string;
  attribute C_RX_DELAY_VALUE_EXT11 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT12 : string;
  attribute C_RX_DELAY_VALUE_EXT12 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT13 : string;
  attribute C_RX_DELAY_VALUE_EXT13 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT14 : string;
  attribute C_RX_DELAY_VALUE_EXT14 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT15 : string;
  attribute C_RX_DELAY_VALUE_EXT15 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT16 : string;
  attribute C_RX_DELAY_VALUE_EXT16 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT17 : string;
  attribute C_RX_DELAY_VALUE_EXT17 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT18 : string;
  attribute C_RX_DELAY_VALUE_EXT18 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT19 : string;
  attribute C_RX_DELAY_VALUE_EXT19 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT2 : string;
  attribute C_RX_DELAY_VALUE_EXT2 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT20 : string;
  attribute C_RX_DELAY_VALUE_EXT20 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT21 : string;
  attribute C_RX_DELAY_VALUE_EXT21 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT22 : string;
  attribute C_RX_DELAY_VALUE_EXT22 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT23 : string;
  attribute C_RX_DELAY_VALUE_EXT23 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT24 : string;
  attribute C_RX_DELAY_VALUE_EXT24 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT25 : string;
  attribute C_RX_DELAY_VALUE_EXT25 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT26 : string;
  attribute C_RX_DELAY_VALUE_EXT26 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT27 : string;
  attribute C_RX_DELAY_VALUE_EXT27 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT28 : string;
  attribute C_RX_DELAY_VALUE_EXT28 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT29 : string;
  attribute C_RX_DELAY_VALUE_EXT29 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT3 : string;
  attribute C_RX_DELAY_VALUE_EXT3 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT30 : string;
  attribute C_RX_DELAY_VALUE_EXT30 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT31 : string;
  attribute C_RX_DELAY_VALUE_EXT31 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT32 : string;
  attribute C_RX_DELAY_VALUE_EXT32 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT33 : string;
  attribute C_RX_DELAY_VALUE_EXT33 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT34 : string;
  attribute C_RX_DELAY_VALUE_EXT34 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT35 : string;
  attribute C_RX_DELAY_VALUE_EXT35 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT36 : string;
  attribute C_RX_DELAY_VALUE_EXT36 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT37 : string;
  attribute C_RX_DELAY_VALUE_EXT37 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT38 : string;
  attribute C_RX_DELAY_VALUE_EXT38 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT39 : string;
  attribute C_RX_DELAY_VALUE_EXT39 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT4 : string;
  attribute C_RX_DELAY_VALUE_EXT4 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT40 : string;
  attribute C_RX_DELAY_VALUE_EXT40 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT41 : string;
  attribute C_RX_DELAY_VALUE_EXT41 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT42 : string;
  attribute C_RX_DELAY_VALUE_EXT42 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT43 : string;
  attribute C_RX_DELAY_VALUE_EXT43 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT44 : string;
  attribute C_RX_DELAY_VALUE_EXT44 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT45 : string;
  attribute C_RX_DELAY_VALUE_EXT45 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT46 : string;
  attribute C_RX_DELAY_VALUE_EXT46 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT47 : string;
  attribute C_RX_DELAY_VALUE_EXT47 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT48 : string;
  attribute C_RX_DELAY_VALUE_EXT48 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT49 : string;
  attribute C_RX_DELAY_VALUE_EXT49 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT5 : string;
  attribute C_RX_DELAY_VALUE_EXT5 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT50 : string;
  attribute C_RX_DELAY_VALUE_EXT50 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT51 : string;
  attribute C_RX_DELAY_VALUE_EXT51 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT6 : string;
  attribute C_RX_DELAY_VALUE_EXT6 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT7 : string;
  attribute C_RX_DELAY_VALUE_EXT7 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT8 : string;
  attribute C_RX_DELAY_VALUE_EXT8 of inst : label is "12'b000000000000";
  attribute C_RX_DELAY_VALUE_EXT9 : string;
  attribute C_RX_DELAY_VALUE_EXT9 of inst : label is "12'b000000000000";
  attribute C_RX_EQUALIZATION_D : string;
  attribute C_RX_EQUALIZATION_D of inst : label is "NONE";
  attribute C_RX_EQUALIZATION_S : string;
  attribute C_RX_EQUALIZATION_S of inst : label is "NONE";
  attribute C_RX_FIFO_SYNC_MODE : string;
  attribute C_RX_FIFO_SYNC_MODE of inst : label is "FALSE";
  attribute C_RX_IS_CLK_EXT_INVERTED : string;
  attribute C_RX_IS_CLK_EXT_INVERTED of inst : label is "1'b0";
  attribute C_RX_IS_CLK_INVERTED : string;
  attribute C_RX_IS_CLK_INVERTED of inst : label is "1'b0";
  attribute C_RX_IS_RST_DLY_EXT_INVERTED : string;
  attribute C_RX_IS_RST_DLY_EXT_INVERTED of inst : label is "1'b0";
  attribute C_RX_IS_RST_DLY_INVERTED : string;
  attribute C_RX_IS_RST_DLY_INVERTED of inst : label is "1'b0";
  attribute C_RX_IS_RST_INVERTED : string;
  attribute C_RX_IS_RST_INVERTED of inst : label is "1'b0";
  attribute C_RX_PIN_EN : string;
  attribute C_RX_PIN_EN of inst : label is "52'b0000000000000000000000000000000000000000000000000000";
  attribute C_RX_REFCLK_FREQ : string;
  attribute C_RX_REFCLK_FREQ of inst : label is "1280.000000";
  attribute C_RX_STROBE_EN : string;
  attribute C_RX_STROBE_EN of inst : label is "16'b0000000000000000";
  attribute C_SERIALIZATION_FACTOR : integer;
  attribute C_SERIALIZATION_FACTOR of inst : label is 8;
  attribute C_SERIAL_MODE : string;
  attribute C_SERIAL_MODE of inst : label is "FALSE";
  attribute C_SIM_DEVICE : string;
  attribute C_SIM_DEVICE of inst : label is "ULTRASCALE";
  attribute C_SIM_VERSION : string;
  attribute C_SIM_VERSION of inst : label is "1.000000";
  attribute C_SINGLE_ENDED_IO_STD : string;
  attribute C_SINGLE_ENDED_IO_STD of inst : label is "NONE";
  attribute C_SINGLE_ENDED_IO_TERMINATION : string;
  attribute C_SINGLE_ENDED_IO_TERMINATION of inst : label is "NONE";
  attribute C_STRB_INFO : string;
  attribute C_STRB_INFO of inst : label is "99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99 99";
  attribute C_TEMPLATE : integer;
  attribute C_TEMPLATE of inst : label is 0;
  attribute C_TX_BITSLICE_EN : string;
  attribute C_TX_BITSLICE_EN of inst : label is "52'b0000000000000000000101010100101010101010010101010101";
  attribute C_TX_DATA_PHASE : integer;
  attribute C_TX_DATA_PHASE of inst : label is 0;
  attribute C_TX_DELAY_FORMAT : string;
  attribute C_TX_DELAY_FORMAT of inst : label is "TIME";
  attribute C_TX_DELAY_TYPE : integer;
  attribute C_TX_DELAY_TYPE of inst : label is 0;
  attribute C_TX_DELAY_TYPE0 : string;
  attribute C_TX_DELAY_TYPE0 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE1 : string;
  attribute C_TX_DELAY_TYPE1 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE10 : string;
  attribute C_TX_DELAY_TYPE10 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE11 : string;
  attribute C_TX_DELAY_TYPE11 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE12 : string;
  attribute C_TX_DELAY_TYPE12 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE13 : string;
  attribute C_TX_DELAY_TYPE13 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE14 : string;
  attribute C_TX_DELAY_TYPE14 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE15 : string;
  attribute C_TX_DELAY_TYPE15 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE16 : string;
  attribute C_TX_DELAY_TYPE16 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE17 : string;
  attribute C_TX_DELAY_TYPE17 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE18 : string;
  attribute C_TX_DELAY_TYPE18 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE19 : string;
  attribute C_TX_DELAY_TYPE19 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE2 : string;
  attribute C_TX_DELAY_TYPE2 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE20 : string;
  attribute C_TX_DELAY_TYPE20 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE21 : string;
  attribute C_TX_DELAY_TYPE21 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE22 : string;
  attribute C_TX_DELAY_TYPE22 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE23 : string;
  attribute C_TX_DELAY_TYPE23 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE24 : string;
  attribute C_TX_DELAY_TYPE24 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE25 : string;
  attribute C_TX_DELAY_TYPE25 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE26 : string;
  attribute C_TX_DELAY_TYPE26 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE27 : string;
  attribute C_TX_DELAY_TYPE27 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE28 : string;
  attribute C_TX_DELAY_TYPE28 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE29 : string;
  attribute C_TX_DELAY_TYPE29 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE3 : string;
  attribute C_TX_DELAY_TYPE3 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE30 : string;
  attribute C_TX_DELAY_TYPE30 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE31 : string;
  attribute C_TX_DELAY_TYPE31 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE32 : string;
  attribute C_TX_DELAY_TYPE32 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE33 : string;
  attribute C_TX_DELAY_TYPE33 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE34 : string;
  attribute C_TX_DELAY_TYPE34 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE35 : string;
  attribute C_TX_DELAY_TYPE35 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE36 : string;
  attribute C_TX_DELAY_TYPE36 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE37 : string;
  attribute C_TX_DELAY_TYPE37 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE38 : string;
  attribute C_TX_DELAY_TYPE38 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE39 : string;
  attribute C_TX_DELAY_TYPE39 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE4 : string;
  attribute C_TX_DELAY_TYPE4 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE40 : string;
  attribute C_TX_DELAY_TYPE40 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE41 : string;
  attribute C_TX_DELAY_TYPE41 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE42 : string;
  attribute C_TX_DELAY_TYPE42 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE43 : string;
  attribute C_TX_DELAY_TYPE43 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE44 : string;
  attribute C_TX_DELAY_TYPE44 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE45 : string;
  attribute C_TX_DELAY_TYPE45 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE46 : string;
  attribute C_TX_DELAY_TYPE46 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE47 : string;
  attribute C_TX_DELAY_TYPE47 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE48 : string;
  attribute C_TX_DELAY_TYPE48 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE49 : string;
  attribute C_TX_DELAY_TYPE49 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE5 : string;
  attribute C_TX_DELAY_TYPE5 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE50 : string;
  attribute C_TX_DELAY_TYPE50 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE51 : string;
  attribute C_TX_DELAY_TYPE51 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE6 : string;
  attribute C_TX_DELAY_TYPE6 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE7 : string;
  attribute C_TX_DELAY_TYPE7 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE8 : string;
  attribute C_TX_DELAY_TYPE8 of inst : label is "2'b00";
  attribute C_TX_DELAY_TYPE9 : string;
  attribute C_TX_DELAY_TYPE9 of inst : label is "2'b00";
  attribute C_TX_DELAY_VALUE : string;
  attribute C_TX_DELAY_VALUE of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE0 : string;
  attribute C_TX_DELAY_VALUE0 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE1 : string;
  attribute C_TX_DELAY_VALUE1 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE10 : string;
  attribute C_TX_DELAY_VALUE10 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE11 : string;
  attribute C_TX_DELAY_VALUE11 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE12 : string;
  attribute C_TX_DELAY_VALUE12 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE13 : string;
  attribute C_TX_DELAY_VALUE13 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE14 : string;
  attribute C_TX_DELAY_VALUE14 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE15 : string;
  attribute C_TX_DELAY_VALUE15 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE16 : string;
  attribute C_TX_DELAY_VALUE16 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE17 : string;
  attribute C_TX_DELAY_VALUE17 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE18 : string;
  attribute C_TX_DELAY_VALUE18 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE19 : string;
  attribute C_TX_DELAY_VALUE19 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE2 : string;
  attribute C_TX_DELAY_VALUE2 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE20 : string;
  attribute C_TX_DELAY_VALUE20 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE21 : string;
  attribute C_TX_DELAY_VALUE21 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE22 : string;
  attribute C_TX_DELAY_VALUE22 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE23 : string;
  attribute C_TX_DELAY_VALUE23 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE24 : string;
  attribute C_TX_DELAY_VALUE24 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE25 : string;
  attribute C_TX_DELAY_VALUE25 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE26 : string;
  attribute C_TX_DELAY_VALUE26 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE27 : string;
  attribute C_TX_DELAY_VALUE27 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE28 : string;
  attribute C_TX_DELAY_VALUE28 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE29 : string;
  attribute C_TX_DELAY_VALUE29 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE3 : string;
  attribute C_TX_DELAY_VALUE3 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE30 : string;
  attribute C_TX_DELAY_VALUE30 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE31 : string;
  attribute C_TX_DELAY_VALUE31 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE32 : string;
  attribute C_TX_DELAY_VALUE32 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE33 : string;
  attribute C_TX_DELAY_VALUE33 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE34 : string;
  attribute C_TX_DELAY_VALUE34 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE35 : string;
  attribute C_TX_DELAY_VALUE35 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE36 : string;
  attribute C_TX_DELAY_VALUE36 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE37 : string;
  attribute C_TX_DELAY_VALUE37 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE38 : string;
  attribute C_TX_DELAY_VALUE38 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE39 : string;
  attribute C_TX_DELAY_VALUE39 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE4 : string;
  attribute C_TX_DELAY_VALUE4 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE40 : string;
  attribute C_TX_DELAY_VALUE40 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE41 : string;
  attribute C_TX_DELAY_VALUE41 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE42 : string;
  attribute C_TX_DELAY_VALUE42 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE43 : string;
  attribute C_TX_DELAY_VALUE43 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE44 : string;
  attribute C_TX_DELAY_VALUE44 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE45 : string;
  attribute C_TX_DELAY_VALUE45 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE46 : string;
  attribute C_TX_DELAY_VALUE46 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE47 : string;
  attribute C_TX_DELAY_VALUE47 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE48 : string;
  attribute C_TX_DELAY_VALUE48 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE49 : string;
  attribute C_TX_DELAY_VALUE49 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE5 : string;
  attribute C_TX_DELAY_VALUE5 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE50 : string;
  attribute C_TX_DELAY_VALUE50 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE51 : string;
  attribute C_TX_DELAY_VALUE51 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE6 : string;
  attribute C_TX_DELAY_VALUE6 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE7 : string;
  attribute C_TX_DELAY_VALUE7 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE8 : string;
  attribute C_TX_DELAY_VALUE8 of inst : label is "12'b000000000000";
  attribute C_TX_DELAY_VALUE9 : string;
  attribute C_TX_DELAY_VALUE9 of inst : label is "12'b000000000000";
  attribute C_TX_DRIVE_D : string;
  attribute C_TX_DRIVE_D of inst : label is "";
  attribute C_TX_DRIVE_S : string;
  attribute C_TX_DRIVE_S of inst : label is "NONE";
  attribute C_TX_IS_CLK_INVERTED : string;
  attribute C_TX_IS_CLK_INVERTED of inst : label is "1'b0";
  attribute C_TX_IS_RST_DLY_INVERTED : string;
  attribute C_TX_IS_RST_DLY_INVERTED of inst : label is "1'b0";
  attribute C_TX_IS_RST_INVERTED : string;
  attribute C_TX_IS_RST_INVERTED of inst : label is "1'b0";
  attribute C_TX_NATIVE_ODELAY_BYPASS : string;
  attribute C_TX_NATIVE_ODELAY_BYPASS of inst : label is "FALSE";
  attribute C_TX_PRE_EMPHASIS_D : string;
  attribute C_TX_PRE_EMPHASIS_D of inst : label is "NONE";
  attribute C_TX_PRE_EMPHASIS_S : string;
  attribute C_TX_PRE_EMPHASIS_S of inst : label is "NONE";
  attribute C_TX_REFCLK_FREQ : string;
  attribute C_TX_REFCLK_FREQ of inst : label is "1280.000000";
  attribute C_TX_SLEW_D : string;
  attribute C_TX_SLEW_D of inst : label is "SLOW MEDIUM FAST";
  attribute C_TX_SLEW_S : string;
  attribute C_TX_SLEW_S of inst : label is "NONE";
  attribute C_TX_TRI_DELAY_FORMAT : string;
  attribute C_TX_TRI_DELAY_FORMAT of inst : label is "TIME";
  attribute C_TX_TRI_INIT : string;
  attribute C_TX_TRI_INIT of inst : label is "1'b1";
  attribute C_TX_TRI_IS_CLK_INVERTED : string;
  attribute C_TX_TRI_IS_CLK_INVERTED of inst : label is "1'b0";
  attribute C_TX_TRI_IS_RST_DLY_INVERTED : string;
  attribute C_TX_TRI_IS_RST_DLY_INVERTED of inst : label is "1'b0";
  attribute C_TX_TRI_IS_RST_INVERTED : string;
  attribute C_TX_TRI_IS_RST_INVERTED of inst : label is "1'b0";
  attribute C_TX_TRI_NATIVE_ODELAY_BYPASS : string;
  attribute C_TX_TRI_NATIVE_ODELAY_BYPASS of inst : label is "FALSE";
  attribute C_TX_TRI_OUTPUT_PHASE_90 : string;
  attribute C_TX_TRI_OUTPUT_PHASE_90 of inst : label is "FALSE";
  attribute DowngradeIPIdentifiedWarnings of inst : label is "yes";
  attribute TX_BITSLICE_TRI_EN : string;
  attribute TX_BITSLICE_TRI_EN of inst : label is "8'b00000000";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of TH_out_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin33";
  attribute X_INTERFACE_INFO of TH_out_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin32";
  attribute X_INTERFACE_INFO of ch1_out7_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin7";
  attribute X_INTERFACE_INFO of ch1_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin6";
  attribute X_INTERFACE_INFO of ch2_out0_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin3";
  attribute X_INTERFACE_INFO of ch2_out0_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin2";
  attribute X_INTERFACE_INFO of ch2_out2_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin20";
  attribute X_INTERFACE_INFO of ch2_out2_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin19";
  attribute X_INTERFACE_INFO of ch2_out3_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin1";
  attribute X_INTERFACE_INFO of ch2_out3_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin0";
  attribute X_INTERFACE_INFO of ch2_out5_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin18";
  attribute X_INTERFACE_INFO of ch2_out5_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin17";
  attribute X_INTERFACE_INFO of ch2_out6_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin11";
  attribute X_INTERFACE_INFO of ch2_out6_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin10";
  attribute X_INTERFACE_INFO of ch2_out7_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin16";
  attribute X_INTERFACE_INFO of ch2_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin15";
  attribute X_INTERFACE_INFO of ch3_out0_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin14";
  attribute X_INTERFACE_INFO of ch3_out0_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin13";
  attribute X_INTERFACE_INFO of ch3_out1_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin5";
  attribute X_INTERFACE_INFO of ch3_out1_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin4";
  attribute X_INTERFACE_INFO of ch3_out2_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin9";
  attribute X_INTERFACE_INFO of ch3_out2_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin8";
  attribute X_INTERFACE_INFO of ch3_out3_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin24";
  attribute X_INTERFACE_INFO of ch3_out3_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin23";
  attribute X_INTERFACE_INFO of ch3_out4_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin22";
  attribute X_INTERFACE_INFO of ch3_out4_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin21";
  attribute X_INTERFACE_INFO of ch3_out5_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin27";
  attribute X_INTERFACE_INFO of ch3_out5_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin26";
  attribute X_INTERFACE_INFO of ch3_out6_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin31";
  attribute X_INTERFACE_INFO of ch3_out6_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin30";
  attribute X_INTERFACE_INFO of ch3_out7_N : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin29";
  attribute X_INTERFACE_INFO of ch3_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_data_tx:1.0 xiphy_tx_pins pin28";
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clock_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clock_CLK, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of dly_rdy_bsc0 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc0";
  attribute X_INTERFACE_INFO of dly_rdy_bsc1 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc1";
  attribute X_INTERFACE_INFO of dly_rdy_bsc2 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc2";
  attribute X_INTERFACE_INFO of dly_rdy_bsc3 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc3";
  attribute X_INTERFACE_INFO of dly_rdy_bsc4 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc4";
  attribute X_INTERFACE_INFO of dly_rdy_bsc5 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL dly_rdy_bsc5";
  attribute X_INTERFACE_INFO of en_vtc_bsc0 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc0";
  attribute X_INTERFACE_INFO of en_vtc_bsc1 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc1";
  attribute X_INTERFACE_INFO of en_vtc_bsc2 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc2";
  attribute X_INTERFACE_INFO of en_vtc_bsc3 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc3";
  attribute X_INTERFACE_INFO of en_vtc_bsc4 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc4";
  attribute X_INTERFACE_INFO of en_vtc_bsc5 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL en_vtc_bsc5";
  attribute X_INTERFACE_INFO of pll0_clkout0 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL pll0_clkout0";
  attribute X_INTERFACE_INFO of pll0_clkout1 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL pll0_clkout1";
  attribute X_INTERFACE_INFO of pll0_locked : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL pll0_locked";
  attribute X_INTERFACE_INFO of rst : signal is "xilinx.com:signal:reset:1.0 reset_rst RST";
  attribute X_INTERFACE_PARAMETER of rst : signal is "XIL_INTERFACENAME reset_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of rst_seq_done : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL rst_seq_done";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc0 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc0";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc1 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc1";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc2 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc2";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc3 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc3";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc4 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc4";
  attribute X_INTERFACE_INFO of vtc_rdy_bsc5 : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_ctrl:1.0 HSSIO_CTRL vtc_rdy_bsc5";
  attribute X_INTERFACE_INFO of data_from_fabric_TH_out_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_32";
  attribute X_INTERFACE_INFO of data_from_fabric_ch1_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_6";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out0_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_2";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out2_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_19";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out3_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_0";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out5_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_17";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out6_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_10";
  attribute X_INTERFACE_INFO of data_from_fabric_ch2_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_15";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out0_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_13";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out1_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_4";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out2_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_8";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out3_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_23";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out4_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_21";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out5_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_26";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out6_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_30";
  attribute X_INTERFACE_INFO of data_from_fabric_ch3_out7_P : signal is "xilinx.com:display_high_speed_selectio_wiz:hssio_fab_to_ip:1.0 data_from_fabric fabric_to_ip_28";
begin
inst: entity work.high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_Bank_66_high_speed_selectio_wiz_v3_6_9
     port map (
      TH_out_N => TH_out_N,
      TH_out_P => TH_out_P,
      app_clk => '0',
      bg0_pin0_nc => '0',
      bg0_pin6_nc => '0',
      bg1_pin0_nc => '0',
      bg1_pin6_nc => '0',
      bg2_pin0_nc => '0',
      bg2_pin6_nc => '0',
      bg3_pin0_nc => '0',
      bg3_pin6_nc => '0',
      bidir_rx_clk => '0',
      bidir_tx_bs_tri_ce0 => '0',
      bidir_tx_bs_tri_ce1 => '0',
      bidir_tx_bs_tri_ce2 => '0',
      bidir_tx_bs_tri_ce3 => '0',
      bidir_tx_bs_tri_ce4 => '0',
      bidir_tx_bs_tri_ce5 => '0',
      bidir_tx_bs_tri_ce6 => '0',
      bidir_tx_bs_tri_ce7 => '0',
      bidir_tx_bs_tri_clk => '0',
      bidir_tx_bs_tri_cntvaluein0(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein1(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein2(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein3(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein4(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein5(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein6(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvaluein7(8 downto 0) => B"000000000",
      bidir_tx_bs_tri_cntvalueout0(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout0_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout1(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout1_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout2(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout2_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout3(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout3_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout4(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout4_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout5(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout5_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout6(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout6_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_cntvalueout7(8 downto 0) => NLW_inst_bidir_tx_bs_tri_cntvalueout7_UNCONNECTED(8 downto 0),
      bidir_tx_bs_tri_en_vtc0 => '0',
      bidir_tx_bs_tri_en_vtc1 => '0',
      bidir_tx_bs_tri_en_vtc2 => '0',
      bidir_tx_bs_tri_en_vtc3 => '0',
      bidir_tx_bs_tri_en_vtc4 => '0',
      bidir_tx_bs_tri_en_vtc5 => '0',
      bidir_tx_bs_tri_en_vtc6 => '0',
      bidir_tx_bs_tri_en_vtc7 => '0',
      bidir_tx_bs_tri_inc0 => '0',
      bidir_tx_bs_tri_inc1 => '0',
      bidir_tx_bs_tri_inc2 => '0',
      bidir_tx_bs_tri_inc3 => '0',
      bidir_tx_bs_tri_inc4 => '0',
      bidir_tx_bs_tri_inc5 => '0',
      bidir_tx_bs_tri_inc6 => '0',
      bidir_tx_bs_tri_inc7 => '0',
      bidir_tx_bs_tri_load0 => '0',
      bidir_tx_bs_tri_load1 => '0',
      bidir_tx_bs_tri_load2 => '0',
      bidir_tx_bs_tri_load3 => '0',
      bidir_tx_bs_tri_load4 => '0',
      bidir_tx_bs_tri_load5 => '0',
      bidir_tx_bs_tri_load6 => '0',
      bidir_tx_bs_tri_load7 => '0',
      bidir_tx_clk => '0',
      bitslip_error_0 => NLW_inst_bitslip_error_0_UNCONNECTED,
      bitslip_error_1 => NLW_inst_bitslip_error_1_UNCONNECTED,
      bitslip_error_10 => NLW_inst_bitslip_error_10_UNCONNECTED,
      bitslip_error_11 => NLW_inst_bitslip_error_11_UNCONNECTED,
      bitslip_error_12 => NLW_inst_bitslip_error_12_UNCONNECTED,
      bitslip_error_13 => NLW_inst_bitslip_error_13_UNCONNECTED,
      bitslip_error_14 => NLW_inst_bitslip_error_14_UNCONNECTED,
      bitslip_error_15 => NLW_inst_bitslip_error_15_UNCONNECTED,
      bitslip_error_16 => NLW_inst_bitslip_error_16_UNCONNECTED,
      bitslip_error_17 => NLW_inst_bitslip_error_17_UNCONNECTED,
      bitslip_error_18 => NLW_inst_bitslip_error_18_UNCONNECTED,
      bitslip_error_19 => NLW_inst_bitslip_error_19_UNCONNECTED,
      bitslip_error_2 => NLW_inst_bitslip_error_2_UNCONNECTED,
      bitslip_error_20 => NLW_inst_bitslip_error_20_UNCONNECTED,
      bitslip_error_21 => NLW_inst_bitslip_error_21_UNCONNECTED,
      bitslip_error_22 => NLW_inst_bitslip_error_22_UNCONNECTED,
      bitslip_error_23 => NLW_inst_bitslip_error_23_UNCONNECTED,
      bitslip_error_24 => NLW_inst_bitslip_error_24_UNCONNECTED,
      bitslip_error_25 => NLW_inst_bitslip_error_25_UNCONNECTED,
      bitslip_error_26 => NLW_inst_bitslip_error_26_UNCONNECTED,
      bitslip_error_27 => NLW_inst_bitslip_error_27_UNCONNECTED,
      bitslip_error_28 => NLW_inst_bitslip_error_28_UNCONNECTED,
      bitslip_error_29 => NLW_inst_bitslip_error_29_UNCONNECTED,
      bitslip_error_3 => NLW_inst_bitslip_error_3_UNCONNECTED,
      bitslip_error_30 => NLW_inst_bitslip_error_30_UNCONNECTED,
      bitslip_error_31 => NLW_inst_bitslip_error_31_UNCONNECTED,
      bitslip_error_32 => NLW_inst_bitslip_error_32_UNCONNECTED,
      bitslip_error_33 => NLW_inst_bitslip_error_33_UNCONNECTED,
      bitslip_error_34 => NLW_inst_bitslip_error_34_UNCONNECTED,
      bitslip_error_35 => NLW_inst_bitslip_error_35_UNCONNECTED,
      bitslip_error_36 => NLW_inst_bitslip_error_36_UNCONNECTED,
      bitslip_error_37 => NLW_inst_bitslip_error_37_UNCONNECTED,
      bitslip_error_38 => NLW_inst_bitslip_error_38_UNCONNECTED,
      bitslip_error_39 => NLW_inst_bitslip_error_39_UNCONNECTED,
      bitslip_error_4 => NLW_inst_bitslip_error_4_UNCONNECTED,
      bitslip_error_40 => NLW_inst_bitslip_error_40_UNCONNECTED,
      bitslip_error_41 => NLW_inst_bitslip_error_41_UNCONNECTED,
      bitslip_error_42 => NLW_inst_bitslip_error_42_UNCONNECTED,
      bitslip_error_43 => NLW_inst_bitslip_error_43_UNCONNECTED,
      bitslip_error_44 => NLW_inst_bitslip_error_44_UNCONNECTED,
      bitslip_error_45 => NLW_inst_bitslip_error_45_UNCONNECTED,
      bitslip_error_46 => NLW_inst_bitslip_error_46_UNCONNECTED,
      bitslip_error_47 => NLW_inst_bitslip_error_47_UNCONNECTED,
      bitslip_error_48 => NLW_inst_bitslip_error_48_UNCONNECTED,
      bitslip_error_49 => NLW_inst_bitslip_error_49_UNCONNECTED,
      bitslip_error_5 => NLW_inst_bitslip_error_5_UNCONNECTED,
      bitslip_error_50 => NLW_inst_bitslip_error_50_UNCONNECTED,
      bitslip_error_51 => NLW_inst_bitslip_error_51_UNCONNECTED,
      bitslip_error_6 => NLW_inst_bitslip_error_6_UNCONNECTED,
      bitslip_error_7 => NLW_inst_bitslip_error_7_UNCONNECTED,
      bitslip_error_8 => NLW_inst_bitslip_error_8_UNCONNECTED,
      bitslip_error_9 => NLW_inst_bitslip_error_9_UNCONNECTED,
      bs_rst_dphy_in => '0',
      ch1_out7_N => ch1_out7_N,
      ch1_out7_P => ch1_out7_P,
      ch2_out0_N => ch2_out0_N,
      ch2_out0_P => ch2_out0_P,
      ch2_out2_N => ch2_out2_N,
      ch2_out2_P => ch2_out2_P,
      ch2_out3_N => ch2_out3_N,
      ch2_out3_P => ch2_out3_P,
      ch2_out5_N => ch2_out5_N,
      ch2_out5_P => ch2_out5_P,
      ch2_out6_N => ch2_out6_N,
      ch2_out6_P => ch2_out6_P,
      ch2_out7_N => ch2_out7_N,
      ch2_out7_P => ch2_out7_P,
      ch3_out0_N => ch3_out0_N,
      ch3_out0_P => ch3_out0_P,
      ch3_out1_N => ch3_out1_N,
      ch3_out1_P => ch3_out1_P,
      ch3_out2_N => ch3_out2_N,
      ch3_out2_P => ch3_out2_P,
      ch3_out3_N => ch3_out3_N,
      ch3_out3_P => ch3_out3_P,
      ch3_out4_N => ch3_out4_N,
      ch3_out4_P => ch3_out4_P,
      ch3_out5_N => ch3_out5_N,
      ch3_out5_P => ch3_out5_P,
      ch3_out6_N => ch3_out6_N,
      ch3_out6_P => ch3_out6_P,
      ch3_out7_N => ch3_out7_N,
      ch3_out7_P => ch3_out7_P,
      clk => clk,
      clk_from_ibuf => NLW_inst_clk_from_ibuf_UNCONNECTED,
      clk_n => '0',
      clk_p => '0',
      daddr(6 downto 0) => B"0000000",
      data_from_fabric_TH_out_P(7 downto 0) => data_from_fabric_TH_out_P(7 downto 0),
      data_from_fabric_ch1_out7_P(7 downto 0) => data_from_fabric_ch1_out7_P(7 downto 0),
      data_from_fabric_ch2_out0_P(7 downto 0) => data_from_fabric_ch2_out0_P(7 downto 0),
      data_from_fabric_ch2_out2_P(7 downto 0) => data_from_fabric_ch2_out2_P(7 downto 0),
      data_from_fabric_ch2_out3_P(7 downto 0) => data_from_fabric_ch2_out3_P(7 downto 0),
      data_from_fabric_ch2_out5_P(7 downto 0) => data_from_fabric_ch2_out5_P(7 downto 0),
      data_from_fabric_ch2_out6_P(7 downto 0) => data_from_fabric_ch2_out6_P(7 downto 0),
      data_from_fabric_ch2_out7_P(7 downto 0) => data_from_fabric_ch2_out7_P(7 downto 0),
      data_from_fabric_ch3_out0_P(7 downto 0) => data_from_fabric_ch3_out0_P(7 downto 0),
      data_from_fabric_ch3_out1_P(7 downto 0) => data_from_fabric_ch3_out1_P(7 downto 0),
      data_from_fabric_ch3_out2_P(7 downto 0) => data_from_fabric_ch3_out2_P(7 downto 0),
      data_from_fabric_ch3_out3_P(7 downto 0) => data_from_fabric_ch3_out3_P(7 downto 0),
      data_from_fabric_ch3_out4_P(7 downto 0) => data_from_fabric_ch3_out4_P(7 downto 0),
      data_from_fabric_ch3_out5_P(7 downto 0) => data_from_fabric_ch3_out5_P(7 downto 0),
      data_from_fabric_ch3_out6_P(7 downto 0) => data_from_fabric_ch3_out6_P(7 downto 0),
      data_from_fabric_ch3_out7_P(7 downto 0) => data_from_fabric_ch3_out7_P(7 downto 0),
      dclk => '0',
      den => '0',
      di(15 downto 0) => B"0000000000000000",
      dly_rdy_bsc0 => dly_rdy_bsc0,
      dly_rdy_bsc1 => dly_rdy_bsc1,
      dly_rdy_bsc2 => dly_rdy_bsc2,
      dly_rdy_bsc3 => dly_rdy_bsc3,
      dly_rdy_bsc4 => dly_rdy_bsc4,
      dly_rdy_bsc5 => dly_rdy_bsc5,
      dly_rdy_bsc6 => NLW_inst_dly_rdy_bsc6_UNCONNECTED,
      dly_rdy_bsc7 => NLW_inst_dly_rdy_bsc7_UNCONNECTED,
      do_out(15 downto 0) => NLW_inst_do_out_UNCONNECTED(15 downto 0),
      drdy => NLW_inst_drdy_UNCONNECTED,
      dwe => '0',
      en_vtc_bsc0 => en_vtc_bsc0,
      en_vtc_bsc1 => en_vtc_bsc1,
      en_vtc_bsc2 => en_vtc_bsc2,
      en_vtc_bsc3 => en_vtc_bsc3,
      en_vtc_bsc4 => en_vtc_bsc4,
      en_vtc_bsc5 => en_vtc_bsc5,
      en_vtc_bsc6 => '0',
      en_vtc_bsc7 => '0',
      fifo_empty_0 => NLW_inst_fifo_empty_0_UNCONNECTED,
      fifo_empty_1 => NLW_inst_fifo_empty_1_UNCONNECTED,
      fifo_empty_10 => NLW_inst_fifo_empty_10_UNCONNECTED,
      fifo_empty_11 => NLW_inst_fifo_empty_11_UNCONNECTED,
      fifo_empty_12 => NLW_inst_fifo_empty_12_UNCONNECTED,
      fifo_empty_13 => NLW_inst_fifo_empty_13_UNCONNECTED,
      fifo_empty_14 => NLW_inst_fifo_empty_14_UNCONNECTED,
      fifo_empty_15 => NLW_inst_fifo_empty_15_UNCONNECTED,
      fifo_empty_16 => NLW_inst_fifo_empty_16_UNCONNECTED,
      fifo_empty_17 => NLW_inst_fifo_empty_17_UNCONNECTED,
      fifo_empty_18 => NLW_inst_fifo_empty_18_UNCONNECTED,
      fifo_empty_19 => NLW_inst_fifo_empty_19_UNCONNECTED,
      fifo_empty_2 => NLW_inst_fifo_empty_2_UNCONNECTED,
      fifo_empty_20 => NLW_inst_fifo_empty_20_UNCONNECTED,
      fifo_empty_21 => NLW_inst_fifo_empty_21_UNCONNECTED,
      fifo_empty_22 => NLW_inst_fifo_empty_22_UNCONNECTED,
      fifo_empty_23 => NLW_inst_fifo_empty_23_UNCONNECTED,
      fifo_empty_24 => NLW_inst_fifo_empty_24_UNCONNECTED,
      fifo_empty_25 => NLW_inst_fifo_empty_25_UNCONNECTED,
      fifo_empty_26 => NLW_inst_fifo_empty_26_UNCONNECTED,
      fifo_empty_27 => NLW_inst_fifo_empty_27_UNCONNECTED,
      fifo_empty_28 => NLW_inst_fifo_empty_28_UNCONNECTED,
      fifo_empty_29 => NLW_inst_fifo_empty_29_UNCONNECTED,
      fifo_empty_3 => NLW_inst_fifo_empty_3_UNCONNECTED,
      fifo_empty_30 => NLW_inst_fifo_empty_30_UNCONNECTED,
      fifo_empty_31 => NLW_inst_fifo_empty_31_UNCONNECTED,
      fifo_empty_32 => NLW_inst_fifo_empty_32_UNCONNECTED,
      fifo_empty_33 => NLW_inst_fifo_empty_33_UNCONNECTED,
      fifo_empty_34 => NLW_inst_fifo_empty_34_UNCONNECTED,
      fifo_empty_35 => NLW_inst_fifo_empty_35_UNCONNECTED,
      fifo_empty_36 => NLW_inst_fifo_empty_36_UNCONNECTED,
      fifo_empty_37 => NLW_inst_fifo_empty_37_UNCONNECTED,
      fifo_empty_38 => NLW_inst_fifo_empty_38_UNCONNECTED,
      fifo_empty_39 => NLW_inst_fifo_empty_39_UNCONNECTED,
      fifo_empty_4 => NLW_inst_fifo_empty_4_UNCONNECTED,
      fifo_empty_40 => NLW_inst_fifo_empty_40_UNCONNECTED,
      fifo_empty_41 => NLW_inst_fifo_empty_41_UNCONNECTED,
      fifo_empty_42 => NLW_inst_fifo_empty_42_UNCONNECTED,
      fifo_empty_43 => NLW_inst_fifo_empty_43_UNCONNECTED,
      fifo_empty_44 => NLW_inst_fifo_empty_44_UNCONNECTED,
      fifo_empty_45 => NLW_inst_fifo_empty_45_UNCONNECTED,
      fifo_empty_46 => NLW_inst_fifo_empty_46_UNCONNECTED,
      fifo_empty_47 => NLW_inst_fifo_empty_47_UNCONNECTED,
      fifo_empty_48 => NLW_inst_fifo_empty_48_UNCONNECTED,
      fifo_empty_49 => NLW_inst_fifo_empty_49_UNCONNECTED,
      fifo_empty_5 => NLW_inst_fifo_empty_5_UNCONNECTED,
      fifo_empty_50 => NLW_inst_fifo_empty_50_UNCONNECTED,
      fifo_empty_51 => NLW_inst_fifo_empty_51_UNCONNECTED,
      fifo_empty_6 => NLW_inst_fifo_empty_6_UNCONNECTED,
      fifo_empty_7 => NLW_inst_fifo_empty_7_UNCONNECTED,
      fifo_empty_8 => NLW_inst_fifo_empty_8_UNCONNECTED,
      fifo_empty_9 => NLW_inst_fifo_empty_9_UNCONNECTED,
      fifo_rd_clk_0 => '0',
      fifo_rd_clk_1 => '0',
      fifo_rd_clk_10 => '0',
      fifo_rd_clk_11 => '0',
      fifo_rd_clk_12 => '0',
      fifo_rd_clk_13 => '0',
      fifo_rd_clk_14 => '0',
      fifo_rd_clk_15 => '0',
      fifo_rd_clk_16 => '0',
      fifo_rd_clk_17 => '0',
      fifo_rd_clk_18 => '0',
      fifo_rd_clk_19 => '0',
      fifo_rd_clk_2 => '0',
      fifo_rd_clk_20 => '0',
      fifo_rd_clk_21 => '0',
      fifo_rd_clk_22 => '0',
      fifo_rd_clk_23 => '0',
      fifo_rd_clk_24 => '0',
      fifo_rd_clk_25 => '0',
      fifo_rd_clk_26 => '0',
      fifo_rd_clk_27 => '0',
      fifo_rd_clk_28 => '0',
      fifo_rd_clk_29 => '0',
      fifo_rd_clk_3 => '0',
      fifo_rd_clk_30 => '0',
      fifo_rd_clk_31 => '0',
      fifo_rd_clk_32 => '0',
      fifo_rd_clk_33 => '0',
      fifo_rd_clk_34 => '0',
      fifo_rd_clk_35 => '0',
      fifo_rd_clk_36 => '0',
      fifo_rd_clk_37 => '0',
      fifo_rd_clk_38 => '0',
      fifo_rd_clk_39 => '0',
      fifo_rd_clk_4 => '0',
      fifo_rd_clk_40 => '0',
      fifo_rd_clk_41 => '0',
      fifo_rd_clk_42 => '0',
      fifo_rd_clk_43 => '0',
      fifo_rd_clk_44 => '0',
      fifo_rd_clk_45 => '0',
      fifo_rd_clk_46 => '0',
      fifo_rd_clk_47 => '0',
      fifo_rd_clk_48 => '0',
      fifo_rd_clk_49 => '0',
      fifo_rd_clk_5 => '0',
      fifo_rd_clk_50 => '0',
      fifo_rd_clk_51 => '0',
      fifo_rd_clk_6 => '0',
      fifo_rd_clk_7 => '0',
      fifo_rd_clk_8 => '0',
      fifo_rd_clk_9 => '0',
      fifo_rd_data_valid => NLW_inst_fifo_rd_data_valid_UNCONNECTED,
      fifo_rd_en_0 => '0',
      fifo_rd_en_1 => '0',
      fifo_rd_en_10 => '0',
      fifo_rd_en_11 => '0',
      fifo_rd_en_12 => '0',
      fifo_rd_en_13 => '0',
      fifo_rd_en_14 => '0',
      fifo_rd_en_15 => '0',
      fifo_rd_en_16 => '0',
      fifo_rd_en_17 => '0',
      fifo_rd_en_18 => '0',
      fifo_rd_en_19 => '0',
      fifo_rd_en_2 => '0',
      fifo_rd_en_20 => '0',
      fifo_rd_en_21 => '0',
      fifo_rd_en_22 => '0',
      fifo_rd_en_23 => '0',
      fifo_rd_en_24 => '0',
      fifo_rd_en_25 => '0',
      fifo_rd_en_26 => '0',
      fifo_rd_en_27 => '0',
      fifo_rd_en_28 => '0',
      fifo_rd_en_29 => '0',
      fifo_rd_en_3 => '0',
      fifo_rd_en_30 => '0',
      fifo_rd_en_31 => '0',
      fifo_rd_en_32 => '0',
      fifo_rd_en_33 => '0',
      fifo_rd_en_34 => '0',
      fifo_rd_en_35 => '0',
      fifo_rd_en_36 => '0',
      fifo_rd_en_37 => '0',
      fifo_rd_en_38 => '0',
      fifo_rd_en_39 => '0',
      fifo_rd_en_4 => '0',
      fifo_rd_en_40 => '0',
      fifo_rd_en_41 => '0',
      fifo_rd_en_42 => '0',
      fifo_rd_en_43 => '0',
      fifo_rd_en_44 => '0',
      fifo_rd_en_45 => '0',
      fifo_rd_en_46 => '0',
      fifo_rd_en_47 => '0',
      fifo_rd_en_48 => '0',
      fifo_rd_en_49 => '0',
      fifo_rd_en_5 => '0',
      fifo_rd_en_50 => '0',
      fifo_rd_en_51 => '0',
      fifo_rd_en_6 => '0',
      fifo_rd_en_7 => '0',
      fifo_rd_en_8 => '0',
      fifo_rd_en_9 => '0',
      fifo_wr_clk_0 => NLW_inst_fifo_wr_clk_0_UNCONNECTED,
      fifo_wr_clk_13 => NLW_inst_fifo_wr_clk_13_UNCONNECTED,
      fifo_wr_clk_19 => NLW_inst_fifo_wr_clk_19_UNCONNECTED,
      fifo_wr_clk_26 => NLW_inst_fifo_wr_clk_26_UNCONNECTED,
      fifo_wr_clk_32 => NLW_inst_fifo_wr_clk_32_UNCONNECTED,
      fifo_wr_clk_39 => NLW_inst_fifo_wr_clk_39_UNCONNECTED,
      fifo_wr_clk_45 => NLW_inst_fifo_wr_clk_45_UNCONNECTED,
      fifo_wr_clk_6 => NLW_inst_fifo_wr_clk_6_UNCONNECTED,
      hs_rx_disable(15 downto 0) => B"0000000000000000",
      intf_rdy => NLW_inst_intf_rdy_UNCONNECTED,
      lp_rx_disable(15 downto 0) => B"0000000000000000",
      lp_rx_o_n(15 downto 0) => NLW_inst_lp_rx_o_n_UNCONNECTED(15 downto 0),
      lp_rx_o_p(15 downto 0) => NLW_inst_lp_rx_o_p_UNCONNECTED(15 downto 0),
      lptx_i_n(15 downto 0) => B"0000000000000000",
      lptx_i_p(15 downto 0) => B"0000000000000000",
      lptx_t(15 downto 0) => B"0000000000000000",
      multi_intf_lock_in => '0',
      phy_rden_bsc0(3 downto 0) => B"0000",
      phy_rden_bsc1(3 downto 0) => B"0000",
      phy_rden_bsc2(3 downto 0) => B"0000",
      phy_rden_bsc3(3 downto 0) => B"0000",
      phy_rden_bsc4(3 downto 0) => B"0000",
      phy_rden_bsc5(3 downto 0) => B"0000",
      phy_rden_bsc6(3 downto 0) => B"0000",
      phy_rden_bsc7(3 downto 0) => B"0000",
      pll0_clkout0 => pll0_clkout0,
      pll0_clkout1 => pll0_clkout1,
      pll0_locked => pll0_locked,
      pll1_clkout0 => NLW_inst_pll1_clkout0_UNCONNECTED,
      pll1_locked => NLW_inst_pll1_locked_UNCONNECTED,
      riu_addr_bg0(5 downto 0) => B"000000",
      riu_addr_bg0_bs0(5 downto 0) => B"000000",
      riu_addr_bg0_bs1(5 downto 0) => B"000000",
      riu_addr_bg1(5 downto 0) => B"000000",
      riu_addr_bg1_bs2(5 downto 0) => B"000000",
      riu_addr_bg1_bs3(5 downto 0) => B"000000",
      riu_addr_bg2(5 downto 0) => B"000000",
      riu_addr_bg2_bs4(5 downto 0) => B"000000",
      riu_addr_bg2_bs5(5 downto 0) => B"000000",
      riu_addr_bg3(5 downto 0) => B"000000",
      riu_addr_bg3_bs6(5 downto 0) => B"000000",
      riu_addr_bg3_bs7(5 downto 0) => B"000000",
      riu_clk => '0',
      riu_nibble_sel_bg0(1 downto 0) => B"00",
      riu_nibble_sel_bg0_bs0 => '0',
      riu_nibble_sel_bg0_bs1 => '0',
      riu_nibble_sel_bg1(1 downto 0) => B"00",
      riu_nibble_sel_bg1_bs2 => '0',
      riu_nibble_sel_bg1_bs3 => '0',
      riu_nibble_sel_bg2(1 downto 0) => B"00",
      riu_nibble_sel_bg2_bs4 => '0',
      riu_nibble_sel_bg2_bs5 => '0',
      riu_nibble_sel_bg3(1 downto 0) => B"00",
      riu_nibble_sel_bg3_bs6 => '0',
      riu_nibble_sel_bg3_bs7 => '0',
      riu_rd_data_bg0(15 downto 0) => NLW_inst_riu_rd_data_bg0_UNCONNECTED(15 downto 0),
      riu_rd_data_bg0_bs0(15 downto 0) => NLW_inst_riu_rd_data_bg0_bs0_UNCONNECTED(15 downto 0),
      riu_rd_data_bg0_bs1(15 downto 0) => NLW_inst_riu_rd_data_bg0_bs1_UNCONNECTED(15 downto 0),
      riu_rd_data_bg1(15 downto 0) => NLW_inst_riu_rd_data_bg1_UNCONNECTED(15 downto 0),
      riu_rd_data_bg1_bs2(15 downto 0) => NLW_inst_riu_rd_data_bg1_bs2_UNCONNECTED(15 downto 0),
      riu_rd_data_bg1_bs3(15 downto 0) => NLW_inst_riu_rd_data_bg1_bs3_UNCONNECTED(15 downto 0),
      riu_rd_data_bg2(15 downto 0) => NLW_inst_riu_rd_data_bg2_UNCONNECTED(15 downto 0),
      riu_rd_data_bg2_bs4(15 downto 0) => NLW_inst_riu_rd_data_bg2_bs4_UNCONNECTED(15 downto 0),
      riu_rd_data_bg2_bs5(15 downto 0) => NLW_inst_riu_rd_data_bg2_bs5_UNCONNECTED(15 downto 0),
      riu_rd_data_bg3(15 downto 0) => NLW_inst_riu_rd_data_bg3_UNCONNECTED(15 downto 0),
      riu_rd_data_bg3_bs6(15 downto 0) => NLW_inst_riu_rd_data_bg3_bs6_UNCONNECTED(15 downto 0),
      riu_rd_data_bg3_bs7(15 downto 0) => NLW_inst_riu_rd_data_bg3_bs7_UNCONNECTED(15 downto 0),
      riu_valid_bg0 => NLW_inst_riu_valid_bg0_UNCONNECTED,
      riu_valid_bg0_bs0 => NLW_inst_riu_valid_bg0_bs0_UNCONNECTED,
      riu_valid_bg0_bs1 => NLW_inst_riu_valid_bg0_bs1_UNCONNECTED,
      riu_valid_bg1 => NLW_inst_riu_valid_bg1_UNCONNECTED,
      riu_valid_bg1_bs2 => NLW_inst_riu_valid_bg1_bs2_UNCONNECTED,
      riu_valid_bg1_bs3 => NLW_inst_riu_valid_bg1_bs3_UNCONNECTED,
      riu_valid_bg2 => NLW_inst_riu_valid_bg2_UNCONNECTED,
      riu_valid_bg2_bs4 => NLW_inst_riu_valid_bg2_bs4_UNCONNECTED,
      riu_valid_bg2_bs5 => NLW_inst_riu_valid_bg2_bs5_UNCONNECTED,
      riu_valid_bg3 => NLW_inst_riu_valid_bg3_UNCONNECTED,
      riu_valid_bg3_bs6 => NLW_inst_riu_valid_bg3_bs6_UNCONNECTED,
      riu_valid_bg3_bs7 => NLW_inst_riu_valid_bg3_bs7_UNCONNECTED,
      riu_wr_data_bg0(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg0_bs0(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg0_bs1(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg1(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg1_bs2(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg1_bs3(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg2(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg2_bs4(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg2_bs5(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg3(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg3_bs6(15 downto 0) => B"0000000000000000",
      riu_wr_data_bg3_bs7(15 downto 0) => B"0000000000000000",
      riu_wr_en_bg0 => '0',
      riu_wr_en_bg0_bs0 => '0',
      riu_wr_en_bg0_bs1 => '0',
      riu_wr_en_bg1 => '0',
      riu_wr_en_bg1_bs2 => '0',
      riu_wr_en_bg1_bs3 => '0',
      riu_wr_en_bg2 => '0',
      riu_wr_en_bg2_bs4 => '0',
      riu_wr_en_bg2_bs5 => '0',
      riu_wr_en_bg3 => '0',
      riu_wr_en_bg3_bs6 => '0',
      riu_wr_en_bg3_bs7 => '0',
      rst => rst,
      rst_seq_done => rst_seq_done,
      rx_bitslip_sync_done => NLW_inst_rx_bitslip_sync_done_UNCONNECTED,
      rx_ce_0 => '0',
      rx_ce_1 => '0',
      rx_ce_10 => '0',
      rx_ce_11 => '0',
      rx_ce_12 => '0',
      rx_ce_13 => '0',
      rx_ce_14 => '0',
      rx_ce_15 => '0',
      rx_ce_16 => '0',
      rx_ce_17 => '0',
      rx_ce_18 => '0',
      rx_ce_19 => '0',
      rx_ce_2 => '0',
      rx_ce_20 => '0',
      rx_ce_21 => '0',
      rx_ce_22 => '0',
      rx_ce_23 => '0',
      rx_ce_24 => '0',
      rx_ce_25 => '0',
      rx_ce_26 => '0',
      rx_ce_27 => '0',
      rx_ce_28 => '0',
      rx_ce_29 => '0',
      rx_ce_3 => '0',
      rx_ce_30 => '0',
      rx_ce_31 => '0',
      rx_ce_32 => '0',
      rx_ce_33 => '0',
      rx_ce_34 => '0',
      rx_ce_35 => '0',
      rx_ce_36 => '0',
      rx_ce_37 => '0',
      rx_ce_38 => '0',
      rx_ce_39 => '0',
      rx_ce_4 => '0',
      rx_ce_40 => '0',
      rx_ce_41 => '0',
      rx_ce_42 => '0',
      rx_ce_43 => '0',
      rx_ce_44 => '0',
      rx_ce_45 => '0',
      rx_ce_46 => '0',
      rx_ce_47 => '0',
      rx_ce_48 => '0',
      rx_ce_49 => '0',
      rx_ce_5 => '0',
      rx_ce_50 => '0',
      rx_ce_51 => '0',
      rx_ce_6 => '0',
      rx_ce_7 => '0',
      rx_ce_8 => '0',
      rx_ce_9 => '0',
      rx_ce_ext_0 => '0',
      rx_ce_ext_1 => '0',
      rx_ce_ext_10 => '0',
      rx_ce_ext_11 => '0',
      rx_ce_ext_12 => '0',
      rx_ce_ext_13 => '0',
      rx_ce_ext_14 => '0',
      rx_ce_ext_15 => '0',
      rx_ce_ext_16 => '0',
      rx_ce_ext_17 => '0',
      rx_ce_ext_18 => '0',
      rx_ce_ext_19 => '0',
      rx_ce_ext_2 => '0',
      rx_ce_ext_20 => '0',
      rx_ce_ext_21 => '0',
      rx_ce_ext_22 => '0',
      rx_ce_ext_23 => '0',
      rx_ce_ext_24 => '0',
      rx_ce_ext_25 => '0',
      rx_ce_ext_26 => '0',
      rx_ce_ext_27 => '0',
      rx_ce_ext_28 => '0',
      rx_ce_ext_29 => '0',
      rx_ce_ext_3 => '0',
      rx_ce_ext_30 => '0',
      rx_ce_ext_31 => '0',
      rx_ce_ext_32 => '0',
      rx_ce_ext_33 => '0',
      rx_ce_ext_34 => '0',
      rx_ce_ext_35 => '0',
      rx_ce_ext_36 => '0',
      rx_ce_ext_37 => '0',
      rx_ce_ext_38 => '0',
      rx_ce_ext_39 => '0',
      rx_ce_ext_4 => '0',
      rx_ce_ext_40 => '0',
      rx_ce_ext_41 => '0',
      rx_ce_ext_42 => '0',
      rx_ce_ext_43 => '0',
      rx_ce_ext_44 => '0',
      rx_ce_ext_45 => '0',
      rx_ce_ext_46 => '0',
      rx_ce_ext_47 => '0',
      rx_ce_ext_48 => '0',
      rx_ce_ext_49 => '0',
      rx_ce_ext_5 => '0',
      rx_ce_ext_50 => '0',
      rx_ce_ext_51 => '0',
      rx_ce_ext_6 => '0',
      rx_ce_ext_7 => '0',
      rx_ce_ext_8 => '0',
      rx_ce_ext_9 => '0',
      rx_clk => '0',
      rx_cntvaluein_0(8 downto 0) => B"000000000",
      rx_cntvaluein_1(8 downto 0) => B"000000000",
      rx_cntvaluein_10(8 downto 0) => B"000000000",
      rx_cntvaluein_11(8 downto 0) => B"000000000",
      rx_cntvaluein_12(8 downto 0) => B"000000000",
      rx_cntvaluein_13(8 downto 0) => B"000000000",
      rx_cntvaluein_14(8 downto 0) => B"000000000",
      rx_cntvaluein_15(8 downto 0) => B"000000000",
      rx_cntvaluein_16(8 downto 0) => B"000000000",
      rx_cntvaluein_17(8 downto 0) => B"000000000",
      rx_cntvaluein_18(8 downto 0) => B"000000000",
      rx_cntvaluein_19(8 downto 0) => B"000000000",
      rx_cntvaluein_2(8 downto 0) => B"000000000",
      rx_cntvaluein_20(8 downto 0) => B"000000000",
      rx_cntvaluein_21(8 downto 0) => B"000000000",
      rx_cntvaluein_22(8 downto 0) => B"000000000",
      rx_cntvaluein_23(8 downto 0) => B"000000000",
      rx_cntvaluein_24(8 downto 0) => B"000000000",
      rx_cntvaluein_25(8 downto 0) => B"000000000",
      rx_cntvaluein_26(8 downto 0) => B"000000000",
      rx_cntvaluein_27(8 downto 0) => B"000000000",
      rx_cntvaluein_28(8 downto 0) => B"000000000",
      rx_cntvaluein_29(8 downto 0) => B"000000000",
      rx_cntvaluein_3(8 downto 0) => B"000000000",
      rx_cntvaluein_30(8 downto 0) => B"000000000",
      rx_cntvaluein_31(8 downto 0) => B"000000000",
      rx_cntvaluein_32(8 downto 0) => B"000000000",
      rx_cntvaluein_33(8 downto 0) => B"000000000",
      rx_cntvaluein_34(8 downto 0) => B"000000000",
      rx_cntvaluein_35(8 downto 0) => B"000000000",
      rx_cntvaluein_36(8 downto 0) => B"000000000",
      rx_cntvaluein_37(8 downto 0) => B"000000000",
      rx_cntvaluein_38(8 downto 0) => B"000000000",
      rx_cntvaluein_39(8 downto 0) => B"000000000",
      rx_cntvaluein_4(8 downto 0) => B"000000000",
      rx_cntvaluein_40(8 downto 0) => B"000000000",
      rx_cntvaluein_41(8 downto 0) => B"000000000",
      rx_cntvaluein_42(8 downto 0) => B"000000000",
      rx_cntvaluein_43(8 downto 0) => B"000000000",
      rx_cntvaluein_44(8 downto 0) => B"000000000",
      rx_cntvaluein_45(8 downto 0) => B"000000000",
      rx_cntvaluein_46(8 downto 0) => B"000000000",
      rx_cntvaluein_47(8 downto 0) => B"000000000",
      rx_cntvaluein_48(8 downto 0) => B"000000000",
      rx_cntvaluein_49(8 downto 0) => B"000000000",
      rx_cntvaluein_5(8 downto 0) => B"000000000",
      rx_cntvaluein_50(8 downto 0) => B"000000000",
      rx_cntvaluein_51(8 downto 0) => B"000000000",
      rx_cntvaluein_6(8 downto 0) => B"000000000",
      rx_cntvaluein_7(8 downto 0) => B"000000000",
      rx_cntvaluein_8(8 downto 0) => B"000000000",
      rx_cntvaluein_9(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_0(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_1(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_10(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_11(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_12(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_13(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_14(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_15(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_16(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_17(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_18(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_19(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_2(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_20(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_21(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_22(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_23(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_24(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_25(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_26(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_27(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_28(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_29(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_3(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_30(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_31(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_32(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_33(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_34(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_35(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_36(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_37(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_38(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_39(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_4(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_40(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_41(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_42(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_43(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_44(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_45(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_46(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_47(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_48(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_49(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_5(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_50(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_51(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_6(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_7(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_8(8 downto 0) => B"000000000",
      rx_cntvaluein_ext_9(8 downto 0) => B"000000000",
      rx_cntvalueout_0(8 downto 0) => NLW_inst_rx_cntvalueout_0_UNCONNECTED(8 downto 0),
      rx_cntvalueout_1(8 downto 0) => NLW_inst_rx_cntvalueout_1_UNCONNECTED(8 downto 0),
      rx_cntvalueout_10(8 downto 0) => NLW_inst_rx_cntvalueout_10_UNCONNECTED(8 downto 0),
      rx_cntvalueout_11(8 downto 0) => NLW_inst_rx_cntvalueout_11_UNCONNECTED(8 downto 0),
      rx_cntvalueout_12(8 downto 0) => NLW_inst_rx_cntvalueout_12_UNCONNECTED(8 downto 0),
      rx_cntvalueout_13(8 downto 0) => NLW_inst_rx_cntvalueout_13_UNCONNECTED(8 downto 0),
      rx_cntvalueout_14(8 downto 0) => NLW_inst_rx_cntvalueout_14_UNCONNECTED(8 downto 0),
      rx_cntvalueout_15(8 downto 0) => NLW_inst_rx_cntvalueout_15_UNCONNECTED(8 downto 0),
      rx_cntvalueout_16(8 downto 0) => NLW_inst_rx_cntvalueout_16_UNCONNECTED(8 downto 0),
      rx_cntvalueout_17(8 downto 0) => NLW_inst_rx_cntvalueout_17_UNCONNECTED(8 downto 0),
      rx_cntvalueout_18(8 downto 0) => NLW_inst_rx_cntvalueout_18_UNCONNECTED(8 downto 0),
      rx_cntvalueout_19(8 downto 0) => NLW_inst_rx_cntvalueout_19_UNCONNECTED(8 downto 0),
      rx_cntvalueout_2(8 downto 0) => NLW_inst_rx_cntvalueout_2_UNCONNECTED(8 downto 0),
      rx_cntvalueout_20(8 downto 0) => NLW_inst_rx_cntvalueout_20_UNCONNECTED(8 downto 0),
      rx_cntvalueout_21(8 downto 0) => NLW_inst_rx_cntvalueout_21_UNCONNECTED(8 downto 0),
      rx_cntvalueout_22(8 downto 0) => NLW_inst_rx_cntvalueout_22_UNCONNECTED(8 downto 0),
      rx_cntvalueout_23(8 downto 0) => NLW_inst_rx_cntvalueout_23_UNCONNECTED(8 downto 0),
      rx_cntvalueout_24(8 downto 0) => NLW_inst_rx_cntvalueout_24_UNCONNECTED(8 downto 0),
      rx_cntvalueout_25(8 downto 0) => NLW_inst_rx_cntvalueout_25_UNCONNECTED(8 downto 0),
      rx_cntvalueout_26(8 downto 0) => NLW_inst_rx_cntvalueout_26_UNCONNECTED(8 downto 0),
      rx_cntvalueout_27(8 downto 0) => NLW_inst_rx_cntvalueout_27_UNCONNECTED(8 downto 0),
      rx_cntvalueout_28(8 downto 0) => NLW_inst_rx_cntvalueout_28_UNCONNECTED(8 downto 0),
      rx_cntvalueout_29(8 downto 0) => NLW_inst_rx_cntvalueout_29_UNCONNECTED(8 downto 0),
      rx_cntvalueout_3(8 downto 0) => NLW_inst_rx_cntvalueout_3_UNCONNECTED(8 downto 0),
      rx_cntvalueout_30(8 downto 0) => NLW_inst_rx_cntvalueout_30_UNCONNECTED(8 downto 0),
      rx_cntvalueout_31(8 downto 0) => NLW_inst_rx_cntvalueout_31_UNCONNECTED(8 downto 0),
      rx_cntvalueout_32(8 downto 0) => NLW_inst_rx_cntvalueout_32_UNCONNECTED(8 downto 0),
      rx_cntvalueout_33(8 downto 0) => NLW_inst_rx_cntvalueout_33_UNCONNECTED(8 downto 0),
      rx_cntvalueout_34(8 downto 0) => NLW_inst_rx_cntvalueout_34_UNCONNECTED(8 downto 0),
      rx_cntvalueout_35(8 downto 0) => NLW_inst_rx_cntvalueout_35_UNCONNECTED(8 downto 0),
      rx_cntvalueout_36(8 downto 0) => NLW_inst_rx_cntvalueout_36_UNCONNECTED(8 downto 0),
      rx_cntvalueout_37(8 downto 0) => NLW_inst_rx_cntvalueout_37_UNCONNECTED(8 downto 0),
      rx_cntvalueout_38(8 downto 0) => NLW_inst_rx_cntvalueout_38_UNCONNECTED(8 downto 0),
      rx_cntvalueout_39(8 downto 0) => NLW_inst_rx_cntvalueout_39_UNCONNECTED(8 downto 0),
      rx_cntvalueout_4(8 downto 0) => NLW_inst_rx_cntvalueout_4_UNCONNECTED(8 downto 0),
      rx_cntvalueout_40(8 downto 0) => NLW_inst_rx_cntvalueout_40_UNCONNECTED(8 downto 0),
      rx_cntvalueout_41(8 downto 0) => NLW_inst_rx_cntvalueout_41_UNCONNECTED(8 downto 0),
      rx_cntvalueout_42(8 downto 0) => NLW_inst_rx_cntvalueout_42_UNCONNECTED(8 downto 0),
      rx_cntvalueout_43(8 downto 0) => NLW_inst_rx_cntvalueout_43_UNCONNECTED(8 downto 0),
      rx_cntvalueout_44(8 downto 0) => NLW_inst_rx_cntvalueout_44_UNCONNECTED(8 downto 0),
      rx_cntvalueout_45(8 downto 0) => NLW_inst_rx_cntvalueout_45_UNCONNECTED(8 downto 0),
      rx_cntvalueout_46(8 downto 0) => NLW_inst_rx_cntvalueout_46_UNCONNECTED(8 downto 0),
      rx_cntvalueout_47(8 downto 0) => NLW_inst_rx_cntvalueout_47_UNCONNECTED(8 downto 0),
      rx_cntvalueout_48(8 downto 0) => NLW_inst_rx_cntvalueout_48_UNCONNECTED(8 downto 0),
      rx_cntvalueout_49(8 downto 0) => NLW_inst_rx_cntvalueout_49_UNCONNECTED(8 downto 0),
      rx_cntvalueout_5(8 downto 0) => NLW_inst_rx_cntvalueout_5_UNCONNECTED(8 downto 0),
      rx_cntvalueout_50(8 downto 0) => NLW_inst_rx_cntvalueout_50_UNCONNECTED(8 downto 0),
      rx_cntvalueout_51(8 downto 0) => NLW_inst_rx_cntvalueout_51_UNCONNECTED(8 downto 0),
      rx_cntvalueout_6(8 downto 0) => NLW_inst_rx_cntvalueout_6_UNCONNECTED(8 downto 0),
      rx_cntvalueout_7(8 downto 0) => NLW_inst_rx_cntvalueout_7_UNCONNECTED(8 downto 0),
      rx_cntvalueout_8(8 downto 0) => NLW_inst_rx_cntvalueout_8_UNCONNECTED(8 downto 0),
      rx_cntvalueout_9(8 downto 0) => NLW_inst_rx_cntvalueout_9_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_0(8 downto 0) => NLW_inst_rx_cntvalueout_ext_0_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_1(8 downto 0) => NLW_inst_rx_cntvalueout_ext_1_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_10(8 downto 0) => NLW_inst_rx_cntvalueout_ext_10_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_11(8 downto 0) => NLW_inst_rx_cntvalueout_ext_11_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_12(8 downto 0) => NLW_inst_rx_cntvalueout_ext_12_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_13(8 downto 0) => NLW_inst_rx_cntvalueout_ext_13_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_14(8 downto 0) => NLW_inst_rx_cntvalueout_ext_14_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_15(8 downto 0) => NLW_inst_rx_cntvalueout_ext_15_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_16(8 downto 0) => NLW_inst_rx_cntvalueout_ext_16_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_17(8 downto 0) => NLW_inst_rx_cntvalueout_ext_17_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_18(8 downto 0) => NLW_inst_rx_cntvalueout_ext_18_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_19(8 downto 0) => NLW_inst_rx_cntvalueout_ext_19_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_2(8 downto 0) => NLW_inst_rx_cntvalueout_ext_2_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_20(8 downto 0) => NLW_inst_rx_cntvalueout_ext_20_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_21(8 downto 0) => NLW_inst_rx_cntvalueout_ext_21_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_22(8 downto 0) => NLW_inst_rx_cntvalueout_ext_22_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_23(8 downto 0) => NLW_inst_rx_cntvalueout_ext_23_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_24(8 downto 0) => NLW_inst_rx_cntvalueout_ext_24_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_25(8 downto 0) => NLW_inst_rx_cntvalueout_ext_25_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_26(8 downto 0) => NLW_inst_rx_cntvalueout_ext_26_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_27(8 downto 0) => NLW_inst_rx_cntvalueout_ext_27_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_28(8 downto 0) => NLW_inst_rx_cntvalueout_ext_28_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_29(8 downto 0) => NLW_inst_rx_cntvalueout_ext_29_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_3(8 downto 0) => NLW_inst_rx_cntvalueout_ext_3_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_30(8 downto 0) => NLW_inst_rx_cntvalueout_ext_30_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_31(8 downto 0) => NLW_inst_rx_cntvalueout_ext_31_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_32(8 downto 0) => NLW_inst_rx_cntvalueout_ext_32_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_33(8 downto 0) => NLW_inst_rx_cntvalueout_ext_33_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_34(8 downto 0) => NLW_inst_rx_cntvalueout_ext_34_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_35(8 downto 0) => NLW_inst_rx_cntvalueout_ext_35_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_36(8 downto 0) => NLW_inst_rx_cntvalueout_ext_36_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_37(8 downto 0) => NLW_inst_rx_cntvalueout_ext_37_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_38(8 downto 0) => NLW_inst_rx_cntvalueout_ext_38_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_39(8 downto 0) => NLW_inst_rx_cntvalueout_ext_39_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_4(8 downto 0) => NLW_inst_rx_cntvalueout_ext_4_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_40(8 downto 0) => NLW_inst_rx_cntvalueout_ext_40_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_41(8 downto 0) => NLW_inst_rx_cntvalueout_ext_41_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_42(8 downto 0) => NLW_inst_rx_cntvalueout_ext_42_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_43(8 downto 0) => NLW_inst_rx_cntvalueout_ext_43_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_44(8 downto 0) => NLW_inst_rx_cntvalueout_ext_44_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_45(8 downto 0) => NLW_inst_rx_cntvalueout_ext_45_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_46(8 downto 0) => NLW_inst_rx_cntvalueout_ext_46_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_47(8 downto 0) => NLW_inst_rx_cntvalueout_ext_47_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_48(8 downto 0) => NLW_inst_rx_cntvalueout_ext_48_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_49(8 downto 0) => NLW_inst_rx_cntvalueout_ext_49_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_5(8 downto 0) => NLW_inst_rx_cntvalueout_ext_5_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_50(8 downto 0) => NLW_inst_rx_cntvalueout_ext_50_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_51(8 downto 0) => NLW_inst_rx_cntvalueout_ext_51_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_6(8 downto 0) => NLW_inst_rx_cntvalueout_ext_6_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_7(8 downto 0) => NLW_inst_rx_cntvalueout_ext_7_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_8(8 downto 0) => NLW_inst_rx_cntvalueout_ext_8_UNCONNECTED(8 downto 0),
      rx_cntvalueout_ext_9(8 downto 0) => NLW_inst_rx_cntvalueout_ext_9_UNCONNECTED(8 downto 0),
      rx_en_vtc_0 => '0',
      rx_en_vtc_1 => '0',
      rx_en_vtc_10 => '0',
      rx_en_vtc_11 => '0',
      rx_en_vtc_12 => '0',
      rx_en_vtc_13 => '0',
      rx_en_vtc_14 => '0',
      rx_en_vtc_15 => '0',
      rx_en_vtc_16 => '0',
      rx_en_vtc_17 => '0',
      rx_en_vtc_18 => '0',
      rx_en_vtc_19 => '0',
      rx_en_vtc_2 => '0',
      rx_en_vtc_20 => '0',
      rx_en_vtc_21 => '0',
      rx_en_vtc_22 => '0',
      rx_en_vtc_23 => '0',
      rx_en_vtc_24 => '0',
      rx_en_vtc_25 => '0',
      rx_en_vtc_26 => '0',
      rx_en_vtc_27 => '0',
      rx_en_vtc_28 => '0',
      rx_en_vtc_29 => '0',
      rx_en_vtc_3 => '0',
      rx_en_vtc_30 => '0',
      rx_en_vtc_31 => '0',
      rx_en_vtc_32 => '0',
      rx_en_vtc_33 => '0',
      rx_en_vtc_34 => '0',
      rx_en_vtc_35 => '0',
      rx_en_vtc_36 => '0',
      rx_en_vtc_37 => '0',
      rx_en_vtc_38 => '0',
      rx_en_vtc_39 => '0',
      rx_en_vtc_4 => '0',
      rx_en_vtc_40 => '0',
      rx_en_vtc_41 => '0',
      rx_en_vtc_42 => '0',
      rx_en_vtc_43 => '0',
      rx_en_vtc_44 => '0',
      rx_en_vtc_45 => '0',
      rx_en_vtc_46 => '0',
      rx_en_vtc_47 => '0',
      rx_en_vtc_48 => '0',
      rx_en_vtc_49 => '0',
      rx_en_vtc_5 => '0',
      rx_en_vtc_50 => '0',
      rx_en_vtc_51 => '0',
      rx_en_vtc_6 => '0',
      rx_en_vtc_7 => '0',
      rx_en_vtc_8 => '0',
      rx_en_vtc_9 => '0',
      rx_en_vtc_ext_0 => '0',
      rx_en_vtc_ext_1 => '0',
      rx_en_vtc_ext_10 => '0',
      rx_en_vtc_ext_11 => '0',
      rx_en_vtc_ext_12 => '0',
      rx_en_vtc_ext_13 => '0',
      rx_en_vtc_ext_14 => '0',
      rx_en_vtc_ext_15 => '0',
      rx_en_vtc_ext_16 => '0',
      rx_en_vtc_ext_17 => '0',
      rx_en_vtc_ext_18 => '0',
      rx_en_vtc_ext_19 => '0',
      rx_en_vtc_ext_2 => '0',
      rx_en_vtc_ext_20 => '0',
      rx_en_vtc_ext_21 => '0',
      rx_en_vtc_ext_22 => '0',
      rx_en_vtc_ext_23 => '0',
      rx_en_vtc_ext_24 => '0',
      rx_en_vtc_ext_25 => '0',
      rx_en_vtc_ext_26 => '0',
      rx_en_vtc_ext_27 => '0',
      rx_en_vtc_ext_28 => '0',
      rx_en_vtc_ext_29 => '0',
      rx_en_vtc_ext_3 => '0',
      rx_en_vtc_ext_30 => '0',
      rx_en_vtc_ext_31 => '0',
      rx_en_vtc_ext_32 => '0',
      rx_en_vtc_ext_33 => '0',
      rx_en_vtc_ext_34 => '0',
      rx_en_vtc_ext_35 => '0',
      rx_en_vtc_ext_36 => '0',
      rx_en_vtc_ext_37 => '0',
      rx_en_vtc_ext_38 => '0',
      rx_en_vtc_ext_39 => '0',
      rx_en_vtc_ext_4 => '0',
      rx_en_vtc_ext_40 => '0',
      rx_en_vtc_ext_41 => '0',
      rx_en_vtc_ext_42 => '0',
      rx_en_vtc_ext_43 => '0',
      rx_en_vtc_ext_44 => '0',
      rx_en_vtc_ext_45 => '0',
      rx_en_vtc_ext_46 => '0',
      rx_en_vtc_ext_47 => '0',
      rx_en_vtc_ext_48 => '0',
      rx_en_vtc_ext_49 => '0',
      rx_en_vtc_ext_5 => '0',
      rx_en_vtc_ext_50 => '0',
      rx_en_vtc_ext_51 => '0',
      rx_en_vtc_ext_6 => '0',
      rx_en_vtc_ext_7 => '0',
      rx_en_vtc_ext_8 => '0',
      rx_en_vtc_ext_9 => '0',
      rx_inc_0 => '0',
      rx_inc_1 => '0',
      rx_inc_10 => '0',
      rx_inc_11 => '0',
      rx_inc_12 => '0',
      rx_inc_13 => '0',
      rx_inc_14 => '0',
      rx_inc_15 => '0',
      rx_inc_16 => '0',
      rx_inc_17 => '0',
      rx_inc_18 => '0',
      rx_inc_19 => '0',
      rx_inc_2 => '0',
      rx_inc_20 => '0',
      rx_inc_21 => '0',
      rx_inc_22 => '0',
      rx_inc_23 => '0',
      rx_inc_24 => '0',
      rx_inc_25 => '0',
      rx_inc_26 => '0',
      rx_inc_27 => '0',
      rx_inc_28 => '0',
      rx_inc_29 => '0',
      rx_inc_3 => '0',
      rx_inc_30 => '0',
      rx_inc_31 => '0',
      rx_inc_32 => '0',
      rx_inc_33 => '0',
      rx_inc_34 => '0',
      rx_inc_35 => '0',
      rx_inc_36 => '0',
      rx_inc_37 => '0',
      rx_inc_38 => '0',
      rx_inc_39 => '0',
      rx_inc_4 => '0',
      rx_inc_40 => '0',
      rx_inc_41 => '0',
      rx_inc_42 => '0',
      rx_inc_43 => '0',
      rx_inc_44 => '0',
      rx_inc_45 => '0',
      rx_inc_46 => '0',
      rx_inc_47 => '0',
      rx_inc_48 => '0',
      rx_inc_49 => '0',
      rx_inc_5 => '0',
      rx_inc_50 => '0',
      rx_inc_51 => '0',
      rx_inc_6 => '0',
      rx_inc_7 => '0',
      rx_inc_8 => '0',
      rx_inc_9 => '0',
      rx_inc_ext_0 => '0',
      rx_inc_ext_1 => '0',
      rx_inc_ext_10 => '0',
      rx_inc_ext_11 => '0',
      rx_inc_ext_12 => '0',
      rx_inc_ext_13 => '0',
      rx_inc_ext_14 => '0',
      rx_inc_ext_15 => '0',
      rx_inc_ext_16 => '0',
      rx_inc_ext_17 => '0',
      rx_inc_ext_18 => '0',
      rx_inc_ext_19 => '0',
      rx_inc_ext_2 => '0',
      rx_inc_ext_20 => '0',
      rx_inc_ext_21 => '0',
      rx_inc_ext_22 => '0',
      rx_inc_ext_23 => '0',
      rx_inc_ext_24 => '0',
      rx_inc_ext_25 => '0',
      rx_inc_ext_26 => '0',
      rx_inc_ext_27 => '0',
      rx_inc_ext_28 => '0',
      rx_inc_ext_29 => '0',
      rx_inc_ext_3 => '0',
      rx_inc_ext_30 => '0',
      rx_inc_ext_31 => '0',
      rx_inc_ext_32 => '0',
      rx_inc_ext_33 => '0',
      rx_inc_ext_34 => '0',
      rx_inc_ext_35 => '0',
      rx_inc_ext_36 => '0',
      rx_inc_ext_37 => '0',
      rx_inc_ext_38 => '0',
      rx_inc_ext_39 => '0',
      rx_inc_ext_4 => '0',
      rx_inc_ext_40 => '0',
      rx_inc_ext_41 => '0',
      rx_inc_ext_42 => '0',
      rx_inc_ext_43 => '0',
      rx_inc_ext_44 => '0',
      rx_inc_ext_45 => '0',
      rx_inc_ext_46 => '0',
      rx_inc_ext_47 => '0',
      rx_inc_ext_48 => '0',
      rx_inc_ext_49 => '0',
      rx_inc_ext_5 => '0',
      rx_inc_ext_50 => '0',
      rx_inc_ext_51 => '0',
      rx_inc_ext_6 => '0',
      rx_inc_ext_7 => '0',
      rx_inc_ext_8 => '0',
      rx_inc_ext_9 => '0',
      rx_load_0 => '0',
      rx_load_1 => '0',
      rx_load_10 => '0',
      rx_load_11 => '0',
      rx_load_12 => '0',
      rx_load_13 => '0',
      rx_load_14 => '0',
      rx_load_15 => '0',
      rx_load_16 => '0',
      rx_load_17 => '0',
      rx_load_18 => '0',
      rx_load_19 => '0',
      rx_load_2 => '0',
      rx_load_20 => '0',
      rx_load_21 => '0',
      rx_load_22 => '0',
      rx_load_23 => '0',
      rx_load_24 => '0',
      rx_load_25 => '0',
      rx_load_26 => '0',
      rx_load_27 => '0',
      rx_load_28 => '0',
      rx_load_29 => '0',
      rx_load_3 => '0',
      rx_load_30 => '0',
      rx_load_31 => '0',
      rx_load_32 => '0',
      rx_load_33 => '0',
      rx_load_34 => '0',
      rx_load_35 => '0',
      rx_load_36 => '0',
      rx_load_37 => '0',
      rx_load_38 => '0',
      rx_load_39 => '0',
      rx_load_4 => '0',
      rx_load_40 => '0',
      rx_load_41 => '0',
      rx_load_42 => '0',
      rx_load_43 => '0',
      rx_load_44 => '0',
      rx_load_45 => '0',
      rx_load_46 => '0',
      rx_load_47 => '0',
      rx_load_48 => '0',
      rx_load_49 => '0',
      rx_load_5 => '0',
      rx_load_50 => '0',
      rx_load_51 => '0',
      rx_load_6 => '0',
      rx_load_7 => '0',
      rx_load_8 => '0',
      rx_load_9 => '0',
      rx_load_ext_0 => '0',
      rx_load_ext_1 => '0',
      rx_load_ext_10 => '0',
      rx_load_ext_11 => '0',
      rx_load_ext_12 => '0',
      rx_load_ext_13 => '0',
      rx_load_ext_14 => '0',
      rx_load_ext_15 => '0',
      rx_load_ext_16 => '0',
      rx_load_ext_17 => '0',
      rx_load_ext_18 => '0',
      rx_load_ext_19 => '0',
      rx_load_ext_2 => '0',
      rx_load_ext_20 => '0',
      rx_load_ext_21 => '0',
      rx_load_ext_22 => '0',
      rx_load_ext_23 => '0',
      rx_load_ext_24 => '0',
      rx_load_ext_25 => '0',
      rx_load_ext_26 => '0',
      rx_load_ext_27 => '0',
      rx_load_ext_28 => '0',
      rx_load_ext_29 => '0',
      rx_load_ext_3 => '0',
      rx_load_ext_30 => '0',
      rx_load_ext_31 => '0',
      rx_load_ext_32 => '0',
      rx_load_ext_33 => '0',
      rx_load_ext_34 => '0',
      rx_load_ext_35 => '0',
      rx_load_ext_36 => '0',
      rx_load_ext_37 => '0',
      rx_load_ext_38 => '0',
      rx_load_ext_39 => '0',
      rx_load_ext_4 => '0',
      rx_load_ext_40 => '0',
      rx_load_ext_41 => '0',
      rx_load_ext_42 => '0',
      rx_load_ext_43 => '0',
      rx_load_ext_44 => '0',
      rx_load_ext_45 => '0',
      rx_load_ext_46 => '0',
      rx_load_ext_47 => '0',
      rx_load_ext_48 => '0',
      rx_load_ext_49 => '0',
      rx_load_ext_5 => '0',
      rx_load_ext_50 => '0',
      rx_load_ext_51 => '0',
      rx_load_ext_6 => '0',
      rx_load_ext_7 => '0',
      rx_load_ext_8 => '0',
      rx_load_ext_9 => '0',
      rxtx_bitslip_sync_done => NLW_inst_rxtx_bitslip_sync_done_UNCONNECTED,
      shared_pll0_clkout0_in => '0',
      shared_pll0_clkoutphy_in => '0',
      shared_pll0_clkoutphy_out => shared_pll0_clkoutphy_out,
      shared_pll0_locked_in => '0',
      shared_pll1_clkout0_in => '0',
      shared_pll1_clkoutphy_in => '0',
      shared_pll1_clkoutphy_out => NLW_inst_shared_pll1_clkoutphy_out_UNCONNECTED,
      shared_pll1_locked_in => '0',
      start_bitslip => '0',
      tri_t_0 => '0',
      tri_t_1 => '0',
      tri_t_10 => '0',
      tri_t_11 => '0',
      tri_t_12 => '0',
      tri_t_13 => '0',
      tri_t_14 => '0',
      tri_t_15 => '0',
      tri_t_16 => '0',
      tri_t_17 => '0',
      tri_t_18 => '0',
      tri_t_19 => '0',
      tri_t_2 => '0',
      tri_t_20 => '0',
      tri_t_21 => '0',
      tri_t_22 => '0',
      tri_t_23 => '0',
      tri_t_24 => '0',
      tri_t_25 => '0',
      tri_t_26 => '0',
      tri_t_27 => '0',
      tri_t_28 => '0',
      tri_t_29 => '0',
      tri_t_3 => '0',
      tri_t_30 => '0',
      tri_t_31 => '0',
      tri_t_32 => '0',
      tri_t_33 => '0',
      tri_t_34 => '0',
      tri_t_35 => '0',
      tri_t_36 => '0',
      tri_t_37 => '0',
      tri_t_38 => '0',
      tri_t_39 => '0',
      tri_t_4 => '0',
      tri_t_40 => '0',
      tri_t_41 => '0',
      tri_t_42 => '0',
      tri_t_43 => '0',
      tri_t_44 => '0',
      tri_t_45 => '0',
      tri_t_46 => '0',
      tri_t_47 => '0',
      tri_t_48 => '0',
      tri_t_49 => '0',
      tri_t_5 => '0',
      tri_t_50 => '0',
      tri_t_51 => '0',
      tri_t_6 => '0',
      tri_t_7 => '0',
      tri_t_8 => '0',
      tri_t_9 => '0',
      tri_tbyte0(3 downto 0) => B"0000",
      tri_tbyte1(3 downto 0) => B"0000",
      tri_tbyte2(3 downto 0) => B"0000",
      tri_tbyte3(3 downto 0) => B"0000",
      tri_tbyte4(3 downto 0) => B"0000",
      tri_tbyte5(3 downto 0) => B"0000",
      tri_tbyte6(3 downto 0) => B"0000",
      tri_tbyte7(3 downto 0) => B"0000",
      tx_ce_0 => '0',
      tx_ce_1 => '0',
      tx_ce_10 => '0',
      tx_ce_11 => '0',
      tx_ce_12 => '0',
      tx_ce_13 => '0',
      tx_ce_14 => '0',
      tx_ce_15 => '0',
      tx_ce_16 => '0',
      tx_ce_17 => '0',
      tx_ce_18 => '0',
      tx_ce_19 => '0',
      tx_ce_2 => '0',
      tx_ce_20 => '0',
      tx_ce_21 => '0',
      tx_ce_22 => '0',
      tx_ce_23 => '0',
      tx_ce_24 => '0',
      tx_ce_25 => '0',
      tx_ce_26 => '0',
      tx_ce_27 => '0',
      tx_ce_28 => '0',
      tx_ce_29 => '0',
      tx_ce_3 => '0',
      tx_ce_30 => '0',
      tx_ce_31 => '0',
      tx_ce_32 => '0',
      tx_ce_33 => '0',
      tx_ce_34 => '0',
      tx_ce_35 => '0',
      tx_ce_36 => '0',
      tx_ce_37 => '0',
      tx_ce_38 => '0',
      tx_ce_39 => '0',
      tx_ce_4 => '0',
      tx_ce_40 => '0',
      tx_ce_41 => '0',
      tx_ce_42 => '0',
      tx_ce_43 => '0',
      tx_ce_44 => '0',
      tx_ce_45 => '0',
      tx_ce_46 => '0',
      tx_ce_47 => '0',
      tx_ce_48 => '0',
      tx_ce_49 => '0',
      tx_ce_5 => '0',
      tx_ce_50 => '0',
      tx_ce_51 => '0',
      tx_ce_6 => '0',
      tx_ce_7 => '0',
      tx_ce_8 => '0',
      tx_ce_9 => '0',
      tx_clk => '0',
      tx_cntvaluein_0(8 downto 0) => B"000000000",
      tx_cntvaluein_1(8 downto 0) => B"000000000",
      tx_cntvaluein_10(8 downto 0) => B"000000000",
      tx_cntvaluein_11(8 downto 0) => B"000000000",
      tx_cntvaluein_12(8 downto 0) => B"000000000",
      tx_cntvaluein_13(8 downto 0) => B"000000000",
      tx_cntvaluein_14(8 downto 0) => B"000000000",
      tx_cntvaluein_15(8 downto 0) => B"000000000",
      tx_cntvaluein_16(8 downto 0) => B"000000000",
      tx_cntvaluein_17(8 downto 0) => B"000000000",
      tx_cntvaluein_18(8 downto 0) => B"000000000",
      tx_cntvaluein_19(8 downto 0) => B"000000000",
      tx_cntvaluein_2(8 downto 0) => B"000000000",
      tx_cntvaluein_20(8 downto 0) => B"000000000",
      tx_cntvaluein_21(8 downto 0) => B"000000000",
      tx_cntvaluein_22(8 downto 0) => B"000000000",
      tx_cntvaluein_23(8 downto 0) => B"000000000",
      tx_cntvaluein_24(8 downto 0) => B"000000000",
      tx_cntvaluein_25(8 downto 0) => B"000000000",
      tx_cntvaluein_26(8 downto 0) => B"000000000",
      tx_cntvaluein_27(8 downto 0) => B"000000000",
      tx_cntvaluein_28(8 downto 0) => B"000000000",
      tx_cntvaluein_29(8 downto 0) => B"000000000",
      tx_cntvaluein_3(8 downto 0) => B"000000000",
      tx_cntvaluein_30(8 downto 0) => B"000000000",
      tx_cntvaluein_31(8 downto 0) => B"000000000",
      tx_cntvaluein_32(8 downto 0) => B"000000000",
      tx_cntvaluein_33(8 downto 0) => B"000000000",
      tx_cntvaluein_34(8 downto 0) => B"000000000",
      tx_cntvaluein_35(8 downto 0) => B"000000000",
      tx_cntvaluein_36(8 downto 0) => B"000000000",
      tx_cntvaluein_37(8 downto 0) => B"000000000",
      tx_cntvaluein_38(8 downto 0) => B"000000000",
      tx_cntvaluein_39(8 downto 0) => B"000000000",
      tx_cntvaluein_4(8 downto 0) => B"000000000",
      tx_cntvaluein_40(8 downto 0) => B"000000000",
      tx_cntvaluein_41(8 downto 0) => B"000000000",
      tx_cntvaluein_42(8 downto 0) => B"000000000",
      tx_cntvaluein_43(8 downto 0) => B"000000000",
      tx_cntvaluein_44(8 downto 0) => B"000000000",
      tx_cntvaluein_45(8 downto 0) => B"000000000",
      tx_cntvaluein_46(8 downto 0) => B"000000000",
      tx_cntvaluein_47(8 downto 0) => B"000000000",
      tx_cntvaluein_48(8 downto 0) => B"000000000",
      tx_cntvaluein_49(8 downto 0) => B"000000000",
      tx_cntvaluein_5(8 downto 0) => B"000000000",
      tx_cntvaluein_50(8 downto 0) => B"000000000",
      tx_cntvaluein_51(8 downto 0) => B"000000000",
      tx_cntvaluein_6(8 downto 0) => B"000000000",
      tx_cntvaluein_7(8 downto 0) => B"000000000",
      tx_cntvaluein_8(8 downto 0) => B"000000000",
      tx_cntvaluein_9(8 downto 0) => B"000000000",
      tx_cntvalueout_0(8 downto 0) => NLW_inst_tx_cntvalueout_0_UNCONNECTED(8 downto 0),
      tx_cntvalueout_1(8 downto 0) => NLW_inst_tx_cntvalueout_1_UNCONNECTED(8 downto 0),
      tx_cntvalueout_10(8 downto 0) => NLW_inst_tx_cntvalueout_10_UNCONNECTED(8 downto 0),
      tx_cntvalueout_11(8 downto 0) => NLW_inst_tx_cntvalueout_11_UNCONNECTED(8 downto 0),
      tx_cntvalueout_12(8 downto 0) => NLW_inst_tx_cntvalueout_12_UNCONNECTED(8 downto 0),
      tx_cntvalueout_13(8 downto 0) => NLW_inst_tx_cntvalueout_13_UNCONNECTED(8 downto 0),
      tx_cntvalueout_14(8 downto 0) => NLW_inst_tx_cntvalueout_14_UNCONNECTED(8 downto 0),
      tx_cntvalueout_15(8 downto 0) => NLW_inst_tx_cntvalueout_15_UNCONNECTED(8 downto 0),
      tx_cntvalueout_16(8 downto 0) => NLW_inst_tx_cntvalueout_16_UNCONNECTED(8 downto 0),
      tx_cntvalueout_17(8 downto 0) => NLW_inst_tx_cntvalueout_17_UNCONNECTED(8 downto 0),
      tx_cntvalueout_18(8 downto 0) => NLW_inst_tx_cntvalueout_18_UNCONNECTED(8 downto 0),
      tx_cntvalueout_19(8 downto 0) => NLW_inst_tx_cntvalueout_19_UNCONNECTED(8 downto 0),
      tx_cntvalueout_2(8 downto 0) => NLW_inst_tx_cntvalueout_2_UNCONNECTED(8 downto 0),
      tx_cntvalueout_20(8 downto 0) => NLW_inst_tx_cntvalueout_20_UNCONNECTED(8 downto 0),
      tx_cntvalueout_21(8 downto 0) => NLW_inst_tx_cntvalueout_21_UNCONNECTED(8 downto 0),
      tx_cntvalueout_22(8 downto 0) => NLW_inst_tx_cntvalueout_22_UNCONNECTED(8 downto 0),
      tx_cntvalueout_23(8 downto 0) => NLW_inst_tx_cntvalueout_23_UNCONNECTED(8 downto 0),
      tx_cntvalueout_24(8 downto 0) => NLW_inst_tx_cntvalueout_24_UNCONNECTED(8 downto 0),
      tx_cntvalueout_25(8 downto 0) => NLW_inst_tx_cntvalueout_25_UNCONNECTED(8 downto 0),
      tx_cntvalueout_26(8 downto 0) => NLW_inst_tx_cntvalueout_26_UNCONNECTED(8 downto 0),
      tx_cntvalueout_27(8 downto 0) => NLW_inst_tx_cntvalueout_27_UNCONNECTED(8 downto 0),
      tx_cntvalueout_28(8 downto 0) => NLW_inst_tx_cntvalueout_28_UNCONNECTED(8 downto 0),
      tx_cntvalueout_29(8 downto 0) => NLW_inst_tx_cntvalueout_29_UNCONNECTED(8 downto 0),
      tx_cntvalueout_3(8 downto 0) => NLW_inst_tx_cntvalueout_3_UNCONNECTED(8 downto 0),
      tx_cntvalueout_30(8 downto 0) => NLW_inst_tx_cntvalueout_30_UNCONNECTED(8 downto 0),
      tx_cntvalueout_31(8 downto 0) => NLW_inst_tx_cntvalueout_31_UNCONNECTED(8 downto 0),
      tx_cntvalueout_32(8 downto 0) => NLW_inst_tx_cntvalueout_32_UNCONNECTED(8 downto 0),
      tx_cntvalueout_33(8 downto 0) => NLW_inst_tx_cntvalueout_33_UNCONNECTED(8 downto 0),
      tx_cntvalueout_34(8 downto 0) => NLW_inst_tx_cntvalueout_34_UNCONNECTED(8 downto 0),
      tx_cntvalueout_35(8 downto 0) => NLW_inst_tx_cntvalueout_35_UNCONNECTED(8 downto 0),
      tx_cntvalueout_36(8 downto 0) => NLW_inst_tx_cntvalueout_36_UNCONNECTED(8 downto 0),
      tx_cntvalueout_37(8 downto 0) => NLW_inst_tx_cntvalueout_37_UNCONNECTED(8 downto 0),
      tx_cntvalueout_38(8 downto 0) => NLW_inst_tx_cntvalueout_38_UNCONNECTED(8 downto 0),
      tx_cntvalueout_39(8 downto 0) => NLW_inst_tx_cntvalueout_39_UNCONNECTED(8 downto 0),
      tx_cntvalueout_4(8 downto 0) => NLW_inst_tx_cntvalueout_4_UNCONNECTED(8 downto 0),
      tx_cntvalueout_40(8 downto 0) => NLW_inst_tx_cntvalueout_40_UNCONNECTED(8 downto 0),
      tx_cntvalueout_41(8 downto 0) => NLW_inst_tx_cntvalueout_41_UNCONNECTED(8 downto 0),
      tx_cntvalueout_42(8 downto 0) => NLW_inst_tx_cntvalueout_42_UNCONNECTED(8 downto 0),
      tx_cntvalueout_43(8 downto 0) => NLW_inst_tx_cntvalueout_43_UNCONNECTED(8 downto 0),
      tx_cntvalueout_44(8 downto 0) => NLW_inst_tx_cntvalueout_44_UNCONNECTED(8 downto 0),
      tx_cntvalueout_45(8 downto 0) => NLW_inst_tx_cntvalueout_45_UNCONNECTED(8 downto 0),
      tx_cntvalueout_46(8 downto 0) => NLW_inst_tx_cntvalueout_46_UNCONNECTED(8 downto 0),
      tx_cntvalueout_47(8 downto 0) => NLW_inst_tx_cntvalueout_47_UNCONNECTED(8 downto 0),
      tx_cntvalueout_48(8 downto 0) => NLW_inst_tx_cntvalueout_48_UNCONNECTED(8 downto 0),
      tx_cntvalueout_49(8 downto 0) => NLW_inst_tx_cntvalueout_49_UNCONNECTED(8 downto 0),
      tx_cntvalueout_5(8 downto 0) => NLW_inst_tx_cntvalueout_5_UNCONNECTED(8 downto 0),
      tx_cntvalueout_50(8 downto 0) => NLW_inst_tx_cntvalueout_50_UNCONNECTED(8 downto 0),
      tx_cntvalueout_51(8 downto 0) => NLW_inst_tx_cntvalueout_51_UNCONNECTED(8 downto 0),
      tx_cntvalueout_6(8 downto 0) => NLW_inst_tx_cntvalueout_6_UNCONNECTED(8 downto 0),
      tx_cntvalueout_7(8 downto 0) => NLW_inst_tx_cntvalueout_7_UNCONNECTED(8 downto 0),
      tx_cntvalueout_8(8 downto 0) => NLW_inst_tx_cntvalueout_8_UNCONNECTED(8 downto 0),
      tx_cntvalueout_9(8 downto 0) => NLW_inst_tx_cntvalueout_9_UNCONNECTED(8 downto 0),
      tx_en_vtc_0 => '0',
      tx_en_vtc_1 => '0',
      tx_en_vtc_10 => '0',
      tx_en_vtc_11 => '0',
      tx_en_vtc_12 => '0',
      tx_en_vtc_13 => '0',
      tx_en_vtc_14 => '0',
      tx_en_vtc_15 => '0',
      tx_en_vtc_16 => '0',
      tx_en_vtc_17 => '0',
      tx_en_vtc_18 => '0',
      tx_en_vtc_19 => '0',
      tx_en_vtc_2 => '0',
      tx_en_vtc_20 => '0',
      tx_en_vtc_21 => '0',
      tx_en_vtc_22 => '0',
      tx_en_vtc_23 => '0',
      tx_en_vtc_24 => '0',
      tx_en_vtc_25 => '0',
      tx_en_vtc_26 => '0',
      tx_en_vtc_27 => '0',
      tx_en_vtc_28 => '0',
      tx_en_vtc_29 => '0',
      tx_en_vtc_3 => '0',
      tx_en_vtc_30 => '0',
      tx_en_vtc_31 => '0',
      tx_en_vtc_32 => '0',
      tx_en_vtc_33 => '0',
      tx_en_vtc_34 => '0',
      tx_en_vtc_35 => '0',
      tx_en_vtc_36 => '0',
      tx_en_vtc_37 => '0',
      tx_en_vtc_38 => '0',
      tx_en_vtc_39 => '0',
      tx_en_vtc_4 => '0',
      tx_en_vtc_40 => '0',
      tx_en_vtc_41 => '0',
      tx_en_vtc_42 => '0',
      tx_en_vtc_43 => '0',
      tx_en_vtc_44 => '0',
      tx_en_vtc_45 => '0',
      tx_en_vtc_46 => '0',
      tx_en_vtc_47 => '0',
      tx_en_vtc_48 => '0',
      tx_en_vtc_49 => '0',
      tx_en_vtc_5 => '0',
      tx_en_vtc_50 => '0',
      tx_en_vtc_51 => '0',
      tx_en_vtc_6 => '0',
      tx_en_vtc_7 => '0',
      tx_en_vtc_8 => '0',
      tx_en_vtc_9 => '0',
      tx_inc_0 => '0',
      tx_inc_1 => '0',
      tx_inc_10 => '0',
      tx_inc_11 => '0',
      tx_inc_12 => '0',
      tx_inc_13 => '0',
      tx_inc_14 => '0',
      tx_inc_15 => '0',
      tx_inc_16 => '0',
      tx_inc_17 => '0',
      tx_inc_18 => '0',
      tx_inc_19 => '0',
      tx_inc_2 => '0',
      tx_inc_20 => '0',
      tx_inc_21 => '0',
      tx_inc_22 => '0',
      tx_inc_23 => '0',
      tx_inc_24 => '0',
      tx_inc_25 => '0',
      tx_inc_26 => '0',
      tx_inc_27 => '0',
      tx_inc_28 => '0',
      tx_inc_29 => '0',
      tx_inc_3 => '0',
      tx_inc_30 => '0',
      tx_inc_31 => '0',
      tx_inc_32 => '0',
      tx_inc_33 => '0',
      tx_inc_34 => '0',
      tx_inc_35 => '0',
      tx_inc_36 => '0',
      tx_inc_37 => '0',
      tx_inc_38 => '0',
      tx_inc_39 => '0',
      tx_inc_4 => '0',
      tx_inc_40 => '0',
      tx_inc_41 => '0',
      tx_inc_42 => '0',
      tx_inc_43 => '0',
      tx_inc_44 => '0',
      tx_inc_45 => '0',
      tx_inc_46 => '0',
      tx_inc_47 => '0',
      tx_inc_48 => '0',
      tx_inc_49 => '0',
      tx_inc_5 => '0',
      tx_inc_50 => '0',
      tx_inc_51 => '0',
      tx_inc_6 => '0',
      tx_inc_7 => '0',
      tx_inc_8 => '0',
      tx_inc_9 => '0',
      tx_load_0 => '0',
      tx_load_1 => '0',
      tx_load_10 => '0',
      tx_load_11 => '0',
      tx_load_12 => '0',
      tx_load_13 => '0',
      tx_load_14 => '0',
      tx_load_15 => '0',
      tx_load_16 => '0',
      tx_load_17 => '0',
      tx_load_18 => '0',
      tx_load_19 => '0',
      tx_load_2 => '0',
      tx_load_20 => '0',
      tx_load_21 => '0',
      tx_load_22 => '0',
      tx_load_23 => '0',
      tx_load_24 => '0',
      tx_load_25 => '0',
      tx_load_26 => '0',
      tx_load_27 => '0',
      tx_load_28 => '0',
      tx_load_29 => '0',
      tx_load_3 => '0',
      tx_load_30 => '0',
      tx_load_31 => '0',
      tx_load_32 => '0',
      tx_load_33 => '0',
      tx_load_34 => '0',
      tx_load_35 => '0',
      tx_load_36 => '0',
      tx_load_37 => '0',
      tx_load_38 => '0',
      tx_load_39 => '0',
      tx_load_4 => '0',
      tx_load_40 => '0',
      tx_load_41 => '0',
      tx_load_42 => '0',
      tx_load_43 => '0',
      tx_load_44 => '0',
      tx_load_45 => '0',
      tx_load_46 => '0',
      tx_load_47 => '0',
      tx_load_48 => '0',
      tx_load_49 => '0',
      tx_load_5 => '0',
      tx_load_50 => '0',
      tx_load_51 => '0',
      tx_load_6 => '0',
      tx_load_7 => '0',
      tx_load_8 => '0',
      tx_load_9 => '0',
      vtc_rdy_bsc0 => vtc_rdy_bsc0,
      vtc_rdy_bsc1 => vtc_rdy_bsc1,
      vtc_rdy_bsc2 => vtc_rdy_bsc2,
      vtc_rdy_bsc3 => vtc_rdy_bsc3,
      vtc_rdy_bsc4 => vtc_rdy_bsc4,
      vtc_rdy_bsc5 => vtc_rdy_bsc5,
      vtc_rdy_bsc6 => NLW_inst_vtc_rdy_bsc6_UNCONNECTED,
      vtc_rdy_bsc7 => NLW_inst_vtc_rdy_bsc7_UNCONNECTED
    );
end STRUCTURE;
