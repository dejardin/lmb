// (c) Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// (c) Copyright 2022-2024 Advanced Micro Devices, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of AMD and is protected under U.S. and international copyright
// and other intellectual property laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// AMD, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND AMD HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) AMD shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or AMD had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// AMD products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of AMD products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:ip:high_speed_selectio_wiz:3.6
// IP Revision: 9

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
high_speed_selectio_wiz_Bank_46 your_instance_name (
  .vtc_rdy_bsc0(vtc_rdy_bsc0),                                // output wire vtc_rdy_bsc0
  .en_vtc_bsc0(en_vtc_bsc0),                                  // input wire en_vtc_bsc0
  .vtc_rdy_bsc1(vtc_rdy_bsc1),                                // output wire vtc_rdy_bsc1
  .en_vtc_bsc1(en_vtc_bsc1),                                  // input wire en_vtc_bsc1
  .vtc_rdy_bsc2(vtc_rdy_bsc2),                                // output wire vtc_rdy_bsc2
  .en_vtc_bsc2(en_vtc_bsc2),                                  // input wire en_vtc_bsc2
  .vtc_rdy_bsc4(vtc_rdy_bsc4),                                // output wire vtc_rdy_bsc4
  .en_vtc_bsc4(en_vtc_bsc4),                                  // input wire en_vtc_bsc4
  .dly_rdy_bsc0(dly_rdy_bsc0),                                // output wire dly_rdy_bsc0
  .dly_rdy_bsc1(dly_rdy_bsc1),                                // output wire dly_rdy_bsc1
  .dly_rdy_bsc2(dly_rdy_bsc2),                                // output wire dly_rdy_bsc2
  .dly_rdy_bsc4(dly_rdy_bsc4),                                // output wire dly_rdy_bsc4
  .rst_seq_done(rst_seq_done),                                // output wire rst_seq_done
  .shared_pll0_clkoutphy_out(shared_pll0_clkoutphy_out),      // output wire shared_pll0_clkoutphy_out
  .pll0_clkout0(pll0_clkout0),                                // output wire pll0_clkout0
  .pll0_clkout1(pll0_clkout1),                                // output wire pll0_clkout1
  .rst(rst),                                                  // input wire rst
  .clk(clk),                                                  // input wire clk
  .pll0_locked(pll0_locked),                                  // output wire pll0_locked
  .ch1_out2_P(ch1_out2_P),                                    // output wire ch1_out2_P
  .data_from_fabric_ch1_out2_P(data_from_fabric_ch1_out2_P),  // input wire [7 : 0] data_from_fabric_ch1_out2_P
  .ch1_out2_N(ch1_out2_N),                                    // output wire ch1_out2_N
  .ch1_out0_P(ch1_out0_P),                                    // output wire ch1_out0_P
  .data_from_fabric_ch1_out0_P(data_from_fabric_ch1_out0_P),  // input wire [7 : 0] data_from_fabric_ch1_out0_P
  .ch1_out0_N(ch1_out0_N),                                    // output wire ch1_out0_N
  .ch1_out3_P(ch1_out3_P),                                    // output wire ch1_out3_P
  .data_from_fabric_ch1_out3_P(data_from_fabric_ch1_out3_P),  // input wire [7 : 0] data_from_fabric_ch1_out3_P
  .ch1_out3_N(ch1_out3_N),                                    // output wire ch1_out3_N
  .ch1_out5_P(ch1_out5_P),                                    // output wire ch1_out5_P
  .data_from_fabric_ch1_out5_P(data_from_fabric_ch1_out5_P),  // input wire [7 : 0] data_from_fabric_ch1_out5_P
  .ch1_out5_N(ch1_out5_N),                                    // output wire ch1_out5_N
  .ch1_out4_P(ch1_out4_P),                                    // output wire ch1_out4_P
  .data_from_fabric_ch1_out4_P(data_from_fabric_ch1_out4_P),  // input wire [7 : 0] data_from_fabric_ch1_out4_P
  .ch1_out4_N(ch1_out4_N),                                    // output wire ch1_out4_N
  .ch1_out1_P(ch1_out1_P),                                    // output wire ch1_out1_P
  .data_from_fabric_ch1_out1_P(data_from_fabric_ch1_out1_P),  // input wire [7 : 0] data_from_fabric_ch1_out1_P
  .ch1_out1_N(ch1_out1_N),                                    // output wire ch1_out1_N
  .ch1_out6_P(ch1_out6_P),                                    // output wire ch1_out6_P
  .data_from_fabric_ch1_out6_P(data_from_fabric_ch1_out6_P),  // input wire [7 : 0] data_from_fabric_ch1_out6_P
  .ch1_out6_N(ch1_out6_N),                                    // output wire ch1_out6_N
  .ch2_out1_P(ch2_out1_P),                                    // output wire ch2_out1_P
  .data_from_fabric_ch2_out1_P(data_from_fabric_ch2_out1_P),  // input wire [7 : 0] data_from_fabric_ch2_out1_P
  .ch2_out1_N(ch2_out1_N),                                    // output wire ch2_out1_N
  .ch2_out4_P(ch2_out4_P),                                    // output wire ch2_out4_P
  .data_from_fabric_ch2_out4_P(data_from_fabric_ch2_out4_P),  // input wire [7 : 0] data_from_fabric_ch2_out4_P
  .ch2_out4_N(ch2_out4_N)                                    // output wire ch2_out4_N
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file high_speed_selectio_wiz_Bank_46.v when simulating
// the core, high_speed_selectio_wiz_Bank_46. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

