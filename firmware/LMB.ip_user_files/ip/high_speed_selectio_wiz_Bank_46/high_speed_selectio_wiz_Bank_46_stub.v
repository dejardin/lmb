// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
// Date        : Thu Aug 22 13:10:40 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode synth_stub
//               /data/cms/ecal/fe/LMB/firmware/LMB.runs/high_speed_selectio_wiz_Bank_46_synth_1/high_speed_selectio_wiz_Bank_46_stub.v
// Design      : high_speed_selectio_wiz_Bank_46
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "high_speed_selectio_wiz_Bank_46_high_speed_selectio_wiz_v3_6_9,Vivado 2024.1.1" *)
module high_speed_selectio_wiz_Bank_46(vtc_rdy_bsc0, en_vtc_bsc0, vtc_rdy_bsc1, 
  en_vtc_bsc1, vtc_rdy_bsc2, en_vtc_bsc2, vtc_rdy_bsc4, en_vtc_bsc4, dly_rdy_bsc0, 
  dly_rdy_bsc1, dly_rdy_bsc2, dly_rdy_bsc4, rst_seq_done, shared_pll0_clkoutphy_out, 
  pll0_clkout0, pll0_clkout1, rst, clk, pll0_locked, ch1_out2_P, data_from_fabric_ch1_out2_P, 
  ch1_out2_N, ch1_out0_P, data_from_fabric_ch1_out0_P, ch1_out0_N, ch1_out3_P, 
  data_from_fabric_ch1_out3_P, ch1_out3_N, ch1_out5_P, data_from_fabric_ch1_out5_P, 
  ch1_out5_N, ch1_out4_P, data_from_fabric_ch1_out4_P, ch1_out4_N, ch1_out1_P, 
  data_from_fabric_ch1_out1_P, ch1_out1_N, ch1_out6_P, data_from_fabric_ch1_out6_P, 
  ch1_out6_N, ch2_out1_P, data_from_fabric_ch2_out1_P, ch2_out1_N, ch2_out4_P, 
  data_from_fabric_ch2_out4_P, ch2_out4_N)
/* synthesis syn_black_box black_box_pad_pin="vtc_rdy_bsc0,en_vtc_bsc0,vtc_rdy_bsc1,en_vtc_bsc1,vtc_rdy_bsc2,en_vtc_bsc2,vtc_rdy_bsc4,en_vtc_bsc4,dly_rdy_bsc0,dly_rdy_bsc1,dly_rdy_bsc2,dly_rdy_bsc4,rst_seq_done,rst,clk,pll0_locked,ch1_out2_P,data_from_fabric_ch1_out2_P[7:0],ch1_out2_N,ch1_out0_P,data_from_fabric_ch1_out0_P[7:0],ch1_out0_N,ch1_out3_P,data_from_fabric_ch1_out3_P[7:0],ch1_out3_N,ch1_out5_P,data_from_fabric_ch1_out5_P[7:0],ch1_out5_N,ch1_out4_P,data_from_fabric_ch1_out4_P[7:0],ch1_out4_N,ch1_out1_P,data_from_fabric_ch1_out1_P[7:0],ch1_out1_N,ch1_out6_P,data_from_fabric_ch1_out6_P[7:0],ch1_out6_N,ch2_out1_P,data_from_fabric_ch2_out1_P[7:0],ch2_out1_N,ch2_out4_P,data_from_fabric_ch2_out4_P[7:0],ch2_out4_N" */
/* synthesis syn_force_seq_prim="shared_pll0_clkoutphy_out" */
/* synthesis syn_force_seq_prim="pll0_clkout0" */
/* synthesis syn_force_seq_prim="pll0_clkout1" */;
  output vtc_rdy_bsc0;
  input en_vtc_bsc0;
  output vtc_rdy_bsc1;
  input en_vtc_bsc1;
  output vtc_rdy_bsc2;
  input en_vtc_bsc2;
  output vtc_rdy_bsc4;
  input en_vtc_bsc4;
  output dly_rdy_bsc0;
  output dly_rdy_bsc1;
  output dly_rdy_bsc2;
  output dly_rdy_bsc4;
  output rst_seq_done;
  output shared_pll0_clkoutphy_out /* synthesis syn_isclock = 1 */;
  output pll0_clkout0 /* synthesis syn_isclock = 1 */;
  output pll0_clkout1 /* synthesis syn_isclock = 1 */;
  input rst;
  input clk;
  output pll0_locked;
  output ch1_out2_P;
  input [7:0]data_from_fabric_ch1_out2_P;
  output ch1_out2_N;
  output ch1_out0_P;
  input [7:0]data_from_fabric_ch1_out0_P;
  output ch1_out0_N;
  output ch1_out3_P;
  input [7:0]data_from_fabric_ch1_out3_P;
  output ch1_out3_N;
  output ch1_out5_P;
  input [7:0]data_from_fabric_ch1_out5_P;
  output ch1_out5_N;
  output ch1_out4_P;
  input [7:0]data_from_fabric_ch1_out4_P;
  output ch1_out4_N;
  output ch1_out1_P;
  input [7:0]data_from_fabric_ch1_out1_P;
  output ch1_out1_N;
  output ch1_out6_P;
  input [7:0]data_from_fabric_ch1_out6_P;
  output ch1_out6_N;
  output ch2_out1_P;
  input [7:0]data_from_fabric_ch2_out1_P;
  output ch2_out1_N;
  output ch2_out4_P;
  input [7:0]data_from_fabric_ch2_out4_P;
  output ch2_out4_N;
endmodule
