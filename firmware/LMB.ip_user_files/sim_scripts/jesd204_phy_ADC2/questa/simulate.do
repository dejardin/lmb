onbreak {quit -f}
onerror {quit -f}

vsim  -lib xil_defaultlib jesd204_phy_ADC2_opt

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure
view signals

do {jesd204_phy_ADC2.udo}

run 1000ns

quit -force
