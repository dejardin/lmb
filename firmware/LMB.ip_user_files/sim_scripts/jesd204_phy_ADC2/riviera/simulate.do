transcript off
onbreak {quit -force}
onerror {quit -force}
transcript on

asim +access +r +m+jesd204_phy_ADC2  -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.jesd204_phy_ADC2 xil_defaultlib.glbl

do {jesd204_phy_ADC2.udo}

run 1000ns

endsim

quit -force
