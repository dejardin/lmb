onbreak {quit -f}
onerror {quit -f}

vsim  -lib xil_defaultlib high_speed_selectio_wiz_Bank_46_opt

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure
view signals

do {high_speed_selectio_wiz_Bank_46.udo}

run 1000ns

quit -force
