transcript off
onbreak {quit -force}
onerror {quit -force}
transcript on

asim +access +r +m+FIFO_mem_gen  -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O2 xil_defaultlib.FIFO_mem_gen xil_defaultlib.glbl

do {FIFO_mem_gen.udo}

run

endsim

quit -force
