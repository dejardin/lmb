transcript off
onbreak {quit -force}
onerror {quit -force}
transcript on

asim +access +r +m+clk_generator_main  -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O2 xil_defaultlib.clk_generator_main xil_defaultlib.glbl

do {clk_generator_main.udo}

run

endsim

quit -force
