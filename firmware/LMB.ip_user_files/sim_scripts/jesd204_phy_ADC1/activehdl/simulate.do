transcript off
onbreak {quit -force}
onerror {quit -force}
transcript on

asim +access +r +m+jesd204_phy_ADC1  -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O2 xil_defaultlib.jesd204_phy_ADC1 xil_defaultlib.glbl

do {jesd204_phy_ADC1.udo}

run

endsim

quit -force
