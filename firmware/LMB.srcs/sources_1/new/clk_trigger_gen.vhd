----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/03/2024 04:12:00 PM
-- Design Name: 
-- Module Name: clk_trigger_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_trigger_gen is
Port (
  clk_640_out      : out  std_logic;
  clk_320_out      : out  std_logic;
  clk_160_out      : out  std_logic;
  reset            : in   std_logic;
  locked           : out  std_logic;
  clk_160_in       : in   std_logic
);
    
end entity clk_trigger_gen;

architecture rtl of clk_trigger_gen is

signal clkfbout_loc : std_logic;
signal clk_640_loc  : std_logic;
signal clk_320_loc  : std_logic;
signal clk_160_loc  : std_logic;

begin

clk_640_out <= clk_640_loc;
clk_320_out <= clk_320_loc;
clk_160_out <= clk_160_loc;

MMCME3_ADV_inst : MMCME3_ADV
generic map (
   BANDWIDTH            => "OPTIMIZED", -- Jitter programming (HIGH, LOW, OPTIMIZED)
   CLKOUT4_CASCADE      => "FALSE",
   COMPENSATION         => "AUTO",      -- AUTO, BUF_IN, EXTERNAL, INTERNAL, ZHOLD
   STARTUP_WAIT         => "FALSE",     -- Delays DONE until MMCM is locked (FALSE, TRUE)
   DIVCLK_DIVIDE        => 1,           -- Master division value (1-106)
   CLKFBOUT_MULT_F      => 4.0,         -- Multiply value for all CLKOUT (2.000-64.000)
   CLKFBOUT_PHASE       => 0.0,         -- Phase offset in degrees of CLKFB (-360.000-360.000)
   CLKFBOUT_USE_FINE_PS => "FALSE",
   CLKOUT0_DIVIDE_F     => 1.0,         -- Divide amount for CLKOUT0 (1.000-128.000)
   CLKOUT0_PHASE        => 0.0,
   CLKOUT0_DUTY_CYCLE   => 0.5,
   CLKOUT0_USE_FINE_PS  => "FALSE",
   CLKIN1_PERIOD        => 6.25
)
port map (
-- Clock Outputs outputs: User configurable clock outputs
   CLKOUT0      => clk_640_loc,  -- 1-bit output: CLKOUT0
   CLKOUT0B     => open,         -- 1-bit output: Inverted CLKOUT0.
   CLKOUT1      => open,         -- 1-bit output: Primary clock
   CLKOUT1B     => open,         -- 1-bit output: Inverted CLKOUT1
   CLKOUT2      => open,         -- 1-bit output: CLKOUT2
   CLKOUT2B     => open,         -- 1-bit output: Inverted CLKOUT2
   CLKOUT3      => open,         -- 1-bit output: CLKOUT3
   CLKOUT3B     => open,         -- 1-bit output: Inverted CLKOUT3
   CLKOUT4      => open,         -- 1-bit output: CLKOUT4
   CLKOUT5      => open,         -- 1-bit output: CLKOUT5
   CLKOUT6      => open,         -- 1-bit output: CLKOUT6
-- DRP Ports outputs: Dynamic reconfiguration ports
   DO           => open,         -- 16-bit output: DRP data
   DRDY         => open,         -- 1-bit output: DRP ready
-- Dynamic Phase Shift Ports outputs: Ports used for dynamic phase shifting of the outputs
   PSDONE       => open,         -- 1-bit output: Phase shift done
-- Feedback outputs: Clock feedback ports
   CLKFBOUT     => clkfbout_loc, -- 1-bit output: Feedback clock
   CLKFBOUTB    => open,         -- 1-bit output: Inverted CLKFBOUT
-- Status Ports outputs: MMCM status ports
   CDDCDONE     => open,         -- 1-bit output: Clock dynamic divide done
   CLKFBSTOPPED => open,         -- 1-bit output: Feedback clock stopped
   CLKINSTOPPED => open,         -- 1-bit output: Input clock stopped
   LOCKED       => LOCKED,       -- 1-bit output: LOCK
   CDDCREQ      => '0',          -- 1-bit input: Request to dynamic divide clock
-- Clock Inputs inputs: Clock inputs
   CLKIN1       => clk_160_in,   -- 1-bit input: Primary clock
   CLKIN2       => '0',          -- 1-bit input: Secondary clock
-- Control Ports inputs: MMCM control ports
   CLKINSEL     => '1',          -- 1-bit input: Clock select, High=CLKIN1 Low=CLKIN2
   PWRDWN       => '0',          -- 1-bit input: Power-down
   RST          => reset,        -- 1-bit input: Reset
-- DRP Ports inputs: Dynamic reconfiguration ports
   DADDR        => "0000000",    -- 7-bit input: DRP address
   DCLK         => '0',          -- 1-bit input: DRP clock
   DEN          => '0',          -- 1-bit input: DRP enable
   DI           => x"0000",      -- 16-bit input: DRP data
   DWE          => '0',          -- 1-bit input: DRP write enable
-- Dynamic Phase Shift Ports inputs: Ports used for dynamic phase shifting of the outputs
   PSCLK        => '0',          -- 1-bit input: Phase shift clock
   PSEN         => '0',          -- 1-bit input: Phase shift enable
   PSINCDEC     => '0',          -- 1-bit input: Phase shift increment/decrement
-- Feedback inputs: Clock feedback ports
   CLKFBIN      => clkfbout_loc  -- 1-bit input: Feedback clock
);

BUFGCE_div_4_inst : BUFGCE_DIV
generic map(
  BUFGCE_DIVIDE => 4,
-- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
  IS_CE_INVERTED => '0',-- Optional inversion for CE
  IS_CLR_INVERTED => '0', -- Optional inversion for CLR
  IS_I_INVERTED => '0'    -- Optional inversion for I
)
port map(
  O   => clk_160_loc, -- 1-bit output: Buffer
  CE  => '1',         -- 1-bit input: Buffer enable
  CLR => '0',         -- 1-bit input: Asynchronous clear
  I   => clk_640_loc  -- 1-bit input: Buffer
);

BUFGCE_div_2_inst : BUFGCE_DIV
generic map(
  BUFGCE_DIVIDE => 2,
-- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
  IS_CE_INVERTED => '0',-- Optional inversion for CE
  IS_CLR_INVERTED => '0', -- Optional inversion for CLR
  IS_I_INVERTED => '0'    -- Optional inversion for I
)
port map(
  O   => clk_320_loc, -- 1-bit output: Buffer
  CE  => '1',         -- 1-bit input: Buffer enable
  CLR => '0',         -- 1-bit input: Asynchronous clear
  I   => clk_640_loc  -- 1-bit input: Buffer
);
end architecture rtl;