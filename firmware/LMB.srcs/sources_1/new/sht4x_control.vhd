----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/24/2025 02:35:17 PM
-- Design Name: 
-- Module Name: sht4x_control - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity sht4x_control is
  generic (
    I2C_address       : std_logic_vector(6 downto 0) := "1000100"  -- Build-in address for the sth4x sensor
  );
  Port (
    reset       : in    std_logic;
    timer_off   : in    std_logic;
    TH_extra    : in    std_logic;
    I2C_command : in    std_logic_vector (7 downto 0);
    temperature : out   std_logic_vector (15 downto 0);
    humidity    : out   std_logic_vector (15 downto 0);
    I2C_scl     : inout std_logic;
    I2C_sda     : inout std_logic;
    I2C_clk     : in    std_logic;
    I2C_error   : out   std_logic;
    SHT_busy    : out   std_logic);
end sht4x_control;

architecture rtl of sht4x_control is
signal TH_extra_latched       : std_logic := '0';
signal I2C_command_latched    : std_logic_vector(7 downto 0);
signal I2C_address_loc        : std_logic_vector(6 downto 0);
signal I2C_command_loc        : std_logic_vector(7 downto 0);
signal I2C_last_transfer      : std_logic := '1';              -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
signal I2C_sda_loc            : std_logic;                     -- 
signal I2C_scl_loc            : std_logic;                     -- 
signal I2C_ena                : std_logic;                     --
signal I2C_access             : std_logic := '0';
signal I2C_ena_del            : std_logic;
signal I2C_R_Wb               : std_logic;                     -- '0' is write, '1' is read
signal I2C_data_w             : std_logic_vector(7 downto 0);  -- data to write to slave
signal I2C_data_r             : std_logic_vector(47 downto 0); -- data read from slave
signal I2C_loc_busy           : std_logic;                     -- indicates transaction in progress
signal I2C_loc_busy_prev      : std_logic;                     -- mandatory to detect busy transiton
signal I2C_loc_data_r         : std_logic_vector(7 downto 0);  -- data read from slave
signal I2C_ack_error          : std_logic;                     -- flag if improper acknowledge from slave
signal I2C_slave_ack          : std_logic;                     -- flag if improper acknowledge from slave
signal loc_I2C_n_ack          : unsigned(7 downto 0)          := (others => '0');
signal I2C_max_bytes          : natural range 0 to 7 := 6;
signal I2C_n_bytes            : natural range 0 to 7 := 0;
signal conversion_delay       : unsigned(23 downto 0) := (others => '0');
signal timer_counter          : unsigned(27 downto 0) := (others => '0');
signal start_measurement      : std_logic :='0';               -- start conversion every second
signal resetb                 : std_logic;

type   I2C_state_type is (idle,
                          write_command,   wait_for_busy_command,
                          wait_conversion, read_reg_data);
signal I2C_state     : I2C_state_type := idle;

begin

  I2C_error         <= I2C_ack_error;
  resetb            <= not reset;

  Inst_I2C_master : entity work.I2C_master
  port map(
    clk             => I2C_clk,             --system clock
    reset_n         => resetb,              --active low reset
    ena             => I2C_ena,             --latch in command
    addr            => I2C_address_loc,     --address of target slave (default = PLL) 
    R_Wb            => I2C_R_Wb,            --'0' is write, '1' is read
    data_w          => I2C_data_w,          --data to write to slave
    busy            => I2C_loc_busy,        --indicates transaction in progress
    data_r          => I2C_loc_data_r,      --data read from slave
    ack_error       => I2C_ack_error,       --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,       -- acknowledge from slave
    sda             => I2C_sda,             --serial data output of i2c bus
    scl             => I2C_scl,             --serial clock output of i2c bus
    scl_int         => I2C_scl_loc,         --internal serial clock output of i2c bus
    sda_int         => I2C_sda_loc          --internal serial data output of i2c bus
  );

-- Timer to read temp and humidity every second
  TH_timer : process(reset,timer_off, I2C_clk)
  begin
    if reset = '1' then
      start_measurement     <= '0';
      timer_counter         <= (others => '0');
      TH_extra_latched      <= '0';
      I2C_command_latched   <= (others => '0');
    elsif Rising_Edge(I2C_clk) then
      if TH_extra = '1' then      -- do we ask something else than what we use to get ?
        TH_extra_latched    <= TH_extra;                   -- then latch signals until the sensor is ready to receive them
        I2C_command_latched <= I2C_command;
      end if;
      if TH_extra_latched = '1' and I2C_state = idle then  -- Be sure that we launch an extra conevrsion when the sensor is idle
        timer_counter       <= (others => '0');
        I2C_command_loc     <= I2C_command_latched;
        TH_extra_latched    <= '0';
        I2C_command_latched <= (others => '0');
        I2C_access                   <= '1';
      elsif timer_counter = x"9896800" then -- 160 000 000 clocks
        timer_counter                <= (others => '0');
        if timer_off = '0' then
          I2C_command_loc            <= x"fd";             -- get humidity and temperature with high resolution
          I2C_access                 <= '1';
        end if;
      else
        timer_counter                <= timer_counter + 1;
        if timer_counter = x"4" then
          I2C_access                 <= '0';
        end if;
      end if;
    end if;
  end process TH_timer;

-- For sth4x sensor we have to read 6 bytes in a raw after the TH command
  program_I2c : process(reset, I2C_Access, I2C_clk, I2C_loc_busy)
  begin
    if reset = '1' then
      I2C_state                      <= idle;
      I2C_ena                        <= '0';
      I2C_loc_busy_prev              <= '0';
      I2C_max_bytes                  <= 6;
      I2C_n_bytes                    <= 0;
    elsif Rising_Edge(I2C_clk) then
      case I2C_state is
      when idle =>
        if I2C_loc_busy = '0' then
          SHT_busy                   <= '0';
        end if;
        I2C_ena                      <= '0';
        if I2C_Access = '1' then
          SHT_busy                   <= '1';                                  -- Stay busy during all the transaction
          I2C_address_loc            <= I2C_address;
          I2C_R_Wb                   <= '0';                                  -- Always read data from sensor
          I2C_state                  <= wait_for_busy_command;                    -- Synchronize with I2C clock (slower)
          I2C_n_bytes                <= 0;                                    -- Temp MSB, Temp LSB Temp CRC, Humi MSB, Humi lSB, Humi CRC
          I2C_R_Wb                   <= '0';                                  -- first write command for sensor
          I2C_data_w                 <= I2C_command_loc;                      -- Register address
          I2C_data_r                 <= (others => '0');
          I2C_ena                    <= '1';                                  -- Initiate the transaction (I2C master latch address and data)
        end if;
      when wait_for_busy_command =>                                           -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                  <= write_command;
        end if;
      when write_command =>                                                   -- Chip address and command have been latched. Prepare next transaction during first write
        I2C_R_Wb                     <= '1';                                  -- Prepare for next read
        I2C_ena                      <= '0';                                  -- Send a stop signal after command. Then wait for 10 ms
        if I2C_loc_busy = '0' then                                            -- Chip/reg adress write is finished
          I2C_state                  <= wait_conversion;
          conversion_delay           <= (others => '0');
        end if;
      when wait_conversion =>                                                 -- Wait for 10 ms before startingg reading data
        conversion_delay             <= conversion_delay+1;
        if conversion_delay = x"186a00" then                                  -- 1600000 cloks of 6.25 ns : 10 ms
          I2C_state                  <= read_reg_data;
          I2C_ena                    <= '1';
        end if;
      when read_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                    <= '0';                                  -- Deassert enable to stop transaction after this read
        else
          I2C_ena                    <= '1';                                  -- Reassert enable to continue transaction after this read
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                -- byte read finished
          I2C_data_r((I2C_n_bytes+1)*8-1 downto I2C_n_bytes*8)  <= I2C_loc_data_r;
          if I2C_n_bytes = I2C_max_bytes-1 then
            I2C_state                <= idle;                                 -- End of transaction 6 bytes read
            temperature              <= I2C_data_r(7 downto 0)  & I2C_data_r(15 downto 8);
            humidity                 <= I2C_data_r(31 downto 24)& I2C_data_r(39 downto 32);
          end if;
          I2C_n_bytes                <= I2C_n_bytes +1;
        end if;
      end case;
      I2C_loc_busy_prev <= I2C_loc_busy;
    end if;
  end process program_I2C;
  
end rtl;
