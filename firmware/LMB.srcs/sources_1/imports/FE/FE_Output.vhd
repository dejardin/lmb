library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

entity FE_Output is

  port (
    clk_80           : in  std_logic;
    clk_160          : in  std_logic;
    reset            : in  std_logic;
    ADC1_out0_P       : out std_logic;
    ADC1_out0_N       : out std_logic;
    ADC1_out1_P       : out std_logic;
    ADC1_out1_N       : out std_logic;
    ADC1_out2_P       : out std_logic;
    ADC1_out2_N       : out std_logic;
    ADC1_out3_P       : out std_logic;
    ADC1_out3_N       : out std_logic;
    ADC1_out4_P       : out std_logic;
    ADC1_out4_N       : out std_logic;
    ADC1_out5_P       : out std_logic;
    ADC1_out5_N       : out std_logic;
    ADC1_out6_P       : out std_logic;
    ADC1_out6_N       : out std_logic;
    ADC1_out7_P       : out std_logic;
    ADC1_out7_N       : out std_logic;
    ADC2_out0_P       : out std_logic;
    ADC2_out0_N       : out std_logic;
    ADC2_out1_P       : out std_logic;
    ADC2_out1_N       : out std_logic;
    ADC2_out2_P       : out std_logic;
    ADC2_out2_N       : out std_logic;
    ADC2_out3_P       : out std_logic;
    ADC2_out3_N       : out std_logic;
    ADC2_out4_P       : out std_logic;
    ADC2_out4_N       : out std_logic;
    ADC2_out5_P       : out std_logic;
    ADC2_out5_N       : out std_logic;
    ADC2_out6_P       : out std_logic;
    ADC2_out6_N       : out std_logic;
    ADC2_out7_P       : out std_logic;
    ADC2_out7_N       : out std_logic;
    ADC3_out0_P       : out std_logic;
    ADC3_out0_N       : out std_logic;
    ADC3_out1_P       : out std_logic;
    ADC3_out1_N       : out std_logic;
    ADC3_out2_P       : out std_logic;
    ADC3_out2_N       : out std_logic;
    ADC3_out3_P       : out std_logic;
    ADC3_out3_N       : out std_logic;
    ADC3_out4_P       : out std_logic;
    ADC3_out4_N       : out std_logic;
    ADC3_out5_P       : out std_logic;
    ADC3_out5_N       : out std_logic;
    ADC3_out6_P       : out std_logic;
    ADC3_out6_N       : out std_logic;
    ADC3_out7_P       : out std_logic;
    ADC3_out7_N       : out std_logic;
    TH_out_P          : out std_logic;
    TH_out_N          : out std_logic;

    ADC1_DTU1_spy     : out std_logic_vector(7 downto 0);
    ADC2_DTU1_spy     : out std_logic_vector(7 downto 0);
    ADC3_DTU1_spy     : out std_logic_vector(7 downto 0);
    ADC1_DTU_out_data : in word32_t(7 downto 0);
    ADC2_DTU_out_data : in word32_t(7 downto 0);
    ADC3_DTU_out_data : in word32_t(7 downto 0);
    TH_DTU_out_data : in std_logic_vector(31 downto 0);
    cycle_pos         : in unsigned(1 downto 0)
  );

end entity FE_Output;

architecture rtl of FE_Output is

component high_speed_selectio_wiz_Bank_66 is
  port (
  vtc_rdy_bsc0                 : out std_logic;
  en_vtc_bsc0                  : in  std_logic;
  vtc_rdy_bsc1                 : out std_logic;
  en_vtc_bsc1                  : in  std_logic;
  vtc_rdy_bsc2                 : out std_logic;
  en_vtc_bsc2                  : in  std_logic;
  vtc_rdy_bsc3                 : out std_logic;
  en_vtc_bsc3                  : in  std_logic;
  vtc_rdy_bsc4                 : out std_logic;
  en_vtc_bsc4                  : in  std_logic;
  vtc_rdy_bsc5                 : out std_logic;
  en_vtc_bsc5                  : in  std_logic;
  dly_rdy_bsc0                 : out std_logic;
  dly_rdy_bsc1                 : out std_logic;
  dly_rdy_bsc2                 : out std_logic;
  dly_rdy_bsc3                 : out std_logic;
  dly_rdy_bsc4                 : out std_logic;
  dly_rdy_bsc5                 : out std_logic;
  rst_seq_done                 : out std_logic;
  shared_pll0_clkoutphy_out    : out std_logic;
  pll0_clkout0                 : out std_logic;
  pll0_clkout1                 : out std_logic;
  rst                          : in  std_logic;
  clk                          : in  std_logic;
  pll0_locked                  : out std_logic;
  ch2_out3_P                   : out std_logic;
  ch2_out3_N                   : out std_logic;
  data_from_fabric_ch2_out3_P  : in  std_logic_vector(7 downto 0);
  ch2_out0_P                   : out std_logic;
  ch2_out0_N                   : out std_logic;
  data_from_fabric_ch2_out0_P  : in  std_logic_vector(7 downto 0);
  ch3_out1_P                   : out std_logic;
  ch3_out1_N                   : out std_logic;
  data_from_fabric_ch3_out1_P  : in  std_logic_vector(7 downto 0);
  ch1_out7_P                   : out std_logic;
  ch1_out7_N                   : out std_logic;
  data_from_fabric_ch1_out7_P  : in  std_logic_vector(7 downto 0);
  ch3_out2_P                   : out std_logic;
  ch3_out2_N                   : out std_logic;
  data_from_fabric_ch3_out2_P  : in  std_logic_vector(7 downto 0);
  ch2_out6_P                   : out std_logic;
  ch2_out6_N                   : out std_logic;
  data_from_fabric_ch2_out6_P  : in  std_logic_vector(7 downto 0);
  ch3_out0_P                   : out std_logic;
  ch3_out0_N                   : out std_logic;
  data_from_fabric_ch3_out0_P  : in  std_logic_vector(7 downto 0);
  ch2_out7_P                   : out std_logic;
  ch2_out7_N                   : out std_logic;
  data_from_fabric_ch2_out7_P  : in  std_logic_vector(7 downto 0);
  ch2_out5_P                   : out std_logic;
  ch2_out5_N                   : out std_logic;
  data_from_fabric_ch2_out5_P  : in  std_logic_vector(7 downto 0);
  ch2_out2_P                   : out std_logic;
  ch2_out2_N                   : out std_logic;
  data_from_fabric_ch2_out2_P  : in  std_logic_vector(7 downto 0);
  ch3_out4_P                   : out std_logic;
  ch3_out4_N                   : out std_logic;
  data_from_fabric_ch3_out4_P  : in  std_logic_vector(7 downto 0);
  ch3_out3_P                   : out std_logic;
  ch3_out3_N                   : out std_logic;
  data_from_fabric_ch3_out3_P  : in  std_logic_vector(7 downto 0);
  ch3_out5_P                   : out std_logic;
  ch3_out5_N                   : out std_logic;
  data_from_fabric_ch3_out5_P  : in  std_logic_vector(7 downto 0);
  ch3_out7_P                   : out std_logic;
  ch3_out7_N                   : out std_logic;
  data_from_fabric_ch3_out7_P  : in  std_logic_vector(7 downto 0);
  ch3_out6_P                   : out std_logic;
  ch3_out6_N                   : out std_logic;
  data_from_fabric_ch3_out6_P  : in  std_logic_vector(7 downto 0);
  TH_out_P                     : out std_logic;
  TH_out_N                     : out std_logic;
  data_from_fabric_TH_out_P    : in  std_logic_vector(7 downto 0)
);
end component high_speed_selectio_wiz_Bank_66;

component high_speed_selectio_wiz_Bank_46 is
  port (
    vtc_rdy_bsc0                 : out std_logic;
    en_vtc_bsc0                  : in  std_logic;
    vtc_rdy_bsc1                 : out std_logic;
    en_vtc_bsc1                  : in  std_logic;
    vtc_rdy_bsc2                 : out std_logic;
    en_vtc_bsc2                  : in  std_logic;
    vtc_rdy_bsc4                 : out std_logic;
    en_vtc_bsc4                  : in  std_logic;
    dly_rdy_bsc0                 : out std_logic;
    dly_rdy_bsc1                 : out std_logic;
    dly_rdy_bsc2                 : out std_logic;
    dly_rdy_bsc4                 : out std_logic;
    rst_seq_done                 : out std_logic;
    shared_pll0_clkoutphy_out    : out std_logic;
    pll0_clkout0                 : out std_logic;
    pll0_clkout1                 : out std_logic;
    rst                          : in  std_logic;
    clk                          : in  std_logic;
    pll0_locked                  : out std_logic;
    ch1_out2_P                   : out std_logic;
    ch1_out2_N                   : out std_logic;
    data_from_fabric_ch1_out2_P  : in  std_logic_vector(7 downto 0);
    ch1_out0_P                   : out std_logic;
    ch1_out0_N                   : out std_logic;
    data_from_fabric_ch1_out0_P  : in  std_logic_vector(7 downto 0);
    ch1_out3_P                   : out std_logic;
    ch1_out3_N                   : out std_logic;
    data_from_fabric_ch1_out3_P  : in  std_logic_vector(7 downto 0);
    ch1_out5_P                   : out std_logic;
    ch1_out5_N                   : out std_logic;
    data_from_fabric_ch1_out5_P  : in  std_logic_vector(7 downto 0);
    ch1_out4_P                   : out std_logic;
    ch1_out4_N                   : out std_logic;
    data_from_fabric_ch1_out4_P  : in  std_logic_vector(7 downto 0);
    ch1_out1_P                   : out std_logic;
    ch1_out1_N                   : out std_logic;
    data_from_fabric_ch1_out1_P  : in  std_logic_vector(7 downto 0);
    ch1_out6_P                   : out std_logic;
    ch1_out6_N                   : out std_logic;
    data_from_fabric_ch1_out6_P  : in  std_logic_vector(7 downto 0);
    ch2_out1_P                   : out std_logic;
    ch2_out1_N                   : out std_logic;
    data_from_fabric_ch2_out1_P  : in  std_logic_vector(7 downto 0);
    ch2_out4_P                   : out std_logic;
    ch2_out4_N                   : out std_logic;
    data_from_fabric_ch2_out4_P  : in  std_logic_vector(7 downto 0)
  );
end component high_speed_selectio_wiz_Bank_46;

signal ADC1_swap_data                 : Byte_t(7 downto 0) := (others => (others => '0'));
signal ADC1_DDR_data                  : Byte_t(7 downto 0) := (others => (others => '0'));
signal ADC2_swap_data                 : Byte_t(7 downto 0) := (others => (others => '0'));
signal ADC2_DDR_data                  : Byte_t(7 downto 0) := (others => (others => '0'));
signal ADC3_swap_data                 : Byte_t(7 downto 0) := (others => (others => '0'));
signal ADC3_DDR_data                  : Byte_t(7 downto 0) := (others => (others => '0'));
signal TH_swap_data                   : std_logic_vector(7 downto 0) := (others => '0');
signal TH_DDR_data                    : std_logic_vector(7 downto 0) := (others => '0');

signal vtc46_rdy_bsc0                 : std_logic;
signal en46_vtc_bsc0                  : std_logic :='1';
signal vtc46_rdy_bsc1                 : std_logic;
signal en46_vtc_bsc1                  : std_logic :='1';
signal vtc46_rdy_bsc2                 : std_logic;
signal en46_vtc_bsc2                  : std_logic :='1';
signal vtc46_rdy_bsc4                 : std_logic;
signal en46_vtc_bsc4                  : std_logic :='1';
signal dly46_rdy_bsc0                 : std_logic;
signal dly46_rdy_bsc1                 : std_logic;
signal dly46_rdy_bsc2                 : std_logic;
signal dly46_rdy_bsc4                 : std_logic;
signal rst46_seq_done                 : std_logic;
signal shared_pll46_clkoutphy_out     : std_logic;
signal pll46_clkout0                  : std_logic;
signal pll46_clkout1                  : std_logic;
signal pll46_locked                   : std_logic;
signal vtc66_rdy_bsc0                 : std_logic;
signal en66_vtc_bsc0                  : std_logic := '1';
signal vtc66_rdy_bsc1                 : std_logic;
signal en66_vtc_bsc1                  : std_logic := '1';
signal vtc66_rdy_bsc2                 : std_logic;
signal en66_vtc_bsc2                  : std_logic := '1';
signal vtc66_rdy_bsc3                 : std_logic;
signal en66_vtc_bsc3                  : std_logic := '1';
signal vtc66_rdy_bsc4                 : std_logic;
signal en66_vtc_bsc4                  : std_logic := '1';
signal vtc66_rdy_bsc5                 : std_logic;
signal en66_vtc_bsc5                  : std_logic := '1';
signal dly66_rdy_bsc0                 : std_logic;
signal dly66_rdy_bsc1                 : std_logic;
signal dly66_rdy_bsc2                 : std_logic;
signal dly66_rdy_bsc3                 : std_logic;
signal dly66_rdy_bsc4                 : std_logic;
signal dly66_rdy_bsc5                 : std_logic;
signal rst66_seq_done                 : std_logic;
signal shared_pll66_clkoutphy_out     : std_logic;
signal pll66_clkout0                  : std_logic;
signal pll66_clkout1                  : std_logic;
signal pll66_locked                   : std_logic;

begin  -- architecture behavioral

  Inst_oSERDES_46 : high_speed_selectio_wiz_Bank_46
  port map
  (
    vtc_rdy_bsc0                => vtc46_rdy_bsc0,
    en_vtc_bsc0                 => en46_vtc_bsc0,
    vtc_rdy_bsc1                => vtc46_rdy_bsc1,
    en_vtc_bsc1                 => en46_vtc_bsc1,
    vtc_rdy_bsc2                => vtc46_rdy_bsc2,
    en_vtc_bsc2                 => en46_vtc_bsc2,
    vtc_rdy_bsc4                => vtc46_rdy_bsc4,
    en_vtc_bsc4                 => en46_vtc_bsc4,
    dly_rdy_bsc0                => dly46_rdy_bsc0,
    dly_rdy_bsc1                => dly46_rdy_bsc1,
    dly_rdy_bsc2                => dly46_rdy_bsc2,
    dly_rdy_bsc4                => dly46_rdy_bsc4,
    rst_seq_done                => rst46_seq_done,
    shared_pll0_clkoutphy_out   => shared_pll46_clkoutphy_out,
    pll0_clkout0                => pll46_clkout0,
    pll0_clkout1                => pll46_clkout1,
    rst                         => reset,
    clk                         => clk_80,
    pll0_locked                 => pll46_locked,
    ch1_out2_P                  => ADC1_out2_P,
    ch1_out2_N                  => ADC1_out2_N,
    data_from_fabric_ch1_out2_P => ADC1_DDR_data(2),
    ch1_out0_P                  => ADC1_out0_P,
    ch1_out0_N                  => ADC1_out0_N,
    data_from_fabric_ch1_out0_P => ADC1_DDR_data(0),
    ch1_out3_P                  => ADC1_out3_P,
    ch1_out3_N                  => ADC1_out3_N,
    data_from_fabric_ch1_out3_P => ADC1_DDR_data(3),
    ch1_out5_P                  => ADC1_out5_P,
    ch1_out5_N                  => ADC1_out5_N,
    data_from_fabric_ch1_out5_P => ADC1_DDR_data(5),
    ch1_out4_P                  => ADC1_out4_P,
    ch1_out4_N                  => ADC1_out4_N,
    data_from_fabric_ch1_out4_P => ADC1_DDR_data(4),
    ch1_out1_P                  => ADC1_out1_P,
    ch1_out1_N                  => ADC1_out1_N,
    data_from_fabric_ch1_out1_P => ADC1_DDR_data(1),
    ch1_out6_P                  => ADC1_out6_P,
    ch1_out6_N                  => ADC1_out6_N,
    data_from_fabric_ch1_out6_P => ADC1_DDR_data(6),
    ch2_out1_P                  => ADC2_out1_P,
    ch2_out1_N                  => ADC2_out1_N,
    data_from_fabric_ch2_out1_P => ADC2_DDR_data(1),
    ch2_out4_P                  => ADC2_out4_P,
    ch2_out4_N                  => ADC2_out4_N,
    data_from_fabric_ch2_out4_P => ADC2_DDR_data(4)
  );
  
  Inst_oSERDES_66 : high_speed_selectio_wiz_Bank_66
  port map
  (
    vtc_rdy_bsc0                => vtc66_rdy_bsc0,
    en_vtc_bsc0                 => en66_vtc_bsc0,
    vtc_rdy_bsc1                => vtc66_rdy_bsc1,
    en_vtc_bsc1                 => en66_vtc_bsc1,
    vtc_rdy_bsc2                => vtc66_rdy_bsc2,
    en_vtc_bsc2                 => en66_vtc_bsc2,
    vtc_rdy_bsc3                => vtc66_rdy_bsc3, 
    en_vtc_bsc3                 => en66_vtc_bsc3,
    vtc_rdy_bsc4                => vtc66_rdy_bsc4,
    en_vtc_bsc4                 => en66_vtc_bsc4,
    vtc_rdy_bsc5                => vtc66_rdy_bsc5,
    en_vtc_bsc5                 => en66_vtc_bsc5,
    dly_rdy_bsc0                => dly66_rdy_bsc0,
    dly_rdy_bsc1                => dly66_rdy_bsc1,
    dly_rdy_bsc2                => dly66_rdy_bsc2,
    dly_rdy_bsc3                => dly66_rdy_bsc3,
    dly_rdy_bsc4                => dly66_rdy_bsc4,
    dly_rdy_bsc5                => dly66_rdy_bsc5,
    rst_seq_done                => rst66_seq_done,
    shared_pll0_clkoutphy_out   => shared_pll66_clkoutphy_out,
    pll0_clkout0                => pll66_clkout0,
    pll0_clkout1                => pll66_clkout1,
    rst                         => reset,
    clk                         => clk_80,
    pll0_locked                 => pll66_locked,
    ch1_out7_P                  => ADC1_out7_P,
    ch1_out7_N                  => ADC1_out7_N,
    data_from_fabric_ch1_out7_P => ADC1_DDR_data(7),
    ch2_out0_P                  => ADC2_out0_P,
    ch2_out0_N                  => ADC2_out0_N,
    data_from_fabric_ch2_out0_P => ADC2_DDR_data(0),
    ch2_out2_P                  => ADC2_out2_P,
    ch2_out2_N                  => ADC2_out2_N,
    data_from_fabric_ch2_out2_P => ADC2_DDR_data(2),
    ch2_out3_P                  => ADC2_out3_P,
    ch2_out3_N                  => ADC2_out3_N,
    data_from_fabric_ch2_out3_P => ADC2_DDR_data(3),
    ch2_out5_P                  => ADC2_out5_P,
    ch2_out5_N                  => ADC2_out5_N,
    data_from_fabric_ch2_out5_P => ADC2_DDR_data(5),
    ch2_out6_P                  => ADC2_out6_P,
    ch2_out6_N                  => ADC2_out6_N,
    data_from_fabric_ch2_out6_P => ADC2_DDR_data(6),
    ch2_out7_P                  => ADC2_out7_P,
    ch2_out7_N                  => ADC2_out7_N,
    data_from_fabric_ch2_out7_P => ADC2_DDR_data(7),
    ch3_out0_P                  => ADC3_out0_P,
    ch3_out0_N                  => ADC3_out0_N,
    data_from_fabric_ch3_out0_P => ADC3_DDR_data(0),
    ch3_out1_P                  => ADC3_out1_P,
    ch3_out1_N                  => ADC3_out1_N,
    data_from_fabric_ch3_out1_P => ADC3_DDR_data(1),
    ch3_out2_P                  => ADC3_out2_P,
    ch3_out2_N                  => ADC3_out2_N,
    data_from_fabric_ch3_out2_P => ADC3_DDR_data(2),
    ch3_out3_P                  => ADC3_out3_P,
    ch3_out3_N                  => ADC3_out3_N,
    data_from_fabric_ch3_out3_P => ADC3_DDR_data(3),
    ch3_out4_P                  => ADC3_out4_P,
    ch3_out4_N                  => ADC3_out4_N,
    data_from_fabric_ch3_out4_P => ADC3_DDR_data(4),
    ch3_out5_P                  => ADC3_out5_P,
    ch3_out5_N                  => ADC3_out5_N,
    data_from_fabric_ch3_out5_P => ADC3_DDR_data(5),
    ch3_out6_P                  => ADC3_out6_P,
    ch3_out6_N                  => ADC3_out6_N,
    data_from_fabric_ch3_out6_P => ADC3_DDR_data(6),
    ch3_out7_P                  => ADC3_out7_P,
    ch3_out7_N                  => ADC3_out7_N,
    data_from_fabric_ch3_out7_P => ADC3_DDR_data(7),
    TH_out_P                    => TH_out_P,
    TH_out_N                    => TH_out_N,
    data_from_fabric_TH_out_P   => TH_DDR_data
  );

  ADC1_DTU1_spy                 <= ADC1_swap_data(0);
-- Output signals to device
-- Prepare for big endian transfer : MSByte first
  oSERDES_cycle : process (clk_160, reset) is
  begin
    if reset = '1' then
      ADC1_swap_data             <= (others => (others => '0'));
      ADC2_swap_data             <= (others => (others => '0'));
      ADC3_swap_data             <= (others => (others => '0'));
    elsif rising_edge(clk_160) then
      if cycle_pos = 1 then                     -- DTU data are latched at cycle_pos 0. So they are ready here at cycle_pos 1
        for iMux in 7 downto 0 loop
          ADC1_swap_data(iMux)   <= ADC1_DTU_out_data(iMux)(31 downto 24);
          ADC2_swap_data(iMux)   <= ADC2_DTU_out_data(iMux)(31 downto 24);
          ADC3_swap_data(iMux)   <= ADC3_DTU_out_data(iMux)(31 downto 24);
        end loop;  -- iMux
        TH_swap_data             <= TH_DTU_out_data(31 downto 24);
      elsif cycle_pos = 2 then
        for iMux in 7 downto 0 loop
          ADC1_swap_data(iMux)   <= ADC1_DTU_out_data(iMux)(23 downto 16);
          ADC2_swap_data(iMux)   <= ADC2_DTU_out_data(iMux)(23 downto 16);
          ADC3_swap_data(iMux)   <= ADC3_DTU_out_data(iMux)(23 downto 16);
        end loop;  -- iMux
        TH_swap_data             <= TH_DTU_out_data(23 downto 16);
      elsif cycle_pos = 3 then
        for iMux in 7 downto 0 loop
          ADC1_swap_data(iMux)   <= ADC1_DTU_out_data(iMux)(15 downto 8);
          ADC2_swap_data(iMux)   <= ADC2_DTU_out_data(iMux)(15 downto 8);
          ADC3_swap_data(iMux)   <= ADC3_DTU_out_data(iMux)(15 downto 8);
        end loop;  -- iMux
        TH_swap_data             <= TH_DTU_out_data(15 downto 8);
      elsif cycle_pos = 0 then
        for iMux in 7 downto 0 loop
          ADC1_swap_data(iMux)   <= ADC1_DTU_out_data(iMux)(7 downto 0);
          ADC2_swap_data(iMux)   <= ADC2_DTU_out_data(iMux)(7 downto 0);
          ADC3_swap_data(iMux)   <= ADC3_DTU_out_data(iMux)(7 downto 0);
        end loop;  -- iMux
        TH_swap_data             <= TH_DTU_out_data(7 downto 0);
      end if;
    end if;
  end process oSERDES_cycle;

-- But oSERDES is little endian. So swap bits before sending
  g_swap_bits : for ibit in 7 downto 0 generate
    g_swap_out : for iMux in 7 downto 0 generate
      ADC1_DDR_data(iMux)(ibit)     <= ADC1_swap_data(iMUx)(7-ibit);
      ADC2_DDR_data(iMux)(ibit)     <= ADC2_swap_data(iMUx)(7-ibit);
      ADC3_DDR_data(iMux)(ibit)     <= ADC3_swap_data(iMUx)(7-ibit);
    end generate g_swap_out;
    TH_DDR_data(ibit)               <= TH_swap_data(7-ibit);
  end generate g_swap_bits;

end architecture rtl;