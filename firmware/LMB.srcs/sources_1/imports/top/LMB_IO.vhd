----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for LMB control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package LMB_IO is

  constant N_ADC                : natural := 3;
  constant N_I2C_spy_bits       : natural := 192;
  constant N_DTU_registers      : natural := 26;
  constant Nb_of_lanes          : natural := 4;
  constant ADDR_LENGTH          : natural := 16;
  constant FIFO_LENGTH          : natural := 10;

-- LMB : GPIO 2 and GPIO 0 connected to ADC inputs,Can measure V1P2 AND V2P5 from uLVRB
-- LVRB, No Mux/APD temp, CATIA temp :
  --Vaux(9) : GPIO2, Vaux(3) : GPIO0, Vaux(2) : APD_temp_out(Vref_reg), Vaux(1) : CATIA_Temp, vaux(0) : APD_Temp (Vdac_buf)
  constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"020F";
-- LVRB, Mux/no APD temp, CATIA temp  :
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"020A"; -- Vaux(9) : GPIO2, Vaux(3) : GPIO0, Vaux(1) : CATIA_Temp
-- DAC, Mux :
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0002"; -- Vaux(1) : CATIA_Temp with LMB
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0100"; -- Vaux(8) : CATIA_Temp with VICE++

  
  type UInt128_t is array (integer range <>) of unsigned(127 downto 0);
  type UInt32_t  is array (integer range <>) of unsigned(31 downto 0);
  type UInt25_t  is array (integer range <>) of unsigned(24 downto 0);
  type UInt24_t  is array (integer range <>) of unsigned(23 downto 0);
  type UInt21_t  is array (integer range <>) of unsigned(20 downto 0);
  type UInt16_t  is array (integer range <>) of unsigned(15 downto 0);
  type UInt14_t  is array (integer range <>) of unsigned(13 downto 0);
  type UInt9_t   is array (integer range <>) of unsigned(8 downto 0);
  type UInt8_t   is array (integer range <>) of unsigned(7 downto 0);
  type UInt7_t   is array (integer range <>) of unsigned(6 downto 0);
  type UInt4_t   is array (integer range <>) of unsigned(3 downto 0);
  type UInt2_t   is array (integer range <>) of unsigned(1 downto 0);
  type Word128_t is array (integer range <>) of std_logic_vector(127 downto 0);
  type Word32_t  is array (integer range <>) of std_logic_vector(31 downto 0);
  type Word16_t  is array (integer range <>) of std_logic_vector(15 downto 0);
  type Word12_t  is array (integer range <>) of std_logic_vector(11 downto 0);
  type Word9_t   is array (integer range <>) of std_logic_vector(8 downto 0);
  type Byte_t    is array (integer range <>) of std_logic_vector(7 downto 0);
  type Word7_t   is array (integer range <>) of std_logic_vector(6 downto 0);
  type Word4_t   is array (integer range <>) of std_logic_vector(3 downto 0);
  type Bit_t     is array (integer range <>) of std_logic_vector(0 downto 0);
  type DTU_reg_t is array (integer range <>) of Byte_t(N_DTU_registers-1 downto 0);

  type Idelay_pos_t is array (integer range <>) of std_logic_vector(4 downto 0);

  type ReSync_command_t is record
    stop            : std_logic;
    start           : std_logic;
    DTU_reset       : std_logic;
    I2C_reset       : std_logic;
    TestUnit_reset  : std_logic;
    DTU_sync_mode   : std_logic;
    DTU_normal_mode : std_logic;
    DTU_flush       : std_logic;
    ADCH_reset      : std_logic;
    ADCH_calib      : std_logic;
    ADCL_reset      : std_logic;
    ADCL_calib      : std_logic;
    laser_trigger   : std_logic;
    CATIA_TP        : std_logic;
    BC0_marker      : std_logic;
    PLL_reset       : std_logic;
  end record ReSync_command_t;
  
  type LMB_Monitor_t is record
    firmware_ver          : std_logic_vector(31 downto 0);
    board_SN              : std_logic_vector(3 downto 0);
    reset                 : std_logic;
    clock_reset           : std_logic;
    clock_locked          : std_logic;
    delay_locked          : std_logic;                     -- DELAYCTRL lock signals
    FE_synchronized       : std_logic;
    ADC_synchronized      : std_logic_vector(2 downto 0);
    JESD_ADC_reset_delay  : unsigned(31 downto 0);         -- Delay between end of ADC reset and jesd reset 
    DCI_locked            : std_logic;
    trigger_in            : std_logic_vector(3 downto 0);
    trigger_in_clear      : std_logic;
    delay_scan            : std_logic;
    SW_delay              : unsigned(15 downto 0);
    HW_delay              : unsigned(15 downto 0);
    self_trigger          : std_logic;
    self_trigger_mode     : std_logic;
    self_trigger_mask     : std_logic_vector(3 downto 1);  -- Which ADC trig ?
    self_trigger_thres    : unsigned(13 downto 0);         -- at which level ?
    extra_trigger         : std_logic;
    trigger_soft          : std_logic;
    trigger_delay         : UInt25_t(7 downto 0);
    update_delay_phase    : std_logic_vector(7 downto 0);
    trigger_width         : UInt7_t(7 downto 0);
    trigger_inhibit_delay : unsigned(23 downto 0);
    trigger_map           : std_logic_vector(31 downto 0);
    trigger_mask          : std_logic_vector(7 downto 0);  -- Mask output signals
    trigger_tap_value     : word9_t(3 downto 1);
    gen_BC0               : std_logic;
    gen_WTE               : std_logic;
    WTE_pos               : unsigned(13 downto 0);
    force_local_WTE_pos   : std_logic;
    next_sequence_pos     : std_logic_vector(31 downto 0);
    LED_on                : std_logic;
    debug1                : word32_t(31 downto 0);          -- Debug1 words
    debug2                : word32_t(31 downto 0);          -- Debug2 words
    AXI_busy              : std_logic;
    ADC1_AXI_addr         : std_logic_vector(12 downto 0);
    ADC1_AXI_data         : std_logic_vector(31 downto 0);
    ADC1_link_config      : word32_t(3 downto 0);
    ADC2_AXI_addr         : std_logic_vector(12 downto 0);
    ADC2_AXI_data         : std_logic_vector(31 downto 0);
    ADC2_link_config      : word32_t(3 downto 0);
    ADC3_AXI_addr         : std_logic_vector(12 downto 0);
    ADC3_AXI_data         : std_logic_vector(31 downto 0);
    ADC3_link_config      : word32_t(3 downto 0);
    temperature           : std_logic_vector(15 downto 0);
    humidity              : std_logic_vector(15 downto 0);
  end record LMB_Monitor_t;

  type LMB_Control_t is record
    clock_reset           : std_logic;
    LMB_reset             : std_logic;
    trigger_soft          : std_logic;
    extra_trigger         : std_logic;
    self_trigger_mask     : std_logic_vector(N_ADC downto 1);
    self_trigger          : std_logic;
    self_trigger_thres    : unsigned(13 downto 0);
    self_trigger_mode     : std_logic;
    trigger_in            : std_logic_vector(3 downto 0);
    trigger_in_clear      : std_logic;
    delay_scan            : std_logic;                     -- Add artificial jitter on laser trigger to get analogue shapes
    SW_delay              : unsigned(15 downto 0);
    HW_delay              : unsigned(15 downto 0);
    gen_BC0               : std_logic;
    gen_WTE               : std_logic;
    WTE_pos               : unsigned(13 downto 0);
    force_local_WTE_pos   : std_logic;
    LED_on                : std_logic;
    IO_reset              : std_logic;                     -- GTH Reset
    JESD_sys_reset        : std_logic;                     -- JESD sys reset
    AXI_reset             : std_logic;
    JESD_ADC_reset_delay  : unsigned(31 downto 0);         -- Delay between end of ADC reset and jesd reset 
    trigger_delay         : UInt25_t(7 downto 0);
    update_delay_phase    : std_logic_vector(7 downto 0);
    trigger_width         : UInt7_t(7 downto 0);
    trigger_inhibit_delay : unsigned(23 downto 0);
    trigger_map           : std_logic_vector(31 downto 0);
    trigger_mask          : std_logic_vector(7 downto 0);
    trigger_tap_value     : word9_t(3 downto 1);
    ADC1_AXI_addr         : std_logic_vector(12 downto 0);
    ADC1_AXI_data         : std_logic_vector(31 downto 0);
    ADC1_AXI_R_Wb         : std_logic;
    ADC1_AXI_access       : std_logic;
    ADC2_AXI_addr         : std_logic_vector(12 downto 0);
    ADC2_AXI_data         : std_logic_vector(31 downto 0);
    ADC2_AXI_R_Wb         : std_logic;
    ADC2_AXI_access       : std_logic;
    ADC3_AXI_addr         : std_logic_vector(12 downto 0);
    ADC3_AXI_data         : std_logic_vector(31 downto 0);
    ADC3_AXI_R_Wb         : std_logic;
    ADC3_AXI_access       : std_logic;
    TH_command            : std_logic_vector(7 downto 0);
    TH_access             : std_logic;
    TH_timer_off          : std_logic;
  end record LMB_Control_t;
  constant DEFAULT_LMB_Control : LMB_Control_t := (clock_reset           => '0',
                                                   LMB_reset             => '0',
                                                   trigger_soft          => '0',
                                                   extra_trigger         => '0', -- Asynchronous on-demand generation of output triggers
                                                   self_trigger          => '0',
                                                   self_trigger_mode     => '0', -- 0 -> aboslute level, 1 -> delta 
                                                   self_trigger_mask     => (others => '0'),
                                                   self_trigger_thres    => (others => '1'),
                                                   trigger_in            => (others => '0'),
                                                   trigger_in_clear      => '0',
                                                   delay_scan            => '0',
                                                   SW_delay              => (others => '0'),
                                                   HW_delay              => (others => '0'),
                                                   gen_BC0               => '1',
                                                   gen_WTE               => '0',
                                                   trigger_delay         => (others => (others => '0')),
                                                   update_delay_phase    => (others => '0'),
                                                   trigger_width         => (others => "0001111"),
                                                   trigger_inhibit_delay => x"100000",         -- don't retrig laser too often
                                                   trigger_map           => x"ba983210",       -- Mapping triggers to physical outputs
                                                   trigger_mask          => (others => '0'),
                                                   trigger_tap_value     => ("011101111", "010011111","001010000"), 
                                                   WTE_pos               => "11"&x"16f",       -- WTE orbit position : 10 us before BC0
                                                   force_local_WTE_pos   => '0',
                                                   LED_on                => '1',
                                                   IO_reset              => '0',
                                                   JESD_ADC_reset_delay  => x"02000000",         -- Wait for 100 ms between ADC reset and JESD reset
                                                   JESD_sys_reset        => '0',
                                                   AXI_reset             => '0',
                                                   ADC1_AXI_data         => (others => '0'),
                                                   ADC1_AXI_addr         => (others => '0'),
                                                   ADC1_AXI_access       => '0',
                                                   ADC1_AXI_R_Wb         => '0',
                                                   ADC2_AXI_data         => (others => '0'),
                                                   ADC2_AXI_addr         => (others => '0'),
                                                   ADC2_AXI_access       => '0',
                                                   ADC2_AXI_R_Wb         => '0',
                                                   ADC3_AXI_data         => (others => '0'),
                                                   ADC3_AXI_addr         => (others => '0'),
                                                   ADC3_AXI_access       => '0',
                                                   ADC3_AXI_R_Wb         => '0',
                                                   TH_command            => (others => '0'),
                                                   TH_access             => '0',
                                                   TH_timer_off          => '0'
                                                   );

  type ADC_Monitor_t is record
    I2C_PLL_data             : std_logic_vector(15 downto 0);   -- Data read from PLL register
    I2C_PLL_reg              : std_logic_vector(7 downto 0);   -- Register number accessed
    I2C_PLL_address          : std_logic_vector(6 downto 0);   -- Device number accessed
    I2C_PLL_n_ack            : unsigned(7 downto 0);           -- Number of I2C acknowledge received during last transaction
    I2C_lpGBT_mode           : std_logic;                      -- Mimick lpGBT bug in READ transactions
    I2C_PLL_busy             : std_logic;                      -- PLL I2C is running
    ReSync_busy              : std_logic;                      -- ReSync broadcast in progress
    DTU_auto_sync            : std_logic;                      -- eLinks synchronization durng ADC calibration with idle patterns
    I2C_R_Wb                 : std_logic;                      -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                      -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    I2C_ack_spy              : std_logic_vector(n_I2C_spy_bits-1 downto 0); -- Spy register of I2C protocol for CATIA1
    SPI_ADC_data             : std_logic_vector(7 downto 0);   -- Data to read/write in register
    SPI_ADC_reg              : std_logic_vector(14 downto 0);  -- ADC register to write in/read from
    SPI_ADC_number           : natural range 0 to 3;           -- ADC number accessed
    SPI_ADC_R_Wb             : std_logic;                      -- ADC action performed (1=read, 0=write)
    SPI_busy                 : std_logic;                      -- Busy during SPI transaction
    ADC_Cal_Busy             : std_logic_vector(5 downto 1);   -- ADC is in calibration procedure use it to synchronize incoming stream with idle patterns
    ADC_PLL_lock             : std_logic_vector(5 downto 1);   -- DTU PLL is locked (1) or not (0)
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);  -- DTU word transmitted in sync mode (V2.0)
    ADC_ReSync_data          : std_logic_vector(31 downto 0);   -- Latest resync code used
    ADC_ReSync_idle          : std_logic_vector(7 downto 0);   -- resync idle patttern
    CATIA_TP_duration        : unsigned(7 downto 0);           -- TP duration in LiTE-DTU
    I2C_reset                : std_logic;
    PLL_update_reg           : std_logic_vector(7 downto 0);
    PLL_update_val           : std_logic_vector(7 downto 0);
    ADC_baseline             : UInt14_t(3 downto 1);
  end record ADC_Monitor_t;
  type ADC_Control_t is record
    I2C_PLL_data             : std_logic_vector(15 downto 0);   -- Data to read/write in register
    I2C_PLL_reg              : std_logic_vector(7 downto 0);   -- Register number to access
    I2C_PLL_number           : natural range 0 to 3;           -- Chip number PLL-DAC1-DAC2-DAC3
    I2C_PLL_access           : std_logic;                      -- Access to PLL registers through I2C bus
    I2C_lpGBT_mode           : std_logic;                      -- Mimick lpGBT bug in READ transactions
    I2C_R_Wb                 : std_logic;                      -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                      -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    ADC_ReSync_data          : std_logic_vector(31 downto 0);  -- Resync code to be sent to LiTE-DTU after Hamming encoding
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);  -- DTU word transmitted in sync mode (V2.0)
    ADC_ReSync_idle          : std_logic_vector(7 downto 0);   -- Resync idle patterm
    ADC_start_ReSync         : std_logic;                      -- Start ReSync transaction
    I2C_reset                : std_logic;
    CATIA_TP_duration        : unsigned(7 downto 0);           -- TP duration in LiTE-DTU
    SPI_ADC_access           : std_logic;                      -- Access to ADC registers through SPI bus
    SPI_ADC_data             : std_logic_vector(7 downto 0);   -- Data to read/write in register
    SPI_ADC_reg              : std_logic_vector(14 downto 0);  -- ADC register to write in/read from
    SPI_ADC_number           : natural range 0 to 3;           -- ADC number accessed
    SPI_ADC_R_Wb             : std_logic;                      -- ADC action performed (1=read, 0=write)
    PLL_reset                : std_logic;
    PLL_update_en            : std_logic;
    PLL_update_reg           : std_logic_vector(7 downto 0);
    PLL_update_val           : std_logic_vector(7 downto 0);
    ADC_reset                : std_logic;
    ADC_rx_sync              : std_logic;
  end record ADC_Control_t;
  constant DEFAULT_ADC_Control : ADC_Control_t := (
                                                   I2C_PLL_data        => (others => '0'),
                                                   I2C_PLL_reg         => (others => '0'),
                                                   I2C_PLL_number      => 0,
                                                   I2C_PLL_access      => '0',
                                                   I2C_long_transfer   => '0',
                                                   I2C_R_Wb            => '0',
                                                   I2C_lpGBT_mode      => '0',
                                                   ADC_ReSync_data     => (others => '0'),
                                                   ADC_ReSync_idle     => x"66",
                                                   DTU_Sync_pattern    => x"eaaaaaaa",
                                                   ADC_start_ReSync    => '0',
                                                   I2C_reset           => '0',
                                                   CATIA_TP_duration   => x"01",
                                                   SPI_ADC_access      => '0',
                                                   SPI_ADC_data        => (others => '0'),
                                                   SPI_ADC_reg         => (others => '0'),
                                                   SPI_ADC_number      => 0,
                                                   SPI_ADC_R_Wb        => '0',
                                                   PLL_reset           => '0',
                                                   PLL_update_en       => '0',
                                                   PLL_update_reg      => (others => '0'),
                                                   PLL_update_val      => (others => '0'),
                                                   ADC_reset           => '0',
                                                   ADC_rx_sync         => '0'
                                                  );
  type XADC_Monitor_t is record
    XADC_ready           : std_logic;
    XADC_addr            : std_logic_vector(7 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Monitor_t;
  type XADC_Control_t is record
    XADC_access          : std_logic;
    XADC_WRb             : std_logic;
    XADC_addr            : std_logic_vector(7 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Control_t;
  constant DEFAULT_XADC_Control : XADC_Control_t := (
                                                     XADC_access         => '0',
                                                     XADC_WRb            => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));
  constant DEFAULT_XADC_Monitor : XADC_Monitor_t := (
                                                     XADC_ready          => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));

  constant DTU_ReSync_code : Byte_t(15 downto 0) := (x"7F",x"78",x"66",x"61",x"55",x"52",x"4C",x"4B",
                                                     x"34",x"33",x"2D",x"2A",x"1E",x"19",x"07",x"00");

end LMB_IO;
