--------------------------------------------------------------------------------
-- Company: CEA-Saclay
-- Engineer: M.D.
--
-- Create Date: 2022/11/01
-- Design Name: LMB
-- Component Name: LMB_top
-- Target Device: XCKU035
-- Tool versions: Vivado 22.02
-- Description:
--   Top level steering module for LMB firmware
--   Instantiate clock generation, PLL setting, ADC management, DTU simulation, FE interface
-- Dependencies:
--    LMB_IO
-- Revision:
--    1.0 : Frst working version
--    2.0 : 2023/11/20 : Start to have a running firmware 
-- Additional Comments:
--    <Additional_comments>

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.LMB_IO.all;

entity LMB is
  generic (
    ADDR               : std_logic_vector(3 downto 0) :="0000"
  );

  port (
  GbE_refclk_P        : in    std_logic;
  GbE_refclk_N        : in    std_logic;
  GbE_RX_N            : in    std_logic;
  GbE_RX_P            : in    std_logic;
  GbE_TX_N            : out   std_logic;
  GbE_TX_P            : out   std_logic;

  SFP_Present         : in    std_logic;
  SFP_LOS             : in    std_logic;
  SFP_Tx_Fault        : in    std_logic;
  SFP_SCL             : out   std_logic;
  SFP_SDA             : inout std_logic;

  LED                 : out   std_logic_vector(7 downto 0);
  GPIO                : out   std_logic_vector(15 downto 0);
  GPIO_ref            : in    std_logic_vector(15 downto 0);
  GPIO0_P             : in    std_logic;
  GPIO0_N             : in    std_logic;
  GPIO1_P             : in    std_logic;
  GPIO1_N             : in    std_logic;
  GPIO2_P             : in    std_logic;
  GPIO2_N             : in    std_logic;
  GPIO3_P             : in    std_logic;
  GPIO3_N             : in    std_logic;
  ADC_DCDC_Temp       : in    std_logic;
  ADC_DCDC_Temp_ref   : in    std_logic;
  FPGA_DCDC_Temp1     : in    std_logic;
  FPGA_DCDC_Temp1_ref : in    std_logic;
  FPGA_DCDC_Temp2     : in    std_logic;
  FPGA_DCDC_Temp2_ref : in    std_logic;

  ADC1_refclk_P     : in    std_logic;
  ADC1_refclk_N     : in    std_logic;
  ADC1_RX0_N        : in    std_logic;
  ADC1_RX0_P        : in    std_logic;
  ADC1_RX1_N        : in    std_logic;
  ADC1_RX1_P        : in    std_logic;
  ADC1_RX2_N        : in    std_logic;
  ADC1_RX2_P        : in    std_logic;
  ADC1_RX3_N        : in    std_logic;
  ADC1_RX3_P        : in    std_logic;
  ADC1_out0_P       : out   std_logic;
  ADC1_out0_N       : out   std_logic;
  ADC1_out1_P       : out   std_logic;
  ADC1_out1_N       : out   std_logic;
  ADC1_out2_P       : out   std_logic;
  ADC1_out2_N       : out   std_logic;
  ADC1_out3_P       : out   std_logic;
  ADC1_out3_N       : out   std_logic;
  ADC1_out4_P       : out   std_logic;
  ADC1_out4_N       : out   std_logic;
  ADC1_out5_P       : out   std_logic;
  ADC1_out5_N       : out   std_logic;
  ADC1_out6_P       : out   std_logic;
  ADC1_out6_N       : out   std_logic;
  ADC1_out7_P       : out   std_logic;
  ADC1_out7_N       : out   std_logic;
  ADC1_GPIO         : inout std_logic_vector(2 downto 1);
  
  ADC2_refclk_P     : in    std_logic;
  ADC2_refclk_N     : in    std_logic;
  ADC2_RX0_N        : in    std_logic;
  ADC2_RX0_P        : in    std_logic;
  ADC2_RX1_N        : in    std_logic;
  ADC2_RX1_P        : in    std_logic;
  ADC2_RX2_N        : in    std_logic;
  ADC2_RX2_P        : in    std_logic;
  ADC2_RX3_N        : in    std_logic;
  ADC2_RX3_P        : in    std_logic;
  ADC2_out0_P       : out   std_logic;
  ADC2_out0_N       : out   std_logic;
  ADC2_out1_P       : out   std_logic;
  ADC2_out1_N       : out   std_logic;
  ADC2_out2_P       : out   std_logic;
  ADC2_out2_N       : out   std_logic;
  ADC2_out3_P       : out   std_logic;
  ADC2_out3_N       : out   std_logic;
  ADC2_out4_P       : out   std_logic;
  ADC2_out4_N       : out   std_logic;
  ADC2_out5_P       : out   std_logic;
  ADC2_out5_N       : out   std_logic;
  ADC2_out6_P       : out   std_logic;
  ADC2_out6_N       : out   std_logic;
  ADC2_out7_P       : out   std_logic;
  ADC2_out7_N       : out   std_logic;
  ADC2_GPIO         : inout std_logic_vector(2 downto 1);

  ADC3_refclk_P     : in    std_logic;
  ADC3_refclk_N     : in    std_logic;
  ADC3_RX0_N        : in    std_logic;
  ADC3_RX0_P        : in    std_logic;
  ADC3_RX1_N        : in    std_logic;
  ADC3_RX1_P        : in    std_logic;
  ADC3_RX2_N        : in    std_logic;
  ADC3_RX2_P        : in    std_logic;
  ADC3_RX3_N        : in    std_logic;
  ADC3_RX3_P        : in    std_logic;
  ADC3_out0_P       : out   std_logic;
  ADC3_out0_N       : out   std_logic;
  ADC3_out1_P       : out   std_logic;
  ADC3_out1_N       : out   std_logic;
  ADC3_out2_P       : out   std_logic;
  ADC3_out2_N       : out   std_logic;
  ADC3_out3_P       : out   std_logic;
  ADC3_out3_N       : out   std_logic;
  ADC3_out4_P       : out   std_logic;
  ADC3_out4_N       : out   std_logic;
  ADC3_out5_P       : out   std_logic;
  ADC3_out5_N       : out   std_logic;
  ADC3_out6_P       : out   std_logic;
  ADC3_out6_N       : out   std_logic;
  ADC3_out7_P       : out   std_logic;
  ADC3_out7_N       : out   std_logic;
  ADC3_GPIO         : inout std_logic_vector(2 downto 1);

  SPI_ADC_Clk       : out   std_logic;
  SPI_ADC_DIO       : inout std_logic;
  SPI_ADC_Csb       : out   std_logic_vector(3 downto 1);
  
  Sync_out_P        : out   std_logic;
  Sync_out_N        : out   std_logic;
  SysRef_P          : out   std_logic;
  SysRef_N          : out   std_logic;

  Pll1_Lock_out     : out   std_logic_vector(7 downto 0);
  Pll2_Lock_out     : out   std_logic_vector(7 downto 0);
  Pll3_Lock_out     : out   std_logic_vector(7 downto 0);

  PllD_Lock_out     : out   std_logic; -- Dummy output
  TH_out_P          : out   std_logic;
  TH_out_N          : out   std_logic;

  Sig_trig_out      : out   std_logic_vector(7 downto 0);
  Sig_trig_in       : in    std_logic_vector(3 downto 0);

  ReSync_in_P       : in    std_logic;
  ReSync_in_N       : in    std_logic;

  I2C_SDA_FE        : inout std_logic;
  I2C_SCL_FE        : in    std_logic;
  Loc_Rstb          : out   std_logic;

  PLL_Rstb          : out   std_logic;
  PLL_LoS           : in    std_logic;
  PLL_LoL           : in    std_logic;
  I2C_SDA_PLL       : inout std_logic;
  I2C_SCL_PLL       : inout std_logic;
  DAC_CS            : out   std_logic_vector(3 downto 1);

  I2C_SDA_Ext_Temp  : inout std_logic;
  I2C_SCL_Ext_Temp  : inout std_logic;

  CLK_160_P         : in    std_logic;
  CLK_160_N         : in    std_logic
);
end LMB;

architecture rtl of LMB is

component clk_generator_main is
  port (
    clk_160_in      : in  std_logic;
    clk_20          : out std_logic;
    clk_40          : out std_logic;
    clk_80          : out std_logic;
    clk_160         : out std_logic;
    clk_160_b       : out std_logic;
    clk_320         : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic
  );
end component clk_generator_main;

component clk_generator_ethernet is
  port (
    clk_160_in      : in  std_logic;
    clk_axi         : out std_logic;
    clk_ethernet    : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic
  );
end component clk_generator_ethernet;

component gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p             : in  std_logic;
    gtrefclk_n             : in  std_logic;
    gtrefclk_out           : out std_logic;
    txp                    : out std_logic;
    txn                    : out std_logic;
    rxp                    : in  std_logic;
    rxn                    : in  std_logic;
    resetdone              : out std_logic;
    userclk_out            : out std_logic;
    userclk2_out           : out std_logic;
    rxuserclk_out          : out std_logic;
    rxuserclk2_out         : out std_logic;
    pma_reset_out          : out std_logic;
    mmcm_locked_out        : out std_logic;
    independent_clock_bufg : in  std_logic;
    gmii_txd               : in  std_logic_vector(7 downto 0);
    gmii_tx_en             : in  std_logic;
    gmii_tx_er             : in  std_logic;
    gmii_rxd               : out std_logic_vector(7 downto 0);
    gmii_rx_dv             : out std_logic;
    gmii_rx_er             : out std_logic;
    gmii_isolate           : out std_logic;
    configuration_vector   : in  std_logic_vector(4 downto 0);
    status_vector          : out std_logic_vector(15 downto 0);
    reset                  : in  std_logic;
    signal_detect          : in  std_logic;
    gtpowergood            : out std_logic
  );
end component gig_ethernet_pcs_pma_0;

component tri_mode_ethernet_mac_0 is
  port (
    gtx_clk                 : in  std_logic;
      -- asynchronous reset
    glbl_rstn               : in  std_logic;
    rx_axi_rstn             : in  std_logic;
    tx_axi_rstn             : in  std_logic;
    rx_statistics_vector    : out std_logic_vector(27 downto 0);
    rx_statistics_valid     : out std_logic;
    rx_mac_aclk             : out std_logic;
    rx_reset                : out std_logic;
    rx_axis_mac_tdata       : out std_logic_vector(7 downto 0);
    rx_axis_mac_tvalid      : out std_logic;
    rx_axis_mac_tlast       : out std_logic;
    rx_axis_mac_tuser       : out std_logic;
    tx_ifg_delay            : in  std_logic_vector(7 downto 0);

    tx_statistics_vector    : out std_logic_vector(31 downto 0);
    tx_statistics_valid     : out std_logic;
    tx_mac_aclk             : out std_logic;
    tx_reset                : out std_logic;
    tx_axis_mac_tdata       : in  std_logic_vector(7 downto 0);
    tx_axis_mac_tvalid      : in  std_logic;
    tx_axis_mac_tlast       : in  std_logic;
    tx_axis_mac_tuser       : in  std_logic_vector(0 downto 0);
    tx_axis_mac_tready      : out std_logic;
    pause_req               : in  std_logic;
    pause_val               : in  std_logic_vector(15 downto 0);
    speedis100              : out std_logic;
    speedis10100            : out std_logic;
--    rx_enable                  => rx_enable,
--    tx_enable                  => tx_enable,
--    tx_ifg_delay               => tx_ifg_delay,
--    gmii_tx_clk                => gmii_tx_clk,
--    gmii_rx_clk                => gmii_rx_clk,
--    mii_tx_clk                 => mii_tx_clk,

    gmii_txd                : out std_logic_vector(7 downto 0);
    gmii_tx_en              : out std_logic;
    gmii_tx_er              : out std_logic;
    gmii_rxd                : in  std_logic_vector(7 downto 0);
    gmii_rx_dv              : in  std_logic;
    gmii_rx_er              : in  std_logic;
    rx_configuration_vector : in  std_logic_vector(79 downto 0);
    tx_configuration_vector : in  std_logic_vector(79 downto 0)
  );
end component tri_mode_ethernet_mac_0;

signal gmii_txd                 : std_logic_vector(7 downto 0);
signal gmii_tx_en               : std_logic;
signal gmii_tx_er               : std_logic;
signal gmii_rxd                 : std_logic_vector(7 downto 0);
signal gmii_rx_dv               : std_logic;
signal gmii_rx_er               : std_logic;
signal GbE_user_clk             : std_logic;
signal fake_packet_counter      : integer range 0 to 1536 := 0;

signal mac_tx_data, mac_rx_data : std_logic_vector(7 downto 0);
signal mac_tx_valid             : std_logic;
signal mac_tx_last              : std_logic;
signal mac_tx_ready             : std_logic;
signal mac_rx_valid             : std_logic;
signal mac_rx_last              : std_logic;
signal mac_rx_error             : std_logic;
signal mac_tx_error             : std_logic_vector(0 downto 0);
signal ipb_master_out           : ipb_wbus;
signal ipb_master_in            : ipb_rbus;
signal mac_addr                 : std_logic_vector(47 downto 0);
signal ip_addr                  : std_logic_vector(31 downto 0);
signal actual_mac_addr          : std_logic_vector(47 downto 0);
signal actual_ip_addr           : std_logic_vector(31 downto 0);
signal got_IP_addr              : std_logic;
signal pkt, pkt_oob             : std_logic;
signal frame_size               : std_logic_vector(15 downto 0);
signal rx_configuration_vector  : std_logic_vector(79 downto 0);
signal tx_configuration_vector  : std_logic_vector(79 downto 0);

signal trigger_in               : std_logic_vector(3 downto 0);
signal WTE                      : std_logic := '0';
signal WWTE                     : std_logic := '0';
signal BC0                      : std_logic := '0';
signal TE                       : std_logic := '0';
signal clk_counter              : unsigned(13 downto 0) := (others => '0');
signal next_lmr                 : std_logic := '0';
signal next_sequence_pos        : std_logic_vector(31 downto 0);

--Array mappings to simplify instances
signal ADC1_data                : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC1_data_valid          : std_logic;
signal ADC1_charisk             : std_logic_vector(Nb_of_lanes*4-1 downto 0);
signal ADC1_rx_disperror        : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC1_rx_notintable       : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC1_rx_sync             : std_logic;
signal ADC1_reset_done          : std_logic;
signal ADC1_gt_powergood        : std_logic;
signal ADC1_sysref_seen         : std_logic;
signal ADC1_DTU_in              : Word16_t(7 downto 0);
signal ADC1_DTU_out             : Word32_t(7 downto 0);
signal ADC1_DTU_cycle_pos       : UInt2_t(7 downto 0);
signal ADC1_DTU1_spy            : std_logic_vector(7 downto 0);

signal ADC2_data                : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC2_data_valid          : std_logic;
signal ADC2_charisk             : std_logic_vector(Nb_of_lanes*4-1 downto 0);
signal ADC2_rx_disperror        : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC2_rx_notintable       : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC2_rx_sync             : std_logic;
signal ADC2_reset_done          : std_logic;
signal ADC2_gt_powergood        : std_logic;
signal ADC2_sysref_seen         : std_logic;
signal ADC2_DTU_in              : Word16_t(7 downto 0);
signal ADC2_DTU_out             : Word32_t(7 downto 0);
signal ADC2_DTU_cycle_pos       : UInt2_t(7 downto 0);
signal ADC2_DTU1_spy            : std_logic_vector(7 downto 0);

signal ADC3_data                : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC3_data_valid          : std_logic;
signal ADC3_charisk             : std_logic_vector(Nb_of_lanes*4-1 downto 0);
signal ADC3_rx_disperror        : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC3_rx_notintable       : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC3_rx_sync             : std_logic;
signal ADC3_reset_done          : std_logic;
signal ADC3_gt_powergood        : std_logic;
signal ADC3_sysref_seen         : std_logic;
signal ADC3_DTU_in              : Word16_t(7 downto 0);
signal ADC3_DTU_out             : Word32_t(7 downto 0);
signal ADC3_DTU_cycle_pos       : UInt2_t(7 downto 0);
signal ADC3_DTU1_spy            : std_logic_vector(7 downto 0);

signal TH_DTU_in                : std_logic_vector(15 downto 0);
signal TH_DTU_out               : std_logic_vector(31 downto 0);
signal TH_DTU_cycle_pos         : unsigned(1 downto 0);
signal TH_generate_cycle        : unsigned(2 downto 0);

signal ADC_Sync_in              : std_logic := '0';
signal ADC_SysRef               : std_logic := '0';
signal ADCD_out                 : std_logic := '0';

signal pwup_reset               : std_logic := '0';
signal loc_clk_in               : std_logic := '0';
signal Clk_ipbus                : std_logic := '0';
signal Clk_Gb_eth               : std_logic := '0';
signal Clk_AXI                  : std_logic := '0';
signal Clk_I2C                  : std_logic := '0';
signal trigger_clk              : std_logic := '0';
signal system_clk               : std_logic := '0';
signal sequence_clk             : std_logic := '0';
signal shift_reg_clk            : std_logic := '0';
signal memory_clk               : std_logic := '0';
signal resync_clk               : std_logic := '0';
signal clock_generator_locked   : std_logic := '0';
signal delay_clock_locked       : std_logic := '0';
signal MMCM_locked              : std_logic := '0';
signal delay_locked_debug       : std_logic := '0';
signal GbE_mmcm_locked          : std_logic := '0';

signal Clk_in                   : std_logic := '0';
signal Clk_20                   : std_logic := '0';
signal Clk_40                   : std_logic := '0';
signal Clk_80                   : std_logic := '0';
signal Clk_160                  : std_logic := '0';
signal Clk_160_b                : std_logic := '0';
signal Clk_320                  : std_logic := '0';

signal Resync_in                : std_logic := '0';
signal ReSync_command           : ReSync_command_t;

signal I2C_data_LVRB            : std_logic_vector(15 downto 0) := (others => '0');
signal I2C_data_ADC             : std_logic_vector(15 downto 0) := (others => '0');
signal I2C_data_DTU             : DTU_reg_t(N_ADC downto 0); -- N_ADC + Temp/humidity LiTE-DTU
signal dbg_i2c_clk              : std_logic;
signal dbg_sda                  : std_logic;
signal dbg_scl                  : std_logic;

--Monitoring
signal LMB_Monitor              : LMB_Monitor_t;
signal LMB_Control              : LMB_Control_t := DEFAULT_LMB_Control;
signal ADC_Monitor              : ADC_Monitor_t;
signal ADC_Control              : ADC_Control_t := DEFAULT_ADC_Control;
signal led_counter              : unsigned(31 downto 0) := (others => '0');
signal CLK_reset                : std_logic := '0';
signal local_trigger            : std_logic;
signal trigger_DAQ              : std_logic;
signal trigger_software         : std_logic;
signal trigger_sw_prev1         : std_logic;
signal trigger_sw_prev2         : std_logic;
signal trigger_sw_width         : unsigned(3 downto 0);
signal start_trigger_sw_delay   : std_logic;
signal trigger_sw_delay         : unsigned(15 downto 0);
signal trigger_hardware         : std_logic;
signal trigger_hw_prev1         : std_logic;
signal trigger_hw_prev2         : std_logic;
signal trigger_hw_width         : unsigned(3 downto 0);
signal start_trigger_hw_delay   : std_logic;
signal trigger_hw_delay         : unsigned(15 downto 0);
signal trigger                  : std_logic := '0';
signal DAQ_busy                 : std_logic := '0';
signal ipbus_counter            : std_logic_vector(2 downto 0) := "000";

signal ADC_reset                : std_logic := '0'; -- ADC reset during PLL locking (PLL_LoL)
signal ADC_rx_sync              : std_logic := '0';
signal SPI_ADC_Csb_loc          : std_logic_vector(3 downto 1);
signal SPI_ADC_Din_loc          : std_logic;
signal SPI_ADC_Dout_loc         : std_logic;
signal SPI_ADC_clk_loc          : std_logic;
signal SPI_ADC_dir              : std_logic;
signal ADC_init_done            : std_logic;
signal ADC_init_done_prev       : std_logic;

signal PLL_I2C_error            : std_logic;
signal PLL_I2C_n_ack            : unsigned(7 downto 0);
signal PLL_reset                : std_logic;
signal PLL_reset_done           : std_logic;
signal PLL_init_done            : std_logic;
signal jesd_reset               : std_logic := '0'; -- JESD204 reset during ADC init
signal jesd_sys_reset           : std_logic := '0'; -- JESD204 main reset
signal jesd_ADC_reset           : std_logic := '0'; -- JESD204 main reset after ADC reset
signal start_jesd_ADC_delay     : std_logic := '0'; -- Start delay between ADC reset and jesd reset : wait for stabilization
signal jesd_ADC_delay           : unsigned(31 downto 0); -- Delay between ADC reset and jesd reset : wait for stabilization
signal axi_resetn               : std_logic := '1';

signal ADC1_clk_buf             : std_logic;
signal ADC2_clk_buf             : std_logic;
signal ADC3_clk_buf             : std_logic;

begin  -- architecture behavioral

  GPIO(0)                           <= '0';
  GPIO(1)                           <= '0';
  GPIO(2)                           <= '0';
  GPIO(4)                           <= '0';
  GPIO(5)                           <= '0';
  GPIO(6)                           <= '0';
  GPIO(7)                           <= '0';
  GPIO(8)                           <= '0';
  GPIO(9)                           <= '0';
  GPIO(10)                          <= '0';
  GPIO(11)                          <= '0';
  GPIO(12)                          <= '0';
  GPIO(13)                          <= '0';
  GPIO(14)                          <= '0';
  GPIO(15)                          <= '0';
  PLL1_lock_out                     <= (others => '1');
  PLL2_lock_out                     <= (others => '1');
  PLL3_lock_out                     <= (others => '1');
  PLLD_lock_out                     <= '1';
 
-- LMB and ADC monitoring :
  LMB_monitor.DCI_locked            <= '1';
  LMB_Monitor.firmware_ver          <= x"25030501";
  LMB_Monitor.board_SN              <= ADDR;
  LMB_monitor.LED_on                <= LMB_control.LED_on;
  LMB_monitor.WTE_pos               <= LMB_control.WTE_pos;
  LMB_monitor.force_local_WTE_pos   <= LMB_control.force_local_WTE_pos;
  LMB_monitor.gen_BC0               <= LMB_control.gen_BC0;
  LMB_monitor.gen_WTE               <= LMB_control.gen_WTE;
  LMB_monitor.trigger_mask          <= LMB_control.trigger_mask;
  LMB_monitor.trigger_delay         <= LMB_control.trigger_delay;
  LMB_monitor.update_delay_phase    <= LMB_control.update_delay_phase;
  LMB_monitor.trigger_width         <= LMB_control.trigger_width;
  LMB_monitor.trigger_soft          <= LMB_control.trigger_soft;
  LMB_monitor.extra_trigger         <= LMB_control.extra_trigger;
  LMB_monitor.self_trigger          <= LMB_control.self_trigger;
  LMB_monitor.self_trigger_mode     <= LMB_control.self_trigger_mode;
  LMB_monitor.self_trigger_thres    <= LMB_control.self_trigger_thres;
  LMB_monitor.self_trigger_mask     <= LMB_control.self_trigger_mask;
  LMB_monitor.trigger_inhibit_delay <= LMB_control.trigger_inhibit_delay;
  LMB_monitor.trigger_in            <= trigger_in;
  LMB_monitor.trigger_in_clear      <= LMB_control.trigger_in_clear;
  LMB_monitor.ADC1_AXI_addr         <= LMB_control.ADC1_AXI_addr;
  LMB_monitor.ADC2_AXI_addr         <= LMB_control.ADC2_AXI_addr;
  LMB_monitor.ADC3_AXI_addr         <= LMB_control.ADC3_AXI_addr;
  LMB_monitor.delay_scan            <= LMB_control.delay_scan;
  LMB_monitor.sw_delay              <= LMB_control.sw_delay;
  LMB_monitor.hw_delay              <= LMB_control.hw_delay;
  LMB_monitor.trigger_map           <= LMB_control.trigger_map;
  LMB_monitor.trigger_tap_value     <= LMB_control.trigger_tap_value;
  LMB_monitor.next_sequence_pos     <= next_sequence_pos;
  LMB_monitor.jesd_ADC_reset_delay  <= LMB_control.jesd_ADC_reset_delay; 

  ADC_monitor.I2C_PLL_reg           <= ADC_control.I2C_PLL_reg;
  ADC_monitor.SPI_ADC_R_Wb          <= ADC_control.SPI_ADC_R_Wb;
  ADC_monitor.SPI_ADC_number        <= ADC_control.SPI_ADC_number;
  ADC_monitor.SPI_ADC_reg           <= ADC_control.SPI_ADC_reg;
  ADC_monitor.PLL_update_reg        <= ADC_control.PLL_update_reg;
  ADC_monitor.PLL_update_val        <= ADC_control.PLL_update_val;

  inst_trig_gen : entity work.trigger_generation
  port map(
    LHC_clk                      => clk_40,
    serdes_clk                   => clk_80,
    system_clk                   => system_clk,
    reset                        => pwup_reset,
    resync_command               => ReSync_command,
    sig_trig_in                  => sig_trig_in,      -- Signal from outside world
    trigger_in                   => trigger_in,       -- trig_in latched, to be released by software
    sig_trig_out                 => sig_trig_out,     -- Signal to trig lasers. (0..3 = NIM, 4..7 = CMOS). Trigged from ReSync link or software
    trigger_delay                => LMB_monitor.trigger_delay,
    update_delay_phase           => LMB_monitor.update_delay_phase,
    trigger_width                => LMB_monitor.trigger_width,
    extra_trigger                => LMB_monitor.extra_trigger,
    trigger_inhibit_delay        => LMB_monitor.trigger_inhibit_delay,
    trigger_in_clear             => LMB_monitor.trigger_in_clear,
    trigger_mask                 => LMB_monitor.trigger_mask,
    trigger_tap_value            => LMB_monitor.trigger_tap_value,
    delay_scan                   => LMB_monitor.delay_scan,
    gen_BC0                      => LMB_monitor.gen_BC0,
    gen_WTE                      => LMB_monitor.gen_WTE,
    WTE_pos                      => LMB_monitor.WTE_pos,
    force_local_WTE_pos          => LMB_monitor.force_local_WTE_pos,
    BC0                          => BC0,
    TE                           => TE,
    WTE                          => WTE,
    next_lmr                     => next_lmr,
    next_sequence_pos            => next_sequence_pos,
    trigger_map                  => LMB_monitor.trigger_map
  );

  gen_soft_trigger : process(pwup_reset, sequence_clk, LMB_monitor.trigger_soft)
  begin
    if pwup_reset = '1' then
      trigger_software           <= '0';
      trigger_sw_width           <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trigger_sw_prev1           <= LMB_monitor.trigger_soft;
      trigger_sw_prev2           <= trigger_sw_prev1;
      if trigger_sw_prev1='1' and trigger_sw_prev2='0' then
        start_trigger_sw_delay   <= '1';
        trigger_sw_delay         <= (others => '0');
      end if;
      if start_trigger_sw_delay = '1'then
        if trigger_sw_delay < unsigned(LMB_monitor.sw_delay) then
          trigger_sw_delay       <= trigger_sw_delay +1;
        else
          start_trigger_sw_delay <= '0';
          trigger_sw_width       <= (others => '0');
          trigger_software       <= '1';
        end if;
      end if; 
      if trigger_software = '1' then
        if trigger_sw_width < x"4" then
          trigger_sw_width       <= trigger_sw_width+1;
        else
          trigger_software       <= '0';
        end if; 
      end if; 
    end if; 
  end process gen_soft_trigger;

  gen_hard_trigger : process(pwup_reset, sequence_clk, local_trigger)
  begin
    if pwup_reset = '1' then
      trigger_hardware           <= '0';
      trigger_hw_width           <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trigger_hw_prev1           <= local_trigger;
      trigger_hw_prev2           <= trigger_hw_prev1;
      if trigger_hw_prev1='1' and trigger_hw_prev2='0' then
        start_trigger_hw_delay   <= '1';
        trigger_hw_delay         <= (others => '0');
      end if;
      if start_trigger_hw_delay = '1'then
        if trigger_hw_delay < unsigned(LMB_monitor.hw_delay) then
          trigger_hw_delay       <= trigger_hw_delay +1;
        else
          start_trigger_hw_delay <= '0';
          trigger_hw_width       <= (others => '0');
          trigger_hardware       <= '1';
        end if;
      end if; 
      if trigger_hardware = '1' then
        if trigger_hw_width < x"4" then
          trigger_hw_width <= trigger_hw_width+1;
        else
          trigger_hardware <= '0';
        end if; 
      end if; 
    end if; 
  end process gen_hard_trigger;
  trigger_DAQ              <= trigger_hardware or trigger_software;

--  gen_pwup_reset : process (system_clk)
--  variable counter  : unsigned(15 downto 0) := (others => '0');
--  begin  -- process test_counter
--    if clock_generator_locked = '0' then
--      counter    := (others => '0');
--      pwup_reset <= '1';
--    elsif rising_edge(system_clk) then
--      pwup_reset <= '1';
--      if counter =  x"FFFF" then
--        pwup_reset <= '0';
--      else
--        counter  := counter + 1;
--      end if;
--    end if;
--  end process gen_pwup_reset;
  pwup_reset        <= not MMCM_locked;

-------------------------------------------------------------------------------
  -- Clocking
---- Clk port from local oscillator
  clk_IBUFGDS :    unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => TRUE) port map (O => loc_clk_in, I  => CLK_160_P, IB => CLK_160_N);
  CLK_in <= loc_clk_in;

  Inst_clk_ethernet : clk_generator_ethernet
  port map
  (
    clk_160_in      => Clk_in,
    clk_ethernet    => Clk_Gb_eth,
    clk_axi         => Clk_AXI,
    reset           => CLK_reset,
    locked          => delay_clock_locked
  );

  Inst_clk_generator_main : clk_generator_main
  port map
  (
    clk_160_in      => Clk_in,
    clk_20          => clk_20,
    clk_40          => Clk_40,
    clk_80          => Clk_80,
    clk_160         => Clk_160,
    clk_160_b       => Clk_160_b,
    clk_320         => Clk_320,
    reset           => CLK_reset,
    locked          => clock_generator_locked
  );

  MMCM_locked       <= clock_generator_locked and delay_clock_locked;

--clock for ADC and ADC capture
  Clk_IPbus         <= CLK_20;
  system_clk        <= CLK_160;
  sequence_clk      <= CLK_160;
  shift_reg_clk     <= CLK_160;
  memory_clk        <= CLK_160_b;
  resync_clk        <= CLK_160;
  clk_I2C           <= CLK_160;
  trigger_clk       <= Clk_320;


  -------------------------------------------------------------------------------
  -- Translation from ADC to FE
  -------------------------------------------------------------------------------  
  inst_slaves : entity work.slaves
  port map(
    reset                    => pwup_reset,
    trigger                  => trigger_DAQ,
    DAQ_busy                 => DAQ_busy,
    ipbus_clk                => clk_ipbus,
    ipbus_in                 => ipb_master_out,
    ipbus_out                => ipb_master_in,
    LMB_Monitor              => LMB_Monitor,
    LMB_Control              => LMB_Control,
    ADC_Monitor              => ADC_Monitor,
    ADC_Control              => ADC_Control,
    clk_counter              => clk_counter,
    clk_40                   => Clk_40,
    clk_160                  => Clk_160,
    clk_shift_reg            => shift_reg_clk,
    clk_memory               => memory_clk,
    ADC1_data_in             => ADC1_data,
    ADC1_data_valid          => ADC1_data_valid,
    ADC2_data_in             => ADC2_data,
    ADC2_data_valid          => ADC2_data_valid,
    ADC3_data_in             => ADC3_data,
    ADC3_data_valid          => ADC3_data_valid,
 
    ADC_DCDC_Temp            => ADC_DCDC_Temp,
    ADC_DCDC_Temp_ref        => ADC_DCDC_Temp_ref,
    FPGA_DCDC_Temp1          => FPGA_DCDC_Temp1,
    FPGA_DCDC_Temp1_ref      => FPGA_DCDC_Temp1_ref,
    FPGA_DCDC_Temp2          => FPGA_DCDC_Temp2,
    FPGA_DCDC_Temp2_ref      => FPGA_DCDC_Temp2_ref,
    
    WTE                      => WTE,
    local_trigger            => local_trigger,
    next_lmr                 => next_lmr,
    next_sequence_pos        => next_sequence_pos
  );
  -------------------------------------------------------------------------------
  -- Networking and IPBUS interface
  -------------------------------------------------------------------------------
  --ip_addr  <= x"0A0000"&"0001"&ADDR(3 downto 0);                             -- 10.0.0.(16..20)
  ip_addr  <= x"C0A800"&"0001"&ADDR(3 downto 0);                             -- 192.168.0.(16..20)
  mac_addr <= x"060264BEEF0"&not(ADDR); -- 06:02:64:BE:EF:XX
  
  Inst_gig_ethernet_pcs_pma_0 : gig_ethernet_pcs_pma_0
  port map (
    gtrefclk_p               => GbE_refclk_P,
    gtrefclk_n               => GbE_refclk_N,
    gtrefclk_out             => open,
    txp                      => Gbe_Tx_P,
    txn                      => Gbe_Tx_N,
    rxp                      => Gbe_Rx_P,
    rxn                      => Gbe_Rx_N,
    resetdone                => open,     --GbE_ready
    userclk_out              => open,
    userclk2_out             => GbE_user_clk,
    rxuserclk_out            => open,
    rxuserclk2_out           => open,
    pma_reset_out            => open,
--    mmcm_locked_out        => open,
    mmcm_locked_out          => GbE_mmcm_locked,
--    independent_clock_bufg => sequence_clk,--CLK_LHC,
    independent_clock_bufg   => CLK_Gb_eth,--CLK_LHC,
    gmii_txd                 => gmii_txd,
    gmii_tx_en               => gmii_tx_en,
    gmii_tx_er               => gmii_tx_er,
    gmii_rxd                 => gmii_rxd,
    gmii_rx_dv               => gmii_rx_dv,
    gmii_rx_er               => gmii_rx_er,
    gmii_isolate             => open,
    configuration_vector     => "00000",  --"00001",
    status_vector            => open,
    reset                    => '0',
    signal_detect            => '1',
    gtpowergood              => open 
  );

  frame_size                 <= x"05EE";
--  rx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "0X00X0000010";
--  tx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "XXX0X0000010";
  rx_configuration_vector    <= X"0000_0000_0000_0000_0812";
  tx_configuration_vector    <= X"0000_0000_0000_0000_0012";
  Inst_tri_mode_ethernet_mac : tri_mode_ethernet_mac_0
  port map (
    gtx_clk                  => GbE_user_clk,
    glbl_rstn                => '1',
    rx_axi_rstn              => '1',
    tx_axi_rstn              => '1',
    rx_statistics_vector     => open,
    rx_statistics_valid      => open,
    rx_mac_aclk              => open,
    rx_reset                 => open,
    rx_axis_mac_tdata        => mac_rx_data,
    rx_axis_mac_tvalid       => mac_rx_valid,
    rx_axis_mac_tlast        => mac_rx_last,
    rx_axis_mac_tuser        => mac_rx_error,
    tx_ifg_delay             => x"00",  --x"04",
    tx_statistics_vector     => open,
    tx_statistics_valid      => open,
    tx_mac_aclk              => open,
    tx_reset                 => open,
    tx_axis_mac_tdata        => mac_tx_data,
    tx_axis_mac_tvalid       => mac_tx_valid,
    tx_axis_mac_tlast        => mac_tx_last,
    tx_axis_mac_tuser        => mac_tx_error,
    tx_axis_mac_tready       => mac_tx_ready,
    pause_req                => '0',
    pause_val                => (others => '0'),
    speedis100               => open,
    speedis10100             => open,
    gmii_txd                 => gmii_txd,
    gmii_tx_en               => gmii_tx_en,
    gmii_tx_er               => gmii_tx_er,
    gmii_rxd                 => gmii_rxd,
    gmii_rx_dv               => gmii_rx_dv,
    gmii_rx_er               => gmii_rx_er,
    rx_configuration_vector  => rx_configuration_vector,
    tx_configuration_vector  => tx_configuration_vector
  );
  inst_ipbus : entity work.ipbus_ctrl
  port map(
    mac_clk                  => GbE_user_clk,
    rst_macclk               => '0',
    ipb_clk                  => clk_ipbus,
    rst_ipb                  => '0',
    mac_rx_data              => mac_rx_data,
    mac_rx_valid             => mac_rx_valid,
    mac_rx_last              => mac_rx_last,
    mac_rx_error             => mac_rx_error,
    mac_tx_data              => mac_tx_data,
    mac_tx_valid             => mac_tx_valid,
    mac_tx_last              => mac_tx_last,
    mac_tx_error             => mac_tx_error(0),
    mac_tx_ready             => mac_tx_ready,
    ipb_out                  => ipb_master_out,
    ipb_in                   => ipb_master_in,
    ipb_req                  => open,
    ipb_grant                => '1',
    mac_addr                 => mac_addr,
    ip_addr                  => ip_addr,
    enable                   => '1',
    ipam_select              => '0',            --: in std_logic := '0';
    actual_mac_addr          => actual_mac_addr,--: out std_logic_vector(47 downto 0); -- actual MAC and IP addresses
    actual_ip_addr           => actual_ip_addr, --: out std_logic_vector(31 downto 0);
    Got_IP_addr              => got_IP_addr,    --: OUT std_logic;
    pkt_oob                  => pkt_oob,
    pkt                      => pkt,
    oob_in                   => (others => ('0', X"00000000", '0')),
    oob_out                  => open
  );

--  LED      <= ADDR;
--  LED(7 downto 3) <= "10101";
  LED(0)                    <= GbE_mmcm_locked;
  LED(1)                    <= delay_clock_locked;
  LED(2)                    <= clock_generator_locked;
  LED(3)                    <= '1';
  test_counter : process (sequence_clk)
  begin  -- process test_counter
    if rising_edge(sequence_clk) then
      led_counter <= led_counter + 1;
      LED(4)                <= not led_counter(0);-- and LMB_Monitor.LED_on;
      LED(5)                <= led_counter(12);-- and LMB_Monitor.LED_on;
      LED(6)                <= led_counter(20);-- and LMB_Monitor.LED_on;
      LED(7)                <= led_counter(28);-- and LMB_Monitor.LED_on;
    end if;
  end process test_counter;

  -- SFP i2c bus
  SFP_SCL     <= 'Z';
  SFP_SDA     <= 'Z';
  
-- System initialisation :
-- Step 1 : init PLL and wait for stable 1280 MHz clock
-- PLL_reset should be at least 1 us long
  gen_PLL_reset : process (sequence_clk, mmcm_locked,ADC_control.PLL_reset, LMB_control.LMB_reset)
  variable PLL_reset_duration : unsigned(7 downto 0);
  begin
    if mmcm_locked='0' or ADC_control.PLL_reset = '1' or LMB_control.LMB_reset = '1' then
      PLL_reset   <= '1';
      PLL_reset_duration     := (others => '0');
    elsif Rising_Edge(sequence_clk) then
      if PLL_reset_duration < x"A0" then
        PLL_reset_duration   := PLL_reset_duration+1;
      else
        PLL_reset <= '0';
      end if;
    end if;
  end process gen_PLL_reset;

  PLL_Rstb                   <= not PLL_reset;
  DAC_CS <= "001" when ADC_control.I2C_PLL_number = 1 else 
            "010" when ADC_control.I2C_PLL_number = 2 else 
            "100" when ADC_control.I2C_PLL_number = 3 else 
            "000";
  inst_SHT4x_control : entity work.SHT4x_control
  port map(
    reset                    => pwup_reset,
    timer_off                => LMB_control.TH_timer_off,
    TH_extra                 => LMB_control.TH_access,        --: in     std_logic; 
    I2C_command              => LMB_control.TH_command,       --: in     std_logic_vector(7 downto 0);

    temperature              => LMB_monitor.temperature,      --: out     std_logic_vector(15 downto 0);
    humidity                 => LMB_monitor.humidity,         --: out     std_logic_vector(15 downto 0);

    I2C_scl                  => I2C_SCL_Ext_Temp,             --: inout  std_logic;
    I2C_sda                  => I2C_SDA_Ext_Temp,             --: inout  std_logic;
    I2C_clk                  => clk_I2C                       --: in     std_logic
  );
  inst_PLL_control : entity work.PLL_control
  port map(
    I2C_number               => ADC_control.I2C_PLL_number,   -- Same I2C bus for PLL(0) and pedestal DACs(1 to 3) 
    I2C_address_out          => ADC_monitor.I2C_PLL_address,  -- return actual device address 
    I2C_Access               => ADC_control.I2C_PLL_access,   --: in     std_logic; 
    I2C_R_Wb                 => ADC_control.I2C_R_Wb,         --: in     std_logic;
    I2C_Reg_number           => ADC_control.I2C_PLL_reg,      --: in     std_logic_vector(7 downto 0);
    PLL_update_reg           => ADC_control.PLL_update_reg,
    PLL_update_val           => ADC_control.PLL_update_val,
    PLL_update_en            => ADC_control.PLL_update_en,

    busy_out                 => ADC_monitor.I2C_PLL_busy,     --: out    std_logic;
    I2C_error                => PLL_I2C_error,                --: out    std_logic;
    I2C_Reg_data_in          => ADC_control.I2C_PLL_data,     --: in     std_logic_vector(7 downto 0);
    I2C_Reg_data_out         => ADC_monitor.I2C_PLL_data,     --: out    std_logic_vector(7 downto 0);
    I2C_n_ack                => ADC_monitor.I2C_PLL_n_ack,    --: out    unsigned(7 downto 0);

--    PLL_reset                => WTE,                        --: in     std_logic;
    PLL_reset                => PLL_reset,                    --: in     std_logic;
    PLL_reset_done           => PLL_reset_done,
    PLL_init_done            => PLL_init_done,
    I2C_scl                  => I2C_scl_PLL,                  --: inout  std_logic;
    I2C_sda                  => I2C_sda_PLL,                  --: inout  std_logic;
    I2C_clk                  => clk_I2C                       --: in     std_logic
  );

-- Step 2 : program ADC once clock is stable
  ADC_reset                  <= PLL_LoL or ADC_control.ADC_reset;
  ADC_monitor.SPI_ADC_number <= ADC_control.SPI_ADC_number;
  ADC_monitor.SPI_ADC_reg    <= ADC_control.SPI_ADC_reg;
  inst_ADC_control : entity work.ADC_ctrl
  port map(
    reset                    => ADC_reset,
    SPI_clk                  => clk_20,
    clk_160                  => clk_160,

    SPI_ADC_access           => ADC_control.SPI_ADC_access,
    SPI_ADC_number           => ADC_control.SPI_ADC_number,
    SPI_ADC_R_Wb             => ADC_control.SPI_ADC_R_Wb,
    SPI_ADC_reg_in           => ADC_control.SPI_ADC_reg,
    SPI_ADC_data_in          => ADC_control.SPI_ADC_data,
    SPI_ADC_data_out         => ADC_monitor.SPI_ADC_data,
    SPI_ADC_Clk              => SPI_ADC_clk_loc,
    SPI_ADC_Din              => SPI_ADC_Din_loc,
    SPI_ADC_Dout             => SPI_ADC_Dout_loc,
    SPI_ADC_Csb              => SPI_ADC_Csb_loc,
    SPI_ADC_dir              => SPI_ADC_dir,
    SPI_busy                 => ADC_monitor.SPI_busy,
    ADC_init_done            => ADC_init_done
  );
-- Map hardware signals with internal ones
  SPI_data_iobuf : IOBUF port map (IO => SPI_ADC_DIO,    O => SPI_ADC_Din_loc, I => SPI_ADC_Dout_loc, T  => SPI_ADC_dir);
  SPI_clk_obuf   : OBUF  port map (O  => SPI_ADC_clk,    I => SPI_ADC_clk_loc);
  SPI_csb_obuf1  : OBUF  port map (O  => SPI_ADC_csb(1), I => SPI_ADC_csb_loc(1));
  SPI_csb_obuf2  : OBUF  port map (O  => SPI_ADC_csb(2), I => SPI_ADC_csb_loc(2));
  SPI_csb_obuf3  : OBUF  port map (O  => SPI_ADC_csb(3), I => SPI_ADC_csb_loc(3));

-- Step 3 : synchronize GTX links :
-------------------------------------------------------------------------------
-- ADC interface
-------------------------------------------------------------------------------
-- Generate 1 us reset signal
  gen_JESD_sys_reset : process (LMB_control.JESD_sys_reset, clk_160)
  variable width : unsigned(7 downto 0) := (others => '0');
  begin
    if LMB_control.JESD_sys_reset = '1' then
      JESD_sys_reset      <= '1';
      width             := (others => '0');
    elsif rising_edge(clk_160) then
      if width < x"a0" then
        width           := width+1;
      else
        JESD_sys_reset    <= '0';
      end if;
    end if;
  end process gen_JESD_sys_reset;

  gen_JESD_ADC_reset : process (ADC_init_done, clk_160)
  variable width : unsigned(7 downto 0) := (others => '0');
  begin
    if ADC_init_done = '0' then
      JESD_ADC_reset           <= '0';
      start_JESD_ADC_delay     <= '0';
      ADC_init_done_prev       <= '0';
      width                    := (others => '0');
    elsif rising_edge(clk_160) then
      ADC_init_done_prev       <= ADC_init_done;
      if ADC_init_done = '1'and ADC_init_done_prev = '0' then
        start_JESD_ADC_delay   <= '1';
        JESD_ADC_delay         <= (others => '0');
      end if;
      if start_JESD_ADC_delay = '1' then
        if JESD_ADC_delay < LMB_monitor.JESD_ADC_reset_delay then
          JESD_ADC_delay       <= JESD_ADC_delay+1;
        else
          start_JESD_ADC_delay <= '0';
          JESD_ADC_reset       <= '1';
          width                := (others => '0');
        end if;
      end if;
      if JESD_ADC_reset = '1' then
        if width < x"a0" then
          width                := width+1;
        else
          JESD_ADC_reset       <= '0';
        end if;
      end if;
    end if;
  end process gen_JESD_ADC_reset;

  jesd_reset              <= JESD_ADC_reset or JESD_sys_reset;
  axi_resetn              <= not (LMB_control.axi_reset or JESD_ADC_reset);
  Inst_ADC_Input : entity work.ADC_Input
  port map (
    reset                 => jesd_reset,
    IO_reset              => LMB_control.IO_reset,
    AXI_aresetn           => axi_resetn,
    AXI_clk               => clk_AXI,
    DRP_clk               => clk_AXI,
    core_clk              => clk_160,

    ADC1_in0_P            => ADC1_RX0_P,
    ADC1_in0_N            => ADC1_RX0_N,
    ADC1_in1_P            => ADC1_RX1_P,
    ADC1_in1_N            => ADC1_RX1_N,
    ADC1_in2_P            => ADC1_RX2_P,
    ADC1_in2_N            => ADC1_RX2_N,
    ADC1_in3_P            => ADC1_RX3_P,
    ADC1_in3_N            => ADC1_RX3_N,
    ADC1_clk_P            => ADC1_refclk_P,
    ADC1_clk_N            => ADC1_refclk_N,
    ADC1_rx_sync          => ADC1_rx_sync,
    ADC1_data_out         => ADC1_data,
    ADC1_data_valid       => ADC1_data_valid,
    ADC1_baseline         => ADC_monitor.ADC_baseline(1),
    ADC1_charisk          => ADC1_charisk,
    ADC1_rx_disperror     => ADC1_rx_disperror,
    ADC1_rx_notintable    => ADC1_rx_notintable,
    ADC1_gt_powergood     => ADC1_gt_powergood,
    ADC1_reset_done       => ADC1_reset_done,
    ADC1_link_config      => LMB_monitor.ADC1_link_config,

    ADC2_in0_P            => ADC2_RX0_P,
    ADC2_in0_N            => ADC2_RX0_N,
    ADC2_in1_P            => ADC2_RX1_P,
    ADC2_in1_N            => ADC2_RX1_N,
    ADC2_in2_P            => ADC2_RX2_P,
    ADC2_in2_N            => ADC2_RX2_N,
    ADC2_in3_P            => ADC2_RX3_P,
    ADC2_in3_N            => ADC2_RX3_N,
    ADC2_clk_P            => ADC2_refclk_P,
    ADC2_clk_N            => ADC2_refclk_N,
    ADC2_rx_sync          => ADC2_rx_sync,
    ADC2_data_out         => ADC2_data,
    ADC2_data_valid       => ADC2_data_valid,
    ADC2_baseline         => ADC_monitor.ADC_baseline(2),
    ADC2_charisk          => ADC2_charisk,
    ADC2_rx_disperror     => ADC2_rx_disperror,
    ADC2_rx_notintable    => ADC2_rx_notintable,
    ADC2_gt_powergood     => ADC2_gt_powergood,
    ADC2_reset_done       => ADC2_reset_done,
    ADC2_link_config      => LMB_monitor.ADC2_link_config,

    ADC3_in0_P            => ADC3_RX0_P,
    ADC3_in0_N            => ADC3_RX0_N,
    ADC3_in1_P            => ADC3_RX1_P,
    ADC3_in1_N            => ADC3_RX1_N,
    ADC3_in2_P            => ADC3_RX2_P,
    ADC3_in2_N            => ADC3_RX2_N,
    ADC3_in3_P            => ADC3_RX3_P,
    ADC3_in3_N            => ADC3_RX3_N,
    ADC3_clk_P            => ADC3_refclk_P,
    ADC3_clk_N            => ADC3_refclk_N,
    ADC3_rx_sync          => ADC3_rx_sync,
    ADC3_data_out         => ADC3_data,
    ADC3_charisk          => ADC3_charisk,
    ADC3_rx_disperror     => ADC3_rx_disperror,
    ADC3_rx_notintable    => ADC3_rx_notintable,
    ADC3_data_valid       => ADC3_data_valid,
    ADC3_baseline         => ADC_monitor.ADC_baseline(3),
    ADC3_gt_powergood     => ADC3_gt_powergood,
    ADC3_reset_done       => ADC3_reset_done,
    ADC3_link_config      => LMB_monitor.ADC3_link_config,

    SPI_ADC_Clk           => SPI_ADC_Clk,
    SPI_ADC_DIO           => SPI_ADC_DIO,
    SPI_ADC_Csb           => SPI_ADC_Csb,

    ADC1_AXI_addr         => LMB_control.ADC1_AXI_addr,
    ADC1_AXI_data_in      => LMB_control.ADC1_AXI_data,
    ADC1_AXI_data_out     => LMB_monitor.ADC1_AXI_data,
    ADC1_AXI_access       => LMB_control.ADC1_AXI_access,
    ADC1_AXI_R_Wb         => LMB_control.ADC1_AXI_R_Wb,
    ADC2_AXI_addr         => LMB_control.ADC2_AXI_addr,
    ADC2_AXI_data_in      => LMB_control.ADC2_AXI_data,
    ADC2_AXI_data_out     => LMB_monitor.ADC2_AXI_data,
    ADC2_AXI_access       => LMB_control.ADC2_AXI_access,
    ADC2_AXI_R_Wb         => LMB_control.ADC2_AXI_R_Wb,
    ADC3_AXI_addr         => LMB_control.ADC3_AXI_addr,
    ADC3_AXI_data_in      => LMB_control.ADC3_AXI_data,
    ADC3_AXI_data_out     => LMB_monitor.ADC3_AXI_data,
    ADC3_AXI_access       => LMB_control.ADC3_AXI_access,
    ADC3_AXI_R_Wb         => LMB_control.ADC3_AXI_R_Wb
  );

-- ADCs synchronization :
  ADC_sysref              <= BC0;
  SysRef_OBUFDS :    unisim.vcomponents.OBUFDS port map (I => ADC_SysRef,   O  => SysRef_P,   OB => SysRef_N);
  ADC_rx_sync             <= ADC1_rx_sync and ADC2_rx_sync and ADC3_rx_sync;
  Sync_out_OBUFDS :  unisim.vcomponents.OBUFDS port map (I => ADC_rx_sync, O  => Sync_out_P, OB => Sync_out_N);

-- LiTE-DTU fast commands and I2C response :
  Resync_IBUFDS :    unisim.vcomponents.IBUFDS port map (O => Resync_in,   I  => Resync_in_P, IB => Resync_in_N);
  Inst_DTU_control : entity work.DTU_control -- All simulated DTUs for a given ADC will behave the same
  port map (
    clk_160               => Clk_160,
    reset                 => LMB_control.LMB_reset,
    I2C_registers         => I2C_data_DTU,
    I2C_SCL               => I2C_SCL_FE,
    I2C_SDA               => I2C_SDA_FE,
    baseline              => ADC_monitor.ADC_baseline,
    Resync                => Resync_in,
    ReSync_command        => ReSync_command
  );

-- Simulate LiTE-DTUs for ADC1 :
  ADC1_DTU_in(0)          <= ADC1_data(15  downto 0);
  ADC1_DTU_in(1)          <= ADC1_data(31  downto 16);
  ADC1_DTU_in(2)          <= ADC1_data(47  downto 32);
  ADC1_DTU_in(3)          <= ADC1_data(63  downto 48);
  ADC1_DTU_in(4)          <= ADC1_data(79  downto 64);
  ADC1_DTU_in(5)          <= ADC1_data(95  downto 80);
  ADC1_DTU_in(6)          <= ADC1_data(111 downto 96);
  ADC1_DTU_in(7)          <= ADC1_data(127 downto 112);
  ADC1_DTU_simul : for i in 0 to 7 generate
    Inst_DTU_simul : entity work.DTU_simul
    port map (
      clk_160             => Clk_160,
      ReSync_command      => ReSYnc_command,
      I2C_data_DTU        => I2C_data_DTU(1),
      DTU_data_in         => ADC1_DTU_in(i),
      DTU_data_out        => ADC1_DTU_out(i),
      cycle_pos           => ADC1_DTU_cycle_pos(i)
    );
  end generate;

-- Simulate LiTE-DTUs for ADC2 :  
  ADC2_DTU_in(0)          <= ADC2_data(15  downto 0);
  ADC2_DTU_in(1)          <= ADC2_data(31  downto 16);
  ADC2_DTU_in(2)          <= ADC2_data(47  downto 32);
  ADC2_DTU_in(3)          <= ADC2_data(63  downto 48);
  ADC2_DTU_in(4)          <= ADC2_data(79  downto 64);
  ADC2_DTU_in(5)          <= ADC2_data(95  downto 80);
  ADC2_DTU_in(6)          <= ADC2_data(111 downto 96);
  ADC2_DTU_in(7)          <= ADC2_data(127 downto 112);
  ADC2_DTU_simul : for i in 0 to 7 generate
    Inst_DTU_simul : entity work.DTU_simul
    port map (
      clk_160             => Clk_160,
      ReSync_command      => ReSYnc_command,
      I2C_data_DTU        => I2C_data_DTU(2),
      DTU_data_in         => ADC2_DTU_in(i),
      DTU_data_out        => ADC2_DTU_out(i),
      cycle_pos           => ADC2_DTU_cycle_pos(i)
    );
  end generate;

-- Simulate LiTE-DTUs for ADC3 :  
  ADC3_DTU_in(0)          <= ADC3_data(15  downto 0);
  ADC3_DTU_in(1)          <= ADC3_data(31  downto 16);
  ADC3_DTU_in(2)          <= ADC3_data(47  downto 32);
  ADC3_DTU_in(3)          <= ADC3_data(63  downto 48);
  ADC3_DTU_in(4)          <= ADC3_data(79  downto 64);
  ADC3_DTU_in(5)          <= ADC3_data(95  downto 80);
  ADC3_DTU_in(6)          <= ADC3_data(111 downto 96);
  ADC3_DTU_in(7)          <= ADC3_data(127 downto 112);
  ADC3_DTU_simul : for i in 0 to 7 generate
    Inst_DTU_simul : entity work.DTU_simul
    port map (
      clk_160             => Clk_160,
      ReSync_command      => ReSYnc_command,
      I2C_data_DTU        => I2C_data_DTU(3),
      DTU_data_in         => ADC3_DTU_in(i),
      DTU_data_out        => ADC3_DTU_out(i),
      cycle_pos           => ADC3_DTU_cycle_pos(i)
    );
  end generate;

-- Split temprature and humidity data in 6-bit data to send to LiTE-DTU as baseline samples
-- Generate an ADC like sample (16-bit word with 1 unused lsb and 2 control bits. msb are at 0) 
  Generate_TH_DTU_input: process (pwup_reset, sequence_clk)
  begin
    if pwup_reset = '1' then
      TH_generate_cycle   <= (others => '0');
      TH_DTU_in           <= (others => '0');
    elsif rising_edge(sequence_clk) then
      case TH_generate_cycle is
      when "000"=>
        TH_DTU_in         <= "0000000"& "111111"                                   &"000";
      when "001"=>
        TH_DTU_in         <= "0000000"& "11"&LMB_monitor.temperature(15 downto 12) &"000";
      when "010"=>
        TH_DTU_in         <= "0000000"& LMB_monitor.temperature(11 downto 6)       &"000";
      when "011"=>
        TH_DTU_in         <= "0000000"& LMB_monitor.temperature(5 downto 0)        &"000";
      when "100"=>
        TH_DTU_in         <= "0000000"& "000000"                                   &"000";
      when "101"=>
        TH_DTU_in         <= "0000000"& "00"&LMB_monitor.humidity(15 downto 12)    &"000";
      when "110"=>
        TH_DTU_in         <= "0000000"& LMB_monitor.humidity(11 downto 6)          &"000";
      when "111"=>
        TH_DTU_in         <= "0000000"& LMB_monitor.humidity(5 downto 0)           &"000";
      when others =>
        TH_DTU_in         <= "0000000"& "110011"                                   &"000";
      end case;
      TH_generate_cycle   <= TH_generate_cycle+1;
    end if;
  end process Generate_TH_DTU_input;

  Inst_TH_DTU_simul : entity work.DTU_simul
  port map (
    clk_160             => Clk_160,
    ReSync_command      => ReSYnc_command,
    I2C_data_DTU        => I2C_data_DTU(0),
    DTU_data_in         => TH_DTU_in,
    DTU_data_out        => TH_DTU_out,
    cycle_pos           => TH_DTU_cycle_pos
  );

  Inst_FE_Output : entity work.FE_Output
  port map (
    clk_80                => Clk_80,
    clk_160               => Clk_160,
    reset                 => LMB_control.LMB_reset,
    ADC1_out0_P           => ADC1_out0_P,
    ADC1_out0_N           => ADC1_out0_N,
    ADC1_out1_P           => ADC1_out1_P,
    ADC1_out1_N           => ADC1_out1_N,
    ADC1_out2_P           => ADC1_out2_P,
    ADC1_out2_N           => ADC1_out2_N,
    ADC1_out3_P           => ADC1_out3_P,
    ADC1_out3_N           => ADC1_out3_N,
    ADC1_out4_P           => ADC1_out4_P,
    ADC1_out4_N           => ADC1_out4_N,
    ADC1_out5_P           => ADC1_out5_P,
    ADC1_out5_N           => ADC1_out5_N,
    ADC1_out6_P           => ADC1_out6_P,
    ADC1_out6_N           => ADC1_out6_N,
    ADC1_out7_P           => ADC1_out7_P,
    ADC1_out7_N           => ADC1_out7_N,
    ADC2_out0_P           => ADC2_out0_P,
    ADC2_out0_N           => ADC2_out0_N,
    ADC2_out1_P           => ADC2_out1_P,
    ADC2_out1_N           => ADC2_out1_N,
    ADC2_out2_P           => ADC2_out2_P,
    ADC2_out2_N           => ADC2_out2_N,
    ADC2_out3_P           => ADC2_out3_P,
    ADC2_out3_N           => ADC2_out3_N,
    ADC2_out4_P           => ADC2_out4_P,
    ADC2_out4_N           => ADC2_out4_N,
    ADC2_out5_P           => ADC2_out5_P,
    ADC2_out5_N           => ADC2_out5_N,
    ADC2_out6_P           => ADC2_out6_P,
    ADC2_out6_N           => ADC2_out6_N,
    ADC2_out7_P           => ADC2_out7_P,
    ADC2_out7_N           => ADC2_out7_N,
    ADC3_out0_P           => ADC3_out0_P,
    ADC3_out0_N           => ADC3_out0_N,
    ADC3_out1_P           => ADC3_out1_P,
    ADC3_out1_N           => ADC3_out1_N,
    ADC3_out2_P           => ADC3_out2_P,
    ADC3_out2_N           => ADC3_out2_N,
    ADC3_out3_P           => ADC3_out3_P,
    ADC3_out3_N           => ADC3_out3_N,
    ADC3_out4_P           => ADC3_out4_P,
    ADC3_out4_N           => ADC3_out4_N,
    ADC3_out5_P           => ADC3_out5_P,
    ADC3_out5_N           => ADC3_out5_N,
    ADC3_out6_P           => ADC3_out6_P,
    ADC3_out6_N           => ADC3_out6_N,
    ADC3_out7_P           => ADC3_out7_P,
    ADC3_out7_N           => ADC3_out7_N,
    TH_out_P              => TH_out_P,
    TH_out_N              => TH_out_N,

    ADC1_DTU1_spy         => ADC1_DTU1_spy,
    ADC2_DTU1_spy         => ADC2_DTU1_spy,
    ADC3_DTU1_spy         => ADC3_DTU1_spy,
    ADC1_DTU_out_data     => ADC1_DTU_out,
    ADC2_DTU_out_data     => ADC2_DTU_out,
    ADC3_DTU_out_data     => ADC3_DTU_out,
    TH_DTU_out_data       => TH_DTU_out,
    cycle_pos             => ADC1_DTU_cycle_pos(0) -- Everybody shpuld be synchronized (common ReSync_command.DTU_reset
  );

end rtl;
