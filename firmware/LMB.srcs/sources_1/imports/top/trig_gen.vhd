library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity trigger_generation is
  port (
    LHC_clk                : in  std_logic;
    serdes_clk             : in  std_logic;
    system_clk             : in  std_logic;
    reset                  : in  std_logic;
    trigger_in             : out std_logic_vector(3 downto 0); -- trig_in latched. To be released by software
    ReSync_command         : in  ReSync_command_t;
    sig_trig_in            : in  std_logic_vector(3 downto 0);
    sig_trig_out           : out std_logic_vector(7 downto 0);
    trigger_delay          : in  UInt25_t(7 downto 0);
    update_delay_phase     : in  std_logic_vector(7 downto 0);
    trigger_width          : in  UInt7_t(7 downto 0);
    trigger_inhibit_delay  : in  unsigned(23 downto 0);
    extra_trigger          : in  std_logic;
    trigger_in_clear       : in  std_logic;
    trigger_mask           : in  std_logic_vector(7 downto 0);
    trigger_tap_value      : in  Word9_t(3 downto 1);
    delay_scan             : in  std_logic;
    gen_BC0                : in  std_logic;
    gen_WTE                : in  std_logic;
    WTE_pos                : in  unsigned(13 downto 0);
    force_local_WTE_pos    : in  std_logic;
    BC0                    : out std_logic;
    TE                     : out std_logic;
    WTE                    : out std_logic;
    next_lmr               : in  std_logic;
    next_sequence_pos      : in  std_logic_vector(31 downto 0);
    trigger_map            : in  std_logic_vector(31 downto 0)
  );
end entity trigger_generation;

architecture rtl of trigger_generation is

signal clk_counter              : unsigned(13 downto 0) := (others => '0');
signal orbit_counter            : unsigned(31 downto 0) := (others => '0');
signal orbit_counter_WTE        : unsigned(7 downto 0)  := (others => '0');
signal trig_delay_counter       : UInt21_t(7 downto 0)  := (others => (others => '0'));
signal trig_width_counter       : UInt9_t(7 downto 0)   := (others => (others => '0'));
signal trig_width_loc           : UInt9_t(7 downto 0)   := (others => (others => '0'));
signal trig_soft_del1           : std_logic_vector(7 downto 0)  := (others => '0');
signal trig_soft_del2           : std_logic_vector(7 downto 0)  := (others => '0');
signal WTE_del1                 : std_logic_vector(7 downto 0)  := (others => '0');
signal WTE_del2                 : std_logic_vector(7 downto 0)  := (others => '0');
signal update_delay_del1        : std_logic_vector(7 downto 0)  := (others => '0');
signal update_delay_del2        : std_logic_vector(7 downto 0)  := (others => '0');
signal update_delay_del3        : std_logic_vector(7 downto 0)  := (others => '0');
signal start_delay              : std_logic_vector(7 downto 0);
signal start_delay_del          : std_logic_vector(7 downto 0);
signal start_trig               : std_logic_vector(7 downto 0);
signal start_trig_del1          : std_logic_vector(7 downto 0);
signal start_trig_del2          : std_logic_vector(7 downto 0);
signal sig_trig_out_no_delay    : std_logic_vector(7 downto 0);
signal sig_trig_loc             : std_logic_vector(7 downto 0);
signal sig_trig_del             : std_logic_vector(7 downto 0);
--signal sig_trig_oserdes         : Word4_t(7 downto 0);
signal sig_trig_oserdes         : Byte_t(7 downto 0);
signal trigger_inhibit          : std_logic_vector(7 downto 0);
signal FE_BC0                   : std_logic;
signal FE_BC0_del               : std_logic;
signal FE_WTE                   : std_logic;
signal FE_WTE_del               : std_logic;
signal local_BC0                : std_logic;
signal local_WTE                : std_logic;
signal WTE_pos_loc              : unsigned(13 downto 0);
signal tx_cntvaluein            : Word9_t(7 downto 0);
signal tx_cntvalueout           : Word9_t(7 downto 0);
signal tx_load                  : std_logic_vector(7 downto 0);
signal tx_clk                   : std_logic;
signal clk_640_loc              : std_logic;
signal clk_320_loc              : std_logic;
signal clk_160_loc              : std_logic;
signal PLL_locked               : std_logic;

begin  -- architecture behavioral

  FE_BC0                              <= ReSync_command.BC0_marker;
  FE_WTE                              <= ReSync_command.laser_trigger;
  BC0                                 <= FE_BC0 or (local_BC0 and gen_BC0);
  WTE                                 <= FE_WTE or (local_WTE and gen_WTE);

  gen_BC0_WTE : process(reset, system_clk)
  variable WTE_width   : unsigned(7 downto 0); 
  begin
    if reset='1' then
      clk_counter                     <= (others => '0');
      orbit_counter                   <= (others => '0');
      orbit_counter_WTE               <= (others => '0');
      WTE_pos_loc                     <= (others => '0');
      local_WTE                       <= '0';
      local_BC0                       <= '0';
      TE                              <= '0';
    elsif rising_edge(system_clk) then
      FE_WTE_del                      <= FE_WTE;
      FE_BC0_del                      <= FE_BC0;
      if WTE_pos_loc = 0 then
        WTE_pos_loc                   <= WTE_pos;
      end if;

      if clk_counter < 14255 then                    -- LHC orbit = 3564 clock at 40 MHz
        clk_counter                   <= clk_counter+1;
      else
        local_BC0                     <= '1';
        clk_counter                   <= (others => '0');
      end if;
      if local_BC0 = '1' then -- BC0 duration : 1 clk
        local_BC0                     <= '0';
      end if;
-- Synchronize with FE board:
      if FE_BC0 = '1' and FE_BC0_del = '0' then
        clk_counter                   <= "00"&x"001";
      end if;

      if local_BC0 = '1' then
        orbit_counter                 <= orbit_counter+1;
        if orbit_counter_WTE < 111 then              -- 100 Hz ~ 1 orbit out of 112
          orbit_counter_WTE           <= orbit_counter_WTE+1;
        else
          orbit_counter_WTE           <= (others => '0');
        end if; 
      end if;
-- Synchronize with FE board:
      if FE_WTE = '1' and FE_WTE_del = '0' then
        if force_local_WTE_pos = '0' then
          WTE_pos_loc                 <= clk_counter-1;
        else
          WTE_pos_loc                 <= WTE_pos;
        end if;
        orbit_counter_WTE             <= (others => '0');
      end if;

      if orbit_counter_WTE = 0 and clk_counter = WTE_pos_loc then
        local_WTE                     <= '1';
      end if; 
      if local_WTE = '1' then -- WTE duration : 1 clk
        local_WTE                     <= '0';
      end if; 
    end if; 
  end process gen_BC0_WTE;

  trigger_generation : for itrig in 0 to 7 generate
-- Trigger mapping :
-- Inputs  : (0..7) : laser triggers from monitoring sequence, (8..d) : control signals
-- Outputs : (0..3) : CMOS, (4..7) : NIM
    start_trig(itrig)                 <= WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"0" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"1" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"2" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"3" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"4" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"5" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"6" else
                                         WTE or extra_trigger         when trigger_map(itrig*4+3 downto itrig*4) = x"7" else
                                         BC0                          when trigger_map(itrig*4+3 downto itrig*4) = x"8" else
                                         WTE                          when trigger_map(itrig*4+3 downto itrig*4) = x"9" else
                                         next_lmr                     when trigger_map(itrig*4+3 downto itrig*4) = x"a" else
                                         ReSync_command.CATIA_TP      when trigger_map(itrig*4+3 downto itrig*4) = x"b" else
                                         ReSync_command.laser_trigger when trigger_map(itrig*4+3 downto itrig*4) = x"c" else
                                         LHC_clk                      when trigger_map(itrig*4+3 downto itrig*4) = x"d" else
                                         '0';
    trig_width_loc(itrig)             <= trigger_width(itrig)&"00"; -- Actual width will be in multiple of 40 MHz clock period

-- Prepare parallel data to feed oserdes and get the trigger signals at the right place
-- First delay with 780 ps step using DDR @ 640 MHz
-- Fine tunig using ODELAY with 390 ps step.
    gen_trig_oserdes : process(reset, system_clk)
    variable count_inhibit            : unsigned(23 downto 0); 
    begin
      if reset = '1' then
        trigger_inhibit(itrig)        <= '0';
        count_inhibit                 := (others => '0');
        start_trig_del1(itrig)        <= '0';
        start_trig_del2(itrig)        <= '0';
        sig_trig_oserdes(itrig)       <= (others => '0');
        sig_trig_loc(itrig)           <= '0';
        trig_delay_counter(itrig)     <= (others => '0');
        trig_width_counter(itrig)     <= (others => '0');
      elsif rising_edge(system_clk) then
        start_trig_del1(itrig)        <= start_trig(itrig);
        start_trig_del2(itrig)        <= start_trig_del1(itrig);
        if start_trig_del1(itrig) = '1' and start_trig_del2(itrig) = '0' and trigger_inhibit(itrig) = '0' then
           start_delay(itrig)         <= '1';
           trig_delay_counter(itrig)  <= (others => '0');
        end if;
        if start_delay(itrig) = '1' then
          if trig_delay_counter(itrig) < trigger_delay(itrig)(24 downto 4) then
            trig_delay_counter(itrig) <= trig_delay_counter(itrig)+1;
          else
            start_delay(itrig)        <= '0';
-- Start "itrig" trigger only if the monitoring sequence ask for, or we we want a control signal :
            if next_sequence_pos(19 downto 16) = trigger_map(itrig*4+3 downto itrig*4) or unsigned(trigger_map(itrig*4+3 downto itrig*4)) >= 8 then
              sig_trig_loc(itrig)     <= trigger_mask(itrig);
            end if;
            trig_width_counter(itrig) <= to_unsigned(1,9);
          end if;
        end if;
        if sig_trig_loc(itrig) = '1' then
          if trig_width_counter(itrig) < trig_width_loc(itrig) then
            trig_width_counter(itrig) <= trig_width_counter(itrig)+1;
          else
            sig_trig_loc(itrig)       <= '0';
          end if;
        end if;
-- Delay tuning : 3.125 ns step
        sig_trig_del(itrig)           <= sig_trig_loc(itrig);
        case trigger_delay(itrig)(3 downto 2) is 
          when "00" =>
            sig_trig_oserdes(itrig)   <= sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&
                                         sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig);
          when "01" =>
            sig_trig_oserdes(itrig)   <= sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&
                                         sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig);
          when "10" => 
            sig_trig_oserdes(itrig)   <= sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&
                                         sig_trig_del(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig);
          when "11" => 
            sig_trig_oserdes(itrig)   <= sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig)&
                                         sig_trig_del(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig)&sig_trig_del(itrig);
          when others =>
            sig_trig_oserdes(itrig)   <= sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&
                                         sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig)&sig_trig_loc(itrig);
        end case;
        update_delay_del1(itrig)      <= update_delay_phase(itrig);
        update_delay_del2(itrig)      <= update_delay_del1(itrig);
        update_delay_del3(itrig)      <= update_delay_del2(itrig);
-- Delay fine tuning : 390 ps step
        tx_load(itrig)                <= '0';
        if update_delay_del2(itrig) = '1'and update_delay_del3(itrig) = '0' then
          tx_load(itrig)              <= '1';
        end if;
        if update_delay_del1(itrig) = '1'and update_delay_del2(itrig) = '0' then
          --tx_cntvaluein(itrig)    <= std_logic_vector(trigger_delay(itrig)(1 downto 0))&"0000000";
        case trigger_delay(itrig)(1 downto 0) is 
          when "00" =>
            tx_cntvaluein(itrig)    <= "000000000";
          when "01" =>
            tx_cntvaluein(itrig)    <= trigger_tap_value(1);
          when "10" =>
            tx_cntvaluein(itrig)    <= trigger_tap_value(2);
          when "11" =>
            tx_cntvaluein(itrig)    <= trigger_tap_value(3);
          when others =>
            tx_cntvaluein(itrig)    <= "000000000";
        end case;
      end if;

-- For the laser, we inhibit any retriggering before a delay which should be around 5 ms 
-- to be sure that the laser will not be over trigged by the TCDS system
        if start_delay(itrig) = '1' and  unsigned(trigger_map(itrig*4+3 downto itrig*4)) < 8 then
          count_inhibit               := (others => '0');
          trigger_inhibit(itrig)      <= '1';
        end if;
-- Inhibit other laser triggers for a period of ~10 ms : (Avoid laser misfunction)
        if trigger_inhibit(itrig) = '1' then
          count_inhibit               := count_inhibit+1;
          if count_inhibit > trigger_inhibit_delay then
            trigger_inhibit(itrig)    <= '0';
          end if; 
        end if; 
      end if;
    end process gen_trig_oserdes;
  end generate;

  inst_trigger_PLL : entity work.clk_trigger_gen
  port map(
    clk_640_out => clk_640_loc,
    clk_320_out => clk_320_loc,
    clk_160_out => clk_160_loc,
    reset       => reset,
    locked      => PLL_locked,
    clk_160_in  => system_clk
  );

-- To fine tune the trigger output position, we use a DDR oserdes @ 640 MHz. Each tap represents 781.25 ps
-- Should not be used with HR bank accordng to data sheet... but it seems to work.
-- By the way, we just use it as a delay line, not a data transmission unit with bit changing at 1280 MHz 
  oserdes_generation : for iout in 0 to 7 generate
    inst_OSERDESE3_out0 : OSERDESE3
    generic map (
      DATA_WIDTH         => 8,               -- Parallel Data Width (4-8)
      INIT               => '0',             -- Initialization value of the OSERDES flip-flops
      IS_CLKDIV_INVERTED => '0',             -- Optional inversion for CLKDIV
      IS_CLK_INVERTED    => '0',             -- Optional inversion for CLK
      IS_RST_INVERTED    => '0',             -- Optional inversion for RST
      SIM_DEVICE         => "ULTRASCALE"     -- Set the device version for simulation functionality (ULTRASCALE,
                                             -- ULTRASCALE_PLUS, ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
    )
    port map (
      OQ     => sig_trig_out_no_delay(iout), -- 1-bit output: Serial Output Data
      T_OUT  => open,                        -- 1-bit output: 3-state control output to IOB
      CLK    => clk_640_loc,                 -- 1-bit input: High-speed clock
      CLKDIV => clk_160_loc,                 -- 1-bit input: Divided Clock
      D      => sig_trig_oserdes(iout),      -- 8-bit input: Parallel Data Input
      RST    => reset,                       -- 1-bit input: Asynchronous Reset
      T      => '0'                          -- 1-bit input: Tristate input from fabric
    );
-- Very fine tune using ODELAY3 : Just one step of ~390 ps
-- Could go lower if needed
    ODELAYE3_inst : ODELAYE3
    generic map (
      CASCADE          => "NONE",            -- Cascade setting (MASTER, NONE, SLAVE_END, SLAVE_MIDDLE)
      DELAY_FORMAT     => "COUNT",           -- (COUNT, TIME)
      DELAY_TYPE       => "VAR_LOAD",        -- Set the type of tap delay line (FIXED, VARIABLE, VAR_LOAD)
      DELAY_VALUE      => 0,                 -- Output delay tap setting
      IS_CLK_INVERTED  => '0',               -- Optional inversion for CLK
      IS_RST_INVERTED  => '0',               -- Optional inversion for RST
      REFCLK_FREQUENCY => 320.0,             -- IDELAYCTRL clock input frequency in MHz (200.0-800.0).
      SIM_DEVICE       => "ULTRASCALE",      -- Set the device version for simulation functionality (ULTRASCALE)
      UPDATE_MODE      => "ASYNC"            -- Determines when updates to the delay will take effect (ASYNC, MANUAL, SYNC)
    )
    port map (
      CASC_OUT    => open,                        -- 1-bit output: Cascade delay output to IDELAY input cascade
      CNTVALUEOUT => tx_cntvalueout(iout),        -- 9-bit output: Counter value output
      DATAOUT     => sig_trig_out(iout),          -- 1-bit output: Delayed data from ODATAIN input port
      CASC_IN     => '0',                         -- 1-bit input: Cascade delay input from slave IDELAY CASCADE_OUT
      CASC_RETURN => '0',                         -- 1-bit input: Cascade delay returning from slave IDELAY DATAOUT
      CE          => '0',                         -- 1-bit input: Active-High enable increment/decrement input
      CLK         => clk_160_loc,                 -- 1-bit input: Clock input
      CNTVALUEIN  => tx_cntvaluein(iout),         -- 9-bit input: Counter value input
      EN_VTC      =>'0',                          -- 1-bit input: Keep delay constant over VT
      INC         => '0',                         -- 1-bit input: Increment/Decrement tap delay input
      LOAD        => tx_load(iout),               -- 1-bit input: Load DELAY_VALUE input
      ODATAIN     => sig_trig_out_no_delay(iout), -- 1-bit input: Data input
      RST         => reset                        -- 1-bit input: Asynchronous Reset to the DELAY_VALUE
   );
  end generate;

  IDELAYCTRL_inst : IDELAYCTRL
  generic map (
    SIM_DEVICE => "ULTRASCALE"  -- Set the device version for simulation functionality (ULTRASCALE)
  )
  port map (
    RDY    => open,         -- 1-bit output: Ready output
    REFCLK => clk_320_loc,  -- 1-bit input: Reference clock input
    RST    => reset         -- 1-bit input: Active-High reset input. Asynchronous assert, synchronous deassert to REFCLK.
  );

  gen_trig_in_all : for i in 0 to 3 generate
    get_trig_in : process(sig_trig_in(i), trigger_in_clear)
    begin
      if sig_trig_in(i) = '1' then
        trigger_in(i)                 <= '1';
      elsif trigger_in_clear = '1' then
        trigger_in(i)                 <= '0';
      end if;
    end process get_trig_in;
  end generate;
end architecture rtl;
