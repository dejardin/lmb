--------------------------------------------------------------------------------
-- Company: CEA-Saclay
-- Engineer: M.D.
--
-- Create Date: 01/11/2023
-- Design Name: LMB
-- Component Name: DTU_control
-- Target Device: <target device>
-- Tool versions: Vivado 22.02
-- Description:
--    Simulate control unit and I2C unit of LiTE-DTU
-- Only 3 LiTE-DTUs are considered, one per ADC. All 8 DTUs belonging to teh same ADC have the same parameters
-- Dependencies:
--    LMB_IO
-- Revision:
--    1.0 : Frst working version
-- Additional Comments:
--    <Additional_comments>
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity DTU_control is
  port (
    clk_160                : in    std_logic;
    reset                  : in    std_logic;
    I2C_registers          : out   DTU_reg_t(N_ADC downto 0);
    I2C_scl                : in    std_logic;
    I2C_sda                : inout std_logic;
    baseline               : in    UInt14_t(N_ADC downto 1);

    Resync                 : in    std_logic;
    ReSync_command         : out   ReSync_command_t
  );
end entity DTU_control;

architecture rtl of DTU_control is

signal SCL_in                    : std_logic := '0';
signal SDA_in                    : std_logic := '0';
signal SDA_out                   : std_logic := '0';
signal I2C_registers_loc         : DTU_reg_t(N_ADC downto 0);
signal ReSync_command_loc        : ReSync_command_t;

signal ReSync_data               : std_logic_vector(7 downto 0) := (others => '0');
signal ReSync_start              : std_logic := '0';
signal ReSync_stop               : std_logic := '0';
type   ReSync_state_t            is (ReSync_decoding,        ReSync_wait_for_next_byte,
                                     ReSync_Idle);
signal ReSync_state              : ReSYnc_state_t := ReSync_idle;
signal ReSync_CATIA_start_TP     : std_logic := '0';
signal ReSync_CATIA_start_TP_del : std_logic := '0';
signal nbit                      : unsigned(2 downto 0);
signal TP_duration               : unsigned(9 downto 0); -- Duration in 40 MHz clock in LiTE-DTU

begin

  ReSync_command           <= ReSync_command_loc;
  I2C_registers            <= I2C_registers_loc;

-- I2C interface
  Inst_DTU_I2C : entity work.I2C_slave
  port map(
    clk               => clk_160,
    reset             => ReSync_command_loc.I2C_reset,
    scl_i             => SCL_in,
    sda_i             => SDA_in,
    sda_o             => SDA_out,
    mem               => I2C_registers_loc,
    baseline          => baseline
  );

  Inst_SCL : IBUF   port map (O => SCL_in,   I => I2C_scl);
  Inst_SDA : IOBUF  port map (O => SDA_in,  IO => I2C_sda, I => '0', T => SDA_out);

  ReSync_FSM : process(clk_160,reset)
  begin
    if reset = '1' then
      ReSync_start                           <= '0';
      ReSync_stop                            <= '0';
      ReSync_state                           <= ReSync_Idle;
      ReSync_data                            <= (others => '0');
      ReSync_command_loc.DTU_sync_mode       <= '0';
      ReSync_command_loc.DTU_normal_mode     <= '1';
    elsif rising_edge(clk_160) then
      ReSync_command_loc.DTU_reset           <= '0';
      ReSync_command_loc.I2C_reset           <= '0';
      ReSync_command_loc.TestUnit_reset      <= '0';
      ReSync_command_loc.DTU_flush           <= '0';
      ReSync_command_loc.ADCH_reset          <= '0';
      ReSync_command_loc.ADCH_calib          <= '0';
      ReSync_command_loc.ADCL_reset          <= '0';
      ReSync_command_loc.ADCL_calib          <= '0';
      ReSync_command_loc.laser_trigger       <= '0';
      ReSync_command_loc.BC0_marker          <= '0';
      ReSync_command_loc.PLL_reset           <= '0';
      ReSync_CATIA_start_TP                  <= '0';
      ReSync_start                           <= '0';
      ReSync_stop                            <= '0';
      ReSync_data <= ReSYnc_data(6 downto 0) & ReSync;
      case ReSync_state is 
      when ReSync_Idle =>
        if ReSync_data = x"07" then
          ReSync_start                       <= '1';
          nbit                               <= "001";
          ReSync_state                       <= ReSync_wait_for_next_byte;
        end if;
      when ReSync_wait_for_next_byte =>
        nbit                                 <= nbit+1;
        if nbit = 7 then
          ReSync_state                       <= ReSync_decoding;
        end if;
      when ReSync_decoding =>
        case ReSync_data is
        when x"00" =>
          ReSync_stop                        <= '1';
          ReSync_state                       <= ReSync_idle;
        when x"19" =>
          ReSync_command_loc.DTU_reset       <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"1E" =>
          ReSync_command_loc.I2C_reset       <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"2A" =>
          ReSync_command_loc.TestUnit_reset  <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"2D" =>
          ReSync_command_loc.DTU_sync_mode   <= '1';
          ReSync_command_loc.DTU_normal_mode <= '0';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"33" =>
          ReSync_command_loc.DTU_sync_mode   <= '0';
          ReSync_command_loc.DTU_normal_mode <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"34" =>
          ReSync_command_loc.DTU_flush       <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"4B" =>
          ReSync_command_loc.ADCH_reset      <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"4C" =>
          ReSync_command_loc.ADCH_calib      <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"52" =>
          ReSync_command_loc.ADCL_reset      <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"55" =>
          ReSync_command_loc.ADCL_calib      <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"61" =>
          ReSync_command_loc.laser_trigger   <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"66" =>
          ReSync_CATIA_start_TP              <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"78" =>
          ReSync_command_loc.BC0_marker      <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when x"7F" =>
          ReSync_command_loc.PLL_reset       <= '1';
          ReSync_state                       <= ReSync_wait_for_next_byte;
        when others =>
          ReSync_stop                        <= '1';
          ReSync_state                       <= ReSync_idle;
          ReSync_data                        <= (others => '0');
        end case;
      when others =>
      end case;
    end if;
  end process ReSync_FSM;

  gen_TP : process(clk_160, reset)
  begin
    if reset = '1' then
      ReSync_command_loc.CATIA_TP     <= '0';
    elsif rising_edge(clk_160) then
      ReSync_Catia_start_TP_del       <= ReSync_CATIA_start_TP;
      if ReSync_CATIA_start_TP = '1' and ReSync_Catia_start_TP_del = '0' then
        ReSync_command_loc.CATIA_TP   <= '1';
        TP_duration                   <= x"01"&"00";
      end if;
      if ReSync_command_loc.CATIA_TP = '1' then
        if TP_duration(9 downto 2) < unsigned(I2C_registers_loc(1)(25)) then
          TP_duration                 <= TP_duration+1;
        else
          ReSync_command_loc.CATIA_TP <= '0';
        end if;
      end if;
    end if;
  end process gen_TP;

end architecture rtl;
