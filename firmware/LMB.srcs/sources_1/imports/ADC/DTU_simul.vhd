library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity DTU_simul is
  generic(
    FIFO_width      : natural := 32;
    FIFO_depth      : natural := 16;
    frame_length    : natural := 50
  );
  port (
    clk_160                : in  std_logic;
    ReSync_command         : in  ReSync_command_t;
    I2C_data_DTU           : in  Byte_t(N_DTU_registers-1 downto 0);
    DTU_data_in            : in  std_logic_vector(15 downto 0); -- Input samples at 160 MHz
    DTU_data_out           : out std_logic_vector(31 downto 0); -- Output words at 40 MHz
    cycle_pos              : out unsigned(1 downto 0)           -- 160 MHz clk wrt 40 MHz clk (for oserdes synchro)
  );
end entity DTU_simul;

architecture rtl of DTU_simul is

type FIFO_t is array (FIFO_depth-1 downto 0) of std_logic_vector(FIFO_width-1 downto 0);
signal FIFO : FIFO_t;
subtype index_t is integer range FIFO_t'range;
signal FIFO_write            : std_logic := '0';
signal FIFO_read             : std_logic := '0';
signal FIFO_full             : std_logic := '0';
signal FIFO_full_next        : std_logic := '0';
signal FIFO_empty            : std_logic := '0';
signal FIFO_empty_next       : std_logic := '0';
signal write_addr            : index_t;
signal read_addr             : index_t;
signal fill_count            : integer range FIFO_depth-1 downto 0;
signal FIFO_in               : std_logic_vector(31 downto 0);
signal next_FIFO_in          : std_logic_vector(31 downto 0);
signal FIFO_in_cur           : std_logic_vector(31 downto 0);
signal clk_counter           : unsigned(1 downto 0);
signal n_bl_samples          : natural range 0 to 5 := 0;
signal n_sig_samples         : natural range 0 to 1 := 0;
signal ReSync_BC0_marker_del : std_logic := '0';
signal cur_samples_frame     : natural range 0 to frame_length*5 := 0;
signal cur_frame_length      : natural range 0 to frame_length*2 := 0;
signal frame_number          : natural range 0 to 255 := 0;
signal next_data_in          : std_logic_vector(12 downto 0);
signal BL_G10                : unsigned(7 downto 0);
signal DTU_flush_mode        : std_logic := '0';
signal DTU_sync_mode         : std_logic := '0';
signal DTU_normal_mode       : std_logic := '1';
signal half_sampling_mode    : std_logic := '0'; -- 80 MHz mode
signal SoF                   : std_logic := '0'; -- Start of Frame
signal pos_from_BC0          : unsigned(13 downto 0);

function CRC_calc(data : std_logic_vector; crc : std_logic_vector) return std_logic_vector is
  variable loc_crc : std_logic_vector(11 downto 0) := (others => '0');
begin
 loc_crc(0)  := data(30) xor data(29) xor data(26) xor data(25) xor data(24) xor data(23) xor data(22) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(6) xor crc(9) xor crc(10) ;
 loc_crc(1)  := data(31) xor data(29) xor data(27) xor data(22) xor data(18) xor data(11) xor data(9)  xor data(0)  xor crc(2)   xor crc(7)   xor crc(9)   xor crc(11) ;
 loc_crc(2)  := data(29) xor data(28) xor data(26) xor data(25) xor data(24) xor data(22) xor data(19) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(11) xor data(10) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(0) xor crc(2) xor crc(4) xor crc(5) xor crc(6) xor crc(8) xor crc(9) ;
 loc_crc(3)  := data(27) xor data(24) xor data(22) xor data(20) xor data(18) xor data(13) xor data(9)  xor data(2)  xor data(0)  xor crc(0)   xor crc(2)   xor crc(4)   xor crc(7) ;
 loc_crc(4)  := data(28) xor data(25) xor data(23) xor data(21) xor data(19) xor data(14) xor data(10) xor data(3)  xor data(1)  xor crc(1)   xor crc(3)   xor crc(5)   xor crc(8) ;
 loc_crc(5)  := data(29) xor data(26) xor data(24) xor data(22) xor data(20) xor data(15) xor data(11) xor data(4)  xor data(2)  xor crc(0)   xor crc(2)   xor crc(4)   xor crc(6)   xor crc(9) ;
 loc_crc(6)  := data(30) xor data(27) xor data(25) xor data(23) xor data(21) xor data(16) xor data(12) xor data(5)  xor data(3)  xor crc(1)   xor crc(3)   xor crc(5)   xor crc(7)   xor crc(10) ;
 loc_crc(7)  := data(31) xor data(28) xor data(26) xor data(24) xor data(22) xor data(17) xor data(13) xor data(6)  xor data(4)  xor crc(2)   xor crc(4)   xor crc(6)   xor crc(8)   xor crc(11) ;
 loc_crc(8)  := data(29) xor data(27) xor data(25) xor data(23) xor data(18) xor data(14) xor data(7)  xor data(5)  xor crc(3)   xor crc(5)   xor crc(7)   xor crc(9) ;
 loc_crc(9)  := data(30) xor data(28) xor data(26) xor data(24) xor data(19) xor data(15) xor data(8)  xor data(6)  xor crc(4)   xor crc(6)   xor crc(8)   xor crc(10) ;
 loc_crc(10) := data(31) xor data(29) xor data(27) xor data(25) xor data(20) xor data(16) xor data(9)  xor data(7)  xor crc(0)   xor crc(5)   xor crc(7)   xor crc(9)   xor crc(11) ;
 loc_crc(11) := data(29) xor data(28) xor data(25) xor data(24) xor data(23) xor data(22) xor data(21) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(10) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(1) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(8) xor crc(9);
 return loc_crc; 
end function;

-- Increment and wrap
procedure incr(signal index : inout index_t) is
begin
  if index = index_t'high then
    index <= index_t'low;
  else
    index <= index + 1;
  end if;
end procedure;

begin  -- architecture behavioral

-- Set the flags
  FIFO_empty         <= '1' when fill_count = 0 else '0';
  FIFO_empty_next    <= '1' when fill_count <= 1 else '0';
  FIFO_full          <= '1' when fill_count >= FIFO_depth - 1 else '0';
  FIFO_full_next     <= '1' when fill_count >= FIFO_depth - 2 else '0';
  BL_G10             <= unsigned(I2C_data_DTU(5));
  half_sampling_mode <= I2C_data_DTU(1)(5);
  cycle_pos          <= clk_counter;

-- Update the fill count
  proc_COUNT : process(write_addr, read_addr)
  begin
    if write_addr < read_addr then
      fill_count    <= write_addr - read_addr + FIFO_depth;
    else
      fill_count    <= write_addr - read_addr;
    end if;
  end process;
  
  manage_FIFO : process(clk_160, ReSync_command.DTU_reset)
  variable cur_CRC               : std_logic_vector(11 downto 0) := (others => '0');
  variable FD_enable             : std_logic; -- Allow to send frame delimiter
  begin
    if ReSync_command.DTU_reset = '1' then -- asynchronous DTU_reset 
      write_addr            <= 0;
      read_addr             <= 0;
      clk_counter           <= (others => '0');
      DTU_data_out          <= x"EAAAAAAA";
      FIFO_in               <= x"EAAAAAAA";
      next_FIFO_in          <= x"EAAAAAAA";
      FIFO_in_cur           <= x"EAAAAAAA";
      cur_samples_frame     <= 0;
      cur_frame_length      <= 0;
      frame_number          <= 0;
      DTU_flush_mode        <= '0';
    elsif rising_edge(clk_160) then
      FD_enable             := '1';
      FIFO_write            <= '0';
      clk_counter           <= clk_counter+1;
      ReSync_BC0_marker_del <= ReSync_command.BC0_marker;

-- Look at ADC data. Take only 13 msb out of 14 bits (remove noise)
-- Then decide if it is a baseline or signal sample
      if unsigned(DTU_data_in(15 downto 3)) > BL_G10 then
        if I2C_data_DTU(8)(6) = '1' then -- Divide by 2
          next_data_in      <= std_logic_vector((unsigned(DTU_data_in(15 downto 3)) - BL_G10) srl 1);
        elsif I2C_data_DTU(8)(7) = '1' then -- Divide by 4
          next_data_in      <= std_logic_vector((unsigned(DTU_data_in(15 downto 3)) - BL_G10) srl 2);
        else
          next_data_in      <= std_logic_vector(unsigned(DTU_data_in(15 downto 3))  - BL_G10); -- Use only 13 bits out of 14 from the ADC, b12 = gain bit
        end if;
        if DTU_data_in(1) = '1' then -- Force sample at 0 when we have de BC0 seen by ADC through Sync line
          next_data_in      <= (others => '0');
        end if;
      else
        next_data_in        <= (others => '0');
      end if;

      if ReSync_command.DTU_sync_mode = '1' then
        DTU_sync_mode       <= '1';
        DTU_normal_mode     <= '0';
        cur_CRC             := (others => '0');
      end if;
      if ReSync_command.DTU_normal_mode = '1' then
        DTU_sync_mode       <= '0';
        DTU_normal_mode     <= '1';
      end if;

-- Write a word in FIFO asynchrounsly wrt 40 MHz clock, if we have enough data.
      FIFO(write_addr)      <= FIFO_in;
      if FIFO_write = '1' then
        if next_FIFO_in(31 downto 30) /= "11" and DTU_sync_mode = '0' then -- Compute CRCs only on data, not on control words or in synchro mode
          cur_CRC           := CRC_calc(next_FIFO_in, cur_CRC);
        end if;
        if SoF = '1' then
          cur_CRC           := (others => '0');
          SoF               <= '0';
        end if;
        if FIFO_full = '0' then
          incr(write_addr);
          FIFO_in           <= next_FIFO_in;
        end if;
      end if;

-- On 40 MhZ clock edge, increment read_address
      if clk_counter = 0 then
-- Always read data from FIFO or send idle word if FIFO empty :
        cur_frame_length  <= cur_frame_length + 1;
        if FIFO_empty = '0' then
          incr(read_addr);
          FIFO_read         <= '1';
        else
          FIFO_read         <= '0';
        end if;
        if DTU_flush_mode = '1' then
          DTU_data_out      <= x"2CF0F0F0";
          DTU_flush_mode    <= '0';
          cur_frame_length  <= 0;
        elsif DTU_sync_mode = '1' then
          DTU_data_out      <= I2C_data_DTU(21)&I2C_data_DTU(22)&I2C_data_DTU(23)&I2C_data_DTU(24);
          cur_frame_length  <= 0;
          FD_enable         := '0';
        elsif FIFO_read = '1' then
          DTU_data_out      <= FIFO(read_addr);
        else
          DTU_data_out      <= x"EAAA"&"10"&std_logic_vector(pos_from_BC0);
        end if;
      end if;

      if DTU_normal_mode= '1' then
-- Save previous BC0 marked sample if we had one in the pipe
        if ReSync_BC0_marker_del = '1' then
          next_FIFO_in        <= FIFO_in_cur;
          FIFO_write          <= '1';
          FD_enable           := '0';
        end if;

-- On DTU fluch, reset the FIFO parameters
        if ReSync_command.DTU_flush = '1' then
          write_addr          <= 0;
          read_addr           <= 0;
          frame_number        <= 0;
          cur_CRC             := (others => '0');
          cur_samples_frame   <= 0;
          FIFO_write          <= '1';
          DTU_flush_mode      <= '1';
          DTU_data_out        <= x"2CF0F0F0";
          FIFO_in             <= x"2CF0F0F0";
          next_FIFO_in        <= x"2CF0F0F0";
          FIFO_in_cur         <= x"2CF0F0F0";
          pos_from_BC0        <= (others => '0');
-- Then BC0 has got the highest priority :
        elsif ReSync_command.BC0_marker = '1' then
-- Close opened baseline or signal word :
          if n_sig_samples   = 1 then
            next_FIFO_in      <= "0010110101010101010"&FIFO_in_cur(12 downto 0);
          elsif n_bl_samples = 1 then
            next_FIFO_in      <= "10000001000000000000000000"&FIFO_in_cur(5 downto 0);
          elsif n_bl_samples = 2 then
            next_FIFO_in      <= "10000010000000000000"&FIFO_in_cur(11 downto 0);
          elsif n_bl_samples = 3 then
            next_FIFO_in      <= "10000011000000"&FIFO_in_cur(17 downto 0);
          elsif n_bl_samples = 4 then
            next_FIFO_in      <= "10000100"&FIFO_in_cur(23 downto 0);
          end if;
          n_sig_samples       <= 0;
          n_bl_samples        <= 0;
          FIFO_write          <= '1';
-- And mark present sample with BC0 :
          if n_sig_samples = 0 and n_bl_samples = 0 then -- If we had nothing in pipeline, send it at next word
            next_FIFO_in      <= "0010111111000001111"&next_data_in;
            ReSync_BC0_marker_del <= '0';
          else                                           -- else, send it after just closed data word
            FIFO_in_cur       <= "0010111111000001111"&next_data_in;
            cur_samples_frame <= cur_samples_frame+1;
          end if;
          FD_enable           := '0'; -- postpone any Frame delimiter since next write is booked with BC0 marked sample
          pos_from_BC0        <= (others => '0');

-- Treat Baseline samples :
        elsif unsigned(next_data_in) < 64 then
-- Close opened signal word if any (Signal data format 1)
          if n_sig_samples > 0 then
            next_FIFO_in      <= "0010110101010101010"&FIFO_in_cur(12 downto 0);
            n_sig_samples     <= 0;
            FIFO_write        <= '1';
            FD_enable         := '0';
          end if;
-- And fill or close baseline word :
          if n_bl_samples < 4 then
            FIFO_in_cur((n_bl_samples+1)*6-1 downto (n_bl_samples*6)) <= next_data_in(5 downto 0);
            n_bl_samples      <= n_bl_samples + 1;
            cur_samples_frame <= cur_samples_frame+1;
          elsif n_bl_samples = 4 then
            next_FIFO_in      <= "01"&next_data_in(5 downto 0)&FIFO_in_cur(23 downto 0);
            n_bl_samples      <= 0;
            FIFO_write        <= '1';
            FIFO_in_cur       <= (others => '0');
            cur_samples_frame <= cur_samples_frame+1;
            FD_enable         := '0';
          end if;
          pos_from_BC0        <= pos_from_BC0 + 1;
-- Treat signal samples :
-- We have 14 bit samples. We drop lsb which is almost noise.
-- b11-b0 are signal-like samples
-- if b12 is set we have "gain switch" like samples. For the moment, we don't apply G1 window. (FIXME)
        else
-- Close opened baseline word, if any (Baseline data format 1)
          if n_bl_samples > 0 then
            if n_bl_samples = 1 then
              next_FIFO_in    <= "10000001000000000000000000"&FIFO_in_cur(5 downto 0);
            elsif n_bl_samples = 2 then
              next_FIFO_in    <= "10000010000000000000"&FIFO_in_cur(11 downto 0);
            elsif n_bl_samples = 3 then
              next_FIFO_in    <= "10000011000000"&FIFO_in_cur(17 downto 0);
            elsif n_bl_samples = 4 then
              next_FIFO_in    <= "10000100"&FIFO_in_cur(23 downto 0);
            end if;
            n_bl_samples      <= 0;
            n_sig_samples     <= 0;
            FIFO_write        <= '1';
            FD_enable         := '0';
          end if;
-- And fill or close signal word :
          if n_sig_samples = 0 then
            FIFO_in_cur       <= "0010110101010101010"&next_data_in;
            n_sig_samples     <= 1;
            cur_samples_frame <= cur_samples_frame+1;
          elsif n_sig_samples = 1 then
            next_FIFO_in      <= "001010"&next_data_in&FIFO_in_cur(12 downto 0);
            n_sig_samples     <= 0;
            FIFO_write        <= '1';
            cur_samples_frame <= cur_samples_frame+1;
            FD_enable         := '0';
          end if;
          pos_from_BC0        <= pos_from_BC0 + 1;
        end if;

-- If we have no write in the next clock, add a frame delimiter if requested
        if cur_frame_length >= frame_length and FD_enable = '1' then
          next_FIFO_in        <= "1101"&std_logic_vector(to_unsigned(cur_samples_frame,8))&cur_CRC&std_logic_vector(to_unsigned(frame_number,8));
          if frame_number < 255 then
            frame_number      <= frame_number+1;
          else
            frame_number      <= 0;
          end if;
          SoF                 <= '1';
          cur_samples_frame   <= 0;
          cur_frame_length    <= 0;
          FIFO_write          <= '1';
        end if;
      end if;
    end if; -- Normal mode
  end process manage_FIFO;
end architecture rtl;
