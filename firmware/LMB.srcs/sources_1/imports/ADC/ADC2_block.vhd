--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
--Date        : Thu Jun 15 11:01:37 2023
--Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 36 (Thirty Six)
--Command     : generate_target jesd204c_ADC2_exdes_bd.bd
--Design      : jesd204c_ADC2_exdes_bd
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.LMB_IO.all;

entity ADC2_block is
  port (
    axi_access              : in  std_logic;
    axi_addr                : in  std_logic_vector(12 downto 0);
    axi_data_in             : in  std_logic_vector(31 downto 0);
    axi_data_out            : out std_logic_vector(31 downto 0);
    axi_R_Wb                : in  std_logic;
    axi_clk                 : in  std_logic;
    axi_aresetn             : in  std_logic;
    core_clk                : in  std_logic;
    drpclk                  : in  std_logic;
    refclk                  : in  std_logic;
    rx_tdata                : out std_logic_vector(Nb_of_lanes*32-1 downto 0);
    rx_charisk              : out std_logic_vector(Nb_of_lanes*4-1 downto 0);
    rx_disperror            : out std_logic_vector(Nb_of_lanes-1 downto 0);
    rx_notintable           : out std_logic_vector(Nb_of_lanes-1 downto 0);
    rx_tvalid               : out std_logic;
    baseline                : out unsigned(13 downto 0);
    rx_sync                 : out std_logic;
    rx_sys_reset            : in  std_logic;
    rx_reset_done           : out std_logic;
    gt_powergood            : out std_logic;
    rx_aresetn              : out std_logic;
    rxn_in                  : in  std_logic_vector(Nb_of_lanes-1 downto 0);
    rxp_in                  : in  std_logic_vector(Nb_of_lanes-1 downto 0);
    link_config             : out word32_t(3 downto 0)
  );
end ADC2_block;

architecture STRUCTURE of ADC2_block is
  component jesd204_phy_ADC2 is
  port (
-- AXI Lite IO
    s_axi_aclk               : in  std_logic;
    s_axi_aresetn            : in  std_logic;
    s_axi_awaddr             : in  std_logic_vector(11 downto 0);
    s_axi_awvalid            : in  std_logic;
    s_axi_awready            : out std_logic;
    s_axi_wdata              : in  std_logic_vector(31 downto 0);
    s_axi_wvalid             : in  std_logic;
    s_axi_wready             : out std_logic;
    s_axi_bresp              : out std_logic_vector(1 downto 0);
    s_axi_bvalid             : out std_logic;
    s_axi_bready             : in  std_logic;
    s_axi_araddr             : in  std_logic_vector(11 downto 0);
    s_axi_arvalid            : in  std_logic;
    s_axi_arready            : out std_logic;
    s_axi_rdata              : out std_logic_vector(31 downto 0);
    s_axi_rresp              : out std_logic_vector(1 downto 0);
    s_axi_rvalid             : out std_logic;
    s_axi_rready             : in  std_logic;
-- System Reset Inputs for each direction
    tx_sys_reset             : in  std_logic;
    rx_sys_reset             : in  std_logic;
-- Reset Inputs for each direction
    tx_reset_gt              : in  std_logic;
    rx_reset_gt              : in  std_logic;
-- Reset Done for each direction
    tx_reset_done            : out std_logic;
    rx_reset_done            : out std_logic;
    gt_powergood             : out std_logic;

-- PRBS mode
    gt_prbssel               : in std_logic_vector(Nb_of_lanes-1 downto 0);

-- Clocks
    cpll_refclk              : in  std_logic;
    qpll0_refclk             : in std_logic;
    common0_qpll0_lock_out   : out std_logic;
    common0_qpll0_refclk_out : out std_logic;
    common0_qpll0_clk_out    : out std_logic;
    qpll1_refclk             : in std_logic;
    common0_qpll1_lock_out   : out std_logic;
    common0_qpll1_refclk_out : out std_logic;
    common0_qpll1_clk_out    : out std_logic;
    tx_core_clk              : in  std_logic;
    rx_core_clk              : in  std_logic;
    txoutclk                 : out std_logic;
    rxoutclk                 : out std_logic;
    drpclk                   : in  std_logic;

    rxencommaalign           : in  std_logic;
-- Tx Ports
    gt0_txdata               : in  std_logic_vector(31 downto 0);
    gt0_txcharisk            : in  std_logic_vector(3 downto 0);
    gt1_txdata               : in  std_logic_vector(31 downto 0);
    gt1_txcharisk            : in  std_logic_vector(3 downto 0);
    gt2_txdata               : in  std_logic_vector(31 downto 0);
    gt2_txcharisk            : in  std_logic_vector(3 downto 0);
    gt3_txdata               : in  std_logic_vector(31 downto 0);
    gt3_txcharisk            : in  std_logic_vector(3 downto 0);
-- Rx Ports
    gt0_rxdata               : out std_logic_vector(31 downto 0);
    gt0_rxcharisk            : out std_logic_vector(3 downto 0);
    gt0_rxdisperr            : out std_logic_vector(3 downto 0);
    gt0_rxnotintable         : out std_logic_vector(3 downto 0);
    gt1_rxdata               : out std_logic_vector(31 downto 0);
    gt1_rxcharisk            : out std_logic_vector(3 downto 0);
    gt1_rxdisperr            : out std_logic_vector(3 downto 0);
    gt1_rxnotintable         : out std_logic_vector(3 downto 0);
    gt2_rxdata               : out std_logic_vector(31 downto 0);
    gt2_rxcharisk            : out std_logic_vector(3 downto 0);
    gt2_rxdisperr            : out std_logic_vector(3 downto 0);
    gt2_rxnotintable         : out std_logic_vector(3 downto 0);
    gt3_rxdata               : out std_logic_vector(31 downto 0);
    gt3_rxcharisk            : out std_logic_vector(3 downto 0);
    gt3_rxdisperr            : out std_logic_vector(3 downto 0);
    gt3_rxnotintable         : out std_logic_vector(3 downto 0);
-- Serial ports
    txp_out                  : out std_logic_vector(3 downto 0);
    txn_out                  : out std_logic_vector(3 downto 0);
    rxp_in                   : in  std_logic_vector(3 downto 0);
    rxn_in                   : in  std_logic_vector(3 downto 0)
  );
  end component jesd204_phy_ADC2;

  signal baseline_pipe                                : UInt14_t(255 downto 0);
  signal baseline_sum                                 : unsigned(21 downto 0);
  signal rx_tdata_loc                                 : std_logic_vector(Nb_of_lanes*32-1 downto 0);
  signal rx_tvalid_loc                                : std_logic;
  signal jesd204c_phy_gt0_rx_rxcharisk                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt0_rx_rxdata                   : std_logic_vector(31 downto 0);
  signal jesd204c_phy_gt0_rx_rxdisperr                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt0_rx_rxheader                 : std_logic_vector(1 downto 0);
  signal jesd204c_phy_gt0_rx_rxmisalign               : std_logic;
  signal jesd204c_phy_gt0_rx_rxnotintable             : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt1_rx_rxcharisk                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt1_rx_rxdata                   : std_logic_vector(31 downto 0);
  signal jesd204c_phy_gt1_rx_rxdisperr                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt1_rx_rxheader                 : std_logic_vector(1 downto 0);
  signal jesd204c_phy_gt1_rx_rxmisalign               : std_logic;
  signal jesd204c_phy_gt1_rx_rxnotintable             : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt2_rx_rxcharisk                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt2_rx_rxdata                   : std_logic_vector(31 downto 0);
  signal jesd204c_phy_gt2_rx_rxdisperr                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt2_rx_rxheader                 : std_logic_vector(1 downto 0);
  signal jesd204c_phy_gt2_rx_rxmisalign               : std_logic;
  signal jesd204c_phy_gt2_rx_rxnotintable             : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt3_rx_rxcharisk                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt3_rx_rxdata                   : std_logic_vector(31 downto 0);
  signal jesd204c_phy_gt3_rx_rxdisperr                : std_logic_vector(3 downto 0);
  signal jesd204c_phy_gt3_rx_rxheader                 : std_logic_vector(1 downto 0);
  signal jesd204c_phy_gt3_rx_rxmisalign               : std_logic;
  signal jesd204c_phy_gt3_rx_rxnotintable             : std_logic_vector(3 downto 0);
  signal jesd204c_phy_rx_reset_done                   : std_logic;
  signal jesd204c_rx_reset_gt                         : std_logic;
  signal jesd204c_phy_gt_powergood_UNCONNECTED        : std_logic;
  signal jesd204c_phy_rxoutclk_UNCONNECTED            : std_logic;
  signal jesd204c_phy_txoutclk_UNCONNECTED            : std_logic;
  signal txn_out_UNCONNECTED                          : std_logic_vector(3 downto 0);
  signal txp_out_UNCONNECTED                          : std_logic_vector(3 downto 0);
  signal rx_outclk_loc                                : std_logic;
--  signal rx_outclk_buf                                : std_logic;
  signal gt_prbssel                                   : std_logic_vector(3 downto 0) := "1111";
  signal rx_encommaalign                              : std_logic;

  signal common0_qpll0_lock_out                       : std_logic;
  signal common0_qpll0_refclk_out                     : std_logic;
  signal common0_qpll0_clk_out                        : std_logic;
  signal common0_qpll1_lock_out                       : std_logic;
  signal common0_qpll1_refclk_out                     : std_logic;
  signal common0_qpll1_clk_out                        : std_logic;
  signal qpll0_refclk_buf                             : std_logic;
  signal qpll0_clk_buf                                : std_logic;
  signal qpll1_refclk_buf                             : std_logic;
  signal qpll1_clk_buf                                : std_logic;

  signal JESD204C_phy_axi_access                      : std_logic := '0';
  signal JESD204C_phy_axi_data_out                    : std_logic_vector(31 downto 0);
  signal JESD204C_phy_axi_awaddr                      : std_logic_vector(11 downto 0);
  signal JESD204C_phy_axi_awvalid                     : std_logic;
  signal JESD204C_phy_axi_awready                     : std_logic;
  signal JESD204C_phy_axi_wdata                       : std_logic_vector(31 downto 0);
  signal JESD204C_phy_axi_wvalid                      : std_logic;
  signal JESD204C_phy_axi_wready                      : std_logic;
  signal JESD204C_phy_axi_bresp                       : std_logic_vector(1 downto 0);
  signal JESD204C_phy_axi_bvalid                      : std_logic;
  signal JESD204C_phy_axi_bready                      : std_logic;
  signal JESD204C_phy_axi_araddr                      : std_logic_vector(11 downto 0);
  signal JESD204C_phy_axi_arvalid                     : std_logic;
  signal JESD204C_phy_axi_arready                     : std_logic;
  signal JESD204C_phy_axi_rdata                       : std_logic_vector(31 downto 0);
  signal JESD204C_phy_axi_rresp                       : std_logic_vector(1 downto 0);
  signal JESD204C_phy_axi_rvalid                      : std_logic;
  signal JESD204C_phy_axi_rready                      : std_logic;
  signal JESD204C_phy_axi_busy                        : std_logic;
begin

  rx_tdata                       <= rx_tdata_loc;
  rx_tvalid                      <= rx_tvalid_loc;
  rx_reset_done                  <= jesd204c_phy_rx_reset_done;
  rx_disperror                   <= jesd204c_phy_gt0_rx_rxdisperr;
  rx_notintable                  <= jesd204c_phy_gt0_rx_rxnotintable;

  compute_baseline : process(rx_tvalid_loc, core_clk)
  begin
    if rx_tvalid_loc = '0' then
      baseline_sum  <= (others => '0');
      baseline_pipe <= (others => (others => '0'));
    elsif rising_edge(core_clk) then
      baseline_pipe <= baseline_pipe(247 downto 0) &
                       unsigned(rx_tdata_loc(127 downto 114)) & unsigned(rx_tdata_loc(111 downto 98)) &
                       unsigned(rx_tdata_loc(95 downto 82))   & unsigned(rx_tdata_loc(79 downto 66))  &
                       unsigned(rx_tdata_loc(63 downto 50))   & unsigned(rx_tdata_loc(47 downto 34))  &
                       unsigned(rx_tdata_loc(31 downto 18))   & unsigned(rx_tdata_loc(15 downto 2));
      baseline_sum  <= baseline_sum-
                       baseline_pipe(255)-baseline_pipe(254) -baseline_pipe(253)-baseline_pipe(252)-
                       baseline_pipe(251)-baseline_pipe(250) -baseline_pipe(249)-baseline_pipe(248)+
                       unsigned(rx_tdata_loc(127 downto 114))+unsigned(rx_tdata_loc(111 downto 98))+
                       unsigned(rx_tdata_loc(95 downto 82))  +unsigned(rx_tdata_loc(79 downto 66))+
                       unsigned(rx_tdata_loc(63 downto 50))  +unsigned(rx_tdata_loc(47 downto 34))+
                       unsigned(rx_tdata_loc(31 downto 18))  +unsigned(rx_tdata_loc(15 downto 2));
      baseline      <= baseline_sum(21 downto 8);
    end if;
  end process compute_baseline;

  inst_jesd204c_ADC2: entity work.JESD204_control
  port map (
    gt0_rxdata       => jesd204c_phy_gt0_rx_rxdata,
    gt1_rxdata       => jesd204c_phy_gt1_rx_rxdata,
    gt2_rxdata       => jesd204c_phy_gt2_rx_rxdata,
    gt3_rxdata       => jesd204c_phy_gt3_rx_rxdata,
    gt0_rxcharisk    => jesd204c_phy_gt0_rx_rxcharisk,
    gt1_rxcharisk    => jesd204c_phy_gt1_rx_rxcharisk,
    gt2_rxcharisk    => jesd204c_phy_gt2_rx_rxcharisk,
    gt3_rxcharisk    => jesd204c_phy_gt3_rx_rxcharisk,
    rx_aresetn       => rx_aresetn,
    rx_core_clk      => core_clk,
    rx_core_reset    => rx_sys_reset,
    rx_reset_done    => jesd204c_phy_rx_reset_done,
    rx_reset_gt      => jesd204c_rx_reset_gt,
    rx_sync          => rx_sync,
    rx_encommaalign  => rx_encommaalign,
    rx_tdata         => rx_tdata,
    rx_charisk       => rx_charisk,
    rx_tvalid        => rx_tvalid,
    link_config      => link_config
  );

  inst_jesd204c_phy_ADC2: component jesd204_phy_ADC2
  port map (
    s_axi_aclk                      => axi_clk,
    s_axi_aresetn                   => axi_aresetn,
    s_axi_awaddr                    => JESD204C_phy_axi_awaddr,
    s_axi_awvalid                   => JESD204C_phy_axi_awvalid,
    s_axi_awready                   => JESD204C_phy_axi_awready,
    s_axi_wdata                     => JESD204C_phy_axi_wdata,
    s_axi_wvalid                    => JESD204C_phy_axi_wvalid,
    s_axi_wready                    => JESD204C_phy_axi_wready,
    s_axi_bresp                     => JESD204C_phy_axi_bresp,
    s_axi_bvalid                    => JESD204C_phy_axi_bvalid,
    s_axi_bready                    => JESD204C_phy_axi_bready,
    s_axi_araddr                    => JESD204C_phy_axi_araddr,
    s_axi_arvalid                   => JESD204C_phy_axi_arvalid,
    s_axi_arready                   => JESD204C_phy_axi_arready,
    s_axi_rdata                     => JESD204C_phy_axi_rdata,
    s_axi_rresp                     => JESD204C_phy_axi_rresp,
    s_axi_rvalid                    => JESD204C_phy_axi_rvalid,
    s_axi_rready                    => JESD204C_phy_axi_rready,
    
    tx_sys_reset                    => rx_sys_reset,
    rx_sys_reset                    => rx_sys_reset,
    tx_reset_gt                     => jesd204c_rx_reset_gt,
    tx_reset_done                   => open,
    rx_reset_gt                     => jesd204c_rx_reset_gt,
    rx_reset_done                   => jesd204c_phy_rx_reset_done,
    gt_powergood                    => gt_powergood,
    gt_prbssel                      => gt_prbssel,

    cpll_refclk                     => refclk,
    qpll0_refclk                    => refclk,
    common0_qpll0_lock_out          => common0_qpll0_lock_out,
    common0_qpll0_refclk_out        => common0_qpll0_refclk_out,
    common0_qpll0_clk_out           => common0_qpll0_clk_out,
    qpll1_refclk                    => refclk,
    common0_qpll1_lock_out          => common0_qpll1_lock_out ,
    common0_qpll1_refclk_out        => common0_qpll1_refclk_out,
    common0_qpll1_clk_out           => common0_qpll1_clk_out,

    rxencommaalign                  => rx_encommaalign,
    tx_core_clk                     => core_clk,
    rx_core_clk                     => core_clk,
    txoutclk                        => open,
    rxoutclk                        => rx_outclk_loc,
    drpclk                          => drpclk,
    gt0_rxcharisk                   => jesd204c_phy_gt0_rx_rxcharisk,
    gt0_rxdata                      => jesd204c_phy_gt0_rx_rxdata,
    gt0_rxdisperr                   => jesd204c_phy_gt0_rx_rxdisperr,
    gt0_rxnotintable                => jesd204c_phy_gt0_rx_rxnotintable,
    gt1_rxcharisk                   => jesd204c_phy_gt1_rx_rxcharisk,
    gt1_rxdata                      => jesd204c_phy_gt1_rx_rxdata,
    gt1_rxdisperr                   => jesd204c_phy_gt1_rx_rxdisperr,
    gt1_rxnotintable                => jesd204c_phy_gt1_rx_rxnotintable,
    gt2_rxcharisk                   => jesd204c_phy_gt2_rx_rxcharisk,
    gt2_rxdata                      => jesd204c_phy_gt2_rx_rxdata,
    gt2_rxdisperr                   => jesd204c_phy_gt2_rx_rxdisperr,
    gt2_rxnotintable                => jesd204c_phy_gt2_rx_rxnotintable,
    gt3_rxcharisk                   => jesd204c_phy_gt3_rx_rxcharisk,
    gt3_rxdata                      => jesd204c_phy_gt3_rx_rxdata,
    gt3_rxdisperr                   => jesd204c_phy_gt3_rx_rxdisperr,
    gt3_rxnotintable                => jesd204c_phy_gt3_rx_rxnotintable,

    gt0_txcharisk                   => (others => '0'),
    gt0_txdata                      => (others => '0'),
    gt1_txcharisk                   => (others => '0'),
    gt1_txdata                      => (others => '0'),
    gt2_txcharisk                   => (others => '0'),
    gt2_txdata                      => (others => '0'),
    gt3_txcharisk                   => (others => '0'),
    gt3_txdata                      => (others => '0'),
    rxn_in                          => rxn_in,
    rxp_in                          => rxp_in,
    txn_out                         => txn_out_UNCONNECTED,
    txp_out                         => txp_out_UNCONNECTED
  );

  axi_data_out <= JESD204C_phy_axi_data_out;
  JESD204C_phy_axi_access <= axi_access when axi_addr(12) = '0' else '0';
  
  inst_JESD204C_phy_AXI_master : entity work.AXI_master
  port map(
    axi_addr            => axi_addr(11 downto 0),
    axi_data_in         => axi_data_in,
    axi_data_out        => JESD204C_phy_axi_data_out,
    axi_R_Wb            => axi_R_Wb,
    axi_access          => JESD204C_phy_axi_access,
    axi_clk             => axi_clk,
    axi_busy            => JESD204C_phy_axi_busy,
    s_axi_aresetn       => axi_aresetn,
    s_axi_araddr        => JESD204C_phy_axi_araddr,
    s_axi_arready       => JESD204C_phy_axi_arready,
    s_axi_arvalid       => JESD204C_phy_axi_arvalid,
    s_axi_awaddr        => JESD204C_phy_axi_awaddr,
    s_axi_awready       => JESD204C_phy_axi_awready,
    s_axi_awvalid       => JESD204C_phy_axi_awvalid,
    s_axi_bready        => JESD204C_phy_axi_bready,
    s_axi_bresp         => JESD204C_phy_axi_bresp,
    s_axi_bvalid        => JESD204C_phy_axi_bvalid,
    s_axi_rdata         => JESD204C_phy_axi_rdata,
    s_axi_rready        => JESD204C_phy_axi_rready,
    s_axi_rresp         => JESD204C_phy_axi_rresp,
    s_axi_rvalid        => JESD204C_phy_axi_rvalid,
    s_axi_wdata         => JESD204C_phy_axi_wdata,
    s_axi_wready        => JESD204C_phy_axi_wready,
    s_axi_wvalid        => JESD204C_phy_axi_wvalid
  );
end STRUCTURE;
