library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ADC_ctrl is
  port (
    reset                 : in  std_logic;

    SPI_clk               : in  std_logic; -- 20 MHz clock
    clk_160               : in  std_logic;

    SPI_ADC_access        : in  std_logic;
    SPI_ADC_number        : in  natural range 0 to 3;
    SPI_ADC_R_Wb          : in  std_logic;
    SPI_ADC_reg_in        : in  std_logic_vector(14 downto 0);
    SPI_ADC_data_in       : in  std_logic_vector(7 downto 0);
    SPI_ADC_data_out      : out std_logic_vector(7 downto 0);
    SPI_ADC_Clk           : out std_logic;
    SPI_ADC_Din           : in  std_logic;
    SPI_ADC_Dout          : out std_logic;
    SPI_ADC_Csb           : out std_logic_vector(3 downto 1);
    SPI_ADC_dir           : out std_logic;
    SPI_busy              : out std_logic;
    ADC_init_done         : out std_logic
  );

end entity ADC_ctrl;

architecture rtl of ADC_ctrl is

signal SPI_ADC_number_loc          : natural range 0 to 3;
signal SPI_ADC_R_Wb_loc            : std_logic := '1';
signal SPI_user_data               : std_logic_vector(23 downto 0);
signal SPI_wait_for_transfer       : std_logic;
signal SPI_FSM_ack                 : std_logic;
signal SPI_user_data_in            : std_logic_vector(22 downto 0) := (others => '0');
signal SPI_user_data_out           : std_logic_vector(7 downto 0) := (others => '0');
signal SPI_transmit                : std_logic_vector(1 downto 0) := "00";

type   SPI_state_type is (send_ADC_reset,      wait_for_reset_transfer, wait_for_ADC_reset,
                          do_ADC_init,         wait_for_PLL_lock,       wait_for_ADC_init,
                          SPI_idle);
signal SPI_state     : SPI_state_type := SPI_idle;
--      command = (iADC<<24) | (0x1908<<8) | (0x04); // analog input optimized for DC coupling
--      command = (iADC<<24) | (0x18a6<<8) | (0x00); // Vref internal
--      command = (iADC<<24) | (0x18e6<<8) | (0x00); // Temp diode High Z
--      command = (iADC<<24) | (0x1910<<8) | (0x0d); // 1.70V full scale
--      command = (iADC<<24) | (0x18e0<<8) | (0x02);
--      command = (iADC<<24) | (0x18e1<<8) | (0x14); // From AD9795 datasheet !
--      command = (iADC<<24) | (0x18e2<<8) | (0x14); // From AD9695 datasheet : 
--      command = (iADC<<24) | (0x18e3<<8) | (0x40); // Turn ON Vcm export
--      command = (iADC<<24) | (0x18e3<<8) | (0x5f); // Tune buffer current
--      command = (iADC<<24) | (0x1a4c<<8) | (0x10); // Buffer current P 360 uA
--      command = (iADC<<24) | (0x1a4d<<8) | (0x10); // Buffer current N 360 uA
--      command = (iADC<<24) | (0x1b03<<8) | (0x02); // Buffer control 3 (as in datasheet)
--      command = (iADC<<24) | (0x1b08<<8) | (0xc1); // Buffer control 4 (as in datasheet)
--      command = (iADC<<24) | (0x1b10<<8) | (0x00); // Buffer control 5 (as in datasheet)
--      command = (iADC<<24) | (0x0561<<8) | (0x00); // Offset binary coding
--// Power down the links
--      command = (iADC<<24) | (0x0002<<8) | (0x03);
--      command = (iADC<<24) | (0x0108<<8) | (0x00); // Input clock divider = 1
--      command = (iADC<<24) | (0x056e<<8) | (0x10); // PLL for 1280 MS/s
--// JESD204B link config
--      command = (iADC<<24) | (0x0200<<8) | (0x00); // ADC in full bandwidth
--      command = (iADC<<24) | (0x0201<<8) | (0x00); // Chip decimation=1, no DDC
--      command = (iADC<<24) | (0x058b<<8) | (daq.ADC_scrambling<<7) | (0x03); // L=4 : 4 lanes active, scrambling upon request
--      command = (iADC<<24) | (0x058c<<8) | (0x00); // F=1 : 1 byte per frame
--      command = (iADC<<24) | (0x058d<<8) | (0x1f); // K=32 : 32 frames per multiframe
--      command = (iADC<<24) | (0x058e<<8) | (0x00); // M=1 : 1 converter
--      command = (iADC<<24) | (0x058f<<8) | ((2<<6)|0x0d); // N=14 : 14-bit resolution, CS=2
--      command = (iADC<<24) | (0x0590<<8) | (0x2f); // N'=16, subcass=1 
--      command = (iADC<<24) | (0x0559<<8) | 0x00;   // CS0=0, CS1=0
--      command = (iADC<<24) | (0x055a<<8) | 0x05;   // CS2=SYSREF
--      command = (iADC<<24) | (0x0120<<8) | 0x02;   // SYSREF = N shot mode
--      command = (iADC<<24) | (0x01ff<<8) | 0x01;   // SYSREF used as timestamp
--      command = (iADC<<24) | (0x0002<<8) | (0x00); // power up the link
--      command = (iADC<<24) | (0x1228<<8) | (0x4f); // Reset JESD204B start-up circuit
--      command = (iADC<<24) | (0x1228<<8) | (0x0f); // JESD204B start-up circuit in normal operation
--      command = (iADC<<24) | (0x1222<<8) | (0x00); // JESD204B PLL force normal operation
--      command = (iADC<<24) | (0x1222<<8) | (0x04); // Reset JESD204B PLL calib
--      command = (iADC<<24) | (0x1222<<8) | (0x00); // JESD204B PLL force normal operation
--      command = (iADC<<24) | (0x1262<<8) | (0x08); // Clear LoL bit
--      command = (iADC<<24) | (0x1262<<8) | (0x00); // LoL bit in normal operation

signal ADC_init_register           : UInt16_t(38 downto 1)  := (x"1262", x"1262", x"1222",
                                                                x"1222", x"1222", x"1228", x"1228", x"0002",
                                                                x"01ff", x"0120", x"055a", x"0559", x"0590",
                                                                x"058f", x"058e", x"058d", x"058c", x"058b",
                                                                x"0201", x"0200", x"056e", x"0108", x"0002",
                                                                x"0561", x"1b10", x"1b08", x"1b03", x"1a4d",
                                                                x"1a4c", x"18e3", x"18e3", x"18e2", x"18e1",
                                                                x"18e0", x"1910", x"18e6", x"18a6", x"1908");
signal ADC_init_value              : UInt8_t(38 downto 1)   := (x"00",   x"08",   x"00", 
                                                                x"04",   x"00",   x"0f",   x"4f",   x"00",
                                                                x"01",   x"02",   x"05",   x"00",   x"2f",
                                                                x"8d",   x"00",   x"1f",   x"00"  , x"83",
                                                                x"00",   x"00",   x"10",   x"00",   x"03",
                                                                x"00",   x"00",   x"c1",   x"02",   x"12",
                                                                x"12",   x"5f",   x"40",   x"14",   x"14",
                                                                x"02",   x"0d",   x"00",   x"00",   x"04");
signal n_ADC_init_register         : natural range 0 to 63 := 38;
signal ADC_init_done_loc           : std_logic := '0';

begin  -- architecture behavioral

  ADC_init_done       <= ADC_init_done_loc;

  inst_ADC_SPI_master : entity work.ADC_SPI_master
  port map(
    SPI_ADC_number    => SPI_ADC_number_loc,
-- CPU Interface Signals
    SPI_clk           => SPI_clk,
    reset             => reset,
    transmit          => SPI_transmit,
    r_wb              => SPI_ADC_R_Wb_loc,
    SPI_user_data_in  => SPI_user_data_in,
    SPI_user_data_out => SPI_user_data_out,
    wait_for_transfer => SPI_wait_for_transfer,
    FSM_ack           => SPI_FSM_ack,
-- Hardware SPI Interface Signals
    SPI_csb           => SPI_ADC_csb,
    SPI_din           => SPI_ADC_Din,
    SPI_dout          => SPI_ADC_Dout,
    SPI_strobe        => SPI_ADC_Clk,
    SPI_dir           => SPI_ADC_dir
  );

-- Program default ADC and DAC configuration
-- ADC in normal binary mode, no test mode
-- Default mode = standard running setup 
  program_SPI : process(reset, SPI_ADC_access, clk_160)
  variable reset_duration : unsigned(19 downto 0) := (others => '0');
  variable PLL_duration   : unsigned(23 downto 0) := (others => '0');
  variable n_init_loop    : natural range 0 to 63 := 0;
  begin
    if reset = '1' then
      SPI_state                           <= send_ADC_reset;
      SPI_transmit                        <= "10";
      SPI_ADC_number_loc                  <=  1;
      ADC_init_done_loc                   <= '0';
      SPI_busy                            <= '0';
    elsif Rising_Edge(clk_160) then
      SPI_transmit                        <= "10";
      case SPI_state is
      when send_ADC_reset =>
        ADC_init_done_loc                 <= '0';
        SPI_user_data_in(22 downto 8)     <= "000"&x"000";             -- Write in register 0x00
        SPI_user_data_in(7  downto 0)     <= x"81";                    -- Write data 0x81 : soft reset
        SPI_ADC_R_Wb_loc                  <= '0';
        SPI_transmit                      <= "11";
        SPI_busy                          <= '1';
        if SPI_FSM_ack = '1' then                                      -- synchronize with SPI clock
          SPI_state                       <= wait_for_reset_transfer;
        end if;
      when wait_for_reset_transfer =>
        if SPI_wait_for_transfer = '0' then
          SPI_busy                        <= '0';
          if SPI_ADC_number_loc < 3 then
            SPI_ADC_number_loc            <= SPI_ADC_number_loc+1;
            SPI_state                     <= send_ADC_reset;
          else
-- wait for 5 ms until boot procedure is finished
            reset_duration                := x"C3500";                 -- 5 ms with 160 MHz clock
            SPI_state                     <= wait_for_ADC_reset;
          end if;
        end if;
      when wait_for_ADC_reset =>
        if reset_duration > 0 then
          reset_duration                  := reset_duration - 1;
        else
          SPI_state                       <= do_ADC_init;
          SPI_ADC_R_Wb_loc                <= '0';
          SPI_ADC_number_loc              <=  1;
          n_init_loop                     := 1;
        end if;
      when do_ADC_init =>
        SPI_user_data_in(22 downto 8)     <= std_logic_vector(ADC_init_register(n_init_loop)(14 downto 0));
        SPI_user_data_in(7 downto 0)      <= std_logic_vector(ADC_init_value(n_init_loop));
        SPI_transmit                      <= "11";
        SPI_busy                          <= '1';
        if SPI_FSM_ack = '1' then                                      -- synchronize with SPI clock
          if n_init_loop = 35 then
            PLL_duration                  := x"7a1200";                -- 50 ms @ 160 MHz
            SPI_state                     <= wait_for_PLL_lock;
          else
            SPI_state                     <= wait_for_ADC_init;
          end if;
        end if;
      when wait_for_PLL_lock =>
        if PLL_duration > 0 then
          PLL_duration                    := PLL_duration-1;
        else
          SPI_state                       <= wait_for_ADC_init;
        end if;
      when wait_for_ADC_init =>
        if SPI_wait_for_transfer = '0' then
          SPI_busy                        <= '0';
          SPI_transmit                    <= "10";
          if n_init_loop < n_ADC_init_register then                    -- Write 38 registers content for this ADC
            n_init_loop                   := n_init_loop+1;
            SPI_state                     <= do_ADC_init;
          else
            if ADC_init_done_loc = '0' and SPI_ADC_number_loc < 3 then -- Restart with next ADC
              SPI_ADC_number_loc          <= SPI_ADC_number_loc+1;
              n_init_loop                 := 1;
              SPI_state                   <= do_ADC_init;
            else                                                       -- Treat user requests
              if SPI_ADC_R_Wb_loc = '1' then
                SPI_ADC_data_out          <= SPI_user_data_out;
              end if;
              SPI_state                   <= SPI_idle;
            end if;
          end if;
        end if;
      when SPI_idle =>                                                 -- We are in the default configuration mode, we are ready and wait for user request
        ADC_init_done_loc                 <= '1';
        SPI_busy                          <= '0';
        n_init_loop                       := n_ADC_init_register;
        if SPI_ADC_access = '1' then
          SPI_ADC_number_loc              <= SPI_ADC_number;
          SPI_user_data_in(22 downto 8)   <= SPI_ADC_reg_in;
          SPI_user_data_in(7 downto 0)    <= SPI_ADC_data_in;
          SPI_ADC_R_Wb_loc                <= SPI_ADC_R_Wb;
          SPI_transmit                    <= "11";
          if SPI_FSM_ack = '1' then                                    -- synchronize with SPI clock
            SPI_state                     <= wait_for_ADC_init;
          end if;
          SPI_busy                        <= '1';
        end if;
      end case;
    end if;
  end process program_SPI;
end architecture rtl;