--===========================================================================--
-- AXI master
--===========================================================================--
-- Version  Author        Date               Description
--
-- 0.1      Marc Dejardin 2023/09/12
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
library UNISIM;
  use UNISIM.vcomponents.all;

entity AXI_master is
  port (
  axi_addr                                     : in  std_logic_vector(11 downto 0);
  axi_data_in                                  : in  std_logic_vector(31 downto 0);
  axi_data_out                                 : out std_logic_vector(31 downto 0);
  axi_R_Wb                                     : in  std_logic;
  axi_access                                   : in  std_logic;
  axi_clk                                      : in  std_logic;
  s_axi_aresetn                                : in  std_logic;
  s_axi_awaddr                                 : out std_logic_vector(11 downto 0);
  s_axi_awvalid                                : out std_logic;
  s_axi_awready                                : in  std_logic;
  s_axi_wdata                                  : out std_logic_vector(31 downto 0);
  s_axi_wvalid                                 : out std_logic;
  s_axi_wready                                 : in  std_logic;
  s_axi_bresp                                  : in  std_logic_vector(1 downto 0);
  s_axi_bvalid                                 : in  std_logic;
  s_axi_bready                                 : out std_logic;
  s_axi_araddr                                 : out std_logic_vector(11 downto 0);
  s_axi_arvalid                                : out std_logic;
  s_axi_arready                                : in  std_logic;
  s_axi_rdata                                  : in  std_logic_vector(31 downto 0);
  s_axi_rresp                                  : in  std_logic_vector(1 downto 0);
  s_axi_rvalid                                 : in  std_logic;
  s_axi_rready                                 : out std_logic;
  axi_busy                                     : out std_logic
  );
end;

architecture rtl of AXI_master is
  signal w_ack         : std_logic := '0';
  signal b_ack         : std_logic := '0';
  signal arvalid_loc   : std_logic;
  signal rready_loc    : std_logic;
  signal awvalid_loc   : std_logic;
  signal wvalid_loc    : std_logic;
  signal bready_loc    : std_logic;
  signal awaddr_loc    : std_logic_vector(11 downto 0);
  signal araddr_loc    : std_logic_vector(11 downto 0);
  signal wdata_loc     : std_logic_vector(31 downto 0);

  type AXI_state_type is (start_AXI_read, AXI_read_data,
                          start_AXI_write, AXI_write_data,
                          AXI_idle);
  signal AXI_state     : AXI_state_type := AXI_idle;

begin
  s_axi_arvalid            <= arvalid_loc;
  s_axi_rready             <= rready_loc;
  s_axi_awvalid            <= awvalid_loc;
  s_axi_wvalid             <= wvalid_loc;
  s_axi_awaddr             <= awaddr_loc;
  s_axi_araddr             <= araddr_loc;
  s_axi_wdata              <= wdata_loc;

  proc_axi_protocol : process(s_axi_aresetn,axi_clk)
  begin
    if s_axi_aresetn = '0' then
      araddr_loc                         <=  (others => '0');
      arvalid_loc                        <= '0';
      rready_loc                         <= '0';
      awaddr_loc                         <= (others => '0');
      awvalid_loc                        <= '0';
      wdata_loc                          <= (others => '0');
      wvalid_loc                         <= '0';
      bready_loc                         <= '0';
    elsif rising_edge(axi_clk) then
      case AXI_state is
      when AXI_idle =>
        AXI_busy                         <= '0';
        arvalid_loc                      <= '0';
        rready_loc                       <= '0';
        awvalid_loc                      <= '0';
        wvalid_loc                       <= '0';
        bready_loc                       <= '0';
        w_ack                            <= '0';
        b_ack                            <= '0';
        if AXI_access = '1' then
          AXI_busy                       <= '1';
          if AXI_R_Wb = '0' then
            AXI_state                    <= start_AXI_write;
          else
            AXI_state                    <= start_AXI_read;
          end if;
        end if;
      when start_AXI_read  =>
          araddr_loc                     <= axi_addr;
          arvalid_loc                    <= '1';
          if s_axi_arready = '1' then
            AXI_state                    <= AXI_read_data;
            arvalid_loc                  <= '0';
            rready_loc                   <= '1';
          end if;
      when AXI_read_data =>
         if s_axi_rvalid = '1' then
            axi_data_out                 <= s_axi_rdata;
            AXI_state                    <= AXI_idle;
            rready_loc                   <= '0';
         end if;
      when start_AXI_write =>
          awaddr_loc                     <= axi_addr;
          awvalid_loc                    <= '1';
          if s_axi_awready = '1' then
            AXI_state                    <= AXI_write_data;
            awvalid_loc                  <= '0';
            wdata_loc                    <= axi_data_in;
            wvalid_loc                   <= '1';
            bready_loc                   <= '1';
            w_ack                        <= '0';
            b_ack                        <= '0';
          end if;
      when AXI_write_data =>
         if s_axi_wready = '1' then
            wvalid_loc                   <= '0';
            w_ack                        <= '1';
         end if;
         if s_axi_bvalid = '1' then
            bready_loc                   <= '0';
            b_ack                        <= '1';
         end if;
         if w_ack = '1' and b_ack = '1' then
            AXI_state                    <= AXI_idle;
        end if;
      end case;
    end if;
  end process proc_axi_protocol;
end rtl;
