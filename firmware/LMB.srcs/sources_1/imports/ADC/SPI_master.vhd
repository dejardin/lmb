--===========================================================================--
-- Serial Port Interface master adapted for AD9697
-- Adapted from SPI_master.vhd
--===========================================================================--
-- Version  Author        Date               Description
--
-- 0.1      Marc Dejardin 2018/09/14
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
library UNISIM;
  use UNISIM.vcomponents.all;

entity ADC_SPI_master is
  port (
    SPI_ADC_number     : in    natural range 0 to 3;
    --+ CPU Interface Signals
    SPI_clk            : in  std_logic;
    reset              : in  std_logic;
    transmit           : in  std_logic_vector(1 downto 0);
    r_wb               : in  std_logic;
    SPI_user_data_in   : in  std_logic_vector(22 downto 0);
    SPI_user_data_out  : out std_logic_vector(7 downto 0);
    wait_for_transfer  : out std_logic;
    FSM_ack            : out std_logic;
    --+ Hardware SPI Interface Signals
    SPI_csb            : out std_logic_vector(3 downto 1);
    SPI_Din            : in  std_logic;
    SPI_Dout           : out std_logic;
    SPI_strobe         : out std_logic;
    SPI_dir            : out std_logic
  );
end;

architecture rtl of ADC_SPI_master is

--* State type of the SPI transfer state machine
type   state_type is (idle, prepare, clk_goes_up, clk_goes_down, finished);
signal state : state_type := idle;

-- 24-bit word to hold data to be sent
signal SPI_word              : std_logic_vector(23 downto 0);
signal SPI_clk_loc           : std_logic :='1';

begin
  SPI_strobe                             <= SPI_clk_loc;
--* SPI transfer state machine
  spi_transfer : process(SPI_clk, reset, transmit)
  variable bit_count :  natural range 0 to 24 := 0;
  begin
    if reset = '1' then
      bit_count                          := 0;
      wait_for_transfer                  <= '0';
      SPI_dir                            <= '0';
      SPI_clk_loc                        <= '0';
      SPI_csb                            <= (others => '1');
      state                              <= idle;
      FSM_ack                            <= '0';
    elsif falling_edge(SPI_clk) then
      case state is
        when idle =>
          SPI_csb                        <= (others => '1');
          SPI_clk_loc                    <= '0';
          wait_for_transfer              <= '0';
          FSM_ack                        <= '0';
          if transmit(0) = '1' then
            state                        <= prepare;
            wait_for_transfer            <= '1';
            FSM_ack                      <= '1';
-- Start with clock high or down depending on the edge on which the data is latched in the electroniucs
            SPI_clk_loc                  <= transmit(1); 
          end if;
        when prepare =>
          FSM_ack                        <= '0';
          SPI_clk_loc                    <= not SPI_clk_loc;
          bit_count                      := 23;
          spi_word(23)                   <= r_wb;
          spi_word(22 downto 0)          <= SPI_user_data_in;
          SPI_dir                        <= '0';
          SPI_Dout                       <= r_wb;
          SPI_csb(SPI_ADC_number)        <= '0';
          state                          <= clk_goes_down;
        when clk_goes_down =>     -- clk rising edge : FPGA data is strobed by ADC or ADC data strobe by FPGA
          SPI_clk_loc                    <= not SPI_clk_loc;
          state                          <= clk_goes_up;
--          if bit_count < 8 and r_wb = '1' then -- Read ADC data put on the bus on the falling edge
--            SPI_user_data_out(bit_count) <= SPI_Din;
--          end if;
        when clk_goes_up =>      -- clk falling edge : prepare data on the bus on both side
          SPI_clk_loc                    <= not SPI_clk_loc;
          state                          <= clk_goes_down;
          if bit_count < 8 and r_wb = '1' then -- Read ADC data put on the bus on the falling edge
            SPI_user_data_out(bit_count) <= SPI_Din;
          end if;
          if bit_count > 0 then
            bit_count                    := bit_count -1;
            if bit_count >=8 or r_wb = '0' then -- We always write the register address and register data if requested
              SPI_Dout                   <= spi_word(bit_count);
            else
              SPI_dir                    <= '1'; -- change iobuf direction to get data from ADC
            end if;
          else
            state                        <= finished;
          end if;
        when finished =>      -- Let time to the caller to aknowledge the transfer end 
          wait_for_transfer              <= '0';
          state                          <= idle;
          SPI_csb                        <= (others => '1');
        when others =>
          null;
      end case;
    end if;
  end process spi_transfer;

end rtl;
