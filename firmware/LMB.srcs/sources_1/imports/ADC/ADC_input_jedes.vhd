library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ADC_Input is
  generic
  (
-- Number of lanes per ADC
    Nb_of_Lanes : natural := 4
  );

  port (
    reset               : in  std_logic;
    IO_reset            : in  std_logic;

    AXI_clk             : in  std_logic; -- 100 MHz clock for AXI protocol
    DRP_clk             : in  std_logic; -- 100 MHz clock for DRP
    core_clk            : in  std_logic;

    ADC1_in0_P          : in  std_logic;
    ADC1_in0_N          : in  std_logic;
    ADC1_in1_P          : in  std_logic;
    ADC1_in1_N          : in  std_logic;
    ADC1_in2_P          : in  std_logic;
    ADC1_in2_N          : in  std_logic;
    ADC1_in3_P          : in  std_logic;
    ADC1_in3_N          : in  std_logic;
    ADC1_clk_P          : in  std_logic;
    ADC1_clk_N          : in  std_logic;
    ADC1_rx_sync        : out std_logic;
    ADC1_data_out       : out std_logic_vector(Nb_of_lanes*32-1 downto 0);
    ADC1_charisk        : out std_logic_vector(Nb_of_lanes*4-1 downto 0);
    ADC1_rx_disperror   : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC1_rx_notintable  : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC1_data_valid     : out std_logic;
    ADC1_gt_powergood   : out std_logic;
    ADC1_reset_done     : out std_logic;
    ADC1_link_config    : out word32_t(3 downto 0);
    ADC1_baseline       : out unsigned(13 downto 0);

    ADC2_in0_P          : in  std_logic;
    ADC2_in0_N          : in  std_logic;
    ADC2_in1_P          : in  std_logic;
    ADC2_in1_N          : in  std_logic;
    ADC2_in2_P          : in  std_logic;
    ADC2_in2_N          : in  std_logic;
    ADC2_in3_P          : in  std_logic;
    ADC2_in3_N          : in  std_logic;
    ADC2_clk_P          : in  std_logic;
    ADC2_clk_N          : in  std_logic;
    ADC2_rx_sync        : out std_logic;
    ADC2_data_out       : out std_logic_vector(Nb_of_lanes*32-1 downto 0);
    ADC2_charisk        : out std_logic_vector(Nb_of_lanes*4-1 downto 0);
    ADC2_rx_disperror   : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC2_rx_notintable  : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC2_data_valid     : out std_logic;
    ADC2_gt_powergood   : out std_logic;
    ADC2_reset_done     : out std_logic;
    ADC2_link_config    : out word32_t(3 downto 0);
    ADC2_baseline       : out unsigned(13 downto 0);

    ADC3_in0_P          : in  std_logic;
    ADC3_in0_N          : in  std_logic;
    ADC3_in1_P          : in  std_logic;
    ADC3_in1_N          : in  std_logic;
    ADC3_in2_P          : in  std_logic;
    ADC3_in2_N          : in  std_logic;
    ADC3_in3_P          : in  std_logic;
    ADC3_in3_N          : in  std_logic;
    ADC3_clk_P          : in  std_logic;
    ADC3_clk_N          : in  std_logic;
    ADC3_rx_sync        : out std_logic;
    ADC3_data_out       : out std_logic_vector(Nb_of_lanes*32-1 downto 0);
    ADC3_charisk        : out std_logic_vector(Nb_of_lanes*4-1 downto 0);
    ADC3_rx_disperror   : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC3_rx_notintable  : out std_logic_vector(Nb_of_lanes-1 downto 0);
    ADC3_data_valid     : out std_logic;
    ADC3_gt_powergood   : out std_logic;
    ADC3_reset_done     : out std_logic;
    ADC3_link_config    : out word32_t(3 downto 0);
    ADC3_baseline       : out unsigned(13 downto 0);

    SPI_ADC_Clk         : out   std_logic;
    SPI_ADC_DIO         : inout std_logic;
    SPI_ADC_Csb         : out   std_logic_vector(3 downto 1);

    AXI_aresetn         : in  std_logic;
    ADC1_AXI_addr       : in  std_logic_vector(12 downto 0);
    ADC1_AXI_data_in    : in  std_logic_vector(31 downto 0);
    ADC1_AXI_data_out   : out std_logic_vector(31 downto 0);
    ADC1_AXI_access     : in  std_logic;
    ADC1_AXI_R_Wb       : in  std_logic;
    ADC2_AXI_addr       : in  std_logic_vector(12 downto 0);
    ADC2_AXI_data_in    : in  std_logic_vector(31 downto 0);
    ADC2_AXI_data_out   : out std_logic_vector(31 downto 0);
    ADC2_AXI_access     : in  std_logic;
    ADC2_AXI_R_Wb       : in  std_logic;
    ADC3_AXI_addr       : in  std_logic_vector(12 downto 0);
    ADC3_AXI_data_in    : in  std_logic_vector(31 downto 0);
    ADC3_AXI_data_out   : out std_logic_vector(31 downto 0);
    ADC3_AXI_access     : in  std_logic;
    ADC3_AXI_R_Wb       : in  std_logic
  );

end entity ADC_Input;


architecture rtl of ADC_Input is

signal ADC1_clk                    : std_logic;
signal ADC1_in_P                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC1_in_N                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC1_rx_tvalid              : std_logic;
signal ADC1_rx_tdata               : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC1_rx_aresetn             : std_logic;
signal ADC1_axi_araddr             : std_logic_vector(31 downto 0);
signal ADC1_axi_arready            : std_logic;
signal ADC1_axi_arvalid            : std_logic;
signal ADC1_axi_awaddr             : std_logic_vector(31 downto 0);
signal ADC1_axi_awready            : std_logic;
signal ADC1_axi_awvalid            : std_logic;
signal ADC1_axi_bready             : std_logic;
signal ADC1_axi_bresp              : std_logic_vector(1 downto 0);
signal ADC1_axi_bvalid             : std_logic;
signal ADC1_axi_rdata              : std_logic_vector(31 downto 0);
signal ADC1_axi_rready             : std_logic;
signal ADC1_axi_rresp              : std_logic_vector(1 downto 0);
signal ADC1_axi_rvalid             : std_logic;
signal ADC1_axi_wdata              : std_logic_vector(31 downto 0);
signal ADC1_axi_wready             : std_logic;
signal ADC1_axi_wstrb              : std_logic_vector(3 downto 0);
signal ADC1_axi_wvalid             : std_logic;

signal ADC2_clk                    : std_logic;
signal ADC2_in_P                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC2_in_N                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC2_rx_tvalid              : std_logic;
signal ADC2_rx_tdata               : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC2_rx_aresetn             : std_logic;
signal ADC2_axi_araddr             : std_logic_vector(31 downto 0);
signal ADC2_axi_arready            : std_logic;
signal ADC2_axi_arvalid            : std_logic;
signal ADC2_axi_awaddr             : std_logic_vector(31 downto 0);
signal ADC2_axi_awready            : std_logic;
signal ADC2_axi_awvalid            : std_logic;
signal ADC2_axi_bready             : std_logic;
signal ADC2_axi_bresp              : std_logic_vector(1 downto 0);
signal ADC2_axi_bvalid             : std_logic;
signal ADC2_axi_rdata              : std_logic_vector(31 downto 0);
signal ADC2_axi_rready             : std_logic;
signal ADC2_axi_rresp              : std_logic_vector(1 downto 0);
signal ADC2_axi_rvalid             : std_logic;
signal ADC2_axi_wdata              : std_logic_vector(31 downto 0);
signal ADC2_axi_wready             : std_logic;
signal ADC2_axi_wstrb              : std_logic_vector(3 downto 0);
signal ADC2_axi_wvalid             : std_logic;

signal ADC3_clk                    : std_logic;
signal ADC3_in_P                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC3_in_N                   : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ADC3_rx_tvalid              : std_logic;
signal ADC3_rx_tdata               : std_logic_vector(Nb_of_lanes*32-1 downto 0);
signal ADC3_rx_aresetn             : std_logic;
signal ADC3_axi_araddr             : std_logic_vector(31 downto 0);
signal ADC3_axi_arready            : std_logic;
signal ADC3_axi_arvalid            : std_logic;
signal ADC3_axi_awaddr             : std_logic_vector(31 downto 0);
signal ADC3_axi_awready            : std_logic;
signal ADC3_axi_awvalid            : std_logic;
signal ADC3_axi_bready             : std_logic;
signal ADC3_axi_bresp              : std_logic_vector(1 downto 0);
signal ADC3_axi_bvalid             : std_logic;
signal ADC3_axi_rdata              : std_logic_vector(31 downto 0);
signal ADC3_axi_rready             : std_logic;
signal ADC3_axi_rresp              : std_logic_vector(1 downto 0);
signal ADC3_axi_rvalid             : std_logic;
signal ADC3_axi_wdata              : std_logic_vector(31 downto 0);
signal ADC3_axi_wready             : std_logic;
signal ADC3_axi_wstrb              : std_logic_vector(3 downto 0);
signal ADC3_axi_wvalid             : std_logic;

signal axi_rq_strb                 : std_logic_vector(3 downto 0);
signal axi_rq_add                  : std_logic_vector(31 downto 0);
signal axi_rq_dti                  : std_logic_vector(31 downto 0);
signal axi_rtn_dto                 : std_logic_vector(31 downto 0);
signal axi_tick                    : std_logic;
signal axi_status                  : std_logic_vector(31 downto 0);
signal axi_done                    : std_logic;
signal ADC1_axi_rq_rd              : std_logic;
signal ADC1_axi_rq_wr              : std_logic;
signal ADC2_axi_rq_rd              : std_logic;
signal ADC2_axi_rq_wr              : std_logic;
signal ADC3_axi_rq_rd              : std_logic;
signal ADC3_axi_rq_wr              : std_logic;

begin  -- architecture behavioral

  ADC1_in_P(0) <= ADC1_in0_P;
  ADC1_in_P(1) <= ADC1_in1_P;
  ADC1_in_P(2) <= ADC1_in2_P;
  ADC1_in_P(3) <= ADC1_in3_P;
  ADC1_in_N(0) <= ADC1_in0_N;
  ADC1_in_N(1) <= ADC1_in1_N;
  ADC1_in_N(2) <= ADC1_in2_N;
  ADC1_in_N(3) <= ADC1_in3_N;

  ADC1_clk_IBUFDS :   unisim.vcomponents.IBUFDS_GTE3
  generic map(
    REFCLK_HROW_CK_SEL => "00"
  )
  port map (
    CEB        => '0',
    I          => ADC1_Clk_P,
    IB         => ADC1_Clk_N,
    O          => ADC1_clk,
    ODIV2      => open
  );

  ADC2_in_P(0) <= ADC2_in0_P;
  ADC2_in_P(1) <= ADC2_in1_P;
  ADC2_in_P(2) <= ADC2_in2_P;
  ADC2_in_P(3) <= ADC2_in3_P;
  ADC2_in_N(0) <= ADC2_in0_N;
  ADC2_in_N(1) <= ADC2_in1_N;
  ADC2_in_N(2) <= ADC2_in2_N;
  ADC2_in_N(3) <= ADC2_in3_N;
  ADC2_clk_IBUFDS :   unisim.vcomponents.IBUFDS_GTE3 
  generic map(
    REFCLK_HROW_CK_SEL => "00"
  )
  port map (
    CEB        => '0',
    I          => ADC2_Clk_P,
    IB         => ADC2_Clk_N,
    O          => ADC2_clk,
    ODIV2      => open
  );

  ADC3_in_P(0) <= ADC3_in0_P;
  ADC3_in_P(1) <= ADC3_in1_P;
  ADC3_in_P(2) <= ADC3_in2_P;
  ADC3_in_P(3) <= ADC3_in3_P;
  ADC3_in_N(0) <= ADC3_in0_N;
  ADC3_in_N(1) <= ADC3_in1_N;
  ADC3_in_N(2) <= ADC3_in2_N;
  ADC3_in_N(3) <= ADC3_in3_N;
  ADC3_clk_IBUFDS :   unisim.vcomponents.IBUFDS_GTE3
  generic map(
    REFCLK_HROW_CK_SEL => "00"
  )
  port map (
    CEB        => '0',
    I          => ADC3_Clk_P,
    IB         => ADC3_Clk_N,
    O          => ADC3_clk,
    ODIV2      => open
  );

--    if(rx_tvalid)
--    begin
--      //extract the samples from the input data word
--      //Channel 0
--      signal0_cntrl1_reg <= rx_tdata[31:30];
--      signal0_sampl1_reg <= rx_tdata[29:16];
--      signal0_cntrl0_reg <= rx_tdata[15:14];
--      signal0_sampl0_reg <= rx_tdata[13:0];
--      //Channel 1
--      signal1_cntrl1_reg <= rx_tdata[63:62];
--      signal1_sampl1_reg <= rx_tdata[61:48];
--      signal1_cntrl0_reg <= rx_tdata[47:46];
--      signal1_sampl0_reg <= rx_tdata[45:32];
--      //Channel 2
--      signal2_cntrl1_reg <= rx_tdata[95:94];
--      signal2_sampl1_reg <= rx_tdata[93:80];
--      signal2_cntrl0_reg <= rx_tdata[79:78];
--      signal2_sampl0_reg <= rx_tdata[77:64];
--      //Channel 3
--      signal3_cntrl1_reg <= rx_tdata[127:126];
--      signal3_sampl1_reg <= rx_tdata[125:112];
--      signal3_cntrl0_reg <= rx_tdata[111:110];
--      signal3_sampl0_reg <= rx_tdata[109:96];
--      full <= 1'b1;

  ADC1_data_out         <= ADC1_rx_tdata;
  ADC1_data_valid       <= ADC1_rx_tvalid;
  ADC2_data_out         <= ADC2_rx_tdata;
  ADC2_data_valid       <= ADC2_rx_tvalid;
  ADC3_data_out         <= ADC3_rx_tdata;
  ADC3_data_valid       <= ADC3_rx_tvalid;

  inst_ADC1_block : entity work.ADC1_block
  port map(
    axi_clk           => axi_clk,                    -- in  std_logic;
    axi_aresetn       => AXI_aresetn,                -- in  std_logic;
    axi_addr          => ADC1_AXI_addr,
    axi_data_in       => ADC1_AXI_data_in,
    axi_data_out      => ADC1_AXI_data_out,
    axi_access        => ADC1_AXI_access,
    axi_R_Wb          => ADC1_AXI_R_Wb,
    core_clk          => core_clk,                   -- in  std_logic;
    refclk            => ADC1_clk,                   -- in  std_logic;
    drpclk            => DRP_clk,                    -- in  std_logic;
    rx_tdata          => ADC1_rx_tdata,              -- out std_logic_vector(127 downto 0);
    rx_charisk        => ADC1_charisk,               -- out std_logic_vector(127 downto 0);
    rx_disperror      => ADC1_rx_disperror,          -- out std_logic_vector(3 downto 0);
    rx_notintable     => ADC1_rx_notintable,          -- out std_logic_vector(3 downto 0);
    rx_tvalid         => ADC1_rx_tvalid,             -- out std_logic;
    baseline          => ADC1_baseline,              -- out unsigned(13 downto 0);
    rx_sync           => ADC1_rx_sync,               -- out std_logic;
    rx_aresetn        => ADC1_rx_aresetn,            -- out std_logic;
    rx_sys_reset      => reset,                      -- in  std_logic;
    rx_reset_done     => ADC1_reset_done,            -- out std_logic;
    gt_powergood      => ADC1_gt_powergood,          -- out std_logic
    rxn_in            => ADC1_in_N,                  -- in  std_logic_vector(3 downto 0);
    rxp_in            => ADC1_in_P,                  -- in  std_logic_vector(3 downto 0);
    link_config       => ADC1_link_config            -- out word32_t(3 downto 0)
  );
  inst_ADC2_block : entity work.ADC2_block
  port map(
    axi_clk           => axi_clk,                    -- in  std_logic;
    axi_aresetn       => AXI_aresetn,                -- in  std_logic;
    axi_addr          => ADC2_AXI_addr,
    axi_data_in       => ADC2_AXI_data_in,
    axi_data_out      => ADC2_AXI_data_out,
    axi_access        => ADC2_AXI_access,
    axi_R_Wb          => ADC2_AXI_R_Wb,
    core_clk          => core_clk,                   -- in  std_logic;
    refclk            => ADC2_clk,                   -- in  std_logic;
    drpclk            => DRP_clk,                    -- in  std_logic;
    rx_tdata          => ADC2_rx_tdata,              -- out std_logic_vector(127 downto 0);
    rx_charisk        => ADC2_charisk,               -- out std_logic_vector(127 downto 0);
    rx_disperror      => ADC2_rx_disperror,          -- out std_logic_vector(3 downto 0);
    rx_notintable     => ADC2_rx_notintable,          -- out std_logic_vector(3 downto 0);
    rx_tvalid         => ADC2_rx_tvalid,             -- out std_logic;
    baseline          => ADC2_baseline,              -- out unsigned(13 downto 0);
    rx_sync           => ADC2_rx_sync,               -- out std_logic;
    rx_aresetn        => ADC2_rx_aresetn,            -- out std_logic;
    rx_sys_reset      => reset,                      -- in  std_logic;
    rx_reset_done     => ADC2_reset_done,            -- out std_logic;
    gt_powergood      => ADC2_gt_powergood,          -- out std_logic
    rxn_in            => ADC2_in_N,                  -- in  std_logic_vector(3 downto 0);
    rxp_in            => ADC2_in_P,                  -- in  std_logic_vector(3 downto 0);
    link_config       => ADC2_link_config            -- out word32_t(3 downto 0)
  );
  inst_ADC3_block : entity work.ADC3_block
  port map(
    axi_clk           => axi_clk,                    -- in  std_logic;
    axi_aresetn       => AXI_aresetn,                -- in  std_logic;
    axi_addr          => ADC3_AXI_addr,
    axi_data_in       => ADC3_AXI_data_in,
    axi_data_out      => ADC3_AXI_data_out,
    axi_access        => ADC3_AXI_access,
    axi_R_Wb          => ADC3_AXI_R_Wb,
    core_clk          => core_clk,                   -- in  std_logic;
    refclk            => ADC3_clk,                   -- in  std_logic;
    drpclk            => DRP_clk,                    -- in  std_logic;
    rx_tdata          => ADC3_rx_tdata,              -- out std_logic_vector(127 downto 0);
    rx_charisk        => ADC3_charisk,               -- out std_logic_vector(127 downto 0);
    rx_disperror      => ADC3_rx_disperror,          -- out std_logic_vector(3 downto 0);
    rx_notintable     => ADC3_rx_notintable,          -- out std_logic_vector(3 downto 0);
    rx_tvalid         => ADC3_rx_tvalid,             -- out std_logic;
    baseline          => ADC3_baseline,              -- out unsigned(13 downto 0);
    rx_sync           => ADC3_rx_sync,               -- out std_logic;
    rx_aresetn        => ADC3_rx_aresetn,            -- out std_logic;
    rx_sys_reset      => reset,                      -- in  std_logic;
    rx_reset_done     => ADC3_reset_done,            -- out std_logic;
    gt_powergood      => ADC3_gt_powergood,          -- out std_logic
    rxn_in            => ADC3_in_N,                  -- in  std_logic_vector(3 downto 0);
    rxp_in            => ADC3_in_P,                  -- in  std_logic_vector(3 downto 0);
    link_config       => ADC3_link_config            -- out word32_t(3 downto 0)
  );

end architecture rtl;