--===========================================================================--
-- JESD204_control
--===========================================================================--
-- Version  Author        Date               Description
--
-- 0.1      Marc Dejardin 2023/09/28
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
library UNISIM;
  use UNISIM.vcomponents.all;
use work.LMB_IO.all;

entity JESD204_control is
  port (
  rx_core_clk      : in  std_logic;
  rx_core_reset    : in  std_logic;
  rx_tdata         : out std_logic_vector(Nb_of_lanes*32-1 downto 0);
  rx_charisk       : out std_logic_vector(Nb_of_lanes*4-1 downto 0);
  rx_tvalid        : out std_logic;
  rx_LOS           : out std_logic_vector(Nb_of_lanes-1 downto 0); -- Loss of sync signal for each lane
  rx_aresetn       : out std_logic;
  rx_sync          : out std_logic;
  rx_encommaalign  : out std_logic;
  rx_reset_gt      : out std_logic;
  rx_reset_done    : in  std_logic;
  gt0_rxdata       : in  std_logic_vector(31 downto 0);
  gt1_rxdata       : in  std_logic_vector(31 downto 0);
  gt2_rxdata       : in  std_logic_vector(31 downto 0);
  gt3_rxdata       : in  std_logic_vector(31 downto 0);
  gt0_rxcharisk    : in  std_logic_vector(3 downto 0);
  gt1_rxcharisk    : in  std_logic_vector(3 downto 0);
  gt2_rxcharisk    : in  std_logic_vector(3 downto 0);
  gt3_rxcharisk    : in  std_logic_vector(3 downto 0);
  link_config      : out word32_t(3 downto 0)
  );
end;

architecture rtl of JESD204_control is

constant R_char            : std_logic_vector(7 downto 0) := x"1C"; -- K28.0 = begining of multi-frame
constant A_char            : std_logic_vector(7 downto 0) := x"7C"; -- K28.3 = lane alignment
constant Q_char            : std_logic_vector(7 downto 0) := x"9C"; -- K28.4 = start of link config data
constant K_char            : std_logic_vector(7 downto 0) := x"BC"; -- K28.5 = group synchronixation
constant F_char            : std_logic_vector(7 downto 0) := x"FC"; -- K28.7 = frame alignment

type JESD204_state_type is (JESD_wait_for_PHY_reset_done, JESD_CGS_valid, JESD_ILAS,
                            JESD_idle, JESD_error);
signal JESD204_state       : JESD204_state_type := JESD_idle;
signal ILAS_started        : std_logic_vector(Nb_of_lanes-1 downto 0);
signal ILAS_config_started : std_logic;
signal rx_somf_loc         : std_logic_vector(Nb_of_lanes-1 downto 0);
signal K_seen              : unsigned(7 downto 0) := (others => '0');
signal M_read              : unsigned(7 downto 0) := (others => '0');
signal L_read              : unsigned(7 downto 0) := (others => '0');
signal F_read              : unsigned(7 downto 0) := (others => '0');
signal K_read              : unsigned(7 downto 0) := (others => '0');
signal N_read              : unsigned(7 downto 0) := (others => '0');
signal NP_read             : unsigned(7 downto 0) := (others => '0');
signal CS_read             : unsigned(7 downto 0) := (others => '0');
signal SC_read             : unsigned(7 downto 0) := (others => '0');
signal S_read              : unsigned(7 downto 0) := (others => '0');
signal CF_read             : unsigned(7 downto 0) := (others => '0');
signal HD_read             : unsigned(7 downto 0) := (others => '0');
signal gt0_rxcharisk_prev  : std_logic_vector(3 downto 0);
signal gt1_rxcharisk_prev  : std_logic_vector(3 downto 0);
signal gt2_rxcharisk_prev  : std_logic_vector(3 downto 0);
signal gt3_rxcharisk_prev  : std_logic_vector(3 downto 0);
signal gt0_rxdata_prev     : std_logic_vector(31 downto 0);
signal gt1_rxdata_prev     : std_logic_vector(31 downto 0);
signal gt2_rxdata_prev     : std_logic_vector(31 downto 0);
signal gt3_rxdata_prev     : std_logic_vector(31 downto 0);
signal ILAS_started_del    : std_logic_vector(3 downto 0);
signal rx_tvalid_loc       : std_logic;
signal link_config_loc     : word32_t(3 downto 0);
signal SCR                 : std_logic            := '0';   -- Scrambling
signal L                   : unsigned(7 downto 0) := x"04"; -- Number of lanes
signal M                   : unsigned(7 downto 0) := x"01"; -- Number of converters
signal F                   : unsigned(7 downto 0) := x"01"; -- Frame length
signal K                   : unsigned(7 downto 0) := x"1f"; -- Multi-Frame length
signal N                   : unsigned(7 downto 0) := x"0e"; -- Number of converter bits (14)
signal NP                  : unsigned(7 downto 0) := x"10"; -- Number of embedded converter bits (16)
signal CS                  : unsigned(7 downto 0) := x"02"; -- Number of control bits (2)
signal F_pos               : unsigned(7 downto 0) := x"00"; -- Current position in frame
signal K_pos               : unsigned(7 downto 0) := x"00"; -- Current osition in multi-frame
signal rx_LOS_loc          : std_logic_vector(3 downto 0);

type stack_t is array (integer range <>) of Byte_t(15 downto 0);                -- Stack of received bytes for each lane : depth = 16 (4 clocks)
signal rx_stack          : stack_t(Nb_of_lanes-1 downto 0);                     -- data stack for alignment : one stack per Lane
type stack1_t is array (integer range <>) of std_logic_vector(15 downto 0);     -- Stack of FACI flag for each lane : depth = 16 (4 clocks)
signal charisk_stack     : stack1_t(Nb_of_lanes-1 downto 0);                    -- charisk stack for FACI correction : one stack per Lane
signal raddr             : unsigned(3 downto 0) := (others => '0');              --Address coded on 4 bits, read address same for all lanes
signal waddr             : UInt4_t(Nb_of_lanes-1 downto 0)  := (others => (others => '0')); -- One write address for each lane
signal ILAS_config       : Byte_t(31 downto 0)  := (others => (others => '0')); -- Local stack to get link config from ILAS data

signal ILAS_sync         : std_logic := '0';

function unscramble(din : std_logic_vector(15 downto 0); scram : std_logic_vector(15 downto 0)) return std_logic_vector is
  variable dout : std_logic_vector(15 downto 0) := (others => '0');
begin
-- seems to be big endian (MSB first) :
  dout(0)  := din(0)  xor din(14)   xor din(15);
  dout(1)  := din(1)  xor din(15)   xor scram(0);
  dout(2)  := din(2)  xor scram(0)  xor scram(1);
  dout(3)  := din(3)  xor scram(1)  xor scram(2);
  dout(4)  := din(4)  xor scram(2)  xor scram(3);
  dout(5)  := din(5)  xor scram(3)  xor scram(4);
  dout(6)  := din(6)  xor scram(4)  xor scram(5);
  dout(7)  := din(7)  xor scram(5)  xor scram(6);
  dout(8)  := din(8)  xor scram(6)  xor scram(7);
  dout(9)  := din(9)  xor scram(7)  xor scram(8);
  dout(10) := din(10) xor scram(8)  xor scram(9);
  dout(11) := din(11) xor scram(9)  xor scram(10);
  dout(12) := din(12) xor scram(10) xor scram(11);
  dout(13) := din(13) xor scram(11) xor scram(12);
  dout(14) := din(14) xor scram(12) xor scram(13);
  dout(15) := din(15) xor scram(13) xor scram(14);
  return dout;
end function unscramble;

begin
  link_config                   <= link_config_loc;
  rx_aresetn                    <= '1';
  rx_LOS                        <= rx_LOS_loc;

  proc_jesd_protocol : process(rx_core_reset,rx_core_clk)
  variable CGS_counter          : unsigned(7 downto 0);
  variable waddr0               : unsigned(3 downto 0);
  variable waddr1               : unsigned(3 downto 0);
  variable waddr2               : unsigned(3 downto 0);
  variable waddr3               : unsigned(3 downto 0);
  variable raddr0               : unsigned(3 downto 0);
  variable raddr1               : unsigned(3 downto 0);
  variable raddr2               : unsigned(3 downto 0);
  variable raddr3               : unsigned(3 downto 0);
  variable ILAS_mf_number       : unsigned(2 downto 0);
  variable F_pos_loc            : unsigned(7 downto 0);
  variable rx_stack_loc         : stack_t(Nb_of_lanes-1 downto 0);    -- Actual data after FACI recovery
  variable scram                : Word16_t(Nb_of_lanes-1 downto 0);   -- unscrambling memory for each lane
  variable Din_scram            : std_logic_vector(15 downto 0);
  variable rx_stack_unscrambled : word32_t(3 downto 0);
  variable last_F_char          : Byte_t(3 downto 0);                 -- last frame character seen for FACI
  begin
    if rx_core_reset = '1' then
-- Put ADC in sync mode :
      rx_sync                                 <= '0';
-- Data not valid yet :
      rx_tvalid_loc                           <= '0';  -- Valid data after last ILAS byte (/A/ char)
-- reset JESD204_PHY and wait for reset done
      rx_reset_gt                             <= '1';
      rx_encommaalign                         <= '1';            -- Enable comma alignement during CGS phase
      JESD204_state                           <= JESD_wait_for_PHY_reset_done;
      ILAS_sync                               <= '0';
      ILAS_started                            <= (others => '0');
      ILAS_config_started                     <= '0';
      raddr                                   <= (others => '0');
      waddr                                   <= (others => (others => '0'));
      K_seen                                  <= (others => '0'); -- Multi-frame length measured in ILAS sequence
      K_pos                                   <= (others => '0'); -- Multi-frame current position
      K                                       <= (others => '0'); -- Multi-frame current position
      SCR                                     <= '0';
      scram                                   := (others => x"00ff"); -- Unscrambling initial state
      last_F_char                             := (others => (others => '0'));
      link_config_loc                         <= (others => (others => '0'));
      rx_LOS_loc                              <= (others => '0');
    elsif rising_edge(rx_core_clk) then
      link_config_loc(3)(29)                  <= ILAS_sync;
      link_config_loc(3)(27 downto 24)        <= rx_LOS_loc;
      link_config_loc(3)(23 downto 16)        <= std_logic_vector(K_seen);

-- Data are sent on 4 lanes as folowing :
-- L0 : S0(15..8) | S3(15..8) | S5(15..8) | S7(15..8) | ...
-- L1 : S0(7 ..0) | S3(7 ..0) | S5(7 ..0) | S7(7 ..0) | ...
-- L2 : S1(15..8) | S4(15..8) | S6(15..8) | S8(15..8) | ...
-- L3 : S1(7 ..0) | S4(7 ..0) | S6(7 ..0) | S8(7 ..0) | ...
-- We are using some kind of double port memory (simple register stack with write and read address) for each lane
-- The purpose is to align all the lanes during ILAS step.  
      raddr0                                  := raddr+0;
      raddr1                                  := raddr+1;
      raddr2                                  := raddr+2;
      raddr3                                  := raddr+3;

      if K_pos < K-4 then
        K_pos                                 <= K_pos+4;
      else
        K_pos                                 <= (others => '0');
      end if;
      if F > 4 and F_pos < F-4 then
        F_pos                                 <= F_pos+4;
      else
        F_pos                                 <= (others => '0');
      end if;

-- Control presence of Frame Alignment Character insertion (FACI)
-- Go to Loss-of-Sync (LOS) if not found or found elsewhere than expected
-- Search for FACI and see if they are at the right place 
-- MF length (K) is a mulitple of 4, so we should always get the FACI at the 4th place of a 4-byte word :
      if rx_tvalid_loc = '1' then
        for ilane in 0 to Nb_of_lanes-1 loop
          rx_stack_loc(ilane)(0)              := rx_stack(ilane)(to_integer(raddr0));
          rx_stack_loc(ilane)(1)              := rx_stack(ilane)(to_integer(raddr1));
          rx_stack_loc(ilane)(2)              := rx_stack(ilane)(to_integer(raddr2));
          rx_stack_loc(ilane)(3)              := rx_stack(ilane)(to_integer(raddr3));
          F_pos_loc                           := F_pos;
          if charisk_stack(ilane)(to_integer(raddr0)) = '1' then
            rx_LOS_loc(ilane)                 <= '1';
            if F_pos_loc = F-1 then                                -- Frame FACI (should only be valid for F=1
              if rx_stack(ilane)(to_integer(raddr0)) = F_char then
                rx_LOS_loc(ilane)             <= '0';
                if SCR = '0' then
                  rx_stack_loc(ilane)(0)      := last_F_char(ilane);
                end if;
              else
                last_F_char(ilane)            := rx_stack(ilane)(to_integer(raddr0));
              end if;
            end if;
          elsif F_pos_loc = F-1 then                               -- Save last Frame character in normal situation
            last_F_char(ilane)                := rx_stack(ilane)(to_integer(raddr0));
          end if;
          F_pos_loc                           := F_pos_loc+1;
          if F_pos_loc >= F then
            F_pos_Loc                         := (others => '0');
          end if;
          if charisk_stack(ilane)(to_integer(raddr1)) = '1' then
            rx_LOS_loc(ilane)                 <= '1';
            if F_pos_loc = F-1 then                                -- Frame FACI (should only be valid for F=1 or F=2)
              if rx_stack(ilane)(to_integer(raddr1)) = F_char then
                rx_LOS_loc(ilane)             <= '0';
                if SCR = '0' then
                  rx_stack_loc(ilane)(1)      := last_F_char(ilane);
                end if;
              else
                last_F_char(ilane)            := rx_stack(ilane)(to_integer(raddr1));
              end if;
            end if;
          elsif F_pos_loc = F-1 then                               -- Save last Frame character in normal situation
            last_F_char(ilane)                := rx_stack(ilane)(to_integer(raddr1));
          end if;
          F_pos_loc                           := F_pos_loc+1;
          if F_pos_loc >= F then
            F_pos_Loc                         := (others => '0');
          end if;
          if charisk_stack(ilane)(to_integer(raddr2)) = '1' then
            rx_LOS_loc(ilane)                 <= '1';
            if F_pos_loc = F-1 then                                -- Frame FACI (shoudl only be valid for F=1)
              if rx_stack(ilane)(to_integer(raddr2)) = F_char then
                rx_LOS_loc(ilane)             <= '0';
                if SCR = '0' then
                  rx_stack_loc(ilane)(2)      := last_F_char(ilane);
                end if;
              else
                last_F_char(ilane)            := rx_stack(ilane)(to_integer(raddr2));
              end if;
            end if;
          elsif F_pos_loc = F-1 then                               -- Save last Frame character in normal situation
            last_F_char(ilane)                := rx_stack(ilane)(to_integer(raddr2));
          end if;
          if charisk_stack(ilane)(to_integer(raddr3)) = '1' then
            rx_LOS_loc(ilane)                 <= '1';
            if K_pos+3 = K-1 then                                  -- Multi-Frame FACI
              if rx_stack(ilane)(to_integer(raddr3)) = A_char then
                rx_LOS_loc(ilane)             <= '0';
                if SCR = '0' then
                  rx_stack_loc(ilane)(3)      := last_F_char(ilane);
                end if;
              else
                last_F_char(ilane)            := rx_stack(ilane)(to_integer(raddr3));
              end if;
            elsif F_pos_loc = F-1 then
              if rx_stack(ilane)(to_integer(raddr3)) = F_char then -- Frame FACI (should only be valid for F=1, F=2, F=4 and F=4*n)
                rx_LOS_loc(ilane)             <= '0';
                if SCR = '0' then
                  rx_stack_loc(ilane)(3)      := last_F_char(ilane);
                end if;
              else
                last_F_char(ilane)            := rx_stack(ilane)(to_integer(raddr3));
              end if;
            end if;
          elsif F_pos_loc = F-1 then
            last_F_char(ilane)                := rx_stack(ilane)(to_integer(raddr3));
          end if;
        end loop;
      end if;

-- Unscramble the data if needed before reading them :
-- Seems that the scrambling is done on lane data bytes and not on the samples words
-- Unscramble only when we are in data mode (after ILAS step)
      if SCR = '1'and rx_tvalid_loc = '1' then
        for ilane in 0 to Nb_of_lanes-1 loop
          Din_scram                             := rx_stack(ilane)(to_integer(raddr0))&rx_stack(ilane)(to_integer(raddr1));
          rx_stack_unscrambled(ilane)(15 downto 0)  := unscramble(Din_scram, scram(ilane));
          scram(ilane)                          := Din_scram;
          Din_scram                             := rx_stack(ilane)(to_integer(raddr2))&rx_stack(ilane)(to_integer(raddr3));
          rx_stack_unscrambled(ilane)(31 downto 16) := unscramble(Din_scram, scram(ilane));
          scram(ilane)                          := Din_scram;
        end loop;
      else
        rx_stack_unscrambled(0)(15 downto 0)  := rx_stack_loc(0)(0)&rx_stack_loc(0)(1);
        rx_stack_unscrambled(0)(31 downto 16) := rx_stack_loc(0)(2)&rx_stack_loc(0)(3);
        rx_stack_unscrambled(1)(15 downto 0)  := rx_stack_loc(1)(0)&rx_stack_loc(1)(1);
        rx_stack_unscrambled(1)(31 downto 16) := rx_stack_loc(1)(2)&rx_stack_loc(1)(3);
        rx_stack_unscrambled(2)(15 downto 0)  := rx_stack_loc(2)(0)&rx_stack_loc(2)(1);
        rx_stack_unscrambled(2)(31 downto 16) := rx_stack_loc(2)(2)&rx_stack_loc(2)(3);
        rx_stack_unscrambled(3)(15 downto 0)  := rx_stack_loc(3)(0)&rx_stack_loc(3)(1);
        rx_stack_unscrambled(3)(31 downto 16) := rx_stack_loc(3)(2)&rx_stack_loc(3)(3);
      end if;
      if rx_tvalid_loc = '1' then -- During data mode, send ADC samples
        rx_charisk                            <= (others => '0');
        rx_charisk(0)                         <= charisk_stack(1)(to_integer(raddr0));
        rx_charisk(1)                         <= charisk_stack(0)(to_integer(raddr0));
        rx_charisk(2)                         <= charisk_stack(3)(to_integer(raddr0));
        rx_charisk(3)                         <= charisk_stack(2)(to_integer(raddr0));
        rx_charisk(4)                         <= charisk_stack(1)(to_integer(raddr1));
        rx_charisk(5)                         <= charisk_stack(0)(to_integer(raddr1));
        rx_charisk(6)                         <= charisk_stack(3)(to_integer(raddr1));
        rx_charisk(7)                         <= charisk_stack(2)(to_integer(raddr1));
        rx_charisk(8)                         <= charisk_stack(1)(to_integer(raddr2));
        rx_charisk(9)                         <= charisk_stack(0)(to_integer(raddr2));
        rx_charisk(10)                        <= charisk_stack(3)(to_integer(raddr2));
        rx_charisk(11)                        <= charisk_stack(2)(to_integer(raddr2));
        rx_charisk(12)                        <= charisk_stack(1)(to_integer(raddr3));
        rx_charisk(13)                        <= charisk_stack(0)(to_integer(raddr3));
        rx_charisk(14)                        <= charisk_stack(3)(to_integer(raddr3));
        rx_charisk(15)                        <= charisk_stack(2)(to_integer(raddr3));
        rx_tdata(15 downto 0)                 <= rx_stack_unscrambled(0)(15 downto 8) &rx_stack_unscrambled(1)(15 downto 8);
        rx_tdata(31 downto 16)                <= rx_stack_unscrambled(2)(15 downto 8) &rx_stack_unscrambled(3)(15 downto 8);
        rx_tdata(47 downto 32)                <= rx_stack_unscrambled(0)(7 downto 0)  &rx_stack_unscrambled(1)(7 downto 0);
        rx_tdata(63 downto 48)                <= rx_stack_unscrambled(2)(7 downto 0)  &rx_stack_unscrambled(3)(7 downto 0);
        rx_tdata(79 downto 64)                <= rx_stack_unscrambled(0)(31 downto 24)&rx_stack_unscrambled(1)(31 downto 24);
        rx_tdata(95 downto 80)                <= rx_stack_unscrambled(2)(31 downto 24)&rx_stack_unscrambled(3)(31 downto 24);
        rx_tdata(111 downto 96)               <= rx_stack_unscrambled(0)(23 downto 16)&rx_stack_unscrambled(1)(23 downto 16);
        rx_tdata(127 downto 112)              <= rx_stack_unscrambled(2)(23 downto 16)&rx_stack_unscrambled(3)(23 downto 16);
      else                        -- During sync process, send gt_rx data image
        rx_charisk(0)                         <= charisk_stack(0)(to_integer(raddr1));
        rx_charisk(1)                         <= charisk_stack(0)(to_integer(raddr0));
        rx_charisk(2)                         <= charisk_stack(1)(to_integer(raddr1));
        rx_charisk(3)                         <= charisk_stack(1)(to_integer(raddr0));
        rx_charisk(4)                         <= charisk_stack(2)(to_integer(raddr1));
        rx_charisk(5)                         <= charisk_stack(2)(to_integer(raddr0));
        rx_charisk(6)                         <= charisk_stack(3)(to_integer(raddr1));
        rx_charisk(7)                         <= charisk_stack(3)(to_integer(raddr0));
        rx_charisk(8)                         <= charisk_stack(0)(to_integer(raddr3));
        rx_charisk(9)                         <= charisk_stack(0)(to_integer(raddr2));
        rx_charisk(10)                        <= charisk_stack(1)(to_integer(raddr3));
        rx_charisk(11)                        <= charisk_stack(1)(to_integer(raddr2));
        rx_charisk(12)                        <= charisk_stack(2)(to_integer(raddr3));
        rx_charisk(13)                        <= charisk_stack(2)(to_integer(raddr2));
        rx_charisk(14)                        <= charisk_stack(3)(to_integer(raddr3));
        rx_charisk(15)                        <= charisk_stack(3)(to_integer(raddr2));
        rx_tdata(7   downto 0)                <= rx_stack(0)(to_integer(raddr1));
        rx_tdata(15  downto 8)                <= rx_stack(0)(to_integer(raddr0));
        rx_tdata(23  downto 16)               <= rx_stack(1)(to_integer(raddr1));
        rx_tdata(31  downto 24)               <= rx_stack(1)(to_integer(raddr0));
        rx_tdata(39  downto 32)               <= rx_stack(2)(to_integer(raddr1));
        rx_tdata(47  downto 40)               <= rx_stack(2)(to_integer(raddr0));
        rx_tdata(55  downto 48)               <= rx_stack(3)(to_integer(raddr1));
        rx_tdata(63  downto 56)               <= rx_stack(3)(to_integer(raddr0));
        rx_tdata(71  downto 64)               <= rx_stack(0)(to_integer(raddr3));
        rx_tdata(79  downto 72)               <= rx_stack(0)(to_integer(raddr2));
        rx_tdata(87  downto 80)               <= rx_stack(1)(to_integer(raddr3));
        rx_tdata(95  downto 88)               <= rx_stack(1)(to_integer(raddr2));
        rx_tdata(103 downto 96)               <= rx_stack(2)(to_integer(raddr3));
        rx_tdata(111 downto 104)              <= rx_stack(2)(to_integer(raddr2));
        rx_tdata(119 downto 112)              <= rx_stack(3)(to_integer(raddr3));
        rx_tdata(127 downto 120)              <= rx_stack(3)(to_integer(raddr2));
      end if;
      raddr                                   <= raddr+4;

      waddr0                                  := waddr(0)+0;
      waddr1                                  := waddr(0)+1;
      waddr2                                  := waddr(0)+2;
      waddr3                                  := waddr(0)+3;
      charisk_stack(0)(to_integer(waddr0))    <= gt0_rxcharisk_prev(0);
      charisk_stack(0)(to_integer(waddr1))    <= gt0_rxcharisk_prev(1);
      charisk_stack(0)(to_integer(waddr2))    <= gt0_rxcharisk_prev(2);
      charisk_stack(0)(to_integer(waddr3))    <= gt0_rxcharisk_prev(3);
      rx_stack(0)(to_integer(waddr0))         <= gt0_rxdata_prev(7  downto 0);
      rx_stack(0)(to_integer(waddr1))         <= gt0_rxdata_prev(15 downto 8);
      rx_stack(0)(to_integer(waddr2))         <= gt0_rxdata_prev(23 downto 16);
      rx_stack(0)(to_integer(waddr3))         <= gt0_rxdata_prev(31 downto 24);
      waddr0                                  := waddr(1)+0;
      waddr1                                  := waddr(1)+1;
      waddr2                                  := waddr(1)+2;
      waddr3                                  := waddr(1)+3;
      charisk_stack(1)(to_integer(waddr0))    <= gt1_rxcharisk_prev(0);
      charisk_stack(1)(to_integer(waddr1))    <= gt1_rxcharisk_prev(1);
      charisk_stack(1)(to_integer(waddr2))    <= gt1_rxcharisk_prev(2);
      charisk_stack(1)(to_integer(waddr3))    <= gt1_rxcharisk_prev(3);
      rx_stack(1)(to_integer(waddr0))         <= gt1_rxdata_prev(7  downto 0);
      rx_stack(1)(to_integer(waddr1))         <= gt1_rxdata_prev(15 downto 8);
      rx_stack(1)(to_integer(waddr2))         <= gt1_rxdata_prev(23 downto 16);
      rx_stack(1)(to_integer(waddr3))         <= gt1_rxdata_prev(31 downto 24);
      waddr0                                  := waddr(2)+0;
      waddr1                                  := waddr(2)+1;
      waddr2                                  := waddr(2)+2;
      waddr3                                  := waddr(2)+3;
      charisk_stack(2)(to_integer(waddr0))    <= gt2_rxcharisk_prev(0);
      charisk_stack(2)(to_integer(waddr1))    <= gt2_rxcharisk_prev(1);
      charisk_stack(2)(to_integer(waddr2))    <= gt2_rxcharisk_prev(2);
      charisk_stack(2)(to_integer(waddr3))    <= gt2_rxcharisk_prev(3);
      rx_stack(2)(to_integer(waddr0))         <= gt2_rxdata_prev(7  downto 0);
      rx_stack(2)(to_integer(waddr1))         <= gt2_rxdata_prev(15 downto 8);
      rx_stack(2)(to_integer(waddr2))         <= gt2_rxdata_prev(23 downto 16);
      rx_stack(2)(to_integer(waddr3))         <= gt2_rxdata_prev(31 downto 24);
      waddr0                                  := waddr(3)+0;
      waddr1                                  := waddr(3)+1;
      waddr2                                  := waddr(3)+2;
      waddr3                                  := waddr(3)+3;
      charisk_stack(3)(to_integer(waddr0))    <= gt3_rxcharisk_prev(0);
      charisk_stack(3)(to_integer(waddr1))    <= gt3_rxcharisk_prev(1);
      charisk_stack(3)(to_integer(waddr2))    <= gt3_rxcharisk_prev(2);
      charisk_stack(3)(to_integer(waddr3))    <= gt3_rxcharisk_prev(3);
      rx_stack(3)(to_integer(waddr0))         <= gt3_rxdata_prev(7  downto 0);
      rx_stack(3)(to_integer(waddr1))         <= gt3_rxdata_prev(15 downto 8);
      rx_stack(3)(to_integer(waddr2))         <= gt3_rxdata_prev(23 downto 16);
      rx_stack(3)(to_integer(waddr3))         <= gt3_rxdata_prev(31 downto 24);

      waddr(0)                                <= waddr(0)+4;
      waddr(1)                                <= waddr(1)+4;
      waddr(2)                                <= waddr(2)+4;
      waddr(3)                                <= waddr(3)+4;

--      if gt0_rxdata=x"fc" and (F_pos=F or F=1) then
--      end if;
      gt0_rxcharisk_prev                      <= gt0_rxcharisk;
      gt1_rxcharisk_prev                      <= gt1_rxcharisk;
      gt2_rxcharisk_prev                      <= gt2_rxcharisk;
      gt3_rxcharisk_prev                      <= gt3_rxcharisk;
      gt0_rxdata_prev                         <= gt0_rxdata;
      gt1_rxdata_prev                         <= gt1_rxdata;
      gt2_rxdata_prev                         <= gt2_rxdata;
      gt3_rxdata_prev                         <= gt3_rxdata;
      ILAS_started_del                        <= ILAS_started;
      rx_tvalid                               <= rx_tvalid_loc;


-- Look for BC0 marker and replace actual sample with x3fff :
--      if gt1_rxdata(1 downto 0) /= "00" then
--        gt0_rxdata_prev(7 downto 0) <= x"ff";
--        gt1_rxdata_prev(7 downto 0) <= x"ff";
--      end if;
--      if gt3_rxdata(1 downto 0) /= "00" then
--        gt2_rxdata_prev(7 downto 0) <= x"ff";
--        gt3_rxdata_prev(7 downto 0) <= x"ff";
--      end if;
--      if gt1_rxdata(9 downto 8) /= "00" then
--        gt0_rxdata_prev(15 downto 8) <= x"ff";
--        gt1_rxdata_prev(15 downto 8) <= x"ff";
--      end if;
--      if gt3_rxdata(9 downto 8) /= "00" then
--        gt2_rxdata_prev(15 downto 8) <= x"ff";
--        gt3_rxdata_prev(15 downto 8) <= x"ff";
--      end if;
--      if gt1_rxdata(17 downto 16) /= "00" then
--        gt0_rxdata_prev(23 downto 16) <= x"ff";
--        gt1_rxdata_prev(23 downto 16) <= x"ff";
--      end if;
--      if gt3_rxdata(17 downto 16) /= "00" then
--        gt2_rxdata_prev(23 downto 16) <= x"ff";
--        gt3_rxdata_prev(23 downto 16) <= x"ff";
--      end if;
--      if gt1_rxdata(25 downto 24) /= "00" then
--        gt0_rxdata_prev(31 downto 24) <= x"ff";
--        gt1_rxdata_prev(31 downto 24) <= x"ff";
--      end if;
--      if gt3_rxdata(25 downto 24) /= "00" then
--        gt2_rxdata_prev(31 downto 24) <= x"ff";
--        gt3_rxdata_prev(31 downto 24) <= x"ff";
--      end if;

      case JESD204_state is
      when JESD_idle =>
        rx_reset_gt                           <= '0';
        rx_sync                               <= '1';

      when JESD_wait_for_PHY_reset_done  =>
        rx_reset_gt                           <= '0';
        if rx_reset_done = '1' then
          CGS_counter                         := (others => '0');
          JESD204_state                       <= JESD_CGS_valid;
        end if;

      when JESD_CGS_valid =>                                       -- Code Group Synchronization validation : check for K28.5 character
        if CGS_counter < x"ff" then
          if (gt0_rxdata /= x"bcbcbcbc") or (gt1_rxdata /= x"bcbcbcbc") or
             (gt2_rxdata /= x"bcbcbcbc") or (gt3_rxdata /= x"bcbcbcbc") then 
            link_config_loc(3)(30)            <= '1';              -- CGS error flag
          end if;
          CGS_counter                         := CGS_counter + 1;
        else
          if link_config_loc(3)(30) = '1'then
            JESD204_state                     <= JESD_error;
          else
-- Release ADC sync mode :
            rx_sync                           <= '1';              -- We are synced : release Syncb signal
            rx_encommaalign                   <= '0';              -- Stop comma alignement 
            JESD204_state                     <= JESD_ILAS;
            ILAS_started                      <= (others => '0');
            waddr                             <= (others => (others => '0'));
            ILAS_mf_number                    := (others => '0');
            K_seen                            <= (others => '0');
            K_pos                             <= (others => '0');
            K                                 <= (others => '0');
          end if;
        end if;

      when JESD_ILAS =>    -- MultiFrame 0 : align lanes and get MF length
-- First, wait for ILAS start :
-- Search for /R/ (K28.0) character : Start Of Multi-Frame and align the lines
        if ILAS_started(0) = '0' then
          if gt0_rxdata(7 downto 0) = R_char and gt0_rxcharisk(0) = '1' then
            ILAS_started(0)                   <= '1';
            K_seen                            <= x"04";
            waddr(0)                          <= "0000";
          elsif gt0_rxdata(15 downto 8) = R_char and gt0_rxcharisk(1) = '1' then
            ILAS_started(0)                   <= '1';
            K_seen                            <= x"03";
            waddr(0)                          <= "1111";
          elsif gt0_rxdata(23 downto 16) = R_char and gt0_rxcharisk(2) = '1' then
            ILAS_started(0)                   <= '1';
            K_seen                            <= x"02";
            waddr(0)                          <= "1110";
          elsif gt0_rxdata(31 downto 24) = R_char and gt0_rxcharisk(3) = '1' then
            ILAS_started(0)                   <= '1';
            K_seen                            <= x"01";
            waddr(0)                          <= "1101";
          end if;
        end if;
        if ILAS_started(1) = '0' then
          if gt1_rxdata(7 downto 0) = R_char and gt1_rxcharisk(0) = '1' then
            ILAS_started(1)                   <= '1';
            waddr(1)                          <= "0000";
          elsif gt1_rxdata(15 downto 8) = R_char and gt1_rxcharisk(1) = '1' then
            ILAS_started(1)                   <= '1';
            waddr(1)                          <= "1111";
          elsif gt1_rxdata(23 downto 16) = R_char and gt1_rxcharisk(2) = '1' then
            ILAS_started(1)                   <= '1';
            waddr(1)                          <= "1110";
          elsif gt1_rxdata(31 downto 24) = R_char and gt1_rxcharisk(3) = '1' then
            ILAS_started(1)                   <= '1';
            waddr(1)                          <= "1101";
          end if;
        end if;
        if ILAS_started(2) = '0' then
          if gt2_rxdata(7 downto 0) = R_char and gt2_rxcharisk(0) = '1' then
            ILAS_started(2)                   <= '1';
            waddr(2)                          <= "0000";
          elsif gt2_rxdata(15 downto 8) = R_char and gt2_rxcharisk(1) = '1' then
            ILAS_started(2)                   <= '1';
            waddr(2)                          <= "1111";
          elsif gt2_rxdata(23 downto 16) = R_char and gt2_rxcharisk(2) = '1' then
            ILAS_started(2)                   <= '1';
            waddr(2)                          <= "1110";
          elsif gt2_rxdata(31 downto 24) = R_char and gt2_rxcharisk(3) = '1' then
            ILAS_started(2)                   <= '1';
            waddr(2)                          <= "1101";
          end if;
        end if;
        if ILAS_started(3) = '0' then
          if gt3_rxdata(7 downto 0) = R_char and gt3_rxcharisk(0) = '1' then
            ILAS_started(3)                   <= '1';
            waddr(3)                          <= "0000";
          elsif gt3_rxdata(15 downto 8) = R_char and gt3_rxcharisk(1) = '1' then
            ILAS_started(3)                   <= '1';
            waddr(3)                          <= "1111";
          elsif gt3_rxdata(23 downto 16) = R_char and gt3_rxcharisk(2) = '1' then
            ILAS_started(3)                   <= '1';
            waddr(3)                          <= "1110";
          elsif gt3_rxdata(31 downto 24) = R_char and gt3_rxcharisk(3) = '1' then
            ILAS_started(3)                   <= '1';
            waddr(3)                          <= "1101";
          end if;
        end if;
          
-- Search for /A/ (K28.3) character : End Of Multi-Frame -> Measure Multi-frame length in data
        if ILAS_started(0) = '1' then
           if gt0_rxdata(7 downto 0) = A_char and gt0_rxcharisk(0) = '1' then
            if ILAS_MF_number = 0 then
              K_seen                          <= K_seen+1;
            end if;
            ILAS_MF_number                    := ILAS_MF_number+1;
          elsif gt0_rxdata(15 downto 8) = A_char and gt0_rxcharisk(1) = '1' then
            if ILAS_MF_number = 0 then
              K_seen                          <= K_seen+2;
            end if;
            ILAS_MF_number                    := ILAS_MF_number+1;
          elsif gt0_rxdata(23 downto 16) = A_char and gt0_rxcharisk(2) = '1' then
            if ILAS_MF_number = 0 then
              K_seen                          <= K_seen+3;
            end if;
            ILAS_MF_number                    := ILAS_MF_number+1;
          elsif gt0_rxdata(31 downto 24) = A_char and gt0_rxcharisk(3) = '1' then
            if ILAS_MF_number = 0 then
              K_seen                          <= K_seen+4;
            end if;
            ILAS_MF_number                    := ILAS_MF_number+1;
          elsif ILAS_MF_number = 0 then
            K_seen                            <= K_seen+4;
          end if;
        end if;

-- Start to read synchronized data as soon as we have seen ILAS starting point on all lanes :
        if ILAS_started_del(0) = '1' and ILAS_started_del(1) = '1' and
           ILAS_started_del(2) = '1' and ILAS_started_del(3) = '1' and
           ILAS_sync = '0' then
          ILAS_config_started                 <= '0';
          ILAS_sync                           <= '1';
          raddr                               <= (others => '0');
          K_pos                               <= (others => '0');
          F_pos                               <= (others => '0');
        end if;

        if ILAS_MF_number = 1 then   -- MultiFrame 1 : get link parameters
-- Get ADC config in ILAS Multiframe 1 :
-- Search for Multi-frame 1 start :
          if gt0_rxdata(7 downto 0) = Q_char and gt0_rxcharisk(0) = '1' then
            ILAS_config_started                 <= '1';
            K_pos                               <= x"03";
            ILAS_config(0)                      <= gt0_rxdata(15 downto 8);
            ILAS_config(1)                      <= gt0_rxdata(23 downto 16);
            ILAS_config(2)                      <= gt0_rxdata(31 downto 24);
          elsif gt0_rxdata(15 downto 8) = Q_char and gt0_rxcharisk(1) = '1' then
            ILAS_config_started                 <= '1';
            K_pos                               <= x"02";
            ILAS_config(0)                      <= gt0_rxdata(23 downto 16);
            ILAS_config(1)                      <= gt0_rxdata(31 downto 24);
          elsif gt0_rxdata(23 downto 16) = Q_char and gt0_rxcharisk(2) = '1' then
            ILAS_config_started                 <= '1';
            K_pos                               <= x"01";
            ILAS_config(0)                      <= gt0_rxdata(31 downto 24);
          elsif gt0_rxdata(31 downto 24) = Q_char and gt0_rxcharisk(3) = '1' then
            ILAS_config_started                 <= '1';
            K_pos                               <= x"00";
          elsif K_pos < 28 and ILAS_config_started = '1' then
            ILAS_config(to_integer(K_pos))      <= gt0_rxdata(7 downto 0);
            ILAS_config(to_integer(K_pos+1))    <= gt0_rxdata(15 downto 8);
            ILAS_config(to_integer(K_pos+2))    <= gt0_rxdata(23 downto 16);
            ILAS_config(to_integer(K_pos+3))    <= gt0_rxdata(31 downto 24);
            K_pos                               <= K_pos+4;
          end if;
          if K_pos > 15 and ILAS_config_started = '1' then -- save link info when we have them
            link_config_loc(0)                  <= ILAS_config(3)  & ILAS_config(2)  & ILAS_config(1)  & ILAS_config(0);
            link_config_loc(1)                  <= ILAS_config(7)  & ILAS_config(6)  & ILAS_config(5)  & ILAS_config(4);
            link_config_loc(2)                  <= ILAS_config(11) & ILAS_config(10) & ILAS_config(9)  & ILAS_config(8);
            link_config_loc(3)(15 downto 0)     <=                                     ILAS_config(13) & ILAS_config(12);
            SCR                                 <=                   ILAS_config(3)(7);
            L                                   <=    "000"&unsigned(ILAS_config(3)(4 downto 0))+1;
            F                                   <=          unsigned(ILAS_config(4))+1;
            K                                   <=    "000"&unsigned(ILAS_config(5)(4 downto 0))+1;
            M                                   <=          unsigned(ILAS_config(6))+1;
            N                                   <=    "000"&unsigned(ILAS_config(7)(4 downto 0))+1;
            CS                                  <= "000000"&unsigned(ILAS_config(7)(7 downto 6));
            NP                                  <=    "000"&unsigned(ILAS_config(8)(4 downto 0))+1;
          end if;
        end if;

        if ILAS_MF_number = 2 and K /= K_seen then       -- Inconsistancy in parameters
          JESD204_state                       <= JESD_error;
          link_config_loc(3)(31)              <= '1';    -- ILAS_error flag
        end if;

        if ILAS_MF_number = 4 then
-- Data valid soon coming :
-- but only when we have purged the last ILAS data :
          if rx_stack(0)(to_integer(raddr3)) = A_char and charisk_stack(0)(to_integer(raddr3)) = '1' and
             rx_stack(1)(to_integer(raddr3)) = A_char and charisk_stack(1)(to_integer(raddr3)) = '1' and
             rx_stack(2)(to_integer(raddr3)) = A_char and charisk_stack(2)(to_integer(raddr3)) = '1' and
             rx_stack(3)(to_integer(raddr3)) = A_char and charisk_stack(3)(to_integer(raddr3)) = '1' then
-- Synchronize Frames and Multi-Frames on first sent data :
            K_pos                             <= (others => '0');
            F_pos                             <= (others => '0');
            rx_tvalid_loc                     <= '1';
-- Then prepare unscrambling if requested
            scram                             := (others => x"00ff");
            JESD204_state                     <= JESD_idle;
          end if;
        end if;
      when JESD_error =>
        JESD204_state                         <= JESD_idle; -- Don't get stuck here for the moment.
      end case;
    end if;
  end process proc_jesd_protocol;
end rtl;