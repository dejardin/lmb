library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.LMB_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity PLL_control is
  generic (
    PLL_address       : std_logic_vector(6 downto 0) := "1101000"
  );
  port (
    I2C_number        : in     natural range 0 to 3;
    I2C_address_out   : out    std_logic_vector(6 downto 0);
    I2C_Access        : in     std_logic;
    I2C_R_Wb          : in     std_logic;
    I2C_reg_number    : in     std_logic_vector(7 downto 0);

    PLL_update_en     : in     std_logic;
    PLL_update_reg    : in     std_logic_vector(7 downto 0);
    PLL_update_val    : in     std_logic_vector(7 downto 0);

    busy_out          : out    std_logic;
    I2C_error         : out    std_logic;
    I2C_Reg_data_in   : in     std_logic_vector(15 downto 0);
    I2C_Reg_data_out  : out    std_logic_vector(15 downto 0);
    I2C_n_ack         : out    unsigned(7 downto 0);
  
    PLL_reset         : in     std_logic;
    PLL_reset_done    : out    std_logic;
    PLL_init_done     : out    std_logic;
    I2C_scl           : inout  std_logic;
    I2C_sda           : inout  std_logic;
    I2C_clk           : in     std_logic
  );
end entity PLL_control;

architecture rtl of PLL_control is

constant I2C_addresses    : Word7_t(3 downto 0) := ("1001110","1001110","1001110","1101000"); 
signal I2C_address_loc        : std_logic_vector(6 downto 0);
signal I2C_last_transfer      : std_logic := '1';              -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
signal I2C_reset_n            : std_logic;                     -- active low reset
signal I2C_sda_loc            : std_logic;                     -- 
signal I2C_scl_loc            : std_logic;                     -- 
signal I2C_scl_loc_del        : std_logic;
signal I2C_ena                : std_logic;                     -- 
signal I2C_ena_del            : std_logic;
--signal I2C_addr               : STD_LOGIC_VECTOR(6 DOWNTO 0); -- address of target slave
signal I2C_R_Wb_loc           : std_logic;                     -- '0' is write, '1' is read
signal I2C_data_w             : std_logic_vector(7 downto 0);  -- data to write to slave
signal I2C_data_r             : std_logic_vector(15 downto 0); -- data read from slave
signal I2C_loc_busy           : std_logic;                     -- indicates transaction in progress
signal I2C_loc_busy_prev      : std_logic;                     -- mandatory to detect busy transiton
signal I2C_loc_data_r         : std_logic_vector(7 downto 0);  -- data read from slave
signal I2C_ack_error          : std_logic;                     -- flag if improper acknowledge from slave
signal I2C_slave_ack          : std_logic;                     -- flag if improper acknowledge from slave
signal loc_I2C_n_ack          : unsigned(7 downto 0)          := (others => '0');
signal PLL_init               : std_logic := '1';
signal PLL_R_Wb               : std_logic;
signal PLL_busy               : std_logic := '0';
signal reg_number_loc         : natural range 0 to 63 := 0;
signal PLL_update_en_del      : std_logic := '0';
signal PLL_reset_done_loc     : std_logic := '0';
signal PLL_reset_duration     : unsigned(20 downto 0);
signal I2C_max_bytes          : natural range 0 to 2 := 1;
signal I2C_n_bytes            : natural range 0 to 2 := 0;
signal counter                : unsigned(7 downto 0);

-- PLL SI5319A settings :
-- F_out = F_in/N31*N2/N1 with N31=80, N2=N@_HS*N2_LS=5*512 and N1=N1_HS*N1_LS=1*4
constant n_PLL_init_registers : natural := 31;
signal init_Reg_Number        : natural range 0 to n_PLL_init_registers := 0;
signal PLL_reg_numbers        : Byte_t(n_PLL_init_registers downto 1);
signal PLL_reg_numbers_def    : Byte_t(n_PLL_init_registers downto 1) := (x"88", x"8b", x"8a", x"84", x"83", x"30", x"2f", x"2e", x"2d", x"2c", x"2b",
                                                                                 x"2a", x"29", x"28", x"21", x"20", x"1f", x"19", x"18", x"17", x"16",
                                                                                 x"14", x"13", x"0b", x"0a", x"08", x"06", x"05", x"03", x"02", x"00");

signal PLL_reg_contents       : Byte_t(n_PLL_init_registers downto 1);
signal PLL_reg_contents_def   : Byte_t(n_PLL_init_registers downto 1) := (x"40", x"ff", x"0f", x"02", x"1f", x"4f", x"00", x"00", x"4f", x"00", x"00",
                                                                                 x"ff", x"01", x"20", x"00", x"00", x"00", x"00", x"3f", x"1f", x"df", -- 1280 MHz
--                                                                                 x"ff", x"01", x"20", x"01", x"00", x"00", x"00", x"3f", x"1f", x"df", -- 640 MHz
                                                                                 x"3e", x"2c", x"40", x"00", x"00", x"2d", x"ed", x"15", x"A2", x"34");
type Index_t   is array (integer range <>) of natural range 0 to 255;
constant PLL_index : index_t(255 downto 0) := (255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255, 35,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255, 30, 29,255, 31,255,255,255, 28, 27,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
                                               255,255,255,255,255,255,255,255,255,255,255,255,255,255,255, 26,
                                                25, 24, 23, 22, 21, 20, 19, 18,255,255,255,255,255,255, 17, 16,
                                                15,255,255,255,255,255, 14, 13, 12, 11,255, 10,  9,255,255,255,
                                               255,255,255,255,  8,  7,255,  6,255,  5,  4,255,  3,  2,255,  1);


-- Finish I2C writing by setting b6 (x40) of register 136 (x88) to launch an internal calibration of the PLL after register setting
                                      
type   I2C_state_type is (init_PLL,          idle,
                          write_reg_address, wait_for_busy_reg,
                          read_reg_data,     wait_for_busy_read,
                          write_reg_data,    wait_for_busy_write);
signal I2C_state     : I2C_state_type := idle;

begin  -- architecture behavioral

  I2C_reset_n       <= not PLL_reset;
  I2C_error         <= I2C_ack_error;
  I2C_Reg_data_out  <= I2C_data_r;
  PLL_reset_done    <= PLL_reset_done_loc;
  I2C_address_out   <= I2C_address_loc;
  busy_out          <= PLL_busy;

  Inst_I2C_master : entity work.I2C_master
  port map(
    clk             => I2C_clk,             --system clock
    reset_n         => I2C_reset_n,         --active low reset
    ena             => I2C_ena,             --latch in command
    addr            => I2C_address_loc,     --address of target slave (default = PLL) 
    R_Wb            => I2C_R_Wb_loc,        --'0' is write, '1' is read
    data_w          => I2C_data_w,          --data to write to slave
    busy            => I2C_loc_busy,        --indicates transaction in progress
    data_r          => I2C_loc_data_r,      --data read from slave
    ack_error       => I2C_ack_error,       --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,       -- acknowledge from slave
    sda             => I2C_sda,             --serial data output of i2c bus
    scl             => I2C_scl,             --serial clock output of i2c bus
    scl_int         => I2C_scl_loc,         --internal serial clock output of i2c bus
    sda_int         => I2C_sda_loc          --internal serial data output of i2c bus
  );

  update_PLL : process(I2C_clk, PLL_update_en) -- Modify default PLL values
  begin
    if PLL_reset = '1' then
      PLL_reg_numbers     <= PLL_reg_numbers_def;
      PLL_reg_contents    <= PLL_reg_contents_def;
    elsif rising_edge(I2C_clk) then
      PLL_update_en_del   <= PLL_update_en;
      if PLL_update_en = '1' and PLL_update_en_del = '0' then
        PLL_reg_contents(PLL_index(to_integer(unsigned(PLL_update_reg)))) <= PLL_update_val;
      end if;  
    end if;
  end process update_PLL;

  wait_for_PLL_reset_done : process(PLL_reset, I2C_clk) -- Wait for 10 ms according to datasheet
  begin
    if PLL_reset = '1' then
      PLL_reset_done_loc   <= '0';
      PLL_reset_duration   <= (others => '0');
    elsif Rising_Edge(I2C_clk) then
      if PLL_reset_duration < x"186a00" then
        PLL_reset_duration <= PLL_reset_duration+1;
      else
        PLL_reset_done_loc <= '1';
      end if;
    end if;
  end process wait_for_PLL_reset_done;

-- For PLL, we have a standard I2C protocol.
-- For DAC, we have to send 3 bytes in a raw after the DAC address
  program_I2c : process(PLL_reset_done_loc, I2C_Access, I2C_clk, I2C_loc_busy)
  begin
    if PLL_reset_done_loc = '0' then
      I2C_state                      <= init_PLL;
      I2C_ena                        <= '0';
      PLL_busy                       <= '0';
      I2C_loc_busy_prev              <= '0';
      init_Reg_Number                <=  1;
      PLL_init_done                  <= '0';
      I2C_max_bytes                  <= 1;
      I2C_n_bytes                    <= 0;
    elsif Rising_Edge(I2C_clk) then
      case I2C_state is
      when init_PLL =>
        I2C_address_loc              <= PLL_address;
        PLL_busy                     <= '0';
        PLL_init                     <= '1';
        I2C_max_bytes                <= 1;
        I2C_n_bytes                  <= 0;
        PLL_R_Wb                     <= '0';                                  -- First cycle : write register data
        I2C_R_Wb_loc                 <= '0';                                  -- first write register address
        init_Reg_Number              <=  1;
        I2C_data_w                   <= std_logic_vector(PLL_Reg_numbers(1)); -- Register address
        I2C_state                    <= wait_for_busy_reg;                    -- Synchronize with I2C clock (slower)
        I2C_data_r                   <= (others => '0');
        I2C_ena                      <= '1';                                  -- Initiate the transaction (I2C master latch address and data)
        I2C_state                    <= wait_for_busy_reg;
      when idle =>
        PLL_init                     <= '0';
        PLL_init_done                <= '1';
        I2C_n_ack                    <= loc_I2C_n_ack;
        if I2C_loc_busy = '0' then
          PLL_busy                   <= '0';
        end if;
        I2C_ena                      <= '0';
        if I2C_Access = '1' then
          PLL_busy                   <= '1';                                  -- Stay busy during all the transaction
          I2C_address_loc            <= I2C_addresses(I2C_number);
          PLL_R_Wb                   <= I2C_R_Wb;
          I2C_state                  <= wait_for_busy_reg;                    -- Synchronize with I2C clock (slower)
          I2C_n_bytes                <= 0;
          if I2C_number > 0 then
            I2C_max_bytes            <= 2;
          else
            I2C_max_bytes            <= 1;
          end if;
          if I2C_number = 0 or I2C_R_Wb = '0' then
            I2C_R_Wb_loc             <= '0';                                  -- first write register address for PLL or command for DACs
          else
            I2C_R_Wb_loc             <= '1';                                  -- There is no register in the DACs, read data directly
            I2C_state                <= wait_for_busy_read;                   -- Synchronize with I2C clock (slower)
          end if;
          I2C_data_w                 <= I2C_Reg_number;                       -- Register address
          I2C_data_r                 <= (others => '0');
          I2C_ena                    <= '1';                                  -- Initiate the transaction (I2C master latch address and data)
        end if;
      when wait_for_busy_reg =>                                               -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                  <= write_reg_address;
        end if;
      when write_reg_address =>                                               -- Chip/reg addresses have been latched. Prepare next transaction during first write
        I2C_R_Wb_loc                 <= PLL_R_Wb;
        if PLL_init = '1' then
          I2C_data_w                 <= std_logic_vector(PLL_reg_contents(init_Reg_Number));
        elsif I2C_number = 0 then
          I2C_data_w                 <= I2C_Reg_data_in(7 downto 0);
        else
          I2C_data_w                 <= I2C_Reg_data_in(15 downto 8);
        end if;
        if I2C_loc_busy = '0' then                                            -- Chip/reg adress write is finished
          if PLL_R_Wb = '0' then
            I2C_state                <= wait_for_busy_write;
          else
            I2C_state                <= wait_for_busy_read;
          end if;
        end if;
      when wait_for_busy_read =>                                              -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                  <= read_reg_data;
        end if;
      when read_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                      <= '0';                                -- Deassert enable to stop transaction after this read
        else
          I2C_ena                      <= '1';                                -- Reassert enable to continue transaction after this read
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                -- byte read finished
          if I2C_n_bytes = 0 then
            I2C_data_r(15 downto 8)  <= I2C_loc_data_r;
          else
            I2C_data_r(7 downto 0)   <= I2C_loc_data_r;
          end if;
          if I2C_n_bytes = I2C_max_bytes-1 then
            I2C_state                <= idle;                                 -- End of transaction 1 or 2 bytes read
          end if;
          I2C_n_bytes                <= I2C_n_bytes +1;
        end if;
      when wait_for_busy_write =>                                             -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                  <= write_reg_data;
        end if;
      when write_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                    <= '0';                                -- Deassert enable to stop transaction after this write
        else
          I2C_ena                    <= '1';                                -- Reassert enable to continue transaction after this write
          I2C_data_w                 <= I2C_Reg_data_in(7 downto 0);
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                                            -- byte write finished
          I2C_n_bytes                <= I2C_n_bytes +1;
          if PLL_init = '1'and init_Reg_Number < n_PLL_init_registers then
            init_Reg_Number          <= init_Reg_Number+1;
            PLL_R_Wb                 <= '0';                                  -- Write next register
            I2C_R_Wb_loc             <= '0';                                  -- first write register address
            I2C_data_w               <= std_logic_vector(PLL_Reg_numbers(init_Reg_number+1));     -- Register address
            I2C_state                <= wait_for_busy_reg;                    -- Synchronize with I2C clock (slower)
            I2C_data_r               <= (others => '0');
            I2C_ena                  <= '1';                                  -- Initiate the transaction (I2C master latch address and data)
            I2C_n_bytes              <= 0;
          else
            if I2C_n_bytes = I2C_max_bytes-1 then
              I2C_state              <= idle;                               -- End of transaction and initialisation
            end if;
          end if;
        end if;
      end case;
      I2C_loc_busy_prev <= I2C_loc_busy;
    end if;
  end process program_I2C;
  
  I2C_scl_loc_del <= I2C_scl_loc when rising_edge(I2C_clk) else I2C_scl_loc_del;
  proc_I2C_spy : process (PLL_reset, I2C_clk, I2C_scl_loc, I2C_scl_loc_del, I2C_state) is
  begin
    if PLL_reset = '1' or I2C_state = wait_for_busy_reg then
      loc_I2C_n_ack   <= (others => '0');
    elsif rising_edge(I2C_clk) then
      if I2C_SCL_loc='1' and I2C_scl_loc_del='0' and I2C_slave_ack = '1' then
        loc_I2C_n_ack <= loc_I2C_n_ack+1;
      end if;

    end if; 
  end process proc_I2C_spy;

end architecture rtl;
