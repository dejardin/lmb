--state machine For I2C slave
---------------------------------------------------------------------------------
-- Company: CEA-Saclay
-- Engineer: M.D.
-- 
-- Create Date:    11:12:22 01/11/2023 
-- Design Name: 
-- Module Name:    i2c_slave
-- Project Name:  LMB
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.LMB_IO.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_slave is
port (
  clk               : in  std_logic;                           -- Clock
  reset             : in  std_logic;                           -- Reset from FE (ReSync line)
  scl_i             : in  std_logic;                           -- i2c clock line input
  sda_i             : in  std_logic;                           -- i2c data line input
  sda_o             : out std_logic;                           -- i2c data line output0
  mem               : inout DTU_reg_t(N_ADC downto 0);         -- Number of accessible registers in LiTE-DTU.
                                                               -- In fact, all LiTE-DTU (8) for a given ADC will have the same setting
  baseline          : in  UInt14_t(N_ADC downto 1)             -- On I2C reset, initiate LiTE-DTU baseline with almost correct value
);
end i2c_slave;

architecture rtl of i2c_slave is

constant CERN_I2C       : std_logic := '1';                    -- Don't reset register address on "stop" if we use "CERN" protocol

signal I2C_init_reg     : std_logic := '0';
signal I2C_start        : std_logic := '0';
signal I2C_stop         : std_logic := '1';                    -- Begin with I2C transaction stopped, so wait for "start" and not "restart"
signal SCL_high         : std_logic := '0';
signal SCL_low          : std_logic := '0';
signal SCL_del1         : std_logic := '0';                    -- previous value scl
signal SDA_del1         : std_logic := '0';                    -- previous value sda
signal SCL_del2         : std_logic := '0';                    -- previous value scl
signal SDA_del2         : std_logic := '0';                    -- previous value sda
signal ack              : std_logic := '0';                    -- acknowledge signal
signal addr             : std_logic := '1';                    -- adress receiving (i2c high level)
signal Device_addr      : std_logic_vector(6 downto 0);
signal Register_addr    : unsigned(7 downto 0);
signal Register_data    : std_logic_vector(7 downto 0);
signal RWb              : std_logic := '0';                    -- Read from slave (1) or Write to slave (0)
signal skip_next_SCL    : std_logic := '0';                    -- Skip 1 SCL falling edge (after start signal)
signal bit_number       : natural range 0 to 9 := 0;
signal ADC_number       : natural range 0 to 3 := 0;
signal sda_o_loc        : std_logic;
signal I2C_start_timeout: std_logic;
signal I2C_timeout      : std_logic;
signal I2C_timeout_del  : std_logic;
signal FE_reset_del     : std_logic;
signal timeout_counter  : unsigned(23 downto 0);

constant mem_def        : Byte_t(N_DTU_registers-1 downto 0) := (x"1F",x"CF",x"CC",x"CC",x"0C",x"00",
                                                                 x"80",x"88",x"32",x"3C",x"00",x"55",x"55",x"44",x"88",x"00",
                                                                 x"00",x"0F",x"FF",x"00",x"00",x"00",x"30",x"04",x"00",x"81");
type   I2C_state_t   is (I2C_idle, I2C_get_device_addr,   I2C_device_ack,
                                   I2C_get_register_addr, I2C_register_ack,
                                   I2C_read_data,         I2C_read_data_ack,
                                   I2C_write_data,        I2C_write_data_ack);
signal I2C_state     : I2C_state_t := I2C_idle;

begin

  sda_o        <= sda_o_loc;

proc_I2C_slave : process (clk, reset)
begin
  if reset = '1' then
    RWb                             <= '0';
    sda_o_loc                       <= '1';
    I2C_start                       <= '0';
    I2C_stop                        <= '1';
    I2C_state                       <= I2C_idle;
    I2C_timeout                     <= '0';
    I2C_init_reg                    <= '1';
    timeout_counter                 <= (others => '0');
  elsif rising_edge(clk) then
    if I2C_init_reg = '1' then
      for iADC in 1 to N_ADC loop
        mem(iADC)                   <= mem_def;
        if baseline(iADC)(13 downto 1) < 272 then
          mem(iADC)(5)              <= std_logic_vector(baseline(iADC)(8 downto 1)-16);
        end if;
      end loop;
      mem(0)                        <= mem_def;
      I2C_init_reg                  <= '0';                      -- Keep baseline at 0 for Temperature/Humidity LiTe-DTU
    end if;
-- Interupt signal decoding (start and stop) are put after the FSM treatment to override the FSM state if needed

-- SCL and SDA are asynchronous : Double latch to change time domain and avoid glitch or error decision
    SCL_del1                        <= SCL_i;
    SDA_del1                        <= SDA_i;
    SCL_del2                        <= SCL_del1;
    SDA_del2                        <= SDA_del1;
    SCL_high                        <= '0';
    if SCL_del2 ='0' and SCL_del1 = '1' then                     -- Detect SCL rising edge and put some margin
      SCL_high                      <= '1';
    end if;
    SCL_low                         <= '0';
    if SCL_del2 ='1' and SCL_del1 = '0' then                     -- Detect SCL falling edge and put some margin
      SCL_low                       <= '1';
    end if;
    if SCL_low ='1' then                                         -- release SDA bus anyway
      sda_o_loc                     <= '1';
    end if;

    case I2C_state is
    when I2C_idle => 
      if I2C_stop = '1' then
        if CERN_I2C = '0' then             -- in standard protocol, reset register address on stop signal
          Register_addr             <= (others => '0');
        end if;
        I2C_stop                    <= '0';
      end if;
      if I2C_start = '1' then
        I2C_state                   <= I2C_get_device_addr;
        bit_number                  <= 7;
        Device_addr                 <= (others => '0');
        I2C_start                   <= '0';
      end if;
    when I2C_get_device_addr =>
      if SCL_high = '1' then                                     -- Look at SDA value after the rising edge of SCL
        if bit_number > 0 then
          Device_addr(bit_number-1) <= SDA_i;                    -- Receive msb first
          bit_number                <= bit_number - 1;
        else
          RWb                       <= SDA_i;                    -- Last bit is RW direction of the transaction
          if unsigned(Device_addr(6 downto 3)) >= 1 and
             unsigned(Device_addr(6 downto 3)) <= N_ADC  then    -- Answer for LiTE-DTU numbers between 1 and 3 (In fact ADC number)
            ADC_number              <= to_integer(unsigned(Device_addr(4 downto 3)));
            I2C_state               <= I2C_device_ack;           -- Keep ack on bus for one clock
          else                                                   -- Device not addressed
            I2C_state               <= I2C_idle;
          end if;
        end if;
      end if;
    when I2C_device_ack =>
      if SCL_low = '1' then
        sda_o_loc                   <= '0';                      -- Send ACK
      end if;
      bit_number                    <= 7;
      if SCL_high = '1' then
        if RWb = '0' then                                        -- Write register address
          I2C_state                 <= I2C_get_register_addr;
        else                                                     -- Read data from slave at previously sent register address
          I2C_state                 <= I2C_read_data;
        end if;
      end if;
    when I2C_get_register_addr =>
      if SCL_high = '1' then
        Register_addr(bit_number)   <= SDA_i;                    -- Receive msb first
        if bit_number > 0 then
          bit_number                <= bit_number - 1;
        else
          I2C_state                 <= I2C_register_ack;
        end if;
      end if;
    when I2C_register_ack =>
      if Register_addr < N_DTU_registers then
        if SCL_low = '1' then
          sda_o_loc                 <= '0';                      -- Send ACK if register exists
        end if;
      else
        I2C_state                   <= I2C_idle;
      end if;
      bit_number                    <= 7;                        -- We will receive 8 bits at next transaction
      if SCL_high = '1' then                                     -- Keep ack for on SCL period
        I2C_state                   <= I2C_write_data;           -- Keep ack on bus for 1 clock
      end if;
    when I2C_write_data =>
      if SCL_high = '1' then
        register_data(bit_number)   <= SDA_i;                    -- Receive msb first
        if bit_number > 0 then
          bit_number                <= bit_number - 1;
        else
          I2C_state                 <= I2C_write_data_ack;
        end if;
      end if;
    when I2C_write_data_ack =>
      if SCL_low = '1' then
        mem(ADC_number)(to_integer(register_addr)) <= Register_data;
        sda_o_loc                   <= '0';                      -- Send ACK if register exists
      end if;
      bit_number                    <= 7;
      if SCL_high = '1' then
        if Register_addr <  N_DTU_registers-1 then
          Register_addr             <= Register_addr+1;          -- prepare for bulk transfer
          I2C_state                 <= I2C_write_data;           -- Wait for next transfer (could be interupted by "stop" signal
        else
          I2C_state                 <= I2C_idle;
        end if;
      end if;
    when I2C_read_data =>
      if SCL_low = '1' then                                      -- Present data when SCL is low
        sda_o_loc                   <= mem(ADC_number)(to_integer(register_addr))(bit_number);
      end if;
      if SCL_high = '1' then
        if bit_number > 0 then
          bit_number                <= bit_number - 1;
        else
          I2C_state                 <= I2C_read_data_ack;
        end if;
      end if;
    when I2C_read_data_ack =>
      if SCL_high = '1' then
        if SDA_i = '0' then                                      -- Do we have ack from master ?
          bit_number                <= 7;                        -- prepare for next transfer
          if Register_addr < N_DTU_registers-1 then
            Register_addr           <= Register_addr+1;          -- prepare for bulk transfer
            I2C_state               <= I2C_read_data;
          else
            I2C_state               <= I2C_idle;
          end if;
        else
          I2C_state                 <= I2C_idle;
        end if;
      end if;
    when others =>
      I2C_state                     <= I2C_idle;
    end case;

-- Override state of FSM in case of interupt :
    if I2C_start_timeout = '1' then
      if timeout_counter < x"F42400" then
        timeout_counter   <= timeout_counter +1;
      else
        I2C_timeout       <= '1';
        timeout_counter   <= (others => '0');
      end if;
      if I2C_timeout = '1' and timeout_counter > 10 then
        I2C_timeout       <= '0';
        I2C_start_timeout <= '0';
      end if;
    end if;

    I2C_timeout_del                 <= I2C_timeout;
    if I2C_timeout = '1' and I2C_timeout_del = '0' then
      I2C_stop                      <= '1';
      I2C_state                     <= I2C_idle;
    end if;

    if SCL_del2 = '1' and SCL_del1 = '1' and SDA_del2 = '1' and SDA_del1 = '0' then -- Start or restart condition
      I2C_start                     <= '1';
      I2C_start_timeout             <= '1';
      timeout_counter               <= (others => '0');
      I2C_state                     <= I2C_idle;
    end if;

    if SCL_del2 = '1' and SCL_del1 = '1' and SDA_del2 = '0' and SDA_del1 = '1' then -- Stop condition
      I2C_stop                      <= '1';
      I2C_state                     <= I2C_idle;
      I2C_start_timeout             <= '0';
      timeout_counter               <= (others => '0');
    end if;
  end if;

end process proc_I2C_slave;

END rtl;

