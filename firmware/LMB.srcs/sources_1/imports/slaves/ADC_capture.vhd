library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.LMB_IO.all;

entity ADC_Capture is
  generic(
   MAX_RAM_ADDRESS        : unsigned(ADDR_LENGTH-1 downto 0) := (others => '1')
  );
  port(
    trigger            : in  std_logic;
    DAQ_busy           : out std_logic;
    ipbus_clk          : in  std_logic;
    reset              : in  std_logic;
    ipbus_in           : in  ipb_wbus;
    ipbus_out          : out ipb_rbus;
    clk_counter        : in  unsigned(13 downto 0);
    clk_40             : in  std_logic;
    clk_sequence       : in  std_logic;
    clk_shift_reg      : in  std_logic;
    clk_memory         : in  std_logic;
    local_trigger      : out std_logic;
    self_trigger       : in  std_logic;
    self_trigger_mode  : in  std_logic;
    self_trigger_thres : in  unsigned(13 downto 0);
    self_trigger_mask  : in  std_logic_vector(N_ADC downto 1);
    ADC1_data_in       : in  std_logic_vector(127 downto 0);
    ADC1_data_valid    : in  std_logic;
    ADC2_data_in       : in  std_logic_vector(127 downto 0);
    ADC2_data_valid    : in  std_logic;
    ADC3_data_in       : in  std_logic_vector(127 downto 0);
    ADC3_data_valid    : in  std_logic
  );

end ADC_Capture;

architecture rtl of ADC_Capture is

component dist_mem_gen_0 is -- 4k x128 bit words = 32k ADC samples (25.6 us)
port (
  a    : in  std_logic_vector(ADDR_LENGTH-1 DOWNTO 0);
  d    : in  std_logic_vector(127 DOWNTO 0);
  dpra : in  std_logic_vector(ADDR_LENGTH-1 DOWNTO 0);
  clk  : in  std_logic;
  we   : in  std_logic;
  dpo  : out std_logic_vector(127 DOWNTO 0)
);
end component dist_mem_gen_0;

component blk_mem_128b IS
port (
  clka  : in  std_logic;
  wea   : in  std_logic_vector(0 DOWNTO 0);
  addra : in  std_logic_vector(15 DOWNTO 0);
  dina  : in  std_logic_vector(127 DOWNTO 0);
  clkb  : in  std_logic;
  addrb : in  std_logic_vector(15 DOWNTO 0);
  doutb : out std_logic_vector(127 DOWNTO 0)
);
end component blk_mem_128b;

component c_shift_ram_0 IS
port (
  d   : IN STD_LOGIC_VECTOR(127 downto 0);
  clk : IN STD_LOGIC;
  q   : OUT STD_LOGIC_VECTOR(127 downto 0)
);
end component c_shift_ram_0;

signal sel : integer;
signal ack : std_logic;

signal ipbus_sample_address     : std_logic_vector(15 downto 0) := x"0000";

signal ADC_data_delayed         : Word128_t(N_ADC downto 1);
signal ADC_ram_data_in          : Word128_t(N_ADC downto 1);
signal ADC_ram_data_out         : Word128_t(N_ADC downto 1);
signal ADC_data_in              : Word128_t(N_ADC downto 1);

signal write_address            : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- write address
signal read_address             : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- read address
--signal reset_read_address       : std_logic             := '0';
--signal reset_read_address_done  : std_logic             := '0';
signal capture_write            : std_logic := '0';
signal frame_length             : unsigned(ADDR_LENGTH-1 downto 0) := (0 => '1', others => '0');

signal capture_running          : std_logic := '0';  -- capturing data
signal capture_start            : std_logic := '0';  -- set capturing data to '1'
signal capture_pause            : std_logic := '0';  -- hold capturing data
signal capture_stop             : std_logic := '0';  -- set capturing data to '0'
  
--Strobes from the IPBUS clock domain to capture domain
signal capture_start_strobe     : std_logic := '0';
signal capture_start_del1       : std_logic := '0';
signal capture_start_del2       : std_logic := '0';
signal capture_stop_strobe      : std_logic := '0';
signal capture_stop_del1        : std_logic := '0';
signal capture_stop_del2        : std_logic := '0';
  
-- State type of the writing process
type   write_type is (write_start, write_idle, write_running);
signal write_state : write_type := write_start;

-- State type of the ipbus process
type   ipbus_rw_type is (ipbus_rw_start, ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_delay, ipbus_read, ipbus_ack, ipbus_finished);
signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_start;

type cycle_state is (header,ADC1_1, ADC1_2, ADC1_3, ADC1_4,
                            ADC2_1, ADC2_2, ADC2_3, ADC2_4,
                            ADC3_1, ADC3_2, ADC3_3, ADC3_4);
signal read_cycle : cycle_state := header;
type busy_type is (not_busy, stay_busy, busy);
signal busy_state : busy_type := not_busy;
  
signal channel_ram_data_read : std_logic    := '0';
signal ram_full              : std_logic    := '0';
signal trigger_del1          : std_logic    := '0';
signal trigger_del2          : std_logic    := '0';
signal nsample               : unsigned(ADDR_LENGTH-1 downto 0) := (others => '0');
signal free_ram              : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- Available ram
signal ram_read              : std_logic := '0';
signal ram_read_del1         : std_logic := '0';
signal ram_read_del2         : std_logic := '0';
signal ipbus_rdata_loc       : std_logic_vector(31 downto 0);

signal time_stamp            : unsigned(63 downto 0);
signal trig_width            : unsigned(3 downto 0);

begin

  ADC_data_in(1)          <= ADC1_data_in;
  ADC_data_in(2)          <= ADC2_data_in;
  ADC_data_in(3)          <= ADC3_data_in;
  gen_local_trigger : process (clk_sequence, self_trigger, reset) is
  variable trig_cond : std_logic_vector(N_ADC downto 1) := (others => '0');
  begin
    if reset ='1' then
      local_trigger        <= '0';
      trig_width           <= (others => '0');
    elsif rising_edge(clk_sequence) then
      if self_trigger = '1' and trig_width = 0 and write_state = write_idle and capture_running = '1' then -- Wait for the end of previous trigger has been processed
        for i in 1 to N_ADC loop
          if self_trigger_mask(i)='1' then
            if self_trigger_mode= '0' and
               (unsigned(ADC_data_in(i)(15  downto 2))   > self_trigger_thres or
                unsigned(ADC_data_in(i)(31  downto 18))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(47  downto 34))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(63  downto 50))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(79  downto 66))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(95  downto 82))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(111 downto 98))  > self_trigger_thres or
                unsigned(ADC_data_in(i)(127 downto 120)) > self_trigger_thres)  then
              trig_cond(i) := '1';
            elsif self_trigger_mode= '1' and
               (unsigned(ADC_data_in(i)(63  downto 50))  > unsigned(ADC_data_in(i)(15  downto 2)) + self_trigger_thres or
                unsigned(ADC_data_in(i)(127 downto 120)) > unsigned(ADC_data_in(i)(79  downto 66))+ self_trigger_thres) then
              trig_cond(i) := '1';
            else
              trig_cond(i) := '0';
            end if;
          else
            trig_cond(i)   := '0';
          end if;
        end loop;
        if (or trig_cond) = '1' then
          trig_width <= (others => '1');
          local_trigger    <= '1';
        else
          local_trigger    <= '0';
        end if;
      elsif trig_width > 0 then
        trig_width         <= trig_width-1;
      else
        local_trigger      <= '0';
      end if;
    end if;
  end process gen_local_trigger;

-- We receive 128 bit words synchronous with the 160 MHz clock
  inst_shift_ram : for iADC in 1 to N_ADC generate
    inst_ADC_shift_ram : c_shift_ram_0
    port map (
      d   => ADC_data_in(iADC),
      clk => clk_shift_reg,
      q   => ADC_data_delayed(iADC)
    );
  end generate;

  inst_memory_blocks : for iADC in 1 to N_ADC generate
--    inst_ADC1_memory_block : dist_mem_gen_0
--    port map (
--      clk   => clka_ram,
--      we    => capture_write,
--      a     => std_logic_vector(write_address),
--      d     => ADC_ram_data_in(iADC),
--     dpra  => std_logic_vector(read_address),
--      dpo   => ADC_ram_data_out(iADC)
--    );
    inst_ADC1_memory_block : blk_mem_128b
    port map (
      clka   => clk_memory,
      wea(0) => capture_write,
      addra  => std_logic_vector(write_address),
      dina   => ADC_ram_data_in(iADC),
      clkb   => clk_memory,
      addrb  => std_logic_vector(read_address),
      doutb  => ADC_ram_data_out(iADC)
    );
  end generate;

  capture_data : process (clk_sequence, reset) is
  variable delay : unsigned(7 downto 0) := (others => '0');
  begin  -- process capture_counter
    if reset = '1' then
      DAQ_busy                                <= '0';
      capture_write                           <= '0';
      capture_running                         <= '0';
      write_address                           <= MAX_RAM_ADDRESS;
      free_ram                                <= MAX_RAM_ADDRESS;
      ram_full                                <= '0';
      delay                                   := (others => '0');
      write_state                             <= write_start;
    elsif rising_edge(clk_sequence) then
      trigger_del1                             <= trigger;
      trigger_del2                             <= trigger_del1;
      ram_read_del1                            <= ram_read;
      ram_read_del2                            <= ram_read_del1;
      capture_start_del1                       <= capture_start_strobe;
      capture_start_del2                       <= capture_start_del1;
      capture_start                            <= capture_start_del1 and not capture_start_del2;
      capture_stop_del1                        <= capture_stop_strobe;
      capture_stop_del2                        <= capture_stop_del1;
      capture_stop                             <= capture_stop_del1 and not capture_stop_del2;
--      if reset_read_address_done = '1' then
--        reset_read_address       <= '0';
--      end if;

      capture_write                             <= '0';
      case write_state is
      when write_start =>
        DAQ_busy                                <= '0';
        nsample                                 <= (others => '0');
        if delay /= x"FF" then
          delay                                 := delay+1;
        else
          write_state                            <= write_idle;
        end if;
      when write_idle =>
        DAQ_busy                                 <= '0';
        if capture_start = '1' then   --start control
          capture_running                        <= '1';
          write_address                          <= MAX_RAM_ADDRESS;
          free_ram                               <= MAX_RAM_ADDRESS;
          ram_full                               <= '0';
        end if;
        if capture_stop = '1' then
          capture_running                        <= '0';
        end if;
        if free_ram < frame_length  then
          ram_full                               <= '1';
        else
          ram_full                               <= '0';
        end if;
        if capture_running = '1' and trigger_del1 = '1' and trigger_del2 = '0' then
          write_address                          <= MAX_RAM_ADDRESS;
          free_ram                               <= MAX_RAM_ADDRESS;
          ram_full                               <= '0';
          nsample                                <= (others=> '0');
--          reset_read_address     <= '1';
          write_state                            <= write_running;
        end if;
        if ram_read_del1 = '1' and ram_read_del2 = '0' and free_ram /= MAX_RAM_ADDRESS then
          free_ram                               <= free_ram + 1;
        end if;
      when write_running =>
        DAQ_busy                                 <= '1';
        capture_write                            <= '1';
        if nsample = 0 then
          for iADC in 1 to N_ADC loop
            ADC_ram_data_in(iADC)(127 downto 80) <= (others => '1');
            ADC_ram_data_in(iADC)(79 downto 64)  <= "00"&std_logic_vector(clk_counter);
            ADC_ram_data_in(iADC)(63 downto 0)   <= std_logic_vector(time_stamp);
          end loop;
        else
          for iADC in 1 to N_ADC loop
            ADC_ram_data_in(iADC)(127 downto 0)  <= ADC_data_delayed(iADC);
          end loop;
        end if;
        if nsample = (frame_length-1) then
          write_state                            <= write_idle;
        end if;
        nsample                                  <= nsample + 1;
        if write_address = MAX_RAM_ADDRESS then
          write_address                          <= (others => '0');
        else
          write_address                          <= write_address+1;
        end if;
        if ram_read_del1 = '0' or ram_read_del2 = '1' then
          free_ram                               <= free_ram - 1;
        end if;

        if capture_stop = '1' then
          capture_running                        <= '0';
          write_state                            <= write_idle;
        end if;
      end case;
    end if;
  end process capture_data;

-- Running clk counter to get a timestamp
  gen_timestamp : process(reset, capture_start, clk_sequence)
  begin
    if reset = '1' or capture_start = '1' then
      time_stamp                        <= (others => '0');
    elsif rising_edge(clk_sequence) then
      time_stamp                        <= time_stamp+1;
    end if;
  end process gen_timestamp;
  
-- Process ipbus read/writes
  ipbus_out.ipb_rdata                                 <= ipbus_rdata_loc;
  ipbus_data : process(reset, ipbus_clk)
  begin
    if reset = '1' then
      read_address                                    <= MAX_RAM_ADDRESS;
      read_cycle                                      <= header;
      ipbus_rw_state                                  <= ipbus_rw_start;
      channel_ram_data_read                           <= '0';
      ram_read                                        <= '0';
      ack                                             <= '0';
      frame_length                                    <= (0 => '1', others => '0');
    elsif rising_edge(ipbus_clk) then
      capture_start_strobe                            <= '0';
      capture_stop_strobe                             <= '0';
      channel_ram_data_read                           <= '0';
      ram_read                                        <= '0';
      ack                                             <= '0';
--      reset_read_address_done    <= '0';
--      if reset_read_address = '1' then
--        read_address             <= MAX_RAM_ADDRESS;
--        reset_read_address_done  <= '1';
--      end if;
      ipbus_state_machine : case ipbus_rw_state is
      when ipbus_rw_start =>
        read_address                                  <= MAX_RAM_ADDRESS;
        read_cycle                                    <= header;
        ipbus_rw_state                                <= ipbus_rw_idle;
      when ipbus_rw_idle =>
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state                            <= ipbus_write_start;
          else
            ipbus_rw_state                            <= ipbus_read_start;
--            read_cycle                               <= header;
          end if;
        end if;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(15 downto 0) is
          when x"0000" => --control registers
            capture_start_strobe                      <= ipbus_in.ipb_wdata(0);
            capture_stop_strobe                       <= ipbus_in.ipb_wdata(1);
            frame_length                              <= unsigned(ipbus_in.ipb_wdata(ADDR_LENGTH+15 downto 16));
          when x"0001" => --Start read address
            read_address                              <= unsigned(ipbus_in.ipb_wdata(ADDR_LENGTH-1 downto 0));
          when others => null;
        end case;
        ipbus_rw_state                                <= ipbus_ack;
      when ipbus_read_start =>
        ipbus_rdata_loc                               <= (others => '0');
        decode : case ipbus_in.ipb_addr(15) is
        when '0' =>          --control registers
          case ipbus_in.ipb_addr(1 downto 0) is
          when "00" =>
            ipbus_rdata_loc(1 downto 0)               <= capture_stop_strobe & capture_start_strobe;
            ipbus_rdata_loc(ADDR_LENGTH+15 downto 16) <= std_logic_vector(frame_length);
--            ipbus_rdata_loc             <= "0000"&std_logic_vector(frame_length) & x"000" & "00" & capture_stop_strobe & capture_start_strobe;
          when "01" =>
            ipbus_rdata_loc(ADDR_LENGTH-1 downto 0)   <= std_logic_vector(read_address);
            ipbus_rdata_loc(ADDR_LENGTH+15 downto 16) <= std_logic_vector(write_address);
--            ipbus_rdata_loc             <= "0000"&std_logic_vector(write_address) & "0000"&std_logic_vector(read_address);
          when "10" =>
            ipbus_rdata_loc(ADDR_LENGTH-1 downto 0)   <= std_logic_vector(free_ram);
          when others => null;
          end case;
          ipbus_rw_state <= ipbus_ack;
        when '1' => -- Captured data
          if (read_address = write_address and read_cycle = header) or
             (free_ram = MAX_RAM_ADDRESS) then
            ipbus_rdata_loc                           <= x"DEADBEEF";
            ipbus_rw_state                            <= ipbus_ack;
          else
            if read_cycle = header then
              if read_address = MAX_RAM_ADDRESS then
                read_address                         <= (others => '0');
              else
                read_address                          <= read_address + 1;
              end if;
            end if;
            ipbus_rw_state                            <= ipbus_read;
          end if;          
        when others =>
          ipbus_rdata_loc                             <= x"DEADBEEF";
          ipbus_rw_state                              <= ipbus_ack;
        end case decode;
      when ipbus_read =>
        dispatch : case read_cycle is
        when header =>
          ipbus_rdata_loc(ADDR_LENGTH-1 downto 0)     <= std_logic_vector(read_address);
          read_cycle                                  <= ADC1_1;
        when ADC1_1 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(1)(31 downto 0);
          read_cycle                                  <= ADC1_2;
        when ADC1_2 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(1)(63 downto 32);
          read_cycle                                  <= ADC1_3;
        when ADC1_3 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(1)(95 downto 64);
          read_cycle                                  <= ADC1_4;
        when ADC1_4 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(1)(127 downto 96);
          read_cycle                                  <= ADC2_1;
        when ADC2_1 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(2)(31 downto 0);
          read_cycle                                  <= ADC2_2;
        when ADC2_2 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(2)(63 downto 32);
          read_cycle                                  <= ADC2_3;
        when ADC2_3 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(2)(95 downto 64);
          read_cycle                                  <= ADC2_4;
        when ADC2_4 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(2)(127 downto 96);
          read_cycle                                  <= ADC3_1;
        when ADC3_1 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(3)(31 downto 0);
          read_cycle                                  <= ADC3_2;
        when ADC3_2 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(3)(63 downto 32);
          read_cycle                                  <= ADC3_3;
        when ADC3_3 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(3)(95 downto 64);
          read_cycle                                  <= ADC3_4;
        when ADC3_4 =>
          ipbus_rdata_loc(31 downto 0)                <= ADC_ram_data_out(3)(127 downto 96);
          read_cycle                                  <= header;
          ram_read                                    <= '1';
        when others =>
          read_cycle <= header;
        end case dispatch;
        ipbus_rw_state                                <= ipbus_ack;
      when ipbus_ack =>
        if capture_start_strobe = '1' then -- Reset read address when we restart capture
          read_address                                <= MAX_RAM_ADDRESS;
          read_cycle                                  <= header;
        end if;
        ack                                           <= '1';
        ipbus_rw_state                                <= ipbus_finished;
      when ipbus_finished =>
        ack                                           <= '0';
        ipbus_rw_state                                <= ipbus_rw_idle;
      when others =>
        ack                                           <= '0';
        ipbus_rw_state                                <= ipbus_rw_idle;
      end case ipbus_state_machine;
    end if;
  end process ipbus_data;



  ipbus_out.ipb_ack                                   <= ack;
  ipbus_out.ipb_err                                   <= '0';

end rtl;