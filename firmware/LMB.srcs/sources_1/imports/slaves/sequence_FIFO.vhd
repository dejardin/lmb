library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.LMB_IO.all;

entity sequence_FIFO is
  generic(
   MAX_FIFO_ADDRESS        : unsigned(FIFO_LENGTH-1 downto 0) := (others => '1')
  );
  port(
    ipbus_clk         : in  std_logic;
    reset             : in  std_logic;
    ipbus_in          : in  ipb_wbus;
    ipbus_out         : out ipb_rbus;
    clk_160           : in  std_logic;
    WTE               : in  std_logic;
    next_sequence_pos : out std_logic_vector(31 downto 0);
    next_lmr          : out std_logic              -- Signal to tell optical switch to change monitorng region
  );

end sequence_FIFO;

architecture rtl of sequence_FIFO is

component FIFO_mem_gen IS -- 1k x 32bit words
port (
    a    : in  std_logic_vector(9 DOWNTO 0);
    d    : in  std_logic_vector(31 DOWNTO 0);
    dpra : in  std_logic_vector(9 DOWNTO 0);
    clk  : in  std_logic;
    we   : in  std_logic;
    dpo  : out std_logic_vector(31 DOWNTO 0)
  );
end component FIFO_mem_gen;


signal ack : std_logic;

signal write_address            : unsigned(FIFO_LENGTH-1 downto 0) := (others => '0');  -- write address
signal read_address_IPbus       : unsigned(FIFO_LENGTH-1 downto 0) := (others => '0');  -- read address for IPbus access
signal read_address_local       : unsigned(FIFO_LENGTH-1 downto 0) := (others => '0');  -- read address for monitoring sequence management
signal read_address             : unsigned(FIFO_LENGTH-1 downto 0) := (others => '0');  -- read address
signal sequence_length          : unsigned(FIFO_LENGTH-1 downto 0) := (others => '0');  -- length of monitoring sequence
  
signal FIFO_read                : std_logic := '0';
signal FIFO_write               : std_logic := '0';
signal FIFO_data_in             : std_logic_vector(31 downto 0);
signal FIFO_data_out            : std_logic_vector(31 downto 0);
signal next_sequence_pos_loc    : std_logic_vector(31 downto 0);
signal prev_sequence_pos        : std_logic_vector(31 downto 0);
signal next_lmr_duration        : unsigned(7 downto 0);
signal next_lmr_loc             : std_logic;

signal WTE_del                  : std_logic := '0';
signal WTE_counter              : unsigned(9 downto 0);
signal next_FIFO_pos            : std_logic := '0';
signal next_FIFO_pos_del        : std_logic := '0';
signal next_FIFO_pos_ack        : std_logic := '0';
-- State type of the ipbus process
type   ipbus_rw_type is (ipbus_rw_start, ipbus_rw_idle, ipbus_write, ipbus_read, local_read, ipbus_ack, ipbus_finished);
signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_start;

type write_state_t is (write_data, increment_Waddr);
signal write_state : write_state_t := write_data;
type read_state_t is (present_Raddr, read_data, increment_Raddr);
signal read_state : read_state_t := present_Raddr;

begin

-- We receive 32 bit words synchronous with the 160 MHz clock
-- FIFO content : b31..24 = monitoring region, b23..16 : event type, B15..0 : Nb of events
-- btw we need only 7 bits fr the monitoring region, 4 bits for trigger type and 10 bits for nb of events
  inst_SEQ_memory_block : FIFO_mem_gen
  port map (
    clk   => clk_160,
    we    => FIFO_write,
    a     => std_logic_vector(write_address),
    d     => FIFO_data_in,
    dpra  => std_logic_vector(read_address),
    dpo   => FIFO_data_out
  );

-- count WTEs and ask for next FIFO position when needed
  next_lmr                  <= next_lmr_loc;
  count_WTE : process(reset, clk_160, WTE)
  begin
    if reset = '1' then
      WTE_counter           <= (others => '0');
      next_FIFO_pos         <= '0';
      next_lmr_loc          <= '0';
      next_lmr_duration     <= (others => '0');
    elsif rising_edge(clk_160) then
      WTE_del               <= WTE;
      prev_sequence_pos     <= next_sequence_pos_loc;
      if next_sequence_pos_loc(31 downto 24) /= prev_sequence_pos(31 downto 24) then
        next_lmr_loc        <= '1';
        next_lmr_duration   <= (others => '0');
      end if;
      if next_lmr_loc = '1' then
        if next_lmr_duration < x"0f" then
          next_lmr_duration <= next_lmr_duration + 1;
        else
          next_lmr_loc      <= '0';
        end if;
      end if;
      if WTE = '1' and WTE_del = '0' then
        if WTE_counter < unsigned(next_sequence_pos_loc(9 downto 0))-1 then
          WTE_counter   <= WTE_counter+1;
        else
          WTE_counter   <= (others => '0');
          next_FIFO_pos <= '1';
        end if;
      end if;
      if next_FIFO_pos_ack = '1' then
        next_FIFO_pos   <= '0';
      end if;
    end if;
  end process count_WTE;

-- Process ipbus read/writes
  next_sequence_pos                                     <= next_sequence_pos_loc;
  FIFO_access : process(reset, ipbus_clk)
  begin
    if reset = '1' then
      read_address                                      <= MAX_FIFO_ADDRESS;
      ipbus_rw_state                                    <= ipbus_rw_start;
      FIFO_read                                         <= '0';
      ack                                               <= '0';
    elsif rising_edge(ipbus_clk) then
      ack                                               <= '0';
      FIFO_write                                        <= '0';
      next_FIFO_pos_ack                                 <= '0';
      next_FIFO_pos_del                                 <= next_FIFO_pos;
      ipbus_state_machine : case ipbus_rw_state is
      when ipbus_rw_start =>
        read_address_IPbus                              <= (others => '0'); -- Read address for IPbus process
        read_address_local                              <= (others => '0'); -- Read address for local process (monitoring sequence driving)
        read_address                                    <= (others => '0');
        write_address                                   <= (others => '0');
        sequence_length                                 <= (others => '0');
        next_sequence_pos_loc                           <= (others => '0');
        ipbus_rw_state                                  <= ipbus_rw_idle;
        write_state                                     <= write_data;
        read_state                                      <= present_Raddr;
      when ipbus_rw_idle =>
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state                              <= ipbus_write;
          else
            read_address                                <= read_address_IPbus;
            ipbus_rw_state                              <= ipbus_read;
          end if;
        elsif next_FIFO_pos = '1' and next_FIFO_pos_del = '0' then
          next_FIFO_pos_ack                             <= '1';
          if read_address_local < sequence_length-1 then
            read_address                                <= read_address_local+1;
            read_address_local                          <= read_address_local+1;
          else
            read_address                                <= (others => '0');
            read_address_local                          <= (others => '0');
          end if;
          ipbus_rw_state                                <= local_read;
        end if;
      when local_read =>
        next_sequence_pos_loc                           <= FIFO_data_out(31 downto 0);
        ipbus_rw_state                                  <= ipbus_rw_idle;
      when ipbus_write =>
        case ipbus_in.ipb_addr(15 downto 0) is
        when x"0000" =>           -- FIFO length
          sequence_length                               <= unsigned(ipbus_in.ipb_wdata(FIFO_LENGTH-1 downto 0));
          ipbus_rw_state                                <= ipbus_ack;
        when x"0001" =>           --Starting read and write address
          read_address_IPbus                            <= unsigned(ipbus_in.ipb_wdata(FIFO_LENGTH-1  downto 0));
          write_address                                 <= unsigned(ipbus_in.ipb_wdata(FIFO_LENGTH+15 downto 16));
          ipbus_rw_state                                <= ipbus_ack;
        when x"0002" =>           -- Starting position of monitoring sequence in FIFO
          read_address_local                            <= unsigned(ipbus_in.ipb_wdata(FIFO_LENGTH-1 downto 0));
          ipbus_rw_state                                <= ipbus_ack;
        when x"0003" =>           -- Fill FIFO content
          case write_state is
          when write_data =>
            FIFO_data_in                                <= ipbus_in.ipb_wdata;
            FIFO_write                                  <= '1';
            write_state                                 <= increment_Waddr;
          when increment_Waddr => 
            if write_address < sequence_length-1 then
              write_address                             <= write_address + 1;
            else
              write_address                             <= (others => '0');
            end if;
            write_state                                 <= write_data;
            ipbus_rw_state                              <= ipbus_ack;
          when others =>
            ipbus_rw_state                              <= ipbus_ack;
        end case;
        when others =>
          ipbus_rw_state                                <= ipbus_ack;
        end case;
      when ipbus_read =>
        case ipbus_in.ipb_addr(15 downto 0) is
        when x"0000" =>       -- FIFO length
          ipbus_out.ipb_rdata                           <= (others => '0');
          ipbus_out.ipb_rdata(FIFO_length-1 downto 0)   <= std_logic_vector(sequence_length);
          ipbus_rw_state                                <= ipbus_ack;
        when x"0001" =>       -- Current read and write address
          ipbus_out.ipb_rdata                             <= (others => '0');
          ipbus_out.ipb_rdata(FIFO_length-1 downto 0)   <= std_logic_vector(read_address_IPbus);
          ipbus_out.ipb_rdata(FIFO_length+15 downto 16) <= std_logic_vector(write_address);
          ipbus_rw_state                                <= ipbus_ack;
        when x"0002" =>       -- Running position
          ipbus_out.ipb_rdata                           <= (others => '0');
          ipbus_out.ipb_rdata(31 downto 16)             <= next_sequence_pos_loc(31 downto 16);
          ipbus_out.ipb_rdata(9 downto 0)               <= std_logic_vector(WTE_counter);
          ipbus_rw_state                                <= ipbus_ack;
        when x"0003" =>       -- Read back FIFO content
          case read_state is
          when present_Raddr =>
            read_address                                <= read_address_IPbus;
            read_state                                  <= read_data;
          when read_data => 
            ipbus_out.ipb_rdata                         <= FIFO_data_out;
            read_state                                  <= increment_Raddr;
          when increment_Raddr => 
            if read_address_IPbus < sequence_length-1 then
              read_address_IPbus                        <= read_address_IPbus + 1;
            else
              read_address_IPbus                        <= (others => '0');
            end if;
            read_state                                  <= present_Raddr;
            ipbus_rw_state                              <= ipbus_ack;
          when others =>
            ipbus_rw_state                              <= ipbus_ack;
          end case;
        when others => null;
          ipbus_out.ipb_rdata                           <= x"DEADBEEF";
          ipbus_rw_state                                <= ipbus_ack;
        end case;
      when ipbus_ack =>
        ack                                             <= '1';
        ipbus_rw_state                                  <= ipbus_finished;
      when ipbus_finished =>
        ack                                             <= '0';
        ipbus_rw_state                                  <= ipbus_rw_idle;
      when others =>
        ack                                             <= '0';
        ipbus_rw_state                                  <= ipbus_rw_idle;
      end case ipbus_state_machine;
    end if;
  end process FIFO_access;

  ipbus_out.ipb_ack                                     <= ack;
  ipbus_out.ipb_err                                     <= '0';

end rtl;
