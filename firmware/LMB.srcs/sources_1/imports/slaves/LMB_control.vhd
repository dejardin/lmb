-- package for LMB control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.LMB_IO.all;

entity LMB_ctrlreg is
  port(
    ipbus_clk           : in  std_logic;
    reset               : in  std_logic;
    ipbus_in            : in  ipb_wbus;
    ipbus_out           : out ipb_rbus;
    LMB_monitor         : in  LMB_Monitor_t;
    LMB_control         : out LMB_Control_t;
    ADC_monitor         : in  ADC_Monitor_t;
    ADC_control         : out ADC_Control_t;

    ADC_DCDC_Temp       : in  std_logic;
    ADC_DCDC_Temp_ref   : in  std_logic;
    FPGA_DCDC_Temp1     : in  std_logic;
    FPGA_DCDC_Temp1_ref : in  std_logic;
    FPGA_DCDC_Temp2     : in  std_logic;
    FPGA_DCDC_Temp2_ref : in  std_logic

  );
end LMB_ctrlreg;

architecture rtl of LMB_ctrlreg is

  signal ack : std_logic := '0';

    --* State type of the ipbus process
  type   ipbus_rw_type is (ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_ack, ipbus_finished);
  signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_idle;

  type   XADC_status_type is (XADC_idle, XADC_read, XADC_write);
  signal XADC_status             : XADC_status_type := XADC_idle;

  signal XADC_control            : XADC_control_t := default_XADC_control;
  signal XADC_monitor            : XADC_monitor_t := default_XADC_monitor;
  signal loc_XADC_data           : std_logic_vector(15 downto 0) := (others => '0');
  signal reading                 : std_logic_vector(15 downto 0) := (others => '0');
  signal muxaddr                 : std_logic_vector( 5 downto 0) := (others => '0');
  signal channel                 : std_logic_vector( 5 downto 0) := (others => '0');
  signal vauxn                   : std_logic_vector(15 downto 0) := (others => '0');
  signal vauxp                   : std_logic_vector(15 downto 0) := (others => '0');
  signal wait_for_XADC_ready     : std_logic := '0';
  signal wait_for_DTU_ready      : std_logic := '0';
  signal wait_for_I2C_ready      : std_logic := '0';
  signal wait_for_SPI_ready      : std_logic := '0';
  signal wait_for_ADC1_AXI_ready : std_logic := '0';
  signal wait_for_ADC2_AXI_ready : std_logic := '0';
  signal wait_for_ADC3_AXI_ready : std_logic := '0';
  signal total_seq               : unsigned(31 downto 0) := (others => '0');
  signal total_conv              : unsigned(31 downto 0) := (others => '0');
  signal total_access            : unsigned(31 downto 0) := (others => '0');
  signal XADC_eos                : std_logic := '0';
  signal XADC_eos_del            : std_logic := '0';
  signal XADC_eoc                : std_logic := '0';
  signal XADC_eoc_del            : std_logic := '0';
  signal XADC_ready_del          : std_logic := '0';
  signal XADC_clk                : std_logic := '0';

begin
  reg: process (reset, ipbus_clk) is
  begin  -- process reg
    if reset = '1' then
      LMB_control                          <= DEFAULT_LMB_Control;
      ADC_control                          <= DEFAULT_ADC_Control;
      ipbus_rw_state                       <= ipbus_rw_idle;
      ipbus_out.ipb_rdata                  <= (others => '0'); --zero the response before the actual case response
      wait_for_XADC_ready                  <= '0';
      wait_for_DTU_ready                   <= '0';
    elsif rising_edge(ipbus_clk) then
      LMB_control.trigger_soft             <= '0';
      LMB_control.extra_trigger            <= '0';
      LMB_control.clock_reset              <= '0';
      LMB_control.LMB_reset                <= '0';
      XADC_control.XADC_access             <= '0';
      ADC_control.ADC_Start_ReSync         <= '0';
      ADC_control.I2C_reset                <= '0';
      ADC_control.PLL_reset                <= '0';
      ADC_control.PLL_update_en            <= '0';
      ADC_control.ADC_reset                <= '0';
      LMB_control.IO_reset                 <= '0';
      LMB_control.JESD_sys_reset           <= '0';
      LMB_control.AXI_reset                <= '0';
      LMB_control.trigger_in_clear         <= '0';
      ADC_control.I2C_PLL_access           <= '0';
      ADC_control.SPI_ADC_access           <= '0';
      LMB_control.ADC1_AXI_access          <= '0';
      LMB_control.ADC2_AXI_access          <= '0';
      LMB_control.ADC3_AXI_access          <= '0';
      LMB_control.update_delay_phase       <= (others => '0');
      LMB_control.TH_access                <= '0';

      case ipbus_rw_state is
      when ipbus_rw_idle =>
        ack <= '0';
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state                  <= ipbus_write_start;
          else
            ipbus_rw_state                  <= ipbus_read_start;
          end if;
        end if;
      when ipbus_read_start =>
        ipbus_out.ipb_rdata                   <= (others => '0'); --zero the response before the actual case response
        case ipbus_in.ipb_addr(7 downto 0) is
        when x"00" =>                                                                   -- FW_VER
          ipbus_out.ipb_rdata                 <= LMB_monitor.firmware_ver;
        when x"01" =>                                                                   -- BRD_SN
          ipbus_out.ipb_rdata(3 downto 0)     <= LMB_monitor.board_sn(3 downto 0);
        when x"02" =>                                                                   -- ACTIONS  
          ipbus_out.ipb_rdata(11 downto 8)    <= LMB_monitor.trigger_in(3 downto 0);
          LMB_control.trigger_in_clear        <= '1';
        when x"03" =>                                                                   -- SETTINGS
          ipbus_out.ipb_rdata(7 downto 0)     <= LMB_monitor.trigger_mask;
          ipbus_out.ipb_rdata(8)              <= LMB_monitor.LED_on;
          ipbus_out.ipb_rdata(9)              <= LMB_monitor.gen_BC0;
          ipbus_out.ipb_rdata(10)             <= LMB_monitor.gen_WTE;
          ipbus_out.ipb_rdata(11)             <= LMB_monitor.self_trigger;
          ipbus_out.ipb_rdata(14 downto 12)   <= LMB_monitor.self_trigger_mask;
          ipbus_out.ipb_rdata(15)             <= LMB_monitor.self_trigger_mode;
          ipbus_out.ipb_rdata(29 downto 16)   <= std_logic_vector(LMB_monitor.WTE_pos);
          ipbus_out.ipb_rdata(30)             <= LMB_monitor.force_local_WTE_pos;
          ipbus_out.ipb_rdata(31)             <= LMB_monitor.delay_scan;
        when x"04" =>                                                                   -- TRIGGER_0
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(0));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(0));
        when x"05" =>                                                                   -- TRIGGER_1
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(1));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(1));
        when x"06" =>                                                                   -- TRIGGER_2
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(2));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(2));
        when x"07" =>                                                                   -- TRIGGER_3
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(3));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(3));
        when x"08" =>                                                                   -- TRIGGER_4
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(4));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(4));
        when x"09" =>                                                                   -- TRIGGER_5
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(5));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(5));
        when x"0a" =>                                                                   -- TRIGGER_6
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(6));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(6));
        when x"0b" =>                                                                   -- TRIGGER_7
          ipbus_out.ipb_rdata(24 downto 0)    <= std_logic_vector(LMB_monitor.trigger_delay(7));
          ipbus_out.ipb_rdata(31 downto 25)   <= std_logic_vector(LMB_monitor.trigger_width(7));
        when x"0c" => -- XADC register                                                  -- DRP_XADC
          ipbus_out.ipb_rdata(23 downto 16)   <= XADC_monitor.XADC_addr; 
          ipbus_out.ipb_rdata(15 downto 0)    <= XADC_monitor.XADC_data;
        when x"0d" => -- Sequence counter (32 bits)                                     -- CONV_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_conv);
        when x"0e" => -- Sequence counter (32 bits)                                     -- SEQ_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_seq);
        when x"0f" => -- Sequence counter (32 bits)                                     -- ACCESS_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_access);
        when x"10" =>                                                                   -- PLL_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= ADC_monitor.I2C_PLL_data;
          ipbus_out.ipb_rdata(23 downto 16)   <= ADC_monitor.I2C_PLL_reg;
          ipbus_out.ipb_rdata(30 downto 24)   <= ADC_monitor.I2C_PLL_address;
          ipbus_out.ipb_rdata(31)             <= ADC_monitor.I2C_PLL_busy;
        when x"11" => -- I2C spy acknowledge byte 6                                     -- PLL_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(ADC_monitor.I2C_PLL_n_ack);
        when x"12" =>                                                                   -- ADC_CTRL
          ipbus_out.ipb_rdata(7 downto 0)     <= ADC_monitor.SPI_ADC_data;
          ipbus_out.ipb_rdata(22 downto 8)    <= ADC_monitor.SPI_ADC_reg;
          ipbus_out.ipb_rdata(25 downto 24)   <= std_logic_vector(to_unsigned(ADC_monitor.SPI_ADC_number,2));
        when x"13" =>                                                                   -- PLL_CONF
          ipbus_out.ipb_rdata(7 downto 0)     <= ADC_monitor.PLL_update_val;
          ipbus_out.ipb_rdata(23 downto 16)   <= ADC_monitor.PLL_update_reg;
        when x"14" =>                                                                   -- DAQ_DELAY
          ipbus_out.ipb_rdata(31 downto 16)   <= std_logic_vector(LMB_monitor.SW_delay);
          ipbus_out.ipb_rdata(15 downto 0)    <= std_logic_vector(LMB_monitor.HW_delay);
        when x"15" =>                                                                   -- JESD_RST_DELAY
          ipbus_out.ipb_rdata                 <= std_logic_vector(LMB_monitor.jesd_ADC_reset_delay);
        when x"18" =>                                                                   -- DTU_SYNC_PATTERN
          ipbus_out.ipb_rdata                 <= ADC_monitor.DTU_Sync_pattern;
        when x"19" => -- LiTE-DTU Resync idle pattern                                   -- DTU_IDLE_PATTERN
          ipbus_out.ipb_rdata(7 downto 0)     <= ADC_monitor.ADC_ReSync_idle;
          ipbus_out.ipb_rdata(13 downto 9)    <= ADC_monitor.ADC_PLL_lock;
        when x"1a" =>                                                                   -- ADC1_BASELINE
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(ADC_monitor.ADC_baseline(1));
        when x"1b" =>                                                                   -- ADC2_BASELINE
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(ADC_monitor.ADC_baseline(2));
        when x"1c" =>                                                                   -- ADC3_BASELINE
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(ADC_monitor.ADC_baseline(3));
        when x"20" =>                                                                   -- AXI1_DATA
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC1_AXI_data;
        when x"22" =>                                                                   -- AXI2_DATA
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC2_AXI_data;
        when x"24" =>                                                                   -- AXI3_DATA
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC3_AXI_data;
        when x"30" =>                                                                   -- ADC1_CFG0
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC1_link_config(0);
        when x"31" =>                                                                   -- ADC1_CFG1
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC1_link_config(1);
        when x"32" =>                                                                   -- ADC1_CFG2
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC1_link_config(2);
        when x"33" =>                                                                   -- ADC1_CFG3
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC1_link_config(3);
        when x"34" =>                                                                   -- ADC2_CFG0
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC2_link_config(0);
        when x"35" =>                                                                   -- ADC2_CFG1
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC2_link_config(1);
        when x"36" =>                                                                   -- ADC2_CFG2
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC2_link_config(2);
        when x"37" =>                                                                   -- ADC2_CFG3
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC2_link_config(3);
        when x"38" =>                                                                   -- ADC3_CFG0
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC3_link_config(0);
        when x"39" =>                                                                   -- ADC3_CFG1
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC3_link_config(1);
        when x"3a" =>                                                                   -- ADC3_CFG2
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC3_link_config(2);
        when x"3b" =>                                                                   -- ADC3_CFG3
          ipbus_out.ipb_rdata                 <= LMB_monitor.ADC3_link_config(3);
        when x"3c" =>                                                                   -- INHIBIT
          ipbus_out.ipb_rdata(23 downto 0)    <= std_logic_vector(LMB_monitor.trigger_inhibit_delay);
        when x"3d" =>                                                                   -- NEXT_POS
          ipbus_out.ipb_rdata                 <= std_logic_vector(LMB_monitor.next_sequence_pos);
        when x"3e" =>                                                                   -- TRIG_MAP
          ipbus_out.ipb_rdata                 <= LMB_monitor.trigger_map;
        when x"3f" =>                                                                   -- TRIG_THRES
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(LMB_monitor.self_trigger_thres);
        when x"40" =>                                                                   -- TRIG_THRES
          ipbus_out.ipb_rdata(8 downto 0)     <= LMB_monitor.trigger_tap_value(1);
          ipbus_out.ipb_rdata(17 downto 9)    <= LMB_monitor.trigger_tap_value(2);
          ipbus_out.ipb_rdata(26 downto 18)   <= LMB_monitor.trigger_tap_value(3);
        when x"41" => -- DEBUG1_0
          ipbus_out.ipb_rdata                 <= LMB_monitor.debug1(0);
        when x"42" => -- Debug register. Put here what you want                         -- DEBUG1
          ipbus_out.ipb_rdata(30)             <= LMB_monitor.DCI_locked;                -- DCI locked signal
          ipbus_out.ipb_rdata(31)             <= LMB_monitor.delay_locked;              -- DelayCTRL locked signal
        when x"43" => -- Temperature/Humidity data     .                                -- TH_SENSOR
          ipbus_out.ipb_rdata(15 downto 0)    <= LMB_monitor.temperature;
          ipbus_out.ipb_rdata(31 downto 16)   <= LMB_monitor.humidity;
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(6 downto 0) is
        when x"02" =>                                                                   -- ACTIONS
          LMB_control.LMB_reset             <= ipbus_in.ipb_wdata(0);
          LMB_control.clock_reset           <= ipbus_in.ipb_wdata(1);
          LMB_control.JESD_sys_reset        <= ipbus_in.ipb_wdata(2);
          LMB_control.IO_reset              <= ipbus_in.ipb_wdata(3);
          ADC_control.I2C_reset             <= ipbus_in.ipb_wdata(4);
          ADC_control.PLL_reset             <= ipbus_in.ipb_wdata(5);
          ADC_control.ADC_reset             <= ipbus_in.ipb_wdata(6);
          LMB_control.AXI_reset             <= ipbus_in.ipb_wdata(7);
          ADC_control.ADC_rx_sync           <= ipbus_in.ipb_wdata(8);
          LMB_control.extra_trigger         <= ipbus_in.ipb_wdata(30);
          LMB_control.trigger_soft          <= ipbus_in.ipb_wdata(31);
        when x"03" =>                                                                   -- SETTINGS
          LMB_control.trigger_mask          <= ipbus_in.ipb_wdata(7 downto 0);
          LMB_control.LED_on                <= ipbus_in.ipb_wdata(8);
          LMB_control.gen_BC0               <= ipbus_in.ipb_wdata(9);
          LMB_control.gen_WTE               <= ipbus_in.ipb_wdata(10);
          LMB_control.self_trigger          <= ipbus_in.ipb_wdata(11);
          LMB_control.self_trigger_mask     <= ipbus_in.ipb_wdata(11+N_ADC downto 12);
          LMB_control.self_trigger_mode     <= ipbus_in.ipb_wdata(15);
          LMB_control.WTE_pos               <= unsigned(ipbus_in.ipb_wdata(29 downto 16));
          LMB_control.force_local_WTE_pos   <= ipbus_in.ipb_wdata(30);
          LMB_control.delay_scan            <= ipbus_in.ipb_wdata(31);
        when x"04" =>                                                                   -- TRIGGER_0
          LMB_control.trigger_delay(0)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(0)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(0) <= '1';
        when x"05" =>                                                                   -- TRIGGER_1
          LMB_control.trigger_delay(1)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(1)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(1) <= '1';
        when x"06" =>                                                                   -- TRIGGER_2
          LMB_control.trigger_delay(2)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(2)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(2) <= '1';
        when x"07" =>                                                                   -- TRIGGER_3
          LMB_control.trigger_delay(3)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(3)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(3) <= '1';
        when x"08" =>                                                                   -- TRIGGER_4
          LMB_control.trigger_delay(4)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(4)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(4) <= '1';
        when x"09" =>                                                                   -- TRIGGER_5
          LMB_control.trigger_delay(5)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(5)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(5) <= '1';
        when x"0a" =>                                                                   -- TRIGGER_6
          LMB_control.trigger_delay(6)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(6)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(6) <= '1';
        when x"0b" =>                                                                   -- TRIGGER_7
          LMB_control.trigger_delay(7)      <= unsigned(ipbus_in.ipb_wdata(24 downto 0));
          LMB_control.trigger_width(7)      <= unsigned(ipbus_in.ipb_wdata(31 downto 25));
          LMB_control.update_delay_phase(7) <= '1';
        when x"0c" => -- Ask to read (b31=1) or write (b31=0) content of register b23..b16 in/with data b15..b0 (DRP_XADC)
          XADC_control.XADC_access          <= '1';
          XADC_control.XADC_WRb             <= ipbus_in.ipb_wdata(31);
          XADC_control.XADC_addr            <= ipbus_in.ipb_wdata(23 downto 16); 
          XADC_control.XADC_data            <= ipbus_in.ipb_wdata(15 downto 0); 
          wait_for_XADC_ready               <= '1';
        when x"10" => -- Read/Write PLL or DTU registers with I2C                       -- PLL_CTRL
          ADC_control.I2C_PLL_data          <= ipbus_in.ipb_wdata(11 downto 0)&"0000";
          ADC_control.I2C_PLL_reg           <= ipbus_in.ipb_wdata(23 downto 16);
          ADC_control.I2C_PLL_Number        <= to_integer(unsigned(ipbus_in.ipb_wdata(25 downto 24)));        -- Chip number 0..3 : PLL-DAC1-DAC2-DAC3
          ADC_control.I2C_R_Wb              <= ipbus_in.ipb_wdata(31);
          ADC_control.I2C_PLL_access        <= '1';
          wait_for_I2C_ready                <= '1';
        when x"12" => -- Read/Write ADC registers with SPI                              -- ADC_CTRL
          ADC_control.SPI_ADC_data          <= ipbus_in.ipb_wdata(7 downto 0);
          ADC_control.SPI_ADC_reg           <= ipbus_in.ipb_wdata(22 downto 8);
          ADC_control.SPI_ADC_number        <= to_integer(unsigned(ipbus_in.ipb_wdata(25 downto 24)));
          ADC_control.SPI_ADC_R_Wb          <= ipbus_in.ipb_wdata(31);
          ADC_control.SPI_ADC_access        <= '1';
          wait_for_SPI_ready                <= '1';
        when x"13" =>                                                                   -- PLL_CONF
          ADC_control.PLL_update_reg        <= ipbus_in.ipb_wdata(23 downto 16);
          ADC_control.PLL_update_val        <= ipbus_in.ipb_wdata(7 downto 0);
          ADC_control.PLL_update_en         <= '1';
        when x"14" =>                                                                   -- DAQ_DELAY
          LMB_control.SW_delay              <= unsigned(ipbus_in.ipb_wdata(31 downto 16));
          LMB_control.HW_delay              <= unsigned(ipbus_in.ipb_wdata(15 downto 0));
        when x"15" =>                                                                   -- JESD_RST_DELAY
          LMB_control.jesd_ADC_reset_delay  <= unsigned(ipbus_in.ipb_wdata);
        when x"18" => -- Sync pattern word from DTU in sync mode                        -- DTU_SYNC_PATTERN
          ADC_control.DTU_Sync_pattern      <= ipbus_in.ipb_wdata(31 downto 0);
        when x"19" => -- Idle pattern in ReSync mode                                    -- DTU_IDLE_PATTERN
          ADC_control.ADC_ReSync_idle       <= ipbus_in.ipb_wdata(7 downto 0);
        when x"20" =>                                                                   -- AXI1_DATA
          LMB_control.ADC1_AXI_data         <= ipbus_in.ipb_wdata;
        when x"21" =>                                                                   -- AXI1_ADDR
          LMB_control.ADC1_AXI_addr         <= ipbus_in.ipb_wdata(12 downto 0); -- 12 bit address + 1 bit to access JESD204C (1) or JESD204C_phy (0) 
          LMB_control.ADC1_AXI_R_Wb         <= ipbus_in.ipb_wdata(31);
          LMB_control.ADC1_AXI_access       <= '1';
          wait_for_ADC1_AXI_ready           <= '1';
        when x"22" =>                                                                   -- AXI2_DATA
          LMB_control.ADC2_AXI_data         <= ipbus_in.ipb_wdata;
        when x"23" =>                                                                   -- AXI2_ADDR
          LMB_control.ADC2_AXI_addr         <= ipbus_in.ipb_wdata(12 downto 0);
          LMB_control.ADC2_AXI_R_Wb         <= ipbus_in.ipb_wdata(31);
          LMB_control.ADC2_AXI_access       <= '1';
          wait_for_ADC2_AXI_ready           <= '1';
        when x"24" =>                                                                   -- AXI3_DATA
          LMB_control.ADC3_AXI_data         <= ipbus_in.ipb_wdata;
        when x"25" =>                                                                   -- AXI3_ADDR
          LMB_control.ADC3_AXI_addr         <= ipbus_in.ipb_wdata(12 downto 0);
          LMB_control.ADC3_AXI_R_Wb         <= ipbus_in.ipb_wdata(31);
          LMB_control.ADC3_AXI_access       <= '1';
          wait_for_ADC3_AXI_ready           <= '1';
        when x"3c" =>                                                                   -- INHIBIT
          LMB_control.trigger_inhibit_delay <= unsigned(ipbus_in.ipb_wdata(23 downto 0));
        when x"3e" =>                                                                   -- TRIG_MAP
          LMB_control.trigger_map           <= ipbus_in.ipb_wdata;
        when x"3f" =>                                                                   -- TRIG_THRES
          LMB_control.self_trigger_thres    <= unsigned(ipbus_in.ipb_wdata(13 downto 0));
        when x"40" =>                                                                   -- TRIG_THRES
          LMB_control.trigger_tap_value(1)  <= ipbus_in.ipb_wdata(8 downto 0);
          LMB_control.trigger_tap_value(2)  <= ipbus_in.ipb_wdata(17 downto 9);
          LMB_control.trigger_tap_value(3)  <= ipbus_in.ipb_wdata(26 downto 18);
        when x"43" => -- Temperature/Humidity register1.                                -- TH_SENSOR
          LMB_control.TH_command            <= ipbus_in.ipb_wdata(7 downto 0);
          LMB_control.TH_timer_off          <= ipbus_in.ipb_wdata(31);
          LMB_control.TH_access             <= '1';
          
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_ack =>
        if wait_for_XADC_ready='1' and XADC_monitor.XADC_ready='1' then
          wait_for_XADC_ready            <= '0';
        end if;
        if wait_for_DTU_ready='1'  and ADC_monitor.ReSync_busy='0' then
          wait_for_DTU_ready             <= '0';
        end if;
        -- Don't wait for I2C transaction, we generate a IPBus time out. Should be checked offline
        --if wait_for_I2C_ready='1'  and ADC_monitor.I2C_PLL_busy='0' then
        --  wait_for_I2C_ready             <= '0';
        --end if;
        if wait_for_SPI_ready='1'  and ADC_monitor.SPI_busy='0' then
          wait_for_SPI_ready             <= '0';
        end if;
        if wait_for_XADC_ready='0' and wait_for_DTU_ready='0' and wait_for_SPI_ready='0' then
          ack <= '1';
          ipbus_rw_state                 <= ipbus_finished;
        end if;
      when ipbus_finished =>
        ack <= '0';
        ipbus_rw_state <= ipbus_rw_idle;
      end case;
    end if;
  end process reg;

  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';

 -- SYSMONE1: Xilinx Analog-to-Digital Converter and System Monitor
   --           Kintex UltraScale
   -- Xilinx HDL Language Template, version 2019.2

   SYSMONE1_inst : SYSMONE1
   generic map (
      -- INIT_40 - INIT_44: SYSMON configuration registers
      INIT_40 => X"0000",
      INIT_41 => X"0000",
      INIT_42 => X"0000",
      INIT_43 => X"0000",
      INIT_44 => X"0000",
      INIT_45 => X"0000",               -- Analog Bus Register
      -- INIT_46 - INIT_4F: Sequence Registers
      INIT_46 => X"0000",
      INIT_47 => X"0000",
      INIT_48 => X"0000",
      INIT_49 => X"0000",
      INIT_4A => X"0000",
      INIT_4B => X"0000",
      INIT_4C => X"0000",
      INIT_4D => X"0000",
      INIT_4E => X"0000",
      INIT_4F => X"0000",
      -- INIT_50 - INIT_5F: Alarm Limit Registers
      INIT_50 => X"0000",
      INIT_51 => X"0000",
      INIT_52 => X"0000",
      INIT_53 => X"0000",
      INIT_54 => X"0000",
      INIT_55 => X"0000",
      INIT_56 => X"0000",
      INIT_57 => X"0000",
      INIT_58 => X"0000",
      INIT_59 => X"0000",
      INIT_5A => X"0000",
      INIT_5B => X"0000",
      INIT_5C => X"0000",
      INIT_5D => X"0000",
      INIT_5E => X"0000",
      INIT_5F => X"0000",
      -- INIT_60 - INIT_6F: User Supply Alarms
      INIT_60 => X"0000",
      INIT_61 => X"0000",
      INIT_62 => X"0000",
      INIT_63 => X"0000",
      INIT_64 => X"0000",
      INIT_65 => X"0000",
      INIT_66 => X"0000",
      INIT_67 => X"0000",
      INIT_68 => X"0000",
      INIT_69 => X"0000",
      INIT_6A => X"0000",
      INIT_6B => X"0000",
      INIT_6C => X"0000",
      INIT_6D => X"0000",
      INIT_6E => X"0000",
      INIT_6F => X"0000",
      -- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion on
      -- specific pins
      IS_CONVSTCLK_INVERTED => '0',     -- Optional inversion for CONVSTCLK, 0-1
      IS_DCLK_INVERTED => '0',          -- Optional inversion for DCLK, 0-1
      -- Simulation attributes: Set for proper simulation behavior
      SIM_MONITOR_FILE => "design.txt", -- Analog simulation data file name
      -- User Voltage Monitor: SYSMON User voltage monitor
      SYSMON_VUSER0_BANK => 0,          -- Specify IO Bank for User0
      SYSMON_VUSER0_MONITOR => "NONE",  -- Specify Voltage for User0
      SYSMON_VUSER1_BANK => 0,          -- Specify IO Bank for User1
      SYSMON_VUSER1_MONITOR => "NONE",  -- Specify Voltage for User1
      SYSMON_VUSER2_BANK => 0,          -- Specify IO Bank for User2
      SYSMON_VUSER2_MONITOR => "NONE",  -- Specify Voltage for User2
      SYSMON_VUSER3_MONITOR => "NONE"   -- Specify Voltage for User3
   )
   port map (
-- ALARMS outputs: ALM, OT
      ALM          => open,           -- 16-bit output: Output alarm for temp, Vccint, Vccaux and Vccbram
      OT           => open,           -- 1-bit output: Over-Temperature alarm
-- STATUS outputs: SYSMON status ports
      BUSY         => open,                     -- 1-bit output: System Monitor busy output
      CHANNEL      => channel,                  -- 6-bit output: Channel selection outputs
      EOC          => XADC_eoc,                 -- 1-bit output: End of Conversion
      EOS          => XADC_eos,                 -- 1-bit output: End of Sequence

      JTAGBUSY     => open,                     -- 1-bit output: JTAG DRP transaction in progress output
      JTAGLOCKED   => open,                     -- 1-bit output: JTAG requested DRP port lock
      JTAGMODIFIED => open,                     -- 1-bit output: JTAG Write to the DRP has occurred
      MUXADDR      => open,                     -- 5-bit output: External MUX channel decode
-- Auxiliary Analog-Input Pairs inputs: VAUXP[15:0], VAUXN[15:0]
      VAUXN        => vauxn,                    -- 16-bit input: N-side auxiliary analog input
      VAUXP        => vauxp,                    -- 16-bit input: P-side auxiliary analog input
-- CONTROL and CLOCK inputs: Reset, conversion start and clock inputs
      CONVST       => '0',                      -- 1-bit input: Convert start input
      CONVSTCLK    => '0',                      -- 1-bit input: Convert start input
      RESET        => '0',                      -- 1-bit input: Active-High reset
-- Dedicated Analog Input Pair inputs: VP/VN
      VN           => '0',                      -- 1-bit input: N-side analog input
      VP           => '0',                      -- 1-bit input: P-side analog input
-- Dynamic Reconfiguration Port (DRP) outputs: Dynamic Reconfiguration Ports
      DO           => loc_XADC_data,            -- 16-bit output: DRP output data bus
      DRDY         => XADC_monitor.XADC_ready,  -- 1-bit output: DRP data ready
-- Dynamic Reconfiguration Port (DRP) inputs: Dynamic Reconfiguration Ports
      DADDR        => XADC_control.XADC_addr,   -- 8-bit input: DRP address bus
      DCLK         => XADC_clk,                 -- 1-bit input: DRP clock
      DEN          => XADC_control.XADC_access, -- 1-bit input: DRP enable signal
      DI           => XADC_control.XADC_data,   -- 16-bit input: DRP input data bus
      DWE          => XADC_control.XADC_WRb,    -- 1-bit input: DRP write enable

-- I2C Interface outputs: Ports used with the I2C DRP interface
      I2C_SCLK_TS  => open,    -- 1-bit output: I2C_SCLK output port
      I2C_SDA_TS   => open,     -- 1-bit output: I2C_SDA_TS output port
-- I2C Interface inputs: Ports used with the I2C DRP interface
      I2C_SCLK     => '1',                     -- 1-bit input: I2C_SCLK input port
      I2C_SDA      => '1'                      -- 1-bit input: I2C_SDA input port
   );

  XADC_clk <= not ipbus_clk;

  vauxp(0) <= ADC_DCDC_Temp;
  vauxn(0) <= ADC_DCDC_Temp_ref;
  vauxp(1) <= FPGA_DCDC_Temp2;
  vauxn(1) <= FPGA_DCDC_Temp2_ref;
  vauxp(2) <= FPGA_DCDC_Temp1;
  vauxn(2) <= FPGA_DCDC_Temp1_ref;

  --vauxp(2) <= Vdummy_in;
  --vauxn(2) <= Vdummy_ref;

  XADC_eos_del <= XADC_eos when rising_edge(ipbus_clk) else XADC_eos_del;
  seq_counter : process (reset, ipbus_clk, XADC_eos, XADC_eos_del) is
  begin  -- process reg
    if reset = '1' then
      total_seq <= (others => '0');
    elsif rising_edge(ipbus_clk) then
      if XADC_eos='1' and XADC_eos_del='0' then
        total_seq <= total_seq+1;
      end if;
    end if;
  end process seq_counter;
  
  conv_counter : process (reset, ipbus_clk) is
  begin
    if reset = '1' then
      total_conv <= (others => '0');
    elsif rising_edge(ipbus_clk) then
      XADC_eoc_del <= XADC_eoc;
      if XADC_eoc='1' and XADC_eoc_del='0' then
        total_conv <= total_conv+1;
      end if;
    end if;
  end process conv_counter;

  access_counter : process (reset, ipbus_clk) is
  begin
    if reset = '1' then
      total_access <= (others => '0');
    elsif rising_edge(ipbus_clk) then
      XADC_ready_del <= XADC_monitor.XADC_ready;
      if XADC_monitor.XADC_ready='1' and XADC_ready_del='0' then
        XADC_monitor.XADC_data <= loc_XADC_data;
        total_access <= total_access+1;
      end if;
    end if;
  end process access_counter;
  XADC_monitor.XADC_addr <= XADC_control.XADC_addr;

end architecture rtl;
