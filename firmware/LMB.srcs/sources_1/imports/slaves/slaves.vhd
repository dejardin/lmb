-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.ALL;
use work.LMB_IO.all;
use IEEE.NUMERIC_STD.all;

entity slaves is
  port(
    reset               : in    std_logic;
    trigger             : in    std_logic;
    DAQ_busy            : out   std_logic;
    ipbus_clk           : in    std_logic;
    ipbus_in            : in    ipb_wbus;
    ipbus_out           : out   ipb_rbus;
    LMB_Monitor         : in    LMB_Monitor_t;
    LMB_Control         : out   LMB_Control_t;
    ADC_Monitor         : inout ADC_Monitor_t;
    ADC_Control         : out   ADC_Control_t;
    clk_counter         : in    unsigned(13 downto 0);
    clk_40              : in    std_logic;
    clk_160             : in    std_logic;
    clk_shift_reg       : in    std_logic;
    clk_memory          : in    std_logic;
    ADC1_data_in        : in    std_logic_vector(127 downto 0);
    ADC1_data_valid     : in    std_logic;
    ADC2_data_in        : in    std_logic_vector(127 downto 0);
    ADC2_data_valid     : in    std_logic;
    ADC3_data_in        : in    std_logic_vector(127 downto 0);
    ADC3_data_valid     : in    std_logic;
    ADC_DCDC_Temp       : in    std_logic;
    ADC_DCDC_Temp_ref   : in    std_logic;
    FPGA_DCDC_Temp1     : in    std_logic;
    FPGA_DCDC_Temp1_ref : in    std_logic;
    FPGA_DCDC_Temp2     : in    std_logic;
    FPGA_DCDC_Temp2_ref : in    std_logic;
    WTE                 : in    std_logic;
    next_lmr            : out   std_logic;
    local_trigger       : out   std_logic;
    next_sequence_pos   : out   std_logic_vector(31 downto 0)
  );
end slaves;

architecture rtl of slaves is

  constant NSLV             : positive := 3;
  signal ipbw               : ipb_wbus_array(NSLV-1 downto 0);
  signal ipbr, ipbr_d       : ipb_rbus_array(NSLV-1 downto 0);
  signal ctrl_reg           : std_logic_vector(31 downto 0);
  signal inj_ctrl, inj_stat : std_logic_vector(63 downto 0);

begin


fabric: entity work.ipbus_fabric
  generic map(NSLV => NSLV)
  port map(
    ipbus_in            => ipbus_in,
    ipbus_out           => ipbus_out,
    ipbus_to_slaves     => ipbw,
    ipbus_from_slaves   => ipbr
  );

-- Slave 0: id / rst reg
slave0: entity work.LMB_ctrlreg
  port map (
    ipbus_clk           => ipbus_clk,
    reset               => reset,
    ipbus_in            => ipbw(0),
    ipbus_out           => ipbr(0),
    LMB_monitor         => LMB_Monitor,
    LMB_control         => LMB_Control,
    ADC_monitor         => ADC_Monitor,
    ADC_control         => ADC_Control,
    
    ADC_DCDC_Temp       => ADC_DCDC_Temp,
    ADC_DCDC_Temp_ref   => ADC_DCDC_Temp_ref,
    FPGA_DCDC_Temp1     => FPGA_DCDC_Temp1,
    FPGA_DCDC_Temp1_ref => FPGA_DCDC_Temp1_ref,
    FPGA_DCDC_Temp2     => FPGA_DCDC_Temp2,
    FPGA_DCDC_Temp2_ref => FPGA_DCDC_Temp2_ref
  );

-- Slave 1: ADC capture
slave1: entity work.ADC_capture
  port map(
    trigger             => trigger,
    DAQ_busy            => DAQ_busy,
    ipbus_clk           => ipbus_clk,
    reset               => reset,
    ipbus_in            => ipbw(1),
    ipbus_out           => ipbr(1),
    clk_counter         => clk_counter,
    clk_40              => clk_40,
    clk_sequence        => clk_160,
    clk_shift_reg       => clk_shift_reg,
    clk_memory          => clk_memory,
    local_trigger       => local_trigger,
    self_trigger        => LMB_Monitor.self_trigger,
    self_trigger_mode   => LMB_Monitor.self_trigger_mode,
    self_trigger_mask   => LMB_Monitor.self_trigger_mask,
    self_trigger_thres  => LMB_Monitor.self_trigger_thres,
    ADC1_data_in        => ADC1_data_in,
    ADC1_data_valid     => ADC1_data_valid,
    ADC2_data_in        => ADC2_data_in,
    ADC2_data_valid     => ADC2_data_valid,
    ADC3_data_in        => ADC3_data_in,
    ADC3_data_valid     => ADC3_data_valid
  );

-- Slave 2: Monitoring sequence
slave2: entity work.sequence_FIFO
  port map(
    ipbus_clk           => ipbus_clk,
    reset               => reset,
    ipbus_in            => ipbw(2),
    ipbus_out           => ipbr(2),
    clk_160             => clk_160,
    WTE                 => WTE,
    next_lmr            => next_lmr,
    next_sequence_pos   => next_sequence_pos
  );
end rtl;
