EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 15
Title "FE connection"
Date "2021-06-18"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2250 1025 2    39   ~ 0
ReSync_N
Text Label 2250 925  2    39   ~ 0
ReSync_P
Text Label 9375 1325 2    39   ~ 0
Clk1_1280_N
Text Label 9375 1225 2    39   ~ 0
Clk1_1280_P
Text Label 2250 5750 2    39   ~ 0
PwupRstb
Wire Wire Line
	2050 3550 1850 3550
Wire Wire Line
	1850 1025 2250 1025
Wire Wire Line
	8975 1225 9375 1225
Wire Wire Line
	8975 1325 9375 1325
Wire Wire Line
	750  3450 650  3450
Wire Wire Line
	1850 3450 1950 3450
Wire Wire Line
	650  6050 750  6050
Wire Wire Line
	1850 6050 1950 6050
Wire Wire Line
	650  3225 750  3225
Wire Wire Line
	1950 3225 1850 3225
Wire Wire Line
	1850 625  1950 625 
Wire Wire Line
	750  625  650  625 
Wire Wire Line
	550  3125 550  3325
Wire Wire Line
	550  3325 2050 3325
Wire Wire Line
	2050 3325 2050 3125
Wire Wire Line
	2050 3125 1850 3125
$Comp
L Device:R R?
U 1 1 5F9A7EB1
P 4425 6800
AR Path="/5F9A7EB1" Ref="R?"  Part="1" 
AR Path="/5F997D25/5F9A7EB1" Ref="R?"  Part="1" 
AR Path="/5EFE8A79/5F9A7EB1" Ref="R?"  Part="1" 
AR Path="/5D2A5AA9/5F9A7EB1" Ref="R1001"  Part="1" 
F 0 "R1001" V 4505 6800 39  0000 C CNN
F 1 "1k" V 4425 6800 39  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4355 6800 50  0001 C CNN
F 3 "" H 4425 6800 50  0000 C CNN
	1    4425 6800
	-1   0    0    1   
$EndComp
Wire Wire Line
	550  3550 750  3550
Wire Wire Line
	550  3125 750  3125
$Comp
L Device:R R?
U 1 1 5F9A7EBD
P 4575 6800
AR Path="/5F9A7EBD" Ref="R?"  Part="1" 
AR Path="/5F997D25/5F9A7EBD" Ref="R?"  Part="1" 
AR Path="/5EFE8A79/5F9A7EBD" Ref="R?"  Part="1" 
AR Path="/5D2A5AA9/5F9A7EBD" Ref="R1002"  Part="1" 
F 0 "R1002" V 4655 6800 39  0000 C CNN
F 1 "1k" V 4575 6800 39  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4505 6800 50  0001 C CNN
F 3 "" H 4575 6800 50  0000 C CNN
	1    4575 6800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 5750 1850 5750
Connection ~ 550  3325
$Comp
L power:GNDD #PWR?
U 1 1 5F9A7F01
P 650 6050
AR Path="/5F9A7F01" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5F9A7F01" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5F9A7F01" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5F9A7F01" Ref="#PWR01005"  Part="1" 
F 0 "#PWR01005" H 650 5800 50  0001 C CNN
F 1 "GNDD" H 650 5900 50  0000 C CNN
F 2 "" H 650 6050 50  0000 C CNN
F 3 "" H 650 6050 50  0000 C CNN
	1    650  6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5F9A7F07
P 1950 6050
AR Path="/5F9A7F07" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5F9A7F07" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5F9A7F07" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5F9A7F07" Ref="#PWR01006"  Part="1" 
F 0 "#PWR01006" H 1950 5800 50  0001 C CNN
F 1 "GNDD" H 1950 5900 50  0000 C CNN
F 2 "" H 1950 6050 50  0000 C CNN
F 3 "" H 1950 6050 50  0000 C CNN
	1    1950 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 925  2250 925 
Wire Wire Line
	650  625  650  925 
Wire Wire Line
	1950 3450 1950 3650
Wire Wire Line
	1950 625  1950 725 
Wire Wire Line
	4575 6650 4425 6650
Wire Wire Line
	1850 5550 1950 5550
Connection ~ 1950 5550
Wire Wire Line
	1950 5550 1950 6050
Wire Wire Line
	1850 5250 1950 5250
Connection ~ 1950 5250
Wire Wire Line
	1950 5250 1950 5550
Wire Wire Line
	1850 4950 1950 4950
Connection ~ 1950 4950
Wire Wire Line
	1950 4950 1950 5250
Wire Wire Line
	750  5050 650  5050
Connection ~ 650  5050
Wire Wire Line
	650  5050 650  5150
Wire Wire Line
	750  5150 650  5150
Connection ~ 650  5150
Wire Wire Line
	750  5450 650  5450
Wire Wire Line
	650  5150 650  5350
Connection ~ 650  5450
Wire Wire Line
	650  5450 650  6050
Wire Wire Line
	750  5350 650  5350
Connection ~ 650  5350
Wire Wire Line
	650  5350 650  5450
Wire Wire Line
	750  4650 650  4650
Connection ~ 650  4650
Wire Wire Line
	650  4650 650  5050
Wire Wire Line
	750  4550 650  4550
Connection ~ 650  4550
Wire Wire Line
	650  4550 650  4650
Wire Wire Line
	1850 4450 1950 4450
Connection ~ 1950 4450
Wire Wire Line
	1850 4750 1950 4750
Wire Wire Line
	1950 4450 1950 4750
Connection ~ 1950 4750
Wire Wire Line
	1950 4750 1950 4950
Wire Wire Line
	1850 4150 1950 4150
Connection ~ 1950 4150
Wire Wire Line
	1950 4150 1950 4450
Wire Wire Line
	1850 3950 1950 3950
Connection ~ 1950 3950
Wire Wire Line
	1950 3950 1950 4150
Wire Wire Line
	1850 3650 1950 3650
Connection ~ 1950 3650
Wire Wire Line
	1950 3650 1950 3950
Wire Wire Line
	750  3650 650  3650
Connection ~ 650  3650
Wire Wire Line
	650  3650 650  3750
Wire Wire Line
	750  3750 650  3750
Connection ~ 650  3750
Wire Wire Line
	650  3750 650  3850
Wire Wire Line
	750  3850 650  3850
Connection ~ 650  3850
Wire Wire Line
	650  3850 650  4250
Wire Wire Line
	1850 3025 1950 3025
Connection ~ 1950 3025
Wire Wire Line
	1950 3025 1950 3225
Wire Wire Line
	1850 2725 1950 2725
Connection ~ 1950 2725
Wire Wire Line
	1950 2725 1950 3025
Wire Wire Line
	1850 2525 1950 2525
Connection ~ 1950 2525
Wire Wire Line
	1950 2525 1950 2725
Wire Wire Line
	1850 2225 1950 2225
Connection ~ 1950 2225
Wire Wire Line
	1950 2225 1950 2525
Wire Wire Line
	1850 1925 1950 1925
Connection ~ 1950 1925
Wire Wire Line
	1950 1925 1950 2225
Wire Wire Line
	1850 1725 1950 1725
Connection ~ 1950 1725
Wire Wire Line
	1950 1725 1950 1925
Wire Wire Line
	750  2025 650  2025
Connection ~ 650  2025
Wire Wire Line
	650  2025 650  2125
Wire Wire Line
	750  2125 650  2125
Connection ~ 650  2125
Wire Wire Line
	650  2125 650  2325
Wire Wire Line
	750  2325 650  2325
Connection ~ 650  2325
Wire Wire Line
	650  2325 650  2425
Wire Wire Line
	750  2425 650  2425
Connection ~ 650  2425
Wire Wire Line
	650  2425 650  2825
Wire Wire Line
	750  2825 650  2825
Connection ~ 650  2825
Wire Wire Line
	650  2825 650  2925
Wire Wire Line
	750  2925 650  2925
Connection ~ 650  2925
Wire Wire Line
	650  2925 650  3225
Wire Wire Line
	750  1625 650  1625
Connection ~ 650  1625
Wire Wire Line
	650  1625 650  2025
Wire Wire Line
	750  1525 650  1525
Connection ~ 650  1525
Wire Wire Line
	650  1525 650  1625
Wire Wire Line
	750  1325 650  1325
Connection ~ 650  1325
Wire Wire Line
	650  1325 650  1525
Wire Wire Line
	750  1225 650  1225
Connection ~ 650  1225
Wire Wire Line
	650  1225 650  1325
Wire Wire Line
	750  1025 650  1025
Connection ~ 650  1025
Wire Wire Line
	650  1025 650  1225
Wire Wire Line
	750  925  650  925 
Connection ~ 650  925 
Wire Wire Line
	650  925  650  1025
Wire Wire Line
	1850 725  1950 725 
Connection ~ 1950 725 
Wire Wire Line
	1950 725  1950 825 
Wire Wire Line
	1850 825  1950 825 
Connection ~ 1950 825 
Wire Wire Line
	1950 825  1950 1125
Wire Wire Line
	1850 1125 1950 1125
Connection ~ 1950 1125
Wire Wire Line
	1950 1125 1950 1425
Wire Wire Line
	1850 1425 1950 1425
Connection ~ 1950 1425
Wire Wire Line
	1950 1425 1950 1725
Wire Wire Line
	750  4250 650  4250
Connection ~ 650  4250
Wire Wire Line
	650  4250 650  4350
Wire Wire Line
	750  4350 650  4350
Connection ~ 650  4350
Wire Wire Line
	650  4350 650  4550
NoConn ~ 750  5550
NoConn ~ 750  5250
NoConn ~ 750  4050
NoConn ~ 750  3950
NoConn ~ 750  3025
NoConn ~ 750  2625
NoConn ~ 750  2525
NoConn ~ 750  2225
Wire Wire Line
	500  725  750  725 
Wire Wire Line
	500  825  750  825 
NoConn ~ 750  5650
NoConn ~ 750  1825
NoConn ~ 750  4450
NoConn ~ 750  4750
NoConn ~ 750  1425
NoConn ~ 750  1725
NoConn ~ 750  4850
NoConn ~ 750  5750
NoConn ~ 750  5950
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5E94E6CF
P 1300 1925
AR Path="/5F997D25/5E94E6CF" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5E94E6CF" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5E94E6CF" Ref="J1001"  Part="1" 
F 0 "J1001" H 1300 650 60  0000 C CNN
F 1 "ERNI-244838" V 1300 1925 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 1200 1925 60  0001 C CNN
F 3 "" H 1200 1925 60  0000 C CNN
	1    1300 1925
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5E9566FE
P 1300 4750
AR Path="/5F997D25/5E9566FE" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5E9566FE" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5E9566FE" Ref="J1006"  Part="1" 
F 0 "J1006" H 1300 3475 60  0000 C CNN
F 1 "ERNI-244838" V 1300 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 1200 4750 60  0001 C CNN
F 3 "" H 1200 4750 60  0000 C CNN
	1    1300 4750
	-1   0    0    -1  
$EndComp
Text HLabel 3200 6800 0    39   Input ~ 0
PiN2_out_P[0..7]
Text HLabel 3200 6850 0    39   Input ~ 0
PiN2_out_N[0..7]
Wire Wire Line
	8975 2325 9375 2325
Wire Wire Line
	8975 2425 9375 2425
Text Label 9375 2325 2    39   ~ 0
PiN1_out_P1
Text Label 9375 2425 2    39   ~ 0
PiN1_out_N1
Wire Wire Line
	8975 3750 9375 3750
Wire Wire Line
	8975 3850 9375 3850
Text Label 9375 3750 2    39   ~ 0
PiN1_out_P2
Text Label 9375 3850 2    39   ~ 0
PiN1_out_N2
Wire Wire Line
	8975 4550 9375 4550
Wire Wire Line
	8975 4650 9375 4650
Text Label 9375 4550 2    39   ~ 0
PiN1_out_P3
Text Label 9375 4650 2    39   ~ 0
PiN1_out_N3
Wire Wire Line
	8975 5350 9375 5350
Wire Wire Line
	8975 5450 9375 5450
Text Label 9375 5350 2    39   ~ 0
PiN1_out_P4
Text Label 9375 5450 2    39   ~ 0
PiN1_out_N4
NoConn ~ 1850 1825
NoConn ~ 1850 2625
NoConn ~ 1850 4050
NoConn ~ 1850 4850
NoConn ~ 1850 5650
Wire Wire Line
	2050 3325 2050 3550
Connection ~ 2050 3325
Wire Wire Line
	550  3325 550  3550
Wire Wire Line
	650  3225 650  3450
Connection ~ 650  3225
Connection ~ 650  3450
Wire Wire Line
	650  3450 650  3650
Wire Wire Line
	1950 3225 1950 3450
Connection ~ 1950 3225
Connection ~ 1950 3450
Connection ~ 650  6050
Connection ~ 1950 6050
Wire Wire Line
	3825 3550 3625 3550
Wire Wire Line
	2525 3450 2425 3450
Wire Wire Line
	3625 3450 3725 3450
Wire Wire Line
	2425 6050 2525 6050
Wire Wire Line
	3625 6050 3725 6050
Wire Wire Line
	2425 3225 2525 3225
Wire Wire Line
	3725 3225 3625 3225
Wire Wire Line
	3625 625  3725 625 
Wire Wire Line
	2525 625  2425 625 
Wire Wire Line
	2325 3125 2325 3325
Wire Wire Line
	2325 3325 3825 3325
Wire Wire Line
	3825 3325 3825 3125
Wire Wire Line
	3825 3125 3625 3125
Wire Wire Line
	2325 3550 2525 3550
Wire Wire Line
	2325 3125 2525 3125
Connection ~ 2325 3325
$Comp
L power:GNDD #PWR?
U 1 1 5EA2635C
P 2425 6050
AR Path="/5EA2635C" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EA2635C" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EA2635C" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EA2635C" Ref="#PWR01007"  Part="1" 
F 0 "#PWR01007" H 2425 5800 50  0001 C CNN
F 1 "GNDD" H 2425 5900 50  0000 C CNN
F 2 "" H 2425 6050 50  0000 C CNN
F 3 "" H 2425 6050 50  0000 C CNN
	1    2425 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EA26366
P 3725 6050
AR Path="/5EA26366" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EA26366" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EA26366" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EA26366" Ref="#PWR01008"  Part="1" 
F 0 "#PWR01008" H 3725 5800 50  0001 C CNN
F 1 "GNDD" H 3725 5900 50  0000 C CNN
F 2 "" H 3725 6050 50  0000 C CNN
F 3 "" H 3725 6050 50  0000 C CNN
	1    3725 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2425 625  2425 925 
Wire Wire Line
	3725 3450 3725 3650
Wire Wire Line
	3725 625  3725 725 
Wire Wire Line
	3625 5550 3725 5550
Connection ~ 3725 5550
Wire Wire Line
	3725 5550 3725 6050
Wire Wire Line
	3625 5250 3725 5250
Connection ~ 3725 5250
Wire Wire Line
	3725 5250 3725 5550
Wire Wire Line
	3625 4950 3725 4950
Connection ~ 3725 4950
Wire Wire Line
	3725 4950 3725 5250
Wire Wire Line
	2525 5050 2425 5050
Connection ~ 2425 5050
Wire Wire Line
	2425 5050 2425 5150
Wire Wire Line
	2525 5150 2425 5150
Connection ~ 2425 5150
Wire Wire Line
	2525 5450 2425 5450
Wire Wire Line
	2425 5150 2425 5350
Connection ~ 2425 5450
Wire Wire Line
	2425 5450 2425 6050
Wire Wire Line
	2525 5350 2425 5350
Connection ~ 2425 5350
Wire Wire Line
	2425 5350 2425 5450
Wire Wire Line
	2525 4650 2425 4650
Connection ~ 2425 4650
Wire Wire Line
	2425 4650 2425 5050
Wire Wire Line
	2525 4550 2425 4550
Connection ~ 2425 4550
Wire Wire Line
	2425 4550 2425 4650
Wire Wire Line
	3625 4450 3725 4450
Connection ~ 3725 4450
Wire Wire Line
	3625 4750 3725 4750
Wire Wire Line
	3725 4450 3725 4750
Connection ~ 3725 4750
Wire Wire Line
	3725 4750 3725 4950
Wire Wire Line
	3625 4150 3725 4150
Connection ~ 3725 4150
Wire Wire Line
	3725 4150 3725 4450
Wire Wire Line
	3625 3950 3725 3950
Connection ~ 3725 3950
Wire Wire Line
	3725 3950 3725 4150
Wire Wire Line
	3625 3650 3725 3650
Connection ~ 3725 3650
Wire Wire Line
	3725 3650 3725 3950
Wire Wire Line
	2525 3650 2425 3650
Connection ~ 2425 3650
Wire Wire Line
	2425 3650 2425 3750
Wire Wire Line
	2525 3750 2425 3750
Connection ~ 2425 3750
Wire Wire Line
	2425 3750 2425 3850
Wire Wire Line
	2525 3850 2425 3850
Connection ~ 2425 3850
Wire Wire Line
	2425 3850 2425 4250
Wire Wire Line
	3625 3025 3725 3025
Connection ~ 3725 3025
Wire Wire Line
	3725 3025 3725 3225
Wire Wire Line
	3625 2725 3725 2725
Connection ~ 3725 2725
Wire Wire Line
	3725 2725 3725 3025
Wire Wire Line
	3625 2525 3725 2525
Connection ~ 3725 2525
Wire Wire Line
	3725 2525 3725 2725
Wire Wire Line
	3625 2225 3725 2225
Connection ~ 3725 2225
Wire Wire Line
	3725 2225 3725 2525
Wire Wire Line
	3625 1925 3725 1925
Connection ~ 3725 1925
Wire Wire Line
	3725 1925 3725 2225
Wire Wire Line
	3625 1725 3725 1725
Connection ~ 3725 1725
Wire Wire Line
	3725 1725 3725 1925
Wire Wire Line
	2525 2025 2425 2025
Connection ~ 2425 2025
Wire Wire Line
	2425 2025 2425 2125
Wire Wire Line
	2525 2125 2425 2125
Connection ~ 2425 2125
Wire Wire Line
	2425 2125 2425 2325
Wire Wire Line
	2525 2325 2425 2325
Connection ~ 2425 2325
Wire Wire Line
	2425 2325 2425 2425
Wire Wire Line
	2525 2425 2425 2425
Connection ~ 2425 2425
Wire Wire Line
	2425 2425 2425 2825
Wire Wire Line
	2525 2825 2425 2825
Connection ~ 2425 2825
Wire Wire Line
	2425 2825 2425 2925
Wire Wire Line
	2525 2925 2425 2925
Connection ~ 2425 2925
Wire Wire Line
	2425 2925 2425 3225
Wire Wire Line
	2525 1625 2425 1625
Connection ~ 2425 1625
Wire Wire Line
	2425 1625 2425 2025
Wire Wire Line
	2525 1525 2425 1525
Connection ~ 2425 1525
Wire Wire Line
	2425 1525 2425 1625
Wire Wire Line
	2525 1325 2425 1325
Connection ~ 2425 1325
Wire Wire Line
	2425 1325 2425 1525
Wire Wire Line
	2525 1225 2425 1225
Connection ~ 2425 1225
Wire Wire Line
	2425 1225 2425 1325
Wire Wire Line
	2525 1025 2425 1025
Connection ~ 2425 1025
Wire Wire Line
	2425 1025 2425 1225
Wire Wire Line
	2525 925  2425 925 
Connection ~ 2425 925 
Wire Wire Line
	2425 925  2425 1025
Wire Wire Line
	3625 725  3725 725 
Connection ~ 3725 725 
Wire Wire Line
	3725 725  3725 825 
Wire Wire Line
	3625 825  3725 825 
Connection ~ 3725 825 
Wire Wire Line
	3725 825  3725 1125
Wire Wire Line
	3625 1125 3725 1125
Connection ~ 3725 1125
Wire Wire Line
	3725 1125 3725 1425
Wire Wire Line
	3625 1425 3725 1425
Connection ~ 3725 1425
Wire Wire Line
	3725 1425 3725 1725
Wire Wire Line
	2525 4250 2425 4250
Connection ~ 2425 4250
Wire Wire Line
	2425 4250 2425 4350
Wire Wire Line
	2525 4350 2425 4350
Connection ~ 2425 4350
Wire Wire Line
	2425 4350 2425 4550
NoConn ~ 2525 5550
NoConn ~ 2525 5250
NoConn ~ 2525 4050
NoConn ~ 2525 3950
NoConn ~ 2525 3025
NoConn ~ 2525 2625
NoConn ~ 2525 2525
NoConn ~ 2525 2225
NoConn ~ 2525 5650
NoConn ~ 2525 1825
NoConn ~ 2525 4450
NoConn ~ 2525 4750
NoConn ~ 2525 1425
NoConn ~ 2525 1725
NoConn ~ 2525 4850
NoConn ~ 2525 5750
NoConn ~ 2525 5950
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EA26404
P 3075 1925
AR Path="/5F997D25/5EA26404" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EA26404" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EA26404" Ref="J1002"  Part="1" 
F 0 "J1002" H 3075 650 60  0000 C CNN
F 1 "ERNI-244838" V 3075 1925 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 2975 1925 60  0001 C CNN
F 3 "" H 2975 1925 60  0000 C CNN
	1    3075 1925
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EA2640E
P 3075 4750
AR Path="/5F997D25/5EA2640E" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EA2640E" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EA2640E" Ref="J1007"  Part="1" 
F 0 "J1007" H 3075 3475 60  0000 C CNN
F 1 "ERNI-244838" V 3075 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 2975 4750 60  0001 C CNN
F 3 "" H 2975 4750 60  0000 C CNN
	1    3075 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7175 1525 7575 1525
Wire Wire Line
	7175 1625 7575 1625
Text Label 7575 1525 2    39   ~ 0
PiN1_out_P5
Text Label 7575 1625 2    39   ~ 0
PiN1_out_N5
Wire Wire Line
	7175 2325 7575 2325
Wire Wire Line
	7175 2425 7575 2425
Text Label 7575 2325 2    39   ~ 0
PiN1_out_P6
Text Label 7575 2425 2    39   ~ 0
PiN1_out_N6
Wire Wire Line
	7175 3750 7575 3750
Wire Wire Line
	7175 3850 7575 3850
Text Label 7575 3750 2    39   ~ 0
PiN1_out_P7
Text Label 7575 3850 2    39   ~ 0
PiN1_out_N7
NoConn ~ 3625 1825
NoConn ~ 3625 2625
NoConn ~ 3625 4050
NoConn ~ 3625 4850
NoConn ~ 3625 5650
Wire Wire Line
	3825 3325 3825 3550
Connection ~ 3825 3325
Wire Wire Line
	2325 3325 2325 3550
Wire Wire Line
	2425 3225 2425 3450
Connection ~ 2425 3225
Connection ~ 2425 3450
Wire Wire Line
	2425 3450 2425 3650
Wire Wire Line
	3725 3225 3725 3450
Connection ~ 3725 3225
Connection ~ 3725 3450
Connection ~ 2425 6050
Connection ~ 3725 6050
NoConn ~ 2525 725 
NoConn ~ 2525 825 
NoConn ~ 3625 925 
NoConn ~ 3625 1025
Wire Wire Line
	5600 3550 5400 3550
Wire Wire Line
	4300 3450 4200 3450
Wire Wire Line
	5400 3450 5500 3450
Wire Wire Line
	4200 6050 4300 6050
Wire Wire Line
	5400 6050 5500 6050
Wire Wire Line
	4200 3225 4300 3225
Wire Wire Line
	5500 3225 5400 3225
Wire Wire Line
	5400 625  5500 625 
Wire Wire Line
	4300 625  4200 625 
Wire Wire Line
	4100 3125 4100 3325
Wire Wire Line
	4100 3325 5600 3325
Wire Wire Line
	5600 3325 5600 3125
Wire Wire Line
	5600 3125 5400 3125
Wire Wire Line
	4100 3550 4300 3550
Wire Wire Line
	4100 3125 4300 3125
Connection ~ 4100 3325
$Comp
L power:GNDD #PWR?
U 1 1 5EAA1365
P 4200 6050
AR Path="/5EAA1365" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EAA1365" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EAA1365" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EAA1365" Ref="#PWR01009"  Part="1" 
F 0 "#PWR01009" H 4200 5800 50  0001 C CNN
F 1 "GNDD" H 4200 5900 50  0000 C CNN
F 2 "" H 4200 6050 50  0000 C CNN
F 3 "" H 4200 6050 50  0000 C CNN
	1    4200 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EAA136F
P 5500 6050
AR Path="/5EAA136F" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EAA136F" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EAA136F" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EAA136F" Ref="#PWR01010"  Part="1" 
F 0 "#PWR01010" H 5500 5800 50  0001 C CNN
F 1 "GNDD" H 5500 5900 50  0000 C CNN
F 2 "" H 5500 6050 50  0000 C CNN
F 3 "" H 5500 6050 50  0000 C CNN
	1    5500 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 625  4200 925 
Wire Wire Line
	5500 3450 5500 3650
Wire Wire Line
	5500 625  5500 725 
Wire Wire Line
	5400 5550 5500 5550
Connection ~ 5500 5550
Wire Wire Line
	5500 5550 5500 6050
Wire Wire Line
	5400 5250 5500 5250
Connection ~ 5500 5250
Wire Wire Line
	5500 5250 5500 5550
Wire Wire Line
	5400 4950 5500 4950
Connection ~ 5500 4950
Wire Wire Line
	5500 4950 5500 5250
Wire Wire Line
	4300 5050 4200 5050
Connection ~ 4200 5050
Wire Wire Line
	4200 5050 4200 5150
Wire Wire Line
	4300 5150 4200 5150
Connection ~ 4200 5150
Wire Wire Line
	4300 5450 4200 5450
Wire Wire Line
	4200 5150 4200 5350
Connection ~ 4200 5450
Wire Wire Line
	4200 5450 4200 6050
Wire Wire Line
	4300 5350 4200 5350
Connection ~ 4200 5350
Wire Wire Line
	4200 5350 4200 5450
Wire Wire Line
	4300 4650 4200 4650
Connection ~ 4200 4650
Wire Wire Line
	4300 4550 4200 4550
Connection ~ 4200 4550
Wire Wire Line
	4200 4550 4200 4650
Wire Wire Line
	5400 4450 5500 4450
Connection ~ 5500 4450
Wire Wire Line
	5400 4750 5500 4750
Wire Wire Line
	5500 4450 5500 4750
Connection ~ 5500 4750
Wire Wire Line
	5500 4750 5500 4950
Wire Wire Line
	5400 4150 5500 4150
Connection ~ 5500 4150
Wire Wire Line
	5500 4150 5500 4450
Wire Wire Line
	5400 3950 5500 3950
Connection ~ 5500 3950
Wire Wire Line
	5500 3950 5500 4150
Wire Wire Line
	5400 3650 5500 3650
Connection ~ 5500 3650
Wire Wire Line
	5500 3650 5500 3950
Wire Wire Line
	4300 3650 4200 3650
Connection ~ 4200 3650
Wire Wire Line
	4200 3650 4200 3750
Wire Wire Line
	4300 3750 4200 3750
Connection ~ 4200 3750
Wire Wire Line
	4200 3750 4200 3850
Wire Wire Line
	4300 3850 4200 3850
Connection ~ 4200 3850
Wire Wire Line
	4200 3850 4200 4250
Wire Wire Line
	5400 3025 5500 3025
Connection ~ 5500 3025
Wire Wire Line
	5500 3025 5500 3225
Wire Wire Line
	5400 2725 5500 2725
Connection ~ 5500 2725
Wire Wire Line
	5500 2725 5500 3025
Wire Wire Line
	5400 2525 5500 2525
Connection ~ 5500 2525
Wire Wire Line
	5500 2525 5500 2725
Wire Wire Line
	5400 2225 5500 2225
Connection ~ 5500 2225
Wire Wire Line
	5500 2225 5500 2525
Wire Wire Line
	5400 1925 5500 1925
Connection ~ 5500 1925
Wire Wire Line
	5500 1925 5500 2225
Wire Wire Line
	5400 1725 5500 1725
Connection ~ 5500 1725
Wire Wire Line
	5500 1725 5500 1925
Wire Wire Line
	4300 2025 4200 2025
Connection ~ 4200 2025
Wire Wire Line
	4200 2025 4200 2125
Wire Wire Line
	4300 2125 4200 2125
Connection ~ 4200 2125
Wire Wire Line
	4200 2125 4200 2325
Wire Wire Line
	4300 2325 4200 2325
Connection ~ 4200 2325
Wire Wire Line
	4200 2325 4200 2425
Wire Wire Line
	4300 2425 4200 2425
Connection ~ 4200 2425
Wire Wire Line
	4300 2825 4200 2825
Connection ~ 4200 2825
Wire Wire Line
	4200 2825 4200 2925
Wire Wire Line
	4300 2925 4200 2925
Connection ~ 4200 2925
Wire Wire Line
	4200 2925 4200 3225
Wire Wire Line
	4300 1625 4200 1625
Connection ~ 4200 1625
Wire Wire Line
	4200 1625 4200 2025
Wire Wire Line
	4300 1525 4200 1525
Connection ~ 4200 1525
Wire Wire Line
	4200 1525 4200 1625
Wire Wire Line
	4300 1325 4200 1325
Connection ~ 4200 1325
Wire Wire Line
	4200 1325 4200 1525
Wire Wire Line
	4300 1225 4200 1225
Connection ~ 4200 1225
Wire Wire Line
	4200 1225 4200 1325
Wire Wire Line
	4300 1025 4200 1025
Connection ~ 4200 1025
Wire Wire Line
	4200 1025 4200 1225
Wire Wire Line
	4300 925  4200 925 
Connection ~ 4200 925 
Wire Wire Line
	4200 925  4200 1025
Wire Wire Line
	5400 725  5500 725 
Connection ~ 5500 725 
Wire Wire Line
	5500 725  5500 825 
Wire Wire Line
	5400 825  5500 825 
Connection ~ 5500 825 
Wire Wire Line
	5500 825  5500 1125
Wire Wire Line
	5400 1125 5500 1125
Connection ~ 5500 1125
Wire Wire Line
	5500 1125 5500 1425
Wire Wire Line
	5400 1425 5500 1425
Connection ~ 5500 1425
Wire Wire Line
	5500 1425 5500 1725
Wire Wire Line
	4300 4250 4200 4250
Connection ~ 4200 4250
Wire Wire Line
	4200 4250 4200 4350
Wire Wire Line
	4300 4350 4200 4350
Connection ~ 4200 4350
Wire Wire Line
	4200 4350 4200 4550
NoConn ~ 4300 5550
NoConn ~ 4300 5250
NoConn ~ 4300 4050
NoConn ~ 4300 3950
NoConn ~ 4300 3025
NoConn ~ 4300 2625
NoConn ~ 4300 2525
NoConn ~ 4300 2225
NoConn ~ 4300 5650
NoConn ~ 4300 1825
NoConn ~ 4300 4450
NoConn ~ 4300 4750
NoConn ~ 4300 1425
NoConn ~ 4300 1725
NoConn ~ 4300 4850
NoConn ~ 4300 5750
NoConn ~ 4300 5950
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EAA140D
P 4850 1925
AR Path="/5F997D25/5EAA140D" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EAA140D" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EAA140D" Ref="J1003"  Part="1" 
F 0 "J1003" H 4850 650 60  0000 C CNN
F 1 "ERNI-244838" V 4850 1925 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 4750 1925 60  0001 C CNN
F 3 "" H 4750 1925 60  0000 C CNN
	1    4850 1925
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EAA1417
P 4850 4750
AR Path="/5F997D25/5EAA1417" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EAA1417" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EAA1417" Ref="J1008"  Part="1" 
F 0 "J1008" H 4850 3475 60  0000 C CNN
F 1 "ERNI-244838" V 4850 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 4750 4750 60  0001 C CNN
F 3 "" H 4750 4750 60  0000 C CNN
	1    4850 4750
	-1   0    0    -1  
$EndComp
NoConn ~ 5400 1825
NoConn ~ 5400 2625
NoConn ~ 5400 4050
NoConn ~ 5400 4850
NoConn ~ 5400 5650
Wire Wire Line
	5600 3325 5600 3550
Connection ~ 5600 3325
Wire Wire Line
	4100 3325 4100 3550
Wire Wire Line
	4200 3225 4200 3450
Connection ~ 4200 3225
Connection ~ 4200 3450
Wire Wire Line
	4200 3450 4200 3650
Wire Wire Line
	5500 3225 5500 3450
Connection ~ 5500 3225
Connection ~ 5500 3450
Connection ~ 4200 6050
Connection ~ 5500 6050
Wire Wire Line
	7375 3550 7175 3550
Wire Wire Line
	6075 3450 5975 3450
Wire Wire Line
	7175 3450 7275 3450
Wire Wire Line
	5975 6050 6075 6050
Wire Wire Line
	7175 6050 7275 6050
Wire Wire Line
	5975 3225 6075 3225
Wire Wire Line
	7275 3225 7175 3225
Wire Wire Line
	7175 625  7275 625 
Wire Wire Line
	6075 625  5975 625 
Wire Wire Line
	5875 3125 5875 3325
Wire Wire Line
	5875 3325 7375 3325
Wire Wire Line
	7375 3325 7375 3125
Wire Wire Line
	7375 3125 7175 3125
Wire Wire Line
	5875 3550 6075 3550
Wire Wire Line
	5875 3125 6075 3125
Connection ~ 5875 3325
$Comp
L power:GNDD #PWR?
U 1 1 5EAA1487
P 5975 6050
AR Path="/5EAA1487" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EAA1487" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EAA1487" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EAA1487" Ref="#PWR01011"  Part="1" 
F 0 "#PWR01011" H 5975 5800 50  0001 C CNN
F 1 "GNDD" H 5975 5900 50  0000 C CNN
F 2 "" H 5975 6050 50  0000 C CNN
F 3 "" H 5975 6050 50  0000 C CNN
	1    5975 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EAA1491
P 7275 6050
AR Path="/5EAA1491" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EAA1491" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EAA1491" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EAA1491" Ref="#PWR01012"  Part="1" 
F 0 "#PWR01012" H 7275 5800 50  0001 C CNN
F 1 "GNDD" H 7275 5900 50  0000 C CNN
F 2 "" H 7275 6050 50  0000 C CNN
F 3 "" H 7275 6050 50  0000 C CNN
	1    7275 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 625  5975 925 
Wire Wire Line
	7275 3450 7275 3650
Wire Wire Line
	7275 625  7275 725 
Wire Wire Line
	7175 5550 7275 5550
Connection ~ 7275 5550
Wire Wire Line
	7275 5550 7275 6050
Wire Wire Line
	7175 5250 7275 5250
Connection ~ 7275 5250
Wire Wire Line
	7275 5250 7275 5550
Wire Wire Line
	7175 4950 7275 4950
Connection ~ 7275 4950
Wire Wire Line
	7275 4950 7275 5250
Wire Wire Line
	6075 5050 5975 5050
Connection ~ 5975 5050
Wire Wire Line
	5975 5050 5975 5150
Wire Wire Line
	6075 5150 5975 5150
Connection ~ 5975 5150
Wire Wire Line
	6075 5450 5975 5450
Wire Wire Line
	5975 5150 5975 5350
Connection ~ 5975 5450
Wire Wire Line
	5975 5450 5975 6050
Wire Wire Line
	6075 5350 5975 5350
Connection ~ 5975 5350
Wire Wire Line
	5975 5350 5975 5450
Wire Wire Line
	6075 4650 5975 4650
Connection ~ 5975 4650
Wire Wire Line
	5975 4650 5975 5050
Wire Wire Line
	6075 4550 5975 4550
Connection ~ 5975 4550
Wire Wire Line
	5975 4550 5975 4650
Wire Wire Line
	7175 4450 7275 4450
Connection ~ 7275 4450
Wire Wire Line
	7175 4750 7275 4750
Wire Wire Line
	7275 4450 7275 4750
Connection ~ 7275 4750
Wire Wire Line
	7275 4750 7275 4950
Wire Wire Line
	7175 4150 7275 4150
Connection ~ 7275 4150
Wire Wire Line
	7275 4150 7275 4450
Wire Wire Line
	7175 3950 7275 3950
Connection ~ 7275 3950
Wire Wire Line
	7275 3950 7275 4150
Wire Wire Line
	7175 3650 7275 3650
Connection ~ 7275 3650
Wire Wire Line
	7275 3650 7275 3950
Wire Wire Line
	6075 3650 5975 3650
Connection ~ 5975 3650
Wire Wire Line
	5975 3650 5975 3750
Wire Wire Line
	6075 3750 5975 3750
Connection ~ 5975 3750
Wire Wire Line
	5975 3750 5975 3850
Wire Wire Line
	6075 3850 5975 3850
Connection ~ 5975 3850
Wire Wire Line
	5975 3850 5975 4250
Wire Wire Line
	7175 3025 7275 3025
Connection ~ 7275 3025
Wire Wire Line
	7275 3025 7275 3225
Wire Wire Line
	7175 2725 7275 2725
Connection ~ 7275 2725
Wire Wire Line
	7275 2725 7275 3025
Wire Wire Line
	7175 2525 7275 2525
Connection ~ 7275 2525
Wire Wire Line
	7275 2525 7275 2725
Wire Wire Line
	7175 2225 7275 2225
Connection ~ 7275 2225
Wire Wire Line
	7275 2225 7275 2525
Wire Wire Line
	7175 1925 7275 1925
Connection ~ 7275 1925
Wire Wire Line
	7275 1925 7275 2225
Wire Wire Line
	7175 1725 7275 1725
Connection ~ 7275 1725
Wire Wire Line
	7275 1725 7275 1925
Wire Wire Line
	6075 2025 5975 2025
Connection ~ 5975 2025
Wire Wire Line
	5975 2025 5975 2125
Wire Wire Line
	6075 2125 5975 2125
Connection ~ 5975 2125
Wire Wire Line
	5975 2125 5975 2325
Wire Wire Line
	6075 2325 5975 2325
Connection ~ 5975 2325
Wire Wire Line
	5975 2325 5975 2425
Wire Wire Line
	6075 2425 5975 2425
Connection ~ 5975 2425
Wire Wire Line
	5975 2425 5975 2825
Wire Wire Line
	6075 2825 5975 2825
Connection ~ 5975 2825
Wire Wire Line
	5975 2825 5975 2925
Wire Wire Line
	6075 2925 5975 2925
Connection ~ 5975 2925
Wire Wire Line
	5975 2925 5975 3225
Wire Wire Line
	6075 1625 5975 1625
Connection ~ 5975 1625
Wire Wire Line
	5975 1625 5975 2025
Wire Wire Line
	6075 1525 5975 1525
Connection ~ 5975 1525
Wire Wire Line
	5975 1525 5975 1625
Wire Wire Line
	6075 1325 5975 1325
Connection ~ 5975 1325
Wire Wire Line
	5975 1325 5975 1525
Wire Wire Line
	6075 1225 5975 1225
Connection ~ 5975 1225
Wire Wire Line
	5975 1225 5975 1325
Wire Wire Line
	6075 1025 5975 1025
Connection ~ 5975 1025
Wire Wire Line
	5975 1025 5975 1225
Wire Wire Line
	6075 925  5975 925 
Connection ~ 5975 925 
Wire Wire Line
	5975 925  5975 1025
Wire Wire Line
	7175 725  7275 725 
Connection ~ 7275 725 
Wire Wire Line
	7275 725  7275 825 
Wire Wire Line
	7175 825  7275 825 
Connection ~ 7275 825 
Wire Wire Line
	7275 825  7275 1125
Wire Wire Line
	7175 1125 7275 1125
Connection ~ 7275 1125
Wire Wire Line
	7275 1125 7275 1425
Wire Wire Line
	7175 1425 7275 1425
Connection ~ 7275 1425
Wire Wire Line
	7275 1425 7275 1725
Wire Wire Line
	6075 4250 5975 4250
Connection ~ 5975 4250
Wire Wire Line
	5975 4250 5975 4350
Wire Wire Line
	6075 4350 5975 4350
Connection ~ 5975 4350
Wire Wire Line
	5975 4350 5975 4550
NoConn ~ 6075 5550
NoConn ~ 6075 5250
NoConn ~ 6075 4050
NoConn ~ 6075 3950
NoConn ~ 6075 3025
NoConn ~ 6075 2625
NoConn ~ 6075 2525
NoConn ~ 6075 2225
NoConn ~ 6075 5650
NoConn ~ 6075 1825
NoConn ~ 6075 4450
NoConn ~ 6075 4750
NoConn ~ 6075 1425
NoConn ~ 6075 1725
NoConn ~ 6075 4850
NoConn ~ 6075 5750
NoConn ~ 6075 5950
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EAA152C
P 6625 1925
AR Path="/5F997D25/5EAA152C" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EAA152C" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EAA152C" Ref="J1004"  Part="1" 
F 0 "J1004" H 6625 650 60  0000 C CNN
F 1 "ERNI-244838" V 6625 1925 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 6525 1925 60  0001 C CNN
F 3 "" H 6525 1925 60  0000 C CNN
	1    6625 1925
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EAA1536
P 6625 4750
AR Path="/5F997D25/5EAA1536" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EAA1536" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EAA1536" Ref="J1009"  Part="1" 
F 0 "J1009" H 6625 3475 60  0000 C CNN
F 1 "ERNI-244838" V 6625 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 6525 4750 60  0001 C CNN
F 3 "" H 6525 4750 60  0000 C CNN
	1    6625 4750
	-1   0    0    -1  
$EndComp
NoConn ~ 7175 1825
NoConn ~ 7175 2625
NoConn ~ 7175 4050
NoConn ~ 7175 4850
NoConn ~ 7175 5650
Wire Wire Line
	7375 3325 7375 3550
Connection ~ 7375 3325
Wire Wire Line
	5875 3325 5875 3550
Wire Wire Line
	5975 3225 5975 3450
Connection ~ 5975 3225
Connection ~ 5975 3450
Wire Wire Line
	5975 3450 5975 3650
Wire Wire Line
	7275 3225 7275 3450
Connection ~ 7275 3225
Connection ~ 7275 3450
Connection ~ 5975 6050
Connection ~ 7275 6050
NoConn ~ 6075 725 
NoConn ~ 6075 825 
NoConn ~ 7175 925 
NoConn ~ 7175 1025
NoConn ~ 5400 925 
NoConn ~ 5400 1025
NoConn ~ 4300 725 
NoConn ~ 4300 825 
Wire Wire Line
	9175 3550 8975 3550
Wire Wire Line
	7875 3450 7775 3450
Wire Wire Line
	8975 3450 9075 3450
Wire Wire Line
	7775 6050 7875 6050
Wire Wire Line
	8975 6050 9075 6050
Wire Wire Line
	7775 3225 7875 3225
Wire Wire Line
	9075 3225 8975 3225
Wire Wire Line
	8975 625  9075 625 
Wire Wire Line
	7875 625  7775 625 
Wire Wire Line
	7675 3125 7675 3325
Wire Wire Line
	7675 3325 9175 3325
Wire Wire Line
	9175 3325 9175 3125
Wire Wire Line
	9175 3125 8975 3125
Wire Wire Line
	7675 3550 7875 3550
Wire Wire Line
	7675 3125 7875 3125
Connection ~ 7675 3325
$Comp
L power:GNDD #PWR?
U 1 1 5EC346BA
P 7775 6050
AR Path="/5EC346BA" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EC346BA" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EC346BA" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EC346BA" Ref="#PWR01013"  Part="1" 
F 0 "#PWR01013" H 7775 5800 50  0001 C CNN
F 1 "GNDD" H 7775 5900 50  0000 C CNN
F 2 "" H 7775 6050 50  0000 C CNN
F 3 "" H 7775 6050 50  0000 C CNN
	1    7775 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EC346C4
P 9075 6050
AR Path="/5EC346C4" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/5EC346C4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/5EC346C4" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/5EC346C4" Ref="#PWR01014"  Part="1" 
F 0 "#PWR01014" H 9075 5800 50  0001 C CNN
F 1 "GNDD" H 9075 5900 50  0000 C CNN
F 2 "" H 9075 6050 50  0000 C CNN
F 3 "" H 9075 6050 50  0000 C CNN
	1    9075 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7775 625  7775 925 
Wire Wire Line
	9075 3450 9075 3650
Wire Wire Line
	9075 625  9075 725 
Wire Wire Line
	8975 5550 9075 5550
Connection ~ 9075 5550
Wire Wire Line
	9075 5550 9075 6050
Wire Wire Line
	8975 5250 9075 5250
Connection ~ 9075 5250
Wire Wire Line
	9075 5250 9075 5550
Wire Wire Line
	8975 4950 9075 4950
Connection ~ 9075 4950
Wire Wire Line
	9075 4950 9075 5250
Wire Wire Line
	7875 5050 7775 5050
Connection ~ 7775 5050
Wire Wire Line
	7775 5050 7775 5150
Wire Wire Line
	7875 5150 7775 5150
Connection ~ 7775 5150
Wire Wire Line
	7875 5450 7775 5450
Wire Wire Line
	7775 5150 7775 5350
Connection ~ 7775 5450
Wire Wire Line
	7775 5450 7775 6050
Wire Wire Line
	7875 5350 7775 5350
Connection ~ 7775 5350
Wire Wire Line
	7775 5350 7775 5450
Wire Wire Line
	7875 4650 7775 4650
Connection ~ 7775 4650
Wire Wire Line
	7775 4650 7775 5050
Wire Wire Line
	7875 4550 7775 4550
Connection ~ 7775 4550
Wire Wire Line
	7775 4550 7775 4650
Wire Wire Line
	8975 4450 9075 4450
Connection ~ 9075 4450
Wire Wire Line
	8975 4750 9075 4750
Wire Wire Line
	9075 4450 9075 4750
Connection ~ 9075 4750
Wire Wire Line
	9075 4750 9075 4950
Wire Wire Line
	8975 4150 9075 4150
Connection ~ 9075 4150
Wire Wire Line
	9075 4150 9075 4450
Wire Wire Line
	8975 3950 9075 3950
Connection ~ 9075 3950
Wire Wire Line
	9075 3950 9075 4150
Wire Wire Line
	8975 3650 9075 3650
Connection ~ 9075 3650
Wire Wire Line
	9075 3650 9075 3950
Wire Wire Line
	7875 3650 7775 3650
Connection ~ 7775 3650
Wire Wire Line
	7775 3650 7775 3750
Wire Wire Line
	7875 3750 7775 3750
Connection ~ 7775 3750
Wire Wire Line
	7775 3750 7775 3850
Wire Wire Line
	7875 3850 7775 3850
Connection ~ 7775 3850
Wire Wire Line
	7775 3850 7775 4250
Wire Wire Line
	8975 3025 9075 3025
Connection ~ 9075 3025
Wire Wire Line
	9075 3025 9075 3225
Wire Wire Line
	8975 2725 9075 2725
Connection ~ 9075 2725
Wire Wire Line
	9075 2725 9075 3025
Wire Wire Line
	8975 2525 9075 2525
Connection ~ 9075 2525
Wire Wire Line
	9075 2525 9075 2725
Wire Wire Line
	8975 2225 9075 2225
Connection ~ 9075 2225
Wire Wire Line
	9075 2225 9075 2525
Wire Wire Line
	8975 1925 9075 1925
Connection ~ 9075 1925
Wire Wire Line
	9075 1925 9075 2225
Wire Wire Line
	8975 1725 9075 1725
Connection ~ 9075 1725
Wire Wire Line
	9075 1725 9075 1925
Wire Wire Line
	7875 2025 7775 2025
Connection ~ 7775 2025
Wire Wire Line
	7775 2025 7775 2125
Wire Wire Line
	7875 2125 7775 2125
Connection ~ 7775 2125
Wire Wire Line
	7775 2125 7775 2325
Wire Wire Line
	7875 2325 7775 2325
Connection ~ 7775 2325
Wire Wire Line
	7775 2325 7775 2425
Wire Wire Line
	7875 2425 7775 2425
Connection ~ 7775 2425
Wire Wire Line
	7775 2425 7775 2825
Wire Wire Line
	7875 2825 7775 2825
Connection ~ 7775 2825
Wire Wire Line
	7775 2825 7775 2925
Wire Wire Line
	7875 2925 7775 2925
Connection ~ 7775 2925
Wire Wire Line
	7775 2925 7775 3225
Wire Wire Line
	7875 1625 7775 1625
Connection ~ 7775 1625
Wire Wire Line
	7775 1625 7775 2025
Wire Wire Line
	7875 1525 7775 1525
Connection ~ 7775 1525
Wire Wire Line
	7775 1525 7775 1625
Wire Wire Line
	7875 1325 7775 1325
Connection ~ 7775 1325
Wire Wire Line
	7775 1325 7775 1525
Wire Wire Line
	7875 1225 7775 1225
Connection ~ 7775 1225
Wire Wire Line
	7775 1225 7775 1325
Wire Wire Line
	7875 1025 7775 1025
Connection ~ 7775 1025
Wire Wire Line
	7775 1025 7775 1225
Wire Wire Line
	7875 925  7775 925 
Connection ~ 7775 925 
Wire Wire Line
	7775 925  7775 1025
Wire Wire Line
	8975 725  9075 725 
Connection ~ 9075 725 
Wire Wire Line
	9075 725  9075 825 
Wire Wire Line
	8975 825  9075 825 
Connection ~ 9075 825 
Wire Wire Line
	9075 825  9075 1125
Wire Wire Line
	8975 1125 9075 1125
Connection ~ 9075 1125
Wire Wire Line
	9075 1125 9075 1425
Wire Wire Line
	8975 1425 9075 1425
Connection ~ 9075 1425
Wire Wire Line
	9075 1425 9075 1725
Wire Wire Line
	7875 4250 7775 4250
Connection ~ 7775 4250
Wire Wire Line
	7775 4250 7775 4350
Wire Wire Line
	7875 4350 7775 4350
Connection ~ 7775 4350
Wire Wire Line
	7775 4350 7775 4550
NoConn ~ 7875 5550
NoConn ~ 7875 5250
NoConn ~ 7875 4050
NoConn ~ 7875 3950
NoConn ~ 7875 3025
NoConn ~ 7875 2625
NoConn ~ 7875 2525
NoConn ~ 7875 2225
NoConn ~ 7875 5650
NoConn ~ 7875 1825
NoConn ~ 7875 4450
NoConn ~ 7875 4750
NoConn ~ 7875 1425
NoConn ~ 7875 1725
NoConn ~ 7875 4850
NoConn ~ 7875 5750
NoConn ~ 7875 5950
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EC3475F
P 8425 1925
AR Path="/5F997D25/5EC3475F" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EC3475F" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EC3475F" Ref="J1005"  Part="1" 
F 0 "J1005" H 8425 650 60  0000 C CNN
F 1 "ERNI-244838" V 8425 1925 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 8325 1925 60  0001 C CNN
F 3 "" H 8325 1925 60  0000 C CNN
	1    8425 1925
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5EC34769
P 8425 4750
AR Path="/5F997D25/5EC34769" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5EC34769" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5EC34769" Ref="J1010"  Part="1" 
F 0 "J1010" H 8425 3475 60  0000 C CNN
F 1 "ERNI-244838" V 8425 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 8325 4750 60  0001 C CNN
F 3 "" H 8325 4750 60  0000 C CNN
	1    8425 4750
	-1   0    0    -1  
$EndComp
NoConn ~ 8975 1825
NoConn ~ 8975 2625
NoConn ~ 8975 4050
NoConn ~ 8975 4850
NoConn ~ 8975 5650
Wire Wire Line
	9175 3325 9175 3550
Connection ~ 9175 3325
Wire Wire Line
	7675 3325 7675 3550
Wire Wire Line
	7775 3225 7775 3450
Connection ~ 7775 3225
Connection ~ 7775 3450
Wire Wire Line
	7775 3450 7775 3650
Wire Wire Line
	9075 3225 9075 3450
Connection ~ 9075 3225
Connection ~ 9075 3450
Connection ~ 7775 6050
Connection ~ 9075 6050
NoConn ~ 8975 925 
NoConn ~ 8975 1025
NoConn ~ 7875 725 
NoConn ~ 7875 825 
Text HLabel 4725 6950 2    39   BiDi ~ 0
I2C_SCL
Text HLabel 4725 7075 2    39   BiDi ~ 0
I2C_SDA
Wire Wire Line
	4575 6950 4725 6950
Wire Wire Line
	4425 6950 4425 7075
Wire Wire Line
	4425 7075 4725 7075
Text Label 500  725  0    39   ~ 0
I2C_SCL
Text Label 500  825  0    39   ~ 0
I2C_SDA
$Comp
L MDJ_compo:ERNI-244838 J?
U 1 1 5F798223
P 10375 4750
AR Path="/5F997D25/5F798223" Ref="J?"  Part="1" 
AR Path="/5EFE8A79/5F798223" Ref="J?"  Part="1" 
AR Path="/5D2A5AA9/5F798223" Ref="J1011"  Part="1" 
F 0 "J1011" H 10375 3475 60  0000 C CNN
F 1 "ERNI-244838" V 10375 4750 60  0000 C CNN
F 2 "MDJ_mod:ERNI-244838" H 10275 4750 60  0001 C CNN
F 3 "" H 10275 4750 60  0000 C CNN
	1    10375 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9825 3450 9775 3450
Wire Wire Line
	9775 3450 9775 3850
Wire Wire Line
	10925 3450 10975 3450
Wire Wire Line
	10975 3450 10975 3850
$Comp
L power:GNDD #PWR?
U 1 1 638DAE32
P 10975 6100
AR Path="/638DAE32" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/638DAE32" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/638DAE32" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/638DAE32" Ref="#PWR01016"  Part="1" 
F 0 "#PWR01016" H 10975 5850 50  0001 C CNN
F 1 "GNDD" H 10975 5950 50  0000 C CNN
F 2 "" H 10975 6100 50  0000 C CNN
F 3 "" H 10975 6100 50  0000 C CNN
	1    10975 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10925 6050 10975 6050
Connection ~ 10975 6050
Wire Wire Line
	10975 6050 10975 6100
$Comp
L power:GNDD #PWR?
U 1 1 63941254
P 9775 6100
AR Path="/63941254" Ref="#PWR?"  Part="1" 
AR Path="/5F997D25/63941254" Ref="#PWR?"  Part="1" 
AR Path="/5EFE8A79/63941254" Ref="#PWR?"  Part="1" 
AR Path="/5D2A5AA9/63941254" Ref="#PWR01015"  Part="1" 
F 0 "#PWR01015" H 9775 5850 50  0001 C CNN
F 1 "GNDD" H 9775 5950 50  0000 C CNN
F 2 "" H 9775 6100 50  0000 C CNN
F 3 "" H 9775 6100 50  0000 C CNN
	1    9775 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9825 6050 9775 6050
Connection ~ 9775 6050
Wire Wire Line
	9775 6050 9775 6100
Wire Wire Line
	9825 3850 9775 3850
Connection ~ 9775 3850
Wire Wire Line
	9775 3850 9775 3950
Wire Wire Line
	9825 3950 9775 3950
Connection ~ 9775 3950
Wire Wire Line
	9775 3950 9775 4050
Wire Wire Line
	9825 4050 9775 4050
Connection ~ 9775 4050
Wire Wire Line
	9775 4050 9775 4150
Wire Wire Line
	9825 4150 9775 4150
Connection ~ 9775 4150
Wire Wire Line
	9775 4150 9775 4750
Wire Wire Line
	10925 3850 10975 3850
Connection ~ 10975 3850
Wire Wire Line
	10975 3850 10975 3950
Wire Wire Line
	10925 3950 10975 3950
Connection ~ 10975 3950
Wire Wire Line
	10975 3950 10975 4050
Wire Wire Line
	10925 4050 10975 4050
Connection ~ 10975 4050
Wire Wire Line
	10975 4050 10975 4150
Wire Wire Line
	10925 4150 10975 4150
Connection ~ 10975 4150
Wire Wire Line
	10975 4150 10975 4850
Connection ~ 9775 4750
Wire Wire Line
	9775 4750 9775 4950
Wire Wire Line
	9825 4950 9775 4950
Connection ~ 9775 4950
Wire Wire Line
	9775 4950 9775 5150
Wire Wire Line
	9775 4750 9825 4750
Wire Wire Line
	9825 5150 9775 5150
Connection ~ 9775 5150
Wire Wire Line
	9775 5150 9775 5350
Wire Wire Line
	9825 5350 9775 5350
Connection ~ 9775 5350
Wire Wire Line
	9775 5350 9775 5550
Wire Wire Line
	9825 5550 9775 5550
Connection ~ 9775 5550
Wire Wire Line
	9775 5550 9775 5750
Wire Wire Line
	9825 5750 9775 5750
Connection ~ 9775 5750
Wire Wire Line
	9775 5750 9775 5950
Wire Wire Line
	9825 5950 9775 5950
Connection ~ 9775 5950
Wire Wire Line
	9775 5950 9775 6050
Wire Wire Line
	10925 5950 10975 5950
Connection ~ 10975 5950
Wire Wire Line
	10975 5950 10975 6050
Wire Wire Line
	10925 5650 10975 5650
Connection ~ 10975 5650
Wire Wire Line
	10975 5650 10975 5950
Wire Wire Line
	10925 5350 10975 5350
Connection ~ 10975 5350
Wire Wire Line
	10975 5350 10975 5650
Wire Wire Line
	10925 5150 10975 5150
Connection ~ 10975 5150
Wire Wire Line
	10975 5150 10975 5350
Wire Wire Line
	10925 4850 10975 4850
Connection ~ 10975 4850
Wire Wire Line
	10975 4850 10975 5150
Wire Wire Line
	9825 3750 9675 3750
Wire Wire Line
	9675 3750 9675 3650
Wire Wire Line
	9825 3650 9675 3650
Connection ~ 9675 3650
Wire Wire Line
	9675 3650 9675 3550
Wire Wire Line
	9825 3550 9675 3550
Connection ~ 9675 3550
Wire Wire Line
	9675 3550 9675 3325
Wire Wire Line
	10925 3750 11075 3750
Wire Wire Line
	11075 3750 11075 3650
Wire Wire Line
	10925 3550 11075 3550
Connection ~ 11075 3550
Wire Wire Line
	11075 3550 11075 3350
Wire Wire Line
	10925 3650 11075 3650
Connection ~ 11075 3650
Wire Wire Line
	11075 3650 11075 3550
Wire Wire Line
	9825 4250 9675 4250
Wire Wire Line
	9675 4550 9825 4550
Wire Wire Line
	9825 4350 9675 4350
Wire Wire Line
	9675 4250 9675 4350
Connection ~ 9675 4350
Wire Wire Line
	9675 4350 9675 4450
Wire Wire Line
	9825 4450 9675 4450
Connection ~ 9675 4450
Wire Wire Line
	9675 4450 9675 4550
Wire Wire Line
	10925 4250 11075 4250
Wire Wire Line
	11075 4250 11075 4350
Wire Wire Line
	11075 4550 10925 4550
Wire Wire Line
	10925 4450 11075 4450
Connection ~ 11075 4450
Wire Wire Line
	11075 4450 11075 4550
Wire Wire Line
	10925 4350 11075 4350
Connection ~ 11075 4350
Wire Wire Line
	11075 4350 11075 4450
Wire Wire Line
	9825 5050 9600 5050
Text Label 9600 5050 0    39   ~ 0
Pg1
Wire Wire Line
	9825 4850 9600 4850
Text Label 9600 4850 0    39   ~ 0
Pg2
Wire Wire Line
	9825 4650 9600 4650
Text Label 9600 4650 0    39   ~ 0
Pg3
Wire Wire Line
	9825 5850 9600 5850
Text Label 9600 5850 0    39   ~ 0
Pg4
Wire Wire Line
	9825 5650 9600 5650
Text Label 9600 5650 0    39   ~ 0
Pg5
Wire Wire Line
	9825 5450 9600 5450
Text Label 9600 5450 0    39   ~ 0
Pg6
Wire Wire Line
	11150 4650 11025 4650
Text Label 11150 4650 2    39   ~ 0
En2
Wire Wire Line
	11150 4950 11025 4950
Text Label 11150 4950 2    39   ~ 0
En1
NoConn ~ 7875 5850
NoConn ~ 6075 5850
NoConn ~ 4300 5850
NoConn ~ 2525 5850
NoConn ~ 3625 5950
NoConn ~ 5400 5950
NoConn ~ 7175 5950
NoConn ~ 8975 5950
NoConn ~ 8975 5850
NoConn ~ 8975 5750
NoConn ~ 7175 5850
NoConn ~ 7175 5750
NoConn ~ 5400 5750
NoConn ~ 5400 5850
NoConn ~ 3625 5750
NoConn ~ 3625 5850
NoConn ~ 750  5850
Wire Wire Line
	11025 5050 11025 4950
Wire Wire Line
	11025 5050 10925 5050
Connection ~ 11025 4950
Wire Wire Line
	11025 4950 10925 4950
Wire Wire Line
	11025 4750 11025 4650
Wire Wire Line
	11025 4750 10925 4750
Connection ~ 11025 4650
Wire Wire Line
	11025 4650 10925 4650
Connection ~ 4425 6650
Wire Wire Line
	2050 3325 2325 3325
Wire Wire Line
	3825 3325 4100 3325
Wire Wire Line
	5600 3325 5875 3325
Wire Wire Line
	7375 3325 7675 3325
Text Label 2275 3325 2    39   ~ 0
1V2P
Text HLabel 3400 7275 2    39   Output ~ 0
PwupRstb
NoConn ~ 8975 5050
NoConn ~ 8975 5150
NoConn ~ 8975 4250
NoConn ~ 8975 4350
NoConn ~ 5400 5050
NoConn ~ 5400 5150
NoConn ~ 5400 4250
NoConn ~ 5400 4350
NoConn ~ 1850 5850
NoConn ~ 10925 5450
NoConn ~ 10925 5750
NoConn ~ 10925 5550
NoConn ~ 10925 5850
NoConn ~ -2300 3500
Text HLabel 3400 6950 2    39   Output ~ 0
Clk_160_P
Text HLabel 3400 7000 2    39   Output ~ 0
Clk_160_N
NoConn ~ 1850 4350
NoConn ~ 1850 4250
NoConn ~ 1850 2925
NoConn ~ 1850 2825
NoConn ~ 1850 2125
NoConn ~ 1850 2025
NoConn ~ 3625 1225
NoConn ~ 3625 1325
NoConn ~ 4025 2125
NoConn ~ 4025 2025
NoConn ~ 3625 2925
NoConn ~ 3625 2825
Wire Wire Line
	1850 1225 2250 1225
Wire Wire Line
	1850 1325 2250 1325
Text Label 2250 1325 2    39   ~ 0
Clk_160_N
Text Label 2250 1225 2    39   ~ 0
Clk_160_P
Text Label 7575 4350 2    39   ~ 0
Clk2_1280_N
Text Label 7575 4250 2    39   ~ 0
Clk2_1280_P
Wire Wire Line
	7175 4250 7575 4250
Wire Wire Line
	7175 4350 7575 4350
Text Label 9375 1625 2    39   ~ 0
PiN1_out_N0
Text Label 9375 1525 2    39   ~ 0
PiN1_out_P0
Wire Wire Line
	8975 1625 9375 1625
Wire Wire Line
	8975 1525 9375 1525
Wire Wire Line
	7175 5350 7575 5350
Wire Wire Line
	7175 5450 7575 5450
Text Label 7575 5350 2    39   ~ 0
PiN2_out_P1
Text Label 7575 5450 2    39   ~ 0
PiN2_out_N1
Text Label 7575 4650 2    39   ~ 0
PiN2_out_N0
Text Label 7575 4550 2    39   ~ 0
PiN2_out_P0
Wire Wire Line
	7175 4650 7575 4650
Wire Wire Line
	7175 4550 7575 4550
Wire Wire Line
	5400 1525 5800 1525
Wire Wire Line
	5400 1625 5800 1625
Text Label 5800 1525 2    39   ~ 0
PiN2_out_P2
Text Label 5800 1625 2    39   ~ 0
PiN2_out_N2
Wire Wire Line
	5400 2325 5800 2325
Wire Wire Line
	5400 2425 5800 2425
Text Label 5800 2325 2    39   ~ 0
PiN2_out_P3
Text Label 5800 2425 2    39   ~ 0
PiN2_out_N3
Wire Wire Line
	5400 3750 5800 3750
Wire Wire Line
	5400 3850 5800 3850
Text Label 5800 3750 2    39   ~ 0
PiN2_out_P4
Text Label 5800 3850 2    39   ~ 0
PiN2_out_N4
Wire Wire Line
	5400 4550 5800 4550
Wire Wire Line
	5400 4650 5800 4650
Text Label 5800 4550 2    39   ~ 0
PiN2_out_P5
Text Label 5800 4650 2    39   ~ 0
PiN2_out_N5
Wire Wire Line
	5400 5350 5800 5350
Wire Wire Line
	5400 5450 5800 5450
Text Label 5800 5350 2    39   ~ 0
PiN2_out_P6
Text Label 5800 5450 2    39   ~ 0
PiN2_out_N6
Wire Wire Line
	3625 1525 4025 1525
Wire Wire Line
	3625 1625 4025 1625
Text Label 4025 1525 2    39   ~ 0
PiN2_out_P7
Text Label 4025 1625 2    39   ~ 0
PiN2_out_N7
NoConn ~ 7175 1225
NoConn ~ 7175 1325
NoConn ~ 5400 1225
NoConn ~ 5400 1325
NoConn ~ 5400 2025
NoConn ~ 5400 2125
NoConn ~ 5400 2825
NoConn ~ 5400 2925
Text Label 2250 5450 2    39   ~ 0
Dummy_out_N
Text Label 2250 5350 2    39   ~ 0
Dummy_out_P
Wire Wire Line
	1850 5450 2250 5450
Wire Wire Line
	1850 5350 2250 5350
Text HLabel 3200 6650 0    39   Input ~ 0
PiN1_out_P[0..7]
Text HLabel 3200 6700 0    39   Input ~ 0
PiN1_out_N[0..7]
Text HLabel 3400 7350 2    39   Output ~ 0
ReSync_P
Text HLabel 3400 7400 2    39   Output ~ 0
ReSync_N
Text HLabel 3200 7100 0    39   Input ~ 0
Dummy_out_P
Text HLabel 3200 7150 0    39   Input ~ 0
Dummy_out_N
NoConn ~ 7175 2825
NoConn ~ 7175 2925
NoConn ~ 7575 4250
NoConn ~ 7575 4350
NoConn ~ 7175 5050
NoConn ~ 7175 5150
NoConn ~ 9375 1225
NoConn ~ 9375 1325
NoConn ~ 8975 2825
NoConn ~ 8975 2925
NoConn ~ 3625 4250
NoConn ~ 3625 4350
NoConn ~ 3625 5050
NoConn ~ 3625 5150
NoConn ~ 8975 2025
NoConn ~ 8975 2125
Wire Wire Line
	7875 1125 7575 1125
Text Label 7575 1125 0    39   ~ 0
Pll1_Lock0
Wire Wire Line
	7875 1925 7575 1925
Text Label 7575 1925 0    39   ~ 0
Pll1_Lock1
Wire Wire Line
	7875 2725 7575 2725
Text Label 7575 2725 0    39   ~ 0
Pll1_Lock2
NoConn ~ 1850 5950
NoConn ~ 10925 5250
NoConn ~ 9825 5250
NoConn ~ 9600 4650
NoConn ~ 9600 4850
NoConn ~ 9600 5050
NoConn ~ 9600 5850
NoConn ~ 9600 5650
NoConn ~ 9600 5450
Text HLabel 3200 6950 0    39   Input ~ 0
PiN3_out_P[0..7]
Text HLabel 3200 7000 0    39   Input ~ 0
PiN3_out_N[0..7]
$Comp
L MDJ_compo:VccO_FE #PWR01017
U 1 1 618D088B
P 4425 6650
F 0 "#PWR01017" H 4425 6500 50  0001 C CNN
F 1 "VccO_FE" H 4440 6823 50  0000 C CNN
F 2 "" H 4425 6650 50  0000 C CNN
F 3 "" H 4425 6650 50  0000 C CNN
	1    4425 6650
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO_FE #PWR01003
U 1 1 618D3862
P 9675 4250
F 0 "#PWR01003" H 9675 4100 50  0001 C CNN
F 1 "VccO_FE" V 9600 4375 50  0000 C CNN
F 2 "" H 9675 4250 50  0000 C CNN
F 3 "" H 9675 4250 50  0000 C CNN
	1    9675 4250
	1    0    0    -1  
$EndComp
Connection ~ 9675 4250
$Comp
L MDJ_compo:VccO_FE #PWR01004
U 1 1 618D4AC1
P 11075 4250
F 0 "#PWR01004" H 11075 4100 50  0001 C CNN
F 1 "VccO_FE" V 11150 4375 50  0000 C CNN
F 2 "" H 11075 4250 50  0000 C CNN
F 3 "" H 11075 4250 50  0000 C CNN
	1    11075 4250
	1    0    0    -1  
$EndComp
Connection ~ 11075 4250
$Comp
L MDJ_compo:VccO #PWR01001
U 1 1 618D57B8
P 9675 3325
F 0 "#PWR01001" H 9675 3175 50  0001 C CNN
F 1 "VccO" H 9692 3498 50  0000 C CNN
F 2 "" H 9675 3325 50  0000 C CNN
F 3 "" H 9675 3325 50  0000 C CNN
	1    9675 3325
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR01002
U 1 1 618D6184
P 11075 3350
F 0 "#PWR01002" H 11075 3200 50  0001 C CNN
F 1 "VccO" H 11092 3523 50  0000 C CNN
F 2 "" H 11075 3350 50  0000 C CNN
F 3 "" H 11075 3350 50  0000 C CNN
	1    11075 3350
	1    0    0    -1  
$EndComp
Text Label 4025 2425 2    39   ~ 0
PiN3_out_N0
Text Label 4025 2325 2    39   ~ 0
PiN3_out_P0
Wire Wire Line
	3625 2425 4025 2425
Wire Wire Line
	3625 2325 4025 2325
Wire Wire Line
	4200 2425 4200 2825
Text Label 4025 3850 2    39   ~ 0
PiN3_out_N1
Text Label 4025 3750 2    39   ~ 0
PiN3_out_P1
Wire Wire Line
	3625 3850 4025 3850
Wire Wire Line
	3625 3750 4025 3750
Text Label 4025 4650 2    39   ~ 0
PiN3_out_N2
Text Label 4025 4550 2    39   ~ 0
PiN3_out_P2
Wire Wire Line
	3625 4650 4025 4650
Wire Wire Line
	3625 4550 4025 4550
Wire Wire Line
	4200 4650 4200 5050
Text Label 4025 5450 2    39   ~ 0
PiN3_out_N3
Text Label 4025 5350 2    39   ~ 0
PiN3_out_P3
Wire Wire Line
	3625 5450 4025 5450
Wire Wire Line
	3625 5350 4025 5350
Text Label 2250 1525 2    39   ~ 0
PiN3_out_P4
Wire Wire Line
	1850 1625 2250 1625
Wire Wire Line
	1850 1525 2250 1525
Text Label 2250 1625 2    39   ~ 0
PiN3_out_N4
Text Label 2250 2325 2    39   ~ 0
PiN3_out_P5
Wire Wire Line
	1850 2425 2250 2425
Wire Wire Line
	1850 2325 2250 2325
Text Label 2250 2425 2    39   ~ 0
PiN3_out_N5
Text Label 2250 3750 2    39   ~ 0
PiN3_out_P6
Wire Wire Line
	1850 3850 2250 3850
Wire Wire Line
	1850 3750 2250 3750
Text Label 2250 3850 2    39   ~ 0
PiN3_out_N6
Text Label 2250 4550 2    39   ~ 0
PiN3_out_P7
Wire Wire Line
	1850 4650 2250 4650
Wire Wire Line
	1850 4550 2250 4550
Text Label 2250 4650 2    39   ~ 0
PiN3_out_N7
Text Label 4025 2125 2    39   ~ 0
Clk3_1280_N
Text Label 4025 2025 2    39   ~ 0
Clk3_1280_P
Wire Wire Line
	3625 2025 4025 2025
Wire Wire Line
	3625 2125 4025 2125
NoConn ~ 7175 2025
NoConn ~ 7175 2125
Wire Wire Line
	7875 4150 7575 4150
Text Label 7575 4150 0    39   ~ 0
Pll1_Lock3
Wire Wire Line
	7875 4950 7575 4950
Text Label 7575 4950 0    39   ~ 0
Pll1_Lock4
Wire Wire Line
	6075 1125 5775 1125
Text Label 5775 1125 0    39   ~ 0
Pll1_Lock5
Wire Wire Line
	6075 1925 5775 1925
Text Label 5775 1925 0    39   ~ 0
Pll1_Lock6
Wire Wire Line
	6075 2725 5775 2725
Text Label 5775 2725 0    39   ~ 0
Pll1_Lock7
Wire Wire Line
	6075 4150 5775 4150
Text Label 5775 4150 0    39   ~ 0
Pll2_Lock0
Wire Wire Line
	6075 4950 5775 4950
Text Label 5775 4950 0    39   ~ 0
Pll2_Lock1
Wire Wire Line
	4300 1125 4000 1125
Text Label 4000 1125 0    39   ~ 0
Pll2_Lock2
Wire Wire Line
	4300 1925 4000 1925
Text Label 4000 1925 0    39   ~ 0
Pll2_Lock3
Wire Wire Line
	4300 2725 4000 2725
Text Label 4000 2725 0    39   ~ 0
Pll2_Lock4
Wire Wire Line
	4300 4150 4000 4150
Text Label 4000 4150 0    39   ~ 0
Pll2_Lock5
Wire Wire Line
	4300 4950 4000 4950
Text Label 4000 4950 0    39   ~ 0
Pll2_Lock6
Wire Wire Line
	2525 1125 2225 1125
Text Label 2225 1125 0    39   ~ 0
Pll2_Lock7
Wire Wire Line
	2525 1925 2225 1925
Text Label 2225 1925 0    39   ~ 0
Pll3_Lock0
Wire Wire Line
	2525 2725 2225 2725
Text Label 2225 2725 0    39   ~ 0
Pll3_Lock1
Wire Wire Line
	2525 4150 2225 4150
Text Label 2225 4150 0    39   ~ 0
Pll3_Lock2
Wire Wire Line
	2525 4950 2225 4950
Text Label 2225 4950 0    39   ~ 0
Pll3_Lock3
Wire Wire Line
	750  1125 500  1125
Text Label 500  1125 0    39   ~ 0
Pll3_Lock4
Wire Wire Line
	750  1925 500  1925
Text Label 500  1925 0    39   ~ 0
Pll3_Lock5
Wire Wire Line
	750  2725 500  2725
Text Label 500  2725 0    39   ~ 0
Pll3_Lock6
Wire Wire Line
	750  4150 500  4150
Text Label 500  4150 0    39   ~ 0
Pll3_Lock7
Wire Wire Line
	750  4950 500  4950
Text Label 500  4950 0    39   ~ 0
PllD_Lock
Text HLabel 3200 7250 0    39   Input ~ 0
Pll1_Lock[0..7]
Text HLabel 3200 7325 0    39   Input ~ 0
Pll2_Lock[0..7]
Text HLabel 3200 7400 0    39   Input ~ 0
Pll3_Lock[0..7]
Text HLabel 3200 7475 0    39   Input ~ 0
PllD_Lock
NoConn ~ 1850 5050
NoConn ~ 1850 5150
$EndSCHEMATC
