EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 15
Title "ADC schematics"
Date "2021-06-18"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2525 700  1200 1600
U 60CBFE70
F0 "Channel_0" 50
F1 "ADC_channel.sch" 50
F2 "sData_P[0..3]" O R 3725 800 50 
F3 "sData_N[0..3]" O R 3725 875 50 
F4 "Clk_1280_P" I L 2525 800 50 
F5 "Clk_1280_N" I L 2525 875 50 
F6 "CSb" I L 2525 1475 50 
F7 "SDIO" B L 2525 1400 50 
F8 "SCLK" I L 2525 1325 50 
F9 "GPIO[1..2]" O L 2525 1575 50 
F10 "SyncIn_P" I L 2525 975 50 
F11 "SyncIn_N" I L 2525 1050 50 
F12 "SysRef_N" I L 2525 1225 50 
F13 "SysRef_P" I L 2525 1150 50 
F14 "V3P0A" I L 2525 1675 50 
F15 "V2P4A" I L 2525 1750 50 
F16 "V1P3A" I L 2525 1825 50 
F17 "V1P3D" I L 2525 1900 50 
F18 "VccBuff" I L 2525 1975 50 
F19 "VssBuff" I L 2525 2050 50 
$EndSheet
Text HLabel 1250 700  0    50   Input ~ 0
Clk_1280_P[1..3]
Text HLabel 1250 775  0    50   Input ~ 0
Clk_1280_N[1..3]
Text HLabel 1250 875  0    50   Input ~ 0
SyncIn_P[1..3]
Text HLabel 1250 950  0    50   Input ~ 0
SyncIn_N[1..3]
Text HLabel 1250 1050 0    50   Input ~ 0
SysRef_P[1..3]
Text HLabel 1250 1125 0    50   Input ~ 0
SysRef_N[1..3]
Text HLabel 1250 1225 0    50   Input ~ 0
SCLK
Text HLabel 1250 1300 0    50   Input ~ 0
SDIO
Text HLabel 1250 1375 0    50   Input ~ 0
CSb[1..3]
Text HLabel 1250 1475 0    50   Input ~ 0
GPIO1_[1..2]
Text HLabel 1250 1550 0    50   Input ~ 0
GPIO2_[1..2]
Text HLabel 1250 1625 0    50   Input ~ 0
GPIO3_[1..2]
Wire Wire Line
	2525 800  1950 800 
Text Label 1950 800  0    50   ~ 0
Clk_1280_P1
Wire Wire Line
	2525 875  1950 875 
Text Label 1950 875  0    50   ~ 0
Clk_1280_N1
Wire Wire Line
	2525 975  1950 975 
Text Label 1950 975  0    50   ~ 0
SyncIn_P1
Wire Wire Line
	2525 1050 1950 1050
Text Label 1950 1050 0    50   ~ 0
SyncIn_N1
Wire Wire Line
	2525 1150 1950 1150
Text Label 1950 1150 0    50   ~ 0
SysRef_P1
Wire Wire Line
	2525 1225 1950 1225
Text Label 1950 1225 0    50   ~ 0
SysRef_N1
Wire Wire Line
	2525 1325 1950 1325
Text Label 1950 1325 0    50   ~ 0
SCLK
Wire Wire Line
	2525 1400 1950 1400
Text Label 1950 1400 0    50   ~ 0
SDIO
Wire Wire Line
	2525 1475 1950 1475
Text Label 1950 1475 0    50   ~ 0
CSb1
Wire Bus Line
	1950 1575 2525 1575
Text Label 1950 1575 0    50   ~ 0
GPIO1_[1..2]
Wire Wire Line
	2525 1675 1950 1675
Text Label 1950 1675 0    50   ~ 0
V3P0A
Wire Wire Line
	2525 1750 1950 1750
Text Label 1950 1750 0    50   ~ 0
V2P4A
Wire Wire Line
	2525 1825 1950 1825
Text Label 1950 1825 0    50   ~ 0
V1P3A
Wire Wire Line
	2525 1900 1950 1900
Text Label 1950 1900 0    50   ~ 0
V1P3D
Text Label 4300 800  2    50   ~ 0
sData1_P[0..3]
Text Label 4300 875  2    50   ~ 0
sData1_N[0..3]
Wire Bus Line
	4300 800  3725 800 
Wire Bus Line
	3725 875  4300 875 
$Sheet
S 5575 700  1200 1600
U 60D0FB9C
F0 "Channel_1" 50
F1 "ADC_channel.sch" 50
F2 "sData_P[0..3]" O R 6775 800 50 
F3 "sData_N[0..3]" O R 6775 875 50 
F4 "Clk_1280_P" I L 5575 800 50 
F5 "Clk_1280_N" I L 5575 875 50 
F6 "CSb" I L 5575 1475 50 
F7 "SDIO" B L 5575 1400 50 
F8 "SCLK" I L 5575 1325 50 
F9 "GPIO[1..2]" O L 5575 1575 50 
F10 "SyncIn_P" I L 5575 975 50 
F11 "SyncIn_N" I L 5575 1050 50 
F12 "SysRef_N" I L 5575 1225 50 
F13 "SysRef_P" I L 5575 1150 50 
F14 "V3P0A" I L 5575 1675 50 
F15 "V2P4A" I L 5575 1750 50 
F16 "V1P3A" I L 5575 1825 50 
F17 "V1P3D" I L 5575 1900 50 
F18 "VccBuff" I L 5575 1975 50 
F19 "VssBuff" I L 5575 2050 50 
$EndSheet
Wire Wire Line
	5575 800  5000 800 
Text Label 5000 800  0    50   ~ 0
Clk_1280_P2
Wire Wire Line
	5575 875  5000 875 
Text Label 5000 875  0    50   ~ 0
Clk_1280_N2
Wire Wire Line
	5575 975  5000 975 
Text Label 5000 975  0    50   ~ 0
SyncIn_P2
Wire Wire Line
	5575 1050 5000 1050
Text Label 5000 1050 0    50   ~ 0
SyncIn_N2
Wire Wire Line
	5575 1150 5000 1150
Text Label 5000 1150 0    50   ~ 0
SysRef_P2
Wire Wire Line
	5575 1225 5000 1225
Text Label 5000 1225 0    50   ~ 0
SysRef_N2
Wire Wire Line
	5575 1325 5000 1325
Text Label 5000 1325 0    50   ~ 0
SCLK
Wire Wire Line
	5575 1400 5000 1400
Text Label 5000 1400 0    50   ~ 0
SDIO
Wire Wire Line
	5575 1475 5000 1475
Text Label 5000 1475 0    50   ~ 0
CSb2
Wire Bus Line
	5000 1575 5575 1575
Text Label 5000 1575 0    50   ~ 0
GPIO2_[1..2]
Text Label 7350 800  2    50   ~ 0
sData2_P[0..3]
Text Label 7350 875  2    50   ~ 0
sData2_N[0..3]
Wire Bus Line
	7350 800  6775 800 
Wire Bus Line
	6775 875  7350 875 
$Sheet
S 8725 675  1200 1600
U 60D12852
F0 "Channel_2" 50
F1 "ADC_channel.sch" 50
F2 "sData_P[0..3]" O R 9925 775 50 
F3 "sData_N[0..3]" O R 9925 850 50 
F4 "Clk_1280_P" I L 8725 775 50 
F5 "Clk_1280_N" I L 8725 850 50 
F6 "CSb" I L 8725 1450 50 
F7 "SDIO" B L 8725 1375 50 
F8 "SCLK" I L 8725 1300 50 
F9 "GPIO[1..2]" O L 8725 1550 50 
F10 "SyncIn_P" I L 8725 950 50 
F11 "SyncIn_N" I L 8725 1025 50 
F12 "SysRef_N" I L 8725 1200 50 
F13 "SysRef_P" I L 8725 1125 50 
F14 "V3P0A" I L 8725 1650 50 
F15 "V2P4A" I L 8725 1725 50 
F16 "V1P3A" I L 8725 1800 50 
F17 "V1P3D" I L 8725 1875 50 
F18 "VccBuff" I L 8725 1950 50 
F19 "VssBuff" I L 8725 2025 50 
$EndSheet
Wire Wire Line
	8725 775  8150 775 
Text Label 8150 775  0    50   ~ 0
Clk_1280_P3
Wire Wire Line
	8725 850  8150 850 
Text Label 8150 850  0    50   ~ 0
Clk_1280_N3
Wire Wire Line
	8725 950  8150 950 
Text Label 8150 950  0    50   ~ 0
SyncIn_P3
Wire Wire Line
	8725 1025 8150 1025
Text Label 8150 1025 0    50   ~ 0
SyncIn_N3
Wire Wire Line
	8725 1125 8150 1125
Text Label 8150 1125 0    50   ~ 0
SysRef_P3
Wire Wire Line
	8725 1200 8150 1200
Text Label 8150 1200 0    50   ~ 0
SysRef_N3
Wire Wire Line
	8725 1300 8150 1300
Text Label 8150 1300 0    50   ~ 0
SCLK
Wire Wire Line
	8725 1375 8150 1375
Text Label 8150 1375 0    50   ~ 0
SDIO
Wire Wire Line
	8725 1450 8150 1450
Text Label 8150 1450 0    50   ~ 0
CSb3
Wire Bus Line
	8150 1550 8725 1550
Text Label 8150 1550 0    50   ~ 0
GPIO3_[1..2]
Text Label 10500 775  2    50   ~ 0
sData3_P[0..3]
Text Label 10500 850  2    50   ~ 0
sData3_N[0..3]
Wire Bus Line
	10500 775  9925 775 
Wire Bus Line
	9925 850  10500 850 
Text HLabel 1250 1725 2    50   Output ~ 0
sData1_P[0..3]
Text HLabel 1250 1800 2    50   Output ~ 0
sData1_N[0..3]
Text HLabel 1250 1875 2    50   Output ~ 0
sData2_P[0..3]
Text HLabel 1250 1950 2    50   Output ~ 0
sData2_N[0..3]
Text HLabel 1250 2025 2    50   Output ~ 0
sData3_P[0..3]
Text HLabel 1250 2100 2    50   Output ~ 0
sData3_N[0..3]
Wire Wire Line
	5575 1675 5000 1675
Text Label 5000 1675 0    50   ~ 0
V3P0A
Wire Wire Line
	5575 1750 5000 1750
Text Label 5000 1750 0    50   ~ 0
V2P4A
Wire Wire Line
	5575 1825 5000 1825
Text Label 5000 1825 0    50   ~ 0
V1P3A
Wire Wire Line
	5575 1900 5000 1900
Text Label 5000 1900 0    50   ~ 0
V1P3D
Wire Wire Line
	8725 1650 8150 1650
Text Label 8150 1650 0    50   ~ 0
V3P0A
Wire Wire Line
	8725 1725 8150 1725
Text Label 8150 1725 0    50   ~ 0
V2P4A
Wire Wire Line
	8725 1800 8150 1800
Text Label 8150 1800 0    50   ~ 0
V1P3A
Wire Wire Line
	8725 1875 8150 1875
Text Label 8150 1875 0    50   ~ 0
V1P3D
Text Label 5425 3250 2    50   ~ 0
V1P3A
Text Label 4875 3700 2    50   ~ 0
V1P3D
Text Label 4350 4150 2    50   ~ 0
V3P0A
Text Label 3825 4600 2    50   ~ 0
V2P4A
$Comp
L MDJ_compo:LTM4644 U?
U 1 1 6121C847
P 2525 4075
AR Path="/5787D549/6121C847" Ref="U?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C847" Ref="U?"  Part="1" 
AR Path="/57059335/6121C847" Ref="U601"  Part="1" 
F 0 "U601" H 2525 5378 60  0000 C CNN
F 1 "LTM4644" H 2525 5272 60  0000 C CNN
F 2 "MDJ_mod:BGA-77-9.0x15.0x5.21mm_Layout7x11_P1.27mm" H 2525 4075 60  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/LTM4644.pdf" H 2525 4075 60  0001 C CNN
	1    2525 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3175 1550 3250
Wire Wire Line
	1550 3250 1625 3250
Wire Wire Line
	1550 3700 1625 3700
Connection ~ 1550 3250
Wire Wire Line
	1550 3700 1550 3775
Wire Wire Line
	1550 4150 1625 4150
Connection ~ 1550 3700
Wire Wire Line
	1550 4150 1550 4225
Wire Wire Line
	1550 4600 1625 4600
Connection ~ 1550 4150
Wire Wire Line
	1625 3325 1550 3325
Wire Wire Line
	1550 3250 1550 3325
Connection ~ 1550 3325
Wire Wire Line
	1550 3325 1550 3400
Wire Wire Line
	1625 3775 1550 3775
Connection ~ 1550 3775
Wire Wire Line
	1550 3775 1550 3850
Wire Wire Line
	1625 4225 1550 4225
Connection ~ 1550 4225
Wire Wire Line
	1550 4225 1550 4300
Wire Wire Line
	1625 4675 1550 4675
Wire Wire Line
	1550 4675 1550 4600
Connection ~ 1550 4600
$Comp
L power:GNDD #PWR?
U 1 1 6121C86A
P 2600 5175
AR Path="/5787D549/6121C86A" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C86A" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C86A" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C86A" Ref="#PWR0606"  Part="1" 
F 0 "#PWR0606" H 2600 4925 50  0001 C CNN
F 1 "GNDD" H 2600 5025 50  0000 C CNN
F 2 "" H 2600 5175 50  0000 C CNN
F 3 "" H 2600 5175 50  0000 C CNN
	1    2600 5175
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6121C870
P 5225 3625
AR Path="/5787D549/6121C870" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C870" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C870" Ref="C?"  Part="1" 
AR Path="/57059335/6121C870" Ref="C602"  Part="1" 
F 0 "C602" V 5175 3500 50  0000 C CNN
F 1 "100n" V 5275 3475 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5225 3625 50  0001 C CNN
F 3 "" H 5225 3625 50  0000 C CNN
	1    5225 3625
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6121C876
P 5425 3475
AR Path="/59140D2B/6121C876" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C876" Ref="R?"  Part="1" 
AR Path="/57059335/6121C876" Ref="R601"  Part="1" 
AR Path="/5787D549/6121C876" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C876" Ref="R?"  Part="1" 
F 0 "R601" V 5500 3475 50  0000 C CNN
F 1 "100k" V 5425 3475 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5355 3475 50  0001 C CNN
F 3 "" H 5425 3475 50  0000 C CNN
	1    5425 3475
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6121C87C
P 5425 3775
AR Path="/59140D2B/6121C87C" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C87C" Ref="R?"  Part="1" 
AR Path="/57059335/6121C87C" Ref="R602"  Part="1" 
AR Path="/5787D549/6121C87C" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C87C" Ref="R?"  Part="1" 
F 0 "R602" V 5500 3775 50  0000 C CNN
F 1 "3,48k" V 5425 3775 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5355 3775 50  0001 C CNN
F 3 "" H 5425 3775 50  0000 C CNN
	1    5425 3775
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 6121C882
P 5425 3925
AR Path="/5787D549/6121C882" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C882" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C882" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C882" Ref="#PWR0602"  Part="1" 
F 0 "#PWR0602" H 5425 3675 50  0001 C CNN
F 1 "GNDD" H 5425 3775 50  0000 C CNN
F 2 "" H 5425 3925 50  0000 C CNN
F 3 "" H 5425 3925 50  0000 C CNN
	1    5425 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 3775 5225 3925
Wire Wire Line
	5225 3925 5425 3925
Connection ~ 5425 3925
Wire Wire Line
	1625 4825 1625 4900
Wire Wire Line
	1625 4375 1625 4450
Wire Wire Line
	1625 3925 1625 4000
Wire Wire Line
	1625 3475 1625 3550
Wire Wire Line
	3425 3325 5425 3325
Wire Wire Line
	3425 3475 5225 3475
$Comp
L Device:R R?
U 1 1 6121C891
P 4350 4375
AR Path="/59140D2B/6121C891" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C891" Ref="R?"  Part="1" 
AR Path="/57059335/6121C891" Ref="R604"  Part="1" 
AR Path="/5787D549/6121C891" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C891" Ref="R?"  Part="1" 
F 0 "R604" V 4425 4375 50  0000 C CNN
F 1 "30k" V 4350 4375 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4280 4375 50  0001 C CNN
F 3 "" H 4350 4375 50  0000 C CNN
	1    4350 4375
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6121C897
P 4350 4675
AR Path="/59140D2B/6121C897" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C897" Ref="R?"  Part="1" 
AR Path="/57059335/6121C897" Ref="R605"  Part="1" 
AR Path="/5787D549/6121C897" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C897" Ref="R?"  Part="1" 
F 0 "R605" V 4425 4675 50  0000 C CNN
F 1 "200" V 4350 4675 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4280 4675 50  0001 C CNN
F 3 "" H 4350 4675 50  0000 C CNN
	1    4350 4675
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 6121C89D
P 4875 4375
AR Path="/5787D549/6121C89D" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C89D" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C89D" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C89D" Ref="#PWR0603"  Part="1" 
F 0 "#PWR0603" H 4875 4125 50  0001 C CNN
F 1 "GNDD" H 4875 4225 50  0000 C CNN
F 2 "" H 4875 4375 50  0000 C CNN
F 3 "" H 4875 4375 50  0000 C CNN
	1    4875 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 3775 4875 3775
$Comp
L Device:C C?
U 1 1 6121C8A4
P 4675 4075
AR Path="/5787D549/6121C8A4" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8A4" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8A4" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8A4" Ref="C604"  Part="1" 
F 0 "C604" V 4625 3950 50  0000 C CNN
F 1 "100n" V 4725 3925 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4675 4075 50  0001 C CNN
F 3 "" H 4675 4075 50  0000 C CNN
	1    4675 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 3925 4675 3925
Wire Wire Line
	4675 4225 4675 4375
Wire Wire Line
	4675 4375 4875 4375
$Comp
L power:GNDD #PWR?
U 1 1 6121C8AD
P 4350 4825
AR Path="/5787D549/6121C8AD" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C8AD" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8AD" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C8AD" Ref="#PWR0604"  Part="1" 
F 0 "#PWR0604" H 4350 4575 50  0001 C CNN
F 1 "GNDD" H 4350 4675 50  0000 C CNN
F 2 "" H 4350 4825 50  0000 C CNN
F 3 "" H 4350 4825 50  0000 C CNN
	1    4350 4825
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6121C8B3
P 4150 4525
AR Path="/5787D549/6121C8B3" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8B3" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8B3" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8B3" Ref="C606"  Part="1" 
F 0 "C606" V 4100 4400 50  0000 C CNN
F 1 "100n" V 4200 4375 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4150 4525 50  0001 C CNN
F 3 "" H 4150 4525 50  0000 C CNN
	1    4150 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4825 4350 4825
Wire Wire Line
	3425 4225 4350 4225
Wire Wire Line
	3425 4375 4150 4375
$Comp
L Device:R R?
U 1 1 6121C8BC
P 4875 3925
AR Path="/59140D2B/6121C8BC" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C8BC" Ref="R?"  Part="1" 
AR Path="/57059335/6121C8BC" Ref="R603"  Part="1" 
AR Path="/5787D549/6121C8BC" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8BC" Ref="R?"  Part="1" 
F 0 "R603" V 4950 3925 50  0000 C CNN
F 1 "90.9k" V 4875 3925 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4805 3925 50  0001 C CNN
F 3 "" H 4875 3925 50  0000 C CNN
	1    4875 3925
	-1   0    0    1   
$EndComp
Wire Wire Line
	4875 4075 4875 4375
Connection ~ 4875 4375
Connection ~ 4350 4825
Wire Wire Line
	4150 4675 4150 4825
$Comp
L Device:R R?
U 1 1 6121C8C6
P 3825 4825
AR Path="/59140D2B/6121C8C6" Ref="R?"  Part="1" 
AR Path="/5B5208C5/6121C8C6" Ref="R?"  Part="1" 
AR Path="/57059335/6121C8C6" Ref="R606"  Part="1" 
AR Path="/5787D549/6121C8C6" Ref="R?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8C6" Ref="R?"  Part="1" 
F 0 "R606" V 3900 4825 50  0000 C CNN
F 1 "90.9k" V 3825 4825 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3755 4825 50  0001 C CNN
F 3 "" H 3825 4825 50  0000 C CNN
	1    3825 4825
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 6121C8CC
P 3625 4975
AR Path="/5787D549/6121C8CC" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8CC" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8CC" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8CC" Ref="C608"  Part="1" 
F 0 "C608" V 3575 4850 50  0000 C CNN
F 1 "100n" V 3675 4825 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3625 4975 50  0001 C CNN
F 3 "" H 3625 4975 50  0000 C CNN
	1    3625 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 4675 3825 4675
Wire Wire Line
	3425 4825 3625 4825
$Comp
L power:GNDD #PWR?
U 1 1 6121C8D4
P 3825 5125
AR Path="/5787D549/6121C8D4" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C8D4" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8D4" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C8D4" Ref="#PWR0605"  Part="1" 
F 0 "#PWR0605" H 3825 4875 50  0001 C CNN
F 1 "GNDD" H 3825 4975 50  0000 C CNN
F 2 "" H 3825 5125 50  0000 C CNN
F 3 "" H 3825 5125 50  0000 C CNN
	1    3825 5125
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 4975 3825 5125
Wire Wire Line
	3825 5125 3625 5125
Connection ~ 3825 5125
$Comp
L Device:C C?
U 1 1 6121C8DF
P 5050 3850
AR Path="/5787D549/6121C8DF" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8DF" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8DF" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8DF" Ref="C603"  Part="1" 
F 0 "C603" H 5025 3925 50  0000 C CNN
F 1 "47u" H 5075 3775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5050 3850 50  0001 C CNN
F 3 "" H 5050 3850 50  0000 C CNN
	1    5050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4000 5050 4375
Wire Wire Line
	5050 4375 4875 4375
Wire Wire Line
	3975 4900 3975 5125
Wire Wire Line
	3975 5125 3825 5125
$Comp
L Device:C C?
U 1 1 6121C8EF
P 4500 4300
AR Path="/5787D549/6121C8EF" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8EF" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8EF" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8EF" Ref="C605"  Part="1" 
F 0 "C605" H 4475 4375 50  0000 C CNN
F 1 "47u" H 4525 4225 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4500 4300 50  0001 C CNN
F 3 "" H 4500 4300 50  0000 C CNN
	1    4500 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4450 4500 4825
Wire Wire Line
	4500 4825 4350 4825
$Comp
L Device:C C?
U 1 1 6121C8F7
P 5600 3400
AR Path="/5787D549/6121C8F7" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8F7" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8F7" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8F7" Ref="C601"  Part="1" 
F 0 "C601" H 5575 3475 50  0000 C CNN
F 1 "47u" H 5625 3325 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5600 3400 50  0001 C CNN
F 3 "" H 5600 3400 50  0000 C CNN
	1    5600 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3550 5600 3925
Wire Wire Line
	5600 3925 5425 3925
$Comp
L power:+5V #PWR?
U 1 1 6121C8FF
P 1550 3175
AR Path="/57059335/5E8E50C4/6121C8FF" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C8FF" Ref="#PWR0601"  Part="1" 
F 0 "#PWR0601" H 1550 3025 50  0001 C CNN
F 1 "+5V" H 1565 3348 50  0000 C CNN
F 2 "" H 1550 3175 50  0001 C CNN
F 3 "" H 1550 3175 50  0001 C CNN
	1    1550 3175
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6121C905
P 1350 5125
AR Path="/5787D549/6121C905" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C905" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C905" Ref="C?"  Part="1" 
AR Path="/57059335/6121C905" Ref="C611"  Part="1" 
F 0 "C611" H 1325 5200 50  0000 C CNN
F 1 "10u" H 1375 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1350 5125 50  0001 C CNN
F 3 "" H 1350 5125 50  0000 C CNN
	1    1350 5125
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6121C90B
P 1550 5125
AR Path="/5787D549/6121C90B" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C90B" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C90B" Ref="C?"  Part="1" 
AR Path="/57059335/6121C90B" Ref="C612"  Part="1" 
F 0 "C612" H 1525 5200 50  0000 C CNN
F 1 "10u" H 1575 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1550 5125 50  0001 C CNN
F 3 "" H 1550 5125 50  0000 C CNN
	1    1550 5125
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 6121C911
P 1350 5275
AR Path="/5787D549/6121C911" Ref="#PWR?"  Part="1" 
AR Path="/572C0D81/6121C911" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C911" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6121C911" Ref="#PWR0607"  Part="1" 
F 0 "#PWR0607" H 1350 5025 50  0001 C CNN
F 1 "GNDD" H 1350 5150 50  0000 C CNN
F 2 "" H 1350 5275 50  0000 C CNN
F 3 "" H 1350 5275 50  0000 C CNN
	1    1350 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5275 1350 5275
$Comp
L Device:C C?
U 1 1 6121C918
P 950 5125
AR Path="/5787D549/6121C918" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C918" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C918" Ref="C?"  Part="1" 
AR Path="/57059335/6121C918" Ref="C609"  Part="1" 
F 0 "C609" H 925 5200 50  0000 C CNN
F 1 "10u" H 975 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 950 5125 50  0001 C CNN
F 3 "" H 950 5125 50  0000 C CNN
	1    950  5125
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6121C91E
P 1150 5125
AR Path="/5787D549/6121C91E" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C91E" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C91E" Ref="C?"  Part="1" 
AR Path="/57059335/6121C91E" Ref="C610"  Part="1" 
F 0 "C610" H 1125 5200 50  0000 C CNN
F 1 "10u" H 1175 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1150 5125 50  0001 C CNN
F 3 "" H 1150 5125 50  0000 C CNN
	1    1150 5125
	1    0    0    -1  
$EndComp
Connection ~ 1350 5275
Wire Wire Line
	1550 4675 1550 4750
Connection ~ 1550 4675
Wire Wire Line
	1550 4975 1350 4975
Connection ~ 1550 4975
Wire Wire Line
	1350 4975 1150 4975
Connection ~ 1350 4975
Wire Wire Line
	1150 4975 950  4975
Connection ~ 1150 4975
Wire Wire Line
	950  5275 1150 5275
Wire Wire Line
	1150 5275 1350 5275
Connection ~ 1150 5275
Wire Wire Line
	3425 3250 5600 3250
Wire Wire Line
	3425 3700 5050 3700
Wire Wire Line
	3425 4150 4500 4150
Wire Wire Line
	3425 4600 3975 4600
$Comp
L power:+5V #PWR?
U 1 1 612292D6
P 800 6200
AR Path="/57059335/5E8E50C4/612292D6" Ref="#PWR?"  Part="1" 
AR Path="/57059335/612292D6" Ref="#PWR0608"  Part="1" 
F 0 "#PWR0608" H 800 6050 50  0001 C CNN
F 1 "+5V" H 815 6373 50  0000 C CNN
F 2 "" H 800 6200 50  0001 C CNN
F 3 "" H 800 6200 50  0001 C CNN
	1    800  6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5775 6800 5625 6800
Wire Wire Line
	5775 6500 5775 6800
Wire Wire Line
	5775 6200 6250 6200
Connection ~ 5775 6200
$Comp
L Device:C C?
U 1 1 612292E0
P 5775 6350
AR Path="/59140D2B/612292E0" Ref="C?"  Part="1" 
AR Path="/5B5208C5/612292E0" Ref="C?"  Part="1" 
AR Path="/57059335/612292E0" Ref="C615"  Part="1" 
AR Path="/57059335/5E8E50C4/612292E0" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612292E0" Ref="C?"  Part="1" 
F 0 "C615" H 5725 6425 50  0000 L CNN
F 1 "10u" H 5725 6275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5813 6200 50  0001 C CNN
F 3 "" H 5775 6350 50  0000 C CNN
	1    5775 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 612292E6
P 3450 6300
AR Path="/59140D2B/612292E6" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/612292E6" Ref="#PWR?"  Part="1" 
AR Path="/57059335/612292E6" Ref="#PWR0609"  Part="1" 
AR Path="/57059335/5E8E50C4/612292E6" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612292E6" Ref="#PWR?"  Part="1" 
F 0 "#PWR0609" H 3450 6050 50  0001 C CNN
F 1 "GNDA" H 3450 6200 39  0000 C CNN
F 2 "" H 3450 6300 50  0000 C CNN
F 3 "" H 3450 6300 50  0000 C CNN
	1    3450 6300
	1    0    0    -1  
$EndComp
Text Notes 5825 6175 2    50   ~ 0
-3.0V
$Comp
L power:GNDA #PWR?
U 1 1 612292EE
P 3900 7000
AR Path="/59140D2B/612292EE" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/612292EE" Ref="#PWR?"  Part="1" 
AR Path="/57059335/612292EE" Ref="#PWR0615"  Part="1" 
AR Path="/57059335/5E8E50C4/612292EE" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612292EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR0615" H 3900 6750 50  0001 C CNN
F 1 "GNDA" H 3900 6850 50  0000 C CNN
F 2 "" H 3900 7000 50  0000 C CNN
F 3 "" H 3900 7000 50  0000 C CNN
	1    3900 7000
	1    0    0    -1  
$EndComp
Text Notes 2575 6200 2    50   ~ 0
4.5V
$Comp
L power:GNDA #PWR?
U 1 1 612292F6
P 5625 6800
AR Path="/59140D2B/612292F6" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/612292F6" Ref="#PWR?"  Part="1" 
AR Path="/57059335/612292F6" Ref="#PWR0613"  Part="1" 
AR Path="/57059335/5E8E50C4/612292F6" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612292F6" Ref="#PWR?"  Part="1" 
F 0 "#PWR0613" H 5625 6550 50  0001 C CNN
F 1 "GNDA" H 5625 6650 50  0000 C CNN
F 2 "" H 5625 6800 50  0000 C CNN
F 3 "" H 5625 6800 50  0000 C CNN
	1    5625 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 6500 5625 6500
Wire Wire Line
	5500 6300 5500 6500
Connection ~ 5625 6800
$Comp
L Device:R R610
U 1 1 612292FF
P 5625 6650
AR Path="/57059335/612292FF" Ref="R610"  Part="1" 
AR Path="/57059335/5E8E50C4/612292FF" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612292FF" Ref="R?"  Part="1" 
F 0 "R610" V 5700 6675 50  0000 C CNN
F 1 "1k" V 5625 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5555 6650 50  0001 C CNN
F 3 "" H 5625 6650 50  0000 C CNN
	1    5625 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5625 6200 5775 6200
Wire Wire Line
	5500 6200 5625 6200
Connection ~ 5625 6200
Connection ~ 5625 6500
$Comp
L Device:R R608
U 1 1 61229309
P 5625 6350
AR Path="/57059335/61229309" Ref="R608"  Part="1" 
AR Path="/57059335/5E8E50C4/61229309" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229309" Ref="R?"  Part="1" 
F 0 "R608" V 5700 6375 50  0000 C CNN
F 1 "1.5k" V 5625 6350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5555 6350 50  0001 C CNN
F 3 "" H 5625 6350 50  0000 C CNN
	1    5625 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 6200 4700 6400
$Comp
L power:GNDA #PWR?
U 1 1 61229310
P 5100 6600
AR Path="/59140D2B/61229310" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/61229310" Ref="#PWR?"  Part="1" 
AR Path="/57059335/61229310" Ref="#PWR0612"  Part="1" 
AR Path="/57059335/5E8E50C4/61229310" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229310" Ref="#PWR?"  Part="1" 
F 0 "#PWR0612" H 5100 6350 50  0001 C CNN
F 1 "GNDA" H 5100 6450 50  0000 C CNN
F 2 "" H 5100 6600 50  0000 C CNN
F 3 "" H 5100 6600 50  0000 C CNN
	1    5100 6600
	1    0    0    -1  
$EndComp
Connection ~ 4700 6200
$Comp
L Regulator_Linear:ADP7182AUJZ U602
U 1 1 61229317
P 5100 6300
AR Path="/57059335/61229317" Ref="U602"  Part="1" 
AR Path="/57059335/5E8E50C4/61229317" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229317" Ref="U?"  Part="1" 
F 0 "U602" H 5100 5933 50  0000 C CNN
F 1 "ADP7182AUJZ" H 5100 6024 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 5100 5900 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADP7182.pdf" H 5100 5800 50  0001 C CNN
	1    5100 6300
	1    0    0    1   
$EndComp
Text Label 2775 6200 0    50   ~ 0
VccBuff
$Comp
L power:GNDA #PWR?
U 1 1 6122931E
P 4525 6500
AR Path="/59140D2B/6122931E" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/6122931E" Ref="#PWR?"  Part="1" 
AR Path="/57059335/6122931E" Ref="#PWR0611"  Part="1" 
AR Path="/57059335/5E8E50C4/6122931E" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/6122931E" Ref="#PWR?"  Part="1" 
F 0 "#PWR0611" H 4525 6250 50  0001 C CNN
F 1 "GNDA" H 4525 6350 50  0000 C CNN
F 2 "" H 4525 6500 50  0000 C CNN
F 3 "" H 4525 6500 50  0000 C CNN
	1    4525 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4375 6500 4300 6500
$Comp
L Device:R R607
U 1 1 61229325
P 4375 6350
AR Path="/57059335/61229325" Ref="R607"  Part="1" 
AR Path="/57059335/5E8E50C4/61229325" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229325" Ref="R?"  Part="1" 
F 0 "R607" V 4450 6375 50  0000 C CNN
F 1 "19k" V 4375 6350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4305 6350 50  0001 C CNN
F 3 "" H 4375 6350 50  0000 C CNN
	1    4375 6350
	-1   0    0    1   
$EndComp
Connection ~ 4375 6200
Wire Wire Line
	4300 6200 4375 6200
Wire Wire Line
	4525 6200 4700 6200
Wire Wire Line
	4375 6200 4525 6200
Connection ~ 4525 6200
$Comp
L Device:C C?
U 1 1 61229330
P 4525 6350
AR Path="/59140D2B/61229330" Ref="C?"  Part="1" 
AR Path="/5B5208C5/61229330" Ref="C?"  Part="1" 
AR Path="/57059335/61229330" Ref="C614"  Part="1" 
AR Path="/57059335/5E8E50C4/61229330" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229330" Ref="C?"  Part="1" 
F 0 "C614" H 4475 6425 50  0000 L CNN
F 1 "47u" H 4475 6275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4563 6200 50  0001 C CNN
F 3 "" H 4525 6350 50  0000 C CNN
	1    4525 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6750 3500 6700
Wire Wire Line
	3350 6750 3500 6750
Wire Wire Line
	3500 6450 3500 6500
Wire Wire Line
	3350 6450 3500 6450
$Comp
L Device:C C?
U 1 1 6122933A
P 3350 6600
AR Path="/59140D2B/6122933A" Ref="C?"  Part="1" 
AR Path="/5B5208C5/6122933A" Ref="C?"  Part="1" 
AR Path="/57059335/6122933A" Ref="C618"  Part="1" 
AR Path="/57059335/5E8E50C4/6122933A" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/6122933A" Ref="C?"  Part="1" 
F 0 "C618" H 3300 6675 50  0000 L CNN
F 1 "47u" H 3300 6525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3388 6450 50  0001 C CNN
F 3 "" H 3350 6600 50  0000 C CNN
	1    3350 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6300 3500 6300
$Comp
L power:GNDD #PWR0610
U 1 1 61229341
P 3175 6500
AR Path="/57059335/61229341" Ref="#PWR0610"  Part="1" 
AR Path="/57059335/5E8E50C4/61229341" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229341" Ref="#PWR?"  Part="1" 
F 0 "#PWR0610" H 3175 6250 50  0001 C CNN
F 1 "GNDD" H 3179 6345 50  0000 C CNN
F 2 "" H 3175 6500 50  0001 C CNN
F 3 "" H 3175 6500 50  0001 C CNN
	1    3175 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 6200 3500 6200
Connection ~ 3175 6200
$Comp
L Device:C C?
U 1 1 6122934A
P 3175 6350
AR Path="/59140D2B/6122934A" Ref="C?"  Part="1" 
AR Path="/5B5208C5/6122934A" Ref="C?"  Part="1" 
AR Path="/57059335/6122934A" Ref="C613"  Part="1" 
AR Path="/57059335/5E8E50C4/6122934A" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/6122934A" Ref="C?"  Part="1" 
F 0 "C613" H 3125 6425 50  0000 L CNN
F 1 "47u" H 3125 6275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3213 6200 50  0001 C CNN
F 3 "" H 3175 6350 50  0000 C CNN
	1    3175 6350
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:ADP3605 U604
U 1 1 61229350
P 3900 6500
AR Path="/57059335/61229350" Ref="U604"  Part="1" 
AR Path="/57059335/5E8E50C4/61229350" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229350" Ref="U?"  Part="1" 
F 0 "U604" H 3900 7067 50  0000 C CNN
F 1 "ADP3605" H 3900 6976 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4000 6400 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADP3605.pdf" H 4000 6400 50  0001 C CNN
	1    3900 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2025 6900 2025 7000
Wire Wire Line
	2425 6700 2425 7000
Connection ~ 2425 7000
Wire Wire Line
	2625 7000 2425 7000
Wire Wire Line
	2625 6700 2625 7000
Wire Wire Line
	2625 6200 2625 6400
Wire Wire Line
	2100 6700 2275 6700
Wire Wire Line
	2100 6550 2100 6700
Wire Wire Line
	2025 7000 2275 7000
Wire Wire Line
	2425 7000 2275 7000
Connection ~ 2275 7000
$Comp
L Device:R R611
U 1 1 61229361
P 2275 6850
AR Path="/57059335/61229361" Ref="R611"  Part="1" 
AR Path="/57059335/5E8E50C4/61229361" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229361" Ref="R?"  Part="1" 
F 0 "R611" V 2350 6875 50  0000 C CNN
F 1 "1k" V 2275 6850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2205 6850 50  0001 C CNN
F 3 "" H 2275 6850 50  0000 C CNN
	1    2275 6850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2275 6400 2100 6400
Connection ~ 2275 6700
$Comp
L Device:R R609
U 1 1 61229369
P 2275 6550
AR Path="/57059335/61229369" Ref="R609"  Part="1" 
AR Path="/57059335/5E8E50C4/61229369" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229369" Ref="R?"  Part="1" 
F 0 "R609" V 2350 6575 50  0000 C CNN
F 1 "2k" V 2275 6550 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2205 6550 50  0001 C CNN
F 3 "" H 2275 6550 50  0000 C CNN
	1    2275 6550
	-1   0    0    1   
$EndComp
Connection ~ 1550 6900
Wire Wire Line
	1550 6800 1550 6900
Wire Wire Line
	1650 6900 2025 6900
Wire Wire Line
	1550 6900 1650 6900
Connection ~ 1650 6900
Wire Wire Line
	1650 6800 1650 6900
$Comp
L power:GNDA #PWR?
U 1 1 61229375
P 1650 6900
AR Path="/59140D2B/61229375" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/61229375" Ref="#PWR?"  Part="1" 
AR Path="/57059335/61229375" Ref="#PWR0614"  Part="1" 
AR Path="/57059335/5E8E50C4/61229375" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229375" Ref="#PWR?"  Part="1" 
F 0 "#PWR0614" H 1650 6650 50  0001 C CNN
F 1 "GNDA" H 1650 6750 50  0000 C CNN
F 2 "" H 1650 6900 50  0000 C CNN
F 3 "" H 1650 6900 50  0000 C CNN
	1    1650 6900
	1    0    0    -1  
$EndComp
Connection ~ 2275 6400
Wire Wire Line
	2425 6400 2275 6400
Wire Wire Line
	2100 6200 2625 6200
Connection ~ 2625 6200
$Comp
L Device:C C?
U 1 1 61229380
P 2625 6550
AR Path="/59140D2B/61229380" Ref="C?"  Part="1" 
AR Path="/5B5208C5/61229380" Ref="C?"  Part="1" 
AR Path="/57059335/61229380" Ref="C617"  Part="1" 
AR Path="/57059335/5E8E50C4/61229380" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229380" Ref="C?"  Part="1" 
F 0 "C617" H 2575 6625 50  0000 L CNN
F 1 "10u" H 2575 6475 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2663 6400 50  0001 C CNN
F 3 "" H 2625 6550 50  0000 C CNN
	1    2625 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61229386
P 2425 6550
AR Path="/59140D2B/61229386" Ref="C?"  Part="1" 
AR Path="/5B5208C5/61229386" Ref="C?"  Part="1" 
AR Path="/57059335/61229386" Ref="C616"  Part="1" 
AR Path="/57059335/5E8E50C4/61229386" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229386" Ref="C?"  Part="1" 
F 0 "C616" H 2375 6625 50  0000 L CNN
F 1 "10u" H 2375 6475 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2463 6400 50  0001 C CNN
F 3 "" H 2425 6550 50  0000 C CNN
	1    2425 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 6400 1000 6600
Wire Wire Line
	1200 6400 1000 6400
Wire Wire Line
	800  6200 800  6600
$Comp
L Device:C C?
U 1 1 6122938F
P 800 6750
AR Path="/59140D2B/6122938F" Ref="C?"  Part="1" 
AR Path="/5B5208C5/6122938F" Ref="C?"  Part="1" 
AR Path="/57059335/6122938F" Ref="C619"  Part="1" 
AR Path="/57059335/5E8E50C4/6122938F" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/6122938F" Ref="C?"  Part="1" 
F 0 "C619" H 750 6825 50  0000 L CNN
F 1 "10u" H 750 6675 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 838 6600 50  0001 C CNN
F 3 "" H 800 6750 50  0000 C CNN
	1    800  6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  6900 1000 6900
Connection ~ 1000 6900
$Comp
L Device:C C?
U 1 1 61229397
P 1000 6750
AR Path="/59140D2B/61229397" Ref="C?"  Part="1" 
AR Path="/5B5208C5/61229397" Ref="C?"  Part="1" 
AR Path="/57059335/61229397" Ref="C620"  Part="1" 
AR Path="/57059335/5E8E50C4/61229397" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/61229397" Ref="C?"  Part="1" 
F 0 "C620" H 950 6825 50  0000 L CNN
F 1 "47u" H 950 6675 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1038 6600 50  0001 C CNN
F 3 "" H 1000 6750 50  0000 C CNN
	1    1000 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6900 1550 6900
Wire Wire Line
	1000 6900 1200 6900
Connection ~ 1200 6900
$Comp
L Device:C C?
U 1 1 612293A0
P 1200 6750
AR Path="/59140D2B/612293A0" Ref="C?"  Part="1" 
AR Path="/5B5208C5/612293A0" Ref="C?"  Part="1" 
AR Path="/57059335/612293A0" Ref="C621"  Part="1" 
AR Path="/57059335/5E8E50C4/612293A0" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612293A0" Ref="C?"  Part="1" 
F 0 "C621" H 1150 6825 50  0000 L CNN
F 1 "10u" H 1125 6675 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1238 6600 50  0001 C CNN
F 3 "" H 1200 6750 50  0000 C CNN
	1    1200 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6300 1200 6200
Connection ~ 800  6200
Wire Wire Line
	1200 6200 800  6200
Connection ~ 1200 6200
$Comp
L MDJ_compo:ADM7151 U603
U 1 1 612293AA
P 1650 6400
AR Path="/57059335/612293AA" Ref="U603"  Part="1" 
AR Path="/57059335/5E8E50C4/612293AA" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5E8E50C4/612293AA" Ref="U?"  Part="1" 
F 0 "U603" H 1650 6400 50  0000 C CNN
F 1 "ADM7151" H 1650 6774 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 1650 6400 50  0001 C CNN
F 3 "" H 1650 6400 50  0000 C CNN
	1    1650 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2625 6200 3175 6200
Text Label 6250 6200 2    50   ~ 0
VssBuff
NoConn ~ 3425 3400
NoConn ~ 3425 3550
NoConn ~ 3425 3850
NoConn ~ 3425 4000
NoConn ~ 3425 4300
NoConn ~ 3425 4450
NoConn ~ 3425 4750
NoConn ~ 3425 4900
NoConn ~ 2450 2975
NoConn ~ 2600 2975
Wire Wire Line
	1625 3400 1550 3400
Connection ~ 1550 3400
Wire Wire Line
	1550 3400 1550 3700
Wire Wire Line
	1625 3850 1550 3850
Connection ~ 1550 3850
Wire Wire Line
	1550 3850 1550 4150
Wire Wire Line
	1625 4300 1550 4300
Connection ~ 1550 4300
Wire Wire Line
	1550 4300 1550 4600
Wire Wire Line
	1625 4750 1550 4750
Connection ~ 1550 4750
Wire Wire Line
	1550 4750 1550 4975
Text Label 1950 1975 0    50   ~ 0
VccBuff
Wire Wire Line
	1950 1975 2525 1975
Text Label 1950 2050 0    50   ~ 0
VssBuff
Wire Wire Line
	1950 2050 2525 2050
Text Label 5000 1975 0    50   ~ 0
VccBuff
Wire Wire Line
	5000 1975 5575 1975
Text Label 5000 2050 0    50   ~ 0
VssBuff
Wire Wire Line
	5000 2050 5575 2050
Text Label 8150 1950 0    50   ~ 0
VccBuff
Wire Wire Line
	8150 1950 8725 1950
Text Label 8150 2025 0    50   ~ 0
VssBuff
Wire Wire Line
	8150 2025 8725 2025
Wire Wire Line
	2525 5175 2600 5175
Connection ~ 2600 5175
Wire Wire Line
	2375 5175 2375 5450
Wire Wire Line
	2375 5450 3175 5450
Text Label 3175 5450 2    50   ~ 0
DCDC_Temp
Text HLabel 1250 2225 2    50   Output ~ 0
DCDC_Temp
$Comp
L Device:C C?
U 1 1 6121C8E7
P 3975 4750
AR Path="/5787D549/6121C8E7" Ref="C?"  Part="1" 
AR Path="/572C0D81/6121C8E7" Ref="C?"  Part="1" 
AR Path="/57059335/5E8E50C4/6121C8E7" Ref="C?"  Part="1" 
AR Path="/57059335/6121C8E7" Ref="C607"  Part="1" 
F 0 "C607" H 3950 4825 50  0000 C CNN
F 1 "47u" H 4000 4675 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3975 4750 50  0001 C CNN
F 3 "" H 3975 4750 50  0000 C CNN
	1    3975 4750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
