EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 15
Title "FPGA power banks"
Date "2021-06-18"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C401
U 1 1 577DF9F2
P 5650 2925
F 0 "C401" H 5650 3000 50  0000 C CNN
F 1 "330u" H 5650 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 5650 2925 50  0001 C CNN
F 3 "" H 5650 2925 50  0000 C CNN
F 4 "Digi-key" V 5650 2925 60  0001 C CNN "Supplier"
F 5 "399-4105-1-ND" V 5650 2925 60  0001 C CNN "Supplier Part"
	1    5650 2925
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0403
U 1 1 577DFC69
P 5475 3075
F 0 "#PWR0403" H 5475 2825 50  0001 C CNN
F 1 "GNDD" H 5475 2925 50  0000 C CNN
F 2 "" H 5475 3075 50  0000 C CNN
F 3 "" H 5475 3075 50  0000 C CNN
	1    5475 3075
	1    0    0    -1  
$EndComp
$Comp
L Device:C C402
U 1 1 577DFDB5
P 5875 2925
F 0 "C402" H 5875 3000 50  0000 C CNN
F 1 "100u" H 5900 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5875 2925 50  0001 C CNN
F 3 "" H 5875 2925 50  0000 C CNN
F 4 "Digi-key" V 5875 2925 60  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 5875 2925 60  0001 C CNN "Supplier Part"
	1    5875 2925
	1    0    0    -1  
$EndComp
$Comp
L Device:C C403
U 1 1 62F3D632
P 6100 2925
F 0 "C403" H 6100 3000 50  0000 C CNN
F 1 "100u" H 6125 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6100 2925 50  0001 C CNN
F 3 "" H 6100 2925 50  0000 C CNN
F 4 "Digi-key" V 6100 2925 60  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 6100 2925 60  0001 C CNN "Supplier Part"
	1    6100 2925
	1    0    0    -1  
$EndComp
$Comp
L Device:C C404
U 1 1 62F3D633
P 6325 2925
F 0 "C404" H 6325 3000 50  0000 C CNN
F 1 "100u" H 6350 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6325 2925 50  0001 C CNN
F 3 "" H 6325 2925 50  0000 C CNN
F 4 "Digi-key" V 6325 2925 60  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 6325 2925 60  0001 C CNN "Supplier Part"
	1    6325 2925
	1    0    0    -1  
$EndComp
Text Notes 5650 2650 0    60   ~ 0
VCC_INT
$Comp
L Device:C C410
U 1 1 62F3D638
P 5650 3675
F 0 "C410" H 5650 3750 50  0000 C CNN
F 1 "47u" H 5650 3600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 3675 50  0001 C CNN
F 3 "" H 5650 3675 50  0000 C CNN
F 4 "Digi-key" V 5650 3675 60  0001 C CNN "Supplier"
F 5 "490-3390-1-ND	" V 5650 3675 60  0001 C CNN "Supplier Part"
	1    5650 3675
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0406
U 1 1 577E1B2A
P 5475 3825
F 0 "#PWR0406" H 5475 3575 50  0001 C CNN
F 1 "GNDD" H 5475 3675 50  0000 C CNN
F 2 "" H 5475 3825 50  0000 C CNN
F 3 "" H 5475 3825 50  0000 C CNN
	1    5475 3825
	1    0    0    -1  
$EndComp
$Comp
L Device:C C411
U 1 1 577E1B3D
P 6000 3675
F 0 "C411" H 6000 3750 50  0000 C CNN
F 1 "10u" H 6000 3600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6000 3675 50  0001 C CNN
F 3 "" H 6000 3675 50  0000 C CNN
F 4 "Digi-key" V 6000 3675 60  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 6000 3675 60  0001 C CNN "Supplier Part"
	1    6000 3675
	1    0    0    -1  
$EndComp
Text Notes 5650 3400 0    60   ~ 0
VCC_BRAM
$Comp
L Device:C C412
U 1 1 577E24E5
P 5625 4425
F 0 "C412" H 5650 4500 50  0000 C CNN
F 1 "47u" H 5625 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5625 4425 50  0001 C CNN
F 3 "" H 5625 4425 50  0000 C CNN
F 4 "Digi-key" V 5625 4425 60  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 5625 4425 60  0001 C CNN "Supplier Part"
	1    5625 4425
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0409
U 1 1 62F3D63C
P 5450 4575
F 0 "#PWR0409" H 5450 4325 50  0001 C CNN
F 1 "GNDD" H 5450 4425 50  0000 C CNN
F 2 "" H 5450 4575 50  0000 C CNN
F 3 "" H 5450 4575 50  0000 C CNN
	1    5450 4575
	1    0    0    -1  
$EndComp
Text Notes 5625 4150 0    60   ~ 0
VCC_AUX
$Comp
L Device:C C413
U 1 1 62F3D63E
P 6000 4425
F 0 "C413" H 6000 4500 50  0000 C CNN
F 1 "10u" H 5975 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6000 4425 50  0001 C CNN
F 3 "" H 6000 4425 50  0000 C CNN
F 4 "Digi-key" V 6000 4425 60  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 6000 4425 60  0001 C CNN "Supplier Part"
	1    6000 4425
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccInt #PWR0401
U 1 1 62F3D63F
P 5475 2775
F 0 "#PWR0401" H 5475 2625 50  0001 C CNN
F 1 "VccInt" H 5475 2925 50  0000 C CNN
F 2 "" H 5475 2775 50  0000 C CNN
F 3 "" H 5475 2775 50  0000 C CNN
	1    5475 2775
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccInt #PWR0405
U 1 1 62F3D641
P 5475 3525
F 0 "#PWR0405" H 5475 3375 50  0001 C CNN
F 1 "VccInt" H 5475 3675 50  0000 C CNN
F 2 "" H 5475 3525 50  0000 C CNN
F 3 "" H 5475 3525 50  0000 C CNN
	1    5475 3525
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccAux #PWR0408
U 1 1 57937FC1
P 5450 4275
F 0 "#PWR0408" H 5450 4125 50  0001 C CNN
F 1 "VccAux" H 5450 4425 50  0000 C CNN
F 2 "" H 5450 4275 50  0000 C CNN
F 3 "" H 5450 4275 50  0000 C CNN
	1    5450 4275
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0411
U 1 1 5B5914F0
P 925 5500
F 0 "#PWR0411" H 925 5250 50  0001 C CNN
F 1 "GNDD" H 925 5350 50  0000 C CNN
F 2 "" H 925 5500 50  0000 C CNN
F 3 "" H 925 5500 50  0000 C CNN
	1    925  5500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0410
U 1 1 5B5917DF
P 1625 5450
F 0 "#PWR0410" H 1625 5200 50  0001 C CNN
F 1 "GNDD" H 1625 5300 50  0000 C CNN
F 2 "" H 1625 5450 50  0000 C CNN
F 3 "" H 1625 5450 50  0000 C CNN
	1    1625 5450
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccInt #PWR0402
U 1 1 62F3D640
P 4125 3050
F 0 "#PWR0402" H 4125 2900 50  0001 C CNN
F 1 "VccInt" H 4125 3200 50  0000 C CNN
F 2 "" H 4125 3050 50  0000 C CNN
F 3 "" H 4125 3050 50  0000 C CNN
	1    4125 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5475 2775 5650 2775
Wire Wire Line
	5475 3075 5650 3075
Connection ~ 5650 2775
Connection ~ 5875 2775
Connection ~ 6100 2775
Connection ~ 6325 2775
Connection ~ 5650 3075
Wire Wire Line
	5875 3075 6100 3075
Connection ~ 5875 3075
Wire Wire Line
	5475 3525 5650 3525
Wire Wire Line
	5475 3825 5650 3825
Connection ~ 5650 3525
Connection ~ 5650 3825
Wire Wire Line
	5450 4275 5625 4275
Wire Wire Line
	5450 4575 5625 4575
Connection ~ 5625 4275
Connection ~ 5625 4575
Wire Wire Line
	975  3900 925  3900
Wire Wire Line
	975  3850 925  3850
Connection ~ 925  3850
Wire Wire Line
	975  3800 925  3800
Connection ~ 925  3800
Wire Wire Line
	975  3750 925  3750
Connection ~ 925  3750
Wire Wire Line
	975  3700 925  3700
Connection ~ 925  3700
Wire Wire Line
	975  3650 925  3650
Connection ~ 925  3650
Wire Wire Line
	975  3600 925  3600
Connection ~ 925  3600
Wire Wire Line
	925  3550 975  3550
Connection ~ 925  3550
Wire Wire Line
	925  3500 975  3500
Connection ~ 925  3500
Wire Wire Line
	925  3450 975  3450
Connection ~ 925  3450
Wire Wire Line
	925  3400 975  3400
Connection ~ 925  3400
Wire Wire Line
	975  3350 925  3350
Connection ~ 925  3350
Wire Wire Line
	975  3300 925  3300
Connection ~ 925  3300
Wire Wire Line
	975  3250 925  3250
Connection ~ 925  3250
Wire Wire Line
	975  3200 925  3200
Connection ~ 925  3200
Wire Wire Line
	975  3150 925  3150
Connection ~ 925  3150
Wire Wire Line
	975  3100 925  3100
Connection ~ 925  3100
Wire Wire Line
	975  3050 925  3050
Connection ~ 925  3050
Wire Wire Line
	975  3000 925  3000
Connection ~ 925  3000
Wire Wire Line
	975  2950 925  2950
Connection ~ 925  2950
Wire Wire Line
	975  2900 925  2900
Connection ~ 925  2900
Wire Wire Line
	975  2850 925  2850
Connection ~ 925  2850
Wire Wire Line
	975  2800 925  2800
Connection ~ 925  2800
Wire Wire Line
	975  2750 925  2750
Connection ~ 925  2750
Wire Wire Line
	975  2700 925  2700
Connection ~ 925  2700
Wire Wire Line
	975  2650 925  2650
Connection ~ 925  2650
Wire Wire Line
	975  2600 925  2600
Connection ~ 925  2600
Wire Wire Line
	975  2550 925  2550
Connection ~ 925  2550
Wire Wire Line
	975  2500 925  2500
Connection ~ 925  2500
Wire Wire Line
	975  2450 925  2450
Connection ~ 925  2450
Wire Wire Line
	975  2400 925  2400
Connection ~ 925  2400
Wire Wire Line
	975  2350 925  2350
Connection ~ 925  2350
Wire Wire Line
	975  2300 925  2300
Connection ~ 925  2300
Wire Wire Line
	975  2250 925  2250
Connection ~ 925  2250
Wire Wire Line
	975  2200 925  2200
Connection ~ 925  2200
Wire Wire Line
	975  2150 925  2150
Connection ~ 925  2150
Wire Wire Line
	975  2100 925  2100
Connection ~ 925  2100
Wire Wire Line
	975  2050 925  2050
Connection ~ 925  2050
Wire Wire Line
	975  2000 925  2000
Connection ~ 925  2000
Wire Wire Line
	975  1950 925  1950
Connection ~ 925  1950
Wire Wire Line
	975  1900 925  1900
Connection ~ 925  1900
Wire Wire Line
	975  1850 925  1850
Connection ~ 925  1850
Wire Wire Line
	975  1800 925  1800
Wire Wire Line
	1575 1900 1625 1900
Wire Wire Line
	1625 1950 1575 1950
Connection ~ 1625 1950
Wire Wire Line
	1575 2000 1625 2000
Connection ~ 1625 2000
Wire Wire Line
	1575 2050 1625 2050
Connection ~ 1625 2050
Wire Wire Line
	1575 2100 1625 2100
Connection ~ 1625 2100
Wire Wire Line
	1575 2150 1625 2150
Connection ~ 1625 2150
Wire Wire Line
	1575 2200 1625 2200
Connection ~ 1625 2200
Wire Wire Line
	1575 2250 1625 2250
Connection ~ 1625 2250
Wire Wire Line
	1575 2300 1625 2300
Connection ~ 1625 2300
Wire Wire Line
	1575 2350 1625 2350
Connection ~ 1625 2350
Wire Wire Line
	1575 2400 1625 2400
Connection ~ 1625 2400
Wire Wire Line
	1575 2450 1625 2450
Connection ~ 1625 2450
Wire Wire Line
	1575 2500 1625 2500
Connection ~ 1625 2500
Wire Wire Line
	1575 2550 1625 2550
Connection ~ 1625 2550
Wire Wire Line
	1575 2600 1625 2600
Connection ~ 1625 2600
Wire Wire Line
	1575 2650 1625 2650
Connection ~ 1625 2650
Wire Wire Line
	1575 2700 1625 2700
Connection ~ 1625 2700
Wire Wire Line
	1575 2750 1625 2750
Connection ~ 1625 2750
Wire Wire Line
	1575 2800 1625 2800
Connection ~ 1625 2800
Wire Wire Line
	1625 2850 1575 2850
Connection ~ 1625 2850
Wire Wire Line
	1575 2900 1625 2900
Connection ~ 1625 2900
Wire Wire Line
	1625 2950 1575 2950
Connection ~ 1625 2950
Wire Wire Line
	1575 3000 1625 3000
Connection ~ 1625 3000
Wire Wire Line
	1625 3050 1575 3050
Connection ~ 1625 3050
Wire Wire Line
	1575 3100 1625 3100
Connection ~ 1625 3100
Wire Wire Line
	1625 3150 1575 3150
Connection ~ 1625 3150
Wire Wire Line
	1575 3200 1625 3200
Connection ~ 1625 3200
Wire Wire Line
	1575 3250 1625 3250
Connection ~ 1625 3250
Wire Wire Line
	1575 3300 1625 3300
Connection ~ 1625 3300
Wire Wire Line
	1625 3350 1575 3350
Connection ~ 1625 3350
Wire Wire Line
	1575 3400 1625 3400
Connection ~ 1625 3400
Wire Wire Line
	1625 3450 1575 3450
Connection ~ 1625 3450
Wire Wire Line
	1575 3500 1625 3500
Connection ~ 1625 3500
Wire Wire Line
	1625 3550 1575 3550
Connection ~ 1625 3550
Wire Wire Line
	1575 3600 1625 3600
Connection ~ 1625 3600
Wire Wire Line
	1625 3650 1575 3650
Connection ~ 1625 3650
Wire Wire Line
	1575 3700 1625 3700
Connection ~ 1625 3700
Wire Wire Line
	1625 3750 1575 3750
Connection ~ 1625 3750
Wire Wire Line
	1575 3800 1625 3800
Connection ~ 1625 3800
Wire Wire Line
	1625 3850 1575 3850
Connection ~ 1625 3850
Wire Wire Line
	1575 3900 1625 3900
Connection ~ 1625 3900
Wire Wire Line
	1625 3950 1575 3950
Connection ~ 1625 3950
Wire Wire Line
	1575 4000 1625 4000
Wire Wire Line
	4125 3850 4075 3850
Wire Wire Line
	4075 3250 4125 3250
Connection ~ 4125 3250
Wire Wire Line
	4125 3300 4075 3300
Connection ~ 4125 3300
Wire Wire Line
	4075 3350 4125 3350
Connection ~ 4125 3350
Wire Wire Line
	4125 3400 4075 3400
Connection ~ 4125 3400
Wire Wire Line
	4075 3450 4125 3450
Connection ~ 4125 3450
Wire Wire Line
	4125 3500 4075 3500
Connection ~ 4125 3500
Wire Wire Line
	4075 3550 4125 3550
Connection ~ 4125 3550
Wire Wire Line
	4125 3600 4075 3600
Connection ~ 4125 3600
Wire Wire Line
	4075 3650 4125 3650
Connection ~ 4125 3650
Wire Wire Line
	4125 3700 4075 3700
Connection ~ 4125 3700
Wire Wire Line
	4075 3750 4125 3750
Connection ~ 4125 3750
Wire Wire Line
	4125 3800 4075 3800
Connection ~ 4125 3800
Wire Wire Line
	5650 2775 5875 2775
Wire Wire Line
	5875 2775 6100 2775
Wire Wire Line
	6100 2775 6325 2775
Wire Wire Line
	5650 3075 5875 3075
Wire Wire Line
	5650 3525 6000 3525
Wire Wire Line
	5650 3825 6000 3825
Wire Wire Line
	5625 4275 6000 4275
Wire Wire Line
	5625 4575 6000 4575
Wire Wire Line
	925  3850 925  3900
Wire Wire Line
	925  3800 925  3850
Wire Wire Line
	925  3750 925  3800
Wire Wire Line
	925  3700 925  3750
Wire Wire Line
	925  3650 925  3700
Wire Wire Line
	925  3600 925  3650
Wire Wire Line
	925  3550 925  3600
Wire Wire Line
	925  3500 925  3550
Wire Wire Line
	925  3450 925  3500
Wire Wire Line
	925  3400 925  3450
Wire Wire Line
	925  3350 925  3400
Wire Wire Line
	925  3300 925  3350
Wire Wire Line
	925  3250 925  3300
Wire Wire Line
	925  3200 925  3250
Wire Wire Line
	925  3150 925  3200
Wire Wire Line
	925  3100 925  3150
Wire Wire Line
	925  3050 925  3100
Wire Wire Line
	925  3000 925  3050
Wire Wire Line
	925  2950 925  3000
Wire Wire Line
	925  2900 925  2950
Wire Wire Line
	925  2850 925  2900
Wire Wire Line
	925  2800 925  2850
Wire Wire Line
	925  2750 925  2800
Wire Wire Line
	925  2700 925  2750
Wire Wire Line
	925  2650 925  2700
Wire Wire Line
	925  2600 925  2650
Wire Wire Line
	925  2550 925  2600
Wire Wire Line
	925  2500 925  2550
Wire Wire Line
	925  2450 925  2500
Wire Wire Line
	925  2400 925  2450
Wire Wire Line
	925  2350 925  2400
Wire Wire Line
	925  2300 925  2350
Wire Wire Line
	925  2250 925  2300
Wire Wire Line
	925  2200 925  2250
Wire Wire Line
	925  2150 925  2200
Wire Wire Line
	925  2100 925  2150
Wire Wire Line
	925  2050 925  2100
Wire Wire Line
	925  2000 925  2050
Wire Wire Line
	925  1950 925  2000
Wire Wire Line
	925  1900 925  1950
Wire Wire Line
	925  1850 925  1900
Wire Wire Line
	925  1800 925  1850
Wire Wire Line
	1625 1900 1625 1950
Wire Wire Line
	1625 1950 1625 2000
Wire Wire Line
	1625 2000 1625 2050
Wire Wire Line
	1625 2050 1625 2100
Wire Wire Line
	1625 2100 1625 2150
Wire Wire Line
	1625 2150 1625 2200
Wire Wire Line
	1625 2200 1625 2250
Wire Wire Line
	1625 2250 1625 2300
Wire Wire Line
	1625 2300 1625 2350
Wire Wire Line
	1625 2350 1625 2400
Wire Wire Line
	1625 2400 1625 2450
Wire Wire Line
	1625 2450 1625 2500
Wire Wire Line
	1625 2500 1625 2550
Wire Wire Line
	1625 2550 1625 2600
Wire Wire Line
	1625 2600 1625 2650
Wire Wire Line
	1625 2650 1625 2700
Wire Wire Line
	1625 2700 1625 2750
Wire Wire Line
	1625 2750 1625 2800
Wire Wire Line
	1625 2800 1625 2850
Wire Wire Line
	1625 2850 1625 2900
Wire Wire Line
	1625 2900 1625 2950
Wire Wire Line
	1625 2950 1625 3000
Wire Wire Line
	1625 3000 1625 3050
Wire Wire Line
	1625 3050 1625 3100
Wire Wire Line
	1625 3100 1625 3150
Wire Wire Line
	1625 3150 1625 3200
Wire Wire Line
	1625 3200 1625 3250
Wire Wire Line
	1625 3250 1625 3300
Wire Wire Line
	1625 3300 1625 3350
Wire Wire Line
	1625 3350 1625 3400
Wire Wire Line
	1625 3400 1625 3450
Wire Wire Line
	1625 3450 1625 3500
Wire Wire Line
	1625 3500 1625 3550
Wire Wire Line
	1625 3550 1625 3600
Wire Wire Line
	1625 3600 1625 3650
Wire Wire Line
	1625 3650 1625 3700
Wire Wire Line
	1625 3700 1625 3750
Wire Wire Line
	1625 3750 1625 3800
Wire Wire Line
	1625 3800 1625 3850
Wire Wire Line
	1625 3850 1625 3900
Wire Wire Line
	1625 3900 1625 3950
Wire Wire Line
	1625 3950 1625 4000
Wire Wire Line
	4125 3250 4125 3300
Wire Wire Line
	4125 3300 4125 3350
Wire Wire Line
	4125 3350 4125 3400
Wire Wire Line
	4125 3400 4125 3450
Wire Wire Line
	4125 3450 4125 3500
Wire Wire Line
	4125 3500 4125 3550
Wire Wire Line
	4125 3550 4125 3600
Wire Wire Line
	4125 3600 4125 3650
Wire Wire Line
	4125 3650 4125 3700
Wire Wire Line
	4125 3700 4125 3750
Wire Wire Line
	4125 3750 4125 3800
Wire Wire Line
	4125 3800 4125 3850
Wire Wire Line
	4125 3050 4125 3150
Wire Wire Line
	4075 3900 4125 3900
Wire Wire Line
	4125 3900 4125 3850
Connection ~ 4125 3850
Wire Wire Line
	6100 3075 6325 3075
Connection ~ 6100 3075
Connection ~ 6325 3075
Wire Wire Line
	4075 3200 4125 3200
Connection ~ 4125 3200
Wire Wire Line
	4125 3200 4125 3250
Wire Wire Line
	4075 3150 4125 3150
Connection ~ 4125 3150
Wire Wire Line
	4125 3150 4125 3200
Wire Wire Line
	1575 4050 1625 4050
Connection ~ 1625 4050
Wire Wire Line
	1625 4100 1575 4100
Connection ~ 1625 4100
Wire Wire Line
	1575 4150 1625 4150
Connection ~ 1625 4150
Wire Wire Line
	1625 4200 1575 4200
Connection ~ 1625 4200
Wire Wire Line
	1575 4250 1625 4250
Connection ~ 1625 4250
Wire Wire Line
	1625 4300 1575 4300
Connection ~ 1625 4300
Wire Wire Line
	1575 4350 1625 4350
Connection ~ 1625 4350
Wire Wire Line
	1625 4400 1575 4400
Connection ~ 1625 4400
Wire Wire Line
	1575 4450 1625 4450
Wire Wire Line
	1625 4000 1625 4050
Wire Wire Line
	1625 4050 1625 4100
Wire Wire Line
	1625 4100 1625 4150
Wire Wire Line
	1625 4150 1625 4200
Wire Wire Line
	1625 4200 1625 4250
Wire Wire Line
	1625 4250 1625 4300
Wire Wire Line
	1625 4300 1625 4350
Wire Wire Line
	1625 4350 1625 4400
Wire Wire Line
	1625 4400 1625 4450
Connection ~ 1625 4000
Wire Wire Line
	1575 4500 1625 4500
Connection ~ 1625 4500
Wire Wire Line
	1625 4550 1575 4550
Connection ~ 1625 4550
Wire Wire Line
	1575 4600 1625 4600
Connection ~ 1625 4600
Wire Wire Line
	1625 4650 1575 4650
Connection ~ 1625 4650
Wire Wire Line
	1575 4700 1625 4700
Connection ~ 1625 4700
Wire Wire Line
	1625 4750 1575 4750
Connection ~ 1625 4750
Wire Wire Line
	1575 4800 1625 4800
Wire Wire Line
	1625 4450 1625 4500
Wire Wire Line
	1625 4500 1625 4550
Wire Wire Line
	1625 4550 1625 4600
Wire Wire Line
	1625 4600 1625 4650
Wire Wire Line
	1625 4650 1625 4700
Wire Wire Line
	1625 4700 1625 4750
Wire Wire Line
	1625 4750 1625 4800
Connection ~ 1625 4450
Connection ~ 1625 4800
Wire Wire Line
	975  4500 925  4500
Wire Wire Line
	975  4450 925  4450
Connection ~ 925  4450
Wire Wire Line
	975  4400 925  4400
Connection ~ 925  4400
Wire Wire Line
	975  4350 925  4350
Connection ~ 925  4350
Wire Wire Line
	975  4300 925  4300
Connection ~ 925  4300
Wire Wire Line
	975  4250 925  4250
Connection ~ 925  4250
Wire Wire Line
	975  4200 925  4200
Connection ~ 925  4200
Wire Wire Line
	925  4150 975  4150
Connection ~ 925  4150
Wire Wire Line
	925  4100 975  4100
Connection ~ 925  4100
Wire Wire Line
	925  4050 975  4050
Connection ~ 925  4050
Wire Wire Line
	925  4000 975  4000
Connection ~ 925  4000
Wire Wire Line
	975  3950 925  3950
Connection ~ 925  3950
Wire Wire Line
	925  4450 925  4500
Wire Wire Line
	925  4400 925  4450
Wire Wire Line
	925  4350 925  4400
Wire Wire Line
	925  4300 925  4350
Wire Wire Line
	925  4250 925  4300
Wire Wire Line
	925  4200 925  4250
Wire Wire Line
	925  4150 925  4200
Wire Wire Line
	925  4100 925  4150
Wire Wire Line
	925  4050 925  4100
Wire Wire Line
	925  4000 925  4050
Wire Wire Line
	925  3950 925  4000
Wire Wire Line
	925  3900 925  3950
Connection ~ 925  3900
Wire Wire Line
	975  4800 925  4800
Wire Wire Line
	975  4750 925  4750
Connection ~ 925  4750
Wire Wire Line
	975  4700 925  4700
Connection ~ 925  4700
Wire Wire Line
	975  4650 925  4650
Connection ~ 925  4650
Wire Wire Line
	975  4600 925  4600
Connection ~ 925  4600
Wire Wire Line
	975  4550 925  4550
Connection ~ 925  4550
Wire Wire Line
	925  4750 925  4800
Wire Wire Line
	925  4700 925  4750
Wire Wire Line
	925  4650 925  4700
Wire Wire Line
	925  4600 925  4650
Wire Wire Line
	925  4550 925  4600
Wire Wire Line
	925  4500 925  4550
Connection ~ 925  4500
Connection ~ 925  4800
$Comp
L MDJ_compo:XCKU035FBVA676 U?
U 6 1 60EF4C21
P 975 1800
AR Path="/60EF4C21" Ref="U?"  Part="6" 
AR Path="/572C0D81/60EF4C21" Ref="U302"  Part="6" 
F 0 "U302" H 1275 1992 40  0000 C CNN
F 1 "XCKU035FBVA676" H 1275 1916 40  0000 C CNN
F 2 "Package_BGA:BGA-676_27.0x27.0mm_Layout26x26_P1.0mm_Ball0.6mm_Pad0.5mm_NSMD" H 1025 1825 40  0001 R CNN
F 3 "" H 1025 1650 40  0001 R CNN
	6    975  1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	975  5450 925  5450
Connection ~ 925  5450
Wire Wire Line
	925  5450 925  5500
Wire Wire Line
	925  4800 925  4850
Wire Wire Line
	975  4850 925  4850
Connection ~ 925  4850
Wire Wire Line
	925  4850 925  4900
Wire Wire Line
	975  4900 925  4900
Connection ~ 925  4900
Wire Wire Line
	925  4900 925  4950
Wire Wire Line
	975  4950 925  4950
Connection ~ 925  4950
Wire Wire Line
	925  4950 925  5000
Wire Wire Line
	975  5000 925  5000
Connection ~ 925  5000
Wire Wire Line
	975  5050 925  5050
Wire Wire Line
	925  5000 925  5050
Connection ~ 925  5050
Wire Wire Line
	925  5050 925  5100
Wire Wire Line
	975  5100 925  5100
Connection ~ 925  5100
Wire Wire Line
	925  5100 925  5150
Wire Wire Line
	975  5150 925  5150
Connection ~ 925  5150
Wire Wire Line
	925  5150 925  5200
Wire Wire Line
	975  5200 925  5200
Connection ~ 925  5200
Wire Wire Line
	925  5200 925  5250
Wire Wire Line
	975  5250 925  5250
Connection ~ 925  5250
Wire Wire Line
	925  5250 925  5300
Wire Wire Line
	975  5300 925  5300
Connection ~ 925  5300
Wire Wire Line
	925  5300 925  5350
Wire Wire Line
	975  5350 925  5350
Connection ~ 925  5350
Wire Wire Line
	925  5350 925  5400
Wire Wire Line
	975  5400 925  5400
Connection ~ 925  5400
Wire Wire Line
	925  5400 925  5450
Wire Wire Line
	1625 4800 1625 4850
Wire Wire Line
	1575 5400 1625 5400
Connection ~ 1625 5400
Wire Wire Line
	1625 5400 1625 5450
Wire Wire Line
	1575 5350 1625 5350
Connection ~ 1625 5350
Wire Wire Line
	1625 5350 1625 5400
Wire Wire Line
	1575 5300 1625 5300
Connection ~ 1625 5300
Wire Wire Line
	1625 5300 1625 5350
Wire Wire Line
	1575 5250 1625 5250
Connection ~ 1625 5250
Wire Wire Line
	1625 5250 1625 5300
Wire Wire Line
	1575 5200 1625 5200
Connection ~ 1625 5200
Wire Wire Line
	1625 5200 1625 5250
Wire Wire Line
	1575 5150 1625 5150
Connection ~ 1625 5150
Wire Wire Line
	1625 5150 1625 5200
Wire Wire Line
	1575 5100 1625 5100
Connection ~ 1625 5100
Wire Wire Line
	1625 5100 1625 5150
Wire Wire Line
	1575 5050 1625 5050
Connection ~ 1625 5050
Wire Wire Line
	1625 5050 1625 5100
Wire Wire Line
	1575 5000 1625 5000
Connection ~ 1625 5000
Wire Wire Line
	1625 5000 1625 5050
Wire Wire Line
	1575 4950 1625 4950
Connection ~ 1625 4950
Wire Wire Line
	1625 4950 1625 5000
Wire Wire Line
	1575 4900 1625 4900
Connection ~ 1625 4900
Wire Wire Line
	1625 4900 1625 4950
Wire Wire Line
	1575 4850 1625 4850
Connection ~ 1625 4850
Wire Wire Line
	1625 4850 1625 4900
$Comp
L MDJ_compo:XCKU035FBVA676 U?
U 7 1 6133EA7F
P 3175 3150
AR Path="/6133EA7F" Ref="U?"  Part="7" 
AR Path="/572C0D81/6133EA7F" Ref="U302"  Part="7" 
F 0 "U302" H 3625 3342 40  0000 C CNN
F 1 "XCKU035FBVA676" H 3625 3266 40  0000 C CNN
F 2 "Package_BGA:BGA-676_27.0x27.0mm_Layout26x26_P1.0mm_Ball0.6mm_Pad0.5mm_NSMD" H 3225 3175 40  0001 R CNN
F 3 "" H 3225 3000 40  0001 R CNN
	7    3175 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 3900 4125 3950
Wire Wire Line
	4125 3950 4075 3950
Connection ~ 4125 3900
Wire Wire Line
	4125 3950 4125 4000
Wire Wire Line
	4125 4000 4075 4000
Connection ~ 4125 3950
Wire Wire Line
	4125 4000 4125 4050
Wire Wire Line
	4125 4050 4075 4050
Connection ~ 4125 4000
Wire Wire Line
	4125 4050 4125 4100
Wire Wire Line
	4125 4100 4075 4100
Connection ~ 4125 4050
Wire Wire Line
	4125 4100 4125 4150
Wire Wire Line
	4125 4150 4075 4150
Connection ~ 4125 4100
Wire Wire Line
	4125 4150 4125 4200
Wire Wire Line
	4125 4200 4075 4200
Connection ~ 4125 4150
Wire Wire Line
	4125 4200 4125 4250
Wire Wire Line
	4125 4250 4075 4250
Connection ~ 4125 4200
Connection ~ 3125 3550
Wire Wire Line
	3125 3500 3125 3550
Wire Wire Line
	3125 3450 3125 3500
Wire Wire Line
	2975 3450 2975 3550
Wire Wire Line
	3125 3550 3175 3550
Connection ~ 3125 3500
Wire Wire Line
	3125 3500 3175 3500
Wire Wire Line
	3175 3450 3125 3450
Wire Wire Line
	2975 3550 3125 3550
$Comp
L MDJ_compo:VccAux #PWR0404
U 1 1 5B58D2A2
P 2975 3450
F 0 "#PWR0404" H 2975 3300 50  0001 C CNN
F 1 "VccAux" H 2975 3600 50  0000 C CNN
F 2 "" H 2975 3450 50  0000 C CNN
F 3 "" H 2975 3450 50  0000 C CNN
	1    2975 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 4250 4125 4300
Wire Wire Line
	4125 4300 4075 4300
Connection ~ 4125 4250
Wire Wire Line
	4125 4300 4125 4350
Wire Wire Line
	4125 4350 4075 4350
Connection ~ 4125 4300
Wire Wire Line
	3125 3550 3125 3600
Wire Wire Line
	3125 3900 3175 3900
Wire Wire Line
	3175 3850 3125 3850
Connection ~ 3125 3850
Wire Wire Line
	3125 3850 3125 3900
Wire Wire Line
	3175 3800 3125 3800
Connection ~ 3125 3800
Wire Wire Line
	3125 3800 3125 3850
Wire Wire Line
	3175 3750 3125 3750
Connection ~ 3125 3750
Wire Wire Line
	3125 3750 3125 3800
Wire Wire Line
	3175 3700 3125 3700
Connection ~ 3125 3700
Wire Wire Line
	3125 3700 3125 3750
Wire Wire Line
	3175 3650 3125 3650
Connection ~ 3125 3650
Wire Wire Line
	3125 3650 3125 3700
Wire Wire Line
	3175 3600 3125 3600
Connection ~ 3125 3600
Wire Wire Line
	3125 3600 3125 3650
$Comp
L MDJ_compo:VccInt #PWR0407
U 1 1 6281EDD0
P 2975 4000
F 0 "#PWR0407" H 2975 3850 50  0001 C CNN
F 1 "VccInt" H 2975 4150 50  0000 C CNN
F 2 "" H 2975 4000 50  0000 C CNN
F 3 "" H 2975 4000 50  0000 C CNN
	1    2975 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 4000 3125 4000
Wire Wire Line
	3125 4000 3125 3950
Wire Wire Line
	3125 3950 3175 3950
Connection ~ 3125 4000
Wire Wire Line
	3125 4000 3175 4000
Wire Wire Line
	6325 3075 6550 3075
Connection ~ 6775 3075
Wire Wire Line
	6325 2775 6550 2775
Connection ~ 6775 2775
$Comp
L Device:C C405
U 1 1 62F3D631
P 6550 2925
F 0 "C405" H 6575 3000 50  0000 C CNN
F 1 "100u" H 6550 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6550 2925 50  0001 C CNN
F 3 "" H 6550 2925 50  0000 C CNN
F 4 "Digi-key" V 6550 2925 60  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 6550 2925 60  0001 C CNN "Supplier Part"
	1    6550 2925
	1    0    0    -1  
$EndComp
$Comp
L Device:C C406
U 1 1 62F3D634
P 6775 2925
F 0 "C406" H 6775 3000 50  0000 C CNN
F 1 "100u" H 6775 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6775 2925 50  0001 C CNN
F 3 "" H 6775 2925 50  0000 C CNN
F 4 "Digi-key" V 6775 2925 60  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 6775 2925 60  0001 C CNN "Supplier Part"
	1    6775 2925
	1    0    0    -1  
$EndComp
Connection ~ 7000 3075
Connection ~ 7000 2775
$Comp
L Device:C C407
U 1 1 62F3D635
P 7000 2925
F 0 "C407" H 7000 3000 50  0000 C CNN
F 1 "47u" H 7000 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7000 2925 50  0001 C CNN
F 3 "" H 7000 2925 50  0000 C CNN
F 4 "Digi-key" V 7000 2925 60  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 7000 2925 60  0001 C CNN "Supplier Part"
	1    7000 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3075 7225 3075
Wire Wire Line
	7000 2775 7225 2775
$Comp
L Device:C C408
U 1 1 577E07EE
P 7225 2925
F 0 "C408" H 7225 3000 50  0000 C CNN
F 1 "10u" H 7225 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7225 2925 50  0001 C CNN
F 3 "" H 7225 2925 50  0000 C CNN
F 4 "Digi-key" V 7225 2925 60  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 7225 2925 60  0001 C CNN "Supplier Part"
	1    7225 2925
	1    0    0    -1  
$EndComp
Connection ~ 6550 2775
Wire Wire Line
	6550 2775 6775 2775
Connection ~ 6550 3075
Wire Wire Line
	6550 3075 6775 3075
Wire Wire Line
	6775 3075 7000 3075
Wire Wire Line
	6775 2775 7000 2775
$EndSCHEMATC
