EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 12
Title "ADC schematics"
Date "2020-04-08"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GNDA #PWR?
U 1 1 5FD25810
P 2100 2375
AR Path="/59140D2B/5FD25810" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5FD25810" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5FD25810" Ref="#PWR0826"  Part="1" 
AR Path="/5EB88B2F/5FD25810" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2100 2125 50  0001 C CNN
F 1 "GNDA" H 2100 2225 50  0000 C CNN
F 2 "" H 2100 2375 50  0000 C CNN
F 3 "" H 2100 2375 50  0000 C CNN
	1    2100 2375
	1    0    0    -1  
$EndComp
Text Notes 3075 6500 0    50   ~ 0
AVDD1 : Analog 0.95V (0.2A)\nAVDD2 : Analog 1.8V (0.3A)\nAVDD3 : Analog 2.5V (30mA)\nAVDD1_SR : Analog 0.95V (20mA)\nDRVDD1 : Digital driver 0.95V (0.2A)\nDRVDD2 : Digital driver 1.8V (20 mA)\nDVDD : Digital 0.95V (0.2A)\nSPI_VDDIO : Digital SPI 1.8V (2 mA)\n
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5D1C1653
P 1700 2075
AR Path="/57059335/5D1C1653" Ref="J?"  Part="1" 
AR Path="/5EB88B2F/5D1C1653" Ref="J?"  Part="1" 
F 0 "J?" H 1950 2150 50  0000 C CNN
F 1 "Conn_Coaxial" H 2050 2075 50  0000 C CNN
F 2 "" H 1700 2075 50  0001 C CNN
F 3 " ~" H 1700 2075 50  0001 C CNN
	1    1700 2075
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 2075 2100 2075
$Comp
L power:GNDA #PWR?
U 1 1 5D1D6A2B
P 1700 2275
AR Path="/59140D2B/5D1D6A2B" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5D1D6A2B" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5D1D6A2B" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5D1D6A2B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1700 2025 50  0001 C CNN
F 1 "GNDA" H 1700 2125 50  0000 C CNN
F 2 "" H 1700 2275 50  0000 C CNN
F 3 "" H 1700 2275 50  0000 C CNN
	1    1700 2275
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D2459A6
P 5450 1650
AR Path="/5B5208C5/5D2459A6" Ref="C?"  Part="1" 
AR Path="/59140D2B/5D2459A6" Ref="C?"  Part="1" 
AR Path="/57059335/5D2459A6" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5D2459A6" Ref="C?"  Part="1" 
F 0 "C?" H 5400 1725 50  0000 L CNN
F 1 "10pF" H 5350 1575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5488 1500 50  0001 C CNN
F 3 "" H 5450 1650 50  0000 C CNN
	1    5450 1650
	-1   0    0    1   
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5D24BA83
P 5650 1500
AR Path="/59140D2B/5D24BA83" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5D24BA83" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5D24BA83" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5D24BA83" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 1250 50  0001 C CNN
F 1 "GNDA" H 5650 1350 50  0000 C CNN
F 2 "" H 5650 1500 50  0000 C CNN
F 3 "" H 5650 1500 50  0000 C CNN
	1    5650 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D266355
P 6050 1850
AR Path="/57059335/5D266355" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5D266355" Ref="R?"  Part="1" 
F 0 "R?" V 5975 1850 50  0000 C CNN
F 1 "5" V 6050 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5980 1850 50  0001 C CNN
F 3 "" H 6050 1850 50  0000 C CNN
	1    6050 1850
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D2664AA
P 6050 2000
AR Path="/57059335/5D2664AA" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5D2664AA" Ref="R?"  Part="1" 
F 0 "R?" V 5975 2000 50  0000 C CNN
F 1 "5" V 6050 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5980 2000 50  0001 C CNN
F 3 "" H 6050 2000 50  0000 C CNN
	1    6050 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 2000 6200 1950
Wire Wire Line
	6200 1950 6350 1950
Wire Wire Line
	6200 1850 6200 1900
Wire Wire Line
	6200 1900 6350 1900
$Comp
L Device:C C?
U 1 1 5D2A0F3C
P 3400 1250
AR Path="/5B5208C5/5D2A0F3C" Ref="C?"  Part="1" 
AR Path="/59140D2B/5D2A0F3C" Ref="C?"  Part="1" 
AR Path="/57059335/5D2A0F3C" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5D2A0F3C" Ref="C?"  Part="1" 
F 0 "C?" H 3350 1325 50  0000 L CNN
F 1 "10u" H 3300 1175 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3438 1100 50  0001 C CNN
F 3 "" H 3400 1250 50  0000 C CNN
	1    3400 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D2A1027
P 3200 1250
AR Path="/5B5208C5/5D2A1027" Ref="C?"  Part="1" 
AR Path="/59140D2B/5D2A1027" Ref="C?"  Part="1" 
AR Path="/57059335/5D2A1027" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5D2A1027" Ref="C?"  Part="1" 
F 0 "C?" H 3150 1325 50  0000 L CNN
F 1 "100n" H 3100 1175 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3238 1100 50  0001 C CNN
F 3 "" H 3200 1250 50  0000 C CNN
	1    3200 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 1100 3675 1100
Wire Wire Line
	3400 1100 3200 1100
Connection ~ 3400 1100
$Comp
L power:GNDA #PWR?
U 1 1 5D2E6EFC
P 3400 1400
AR Path="/59140D2B/5D2E6EFC" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5D2E6EFC" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5D2E6EFC" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5D2E6EFC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3400 1150 50  0001 C CNN
F 1 "GNDA" H 3400 1250 50  0000 C CNN
F 2 "" H 3400 1400 50  0000 C CNN
F 3 "" H 3400 1400 50  0000 C CNN
	1    3400 1400
	1    0    0    -1  
$EndComp
Connection ~ 3400 1400
Wire Wire Line
	3200 1400 3400 1400
$Comp
L power:GNDA #PWR?
U 1 1 5D2F94BE
P 4075 2425
AR Path="/59140D2B/5D2F94BE" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5D2F94BE" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5D2F94BE" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5D2F94BE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4075 2175 50  0001 C CNN
F 1 "GNDA" H 4075 2275 50  0000 C CNN
F 2 "" H 4075 2425 50  0000 C CNN
F 3 "" H 4075 2425 50  0000 C CNN
	1    4075 2425
	1    0    0    -1  
$EndComp
Text Notes 4075 1450 0    50   ~ 0
VCM =1.41 V\nSignal 1.70 Vpp differential\n-> 0.85 V dynamic range per input\n-> VCM+-0.425V\nrail+=VCM+0.425 = 1,835 V\nrail-=VCM-0.425 = 0,985 V
Text Notes 4650 2000 0    50   ~ 0
1.815 V
Text Notes 4650 1850 0    50   ~ 0
1.006 V
Wire Wire Line
	8150 1800 8575 1800
Text Label 8300 1800 0    39   ~ 0
sData_P0
Wire Wire Line
	8150 1850 8575 1850
Text Label 8300 1850 0    39   ~ 0
sData_N0
Wire Wire Line
	8150 1950 8575 1950
Text Label 8300 1950 0    39   ~ 0
sData_P1
Wire Wire Line
	8150 2000 8575 2000
Text Label 8300 2000 0    39   ~ 0
sData_N1
Wire Wire Line
	8150 2100 8575 2100
Text Label 8300 2100 0    39   ~ 0
sData_P2
Wire Wire Line
	8150 2150 8575 2150
Text Label 8300 2150 0    39   ~ 0
sData_N2
Wire Wire Line
	8150 2250 8575 2250
Text Label 8300 2250 0    39   ~ 0
sData_P3
Wire Wire Line
	8150 2300 8575 2300
Text Label 8300 2300 0    39   ~ 0
sData_N3
Text HLabel 8750 1750 2    39   Output ~ 0
sData_P[0..3]
Text HLabel 8750 1800 2    39   Output ~ 0
sData_N[0..3]
Wire Wire Line
	6275 1800 6350 1800
Text HLabel 5400 850  0    39   Input ~ 0
Clk_1280_P
Text HLabel 5400 950  0    39   Input ~ 0
Clk_1280_N
$Comp
L Device:C C?
U 1 1 5E1104FE
P 5850 850
AR Path="/5B5208C5/5E1104FE" Ref="C?"  Part="1" 
AR Path="/59140D2B/5E1104FE" Ref="C?"  Part="1" 
AR Path="/57059335/5E1104FE" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E1104FE" Ref="C?"  Part="1" 
F 0 "C?" V 5900 900 50  0000 L CNN
F 1 "100n" V 5800 900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5888 700 50  0001 C CNN
F 3 "" H 5850 850 50  0000 C CNN
	1    5850 850 
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E110604
P 6075 950
AR Path="/5B5208C5/5E110604" Ref="C?"  Part="1" 
AR Path="/59140D2B/5E110604" Ref="C?"  Part="1" 
AR Path="/57059335/5E110604" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E110604" Ref="C?"  Part="1" 
F 0 "C?" V 6125 1000 50  0000 L CNN
F 1 "100n" V 6025 1000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6113 800 50  0001 C CNN
F 3 "" H 6075 950 50  0000 C CNN
	1    6075 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 850  6350 850 
Wire Wire Line
	6225 950  6275 950 
$Comp
L Device:R R?
U 1 1 5E19B846
P 5625 1100
AR Path="/57059335/5E19B846" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E19B846" Ref="R?"  Part="1" 
F 0 "R?" V 5550 1100 50  0000 C CNN
F 1 "100" V 5625 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5555 1100 50  0001 C CNN
F 3 "" H 5625 1100 50  0000 C CNN
	1    5625 1100
	-1   0    0    1   
$EndComp
Connection ~ 5700 850 
Wire Wire Line
	5400 850  5700 850 
Wire Wire Line
	5400 950  5625 950 
Connection ~ 5625 950 
Wire Wire Line
	5625 950  5925 950 
Wire Wire Line
	6275 950  6275 1800
Wire Wire Line
	6350 850  6350 1750
$Comp
L Device:R R?
U 1 1 5E9076F6
P 5075 1850
AR Path="/57059335/5E9076F6" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9076F6" Ref="R?"  Part="1" 
F 0 "R?" V 5000 1850 50  0000 C CNN
F 1 "5" V 5075 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5005 1850 50  0001 C CNN
F 3 "" H 5075 1850 50  0000 C CNN
	1    5075 1850
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E907FA9
P 5075 2000
AR Path="/57059335/5E907FA9" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E907FA9" Ref="R?"  Part="1" 
F 0 "R?" V 5000 2000 50  0000 C CNN
F 1 "5" V 5075 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5005 2000 50  0001 C CNN
F 3 "" H 5075 2000 50  0000 C CNN
	1    5075 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	4325 1875 4325 1850
Wire Wire Line
	4325 1850 4925 1850
Wire Wire Line
	4325 1975 4325 2000
Wire Wire Line
	4325 2000 4925 2000
Wire Wire Line
	3675 1100 3775 1100
Wire Wire Line
	3975 1100 3975 1425
Connection ~ 3675 1100
Wire Wire Line
	3875 1425 3875 1100
Connection ~ 3875 1100
Wire Wire Line
	3875 1100 3975 1100
Wire Wire Line
	3775 1425 3775 1100
Connection ~ 3775 1100
Wire Wire Line
	3775 1100 3875 1100
Wire Wire Line
	3675 1425 3675 1100
$Comp
L Device:C C?
U 1 1 5E8A1C96
P 3875 2700
AR Path="/5B5208C5/5E8A1C96" Ref="C?"  Part="1" 
AR Path="/59140D2B/5E8A1C96" Ref="C?"  Part="1" 
AR Path="/57059335/5E8A1C96" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8A1C96" Ref="C?"  Part="1" 
F 0 "C?" H 3825 2775 50  0000 L CNN
F 1 "10u" H 3775 2625 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3913 2550 50  0001 C CNN
F 3 "" H 3875 2700 50  0000 C CNN
	1    3875 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E8A1CA0
P 3675 2700
AR Path="/5B5208C5/5E8A1CA0" Ref="C?"  Part="1" 
AR Path="/59140D2B/5E8A1CA0" Ref="C?"  Part="1" 
AR Path="/57059335/5E8A1CA0" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E8A1CA0" Ref="C?"  Part="1" 
F 0 "C?" H 3625 2775 50  0000 L CNN
F 1 "100n" H 3575 2625 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3713 2550 50  0001 C CNN
F 3 "" H 3675 2700 50  0000 C CNN
	1    3675 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3975 2425 3875 2425
Connection ~ 3875 2425
Wire Wire Line
	3675 2425 3675 2550
Wire Wire Line
	3875 2550 3875 2425
Wire Wire Line
	3675 2850 3875 2850
$Comp
L Device:C C?
U 1 1 5E9F7223
P 2575 1775
AR Path="/5B5208C5/5E9F7223" Ref="C?"  Part="1" 
AR Path="/59140D2B/5E9F7223" Ref="C?"  Part="1" 
AR Path="/57059335/5E9F7223" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5E9F7223" Ref="C?"  Part="1" 
F 0 "C?" V 2625 1850 50  0000 L CNN
F 1 "100n" V 2525 1825 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2613 1625 50  0001 C CNN
F 3 "" H 2575 1775 50  0000 C CNN
	1    2575 1775
	0    -1   -1   0   
$EndComp
Connection ~ 2100 2075
$Comp
L Device:R R?
U 1 1 5E9F7CDB
P 3175 1775
AR Path="/57059335/5E9F7CDB" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9F7CDB" Ref="R?"  Part="1" 
F 0 "R?" V 3100 1775 50  0000 C CNN
F 1 "301" V 3175 1775 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3105 1775 50  0001 C CNN
F 3 "" H 3175 1775 50  0000 C CNN
	1    3175 1775
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9F8B86
P 3175 2075
AR Path="/57059335/5E9F8B86" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9F8B86" Ref="R?"  Part="1" 
F 0 "R?" V 3100 2075 50  0000 C CNN
F 1 "301" V 3175 2075 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3105 2075 50  0001 C CNN
F 3 "" H 3175 2075 50  0000 C CNN
	1    3175 2075
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9F968E
P 2875 2075
AR Path="/57059335/5E9F968E" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9F968E" Ref="R?"  Part="1" 
F 0 "R?" V 2800 2075 50  0000 C CNN
F 1 "301" V 2875 2075 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2805 2075 50  0001 C CNN
F 3 "" H 2875 2075 50  0000 C CNN
	1    2875 2075
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9FC0EE
P 2875 1775
AR Path="/57059335/5E9FC0EE" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9FC0EE" Ref="R?"  Part="1" 
F 0 "R?" V 2800 1775 50  0000 C CNN
F 1 "301" V 2875 1775 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2805 1775 50  0001 C CNN
F 3 "" H 2875 1775 50  0000 C CNN
	1    2875 1775
	0    1    1    0   
$EndComp
Wire Wire Line
	3025 1775 3025 1875
Wire Wire Line
	3025 1875 3325 1875
Connection ~ 3025 1775
Wire Wire Line
	3025 2075 3025 1975
Wire Wire Line
	3025 1975 3325 1975
Connection ~ 3025 2075
$Comp
L Device:C C?
U 1 1 5EA7B80B
P 2575 2075
AR Path="/5B5208C5/5EA7B80B" Ref="C?"  Part="1" 
AR Path="/59140D2B/5EA7B80B" Ref="C?"  Part="1" 
AR Path="/57059335/5EA7B80B" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5EA7B80B" Ref="C?"  Part="1" 
F 0 "C?" V 2625 2150 50  0000 L CNN
F 1 "100n" V 2700 2075 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2613 1925 50  0001 C CNN
F 3 "" H 2575 2075 50  0000 C CNN
	1    2575 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5EA7BF4B
P 2100 1775
AR Path="/59140D2B/5EA7BF4B" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5EA7BF4B" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5EA7BF4B" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5EA7BF4B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2100 1525 50  0001 C CNN
F 1 "GNDA" H 2100 1625 50  0000 C CNN
F 2 "" H 2100 1775 50  0000 C CNN
F 3 "" H 2100 1775 50  0000 C CNN
	1    2100 1775
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2075 2425 2075
$Comp
L Device:C C?
U 1 1 5EAA0321
P 3050 2325
AR Path="/5B5208C5/5EAA0321" Ref="C?"  Part="1" 
AR Path="/59140D2B/5EAA0321" Ref="C?"  Part="1" 
AR Path="/57059335/5EAA0321" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5EAA0321" Ref="C?"  Part="1" 
F 0 "C?" H 3000 2400 50  0000 L CNN
F 1 "100n" H 2950 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3088 2175 50  0001 C CNN
F 3 "" H 3050 2325 50  0000 C CNN
	1    3050 2325
	-1   0    0    1   
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5EAEC6F6
P 3875 2850
AR Path="/59140D2B/5EAEC6F6" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5EAEC6F6" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5EAEC6F6" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5EAEC6F6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3875 2600 50  0001 C CNN
F 1 "GNDA" H 3875 2700 50  0000 C CNN
F 2 "" H 3875 2850 50  0000 C CNN
F 3 "" H 3875 2850 50  0000 C CNN
	1    3875 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3325 2175 3200 2175
$Comp
L Device:C C?
U 1 1 5D2458D0
P 5250 1650
AR Path="/5B5208C5/5D2458D0" Ref="C?"  Part="1" 
AR Path="/59140D2B/5D2458D0" Ref="C?"  Part="1" 
AR Path="/57059335/5D2458D0" Ref="C?"  Part="1" 
AR Path="/5EB88B2F/5D2458D0" Ref="C?"  Part="1" 
F 0 "C?" H 5200 1725 50  0000 L CNN
F 1 "10pF" H 5150 1575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5288 1500 50  0001 C CNN
F 3 "" H 5250 1650 50  0000 C CNN
	1    5250 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5700 850  5700 1250
Wire Wire Line
	5700 1250 5625 1250
Connection ~ 3675 2425
Wire Wire Line
	3775 2425 3675 2425
Wire Wire Line
	3875 2425 3775 2425
Connection ~ 3775 2425
$Comp
L MDJ_compo:ADA4927-1 U?
U 1 1 5E8F40CE
P 3875 1925
AR Path="/57059335/5E8F40CE" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5E8F40CE" Ref="U?"  Part="1" 
F 0 "U?" H 4225 1750 60  0000 L CNN
F 1 "ADA4927-1" H 4225 1650 60  0000 L CNN
F 2 "" H 3875 1925 60  0001 C CNN
F 3 "" H 3875 1925 60  0001 C CNN
	1    3875 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1100 3075 1100
Wire Wire Line
	3075 1100 3075 1675
Wire Wire Line
	3075 1675 3325 1675
Connection ~ 3200 1100
Wire Wire Line
	2450 2450 2725 2450
Text Label 2075 3225 0    50   ~ 0
Vped_N
Wire Wire Line
	2400 3225 2075 3225
Text Label 2075 3025 0    50   ~ 0
Vped_P
Wire Wire Line
	2075 3025 2400 3025
$Comp
L MDJ_compo:AD9697 U?
U 1 1 5ED6B8A6
P 7250 2300
AR Path="/57059335/5ED6B8A6" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5ED6B8A6" Ref="U?"  Part="1" 
F 0 "U?" H 7250 3317 50  0000 C CNN
F 1 "AD9697" H 7250 3226 50  0000 C CNN
F 2 "Package_CSP:LFCSP-64-1EP_9x9mm_P0.5mm_EP5.21x5.21mm" H 7240 3220 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ad9697.pdf" H 6140 3030 50  0001 C CNN
	1    7250 2300
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:AD9697 U?
U 2 1 5E926E04
P 5525 6325
AR Path="/57059335/5E926E04" Ref="U?"  Part="2" 
AR Path="/5EB88B2F/5E926E04" Ref="U?"  Part="2" 
F 0 "U?" V 4358 6350 50  0000 C CNN
F 1 "AD9697" V 4449 6350 50  0000 C CNN
F 2 "Package_CSP:LFCSP-64-1EP_9x9mm_P0.5mm_EP5.21x5.21mm" H 5515 7245 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ad9697.pdf" H 4415 7055 50  0001 C CNN
	2    5525 6325
	0    1    1    0   
$EndComp
$Sheet
S 1250 5775 775  850 
U 5E8E50C4
F0 "ADC_power" 50
F1 "ADC_power.sch" 50
F2 "AVDD2" O R 2025 5975 50 
F3 "AVDD3" O R 2025 6050 50 
F4 "DRVDD2" O R 2025 6275 50 
F5 "AVDD1" O R 2025 5900 50 
F6 "SPIVDD" O R 2025 6350 50 
F7 "AVDD1_SR" O R 2025 5825 50 
F8 "DVDD" O R 2025 6125 50 
F9 "DRVDD" O R 2025 6200 50 
F10 "VccBuff" O R 2025 6500 50 
F11 "VssBuff" O R 2025 6575 50 
$EndSheet
Wire Wire Line
	2025 5825 2525 5825
Text Label 2525 5825 2    50   ~ 0
AVDD1_SR
Wire Wire Line
	2025 5900 2525 5900
Text Label 2525 5900 2    50   ~ 0
AVDD1
Wire Wire Line
	2025 5975 2525 5975
Text Label 2525 5975 2    50   ~ 0
AVDD2
Wire Wire Line
	2025 6050 2525 6050
Text Label 2525 6050 2    50   ~ 0
AVDD3
Wire Wire Line
	2025 6125 2525 6125
Text Label 2525 6125 2    50   ~ 0
DVDD
Wire Wire Line
	2025 6200 2525 6200
Text Label 2525 6200 2    50   ~ 0
DRVDD
Wire Wire Line
	2025 6275 2525 6275
Text Label 2525 6275 2    50   ~ 0
DRVDD2
Wire Wire Line
	2025 6350 2525 6350
Text Label 2525 6350 2    50   ~ 0
SPIVDD
Wire Wire Line
	2025 6500 2525 6500
Wire Wire Line
	2025 6575 2525 6575
Text Label 2525 6575 2    50   ~ 0
VssBuff
Text Label 2525 6500 2    50   ~ 0
VccBuff
Wire Wire Line
	6025 5775 6025 5725
Connection ~ 6025 5425
Wire Wire Line
	6025 5425 6025 5375
Connection ~ 6025 5475
Wire Wire Line
	6025 5475 6025 5425
Connection ~ 6025 5525
Wire Wire Line
	6025 5525 6025 5475
Connection ~ 6025 5575
Wire Wire Line
	6025 5575 6025 5525
Connection ~ 6025 5625
Wire Wire Line
	6025 5625 6025 5575
Connection ~ 6025 5675
Wire Wire Line
	6025 5675 6025 5625
Connection ~ 6025 5725
Wire Wire Line
	6025 5725 6025 5675
Wire Wire Line
	6025 6775 6525 6775
Text Label 6525 6775 2    50   ~ 0
AVDD1_SR
Wire Wire Line
	6025 5375 6525 5375
Text Label 6525 5375 2    50   ~ 0
AVDD1
Wire Wire Line
	6025 5875 6525 5875
Text Label 6525 5875 2    50   ~ 0
AVDD2
Wire Wire Line
	6025 6525 6525 6525
Text Label 6525 6525 2    50   ~ 0
AVDD3
Wire Wire Line
	6025 7125 6525 7125
Text Label 6525 7125 2    50   ~ 0
DVDD
Wire Wire Line
	6025 6875 6525 6875
Text Label 6525 6875 2    50   ~ 0
DRVDD
Wire Wire Line
	6025 7025 6525 7025
Text Label 6525 7025 2    50   ~ 0
DRVDD2
Wire Wire Line
	6025 7275 6525 7275
Text Label 6525 7275 2    50   ~ 0
SPIVDD
Text Label 3325 2425 0    50   ~ 0
VssBuff
Text Label 2750 1100 0    50   ~ 0
VccBuff
Connection ~ 6025 5375
Wire Wire Line
	6025 5875 6025 5925
Connection ~ 6025 5925
Wire Wire Line
	6025 5925 6025 5975
Connection ~ 6025 5975
Wire Wire Line
	6025 5975 6025 6025
Connection ~ 6025 6025
Wire Wire Line
	6025 6025 6025 6075
Connection ~ 6025 6075
Wire Wire Line
	6025 6075 6025 6125
Connection ~ 6025 6125
Wire Wire Line
	6025 6125 6025 6175
Connection ~ 6025 6175
Wire Wire Line
	6025 6175 6025 6225
Connection ~ 6025 6225
Wire Wire Line
	6025 6225 6025 6275
Connection ~ 6025 6275
Wire Wire Line
	6025 6275 6025 6325
Connection ~ 6025 6325
Wire Wire Line
	6025 6325 6025 6375
Connection ~ 6025 6375
Wire Wire Line
	6025 6375 6025 6425
Wire Wire Line
	6025 6675 6025 6625
Connection ~ 6025 6575
Wire Wire Line
	6025 6575 6025 6525
Connection ~ 6025 6625
Wire Wire Line
	6025 6625 6025 6575
Wire Wire Line
	6025 6925 6025 6875
Wire Wire Line
	6025 7175 6025 7125
Connection ~ 6025 6875
Connection ~ 6025 7125
Wire Wire Line
	6025 7325 6025 7275
Connection ~ 6025 7275
Connection ~ 6025 6525
Connection ~ 6025 5875
$Comp
L power:GNDA #PWR?
U 1 1 5F87B876
P 5075 6625
AR Path="/59140D2B/5F87B876" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5F87B876" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5F87B876" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5F87B876" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5075 6375 50  0001 C CNN
F 1 "GNDA" H 5075 6475 50  0000 C CNN
F 2 "" H 5075 6625 50  0000 C CNN
F 3 "" H 5075 6625 50  0000 C CNN
	1    5075 6625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 6075 5075 6175
Connection ~ 5075 6175
Wire Wire Line
	5075 6175 5075 6225
Connection ~ 5075 6225
Wire Wire Line
	5075 6225 5075 6325
Connection ~ 5075 6325
Wire Wire Line
	5075 6325 5075 6375
Connection ~ 5075 6375
Wire Wire Line
	5075 6375 5075 6475
Connection ~ 5075 6475
Wire Wire Line
	5075 6475 5075 6525
Connection ~ 5075 6525
Wire Wire Line
	5075 6525 5075 6625
Wire Wire Line
	3075 1100 2750 1100
Connection ~ 3075 1100
Connection ~ 3875 2850
Wire Wire Line
	3675 2425 3325 2425
Text Notes 6525 3950 0    39   ~ 0
Registers :\n0x1908 : 0 = AC coupling, 4 = DC coupling\n0x1910 : 0x1D = 1.70 V pp input ramge\n0x18A6 : 0 = Use internal Vref voltage\n0x18E6 : 0 = turn off temp diode export\n0x18E3 : b6 = export VCM to pin Vref,\n          b5-0 = 0x0F (300 uA)\n0x1A4C : b5-0 = 0x0F (300 uA)\n0x1A4D : b5-0 = 0x0F (300 uA)\n0x1B03 : 0x02\n0x1B08 : 0xC1\n0x1B10 : 0x00
Wire Wire Line
	3200 2975 4775 2975
Wire Wire Line
	4775 2975 4775 2150
Wire Wire Line
	4775 2150 6350 2150
$Comp
L power:GNDA #PWR?
U 1 1 5FB1C920
P 3050 2475
AR Path="/59140D2B/5FB1C920" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5FB1C920" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5FB1C920" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5FB1C920" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3050 2225 50  0001 C CNN
F 1 "GNDA" H 3050 2325 50  0000 C CNN
F 2 "" H 3050 2475 50  0000 C CNN
F 3 "" H 3050 2475 50  0000 C CNN
	1    3050 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2175 3200 2175
Connection ~ 3200 2175
$Comp
L Device:R R?
U 1 1 5E9F62ED
P 2100 2225
AR Path="/57059335/5E9F62ED" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5E9F62ED" Ref="R?"  Part="1" 
F 0 "R?" V 2025 2225 50  0000 C CNN
F 1 "57" V 2100 2225 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2030 2225 50  0001 C CNN
F 3 "" H 2100 2225 50  0000 C CNN
	1    2100 2225
	-1   0    0    1   
$EndComp
Text Label 2450 2450 0    50   ~ 0
Vped_N
Connection ~ 2725 1775
Wire Wire Line
	2725 1425 2725 1775
Wire Wire Line
	2725 2450 2725 2075
Wire Wire Line
	2725 1425 2450 1425
Text Label 2450 1425 0    50   ~ 0
Vped_P
Connection ~ 2725 2075
Text Notes 1850 1650 0    39   ~ 0
Put C footprints in order\nto be able to swap signal\npolatiry on diff-amp inputs
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5E9CB79D
P 2600 3125
AR Path="/57059335/5E9CB79D" Ref="J?"  Part="1" 
AR Path="/5EB88B2F/5E9CB79D" Ref="J?"  Part="1" 
F 0 "J?" H 2680 3167 50  0000 L CNN
F 1 "Conn_01x03" H 2680 3076 50  0000 L CNN
F 2 "" H 2600 3125 50  0001 C CNN
F 3 "~" H 2600 3125 50  0001 C CNN
	1    2600 3125
	1    0    0    -1  
$EndComp
Text Notes 1725 2900 0    39   ~ 0
Connect Vped_N for negative pulse (0.15 V)\nConnect Vped_P for positive pulse (2.82 V)
$Comp
L MDJ_compo:ADA4522 U?
U 1 1 5EB606CE
P 1625 3125
AR Path="/57059335/5EB606CE" Ref="U?"  Part="1" 
AR Path="/5EB88B2F/5EB606CE" Ref="U?"  Part="1" 
F 0 "U?" H 1675 3000 50  0000 L CNN
F 1 "ADA4522" H 1675 3275 50  0000 L CNN
F 2 "" H 1625 3125 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4522-1_4522-2_4522-4.pdf" H 1625 3125 50  0001 C CNN
	1    1625 3125
	1    0    0    -1  
$EndComp
Text Label 1300 2825 0    50   ~ 0
VccBuff
Text Label 1300 3450 0    50   ~ 0
VssBuff
Wire Wire Line
	1625 2925 1625 2825
Wire Wire Line
	1625 3450 1625 3325
Wire Wire Line
	1325 3225 1325 3350
Wire Wire Line
	1325 3350 1925 3350
Wire Wire Line
	1925 3350 1925 3125
Wire Wire Line
	1325 3025 1125 3025
$Comp
L Device:R_POT RV?
U 1 1 5E94C8CC
P 975 3025
AR Path="/57059335/5E94C8CC" Ref="RV?"  Part="1" 
AR Path="/5EB88B2F/5E94C8CC" Ref="RV?"  Part="1" 
F 0 "RV?" H 905 3071 50  0000 R CNN
F 1 "10k" H 905 2980 50  0000 R CNN
F 2 "" H 975 3025 50  0001 C CNN
F 3 "~" H 975 3025 50  0001 C CNN
	1    975  3025
	1    0    0    -1  
$EndComp
Wire Wire Line
	975  2875 975  2825
Wire Wire Line
	975  2825 1625 2825
Wire Wire Line
	975  3175 975  3450
Wire Wire Line
	975  3450 1625 3450
Wire Wire Line
	1925 3125 2400 3125
Connection ~ 1925 3125
Wire Wire Line
	2100 1775 2425 1775
Text HLabel 6125 2650 0    39   Input ~ 0
CSb
Text HLabel 6125 2700 0    39   BiDi ~ 0
SDIO
Text HLabel 6125 2750 0    39   Input ~ 0
SCLK
Wire Wire Line
	6125 2700 6350 2700
Wire Wire Line
	6125 2750 6350 2750
Wire Wire Line
	6125 2650 6350 2650
Wire Wire Line
	8150 2400 8575 2400
Wire Wire Line
	8150 2450 8575 2450
Text Label 8575 2400 2    39   ~ 0
GPIO1
Text Label 8575 2450 2    39   ~ 0
GPIO2
Text HLabel 8750 2400 2    39   Output ~ 0
GPIO[1..2]
Text HLabel 6150 2250 0    39   Input ~ 0
SyncIn_P
Text HLabel 6150 2300 0    39   Input ~ 0
SyncIn_N
Text HLabel 6150 2450 0    39   Input ~ 0
SysRef_N
Text HLabel 6150 2400 0    39   Input ~ 0
SysRef_P
Wire Wire Line
	6150 2250 6350 2250
Wire Wire Line
	6150 2300 6350 2300
Wire Wire Line
	6150 2400 6350 2400
Wire Wire Line
	6150 2450 6350 2450
Text Notes 800  900  0    100  ~ 0
Input stage simulated with LTspice :\nInput_stage.net
Text Notes 5350 750  0    50   ~ 0
LVDS/LVPECL (400..1600 mV diff pp)\nAC coupled\nInternal bias
Text Notes 4900 3425 0    50   ~ 0
SysRef & SyncIn :\nLVDS/LVPECL (400 .. 1600 mV diff pp)\nVCM = 0.65 .. 2 V\n-> LVDS18\n\nSPI : \nCMOS using SPI_VDDIO (1.8V)\nLVCMOS18
Text Notes 8300 1650 0    50   ~ 0
SST (360 .. 770 mV diff pp)\nVCM = 0.65 .. 2 V
$Comp
L power:GNDA #PWR?
U 1 1 5EB4A9BF
P 5825 2550
AR Path="/59140D2B/5EB4A9BF" Ref="#PWR?"  Part="1" 
AR Path="/5B5208C5/5EB4A9BF" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5EB4A9BF" Ref="#PWR?"  Part="1" 
AR Path="/5EB88B2F/5EB4A9BF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5825 2300 50  0001 C CNN
F 1 "GNDA" H 5825 2400 50  0000 C CNN
F 2 "" H 5825 2550 50  0000 C CNN
F 3 "" H 5825 2550 50  0000 C CNN
	1    5825 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2550 5825 2550
Wire Wire Line
	5225 2000 5450 2000
Wire Wire Line
	5225 1850 5250 1850
Wire Wire Line
	5250 1800 5250 1850
Connection ~ 5250 1850
Wire Wire Line
	5250 1850 5900 1850
Wire Wire Line
	5450 1800 5450 2000
Connection ~ 5450 2000
Wire Wire Line
	5450 2000 5900 2000
Wire Wire Line
	5250 1500 5450 1500
Connection ~ 5450 1500
Wire Wire Line
	5450 1500 5650 1500
$Comp
L Device:R R?
U 1 1 5EB93AD0
P 5625 2275
AR Path="/57059335/5EB93AD0" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5EB93AD0" Ref="R?"  Part="1" 
F 0 "R?" V 5550 2275 50  0000 C CNN
F 1 "100" V 5625 2275 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5555 2275 50  0001 C CNN
F 3 "" H 5625 2275 50  0000 C CNN
	1    5625 2275
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EB948FB
P 5625 2425
AR Path="/57059335/5EB948FB" Ref="R?"  Part="1" 
AR Path="/5EB88B2F/5EB948FB" Ref="R?"  Part="1" 
F 0 "R?" V 5550 2425 50  0000 C CNN
F 1 "100" V 5625 2425 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5555 2425 50  0001 C CNN
F 3 "" H 5625 2425 50  0000 C CNN
	1    5625 2425
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 2250 6150 2225
Wire Wire Line
	6150 2225 5475 2225
Wire Wire Line
	5475 2225 5475 2275
Wire Wire Line
	5775 2275 5775 2325
Wire Wire Line
	5775 2325 6150 2325
Wire Wire Line
	6150 2325 6150 2300
Wire Wire Line
	6150 2400 6150 2375
Wire Wire Line
	6150 2375 5475 2375
Wire Wire Line
	5475 2375 5475 2425
Wire Wire Line
	5775 2475 6150 2475
Wire Wire Line
	6150 2475 6150 2450
Wire Wire Line
	5775 2425 5775 2475
Wire Wire Line
	3200 2175 3200 2975
$EndSCHEMATC
