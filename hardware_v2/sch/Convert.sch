EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 15
Title "FPGA IO banks"
Date "2021-06-18"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8225 4750 0    39   Input ~ 0
SFP_Present
Text HLabel 8225 4800 0    39   Input ~ 0
SFP_LOS
Text HLabel 8225 4850 0    39   Input ~ 0
SFP_Tx_Fault
Text HLabel 8225 4900 0    39   Output ~ 0
SFP_SCL
Text HLabel 8225 4950 0    39   BiDi ~ 0
SFP_SDA
Text HLabel 8225 4625 0    39   Input ~ 0
Clk_In+
Text HLabel 8450 5650 2    39   Output ~ 0
Loc_Rstb
$Comp
L Device:C C1401
U 1 1 57949719
P 9250 1650
F 0 "C1401" V 9200 1800 50  0000 C CNN
F 1 "47u" V 9300 1775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9250 1650 50  0001 C CNN
F 3 "" H 9250 1650 50  0000 C CNN
F 4 "Digi-key" V 9250 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 9250 1650 50  0001 C CNN "Supplier Part"
	1    9250 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C1404
U 1 1 5794972F
P 9250 1875
F 0 "C1404" V 9200 2025 50  0000 C CNN
F 1 "47u" V 9300 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9250 1875 50  0001 C CNN
F 3 "" H 9250 1875 50  0000 C CNN
F 4 "Digi-key" V 9250 1875 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9250 1875 50  0001 C CNN "Supplier Part"
	1    9250 1875
	0    1    1    0   
$EndComp
$Comp
L Device:C C1407
U 1 1 57949737
P 9250 2150
F 0 "C1407" V 9200 2300 50  0000 C CNN
F 1 "10u" V 9300 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9250 2150 50  0001 C CNN
F 3 "" H 9250 2150 50  0000 C CNN
F 4 "Digi-key" V 9250 2150 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9250 2150 50  0001 C CNN "Supplier Part"
	1    9250 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C C1410
U 1 1 5794973F
P 9250 2375
F 0 "C1410" V 9200 2525 50  0000 C CNN
F 1 "10u" V 9300 2525 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9250 2375 50  0001 C CNN
F 3 "" H 9250 2375 50  0000 C CNN
F 4 "Digi-key" V 9250 2375 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9250 2375 50  0001 C CNN "Supplier Part"
	1    9250 2375
	0    1    1    0   
$EndComp
$Comp
L MDJ_compo:VccO #PWR01405
U 1 1 57949755
P 9400 1475
F 0 "#PWR01405" H 9400 1325 50  0001 C CNN
F 1 "VccO" H 9400 1625 50  0000 C CNN
F 2 "" H 9400 1475 50  0000 C CNN
F 3 "" H 9400 1475 50  0000 C CNN
	1    9400 1475
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR01409
U 1 1 5794975B
P 9100 2475
F 0 "#PWR01409" H 9100 2225 50  0001 C CNN
F 1 "GNDD" H 9100 2325 50  0000 C CNN
F 2 "" H 9100 2475 50  0000 C CNN
F 3 "" H 9100 2475 50  0000 C CNN
	1    9100 2475
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1402
U 1 1 5794A207
P 9900 1650
F 0 "C1402" V 9850 1800 50  0000 C CNN
F 1 "47u" V 9950 1800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9900 1650 50  0001 C CNN
F 3 "" H 9900 1650 50  0000 C CNN
F 4 "Digi-key" V 9900 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 9900 1650 50  0001 C CNN "Supplier Part"
	1    9900 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C1405
U 1 1 5794A21D
P 9900 1875
F 0 "C1405" V 9850 2025 50  0000 C CNN
F 1 "47u" V 9950 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9900 1875 50  0001 C CNN
F 3 "" H 9900 1875 50  0000 C CNN
F 4 "Digi-key" V 9900 1875 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9900 1875 50  0001 C CNN "Supplier Part"
	1    9900 1875
	0    1    1    0   
$EndComp
$Comp
L Device:C C1408
U 1 1 5794A225
P 9900 2150
F 0 "C1408" V 9850 2300 50  0000 C CNN
F 1 "10u" V 9950 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9900 2150 50  0001 C CNN
F 3 "" H 9900 2150 50  0000 C CNN
F 4 "Digi-key" V 9900 2150 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9900 2150 50  0001 C CNN "Supplier Part"
	1    9900 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C C1411
U 1 1 5794A22D
P 9900 2375
F 0 "C1411" V 9850 2525 50  0000 C CNN
F 1 "10u" V 9950 2525 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9900 2375 50  0001 C CNN
F 3 "" H 9900 2375 50  0000 C CNN
F 4 "Digi-key" V 9900 2375 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9900 2375 50  0001 C CNN "Supplier Part"
	1    9900 2375
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR01410
U 1 1 5794A249
P 9750 2475
F 0 "#PWR01410" H 9750 2225 50  0001 C CNN
F 1 "GNDD" H 9750 2325 50  0000 C CNN
F 2 "" H 9750 2475 50  0000 C CNN
F 3 "" H 9750 2475 50  0000 C CNN
	1    9750 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1475 9400 1650
Wire Wire Line
	9100 1650 9100 1875
Connection ~ 9400 1650
Wire Wire Line
	10050 1475 10050 1650
Wire Wire Line
	9750 1650 9750 1875
Connection ~ 10050 1650
Connection ~ 9100 2375
Connection ~ 9100 1875
Connection ~ 9100 2150
Connection ~ 9400 2150
Connection ~ 9400 1875
Connection ~ 9750 2375
Connection ~ 10050 2150
Connection ~ 9750 2150
Connection ~ 9750 1875
Connection ~ 10050 1875
Text HLabel 8450 5800 2    39   Output ~ 0
LED[0..7]
Wire Wire Line
	9400 1650 9400 1875
Wire Wire Line
	10050 1650 10050 1875
Wire Wire Line
	9100 1875 9100 2150
Wire Wire Line
	9100 2150 9100 2375
Wire Wire Line
	9400 2150 9400 2375
Wire Wire Line
	9400 1875 9400 2150
Wire Wire Line
	10050 2150 10050 2375
Wire Wire Line
	9750 2150 9750 2375
Wire Wire Line
	9750 1875 9750 2150
Wire Wire Line
	10050 1875 10050 2150
Text HLabel 8225 4675 0    39   Input ~ 0
Clk_In-
$Comp
L MDJ_compo:VccO_ADC #PWR01406
U 1 1 5D4090C2
P 10050 1475
F 0 "#PWR01406" H 10050 1325 50  0001 C CNN
F 1 "VccO_ADC" H 10065 1648 50  0000 C CNN
F 2 "" H 10050 1475 50  0000 C CNN
F 3 "" H 10050 1475 50  0000 C CNN
	1    10050 1475
	1    0    0    -1  
$EndComp
Text HLabel 8450 5900 2    39   BiDi ~ 0
GPIO[0..15]
Text HLabel 8450 4825 2    39   Output ~ 0
PiN3_out_P[0..7]
Text HLabel 8450 4875 2    39   Output ~ 0
PiN3_out_N[0..7]
Text HLabel 8450 5400 2    39   Output ~ 0
SPI_ADC_CLK
Text HLabel 8450 5350 2    39   BiDi ~ 0
SPI_ADC_DIO
Text HLabel 8450 5450 2    39   Output ~ 0
SPI_ADC_CSb[1..3]
$Comp
L Device:R R?
U 1 1 628AC697
P 1600 6875
AR Path="/5BAC43A6/628AC697" Ref="R?"  Part="1" 
AR Path="/57195CF6/628AC697" Ref="R1401"  Part="1" 
F 0 "R1401" V 1675 6875 50  0000 C CNN
F 1 "1k" V 1600 6875 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 1600 6875 50  0001 C CNN
F 3 "" H 1600 6875 50  0000 C CNN
	1    1600 6875
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR01414
U 1 1 6295984A
P 1600 6725
F 0 "#PWR01414" H 1600 6575 50  0001 C CNN
F 1 "VccO" H 1600 6875 50  0000 C CNN
F 2 "" H 1600 6725 50  0000 C CNN
F 3 "" H 1600 6725 50  0000 C CNN
	1    1600 6725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 7025 1600 7150
Text HLabel 9275 5525 2    39   Output ~ 0
I2C_SCL_Clk
Text HLabel 9275 5475 2    39   BiDi ~ 0
I2C_SDA_Clk
Text HLabel 9275 5575 2    39   Output ~ 0
Clk_Rstb
Text HLabel 9275 5625 2    39   Output ~ 0
Clk_OK[1..2]
Text HLabel 9275 5675 2    39   Output ~ 0
Clk_LoL[1..2]
Text HLabel 8450 5500 2    39   BiDi ~ 0
ADC_GPIO1_[1..2]
Text HLabel 9100 4700 2    39   Output ~ 0
Dummy_out_P
Text HLabel 9100 4750 2    39   Output ~ 0
Dummy_out_N
Text HLabel 9100 4950 2    39   Output ~ 0
ReSync_In_P
Text HLabel 9100 5000 2    39   Output ~ 0
ReSync_In_N
$Comp
L MDJ_compo:XCKU035FBVA676 U?
U 2 1 60D0FDDE
P 2075 1175
AR Path="/60D0FDDE" Ref="U?"  Part="2" 
AR Path="/57195CF6/60D0FDDE" Ref="U302"  Part="2" 
F 0 "U302" H 3300 -1300 40  0000 C CNN
F 1 "XCKU035FBVA676" H 3325 -1400 40  0000 C CNN
F 2 "Package_BGA:BGA-676_27.0x27.0mm_Layout26x26_P1.0mm_Ball0.6mm_Pad0.5mm_NSMD" H 2125 1200 40  0001 R CNN
F 3 "" H 2125 1025 40  0001 R CNN
	2    2075 1175
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:XCKU035FBVA676 U?
U 4 1 60E25161
P 2075 4750
AR Path="/60E25161" Ref="U?"  Part="4" 
AR Path="/57195CF6/60E25161" Ref="U302"  Part="4" 
F 0 "U302" H 3425 2275 40  0000 C CNN
F 1 "XCKU035FBVA676" H 3450 2200 40  0000 C CNN
F 2 "Package_BGA:BGA-676_27.0x27.0mm_Layout26x26_P1.0mm_Ball0.6mm_Pad0.5mm_NSMD" H 2125 4775 40  0001 R CNN
F 3 "" H 2125 4600 40  0001 R CNN
	4    2075 4750
	1    0    0    -1  
$EndComp
NoConn ~ 15275 5950
NoConn ~ 15275 6150
NoConn ~ 15275 6000
Text HLabel 8450 5550 2    39   BiDi ~ 0
ADC_GPIO2_[1..2]
Text HLabel 8450 5600 2    39   BiDi ~ 0
ADC_GPIO3_[1..2]
Text Label 1650 5450 0    30   ~ 0
Clk_Rstb
Wire Wire Line
	1650 5450 2075 5450
Text Label 1650 5300 0    30   ~ 0
Clk_LoL1
Wire Wire Line
	1650 5300 2075 5300
Wire Wire Line
	1650 5250 2075 5250
Text Label 1650 5250 0    30   ~ 0
Clk_OK1
Text Label 2050 7150 2    30   ~ 0
I2C_SCL_Clk
Wire Wire Line
	1600 7150 2075 7150
Wire Wire Line
	1750 7100 2075 7100
Wire Wire Line
	1750 7025 1750 7100
Connection ~ 1600 6725
Wire Wire Line
	1600 6725 1750 6725
$Comp
L Device:R R?
U 1 1 628AC68D
P 1750 6875
AR Path="/5BAC43A6/628AC68D" Ref="R?"  Part="1" 
AR Path="/57195CF6/628AC68D" Ref="R1402"  Part="1" 
F 0 "R1402" V 1825 6875 50  0000 C CNN
F 1 "1k" V 1750 6875 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 1750 6875 50  0001 C CNN
F 3 "" H 1750 6875 50  0000 C CNN
	1    1750 6875
	-1   0    0    -1  
$EndComp
Text Label 2050 7100 2    30   ~ 0
I2C_SDA_Clk
Wire Wire Line
	1925 6950 2075 6950
Wire Wire Line
	1925 6900 2075 6900
Wire Wire Line
	1925 7050 2075 7050
Wire Wire Line
	1925 7000 2075 7000
Text Label 1925 7050 2    30   ~ 0
LED2
Text Label 1925 7000 2    30   ~ 0
LED0
Text Label 1925 6950 2    30   ~ 0
LED7
Text Label 1925 6900 2    30   ~ 0
LED6
Wire Wire Line
	2075 6450 1925 6450
Text Label 1925 6450 2    30   ~ 0
SFP_Tx_Fault
Wire Wire Line
	2075 6550 1925 6550
Wire Wire Line
	2075 6500 1925 6500
Text Label 1925 6500 2    30   ~ 0
SFP_Present
Text Label 1925 6550 2    30   ~ 0
SFP_LOS
Text Label 1925 6650 2    30   ~ 0
SFP_SCL
Text Label 1925 6600 2    30   ~ 0
SFP_SDA
Wire Wire Line
	2075 6600 1925 6600
Wire Wire Line
	2075 6650 1925 6650
Wire Wire Line
	1925 6750 2075 6750
Wire Wire Line
	1925 6700 2075 6700
Wire Wire Line
	1925 6850 2075 6850
Wire Wire Line
	1925 6800 2075 6800
Text Label 1925 6850 2    30   ~ 0
LED3
Text Label 1925 6800 2    30   ~ 0
LED1
Text Label 1925 6750 2    30   ~ 0
LED5
Text Label 1925 6700 2    30   ~ 0
LED4
Wire Wire Line
	5400 2175 5000 2175
Text Label 5000 2175 0    35   ~ 0
Clk_In+
Wire Wire Line
	5400 2225 5000 2225
Text Label 5000 2225 0    35   ~ 0
Clk_In-
$Comp
L MDJ_compo:VccO_ADC #PWR01401
U 1 1 60DFAD46
P 2925 725
F 0 "#PWR01401" H 2925 575 50  0001 C CNN
F 1 "VccO_ADC" H 2940 898 50  0000 C CNN
F 2 "" H 2925 725 50  0000 C CNN
F 3 "" H 2925 725 50  0000 C CNN
	1    2925 725 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2925 725  2975 725 
Connection ~ 2925 725 
Wire Wire Line
	2975 725  3025 725 
Connection ~ 2975 725 
Wire Wire Line
	3025 725  3075 725 
Connection ~ 3025 725 
Wire Wire Line
	3075 725  3125 725 
Connection ~ 3075 725 
Wire Wire Line
	3125 725  3175 725 
Connection ~ 3125 725 
Wire Wire Line
	3175 725  3225 725 
Connection ~ 3175 725 
$Comp
L MDJ_compo:XCKU035FBVA676 U?
U 3 1 6159D663
P 5400 1175
AR Path="/6159D663" Ref="U?"  Part="3" 
AR Path="/57195CF6/6159D663" Ref="U302"  Part="3" 
F 0 "U302" H 6625 -1741 40  0000 C CNN
F 1 "XCKU035FBVA676" H 6625 -1817 40  0000 C CNN
F 2 "Package_BGA:BGA-676_27.0x27.0mm_Layout26x26_P1.0mm_Ball0.6mm_Pad0.5mm_NSMD" H 5450 1200 40  0001 R CNN
F 3 "" H 5450 1025 40  0001 R CNN
	3    5400 1175
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1403
U 1 1 615C62A5
P 10550 1650
F 0 "C1403" V 10500 1800 50  0000 C CNN
F 1 "47u" V 10600 1800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10550 1650 50  0001 C CNN
F 3 "" H 10550 1650 50  0000 C CNN
F 4 "Digi-key" V 10550 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 10550 1650 50  0001 C CNN "Supplier Part"
	1    10550 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C1406
U 1 1 615C747D
P 10550 1875
F 0 "C1406" V 10500 2025 50  0000 C CNN
F 1 "47u" V 10600 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10550 1875 50  0001 C CNN
F 3 "" H 10550 1875 50  0000 C CNN
F 4 "Digi-key" V 10550 1875 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 10550 1875 50  0001 C CNN "Supplier Part"
	1    10550 1875
	0    1    1    0   
$EndComp
$Comp
L Device:C C1409
U 1 1 615C7489
P 10550 2150
F 0 "C1409" V 10500 2300 50  0000 C CNN
F 1 "10u" V 10600 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10550 2150 50  0001 C CNN
F 3 "" H 10550 2150 50  0000 C CNN
F 4 "Digi-key" V 10550 2150 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 10550 2150 50  0001 C CNN "Supplier Part"
	1    10550 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C C1412
U 1 1 615C7495
P 10550 2375
F 0 "C1412" V 10500 2525 50  0000 C CNN
F 1 "10u" V 10600 2525 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10550 2375 50  0001 C CNN
F 3 "" H 10550 2375 50  0000 C CNN
F 4 "Digi-key" V 10550 2375 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 10550 2375 50  0001 C CNN "Supplier Part"
	1    10550 2375
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR01411
U 1 1 615C74B7
P 10400 2475
F 0 "#PWR01411" H 10400 2225 50  0001 C CNN
F 1 "GNDD" H 10400 2325 50  0000 C CNN
F 2 "" H 10400 2475 50  0000 C CNN
F 3 "" H 10400 2475 50  0000 C CNN
	1    10400 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 1475 10700 1650
Wire Wire Line
	10400 1650 10400 1875
Connection ~ 10700 1650
Connection ~ 10400 2375
Connection ~ 10700 2150
Connection ~ 10400 2150
Connection ~ 10400 1875
Connection ~ 10700 1875
Wire Wire Line
	10700 1650 10700 1875
Wire Wire Line
	10700 2150 10700 2375
Wire Wire Line
	10400 2150 10400 2375
Wire Wire Line
	10400 1875 10400 2150
Wire Wire Line
	10700 1875 10700 2150
$Comp
L MDJ_compo:VccO_FE #PWR01407
U 1 1 615CD525
P 10700 1475
F 0 "#PWR01407" H 10700 1325 50  0001 C CNN
F 1 "VccO_FE" H 10715 1648 50  0000 C CNN
F 2 "" H 10700 1475 50  0000 C CNN
F 3 "" H 10700 1475 50  0000 C CNN
	1    10700 1475
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO_FE #PWR01404
U 1 1 615CDC3E
P 7000 725
F 0 "#PWR01404" H 7000 575 50  0001 C CNN
F 1 "VccO_FE" H 7015 898 50  0000 C CNN
F 2 "" H 7000 725 50  0000 C CNN
F 3 "" H 7000 725 50  0000 C CNN
	1    7000 725 
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO_FE #PWR01403
U 1 1 615CF2D1
P 6250 725
F 0 "#PWR01403" H 6250 575 50  0001 C CNN
F 1 "VccO_FE" H 6265 898 50  0000 C CNN
F 2 "" H 6250 725 50  0000 C CNN
F 3 "" H 6250 725 50  0000 C CNN
	1    6250 725 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 725  6300 725 
Connection ~ 6250 725 
Wire Wire Line
	6300 725  6350 725 
Connection ~ 6300 725 
Wire Wire Line
	6350 725  6400 725 
Connection ~ 6350 725 
Wire Wire Line
	6400 725  6450 725 
Connection ~ 6400 725 
Wire Wire Line
	6450 725  6500 725 
Connection ~ 6450 725 
Wire Wire Line
	6500 725  6550 725 
Connection ~ 6500 725 
Wire Wire Line
	6700 725  6750 725 
Wire Wire Line
	6750 725  6800 725 
Connection ~ 6750 725 
Wire Wire Line
	6800 725  6850 725 
Connection ~ 6800 725 
Wire Wire Line
	6850 725  6900 725 
Connection ~ 6850 725 
Wire Wire Line
	6900 725  6950 725 
Connection ~ 6900 725 
Wire Wire Line
	6950 725  7000 725 
Connection ~ 6950 725 
Connection ~ 7000 725 
$Comp
L MDJ_compo:VccO #PWR01413
U 1 1 615F811D
P 3625 4300
F 0 "#PWR01413" H 3625 4150 50  0001 C CNN
F 1 "VccO" H 3625 4450 50  0000 C CNN
F 2 "" H 3625 4300 50  0000 C CNN
F 3 "" H 3625 4300 50  0000 C CNN
	1    3625 4300
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR01412
U 1 1 6160A51D
P 2975 4300
F 0 "#PWR01412" H 2975 4150 50  0001 C CNN
F 1 "VccO" H 2975 4450 50  0000 C CNN
F 2 "" H 2975 4300 50  0000 C CNN
F 3 "" H 2975 4300 50  0000 C CNN
	1    2975 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3375 4300 3425 4300
Wire Wire Line
	2975 4300 3025 4300
Wire Wire Line
	3175 4300 3225 4300
Wire Wire Line
	3575 4300 3625 4300
Connection ~ 2975 4300
Connection ~ 3625 4300
Wire Wire Line
	3025 4300 3075 4300
Connection ~ 3025 4300
Wire Wire Line
	3075 4300 3125 4300
Connection ~ 3075 4300
Wire Wire Line
	3125 4300 3175 4300
Connection ~ 3125 4300
Connection ~ 3175 4300
Wire Wire Line
	3425 4300 3475 4300
Connection ~ 3425 4300
Wire Wire Line
	3475 4300 3525 4300
Connection ~ 3475 4300
Wire Wire Line
	3525 4300 3575 4300
Connection ~ 3525 4300
Connection ~ 3575 4300
Text HLabel 8450 4725 2    39   Output ~ 0
PiN2_out_P[0..7]
Text HLabel 8450 4775 2    39   Output ~ 0
PiN2_out_N[0..7]
Text HLabel 8450 4625 2    39   Output ~ 0
PiN1_out_P[0..7]
Text HLabel 8450 4675 2    39   Output ~ 0
PiN1_out_N[0..7]
Text HLabel 8450 5975 2    39   BiDi ~ 0
GPIO_P[0..3]
Text HLabel 8450 6025 2    39   BiDi ~ 0
GPIO_N[0..3]
Text HLabel 8450 6100 2    39   Output ~ 0
Laser_trigger[1..8]
Text HLabel 8450 6175 2    39   Input ~ 0
FB_PiN[1..2]
Wire Wire Line
	2075 4750 1650 4750
Text Label 1650 4750 0    39   ~ 0
Laser_trigger1
Wire Wire Line
	2075 4800 1650 4800
Text Label 1650 4800 0    39   ~ 0
Laser_trigger2
Wire Wire Line
	2075 4850 1650 4850
Text Label 1650 4850 0    39   ~ 0
Laser_trigger3
Wire Wire Line
	2075 4900 1650 4900
Text Label 1650 4900 0    39   ~ 0
Laser_trigger4
Wire Wire Line
	2075 4950 1650 4950
Text Label 1650 4950 0    39   ~ 0
Laser_trigger5
Wire Wire Line
	2075 5000 1650 5000
Text Label 1650 5000 0    39   ~ 0
Laser_trigger6
Wire Wire Line
	2075 5050 1650 5050
Text Label 1650 5050 0    39   ~ 0
Laser_trigger7
Wire Wire Line
	2075 5100 1650 5100
Text Label 1650 5100 0    39   ~ 0
Laser_trigger8
Wire Wire Line
	2075 5150 1650 5150
Text Label 1650 5150 0    39   ~ 0
FB_PiN1
Wire Wire Line
	2075 5200 1650 5200
Text Label 1650 5200 0    39   ~ 0
FB_PiN2
NoConn ~ 2075 1725
NoConn ~ 2075 1775
NoConn ~ 2075 1825
NoConn ~ 2075 1875
NoConn ~ 2075 1925
NoConn ~ 2075 1975
NoConn ~ 2075 2025
NoConn ~ 2075 2075
NoConn ~ 2075 2125
NoConn ~ 2075 2375
NoConn ~ 2075 2425
NoConn ~ 2075 2475
NoConn ~ 2075 2525
NoConn ~ 2075 2575
NoConn ~ 2075 2625
NoConn ~ 2075 2675
NoConn ~ 2075 2725
NoConn ~ 2075 2775
NoConn ~ 2075 2825
NoConn ~ 2075 2975
NoConn ~ 2075 3025
NoConn ~ 2075 3175
NoConn ~ 2075 3225
NoConn ~ 2075 2175
NoConn ~ 2075 2225
NoConn ~ 2075 3375
NoConn ~ 2075 3425
NoConn ~ 2075 3475
NoConn ~ 2075 3525
NoConn ~ 2075 3625
NoConn ~ 2075 3675
NoConn ~ 2075 3725
NoConn ~ 2075 3775
NoConn ~ 4525 1175
NoConn ~ 4525 1225
NoConn ~ 4525 1275
NoConn ~ 4525 1325
NoConn ~ 4525 1375
NoConn ~ 4525 1425
NoConn ~ 4525 1475
NoConn ~ 4525 1525
NoConn ~ 4525 1575
NoConn ~ 4525 1625
NoConn ~ 4525 1675
NoConn ~ 4525 1725
NoConn ~ 4525 1775
NoConn ~ 4525 1825
NoConn ~ 4525 1875
NoConn ~ 4525 1925
NoConn ~ 4525 1975
NoConn ~ 4525 2025
NoConn ~ 4525 2075
NoConn ~ 4525 2125
NoConn ~ 4525 2175
NoConn ~ 4525 2225
NoConn ~ 4525 2275
NoConn ~ 4525 2325
NoConn ~ 4525 2375
NoConn ~ 4525 2425
NoConn ~ 4525 2475
NoConn ~ 4525 2525
NoConn ~ 4525 2575
NoConn ~ 4525 2625
NoConn ~ 4525 2675
NoConn ~ 4525 2725
NoConn ~ 4525 2775
NoConn ~ 4525 2825
NoConn ~ 4525 2875
NoConn ~ 4525 2925
NoConn ~ 4525 2975
NoConn ~ 4525 3025
NoConn ~ 4525 3075
NoConn ~ 4525 3125
NoConn ~ 4525 3175
NoConn ~ 4525 3225
NoConn ~ 4525 3275
NoConn ~ 4525 3325
NoConn ~ 4525 3375
NoConn ~ 4525 3425
NoConn ~ 4525 3475
NoConn ~ 4525 3525
NoConn ~ 4525 3625
NoConn ~ 4525 3675
NoConn ~ 4525 3725
NoConn ~ 4525 3775
NoConn ~ 5400 1975
NoConn ~ 5400 2025
NoConn ~ 5400 2075
NoConn ~ 5400 2125
NoConn ~ 5400 3625
NoConn ~ 5400 3675
NoConn ~ 5400 3725
NoConn ~ 5400 3775
NoConn ~ 7850 3325
NoConn ~ 7850 3375
NoConn ~ 7850 3425
NoConn ~ 7850 3475
NoConn ~ 7850 3525
NoConn ~ 7850 3625
NoConn ~ 7850 3675
NoConn ~ 7850 3725
NoConn ~ 7850 3775
NoConn ~ 4525 4750
NoConn ~ 4525 4800
NoConn ~ 4525 4850
NoConn ~ 4525 4900
NoConn ~ 4525 6950
NoConn ~ 4525 7000
NoConn ~ 4525 7050
NoConn ~ 4525 7100
NoConn ~ 4525 7150
NoConn ~ 4525 7200
NoConn ~ 4525 7250
NoConn ~ 4525 7300
NoConn ~ 4525 7350
NoConn ~ 2075 5600
NoConn ~ 2075 5650
NoConn ~ 2075 5700
NoConn ~ 2075 5750
NoConn ~ 2075 5800
NoConn ~ 2075 5850
NoConn ~ 2075 5900
NoConn ~ 2075 5950
NoConn ~ 2075 6000
NoConn ~ 2075 6050
NoConn ~ 2075 6100
NoConn ~ 2075 6150
NoConn ~ 2075 6200
NoConn ~ 2075 6250
NoConn ~ 2075 6300
NoConn ~ 2075 6350
NoConn ~ 2075 6400
$Comp
L MDJ_compo:VccO_ADC #PWR01402
U 1 1 6292B329
P 3675 725
F 0 "#PWR01402" H 3675 575 50  0001 C CNN
F 1 "VccO_ADC" H 3690 898 50  0000 C CNN
F 2 "" H 3675 725 50  0000 C CNN
F 3 "" H 3675 725 50  0000 C CNN
	1    3675 725 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3375 725  3425 725 
Wire Wire Line
	3425 725  3475 725 
Connection ~ 3425 725 
Wire Wire Line
	3475 725  3525 725 
Connection ~ 3475 725 
Wire Wire Line
	3525 725  3575 725 
Connection ~ 3525 725 
Wire Wire Line
	3575 725  3625 725 
Connection ~ 3575 725 
Wire Wire Line
	3625 725  3675 725 
Connection ~ 3625 725 
Connection ~ 3675 725 
Wire Wire Line
	5400 1175 5000 1175
Text Label 5000 1175 0    35   ~ 0
PiN1_out_P3
Wire Wire Line
	5400 1225 5000 1225
Text Label 5000 1225 0    35   ~ 0
PiN1_out_N3
Wire Wire Line
	5400 1275 5000 1275
Text Label 5000 1275 0    35   ~ 0
PiN1_out_P0
Wire Wire Line
	5400 1325 5000 1325
Wire Wire Line
	5400 1375 5000 1375
Text Label 5000 1375 0    35   ~ 0
PiN1_out_P2
Wire Wire Line
	5400 1425 5000 1425
Text Label 5000 1425 0    35   ~ 0
PiN1_out_N2
Wire Wire Line
	5400 1475 5000 1475
Text Label 5000 1475 0    35   ~ 0
PiN1_out_P5
Wire Wire Line
	5400 1525 5000 1525
Text Label 5000 1525 0    35   ~ 0
PiN1_out_N5
Wire Wire Line
	5400 1575 5000 1575
Text Label 5000 1575 0    35   ~ 0
PiN1_out_P4
Wire Wire Line
	5400 1625 5000 1625
Text Label 5000 1625 0    35   ~ 0
PiN1_out_N4
Wire Wire Line
	5400 1675 5000 1675
Text Label 5000 1675 0    35   ~ 0
PiN1_out_P1
Wire Wire Line
	5400 1725 5000 1725
Text Label 5000 1725 0    35   ~ 0
PiN1_out_N1
Wire Wire Line
	5400 1775 5000 1775
Text Label 5000 1775 0    35   ~ 0
PiN1_out_P6
Wire Wire Line
	5400 1825 5000 1825
Text Label 5000 1825 0    35   ~ 0
PiN1_out_N6
Wire Wire Line
	5400 1875 5000 1875
Text Label 5000 1875 0    35   ~ 0
PiN1_out_P7
Wire Wire Line
	5400 1925 5000 1925
Text Label 5000 1925 0    35   ~ 0
PiN1_out_N7
Text Label 5000 1325 0    35   ~ 0
PiN1_out_N0
Wire Wire Line
	7850 1175 8250 1175
Text Label 8250 1175 2    35   ~ 0
PiN2_out_P2
Wire Wire Line
	7850 1225 8250 1225
Text Label 8250 1225 2    35   ~ 0
PiN2_out_N2
Wire Wire Line
	7850 1275 8250 1275
Text Label 8250 1275 2    35   ~ 0
PiN2_out_P0
Wire Wire Line
	7850 1325 8250 1325
Wire Wire Line
	7850 1375 8250 1375
Text Label 8250 1375 2    35   ~ 0
PiN2_out_P3
Wire Wire Line
	7850 1425 8250 1425
Text Label 8250 1425 2    35   ~ 0
PiN2_out_N3
Wire Wire Line
	7850 1475 8250 1475
Text Label 8250 1475 2    35   ~ 0
PiN2_out_P1
Wire Wire Line
	7850 1525 8250 1525
Text Label 8250 1525 2    35   ~ 0
PiN2_out_N1
Wire Wire Line
	7850 1575 8250 1575
Text Label 8250 1575 2    35   ~ 0
PiN2_out_P5
Wire Wire Line
	7850 1625 8250 1625
Wire Wire Line
	7850 1675 8250 1675
Text Label 8250 1675 2    35   ~ 0
PiN2_out_P4
Wire Wire Line
	7850 1725 8250 1725
Text Label 8250 1725 2    35   ~ 0
PiN2_out_N4
Wire Wire Line
	7850 1775 8250 1775
Text Label 8250 1775 2    35   ~ 0
PiN3_out_P1
Wire Wire Line
	7850 1825 8250 1825
Text Label 8250 1825 2    35   ~ 0
PiN3_out_N1
Wire Wire Line
	7850 1875 8250 1875
Text Label 8250 1875 2    35   ~ 0
PiN2_out_P7
Wire Wire Line
	7850 1925 8250 1925
Text Label 8250 1925 2    35   ~ 0
PiN2_out_N7
Text Label 8250 1325 2    35   ~ 0
PiN2_out_N0
Wire Wire Line
	7850 1975 8250 1975
Text Label 8250 1975 2    35   ~ 0
PiN3_out_P0
Wire Wire Line
	7850 2025 8250 2025
Text Label 8250 2025 2    35   ~ 0
PiN3_out_N0
Wire Wire Line
	7850 2075 8250 2075
Text Label 8250 2075 2    35   ~ 0
PiN2_out_P6
Wire Wire Line
	7850 2125 8250 2125
Wire Wire Line
	7850 2175 8250 2175
Text Label 8250 2175 2    35   ~ 0
PiN3_out_P2
Wire Wire Line
	7850 2225 8250 2225
Text Label 8250 2225 2    35   ~ 0
PiN3_out_N2
Wire Wire Line
	7850 2275 8250 2275
Text Label 8250 2275 2    35   ~ 0
PiN3_out_P3
Wire Wire Line
	7850 2325 8250 2325
Text Label 8250 2325 2    35   ~ 0
PiN3_out_N3
Wire Wire Line
	7850 2375 8250 2375
Text Label 8250 2375 2    35   ~ 0
PiN3_out_P4
Wire Wire Line
	7850 2425 8250 2425
Text Label 8250 2425 2    35   ~ 0
PiN3_out_N4
Wire Wire Line
	7850 2475 8250 2475
Text Label 8250 2475 2    35   ~ 0
PiN3_out_P5
Wire Wire Line
	7850 2525 8250 2525
Text Label 8250 2525 2    35   ~ 0
PiN3_out_N5
Wire Wire Line
	7850 2575 8250 2575
Text Label 8250 2575 2    35   ~ 0
PiN3_out_P6
Wire Wire Line
	7850 2625 8250 2625
Text Label 8250 2625 2    35   ~ 0
PiN3_out_N6
Wire Wire Line
	7850 2675 8250 2675
Text Label 8250 2675 2    35   ~ 0
PiN3_out_P7
Wire Wire Line
	7850 2725 8250 2725
Text Label 8250 2725 2    35   ~ 0
PiN3_out_N7
Text Label 8250 2125 2    35   ~ 0
PiN2_out_N6
NoConn ~ 2075 2275
NoConn ~ 2075 2325
Wire Wire Line
	5400 2275 5000 2275
Text Label 5000 2275 0    35   ~ 0
ReSync_In_P
Wire Wire Line
	5400 2325 5000 2325
Text Label 5000 2325 0    35   ~ 0
ReSync_In_N
Wire Wire Line
	5400 2375 5000 2375
Text Label 5000 2375 0    35   ~ 0
Dummy_out_P
Wire Wire Line
	5400 2425 5000 2425
Text Label 5000 2425 0    35   ~ 0
Dummy_out_N
Wire Wire Line
	5400 2475 5000 2475
Text Label 5000 2475 0    35   ~ 0
Pll1_Lock0
Wire Wire Line
	5400 2525 5000 2525
Wire Wire Line
	5400 2575 5000 2575
Text Label 5000 2575 0    35   ~ 0
Pll1_Lock2
Wire Wire Line
	5400 2625 5000 2625
Text Label 5000 2625 0    35   ~ 0
Pll1_Lock3
Wire Wire Line
	5400 2675 5000 2675
Text Label 5000 2675 0    35   ~ 0
Pll1_Lock4
Wire Wire Line
	5400 2725 5000 2725
Text Label 5000 2725 0    35   ~ 0
Pll1_Lock5
Wire Wire Line
	5400 2775 5000 2775
Text Label 5000 2775 0    35   ~ 0
Pll1_Lock6
Wire Wire Line
	5400 2825 5000 2825
Text Label 5000 2825 0    35   ~ 0
Pll1_Lock7
Text Label 5000 2525 0    35   ~ 0
Pll1_Lock1
Wire Wire Line
	5400 2875 5000 2875
Text Label 5000 2875 0    35   ~ 0
Pll2_Lock0
Wire Wire Line
	5400 2925 5000 2925
Wire Wire Line
	5400 2975 5000 2975
Text Label 5000 2975 0    35   ~ 0
Pll2_Lock2
Wire Wire Line
	5400 3025 5000 3025
Text Label 5000 3025 0    35   ~ 0
Pll2_Lock3
Wire Wire Line
	5400 3075 5000 3075
Text Label 5000 3075 0    35   ~ 0
Pll2_Lock4
Wire Wire Line
	5400 3125 5000 3125
Text Label 5000 3125 0    35   ~ 0
Pll2_Lock5
Wire Wire Line
	5400 3175 5000 3175
Text Label 5000 3175 0    35   ~ 0
Pll2_Lock6
Wire Wire Line
	5400 3225 5000 3225
Text Label 5000 3225 0    35   ~ 0
Pll2_Lock7
Text Label 5000 2925 0    35   ~ 0
Pll2_Lock1
NoConn ~ 5400 3325
NoConn ~ 5400 3375
NoConn ~ 5400 3425
NoConn ~ 5400 3475
NoConn ~ 5400 3525
Wire Wire Line
	7850 2775 8250 2775
Text Label 8250 2775 2    35   ~ 0
Pll3_Lock0
Wire Wire Line
	7850 2825 8250 2825
Wire Wire Line
	7850 2875 8250 2875
Text Label 8250 2875 2    35   ~ 0
Pll3_Lock2
Wire Wire Line
	7850 2925 8250 2925
Text Label 8250 2925 2    35   ~ 0
Pll3_Lock3
Wire Wire Line
	7850 2975 8250 2975
Text Label 8250 2975 2    35   ~ 0
Pll3_Lock4
Wire Wire Line
	7850 3025 8250 3025
Text Label 8250 3025 2    35   ~ 0
Pll3_Lock5
Wire Wire Line
	7850 3075 8250 3075
Text Label 8250 3075 2    35   ~ 0
Pll3_Lock6
Wire Wire Line
	7850 3125 8250 3125
Text Label 8250 3125 2    35   ~ 0
Pll3_Lock7
Text Label 8250 2825 2    35   ~ 0
Pll3_Lock1
Wire Wire Line
	7850 3175 8250 3175
Text Label 8250 3175 2    35   ~ 0
PllD_Lock
Text HLabel 8450 4950 2    39   Output ~ 0
Pll1_Lock[0..7]
Text HLabel 8450 5000 2    39   Output ~ 0
Pll2_Lock[0..7]
Text HLabel 8450 5050 2    39   Output ~ 0
Pll3_Lock[0..7]
Text HLabel 8450 5100 2    39   Output ~ 0
PllD_Lock
Text HLabel 8450 5225 2    39   Output ~ 0
I2C_SCL_FE
Text HLabel 8450 5175 2    39   BiDi ~ 0
I2C_SDA_FE
Wire Wire Line
	7850 3225 8250 3225
Text Label 8250 3225 2    35   ~ 0
I2C_SCL_FE
Wire Wire Line
	7850 3275 8250 3275
Text Label 8250 3275 2    35   ~ 0
I2C_SDA_FE
Text Label 4925 6850 2    35   ~ 0
GPIO0
Wire Wire Line
	4525 6850 4925 6850
Text Label 4925 6650 2    35   ~ 0
GPIO1
Wire Wire Line
	4525 6650 4925 6650
Text Label 4925 6450 2    35   ~ 0
GPIO2
Wire Wire Line
	4525 6450 4925 6450
Text Label 4925 5650 2    35   ~ 0
GPIO4
Wire Wire Line
	4525 5650 4925 5650
Text Label 4925 6250 2    35   ~ 0
GPIO3
Wire Wire Line
	4525 6250 4925 6250
Text Label 4925 5050 2    35   ~ 0
GPIO7
Wire Wire Line
	4525 5050 4925 5050
Text Label 4925 6750 2    35   ~ 0
GPIO8
Wire Wire Line
	4525 6750 4925 6750
Text Label 4925 5450 2    35   ~ 0
GPIO5
Wire Wire Line
	4525 5450 4925 5450
Text Label 4925 5250 2    35   ~ 0
GPIO6
Wire Wire Line
	4525 5250 4925 5250
Text Label 4925 6150 2    35   ~ 0
GPIO11
Wire Wire Line
	4525 6150 4925 6150
Text Label 4925 5550 2    35   ~ 0
GPIO12
Wire Wire Line
	4525 5550 4925 5550
Text Label 4925 6550 2    35   ~ 0
GPIO9
Wire Wire Line
	4525 6550 4925 6550
Text Label 4925 6350 2    35   ~ 0
GPIO10
Wire Wire Line
	4525 6350 4925 6350
Text Label 4925 5150 2    35   ~ 0
GPIO14
Wire Wire Line
	4525 5150 4925 5150
Text Label 4925 4950 2    35   ~ 0
GPIO15
Wire Wire Line
	4525 4950 4925 4950
Text Label 4925 5350 2    35   ~ 0
GPIO13
Wire Wire Line
	4525 5350 4925 5350
Wire Wire Line
	4525 5000 4600 5000
Wire Wire Line
	4600 5000 4600 5100
$Comp
L power:GNDD #PWR01415
U 1 1 64195BD5
P 4600 7400
F 0 "#PWR01415" H 4600 7150 50  0001 C CNN
F 1 "GNDD" H 4600 7250 50  0000 C CNN
F 2 "" H 4600 7400 50  0000 C CNN
F 3 "" H 4600 7400 50  0000 C CNN
	1    4600 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 6900 4600 6900
Connection ~ 4600 6900
Wire Wire Line
	4600 6900 4600 7400
Wire Wire Line
	4525 6800 4600 6800
Connection ~ 4600 6800
Wire Wire Line
	4600 6800 4600 6900
Wire Wire Line
	4525 6700 4600 6700
Connection ~ 4600 6700
Wire Wire Line
	4600 6700 4600 6800
Wire Wire Line
	4525 6600 4600 6600
Connection ~ 4600 6600
Wire Wire Line
	4600 6600 4600 6700
Wire Wire Line
	4525 6500 4600 6500
Connection ~ 4600 6500
Wire Wire Line
	4600 6500 4600 6600
Wire Wire Line
	4525 6400 4600 6400
Connection ~ 4600 6400
Wire Wire Line
	4600 6400 4600 6500
Wire Wire Line
	4525 6300 4600 6300
Connection ~ 4600 6300
Wire Wire Line
	4600 6300 4600 6400
Wire Wire Line
	4525 6200 4600 6200
Connection ~ 4600 6200
Wire Wire Line
	4600 6200 4600 6300
Wire Wire Line
	4525 5700 4600 5700
Connection ~ 4600 5700
Wire Wire Line
	4600 5700 4600 6200
Wire Wire Line
	4525 5600 4600 5600
Connection ~ 4600 5600
Wire Wire Line
	4600 5600 4600 5700
Wire Wire Line
	4525 5500 4600 5500
Connection ~ 4600 5500
Wire Wire Line
	4600 5500 4600 5600
Wire Wire Line
	4525 5400 4600 5400
Connection ~ 4600 5400
Wire Wire Line
	4600 5400 4600 5500
Wire Wire Line
	4525 5300 4600 5300
Connection ~ 4600 5300
Wire Wire Line
	4600 5300 4600 5400
Wire Wire Line
	4525 5200 4600 5200
Connection ~ 4600 5200
Wire Wire Line
	4600 5200 4600 5300
Wire Wire Line
	4525 5100 4600 5100
Connection ~ 4600 5100
Wire Wire Line
	4600 5100 4600 5200
Text Notes 5075 4750 0    50   ~ 0
GPIO connected to\nADC inputs
Text Label 4925 5750 2    35   ~ 0
GPIO_P0
Wire Wire Line
	4525 5750 4925 5750
Text Label 4925 5800 2    35   ~ 0
GPIO_N0
Wire Wire Line
	4525 5800 4925 5800
Wire Wire Line
	4525 5850 4925 5850
Text Label 4925 5900 2    35   ~ 0
GPIO_N1
Wire Wire Line
	4525 5900 4925 5900
Text Label 4925 5950 2    35   ~ 0
GPIO_P2
Wire Wire Line
	4525 5950 4925 5950
Text Label 4925 6000 2    35   ~ 0
GPIO_N2
Wire Wire Line
	4525 6000 4925 6000
Text Label 4925 6050 2    35   ~ 0
GPIO_P3
Wire Wire Line
	4525 6050 4925 6050
Text Label 4925 6100 2    35   ~ 0
GPIO_N3
Wire Wire Line
	4525 6100 4925 6100
Text Label 4925 5850 2    35   ~ 0
GPIO_P1
NoConn ~ 2075 7350
Wire Wire Line
	2075 1175 1650 1175
Text Label 1650 1175 0    35   ~ 0
SPI_ADC_CLK
Wire Wire Line
	2075 1225 1650 1225
Text Label 1650 1225 0    35   ~ 0
SPI_ADC_DIO
Wire Wire Line
	2075 1275 1650 1275
Text Label 1650 1275 0    35   ~ 0
SPI_ADC_CSb1
Wire Wire Line
	2075 1325 1650 1325
Text Label 1650 1325 0    35   ~ 0
SPI_ADC_CSb2
Wire Wire Line
	2075 1375 1650 1375
Text Label 1650 1375 0    35   ~ 0
SPI_ADC_CSb3
Wire Wire Line
	2075 1425 1650 1425
Text Label 1650 1425 0    35   ~ 0
ADC_GPIO1_1
Wire Wire Line
	2075 1475 1650 1475
Text Label 1650 1475 0    35   ~ 0
ADC_GPIO1_2
Wire Wire Line
	2075 1525 1650 1525
Text Label 1650 1525 0    35   ~ 0
ADC_GPIO2_1
Wire Wire Line
	2075 1575 1650 1575
Text Label 1650 1575 0    35   ~ 0
ADC_GPIO2_2
Wire Wire Line
	2075 1625 1650 1625
Text Label 1650 1625 0    35   ~ 0
ADC_GPIO3_1
Wire Wire Line
	2075 1675 1650 1675
Text Label 1650 1675 0    35   ~ 0
ADC_GPIO3_2
Wire Wire Line
	2075 3275 1650 3275
Text Label 1650 3275 0    35   ~ 0
ADC_DCDC_Temp
$Comp
L power:GNDD #PWR01408
U 1 1 6459ECB4
P 1875 3325
F 0 "#PWR01408" H 1875 3075 50  0001 C CNN
F 1 "GNDD" H 1875 3175 50  0000 C CNN
F 2 "" H 1875 3325 50  0000 C CNN
F 3 "" H 1875 3325 50  0000 C CNN
	1    1875 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2075 3325 2025 3325
Wire Wire Line
	2075 3075 1575 3075
Text Label 1575 3075 0    35   ~ 0
FPGA_DCDC_Temp1
Wire Wire Line
	2075 2875 1575 2875
Text Label 1575 2875 0    35   ~ 0
FPGA_DCDC_Temp2
Wire Wire Line
	2075 2925 2025 2925
Wire Wire Line
	2025 2925 2025 3125
Connection ~ 2025 3325
Wire Wire Line
	2025 3325 1875 3325
Wire Wire Line
	2075 3125 2025 3125
Connection ~ 2025 3125
Wire Wire Line
	2025 3125 2025 3325
Text HLabel 8225 5050 0    39   Input ~ 0
ADC_DCDC_Temp
Text HLabel 8225 5100 0    39   Input ~ 0
FPGA_DCDC_Temp[1..2]
Text Label 1650 5400 0    30   ~ 0
Clk_LoL2
Wire Wire Line
	1650 5400 2075 5400
Wire Wire Line
	1650 5350 2075 5350
Text Label 1650 5350 0    30   ~ 0
Clk_OK2
NoConn ~ 2075 7200
NoConn ~ 2075 7250
NoConn ~ 2075 7300
Wire Wire Line
	5400 3275 5000 3275
Text Label 5000 3275 0    35   ~ 0
Loc_Rstb
NoConn ~ 2075 5500
NoConn ~ 2075 5550
Wire Wire Line
	9100 2375 9100 2475
Wire Wire Line
	10400 2375 10400 2475
Wire Wire Line
	9750 2375 9750 2475
$Comp
L Device:R R?
U 1 1 61379AC5
P 1875 3725
AR Path="/5BAC43A6/61379AC5" Ref="R?"  Part="1" 
AR Path="/57195CF6/61379AC5" Ref="R1403"  Part="1" 
F 0 "R1403" V 1950 3725 50  0000 C CNN
F 1 "240" V 1875 3725 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 1875 3725 50  0001 C CNN
F 3 "" H 1875 3725 50  0000 C CNN
	1    1875 3725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2075 3575 1875 3575
$Comp
L power:GNDD #PWR0109
U 1 1 61394B0F
P 1875 3875
F 0 "#PWR0109" H 1875 3625 50  0001 C CNN
F 1 "GNDD" H 1875 3725 50  0000 C CNN
F 2 "" H 1875 3875 50  0000 C CNN
F 3 "" H 1875 3875 50  0000 C CNN
	1    1875 3875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61397955
P 5200 3725
AR Path="/5BAC43A6/61397955" Ref="R?"  Part="1" 
AR Path="/57195CF6/61397955" Ref="R1405"  Part="1" 
F 0 "R1405" V 5275 3725 50  0000 C CNN
F 1 "240" V 5200 3725 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5200 3725 50  0001 C CNN
F 3 "" H 5200 3725 50  0000 C CNN
	1    5200 3725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 3575 5200 3575
$Comp
L power:GNDD #PWR0110
U 1 1 61398B9E
P 5200 3875
F 0 "#PWR0110" H 5200 3625 50  0001 C CNN
F 1 "GNDD" H 5200 3725 50  0000 C CNN
F 2 "" H 5200 3875 50  0000 C CNN
F 3 "" H 5200 3875 50  0000 C CNN
	1    5200 3875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 613B1FAD
P 4725 3725
AR Path="/5BAC43A6/613B1FAD" Ref="R?"  Part="1" 
AR Path="/57195CF6/613B1FAD" Ref="R1404"  Part="1" 
F 0 "R1404" V 4800 3725 50  0000 C CNN
F 1 "240" V 4725 3725 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4725 3725 50  0001 C CNN
F 3 "" H 4725 3725 50  0000 C CNN
	1    4725 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 3575 4725 3575
$Comp
L power:GNDD #PWR0111
U 1 1 613B3214
P 4725 3875
F 0 "#PWR0111" H 4725 3625 50  0001 C CNN
F 1 "GNDD" H 4725 3725 50  0000 C CNN
F 2 "" H 4725 3875 50  0000 C CNN
F 3 "" H 4725 3875 50  0000 C CNN
	1    4725 3875
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 613E63FC
P 8050 3725
AR Path="/5BAC43A6/613E63FC" Ref="R?"  Part="1" 
AR Path="/57195CF6/613E63FC" Ref="R1406"  Part="1" 
F 0 "R1406" V 8125 3725 50  0000 C CNN
F 1 "240" V 8050 3725 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 8050 3725 50  0001 C CNN
F 3 "" H 8050 3725 50  0000 C CNN
	1    8050 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3575 8050 3575
$Comp
L power:GNDD #PWR0112
U 1 1 613E7681
P 8050 3875
F 0 "#PWR0112" H 8050 3625 50  0001 C CNN
F 1 "GNDD" H 8050 3725 50  0000 C CNN
F 2 "" H 8050 3875 50  0000 C CNN
F 3 "" H 8050 3875 50  0000 C CNN
	1    8050 3875
	-1   0    0    -1  
$EndComp
Text Notes 1025 3850 0    50   ~ 0
0.1% precision\nfor DCI calibration
Text Notes 4675 4225 0    50   ~ 0
0.1% precision\nfor DCI calibration
Text Notes 8200 3825 0    50   ~ 0
0.1% precision\nfor DCI calibration
Text Label 8250 1625 2    35   ~ 0
PiN2_out_N5
$EndSCHEMATC
