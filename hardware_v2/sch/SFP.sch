EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 15
Title "Ethernet communication"
Date "2021-06-18"
Rev "1"
Comp "Irfu/DphP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MDJ_compo:SFP+ J201
U 1 1 57100D5A
P 1950 5350
F 0 "J201" H 1950 5625 60  0000 C CNN
F 1 "SFP+" H 1600 5350 60  0000 C CNN
F 2 "MDJ_mod:SFP+" H 1750 5350 60  0001 C CNN
F 3 "" H 1750 5350 60  0000 C CNN
F 4 "Digikey" H 1950 5350 60  0001 C CNN "Supplier"
F 5 "A120615-ND" H 1950 5350 60  0001 C CNN "Supplier P/N"
	1    1950 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0204
U 1 1 5718FAB0
P 2500 2100
F 0 "#PWR0204" H 2500 1850 50  0001 C CNN
F 1 "GNDD" H 2500 1950 50  0000 C CNN
F 2 "" H 2500 2100 50  0000 C CNN
F 3 "" H 2500 2100 50  0000 C CNN
	1    2500 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C202
U 1 1 5719049E
P 2400 1850
F 0 "C202" H 2400 1925 50  0000 C CNN
F 1 "100n" H 2400 1775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2400 1850 50  0001 C CNN
F 3 "" H 2400 1850 50  0000 C CNN
	1    2400 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C203
U 1 1 571906D8
P 2600 1850
F 0 "C203" H 2600 1925 50  0000 C CNN
F 1 "10u" H 2625 1775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2600 1850 50  0001 C CNN
F 3 "" H 2600 1850 50  0000 C CNN
	1    2600 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C201
U 1 1 5718E775
P 1850 1850
F 0 "C201" H 1850 1925 50  0000 C CNN
F 1 "100n" H 1850 1775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 1850 1850 50  0001 C CNN
F 3 "" H 1850 1850 50  0000 C CNN
	1    1850 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0203
U 1 1 5718E838
P 1850 2100
F 0 "#PWR0203" H 1850 1850 50  0001 C CNN
F 1 "GNDD" H 1850 1950 50  0000 C CNN
F 2 "" H 1850 2100 50  0000 C CNN
F 3 "" H 1850 2100 50  0000 C CNN
	1    1850 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:L L201
U 1 1 5718F869
P 2950 1500
F 0 "L201" V 2900 1375 50  0000 C CNN
F 1 "1uH" V 2900 1600 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2950 1500 50  0001 C CNN
F 3 "" H 2950 1500 50  0000 C CNN
	1    2950 1500
	0    1    1    0   
$EndComp
$Comp
L Device:L L202
U 1 1 5718F972
P 2950 1650
F 0 "L202" V 2900 1525 50  0000 C CNN
F 1 "1uH" V 2900 1750 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2950 1650 50  0001 C CNN
F 3 "" H 2950 1650 50  0000 C CNN
	1    2950 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C204
U 1 1 5718FCB4
P 3450 1850
F 0 "C204" H 3450 1925 50  0000 C CNN
F 1 "100n" H 3450 1775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3450 1850 50  0001 C CNN
F 3 "" H 3450 1850 50  0000 C CNN
	1    3450 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0202
U 1 1 5718FE28
P 3450 2025
F 0 "#PWR0202" H 3450 1775 50  0001 C CNN
F 1 "GNDD" H 3450 1875 50  0000 C CNN
F 2 "" H 3450 2025 50  0000 C CNN
F 3 "" H 3450 2025 50  0000 C CNN
	1    3450 2025
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0211
U 1 1 57190092
P 2150 4450
F 0 "#PWR0211" H 2150 4200 50  0001 C CNN
F 1 "GNDD" H 2150 4300 50  0000 C CNN
F 2 "" H 2150 4450 50  0000 C CNN
F 3 "" H 2150 4450 50  0000 C CNN
	1    2150 4450
	1    0    0    -1  
$EndComp
Text HLabel 4350 2500 2    39   Output ~ 0
SFP_Rx+
Text HLabel 4350 2800 2    39   Input ~ 0
SFP_Tx+
Text HLabel 6800 3100 2    60   Output ~ 0
SFP_Present
Text HLabel 6800 3300 2    60   Output ~ 0
SFP_LOS
Text HLabel 6800 3400 2    60   Output ~ 0
SFP_Tx_Fault
Text HLabel 6800 3700 2    60   Input ~ 0
SFP_SCL
Text HLabel 6800 3800 2    60   BiDi ~ 0
SFP_SDA
Text Notes 3500 1150 0    60   ~ 0
275mA
Wire Wire Line
	2850 5800 2850 5750
Wire Wire Line
	950  5800 1050 5800
Wire Wire Line
	1850 5800 1850 5750
Wire Wire Line
	1950 5750 1950 5800
Connection ~ 1950 5800
Wire Wire Line
	2050 5750 2050 5800
Connection ~ 2050 5800
Wire Wire Line
	2150 5750 2150 5800
Connection ~ 2150 5800
Wire Wire Line
	2250 5750 2250 5800
Connection ~ 2250 5800
Wire Wire Line
	2350 5750 2350 5800
Connection ~ 2350 5800
Wire Wire Line
	2450 5750 2450 5800
Connection ~ 2450 5800
Wire Wire Line
	2550 5750 2550 5800
Connection ~ 2550 5800
Wire Wire Line
	2650 5750 2650 5800
Connection ~ 2650 5800
Wire Wire Line
	2750 5750 2750 5800
Connection ~ 2750 5800
Wire Wire Line
	2400 2000 2400 2100
Wire Wire Line
	2600 2100 2600 2000
Wire Wire Line
	2200 2100 2200 1650
Wire Wire Line
	2200 1650 2400 1650
Wire Wire Line
	2600 1650 2600 1700
Wire Wire Line
	2400 1650 2400 1700
Connection ~ 2400 1650
Wire Wire Line
	2100 2100 2100 1500
Wire Wire Line
	1850 1500 2100 1500
Wire Wire Line
	1850 1500 1850 1700
Wire Wire Line
	1850 2100 1850 2000
Connection ~ 2600 1650
Connection ~ 2100 1500
Wire Wire Line
	3100 1650 3175 1650
Wire Wire Line
	3175 1650 3175 1500
Wire Wire Line
	3100 1500 3175 1500
Wire Wire Line
	3450 1500 3450 1700
Connection ~ 3175 1500
Wire Wire Line
	3450 1350 3450 1500
Connection ~ 3450 1500
Wire Wire Line
	1850 4200 1850 4350
Wire Wire Line
	1850 4350 1950 4350
Wire Wire Line
	2550 4350 2550 4200
Wire Wire Line
	2450 4200 2450 4350
Connection ~ 2450 4350
Wire Wire Line
	2350 4200 2350 4350
Connection ~ 2350 4350
Wire Wire Line
	2250 4200 2250 4350
Connection ~ 2250 4350
Wire Wire Line
	2050 4200 2050 4350
Connection ~ 2050 4350
Wire Wire Line
	1950 4200 1950 4350
Connection ~ 1950 4350
Wire Wire Line
	2150 4350 2150 4450
Connection ~ 2150 4350
Wire Wire Line
	3250 3100 6050 3100
Wire Wire Line
	3250 3300 5750 3300
Wire Wire Line
	3250 3400 5450 3400
Wire Wire Line
	3250 3700 6350 3700
Wire Wire Line
	3250 3800 6650 3800
Wire Wire Line
	3250 2500 4350 2500
Wire Wire Line
	3250 2600 4350 2600
$Comp
L Device:R R201
U 1 1 578454F5
P 5450 2750
F 0 "R201" V 5530 2750 50  0000 C CNN
F 1 "4.7k" V 5450 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 5450 2750 50  0001 C CNN
F 3 "" H 5450 2750 50  0000 C CNN
	1    5450 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2900 5450 3400
Connection ~ 5450 3400
Wire Wire Line
	5450 2600 5450 2350
$Comp
L power:GNDD #PWR0210
U 1 1 5784595C
P 3550 4050
F 0 "#PWR0210" H 3550 3800 50  0001 C CNN
F 1 "GNDD" H 3550 3900 50  0000 C CNN
F 2 "" H 3550 4050 50  0000 C CNN
F 3 "" H 3550 4050 50  0000 C CNN
	1    3550 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3500 3550 4050
$Comp
L Device:R R202
U 1 1 57845B85
P 5750 2750
F 0 "R202" V 5830 2750 50  0000 C CNN
F 1 "4.7k" V 5750 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 5750 2750 50  0001 C CNN
F 3 "" H 5750 2750 50  0000 C CNN
	1    5750 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2600 5750 2350
Wire Wire Line
	5750 2900 5750 3300
Connection ~ 5750 3300
$Comp
L Device:R R203
U 1 1 57845DD6
P 6050 2750
F 0 "R203" V 6130 2750 50  0000 C CNN
F 1 "4.7k" V 6050 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6050 2750 50  0001 C CNN
F 3 "" H 6050 2750 50  0000 C CNN
	1    6050 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2600 6050 2350
Wire Wire Line
	6050 2900 6050 3100
$Comp
L MDJ_compo:VccO #PWR0205
U 1 1 578F4E91
P 5450 2350
F 0 "#PWR0205" H 5450 2200 50  0001 C CNN
F 1 "VccO" H 5450 2500 50  0000 C CNN
F 2 "" H 5450 2350 50  0000 C CNN
F 3 "" H 5450 2350 50  0000 C CNN
	1    5450 2350
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR0206
U 1 1 578F5416
P 5750 2350
F 0 "#PWR0206" H 5750 2200 50  0001 C CNN
F 1 "VccO" H 5750 2500 50  0000 C CNN
F 2 "" H 5750 2350 50  0000 C CNN
F 3 "" H 5750 2350 50  0000 C CNN
	1    5750 2350
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR0207
U 1 1 578F5446
P 6050 2350
F 0 "#PWR0207" H 6050 2200 50  0001 C CNN
F 1 "VccO" H 6050 2500 50  0000 C CNN
F 2 "" H 6050 2350 50  0000 C CNN
F 3 "" H 6050 2350 50  0000 C CNN
	1    6050 2350
	1    0    0    -1  
$EndComp
Connection ~ 6050 3100
$Comp
L MDJ_compo:VccSFP #PWR0201
U 1 1 578F7539
P 3450 1350
F 0 "#PWR0201" H 3450 1200 50  0001 C CNN
F 1 "VccSFP" H 3450 1500 50  0000 C CNN
F 2 "" H 3450 1350 50  0000 C CNN
F 3 "" H 3450 1350 50  0000 C CNN
	1    3450 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R204
U 1 1 579C79BA
P 6350 2750
F 0 "R204" V 6430 2750 50  0000 C CNN
F 1 "4.7k" V 6350 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6350 2750 50  0001 C CNN
F 3 "" H 6350 2750 50  0000 C CNN
	1    6350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2600 6350 2350
$Comp
L Device:R R205
U 1 1 579C79C1
P 6650 2750
F 0 "R205" V 6730 2750 50  0000 C CNN
F 1 "4.7k" V 6650 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6650 2750 50  0001 C CNN
F 3 "" H 6650 2750 50  0000 C CNN
	1    6650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2600 6650 2350
$Comp
L MDJ_compo:VccO #PWR0208
U 1 1 579C79C8
P 6350 2350
F 0 "#PWR0208" H 6350 2200 50  0001 C CNN
F 1 "VccO" H 6350 2500 50  0000 C CNN
F 2 "" H 6350 2350 50  0000 C CNN
F 3 "" H 6350 2350 50  0000 C CNN
	1    6350 2350
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:VccO #PWR0209
U 1 1 579C79CE
P 6650 2350
F 0 "#PWR0209" H 6650 2200 50  0001 C CNN
F 1 "VccO" H 6650 2500 50  0000 C CNN
F 2 "" H 6650 2350 50  0000 C CNN
F 3 "" H 6650 2350 50  0000 C CNN
	1    6650 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2900 6350 3700
Connection ~ 6350 3700
Wire Wire Line
	6650 2900 6650 3800
Connection ~ 6650 3800
Wire Wire Line
	3550 3500 3250 3500
$Comp
L power:GNDD #PWR0212
U 1 1 57B4E699
P 2350 5900
F 0 "#PWR0212" H 2350 5650 50  0001 C CNN
F 1 "GNDD" H 2350 5750 50  0000 C CNN
F 2 "" H 2350 5900 50  0000 C CNN
F 3 "" H 2350 5900 50  0000 C CNN
	1    2350 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  5800 950  5750
Connection ~ 1850 5800
Wire Wire Line
	1050 5750 1050 5800
Connection ~ 1050 5800
Wire Wire Line
	1150 5750 1150 5800
Connection ~ 1150 5800
Wire Wire Line
	1250 5750 1250 5800
Connection ~ 1250 5800
Wire Wire Line
	1350 5750 1350 5800
Connection ~ 1350 5800
Wire Wire Line
	1450 5750 1450 5800
Connection ~ 1450 5800
Wire Wire Line
	1550 5750 1550 5800
Connection ~ 1550 5800
Wire Wire Line
	1650 5750 1650 5800
Connection ~ 1650 5800
Wire Wire Line
	1750 5750 1750 5800
Connection ~ 1750 5800
NoConn ~ 3250 3200
Wire Wire Line
	1950 5800 2050 5800
Wire Wire Line
	2050 5800 2150 5800
Wire Wire Line
	2150 5800 2250 5800
Wire Wire Line
	2250 5800 2350 5800
Wire Wire Line
	2350 5800 2350 5900
Wire Wire Line
	2350 5800 2450 5800
Wire Wire Line
	2450 5800 2550 5800
Wire Wire Line
	2550 5800 2650 5800
Wire Wire Line
	2650 5800 2750 5800
Wire Wire Line
	2750 5800 2850 5800
Wire Wire Line
	2400 1650 2600 1650
Wire Wire Line
	2600 1650 2800 1650
Wire Wire Line
	2100 1500 2800 1500
Wire Wire Line
	3175 1500 3450 1500
Wire Wire Line
	2450 4350 2550 4350
Wire Wire Line
	2350 4350 2450 4350
Wire Wire Line
	2250 4350 2350 4350
Wire Wire Line
	2050 4350 2150 4350
Wire Wire Line
	1950 4350 2050 4350
Wire Wire Line
	2150 4350 2250 4350
Wire Wire Line
	5450 3400 6800 3400
Wire Wire Line
	5750 3300 6800 3300
Wire Wire Line
	6050 3100 6800 3100
Wire Wire Line
	6350 3700 6800 3700
Wire Wire Line
	6650 3800 6800 3800
Wire Wire Line
	1850 5800 1950 5800
Wire Wire Line
	1050 5800 1150 5800
Wire Wire Line
	1150 5800 1250 5800
Wire Wire Line
	1250 5800 1350 5800
Wire Wire Line
	1350 5800 1450 5800
Wire Wire Line
	1450 5800 1550 5800
Wire Wire Line
	1550 5800 1650 5800
Wire Wire Line
	1650 5800 1750 5800
Wire Wire Line
	1750 5800 1850 5800
Wire Wire Line
	3250 2900 4350 2900
Wire Wire Line
	3250 2800 4350 2800
Text HLabel 4350 2600 2    39   Output ~ 0
SFP_Rx-
Text HLabel 4350 2900 2    39   Input ~ 0
SFP_Tx-
Wire Wire Line
	3450 2000 3450 2025
$Comp
L MDJ_compo:SFP+ J201
U 2 1 5C94BECF
P 2350 3150
F 0 "J201" H 1975 3250 60  0000 R CNN
F 1 "SFP+" H 2050 3150 60  0000 R CNN
F 2 "MDJ_mod:SFP+" H 2550 3200 60  0001 C CNN
F 3 "" H 2550 3200 60  0000 C CNN
	2    2350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2100 2500 2100
Connection ~ 2500 2100
Wire Wire Line
	2500 2100 2600 2100
$Comp
L power:PWR_FLAG #FLG0201
U 1 1 629AC65B
P 2100 1500
F 0 "#FLG0201" H 2100 1575 50  0001 C CNN
F 1 "PWR_FLAG" H 2100 1673 50  0000 C CNN
F 2 "" H 2100 1500 50  0001 C CNN
F 3 "~" H 2100 1500 50  0001 C CNN
	1    2100 1500
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0202
U 1 1 629ACDB4
P 2600 1650
F 0 "#FLG0202" H 2600 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 2375 1725 50  0000 C CNN
F 2 "" H 2600 1650 50  0001 C CNN
F 3 "~" H 2600 1650 50  0001 C CNN
	1    2600 1650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
